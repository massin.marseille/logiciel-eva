<?php

namespace Indicator\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Project;
use Campain\Entity\Campain;
use Campain\Entity\Indicator as CampainIndicator;
use User\Entity\User;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;
use DateTime;

/**
 * Class Measure
 *
 * @package Indicator\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="indicator_measure")
 */
class Measure extends BasicRestEntityAbstract
{
	const PERIOD_START        = 'start';
	const PERIOD_INTERMEDIATE = 'intermediate';
	const PERIOD_END          = 'end';
	const PERIOD_NR 		  = 'notprovided';

	const TYPE_DONE   = 'done';
	const TYPE_TARGET = 'target';
	const TYPE_NR     = 'notprovided';
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var Indicator
	 *
	 * @ORM\ManyToOne(targetEntity="Indicator\Entity\Indicator", inversedBy="measures")
	 */
	protected $indicator;

	/**
	 * @var GroupIndicator
	 *
	 * @ORM\ManyToOne(targetEntity="Indicator\Entity\GroupIndicator")
	 */
	protected $groupIndicator;
		
	/**
	 * @var Campain
	 *
	 * @ORM\ManyToOne(targetEntity="Campain\Entity\Campain", inversedBy="measures")
	 */
	protected $campain;

	/**
	 * @var CampainIndicator
	 *
	 * @ORM\ManyToOne(targetEntity="Campain\Entity\Indicator", inversedBy="measures")
	 * @ORM\JoinColumn(name="campainIndicator_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $campainIndicator;

	
	/**
	 * @var CampainIndicator
	 *
	 * @ORM\ManyToOne(targetEntity="Campain\Entity\Measure", inversedBy="measures")
	 * @ORM\JoinColumn(name="campainMeasure_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $campainMeasure;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $period;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	protected $type;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $date;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $start;

	/**
	 * @var Project
	 *
	 * @ORM\ManyToOne(targetEntity="Project\Entity\Project")
	 */
	protected $project;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", name="`value`", nullable=true)
	 */
	protected $value;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $comment;
	
	/**
	* @ORM\Column(name="client", type="json", nullable=true)
	*
	* @var array
	*/
	protected $client;
	
    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $master;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean", options={"default": false})
	 */
	protected $transverse = false;
	
	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $source;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getPeriod()
	{
		return $this->period;
	}

	/**
	 * @param string $period
	 */
	public function setPeriod($period)
	{
		$this->period = $period;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param \DateTime $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}

	/**
	 * @return \DateTime
	 */
	public function getStart()
	{
		return $this->start;
	}

	/**
	 * @param \DateTime $start
	 */
	public function setStart($start)
	{
		$this->start = $start;
	}

	/**
	 * @return Indicator
	 */
	public function getIndicator()
	{
		return $this->indicator;
	}

	/**
	 * @param Indicator $indicator
	 */
	public function setIndicator($indicator)
	{
		$this->indicator = $indicator;
	}

	/**
	 * @return Campain
	 */
	public function getCampain()
	{
		return $this->campain;
	}

	/**
	 * @param Campain $campain
	 */
	public function setCampain($campain)
	{
		$this->campain = $campain;
	}

	/**
	 * @return GroupIndicator
	 */
	public function getGroupIndicator()
	{
		return $this->groupIndicator;
	}

	/**
	 * @param GroupIndicator $groupIndicator
	 */
	public function setGroupIndicator($groupIndicator)
	{
		$this->groupIndicator = $groupIndicator;
	}
	
	/**
	 * @return CampainIndicator
	 */
	public function getCampainIndicator()
	{
		return $this->campainIndicator;
	}

	/**
	 * @param CampainIndicator $campainIndicator
	 */
	public function setCampainIndicator($campainIndicator)
	{
		$this->campainIndicator = $campainIndicator;
	}
	
	/**
	 * @return CampainMeasure
	 */
	public function getCampainMeasure()
	{
		return $this->campainMeasure;
	}

	/**
	 * @param CampainMeasure $campainMeasure
	 */
	public function setCampainMeasure($campainMeasure)
	{
		$this->campainMeasure = $campainMeasure;
	}

	/**
	 * @return Project
	 */
	public function getProject()
	{
		return $this->project;
	}

	/**
	 * @param Project $project
	 */
	public function setProject($project)
	{
		$this->project = $project;
	}

	/**
	 * @return float
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param float $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @param string $comment
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;
	}

	/**
	 * @return bool
	 */
	public function getTransverse()
	{
		return $this->transverse;
	}

	/**
	 * @param bool $transverse
	 */
	public function setTransverse($transverse)
	{
		$this->transverse = $transverse;
	}

	/**
	 * @return array
	 */
	public function getFixedValue()
	{
		if ($this->indicator !=null && $this->indicator->getType() == Indicator::TYPE_FIXED) {
			foreach ($this->indicator->getValues() as $value) {
				if ($value['value'] == $this->value) {
					return $value;
				}
			}
		}

		return [
			'text'  => $this->value,
			'color' => 'initial',
			'icon'  => null
		];
	}

	public function isYear($year)
	{
		if ($year !== null) {
			return $this->getDate()->format('Y') == $year;
		}

		return true;
	}

	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilterFactory = new Factory();
		$inputFilter = $inputFilterFactory->createInputFilter([
	[
				'name'     => 'period',
				'required' => false,
				'validators' => [
					[
						'name' => 'InArray',
						'options' => [
							'haystack' => self::getAllPeriods()
						],
					]
				],
			],
			[
				'name'     => 'type',
				'required' => true,
				'validators' => [
					[
						'name' => 'InArray',
						'options' => [
							'haystack' => self::getAllTypes()
						],
					]
				],
			],
			[
				'name'     => 'date',
				'required' => false,
				'filters' => [
					['name' => 'Core\Filter\DateTimeFilter'],
				],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) {
								if(isset( $context['date'])){
									$endDate = $context['date'];
									if(!($endDate instanceof DateTime)) {
										$endDate = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $context['date'])
											? DateTime::createFromFormat('d/m/Y H:i', $context['date'])
											: DateTime::createFromFormat('d/m/Y', $context['date']);
									}
									if(isset($context['start'])){
										$startDate  = $context['start'];
										if(!($startDate instanceof DateTime)) {
											$startDate = preg_match('#\d{2}/\d{2}/\d{4} \d{2}:\d{2}#', $context['start'])
												? DateTime::createFromFormat('d/m/Y H:i', $context['start'])
												: DateTime::createFromFormat('d/m/Y', $context['start']);
										}
										return $startDate <= $endDate;
									}
								}
								return true;
                            },
                            'message'  => 'measure_field_enddate_before_startdate_error',
                        ],
                    ],
                ]
			],
			[
				'name'     => 'start',
				'required' => false,
				'filters' => [
					['name' => 'Core\Filter\DateTimeFilter'],
				],
			],
			[
				'name'     => 'project',
				'required' => false,
				'filters'  => [
					['name' => 'Core\Filter\IdFilter'],
				],
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value) use ($entityManager) {
								if (is_array($value)) {
									$value = $value['id'];
								}
								$project = $entityManager->getRepository('Project\Entity\Project')->find($value);

								return $project !== null;
							},
							'message' => 'measure_field_project_error_non_exists'
						],
					]
				],
			],
			[
				'name'     => 'campain',
				'required' => false,
				'filters'  => [
					['name' => 'Core\Filter\IdFilter'],
				],
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value) use ($entityManager) {
								if (is_array($value)) {
									$value = $value['id'];
								}
								$campain = $entityManager->getRepository('Campain\Entity\Campain')->find($value);

								return $campain !== null;
							},
							'message' => 'measure_field_campain_error_non_exists'
						],
					]
				],
			],
			[
				'name'     => 'indicator',
				'required' => true,
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value) use ($entityManager) {
								if (is_array($value)) {
									$value = $value['id'];
								}
								 $query = $entityManager->createQuery();
								 $query->setDQL('SELECT i FROM Indicator\Entity\Indicator i
                                                WHERE i.id = :value
                                                OR i.name = :value');
								$query->setParameter('value', trim($value));
								$indicator = $query->getOneOrNullResult();
								return $indicator !== null;
							},
							'message' => 'measure_field_indicator_error_non_exists'
						],
					]
				],
			],
			[
				'name'     => 'value',
				'required' => false,
				// 'filters' => [
				// 	['name' => 'NumberParse'],
				// ],
			],
			[
				'name'     => 'comment',
				'required' => false,
				'filters' => [
					['name' => 'StringTrim'],
					['name' => 'StripTags']
				],
			],
			[
				'name'     => 'transverse',
				'required' => false,
				'allow_empty' => true,
				'filters' => [
					['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
				],
			],
			[
				'name'     => 'source',
				'required' => false,
				'filters' => [
					['name' => 'StringTrim'],
					['name' => 'StripTags']
				],
			],
			[
				'name'     => 'client',
				'required' => false,
			],
            [
                'name'     => 'master',
                'required' => false
            ],
		]);

		return $inputFilter;
	}

	/**
	 * @return array
	 */
	public static function getPeriods()
	{
		return [
			self::PERIOD_START,
			self::PERIOD_INTERMEDIATE,
			self::PERIOD_END
		];
	}

	/**
	 * @return array
	 */
	public static function getAllPeriods()
	{
		$periods = self::getPeriods();
		$periods[] = self::PERIOD_NR;
		return $periods;
	}
	/**
	 * @return array
	 */
	public static function getTypes()
	{
		return [
			self::TYPE_DONE,
			self::TYPE_TARGET
		];
	}

		/**
	 * @return array
	 */
	public static function getAllTypes()
	{
		$types = self::getTypes();
		$types[] = self::TYPE_NR;
		return $types;
	}

	/**
	 * @param User $user
	 * @param string $crudAction
	 * @param null   $owner
	 * @param null   $entityManager
	 * @return bool
	 */
	public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
	{
		if ($this->getProject()) {
			return $this->getProject()->ownerControl($user, $crudAction, $owner, $entityManager);
		}

		return false;
	}

	/**
	 * Get the value of source
	 *
	 * @return  array
	 */ 
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * Set 	*
	 *
	 * @param  array  $client  	*
	 *
	 * @return  self
	 */ 
	public function setClient($client)
	{
		$this->client = $client;

		return $this;
	}

    /**
     * Get the value of master
     *
     * @return  int
     */ 
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * Set the value of master
     *
     * @param  int  $master
     *
     * @return  self
     */ 
    public function setMaster($master)
    {
        $this->master = $master;

        return $this;
	}	
	
	/**
	* Get the value of source
	*
	* @return  string
	*/ 
	public function getSource()
	{
		return $this->source;
	}

	/**
		* Set the value of source
		*
		* @param  string  $source
		*
		* @return  self
		*/ 
	public function setSource($source)
	{
		$this->source = $source;

		return $this;
	}

}
