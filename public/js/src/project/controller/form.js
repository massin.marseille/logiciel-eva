(function(app) {
	app.controller('projectFormController', [
		'configurationService',
		'projectService',
		'templateService',
		'flashMessenger',
		'$scope',
		'$filter',
		'translator',
		'$timeout',
		'$rootScope',
		function projectFormController(
			configurationService,
			projectService,
			templateService,
			flashMessenger,
			$scope,
			$filter,
			translator,
			$timeout,
			$rootScope,
		) {
			var self = this;

			self.customFieldsSaveObject = null;

			var apiParams = {
				col: [
					'id',
					'name',
					'code',
					'parent.id',
					'parent.code',
					'parent.name',
					'keywords',
					'description',
					'result',
					'prevResult',
					'context',
					'objectifs',
                    'level',
					'members.id',
					'members.user.id',
					'members.user.name',
					'members.user.avatar',
					'members.role',
					'members.keywords',
					'publicationStatus',
					'programmingDate',
					'plannedDateStart',
					'plannedDateEnd',
					'realDateStart',
					'realDateEnd',
					'external',
					'networkAccessible',
					'ownership.id',
					'ownership.name',
					'hierarchySup.id',
					'hierarchySup.code',
					'hierarchySup.name',
					'budgetStatus.id',
					'budgetStatus.name',
					'budgetStatus.lock',
					'level',
					'template.id',
					'template.name',
					'template.config',
					'globalTimeTarget',
					'budgetCodes.id',
					'budgetCodes.name',
				],
			};

			self.loading = {
				save: false,
			};

			self.errors = {
				fields: {},
			};

			self.id = null;

			self.project = {
				members: [],
				managers: [],
				validators: [],
				publicationStatus: 'draft',
				_rights: {
					update: true,
				},
			};
			$scope.project = self.project;

			self.templates = [];

			self.exportModel = 'default';

			$scope.$on('loadProject', () => {
				self.loadProject();
			});

			self.init = function init(id, user) {
				self.currentUser = user;
				templateService
					.findAll({
						col: ['id', 'name', 'config', 'default'],
					})
					.then(
						function(res) {
							if (!res.success) {
								flashMessenger.error(translator('error_occured'));
								return;
							}

							self.templates = res.rows;

							if (!id) {
								for (var i in self.templates) {
									var template = self.templates[i];
									if (template.default == true) {
										self.project.template = template;
									}
								}
							}
						},
						function() {
							flashMessenger.error(translator('error_occured'));
						},
					);

				self.id = id;

				if (self.id) {
					self.loadProject();
				}
			};

            self.loadProject = function() {
                projectService.find(self.id, apiParams).then(function(res) {
					self.project = res.object;
                    self.customFieldsSaveObject = { entity: 'project', id: res.object.id };
					self.parseMembers();
                    self.panelTitle =
                        (self.project.code ? self.project.code + ' - ' : '') + self.project.name;

                    self.goToHash();

                    $rootScope.lockBudget = self.project.budgetStatus
                        ? self.project.budgetStatus.lock
                        : false;

                    // Dans le cas ou le template associé à la fiche n'est pas dispo pour le user, on le rajoute dans la liste
                    if (self.project.template) {
                        if (!self.templates.find(template => template.id === self.project.template.id)) {
                            self.templates.push(self.project.template);
                        }
                    }
                });
            };

			self.saveProject = function saveProject(force, mail) {
				self.errors.fields = {};
				self.loading.save = true;

				if (angular.isArray(self.project.parent)) {
					self.project.parent = null;
				}

				var isCreation = !self.project.id;
				if (force === true) {
					apiParams.force = true;
				} else {
					delete apiParams['force'];
				}

				if (mail === true) {
					apiParams.mail = true;
				} else {
					delete apiParams['mail'];
				}

				if (!self.project.id) {
					self.project.managers.push(self.currentUser);
					self.project.members.push({'user': self.currentUser, 'role': 'manager'});
				}
				projectService.save(self.project, apiParams).then(
					function(res) {
						if (!res.success) {
							flashMessenger.error(translator('error_occured'));
							return;
						}

						self.project = res.object;
						self.customFieldsSaveObject = { entity: 'project', id: res.object.id };
						
						$scope.$broadcast("custom-fields-save", { entity: 'project', id: self.project.id });

						self.lock();
						self.parseMembers();

						self.panelTitle =
							(self.project.code ? self.project.code + ' - ' : '') + self.project.name;
						self.loading.save = false;

						if (isCreation) {
							var url = window.location.pathname + '/' + self.project.id;
							history.replaceState('', '', url);
						}

						flashMessenger.success(translator('project_message_saved'));

						$rootScope.lockBudget = self.project.budgetStatus
							? self.project.budgetStatus.lock
							: false;
					},
					function(err) {
						flashMessenger.error(translator('error_occured'));

						for (var field in err.fields) {
							self.errors.fields[field] = err.fields[field];
						}

						self.loading.save = false;
					},
				);
			};

			self.deleteProject = function deleteProject() {
				if (
					confirm(
						translator('project_question_delete').replace(
							'%1',
							(self.project.code ? self.project.code + ' - ' : '') + self.project.name,
						),
					)
				) {
					projectService.remove(self.project).then(
						function(res) {
							if (!res.success) {
								flashMessenger.error(translator('error_occured'));
								return;
							}

							window.location = '/project';
						},
						function() {
							flashMessenger.error(translator('error_occured'));
						},
					);
				}
			};

			self.lock = function() {
				self.project.locked =
					self.project.publicationStatus === 'waiting_validation' ||
					self.project.publicationStatus === 'validated' ||
					self.project.publicationStatus === 'published';
			};

			$scope.$watch(
				function() {
					return self.project.publicationStatus;
				},
				function() {
					self.lock();
				},
			);

			$scope.$watch(
				function() {
					return self.project.managers;
				},
				function() {
					self.synchronizeMembers();
				},
			);

			$scope.$watch(
				function() {
					return self.project.validators;
				},
				function() {
					self.synchronizeMembers();
				},
			);

			self.parseMembers = function parseMembers() {
				self.project.managers = [];
				var managers = $filter('filter')(self.project.members, { role: 'manager' });
				for (var i in managers) {
					self.project.managers.push(managers[i].user);
				}

				self.project.validators = [];
				var validators = $filter('filter')(self.project.members, { role: 'validator' });
				for (var i in validators) {
					self.project.validators.push(validators[i].user);
				}
			};

			self.synchronizeMembers = function() {
				if (typeof self.project.members !== 'object' || self.project.members == null) {
					self.project.members = [];
				}

				var prevManagers = $filter('filter')(self.project.members, function(item) { 
					return item.role === 'manager';
				 });
				var newManagers = self.project.managers;

				for (var i in prevManagers) {
					var prevManager = prevManagers[i];
					var filter = $filter('filter')(newManagers, function(item) { 
						return item.id === prevManager.user.id; 
					});
					if (filter && filter.length == 0) {
						self.project.members.splice(self.project.members.indexOf(prevManager), 1);
					}
				}

				for (var i in newManagers) {
					var newManager = newManagers[i];
					var filter = $filter('filter')(prevManagers, function(item) {
						return item.user.id === newManager.id;
					});
					if (filter && filter.length == 0) {
						self.project.members.push({
							user: {
								id: newManager.id,
								name: newManager.name,
								avatar: newManager.avatar,
							},
							role: 'manager',
						});
					}
				}

				var prevValidators = $filter('filter')(self.project.members, function(item) { 
					return item.role === 'validator';
				 });
				var newValidators = self.project.validators;

				for (var i in prevValidators) {
					var prevValidator = prevValidators[i];
					var filter = $filter('filter')(newValidators, function(item) { 
						return item.id === prevValidator.user.id;
					});
					if (filter && filter.length == 0) {
						self.project.members.splice(self.project.members.indexOf(prevValidator), 1);
					}
				}

				for (var i in newValidators) {
					var newValidator = newValidators[i];
					var filter = $filter('filter')(prevValidators, function(item) {
						return item.user.id === newValidator.id;
					});
					if (filter && filter.length == 0) {
						self.project.members.push({
							user: {
								id: newValidator.id,
								name: newValidator.name,
								avatar: newValidator.avatar,
							},
							role: 'validator',
						});
					}
				}
			};

			self.newMembers = [];
			var memberRights = { _rights: { update: true } };
			self.memberRights = angular.copy(memberRights);

			self.addMembers = function addMembers() {
				if (self.project.members == null) {
					self.project.members = [];
				}
				for (var i in self.newMembers) {
					var member = self.newMembers[i];
					var filter = $filter('filter')(self.project.members, function(item) {
						return item.user.id === member.id;
					}, true);
					if (filter == null || filter.length == 0) {
						self.project.members.push({
							user: {
								id: member.id,
								name: member.name,
								avatar: member.avatar,
							},
							role: 'member',
							keywords: angular.copy(self.memberRights.keywords)
						});
					}
				}
				self.newMembers = [];
				self.memberRights = angular.copy(memberRights);
				self.saveProject();
				angular.element('#membersModal').modal('hide');
			};

			self.removeMember = function removeMember(member) {
				self.project.members.splice(self.project.members.indexOf(member), 1);
				self.saveProject();
			};

			self.isValidator = function() {
				for (var i in self.project.members) {
					var validator = self.project.members[i];
					if (validator.user.id == self.currentUser && validator.role == 'validator') {
						return true;
					}
				}
				return false;
			};

			self.isManager = function() {
				for (var i in self.project.members) {
					var manager = self.project.members[i];
					if (manager.user.id == self.currentUser && manager.role == 'manager') {
						return true;
					}
				}
				return false;
			};

			self.changePublicationStatus = function changePublicationStatus() {
				if (self.project.id) {
					var mail = false;
					if (
						self.project.publicationStatus == 'waiting_validation' &&
						confirm(translator('project_message_notification_validators'))
					) {
						mail = true;
					} else if (
						self.project.publicationStatus == 'validated' &&
						confirm(translator('project_message_notification_managers'))
					) {
						mail = true;
					}

					self.saveProject(true, mail);
				}
			};

			self.showExportModal = function(type) {
				angular.element('#export' + type + 'Modal2').modal('show');
				self.exportFilters = {
					id: self.project.id,
					tree: 'initial',
					min: self.project.level,
					max: self.project.level,
					reference: self.project.level,
					levels: [
						{
							value: self.project.level,
							mode: 'parent',
							filters: {
								id: {
									op: 'eq',
									val: self.project.id,
								},
							},
						},
					],
				};
			};

			self.export = function(type) {
				angular.element('#export' + type + 'Form2').submit();
				angular.element('#export' + type + 'Modal2').modal('hide');
			};

			self.goToHash = function goToHash() {
				$timeout(function() {
					if (window.location.hash) {
						angular
							.element('a[href="' + window.location.hash + '"][data-toggle="tab"]')
							.trigger('click');
					}
				});
			};

			angular.element('.project-form-nav a').click(function(e) {
				window.location.hash = this.hash;
			});

            self.generateCode = function generateCode() {
				configurationService.find(1, {col:['code']}).then(function(res) {
					var configuration = res.object;
					var code = "000000" + configuration.code;
					var year = self.project.plannedDateStart ? self.project.plannedDateStart.substring(8,10) : new Date().getFullYear().toString().substring(2, 4);
					self.project.code = year + 'PRJ' + code.substr(code.length-6);
					configuration.code++;
					configurationService.save(configuration).then(function() {})
				});
            }
		},
	]);

	app.directive('showTpl', [
		function() {
			return {
				restrict: 'A',
				scope: {
					template: '=showTpl',
					type: '@tplFieldType',
					name: '@tplFieldName',
					default: '@tplFieldDefault',
				},
				link: function(scope, element, attrs) {
					var name = typeof scope.name == 'undefined' ? attrs.name : scope.name;
					var type = typeof scope.type == 'undefined' ? 'fields' : scope.type;

					scope.$watch(
						function() {
							return scope.template;
						},
						function() {
							var show = scope.default !== 'false';

							if (typeof scope.template !== 'undefined' && scope.template !== null) {
								if (typeof scope.template.config !== 'undefined') {
									if (typeof scope.template.config[type] !== 'undefined') {
										if (typeof scope.template.config[type][name] !== 'undefined') {
											if (typeof scope.template.config[type][name].show !== 'undefined') {
												show =
													scope.template.config[type][name].show === 'true' ||
													scope.template.config[type][name].show === true;
											}
										}
									}
								}
							}

							element.toggle(show);
						},
					);
				},
			};
		},
	]);
})(window.EVA.app);
