<?php

namespace Indicator\Export;

use Core\Export\IExporter;

abstract class GroupIndicator implements IExporter
{
    public static function getConfig()
    {
        return [
            'indicators' => [
                'property' => 'indicators.name'
            ],
            'type'     => [
                'format' => function ($value, $row, $translator) {
                    return $translator('measure_type_' . $value);
                },
            ],
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
