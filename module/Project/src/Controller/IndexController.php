<?php

namespace Project\Controller;

use Age\Service\AgeSynchronisationServiceInterface;
use Budget\Service\SimplifiedInputService;
use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use Core\Utils\Tree;
use Export\Entity\Custom;
use Export\Entity\Export;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Project\Entity\AgeSynchronisation;
use Project\Entity\Project;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{

	public function indexAction()
	{
		if (!$this->acl()->isAllowed('project:e:project:r')) {
			return $this->notFoundAction();
		}

		$exports = $this->entityManager()
			->getRepository('Export\Entity\Export')
			->findAll();
		if (!$exports) {
			$exports = [];
		}
		$customExportsWord = [];
		$customExportsExcel = [];
		$customExports = Custom::getExports(
			$this->environment()
				->getClient()
				->getKey(),
			'project'
		);

		/** @var Custom $export */
		foreach ($customExports as $export) {
			if ($export->getType() === Export::TYPE_WORD) {
				$customExportsWord[] = $export;
			} elseif ($export->getType() === Export::TYPE_EXCEL) {
				$customExportsExcel[] = $export;
			}
		}

		return new ViewModel([
			'exports' => $exports,
			'customExportsWord' => $customExportsWord,
			'customExportsExcel' => $customExportsExcel,
			'budgetCode' => $this->moduleManager()->getClientModuleConfiguration('budget', 'code'),
		]);
	}

	public function formAction()
	{
		$id = $this->params()->fromRoute('id', false);

		if (!$this->acl()->basicFormAccess('project:e:project', $id)) {
			return $this->notFoundAction();
		}

		/** @var Project $project */
		$project = $this->entityManager()
			->getRepository('Project\Entity\Project')
			->find($id);

		if ($id && !$project) {
			$this->redirect()->toRoute('project/form');
		}

		if (
			$project &&
			!$this->acl()->ownerControl('project:e:project:u', $project) &&
			!$this->acl()->ownerControl('project:e:project:r', $project)
		) {
			return $this->notFoundAction();
		}

		$groups = [];
		if ($this->moduleManager()->isActive('keyword')) {
			$groups = $this->allowedKeywordGroups('project', false);
		}

		$exports = $this->entityManager()
			->getRepository('Export\Entity\Export')
			->findAll();
		if (!$exports) {
			$exports = [];
		}
		$listCustomExportsWord = [];
		$listCustomExportsExcel = [];
		$listCustomExports = Custom::getExports(
			$this->environment()
				->getClient()
				->getKey(),
			'project'
		);

		/** @var Custom $export */
		foreach ($listCustomExports as $export) {
			if ($export->getType() === Export::TYPE_WORD) {
				$listCustomExportsWord[] = $export;
			} elseif ($export->getType() === Export::TYPE_EXCEL) {
				$listCustomExportsExcel[] = $export;
			}
		}

		$customExportsWord = [];
		$customExportsExcel = [];
		$customExports = Custom::getExports(
			$this->environment()
				->getClient()
				->getKey(),
			'project-form'
		);

		/** @var Custom $export */
		foreach ($customExports as $export) {
			if ($export->getType() === Export::TYPE_WORD) {
				$customExportsWord[] = $export;
			} elseif ($export->getType() === Export::TYPE_EXCEL) {
				$customExportsExcel[] = $export;
			}
		}
		$expenseCustomExportsWord = [];
		$expenseCustomExportsExcel = [];
		$expenseCustomExports = Custom::getExports(
			$this->environment()
				->getClient()
				->getKey(),
			'analysis/budget/project-expense'
		);
		foreach ($expenseCustomExports as $export) {
			if ($export->getType() === Export::TYPE_WORD) {
				$expenseCustomExportsWord[] = $export;
			} elseif ($export->getType() === Export::TYPE_EXCEL) {
				$expenseCustomExportsExcel[] = $export;
			}
		}

		$incomeCustomExportsWord = [];
		$incomeCustomExportsExcel = [];
		$incomeCustomExports = Custom::getExports(
			$this->environment()
				->getClient()
				->getKey(),
			'analysis/budget/project-income'
		);
		foreach ($incomeCustomExports as $export) {
			if ($export->getType() === Export::TYPE_WORD) {
				$incomeCustomExportsWord[] = $export;
			} elseif ($export->getType() === Export::TYPE_EXCEL) {
				$incomeCustomExportsExcel[] = $export;
			}
		}

		/** @var AgeSynchronisationServiceInterface */
		$ageSynchroService = $this->serviceLocator->get('AgeSynchronisationService');
		$ageSynchroService->initForProject($project);

		return new ViewModel([
			'project' => $project,
			'exports' => $exports,
			'listCustomExportsWord' => $listCustomExportsWord,
			'listCustomExportsExcel' => $listCustomExportsExcel,
			'customExportsWord' => $customExportsWord,
			'customExportsExcel' => $customExportsExcel,
			'expenseCustomExportsWord' => $expenseCustomExportsWord,
			'expenseCustomExportsExcel' => $expenseCustomExportsExcel,
			'incomeCustomExportsWord' => $incomeCustomExportsWord,
			'incomeCustomExportsExcel' => $incomeCustomExportsExcel,
			'budgetCode' => $this->moduleManager()->getClientModuleConfiguration('budget', 'code'),
			'ageSynchronisationActive' => $ageSynchroService->isSynchronisationActive(),
			'ageSynchroFlags' => AgeSynchronisation::getOperationFlags()
		]);
	}

	/**
	 * @return JsonModel
	 */
	public function duplicateAction()
	{
		$success = true;
		$errors = [];
		$newId = null;

		try {
			$id = $this->params()->fromRoute('id', false);
			$em = $this->entityManager();

			$project = $em->getRepository('Project\Entity\Project')->find($id);

			$newProject = clone $project;
			$newProject->setCreatedAt(new \DateTime());
			$newProject->setCreatedBy($this->authStorage()->getUser());

			$em->persist($newProject);
			$em->flush();

			/**
			 * Get all keywords of the project
			 */
			$associations = $em->getRepository('Keyword\Entity\Association')->findBy([
				'entity' => 'Project\Entity\Project',
				'primary' => $project->getId(),
			]);

			foreach ($associations as $association) {
				$newAssociation = clone $association;
				$newAssociation->setPrimary($newProject->getId());
				$em->persist($newAssociation);
			}

			/**
			 * Get all custom fields of the project
			 */
			$fieldValues = $em->getRepository('Field\Entity\Value')->findBy([
				'entity' => 'Project\Entity\Project',
				'primary' => $project->getId(),
			]);

			foreach ($fieldValues as $fieldValue) {
				$newFieldValue = clone $fieldValue;
				$newFieldValue->setPrimary($newProject->getId());
				$em->persist($newFieldValue);
			}

			/**
			 * Get all actors of the project
			 */
			foreach ($project->getActors() as $i => $actor) {
				/**
				 * Get all actors keywords of the project
				 */
				$associations = $em->getRepository('Keyword\Entity\Association')->findBy([
					'entity' => 'Project\Entity\Actor',
					'primary' => $actor->getId(),
				]);

				foreach ($associations as $association) {
					$newAssociation = clone $association;
					$newAssociation->setPrimary(
						$newProject
							->getActors()
							->get($i)
							->getId()
					);
					$em->persist($newAssociation);
				}
			}

			/**
			 * Get all territories of the project
			 */
			$territories = $em->getRepository('Map\Entity\Association')->findBy([
				'entity' => 'Project\Entity\Project',
				'primary' => $project->getId(),
			]);

			foreach ($territories as $territory) {
				$newTerritory = clone $territory;
				$newTerritory->setPrimary($newProject->getId());
				$em->persist($newTerritory);
			}

			$em->flush();

			$newMembers = [];
			foreach ($newProject->getMembers() as $member) {
				$newMembers[] = [
					'id' => $member->getId(),
					'role' => $member->getRole(),
					'user' => [
						'id' => $member->getUser()->getId(),
						'name' => $member->getUser()->getName(),
						'avatar' => $member->getUser()->getAvatar(),
					],
				];
			}

			$newId = $newProject->getId();
		} catch (\Exception $e) {
			$success = false;
			$errors[] = $e->getMessage();
		} finally {
			return new JsonModel([
				'success' => $success,
				'errors' => $errors,
				'project' => [
					'id' => $newId,
				],
			]);
		}
	}

	public function treeAction()
	{
		$export = $this->params()->fromPost('export', false);
		$customExport = $this->params()->fromPost('customExport', false);
		$filters = $this->params()->fromPost('filters', []);

		if ($export) {
			$filters = json_decode($filters, true);
		}

		if (!isset($filters['levels']) || !isset($filters['reference'])) {
			return new JsonModel([]);
		}

		if ($export && $export === 'project-form_expenses-summary') {
			$id = $filters['id'];

			Custom::doExport(
				$this->environment()->getClient(),
				$export,
				[],
				['id' => $id],
				$this->entityManager(),
				$this->serviceLocator->get('ViewHelperManager')->get('translate'),
				$this->serviceLocator->get('ControllerPluginManager')->get('apiRequest')
			);

			exit();
		}

		if ($export && ($export === 'analysis/budget/project-expense_project-expense' || $export === 'analysis/budget/project-income_project-income')) {
			$id = $filters['id'];
			Custom::doExport(
				$this->environment()->getClient(),
				$export,
				[],
				['id' => $id],
				$this->entityManager(),
				$this->serviceLocator->get('ViewHelperManager')->get('translate'),
				$this->serviceLocator->get('ControllerPluginManager')->get('apiRequest')
			);

			exit();
		}

		$levels = $filters['levels'];
		$reference = $filters['reference'];
		$treeReference = $filters['tree'];
		$sort = isset($filters['sort']) ? $filters['sort'] : 'code';

		$referenceLevel = null;
		$subLevels = [];
		$supLevels = [];

		foreach ($levels as $level) {
			$this->separateFiltersByEntity($level);

			if ($treeReference !== 'initial') {
				$level['clear'] = isset($level['clear']) ? $level['clear'] === 'true' : false;
			}

			if ($level['value'] == $reference) {
				$referenceLevel = $level;
			} elseif ($level['value'] > $reference) {
				$subLevels[] = $level;
			} elseif ($level['value'] < $reference) {
				$supLevels[] = $level;
			}
		}

		$keywords = null;
		// Si on est dans une arbo mot clef, on récupère tout les mot-clef triés par niveau
		if ($treeReference !== 'initial') {
			$keywords = $this->getKeywordsTree($treeReference);
			if (!$keywords) {
				return new JsonModel([]);
			}
		}

		$nodes = $this->getNodesReference($referenceLevel, $keywords, $treeReference, $sort);

		// If export excel groupByKewyords: sort projects by parent.code
		if ($export && is_numeric($export)) {
			/** @var Export $export */
			$export = $this->entityManager()
				->getRepository(Export::class)
				->find($export);

			if ($export->getType() === Export::TYPE_EXCEL) {
				foreach ($nodes as $node) {
					if (isset($node['__type'])) {
						$export->setReference(Export::EXPORT_REFERENCE_KEYWORD);
					}

					break;
				}
			}

			if (
				$export->getReference() === Export::EXPORT_REFERENCE_KEYWORD &&
				$export->getGroupByKeywords()
			) {
				$sort = 'parent.code';
				$nodes = $this->getNodesReference($referenceLevel, $keywords, $treeReference, $sort);
			}
		}

		if ($subLevels) {
			if ($treeReference == 'initial') {
				foreach ($nodes as $i => $project) {
					$nodes[$i]['children'] = array_values($this->getProjectsSub($project, $subLevels, $sort));
				}
			} else {
				$nodes = $this->getKeywordsSub($nodes, $subLevels, $keywords, $treeReference, $sort);
			}
		}

		if ($supLevels) {
			$currentNodes = $nodes;
			$currentNodesParentsIds = array_unique(
				array_map(function ($project) {
					return $project['parent']['id'];
				}, $currentNodes)
			);

			for ($i = count($supLevels) - 1; $i >= 0; $i--) {
				$level = $supLevels[$i];
				$supProjects = [];

				if ($treeReference === 'initial') {
					if ($level['mode'] === 'all') {
						$supProjects = $this->getNodesReference($level, $keywords, $treeReference, $sort);
						// Pour all + filter
						$_level = $level;
						unset($_level['filters']);
						$supParentsProjects = $this->getProjectsSup($_level, $currentNodesParentsIds, $sort);
						foreach ($supParentsProjects as $id => $parent) {
							if (!isset($supProjects[$id])) {
								$supProjects[$id] = $parent;
							}
						}
					} elseif ($level['mode'] === 'parent') {
						$supProjects = $this->getProjectsSup($level, $currentNodesParentsIds, $sort);
					} elseif ($level['mode'] === 'leaves') {
						$supProjects = $this->getProjectsSup($level, $currentNodesParentsIds, $sort, true);
					}
				} else {
					$supProjects = $this->getNodesReference($level, $keywords, $treeReference, $sort);
				}
                if (!empty($supProjects)) {
					foreach ($currentNodes as $project) {
						$parentId = $project['parent']['id'];
						// Si jamais le mot clef à été clear, on le réinsère
						if ($treeReference !== 'initial' && !isset($supProjects[$parentId])) {
							$supProjects[$parentId] = $keywords[$level['value']][$parentId];
						}
						$supProjects[$parentId]['children'][] = $project;
					}
					$currentNodes = $supProjects;
				} else {
					$currentNodes = $nodes;
				}

				if ($i > 0) {
					$currentNodesParentsIds = array_unique(
						array_map(function ($node) {
							return $node['parent']['id'];
						}, $currentNodes)
					);
				}
			}

			$nodes = $currentNodes;
		}

		if ($export) {
			if ($export !== 'xls' && $export !== 'default') {
				ini_set('memory_limit', -1);
				$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
				$translator = $helperPluginManager->get('translate');

				// Reset levels
				foreach ($nodes as &$parent) {
					unset($parent['parent']);
				}

				if (is_string($export) && preg_match_all('/[a-zA-Z]+/', $export)) {
					$customExport = json_decode($customExport, true);
					Custom::doExport(
						$this->environment()->getClient(),
						$export,
						$customExport,
						$nodes,
						$this->entityManager(),
						$translator,
						$this->serviceLocator->get('ControllerPluginManager')->get('apiRequest')
					);
				} else {
					/** @var Export $export */
					$export = $this->entityManager()
						->getRepository('Export\Entity\Export')
						->find($export);

					/** @var SimplifiedInputService */
					$simplifiedInputService = $this->serviceLocator->get('simplifiedInputService');

					switch ($export->getType()) {
						case Export::TYPE_WORD:
							$export->exportWord($nodes, $this->entityManager(), $translator, $simplifiedInputService);
							break;
						case Export::TYPE_EXCEL:
							$export->exportExcel($nodes, $this->entityManager(), $translator, $simplifiedInputService);
							break;
					}
				}

				exit();
			} else {
				$cols = json_decode($this->params()->fromPost('cols', '[]'), true);

				$exporter = $this->serviceLocator->get('ExcelExporter');
				$exporter->setEntityClass('Project\Entity\Project');
				$exporter->setExporter('Project\Export\Project');
				$exporter->setEntityManager($this->entityManager());
				$exporter->setCols($cols);

				$flat = [];
				$this->treeToFlat($nodes, $flat);
				$rows = [$exporter->buildArray()[0]];

				$min = 0;
				$max = 0;

				$arboKeywords = isset($flat[0]['levels']);
				foreach ($flat as $node) {
					if ($arboKeywords) {
						if (isset($node['levels'][0])) {
							$min = $min > $node['levels'][0] ? $node['levels'][0] : $min;
							$max = $max < $node['levels'][0] ? $node['levels'][0] + 1 : $max;
						}
					} else {
						$min = $min > $node['level'] ? $node['level'] : $min;
						$max = $max < $node['level'] ? $node['level'] : $max;
					}
				}
				for ($i = $min; $i <= $max; $i++) {
					array_unshift($rows[0], '');
				}

				$lastLevelKeyword = null;
				foreach ($flat as $node) {
					if (isset($node['__type']) && $node['__type'] == 'keyword') {
						$row = [$node['name']];
						$pap = $node['name'];
					} else {
						$object = $this->entityManager()
							->getRepository('Project\Entity\Project')
							->find($node['id']);
						$exporter->setRows([$object]);

						$row = $exporter->parsedRows()['rows'][1];
						$pap = $object->getName();
					}

					$n = 0;
					for ($i = $min; $i <= $max; $i++) {
						if ($arboKeywords) {
							if (isset($node['levels'])) {
								$lastLevelKeyword = $node['levels'][0];
								array_splice($row, $n, 0, $lastLevelKeyword == $i ? $pap : '');
							} else {
								array_splice($row, $n, 0, $lastLevelKeyword + 1 == $i ? $pap : '');
							}
						} else {
							array_splice($row, $n, 0, $node['level'] == $i ? $pap : '');
						}
						$n++;
					}
					$row[] = '';
					$rows[] = $row;
				}

				ExcelExporter::download($rows, [], function ($excel) use ($max, $min, $rows) {
					/**
					 * @var Spreadsheet $excel
					 */
					$sheet = $excel->getActiveSheet();
					foreach (range('A', Coordinate::stringFromColumnIndex($max - $min)) as $columnID) {
						$sheet
							->getColumnDimension($columnID)
							->setAutoSize(false)
							->setWidth(17);
						$sheet
							->getStyle($columnID . '1:' . $columnID . (sizeof($rows) + 1))
							->getAlignment()
							->setWrapText(false);
					}
				});
				exit();
			}
		}

		return new JsonModel(array_values($nodes));
	}

	public function exportAction()
	{
		$export_id = $this->params()->fromPost('export', false);
		
		$filters = $this->params()->fromPost('filters', []);
		$sort = $this->params()->fromPost('sort', 'name');
		$order = $this->params()->fromPost('order', 'asc');
		$full = $this->params()->fromPost('full', '');
		$itemsRemoved = $this->params()->fromPost('itemsRemoved', []);
		$itemsSelected = $this->params()->fromPost('itemsSelected', []);

		// custom budget export
		$keywordsFamily = json_decode($this->params()->fromPost('keywordsFamily', null), true);
		$expenseType = $this->params()->fromPost('expenseType', null);
		$incomeType = $this->params()->fromPost('incomeType', null);
		$financialYear = $this->params()->fromPost('financialYear', null);
		$nature = json_decode($this->params()->fromPost('nature', null), true);

		$filters = json_decode($filters, true);
		$itemsRemoved = json_decode($itemsRemoved, true);
		$itemsSelected = json_decode($itemsSelected, true);

		$projects = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
			'col' => ['id', 'code', 'name', 'level', 'parent.id','conventions.convention', 'keywords'],
			'search' => [
				'type' => 'list',
				'data' => [
					'sort' => $sort,
					'order' => $order,
					'filters' => $filters,
					'full' => $full,
				],
			],
		])
			->dispatch()
			->getVariable('rows');

		$projectsFinal = [];
		foreach ($projects as $project) {
			if (
				!in_array($project['id'], $itemsRemoved) &&
				($itemsSelected === [] xor in_array($project['id'], $itemsSelected))
			) {
				$projectsFinal[] = $project;
			}
		}

		ini_set('memory_limit', -1);

		$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
		$translator = $helperPluginManager->get('translate');

		/**
		 * Harcoded export (issue #228)
		 */
		if ('budget' === $export_id) {
			if (null === $keywordsFamily) {
				throw new \Exception($translator->__invoke('project_export_budget_no_keywords_family_error'), 400);
			}
			if (empty($expenseType)) {
				throw new \Exception($translator->__invoke('project_export_budget_no_expense_type_error'), 400);
			}
			if (empty($incomeType)) {
				throw new \Exception($translator->__invoke('project_export_budget_no_income_type_error'), 400);
			}
			$budgetExportService = $this->serviceLocator->get('BudgetCustomExportService');
			$budgetExportService->export($projects, $keywordsFamily, $expenseType, $incomeType, $financialYear, $nature);
			exit();
		}

		/** @var Export $export */
		$export = $this->entityManager()
			->getRepository('Export\Entity\Export')
			->find($export_id);

		if (!$export) {
			return new ViewModel();
		}

		$tree = Tree::toTree($projectsFinal);

		/** @var SimplifiedInputService */
		$simplifiedInputService = $this->serviceLocator->get('simplifiedInputService');
		switch ($export->getType()) {
			case Export::TYPE_WORD:
				$export->exportWord($tree, $this->entityManager(), $translator, $simplifiedInputService);
				break;
			case Export::TYPE_EXCEL:
				$export->exportExcel($tree, $this->entityManager(), $translator, $simplifiedInputService);
				break;
			default:
				$this->notFoundAction();
		}

		exit();
	}

	protected function separateFiltersByEntity(&$level)
	{
		if (!isset($level['filters'])) {
			return;
		}

		$filters = $level['filters'];
		$timesheetFilters = null;

		foreach ($filters as $property => $filter) {
			if (preg_match_all('/(.+)-.+/', $property, $matches)) {
				$entity = $matches[1][0];

				if ($entity === 'timesheet') {
					$realProperty = str_replace($entity . '-', '', $property);

					if (!$timesheetFilters) {
						$timesheetFilters = [];
					}

					$timesheetFilters[$realProperty] = $filter;
				}

				unset($filters[$property]);
			}
		}

		$level['filters'] = $filters;
		$level['timesheetFilters'] = $timesheetFilters;
	}

	protected function getNodesReference($level, $keywords, $tree, $sort)
	{
		$filters = isset($level['filters']) ? $level['filters'] : [];
        if (!$keywords) {
            $filters['level'] = [
                'op' => 'eq',
                'val' => $level['value'],
            ];
        } else {
            if (array_key_exists('keyword.' . $tree, $filters)) {
                $filters['keyword.' . $tree] = [
                    'op' => 'eq',
                    'val' => $filters['keyword.' . $tree]['val'],
                ];
            } else {
                $filters['keyword.' . $tree] = [
                    'op' => 'eq',
                    'val' => array_keys($keywords[$level['value']]),
                ];
            }
        }

		$nodes = $this->rowsToAssociative(
			$this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
				'col' => [
					'id',
					'code',
					'name',
					'level',
					'parent.id',
					$keywords ? 'keywords' : 'parent.id',
					'stackedAdvancement',
					'measuresT',
					'publicationStatus',
					'plannedDateStart',
					'plannedDateEnd',
					'realDateStart',
					'realDateEnd',
				],
				'search' => [
					'type' => 'list',
					'data' => [
						'sort' => $sort,
						'order' => 'asc',
						'filters' => $filters,
					],
				],
			])
				->dispatch()
				->getVariable('rows')
		);

		if (isset($level['timesheetFilters'])) {
			$timesheetFilters = $level['timesheetFilters'];

			foreach ($nodes as $id => $node) {
				$timesheetFilters['project'] = [
					'op' => 'eq',
					'val' => $id,
				];

				$timesheets = $this->apiRequest('Time\Controller\API\Timesheet', Request::METHOD_GET, [
					'col' => ['id'],
					'search' => [
						'type' => 'list',
						'data' => [
							'sort' => 'id',
							'order' => 'asc',
							'filters' => $timesheetFilters,
						],
					],
				])
					->dispatch()
					->getVariable('rows');

				if (!$timesheets) {
					unset($nodes[$id]);
				}
			}
		}

		// Si on est dans une arbo mot clef, on envoi les projets dans les mots clefs
		if ($tree !== 'initial') {
			foreach ($nodes as $project) {
				$project['__type'] = 'project';
				$projectKeywords = $project['keywords'][$tree];
				unset($project['keywords']);
				foreach ($projectKeywords as $keyword) {
					if (array_key_exists($keyword['id'], $keywords[$level['value']])) {
						$keywords[$level['value']][$keyword['id']]['children'][] = $project;
					}
				}
			}
			foreach ($keywords[$level['value']] as $key => $array) {
				if (isset($array['children']) && $array['children']) {
					$keywords[$level['value']][$key]['children'] = Tree::toTree($array['children']);
				}
			}

			$nodes = $keywords[$level['value']];

			if ($level['clear'] === true) {
				foreach ($nodes as $i => $node) {
					if (!isset($node['children']) || !$node['children']) {
						unset($nodes[$i]);
					}
				}
			}
		}

		return $nodes;
	}

	protected function getProjectsSub($project, $levels, $sort, $index = 0)
	{
		$level = $levels[$index];

		$filters = isset($level['filters']) ? $level['filters'] : [];
		$filters['level'] = [
			'op' => 'eq',
			'val' => $level['value'],
		];
		$filters['parent'] = [
			'op' => 'eq',
			'val' => $project['id'],
		];

		$projects = $this->rowsToAssociative(
			$this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
				'col' => [
					'id',
					'code',
					'name',
					'level',
					'parent.id',
					'stackedAdvancement',
					'publicationStatus',
					'measuresT',
					'plannedDateStart',
					'plannedDateEnd',
					'realDateStart',
					'realDateEnd',
				],
				'search' => [
					'type' => 'list',
					'data' => [
						'sort' => $sort,
						'order' => 'asc',
						'filters' => $filters,
					],
				],
			])
				->dispatch()
				->getVariable('rows')
		);

		if (isset($level['timesheetFilters'])) {
			$timesheetFilters = $level['timesheetFilters'];

			foreach ($projects as $id => $node) {
				$timesheetFilters['project'] = [
					'op' => 'eq',
					'val' => $id,
				];

				$timesheets = $this->apiRequest('Time\Controller\API\Timesheet', Request::METHOD_GET, [
					'col' => ['id'],
					'search' => [
						'type' => 'list',
						'data' => [
							'sort' => 'id',
							'order' => 'asc',
							'filters' => $timesheetFilters,
						],
					],
				])
					->dispatch()
					->getVariable('rows');

				if (!$timesheets) {
					unset($projects[$id]);
				}
			}
		}

		if (isset($levels[$index + 1])) {
			foreach ($projects as $i => $child) {
				$projects[$i]['children'] = array_values(
					$this->getProjectsSub($projects[$i], $levels, $sort, $index + 1)
				);
			}
		}

		return $projects;
	}

	protected function getKeywordsSub($keywordsResult, $levels, $keywords, $tree, $sort, $index = 0)
	{
		$level = $levels[$index];

		$subKeywords = $this->getNodesReference($level, $keywords, $tree, $sort);

		if (isset($levels[$index + 1])) {
			$subKeywords = $this->getKeywordsSub(
				$subKeywords,
				$levels,
				$keywords,
				$tree,
				$sort,
				$index + 1
			);
		}

		foreach ($subKeywords as $keyword) {
			foreach ($keyword['parents'] as $parent) {
				$id = $parent['id'];
				if (isset($keywords[$level['value'] - 1][$id])) {
					// Si jamais le mot clef à été clear, on le réinsère
					if (!isset($keywordsResult[$id])) {
						$keywordsResult[$id] = $keywords[$level['value'] - 1][$id];
					}
					$keywordsResult[$id]['children'][] = $keyword;
				}
			}
		}

		return $keywordsResult;
	}

	protected function getProjectsSup($level, $ids, $sort, $leaves = false)
	{
		$filters = isset($level['filters']) ? $level['filters'] : [];
		$filters['level'] = [
			'op' => 'eq',
			'val' => $level['value'],
		];

		$filters['id'] = [
			'op' => 'eq',
			'val' => $ids,
		];

		$parents = $this->rowsToAssociative(
			$this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
				'col' => [
					'id',
					'code',
					'name',
					'level',
					'parent.id',
					'stackedAdvancement',
					'publicationStatus',
					'measuresT',
					'plannedDateStart',
					'plannedDateEnd',
					'realDateStart',
					'realDateEnd',
				],
				'search' => [
					'type' => 'list',
					'data' => [
						'sort' => $sort,
						'order' => 'asc',
						'filters' => $filters,
					],
				],
			])
				->dispatch()
				->getVariable('rows')
		);

		if (isset($level['timesheetFilters'])) {
			$timesheetFilters = $level['timesheetFilters'];

			foreach ($parents as $id => $node) {
				$timesheetFilters['project'] = [
					'op' => 'eq',
					'val' => $id,
				];

				$timesheets = $this->apiRequest('Time\Controller\API\Timesheet', Request::METHOD_GET, [
					'col' => ['id'],
					'search' => [
						'type' => 'list',
						'data' => [
							'sort' => 'id',
							'order' => 'asc',
							'filters' => $timesheetFilters,
						],
					],
				])
					->dispatch()
					->getVariable('rows');

				if (!$timesheets) {
					unset($parents[$id]);
				}
			}
		}

		if ($leaves) {
			unset($filters['id']);
			$filters['children'] = [
				'op' => 'isempty',
				'val' => '',
			];

			$parents = $this->rowsToAssociative(
				array_merge(
					$this->rowsToAssociative(
						$this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
							'col' => [
								'id',
								'code',
								'name',
								'level',
								'parent.id',
								'stackedAdvancement',
								'publicationStatus',
								'measuresT',
							],
							'search' => [
								'type' => 'list',
								'data' => [
									'sort' => $sort,
									'order' => 'asc',
									'filters' => $filters,
								],
							],
						])
							->dispatch()
							->getVariable('rows')
					),
					$parents
				)
			);
		}

		return $parents;
	}

	protected function getKeywordsTree($group)
	{
		$keywords = $this->apiRequest('Keyword\Controller\API\Keyword', Request::METHOD_GET, [
			'col' => ['id', 'name', 'parents.id', 'levels'],
			'search' => [
				'type' => 'list',
				'data' => [
					'sort' => 'order',
					'order' => 'asc',
					'filters' => [
						'group' => [
							'op' => 'eq',
							'val' => $group,
						],
					],
				],
			],
		])
			->dispatch()
			->getVariable('rows');

		$tree = [];

		foreach ($keywords as $keyword) {
			$keyword['__type'] = 'keyword';
			foreach ($keyword['levels'] as $level) {
				$id = $keyword['id'];
				$tree[$level][$id] = $keyword;
			}
		}

		return $tree;
	}

	protected function rowsToAssociative($rows)
	{
		$associative = [];
		foreach ($rows as $row) {
			$associative[$row['id']] = $row;
		}

		return $associative;
	}

	protected function treeToFlat($nodes, &$flat)
	{
		foreach ($nodes as $node) {
			$flat[] = $node;
			if (isset($node['children'])) {
				$this->treeToFlat($node['children'], $flat);
			}
		}
	}

}
