<?php namespace Home\Controller\API;

use Core\Controller\BasicRestController;
use Home\Entity\Container;
use Home\Entity\ContainerCalendar;
use Home\Entity\ContainerChat;
use Home\Entity\ContainerGraphic;
use Home\Entity\ContainerList;
use Home\Entity\ContainerText;
use Laminas\View\Model\JsonModel;

class ContainerController extends BasicRestController
{
    protected $entityClass = 'Home\Entity\Container';
    protected $repository  = 'Home\Entity\Container';
    protected $entityChain = 'home:e:home';

    public function create($data)
    {
        if (isset($data['type'])) {
            if ($data['type'] === Container::TYPE_TEXT) {
                $this->entityClass = ContainerText::class;
            } else if ($data['type'] === Container::TYPE_LIST) {
                $this->entityClass = ContainerList::class;
            } else if ($data['type'] === Container::TYPE_CALENDAR) {
                $this->entityClass = ContainerCalendar::class;
            } else if ($data['type'] === Container::TYPE_CHAT) {
                $this->entityClass = ContainerChat::class;
            } else if ($data['type'] === Container::TYPE_GRAPHIC) {
                $this->entityClass = ContainerGraphic::class;
            }
        } else {
            return new JsonModel([
                'errors'  => [
                    'fields'     => [
                        'type' => [
                            'isEmpty' => 'Value is required and can\'t be empty',
                        ],
                    ],
                    'exceptions' => [],
                ],
                'success' => false,
            ]);
        }

        return parent::create($data);
    }
}
