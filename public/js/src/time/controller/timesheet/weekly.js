(function (app) {

    app.controller('timesheetWeeklyController', [
        'timesheetService', 'userService', 'queryService', 'projectService', 'bookmarkService', '$scope', 'flashMessenger', 'translator', 'orderByFilter',
        function timesheetWeeklyController(timesheetService, userService, queryService, projectService, bookmarkService, $scope, flashMessenger, translator, orderBy) {
            var self = this;

            self.timesheets      = [];
            self.dates           = [];
            self.rows            = [];
            self.totals          = {};
            self.historyTotals   = {};
            self.user            = null;
            self.projectActive   = false;
            self.checkboxAll     = false;
            self.currentBookmark = null;
            self.settingsTime    = {};
            self.currentRow      = null;
            self.reverse         = false;
            self.propertyName    = 'project';
            self.saving          = {
                bookmark  : false,
                timesheets: false,
            };
            self.types           = ['done', 'target', 'absence'];
            self.selectedQueryId = '';

            self.init = function init(user_id, projectActive) {
                self.currentDate   = moment().startOf('week');
                self.projectActive = typeof projectActive !== 'undefined' ? projectActive : false;

                bookmarkService.findAll(self.bookmarkApiParams).then(function (res) {
                    self.bookmarks = res.rows;
                });

                self.user = user_id;

                for (var i = 0; i < 7; ++i) {
                    var current                        = self.currentDate.startOf('week').add(i, 'day');
                    self.totals[current.format('ddd')] = 0;
                }
            };

            self.sortBy = function(propertyName) {
                self.reverse = (propertyName !== null && self.propertyName === propertyName)
                    ? !self.reverse : false;
                self.propertyName = propertyName;
                self.rows = orderBy(self.rows, self.propertyName, self.reverse);
            };

            self.selectQuery = function () {
                if (self.selectedQueryId !== '') {
                    queryService
                        .find(self.selectedQueryId, {col: ['filters']})
                        .then(function (res) {
                            if (res.object.filters) {
                                projectService
                                    .findAll({
                                        col   : ['id', 'code', 'name', 'parent.id'],
                                        search: {
                                            type: 'list',
                                            data: {
                                                sort   : 'name',
                                                order  : 'asc',
                                                filters: res.object.filters
                                            }
                                        }
                                    })
                                    .then(function (res) {
                                        self.rows = [];
                                        for (var i in res.rows) {
                                            self.addRow();
                                            self.rows[self.rows.length - 1].project = res.rows[i];
                                        }
                                    });
                            }
                        });
                }
            };

            self.getTimesheetsWeek = function (user_id, start, end) {
                self.loadingHistoric = true;
                timesheetService.findAll({
                    col   : ['id', 'name', 'type', 'absenceType.id', 'absenceType.name', 'hours', 'start', 'project.id', 'project.code', 'project.name'],
                    search: {
                        type: 'list', data: {
                            sort: 'name', order: 'asc', filters: {
                                'user.id': {
                                    op : 'eq',
                                    val: user_id
                                },
                                start    : {
                                    op : 'period',
                                    val: {
                                        start: start,
                                        end  : end
                                    }
                                }
                            }
                        }
                    }
                }).then(function (res) {
                    self.timesheetsProject   = [];
                    self.timesheetsWOProject = [];
                    self.historyTotals       = {};

                    for (var i in res.rows) {
                        var timesheet          = res.rows[i];
                        timesheet.dateFormated = moment(timesheet.start, 'DD/MM/YYYY HH:mm').format('DD/MM/YYYY');
                        timesheet.day          = moment(timesheet.start, 'DD/MM/YYYY HH:mm').format('ddd');

                        if (typeof self.historyTotals[timesheet.type] === 'undefined') {
                            self.historyTotals[timesheet.type] = {};
                            for (var i = 0; i < 7; ++i) {
                                var current                                               = self.currentDate.startOf('week').add(i, 'day');
                                self.historyTotals[timesheet.type][current.format('ddd')] = 0;
                            }
                            self.historyTotals[timesheet.type]['total'] = 0;
                        }

                        self.historyTotals[timesheet.type][timesheet.day] += timesheet.hours;
                        self.historyTotals[timesheet.type]['total'] += timesheet.hours;

                        if (typeof self.timesheetsWOProject[timesheet.type] === 'undefined') {
                            self.timesheetsWOProject[timesheet.type] = []
                        }
                        if (typeof self.timesheetsProject[timesheet.type] === 'undefined') {
                            self.timesheetsProject[timesheet.type] = []
                        }
                        if (typeof timesheet.project === 'undefined' || timesheet.project == null) {
                            self.timesheetsWOProject[timesheet.type].push({
                                id   : timesheet.id,
                                name : timesheet.name,
                                dates: [
                                    {
                                        day  : timesheet.dateFormated,
                                        hours: timesheet.hours
                                    }
                                ],
                                total: timesheet.hours
                            });
                        } else {
                            var index        = null;
                            var isNewProject = true;
                            for (var j in self.timesheetsProject[timesheet.type]) {
                                if (self.timesheetsProject[timesheet.type][j].id == timesheet.project.id) {
                                    isNewProject = false;
                                    index        = j;
                                }
                            }

                            if (isNewProject) {
                                self.timesheetsProject[timesheet.type].push({
                                    id   : timesheet.project.id,
                                    name : timesheet.project.name,
                                    dates: [],
                                    total: 0
                                });
                                index = self.timesheetsProject[timesheet.type].length - 1;
                            }

                            var isNewDate = true;
                            for (var j in self.timesheetsProject[timesheet.type][index].dates) {
                                var date = self.timesheetsProject[timesheet.type][index].dates[j];

                                if (date.day == timesheet.dateFormated) {
                                    self.timesheetsProject[timesheet.type][index].total += timesheet.hours;
                                    date.hours += timesheet.hours;
                                    isNewDate = false;
                                }
                            }

                            if (isNewDate) {
                                self.timesheetsProject[timesheet.type][index].dates.push({
                                    day  : timesheet.dateFormated,
                                    hours: timesheet.hours
                                });
                                self.timesheetsProject[timesheet.type][index].total += timesheet.hours;
                            }
                        }
                    }
                    self.loadingHistoric = false;
                }, function () {});
            };

            /**
             * Recalcule le tableau de la semaine.
             */
            self.recalculateWeek = function recalculateWeek() {
                self.dates = [];
                for (var i = 0; i < 7; ++i) {
                    var current = self.currentDate.startOf('week').add(i, 'day');
                    self.dates.push({
                        day     : current.format('ddd'),
                        formated: current.format('DD/MM/YYYY'),
                        date    : current.clone()
                    });
                }

                var firstDay = moment(self.dates[0].date).format('DD/MM/YYYY');
                var lastDay  = moment(self.dates[self.dates.length - 1].date).format('DD/MM/YYYY');

                self.getTimesheetsWeek(self.user.id || self.user, firstDay, lastDay);
            };

            /**
             * Save all timesheets
             */
            self.save = function save() {
                var rows  = [],
                    error = false;
                for (var i = 0; i < self.rows.length; ++i) {
                    var row = self.rows[i];
                    delete row.errors;
                    var splitRow = self.splitIntoTimesheets(row);
                    for (var j = 0; j < splitRow.length; ++j) {
                        if (typeof splitRow[j].errors !== 'undefined') {
                            error = true;
                        }
            
                        rows.push(splitRow[j]);
                    }
                }

                if (!error && rows.length > 0) {
                    self.saving.timesheets = true;
                    timesheetService.saveAll(rows).then(function (res) {
                        var resError = false;
                        for (var i = 0; i < res.length; ++i) {
                            var row = res[i];
                            if (!row.success) {
                                resError = true;
                            }
                        }

                        if (!resError) {
                            self.rows = [];
                            self.selectedQueryId = '';
                            self.recalculateWeek();
                            flashMessenger.success(translator('timesheet_all_created_success'));
                        }

                        self.saving.timesheets = false;
                    });
                }

                self.recalculateWeek();
            };

            /**
             * Scinde la ligne en feuilles de temps.
             * @param row
             * @returns {Array}
             */
            self.splitIntoTimesheets = function splitIntoTimesheets(row) {
                var timesheets = [];

                for (var i = 0; i < self.dates.length; ++i) {
                    var date = self.dates[i];
                    if (typeof row[date.day] === 'string' || typeof row[date.day] === 'number') {
                        var hours = parseFloat(row[date.day]);
                        if (self.settingsTime.hours) {
                            hours *= self.settingsTime.hours;
                        }

                        var start = date.date.hours(8).minutes(0);
                        var end   = angular.copy(start).add(hours, 'hours');

                        var timesheet = {
                            'name'       : row.name,
                            'start'      : start.format('DD/MM/YYYY HH:mm'),
                            'end'        : end.format('DD/MM/YYYY HH:mm'),
                            'hours'      : hours,
                            'user'       : self.user,
                            'type'       : row.type,
                            'absenceType': row.absenceType
                        };

                        if (self.projectActive) {
                            if ((angular.isObject(row.project) && typeof row.project.id === 'undefined') || row.project == null) {
                                row.errors       = {
                                    fields: {
                                        project: ['Can\'t be empty']
                                    }
                                };
                                timesheet.errors = row.errors;
                            }
                            timesheet.project = row.project;
                        }

                        if (typeof row.keywords !== 'undefined') {
                            timesheet.keywords = row.keywords;
                        }

                        if (typeof row.territories !== 'undefined') {
                            timesheet.territories = row.territories;
                        }

                        if (timesheet.hours == 0 || isNaN(timesheet.hours)) {
                            delete timesheet;
                        } else {
                            timesheets.push(timesheet);
                        }

                    }
                }

                return timesheets;
            };

            /**
             * Modifie la semaine actuelle
             * @param x
             */
            self.addWeek = function addWeek(x) {
                self.currentDate.add(x, 'week');
                self.recalculateWeek();
            };

            /**
             * Ajoute une ligne au tableau
             */
            self.addRow = function addRow() {
                var row = {
                    checked: false,
                    name   : null,
                    type   : 'done'
                };
                if (self.projectActive) {
                    row.project = {};
                }
                self.rows.push(row);
                self.checkboxAll = false;
            };

            /**
             * Delete une ligne du tableau
             * @param row
             */
            self.deleteRow = function deleteRow(row) {
                var index = self.rows.indexOf(row);
                if (index > -1) {
                    self.rows.splice(index, 1);
                }
                self.check();
            };

            /**
             * Check all checkboxes
             */
            self.checkAll = function checkAll() {
                var isChecked = self.checkboxAll;

                for (var i = 0; i < self.rows.length; ++i) {
                    self.rows[i].checked = isChecked;
                }
            };

            /**
             * Check a checkbox
             */
            self.check = function check() {
                var count = self.rows.length;
                for (var i = self.rows.length - 1; i >= 0; --i) {
                    if (self.rows[i].checked) {
                        --count;
                    }
                }

                self.checkboxAll = count == 0;
            };

            $scope.$watch(function () { return self.rows; }, function (newVal) {
                for (var i in self.totals) {
                    self.totals[i] = 0;
                }
                self.total = 0;

                for (var i = 0; i < newVal.length; ++i) {
                    var row       = newVal[i];
                    var totalLine = 0;
                    for (var day in self.totals) {
                        if (typeof row[day] !== 'undefined' && !isNaN(parseFloat(row[day]))) {
                            totalLine += parseFloat(row[day]);
                            self.totals[day] += parseFloat(row[day]);
//                            self.totals['total'] += parseFloat(row[day]);
                        }
                    }
                    row['total'] = totalLine;
                    self.total += totalLine;
                }
            }, true);

            $scope.$watch(function () { return self.user; }, function () {
                if (self.user) {
                    self.recalculateWeek();
                }
            }, true);

            /**
             * Bookmark part
             */
            self.bookmark = {};
            self.bookmarks         = [];
            self.bookmarkApiParams = {
                col   : [
                    'id',
                    'user.id',
                    'user.name',
                    'name',
                    'config'
                ],
                search: {
                    type: 'list',
                    data: {
                        sort : '',
                        order: '',
                    }
                }
            };

            /**
             * Enregistre la configuration personnalisée
             */
            self.saveBookmark = function saveBookmark() {
                var config = [];

                for (var i = 0; i < self.rows.length; ++i) {
                    var row      = self.rows[i],
                        savedRow = {
                            'name': row.name,
                            'type': row.type,
                            'absenceType': row.absenceType,
                            'days': []
                        };
                    if (self.projectActive) {
                        savedRow.project = row.project;
                        if (typeof row.project.id === 'number') {
                            savedRow.project = row.project;
                        }
                    }

                    savedRow.keywords    = row.keywords;
                    savedRow.territories = row.territories;

                    for (var j = 0; j < self.dates.length; ++j) {
                        var date  = self.dates[j],
                            value = null;

                        if (typeof row[date.day] !== 'undefined') {
                            value = row[date.day];
                        }

                        savedRow.days.push(value);
                    }

                    config.push(savedRow);
                }

                self.bookmark.config = config;
                self.bookmark.user   = self.user;

                self.saving.bookmark = true;
                bookmarkService.save(self.bookmark, self.bookmarkApiParams).then(function (res) {
                    self.bookmark = {};
                    $('#modalBookmark').modal('hide');
                    flashMessenger.success(translator('timesheet_bookmark_created_success'));
                    self.saving.bookmark = false;
                    self.rows            = [];
                });
            };

            /**
             * Load a bookmark onto the table.
             * @param bookmark
             */
            self.loadBookmark = function loadBookmark(bookmark) {
                self.rows = [];
                for (var i = 0; i < bookmark.config.length; ++i) {
                    var config = bookmark.config[i],
                        row    = {
                            'name'       : config.name,
                            'project'    : config.project,
                            'keywords'   : config.keywords,
                            'territories': config.territories,
                            'type'       : config.type || 'done',
                            'absenceType'       : config.absenceType,
                        };
                        for (var j = 0; j < config.days.length; ++j) {
                        var date = self.currentDate.clone();
                        date.day(j + 1);
                        row[date.format('ddd')] = config.days[j];
                    }

                    self.rows.push(row);
                }

                self.compileKeywords();
            };

            /**
             * Delete a bookmark from the list.
             * @param bookmark
             */
            self.deleteBookmark = function deleteBookmark(bookmark) {
                if (confirm(translator('timesheet_bookmark_confirm_delete'))) {
                    bookmarkService.remove(bookmark).then(function (res) {
                        var index = 0;
                        for (var i = 0; i < self.bookmarks.length; ++i) {
                            if (self.bookmarks[i].id == bookmark.id) {
                                index = i;
                                break;
                            }
                        }
                        self.bookmarks.splice(index, 1);
                        flashMessenger.success(translator('timesheet_bookmark_delete_success'));
                    });
                }
            };

            /**
             * Keywords
             */
            self.keywordGroups = {
                _rights: {
                    'update': true
                }
            };

            /**
             * Ajoute les keywords aux rows séléctionnées.
             */
            self.addKeywords = function addKeywords() {
                var keywords = self.getKeywords();
                if (self.currentRow){
                    var row = self.currentRow;
                    if (typeof row.keywords === 'undefined') {
                        row.keywords = {};
                    }
                    for (var g in keywords) {
                        for (var k = 0; k < keywords[g].length; ++k) {
                            var exists = -1;
                            if (typeof row.keywords[g] !== 'undefined') {
                                exists = self.getKeywordIndexOf(row.keywords[g], keywords[g][k]);
                            } else {
                                row.keywords[g] = [];

                            }
                            if (exists == -1) {
                                row.keywords[g].push(keywords[g][k]);
                            }
                        }
                    }
                    self.currentRow = null;
                }
                else {
                    for (var i = 0; i < self.rows.length; ++i) {
                        var row = self.rows[i];
                        if (row.checked) {
                            if (typeof row.keywords === 'undefined') {
                                row.keywords = {};
                            }

                            for (var g in keywords) {
                                for (var k = 0; k < keywords[g].length; ++k) {
                                    var exists = -1;
                                    if (typeof row.keywords[g] !== 'undefined') {
                                        exists = self.getKeywordIndexOf(row.keywords[g], keywords[g][k]);
                                    } else {
                                        row.keywords[g] = [];
                                    }

                                    if (exists == -1) {
                                        row.keywords[g].push(keywords[g][k]);
                                    }
                                }
                            }
                        }
                    }
                }

                self.cleanKeywords();
            };

            /**
             * Remplace les keywords dans les rows sélectionnées.
             */
            self.replaceKeywords = function replaceKeywords() {
                var keywords = self.getKeywords();
                if (self.currentRow){
                    var row = self.currentRow;
                    row.keywords = keywords;
                    self.currentRow = null;
                }
                else {
                    for (var i = 0; i < self.rows.length; ++i) {
                        var row = self.rows[i];
                        if (row.checked) {
                            row.keywords = keywords;
                        }
                    }    
                }

                self.cleanKeywords();
            };

            /**
             * Retire les keywords des rows sélectionnées.
             */
            self.removeKeywords = function removeKeywords() {
                var keywords = self.getKeywords();
                if (self.currentRow){
                    var row = self.currentRow;
                    if (typeof row.keywords === 'undefined') {
                        row.keywords = {};
                    }

                    for (var g in keywords) {
                        for (var k = 0; k < keywords[g].length; ++k) {
                            var exists = -1;
                            if (typeof row.keywords[g] !== 'undefined') {
                                exists = self.getKeywordIndexOf(row.keywords[g], keywords[g][k]);
                            }

                            if (exists > -1) {
                                row.keywords[g].splice(exists, 1);
                            }
                        }
                    }
                    self.currentRow = null;
                }
                else {
                    for (var i = 0; i < self.rows.length; ++i) {
                        var row = self.rows[i];
                        if (row.checked) {
                            if (typeof row.keywords === 'undefined') {
                                row.keywords = {};
                            }

                            for (var g in keywords) {
                                for (var k = 0; k < keywords[g].length; ++k) {
                                    var exists = -1;
                                    if (typeof row.keywords[g] !== 'undefined') {
                                        exists = self.getKeywordIndexOf(row.keywords[g], keywords[g][k]);
                                    }

                                    if (exists > -1) {
                                        row.keywords[g].splice(exists, 1);
                                    }
                                }
                            }
                        }
                    }
                }

                self.cleanKeywords();
            };

            /**
             * Récupère les keywords par groupes
             * @returns {{}}
             */
            self.getKeywords = function getKeywords() {
                var keywords = {};
                if (typeof self.keywordGroups.keywords !== 'undefined') {
                    for (var i in self.keywordGroups.keywords) {
                        var group = self.keywordGroups.keywords[i];

                        if (typeof keywords[i] === 'undefined') {
                            keywords[i] = [];
                        }

                        if (angular.isArray(group)) {
                            keywords[i] = group;
                        } else {
                            keywords[i] = [group];
                        }
                    }
                }
                return keywords;
            };

            /**
             * Récupère l'index d'un keyword spécifique dans
             * la liste des keywords passés.
             *
             * @param row La liste des keywords
             * @param keyword Le keyword à chercher
             * @returns {number}
             */
            self.getKeywordIndexOf = function (row, keyword) {
                for (var j = 0; j < row.length; ++j) {
                    if (keyword.id == row[j].id) {
                        return j;
                    }
                }
                return -1;
            };

            /**
             * Vide les keywords actuels.
             */
            self.cleanKeywords = function cleanKeywords() {
                self.compileKeywords();
                delete self.keywordGroups['keywords'];
            };

            /**
             * Prends tous les keywords et les rassemble dans
             * un tableau pour la vue uniquement.
             */
            self.compileKeywords = function compileKeywords() {
                for (var i = 0; i < self.rows.length; ++i) {
                    var row          = self.rows[i];
                    row.keywordsView = [];

                    if (typeof row.keywords !== 'undefined') {
                        for (var g in row.keywords) {
                            for (var k = 0; k < row.keywords[g].length; ++k) {
                                row.keywordsView.push(row.keywords[g][k]);
                            }
                        }
                    }
                }
            };

            /**
             * Territories
             */
            self.territories = [];

            /**
             * Ajoute les territoires aux rows séléctionnées.
             */
            self.addTerritories = function addTerritories() {
                if (self.currentRow){
                    var row = self.currentRow;
                    if (typeof row.territories === 'undefined') {
                        row.territories = [];
                    }

                    for (var g in self.territories) {
                        var territory = self.territories[g];
                        var index     = self.getTerritoryIndexOf(row.territories, territory);
                        if (index === -1) {
                            row.territories.push(territory);
                        }
                    }
                    self.currentRow = null;
                }
                else {
                    for (var i = 0; i < self.rows.length; ++i) {
                        var row = self.rows[i];
                        if (row.checked) {
                            if (typeof row.territories === 'undefined') {
                                row.territories = [];
                            }

                            for (var g in self.territories) {
                                var territory = self.territories[g];
                                var index     = self.getTerritoryIndexOf(row.territories, territory);
                                if (index === -1) {
                                    row.territories.push(territory);
                                }
                            }
                        }
                    }
                }

                self.cleanTerritories();
            };

            /**
             * Remplace les territoires dans les rows sélectionnées.
             */
            self.replaceTerritories = function replaceTerritories() {
                if (self.currentRow){
                    var row = self.currentRow;
                    if (typeof row.territories === 'undefined') {
                        row.territories = [];
                    }

                    row.territories = self.territories;
                    self.currentRow = null;
                }
                else {
                    for (var i = 0; i < self.rows.length; ++i) {
                        var row = self.rows[i];
                        if (row.checked) {
                            if (typeof row.territories === 'undefined') {
                                row.territories = [];
                            }

                            row.territories = self.territories;
                        }
                    }
                }

                self.cleanTerritories();
            };

            /**
             * Retire les territoires des rows sélectionnées.
             */
            self.removeTerritories = function removeTerritories() {
                if (self.currentRow){
                    var row = self.currentRow;
                    if (typeof row.territories === 'undefined') {
                        row.territories = [];
                    }

                    for (var g in self.territories) {
                        var territory = self.territories[g];
                        var index     = self.getTerritoryIndexOf(row.territories, territory);
                        if (index !== -1) {
                            row.territories.splice(index, 1);
                        }
                    }
                    self.currentRow = null;
                }
                else {
                    for (var i = 0; i < self.rows.length; ++i) {
                        var row = self.rows[i];
                        if (row.checked) {
                            if (typeof row.territories === 'undefined') {
                                row.territories = [];
                            }

                            for (var g in self.territories) {
                                var territory = self.territories[g];
                                var index     = self.getTerritoryIndexOf(row.territories, territory);
                                if (index !== -1) {
                                    row.territories.splice(index, 1);
                                }
                            }
                        }
                    }
                }

                self.cleanTerritories();
            };


            /**
             * Vide les territories actuels.
             */
            self.cleanTerritories = function cleanTerritories() {
                self.territories = [];
            };

            self.getTerritoryIndexOf = function (row, territory) {
                for (var j = 0; j < row.length; ++j) {
                    if (territory.id == row[j].id) {
                        return j;
                    }
                }
                return -1;
            };
        }
    ])

})(window.EVA.app);
