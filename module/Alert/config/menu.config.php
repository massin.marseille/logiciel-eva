<?php

$alerts = [
    'icon'     => 'alert',
    'title'    => 'alert_module_title',
    'right'    => 'alert:e:alert:r',
    'href'     => 'alert',
    'priority' => 2
];

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'alerts' => $alerts,
                ],
            ],
        ],
        'client-menu-parc' => [
            'suivi' => [
                'children' => [
                    'alerts' => $alerts,
                ],
            ],
        ],
    ],
];
