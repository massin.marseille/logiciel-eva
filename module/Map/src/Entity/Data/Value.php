<?php

namespace Map\Entity\Data;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Map\Entity\Territory;
use Laminas\InputFilter\Factory;

/**
 * Class Value
 *
 * @package Map\Entity\Data
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="map_data_value")
 */
class Value extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Territory
     *
     * @ORM\ManyToOne(targetEntity="Map\Entity\Territory")
     */
    protected $territory;

    /**
     * @var Definition
     *
     * @ORM\ManyToOne(targetEntity="Map\Entity\Data\Definition")
     */
    protected $definition;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $value;


    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'definition',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $definition = $entityManager->getRepository('Map\Entity\Definition')->find($value);

                                return $definition !== null;
                            },
                            'message' => 'mapDataValue_field_definition_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'territory',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $territory = $entityManager->getRepository('Map\Entity\Territory')->find($value);

                                return $territory !== null;
                            },
                            'message' => 'mapDataValue_field_territory_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'value',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
        ]);

        return $inputFilter;
    }
}
