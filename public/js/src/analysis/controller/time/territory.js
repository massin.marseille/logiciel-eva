(function (app) {

    app.controller('territoryController', [
        '$timeout', '$scope', '$http', 'treeBuilderService', 'territoryService', 'translator',
        function territoryController($timeout, $scope, $http, treeBuilderService, territoryService, translator) {
            var self         = this;
            var first        = true;
            var shouldReload = false;

            self.territories = [];
            self.datas       = [];

            $scope.chart = {
                territories: []
            };

            $scope.$on('switchTab', function (event, data) {
                if (data.active === 'territory' && shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                }
            });

            $scope.$on('applyFilters', function (event, data) {
                if (data.active === 'territory' && data.shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                } else if (data.shouldReload) {
                    shouldReload = true;
                }
            });

            self.applyFilters = function (data) {
                self.loadingOn();
                $http({
                    url   : '/analysis/time/territories?' + $.param({filters: data.filters}),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;

                    self.datas.res.territories = treeBuilderService.toNNTree(self.datas.res.territories);

                    if ($scope.chart.territories !== null) {
                        self.updateChart();
                    }

                    if (first) {
                        territoryService.findAll({
                            col   : ['id', 'name', 'parents.id'],
                            search: {
                                type: 'list',
                                data: {
                                    sort   : 'name',
                                    order  : 'asc',
                                    filters: {
                                        parents: {
                                            op : 'isEmpty',
                                            val: ''
                                        }
                                    }
                                }
                            }
                        }).then(function (res) {
                            self.territories = res.rows;

                            if (self.datas.res.territories.length > 0) {
                                self.loadTerritories(self.territories);
                            }
                        }, function () {});
                    }

                    first = false;
                }, function () {});
            };

            self.loadTerritories = function (territory) {
                $scope.chart.territories = [];
                self.loadChart(territory, null, 0);
                self.updateChart();
            };

            self.loadChart = function (territory, parent, level) {
                $scope.chart.territories.splice(level);
                $scope.chart.territories.push({
                    api      : {},
                    territory: territory,
                    parent   : parent,
                    level    : level,
                    options  : {
                        chart: {
                            type                   : 'pieChart',
                            showControls           : false,
                            showValues             : true,
                            stacked                : true,
                            height                 : 400,
                            useInteractiveGuideline: true,
                            labelsOutside          : true,
                            x                      : function (d) {
                                return d.label;
                            },
                            y                      : function (d) {
                                return d.value;
                            },
                            yAxis                  : {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                }
                            },
                            tooltip                : {
                                valueFormatter: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                }
                            },
                            callback               : function (chart) {
                                chart.pie.dispatch.on('elementClick', function (e) {
                                    $(e.element).parents('svg').find('.slice-selected').removeClass('slice-selected');
                                    $scope.$apply(function () {
                                        if (e.data.territory.children.length > 0) {
                                            $(e.element).find('path').addClass('slice-selected');
                                            self.loadChart(territory, e.data.territory, level + 1);
                                            self.updateChart();
                                        } else {
                                            $scope.chart.territories.splice(level + 1);
                                        }
                                    });
                                });
                            }
                        }
                    },
                    data     : []
                });
            };

            self.updateChart = function () {
                for (var i in $scope.chart.territories) {
                    var chart  = $scope.chart.territories[i];
                    chart.data = [];
                    chart.totalDone = 0;
                    chart.totalTarget = 0;
                    if (chart.parent == null) {
                        for (var i in self.datas.res.territories) {
                            var territory = self.datas.res.territories[i];
                            // Cas de tous les territoires parents affiches
                            if (chart.territory.constructor === Array) {
                                for (var i in chart.territory) {
                                    if (territory.id === chart.territory[i].id) {
                                        chart.data.push({
                                            label    : territory.name,
                                            value    : territory.done,
                                            done     : territory.done,
                                            target   : territory.target,
                                            territory: territory
                                        });
                                        chart.totalDone += territory.done;
                                        chart.totalTarget += territory.target;
                                    }
                                }
                                chart.territory.name = translator('all');
                            } else {
                                if (territory.id === chart.territory.id) {
                                    chart.data.push({
                                        label    : territory.name,
                                        value    : territory.done,
                                        done     : territory.done,
                                        target   : territory.target,
                                        territory: territory
                                    });
                                    chart.totalDone += territory.done;
                                    chart.totalTarget += territory.target;
                                }
                            }
                        }
                    } else {
                        for (var i in chart.parent.children) {
                            var territory = chart.parent.children[i];
                            chart.data.push({
                                label    : territory.name,
                                value    : territory.done,
                                done     : territory.done,
                                target   : territory.target,
                                territory: territory
                            });
                            chart.totalDone += territory.done;
                            chart.totalTarget += territory.target;
                        }
                    }
                }
                for (var i in $scope.chart.territories) {
                    for (var j in $scope.chart.territories[i].data) {
                        $scope.chart.territories[i].data[j].percentDone = $scope.chart.territories[i].data[j].done / $scope.chart.territories[i].totalDone * 100 || 0;
                        $scope.chart.territories[i].data[j].percentTarget = $scope.chart.territories[i].data[j].target / $scope.chart.territories[i].totalTarget * 100 || 0;
                    }
                }
                $timeout(function () {
                    chart.api.refresh();
                    self.loadingOff();
                });
            };

            self.export = function (type, parent, group) {
                $scope.$emit('export', {type: type, parent: parent, group: group});
            };

            self.loadingOn = function () {
                self.loading = true;
                $scope.$emit('loadingOn');
            };

            self.loadingOff = function () {
                self.loading = false;
                $scope.$emit('loadingOff');
            };
        }
    ]);

})(window.EVA.app);
