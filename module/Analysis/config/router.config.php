<?php

namespace Analysis;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'analysis' => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/analysis',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'budget'     => [
                        'type'          => 'Segment',
                        'options'       => [
                            'route'    => '/budget',
                            'defaults' => [
                                'controller' => 'Analysis\Controller\Budget',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'expense'                => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/expense',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Budget',
                                        'action'     => 'expense',
                                    ],
                                ],
                            ],
                            'subvention'             => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/subvention',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Budget',
                                        'action'     => 'subvention',
                                    ],
                                ],
                            ],
                            'income'                 => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/income',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Budget',
                                        'action'     => 'income',
                                    ],
                                ],
                            ],
                            'gbcp'               => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/gbcp',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Budget',
                                        'action'     => 'gbcp',
                                    ],
                                ],
                            ],
                            'financer'               => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/financer',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Budget',
                                        'action'     => 'financer',
                                    ],
                                ],
                            ],
                            'keywords'               => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/keywords',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Budget',
                                        'action'     => 'keywords',
                                    ],
                                ],
                            ],
                            'year' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/year',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Budget',
                                        'action'     => 'year',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'indicator'  => [
                        'type'          => 'Segment',
                        'options'       => [
                            'route'    => '/indicator',
                            'defaults' => [
                                'controller' => 'Analysis\Controller\Indicator',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'recap'             => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/recap',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Indicator',
                                        'action'     => 'recap',
                                    ],
                                ],
                            ],
                            'recapWithMeasures' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/recap-with-measures',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Indicator',
                                        'action'     => 'recapWithMeasures',
                                    ],
                                ],
                            ],
                            'recapByClient' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/recap-by-client',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Indicator',
                                        'action'     => 'recapByClient',
                                    ],
                                ],
                            ],
                            'getClients' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/get-clients',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Indicator',
                                        'action'     => 'getClients',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'time'       => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/time',
                            'defaults' => [
                                'controller' => 'Analysis\Controller\Time',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'timesheets' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/timesheets',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'timesheets',
                                    ],
                                ],
                            ],
                            'projects' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/projects',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'projects',
                                    ],
                                ],
                            ],
                            'projectKeywords' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/projectKeywords',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'projectKeywords',
                                    ],
                                ],
                            ],
                            'users' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/users',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'users',
                                    ],
                                ],
                            ],
                            'months' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/months',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'months',
                                    ],
                                ],
                            ],
                            'userMonths' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/userMonths',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'userMonths',
                                    ],
                                ],
                            ], 
                            'keywords' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/keywords',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'keywords',
                                    ],
                                ],
                            ],
                            'territories' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/territories',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Time',
                                        'action'     => 'territories',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'convention' => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/convention',
                            'defaults' => [
                                'controller' => 'Analysis\Controller\Convention',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'conventions' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/conventions',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Convention',
                                        'action'     => 'conventions',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'project'    => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/project',
                            'defaults' => [
                                'controller' => 'Analysis\Controller\Project',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'projects' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/projects',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Project',
                                        'action'     => 'projects',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'map'        => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/map',
                            'defaults' => [
                                'controller' => 'Analysis\Controller\Map',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'territories' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/territories',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Map',
                                        'action'     => 'territories',
                                    ],
                                ],
                            ],
                            'data'        => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/data',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Map',
                                        'action'     => 'data',
                                    ],
                                ],
                            ],
                            'export'      => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/export',
                                    'defaults' => [
                                        'controller' => 'Analysis\Controller\Map',
                                        'action'     => 'export',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\BudgetController::class     => ServiceLocatorFactory::class,
            Controller\TimeController::class       => ServiceLocatorFactory::class,
            Controller\IndicatorController::class  => ServiceLocatorFactory::class,
            Controller\ConventionController::class => ServiceLocatorFactory::class,
            Controller\ProjectController::class    => ServiceLocatorFactory::class,
            Controller\MapController::class        => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Analysis\Controller\Budget'     => Controller\BudgetController::class,
            'Analysis\Controller\Time'       => Controller\TimeController::class,
            'Analysis\Controller\Indicator'  => Controller\IndicatorController::class,
            'Analysis\Controller\Convention' => Controller\ConventionController::class,
            'Analysis\Controller\Project'    => Controller\ProjectController::class,
            'Analysis\Controller\Map'        => Controller\MapController::class,
        ],
    ],
];
