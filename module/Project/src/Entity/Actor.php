<?php

namespace Project\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Directory\Entity\Structure;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Actor
 *
 * @package Project\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="project_actor")
 */
class Actor extends BasicRestEntityAbstract
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Structure
     * @ORM\ManyToOne(targetEntity="Directory\Entity\Structure")
     */
    protected $structure;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="actors")
     */
    protected $project;

    /**
     * Clone function
     */
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param Structure $structure
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project = null)
    {
        $this->project = $project;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'structure',
                'required' => true,
            ],
            [
                'name'     => 'project',
                'required' => false,
                'filters'  => [
                    ['name' => 'Core\Filter\IdFilter'],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $project = $entityManager->getRepository('Project\Entity\Project')->find($value);

                                return $project !== null;
                            },
                            'message' => 'actor_field_project_error_non_exists'
                        ],
                    ]
                ],
            ],
        ]);

        return $inputFilter;
    }

	/**
	 * @param User $user
	 * @param string $crudAction
	 * @param null   $owner
	 * @param null   $entityManager
	 * @return bool
	 */
	public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
	{
		return $this->project->ownerControl($user, $crudAction, $owner, $entityManager);
	}
}
