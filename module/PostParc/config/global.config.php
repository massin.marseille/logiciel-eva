<?php

use PostParc\Command\SyncParc;
use PostParc\Command\SyncParcFactory;

/**
 * Inject configuations on global scope.
 */
return [
    'service_manager' => [
        'factories' => [
            SyncParc::class => SyncParcFactory::class,
        ],
    ],
    'laminas-cli'     => [
        'commands' => [
            'post-parc:sync-post-parc' => SyncParc::class,
        ],
    ],
];
