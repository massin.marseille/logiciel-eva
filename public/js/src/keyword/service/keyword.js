(function(app) {

    app.factory('keywordService', [
        'restFactoryService', '$http', '$q',
        function keywordService(restFactoryService, $http, $q) {
            var url = '/api/keyword';

            var methods = restFactoryService.create(url);

            methods.fusion = function (keywordToKeep_id, keywordToFusion_id, type) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: url + '/' + keywordToKeep_id + '/fusion',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ keywordToFusion_id : keywordToFusion_id, type: type })
                }).then(function(res) {
                    deferred.resolve(res.data)
                }, function(err) {
                    deferred.reject(err)
                });

                return deferred.promise;
            };

            return methods;
        }
    ])

})(window.EVA.app);
