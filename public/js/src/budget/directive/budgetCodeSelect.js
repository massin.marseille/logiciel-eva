(function(app) {

    app.directive('budgetCodeSelect', [
        'budgetCodeService', 'selectFactoryService',
        function budgetCodeSelect(budgetCodeService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: budgetCodeService,
                    apiParams: {
                        col: ['id', 'name']
                    }
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
