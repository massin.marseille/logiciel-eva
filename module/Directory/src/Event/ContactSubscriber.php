<?php

namespace Directory\Event;

use Directory\Entity\Contact;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Ramsey\Uuid\Uuid;

class ContactSubscriber implements EventSubscriber
{
    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Contact) {
                if (!$entity->getUuid()) {
                    $entity->setUuid(Uuid::uuid4()->toString());
                }

                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }
    }

    public function getSubscribedEvents()
    {
        return [
            Events::onFlush
        ];
    }
}
