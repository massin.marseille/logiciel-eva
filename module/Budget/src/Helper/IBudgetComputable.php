<?php

namespace Budget\Helper;

interface IBudgetComputable {

    public function getAmountPosteExpense($em = null, $balance = '');

    public function getAmountPosteExpenseArbo($em = null, $balance = '');

    public function getAmountHTPosteExpense($em = null, $balance = '');

    public function getAmountHTPosteExpenseArbo($em = null, $balance = '');

    public function getAmountPosteIncome($em = null, $balance = '');

    public function getAmountPosteIncomeArbo($em = null, $balance = '');

    public function getAmountHTPosteIncome($em = null, $balance = '');

    public function getAmountHTPosteIncomeArbo($em = null, $balance = '');

}