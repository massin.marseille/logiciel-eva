<?php

namespace Application\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class ConfigurationController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->basicFormAccess('application:e:configuration', 1)) {
            return $this->notFoundAction();
        }

        $modules = $this->moduleManager()->getModules();
        $translations = [];

        if ($this->moduleManager()->isActive('project')) {
            $clientModules = $this->clientPrivateConfiguration()->getModules();
            $fieldDescriptor = $this->serviceLocator->get('ProjectFieldDescriptorService');
            $translations = [
                'levels' => [],
                'project' => [],
                'project_help' => [],
                'project_placeholder' => []
            ];

            $levels = isset($clientModules['project']['levels']) ? $clientModules['project']['levels'] : [];
            foreach ($levels as $level) {
                $translations['levels'][] = $level['name'];
            }

            $fieldKeys = $fieldDescriptor->getAllFieldsKeys();
            foreach ($fieldKeys as $key) {
                if ($key != 'template') {
                    $translations['project'][] = 'project_field_' . $key;
                    $translations['project_help'][] = 'project_help_' . $key;
                }
            }

            $textareaKeys = $fieldDescriptor->getTextareaKeys();
            foreach ($textareaKeys as $key) {
                $translations['project_placeholder'][] = 'project_placeholder_' . $key;
            }
        }

        return new ViewModel([
            'modules'      => $modules,
            'translations' => $translations
        ]);
    }
}
