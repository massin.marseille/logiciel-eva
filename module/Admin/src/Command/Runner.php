<?php

namespace Admin\Command;

use Age\Import\Importer;
use Bootstrap\Entity\Client;
use Budget\Entity\Expense;
use Budget\Entity\Income;
use Directory\Entity\Contact;
use Doctrine\ORM\EntityManager;
use Exception;
use Field\Entity\Field;
use Field\Entity\Value;
use Project\Entity\Project;
use Ramsey\Uuid\Uuid;
use User\Entity\Role;
use PostParc\Service\PostParcSynchronisationService;

class Runner
{
    protected $entityManager;
    protected $client;

    /**
     * @param EntityManager $entityManager Client EM
     * @param Client $client
     */
    public function __construct(EntityManager $entityManager, Client $client)
    {
        $this->entityManager = $entityManager;
        $this->client        = $client;
    }

    public function run($name)
    {
        $success = false;

        switch ($name) {
            case 'sync-post-parc':
                $modules = $this->client->getModules();
                try {
                    if (isset($modules['postParc']) && $modules['postParc']['active']) {
                        $syncroService = new PostParcSynchronisationService($modules['postParc']['configuration'], $this->entityManager);
                        $syncroService->run();
                    }
                } catch (Exception $e) {
                    var_dump($e->getMessage());
                }

                $success = true;
                break;
            case 'project-level':

                $recomputeConfiguration = false;
                $maxLevel = 0;

                $projects = $this->entityManager->getRepository('Project\Entity\Project')->findAll();
                foreach ($projects as $project) {
                    $level = $project->computeLevel();
                    if ($level > $maxLevel) {
                        $maxLevel = $level;
                    }
                    $recomputeConfiguration = true;
                }

                if ($recomputeConfiguration) {
                    $configuration = $this->entityManager->getRepository('Application\Entity\Configuration')->find(1);
                    $modules = $configuration->getModules();
                    if (!isset($modules['project']['levels'])) {
                        $modules['project']['levels'] = [];
                    }

                    $baseMaxLevel = $this->entityManager->getRepository('Project\Entity\Project')->findMaxLevel();
                    if ($baseMaxLevel > $maxLevel) {
                        $maxLevel = $baseMaxLevel;
                    }

                    for ($i = 0; $i <= $maxLevel; $i++) {
                        if (!isset($modules['project']['levels'][$i])) {
                            $modules['project']['levels'][$i] = [
                                'name' => 'Niveau ' . $i
                            ];
                        }
                    }

                    $configuration->setModules($modules);
                }

                $this->entityManager->flush();
                $success = true;

                break;
            case 'poste-hierarchy':
                $incomes  = $this->entityManager->getRepository('Budget\Entity\PosteIncome')->findAll();
                $expenses = $this->entityManager->getRepository('Budget\Entity\PosteExpense')->findAll();

                foreach ($incomes as $poste) {
                    $parent = $poste->findParent();
                    if ($parent && $poste->getParent() == null) {
                        $poste->setParent($parent);
                    }
                }

                foreach ($expenses as $poste) {
                    $parent = $poste->findParent();
                    if ($parent && $poste->getParent() == null) {
                        $poste->setParent($parent);
                    }
                }

                $this->entityManager->flush();
                $success = true;

                break;
            case 'poste-cleaning':
                $configuration = $this->entityManager->getRepository('Application\Entity\Configuration')->find(1);
                $modules = $configuration->getModules();
                $loop = 4;
                if (isset($modules['project']['levels'])) {
                    $loop = sizeof($modules['project']['levels']);
                }

                for ($i = 0; $i <= $loop; $i++) {
                    $incomes  = $this->entityManager->getRepository('Budget\Entity\PosteIncome')->findAll();
                    $expenses = $this->entityManager->getRepository('Budget\Entity\PosteExpense')->findAll();

                    foreach ($incomes as $poste) {
                        if ($poste->getIncomesArbo()->count() === 0 && $poste->getAmountArbo() === 0) {
                            $children = $this->entityManager->getRepository('Budget\Entity\PosteIncome')->findByParent($poste);
                            if (!$children) {
                                $this->entityManager->remove($poste);
                            }
                        }
                    }

                    foreach ($expenses as $poste) {
                        if ($poste->getExpensesArbo()->count() === 0 && $poste->getAmountArbo() == 0) {
                            $children = $this->entityManager->getRepository('Budget\Entity\PosteExpense')->findByParent($poste);
                            if (!$children) {
                                $this->entityManager->remove($poste);
                            }
                        }
                    }

                    $this->entityManager->flush();
                }

                $success = true;
                break;
            case 'contact-invitation-add-uuid':
                /** @var Contact[] $contacts */
                $contacts = $this->entityManager
                    ->getRepository('Directory\Entity\Contact')
                    ->findAll();

                foreach ($contacts as $contact) {
                    if (!$contact->getUuid()) {
                        $contact->setUuid(Uuid::uuid4());
                        $this->entityManager->persist($contact);
                    }
                }

                $this->entityManager->flush();

                $success = true;
                break;
            case 'sync-age':
                $modules = $this->client->getModules();
                try {
                    if (isset($modules['age']) && $modules['age']['active']) {
                        $importer = new Importer($modules['age']['configuration'], $this->entityManager);
                        $importer->run();
                    }
                } catch (Exception $e) {
                    var_dump($e->getMessage());
                }

                $success = true;
                break;
            case 'event-rgpd':
                /** @var Field[] $fields */
                $fields = $this->entityManager
                    ->getRepository('Field\Entity\Field')
                    ->findAll();

                foreach ($fields as $field) {
                    if (!$field->getUuid()) {
                        $field->setUuid(Uuid::uuid4());
                        $this->entityManager->persist($field);
                    }
                }

                /** @var Value[] $values */
                $values = $this->entityManager
                    ->getRepository('Field\Entity\Value')
                    ->findAll();

                foreach ($values as $value) {
                    if (!$value->getUuid()) {
                        $value->setUuid(Uuid::uuid4());
                        $this->entityManager->persist($value);
                    }
                }

                $this->entityManager->flush();

                $success = true;
                break;
            case 'project-rates':
                // Bug avec ce client (et pour l'instant ils n'ont pas le module budget)
                if ($this->client->getName() === 'PNR Corse') {
                    break;
                }

                /** @var Project[] $projects */
                $projects = $this->entityManager->getRepository(Project::class)->findAll();

                foreach ($projects as $project) {
                    $project->setRateExpenseRealized();
                    $project->setRateIncomeRealized();

                    $this->entityManager->persist($project);
                }

                $this->entityManager->flush();

                $success = true;
                break;
            case 'roles-budget-upgrade':
                /** @var Role[] $roles */
                $roles = $this->entityManager
                    ->getRepository('User\Entity\Role')
                    ->findAll();

                foreach ($roles as $role) {
                    $rights = $role->getRights();
                    foreach (['expense', 'income'] as $transaction) {
                        foreach (['c', 'u', 'd'] as $crud) {
                            $modules = $this->client->getModules();
                            if (isset($modules['budget'])) {
                                if (isset($modules['budget']['configuration'][$transaction . 'Types'])) {
                                    $configuration = $modules['budget']['configuration'];
                                    $types = $configuration[$transaction . 'Types'];
                                } else {
                                    $types = $transaction === 'expense' ? Expense::getTypes() : Income::getTypes();
                                }

                                $rights['budget:c:' . $transaction . ':type:' . $crud] = ['value' => $types];
                            }
                        }
                    }

                    $role->setRights($rights);
                }

                $this->entityManager->flush();

                $success = true;
                break;
        }

        return $success;
    }

}
