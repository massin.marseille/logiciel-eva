<?php

namespace Core\Model;

class CurlAuthentication
{

    /**
     * @var string
     */
    private $_username;

    /**
     * @var string
     */
    private $_password;

    /**
     * @param string username
     * @param string password
     */
    public function __construct($username, $password)
    {
        $this->_username = $username;
        $this->_password = $password;
    }

    

    /**
     * Get the value of _username
     *
     * @return  string
     */ 
    public function getUsername()
    {
        return $this->_username;
    }

    

    /**
     * Get the value of _password
     *
     * @return  string
     */ 
    public function getPassword()
    {
        return $this->_password;
    }
}
