(function(app) {
    app.controller('destinationListController', [
        'destinationService',
        '$scope',
        'flashMessenger',
        'translator',
        function (destinationService, $scope, flashMessenger, translator) {
            var self = this;

            self.destinations = [];
            self.options = {
                name: 'destinations-list',
                col: [
                    'id',
                    'code',
                    'name',
                    'parent.id',
                    'parent.code',
                    'parent.name',
                    'createdAt',
                    'updatedAt',
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: false,
                getItems: function (params, callback, abort) {
                    destinationService.findAll(params, abort.promise).then(function(res) {
                        callback(res);
                    });
                },
            };

            self.fullTextSearch = function () {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function () {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if (event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function (destination) {
                if (angular.isArray(destination.keywords) || destination.keywords == null) {
                    destination.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine = destination;
                self.editedLineFields = angular.copy(destination);
            };

            self.saveEditedLine = function (destination) {
                self.editedLineErrors = {};
                destinationService
                    .save(angular.extend({}, destination, self.editedLineFields), {
                        col: self.options.col,
                    })
                    .then(
                        function(res) {
                            if (res.success) {
                                destination = angular.extend(destination, res.object);
                                self.editedLine = null;
                                self.editedLineFields = null;

                                flashMessenger.success(translator('destination_message_saved'));
                            }
                        },
                        function(err) {
                            for (var field in err.fields) {
                                self.editedLineErrors[field] = err.fields[field];
                            }
                        },
                    );
            };

            self.deleteDestination = function (destination) {
                if (confirm(translator('destination_question_delete').replace('%1', destination.name))) {
                    destinationService.remove(destination).then(function(res) {
                        if (res.success) {
                            self.destinations.splice(self.destinations.indexOf(destination), 1);
                            flashMessenger.success(translator('destination_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        },
    ]);
})(window.EVA.app);
