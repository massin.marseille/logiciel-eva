(function(app) {

    app.directive('profileSelect', [
        'profileService', 'selectFactoryService',
        function profileSelect(profileService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: profileService
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
