<?php

return [
    'icons' => [
        'project'           => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-file-o',
        ],
        'folder'  => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-folder',
        ],
        'project_help'           => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-info-circle',
        ],
        'project_placeholder' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-align-left'
        ],
        'levels'           => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-sitemap',
        ],
        'project-manager'   => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-gears',
        ],
        'project-validator' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-check',
        ],
        'project-template' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-file-o',
        ],
    ],
];
