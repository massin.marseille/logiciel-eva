<?php

namespace Core\Utils;

use Core\Model\CurlAuthentication;
use Exception;

class CurlUtils
{

    /**
     * Retrieve the content of an url through CURL
     *
     * @param string $url  the resource to get content from
     * @param CurlAuthentication $curlAuthentication
     *
     * @return string the url content on success
     * @throws Exception with the Curl error message
     */
    public static function getContent($url, $curlAuthentication = null)
    {
        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CONNECTTIMEOUT => 1000,
            CURLOPT_TIMEOUT        => 1000,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            // mandatory for office365 api
            CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/81.0',
        );
        if (isset($curlAuthentication)) {
            $options[CURLOPT_USERPWD] = $curlAuthentication->getUsername() . ':' . $curlAuthentication->getPassword();
        }

        $ch = \curl_init();
        \curl_setopt_array($ch, $options);
        $result = \curl_exec($ch);
        if ($result === false) {
            throw new \Exception(\curl_error($ch));
        }
        \curl_close($ch);

        return $result;
    }
}
