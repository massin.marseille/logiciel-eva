<?php

namespace Core;

use Core\Controller\Plugin\ApiRequestPlugin;
use Core\Controller\Plugin\ModuleManagerPlugin;
use Core\Export\ExcelExporter;
use Core\Module\MyModuleManager;
use Core\View\Helper\Angular;
use Core\View\Helper\FabMenu;
use Core\View\Helper\Icon;
use Core\View\Helper\Menu;
use Core\View\Helper\TableFilter;
use Laminas\ServiceManager\ServiceManager;

/**
 * Class Module
 *
 * The Core module is not functional, it only contains some utilities/abstract classes.
 *
 * @package Core
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class Module
{
    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'MyModuleManager' => function (ServiceManager $serviceManager) {
                    $moduleManager = $serviceManager->get('ModuleManager');
                    $environment = $serviceManager->get('Environment');
                    return new MyModuleManager($moduleManager, $environment);
                },
                'MergedConfig'    => function (ServiceManager $serviceManager) {
                    $moduleManager = $serviceManager->get('ModuleManager');
                    return $moduleManager->getEvent()->getConfigListener()->getMergedConfig(false);
                },
                'ExcelExporter'   => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $translator = $helperPluginManager->get('translate');
                    return new ExcelExporter($translator);
                },
            ],
            
        ];
    }

    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'invokables' => [
                'translate' => 'Core\View\Helper\Translate',
            ],
            'factories'  => [
                'angular'     => function (ServiceManager $serviceManager) {
                    return new Angular($serviceManager);
                },
                'icon'        => function (ServiceManager $serviceManager) {
                    return new Icon($serviceManager);
                },
                'menu'        => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');

                    $translateViewHelper = $helperPluginManager->get('translate');
                    $iconViewHelper      = $helperPluginManager->get('icon');
                    $urlViewHelper       = $helperPluginManager->get('url');
                    return new Menu($serviceManager, $translateViewHelper, $iconViewHelper, $urlViewHelper);
                },
                'tableFilter' => function (ServiceManager $serviceManager) {
                    $translateViewHelper = $serviceManager->get('ViewHelperManager')->get('translate');
                    return new TableFilter($translateViewHelper);
                },
                'fabMenu'     => function () {
                    return new FabMenu();
                },
                'module'      => function (ServiceManager $serviceManager) {
                    $moduleManager = $serviceManager->get('MyModuleManager');
                    return new \Core\View\Helper\Module($moduleManager);
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'apiRequest' => function (ServiceManager $serviceManager) {
                    $controllerManager = $serviceManager->get('ControllerManager');
                    return new ApiRequestPlugin($controllerManager);
                },
                'moduleManager' => function (ServiceManager $serviceManager) {
                    $moduleManager = $serviceManager->get('MyModuleManager');
                    return new ModuleManagerPlugin($moduleManager);
                },
            ]
        ];
    }
}
