<?php

namespace Admin\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class ClientController extends AbstractActionSLController
{
    public function indexAction()
    {
        $themes       = scandir(getcwd() . '/public/theme');
        $modules      = $this->moduleManager()->getModules('client');
        $icons        = $this->serviceLocator->get('Config')['icons'];
        $translations = $this->serviceLocator->get('Translator')->getAllMessages()->getArrayCopy();

        ksort($icons);
        ksort($translations);

        return new ViewModel([
            'themes'       => $themes,
            'modules'      => $modules,
            'icons'        => $icons,
            'translations' => $translations
        ]);
    }

    public function getUsersAction()
    {
        $res = [];

        $id = $this->params()->fromRoute('id', null);
        if ($id) {
            $client = $this->entityManager()->getRepository('Bootstrap\Entity\Client')->find($id);
            if ($client) {
                $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
                $clientEm   = $entityManagerFactory->getEntityManager($client);

                $users = $clientEm->getRepository('User\Entity\User')->findAll();
                foreach ($users as $user) {
                    $res[] = [
                        'id'    => $user->getId(),
                        'name'  => $user->getName(),
                        'login' => $user->getLogin(),
                        'email' => $user->getEmail(),
                        'role'  => $user->getRole()->getName()
                    ];
                }
            }
        }

        return new JsonModel($res);
    }

    public function loginAsAction()
    {
        $res = [
            'success' => false,
            'host'    => null,
            'id'      => null,
        ];

        $id     = $this->params()->fromRoute('id', null);
        $userId = $this->params()->fromRoute('user', null);
        if ($id && $userId) {
            $client = $this->entityManager()->getRepository('Bootstrap\Entity\Client')->find($id);
            if ($client) {
                $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
                $clientEm   = $entityManagerFactory->getEntityManager($client);

                $user = $clientEm->getRepository('User\Entity\User')->find($userId);

                if ($user) {
                    $cipher = 'aes-256-ctr';
                    $ivlen = openssl_cipher_iv_length($cipher);
                    $iv = openssl_random_pseudo_bytes($ivlen);

                    $res['success'] = true;
                    $res['host']    = $client->getHost();
                    $res['secret']  = openssl_encrypt(json_encode(['user' => $user->getId(), 'client' => $client->getId()]), $cipher, 'BYPASS_LOGIN_SECRET', 0, 'mlmvSXoRqHF9cOkt');
                }
            }
        }

        return new JsonModel($res);
    }
}
