<?php

namespace Core\Utils;

class MailUtils
{

    /**
     * Get sender from a client
     * can be the client email when configured or the domain using a prefix (prefix@domain)
     */
    public static function getFrom($client, $prefix)
    {
        if (!isset($client)) {
            throw new \Exception("No client given to get email sender");
        }
        $clientEmail = $client->getEmail();
        return empty($clientEmail)
            ? $prefix . '@' . $client->getHost()
            : $clientEmail;
    }
}