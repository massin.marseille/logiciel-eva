<?php

namespace Field\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Field
 *
 * @package Field\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="field_field")
 */
class Field extends BasicRestEntityAbstract
{
    const TYPE_TEXT     = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_RADIO    = 'radio';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_SELECT   = 'select';
    const TYPE_DATE     = 'date';
    const TYPE_DATETIME = 'datetime';
    const TYPE_CONSENT  = 'consent';
    const TYPE_AMOUNT   = 'amount';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $uuid;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="Field\Entity\Group", inversedBy="fields")
     */
    protected $group;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $help;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var array
     *
     * @ORM\Column(type="json", nullable=true)
     */
    protected $meta;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="`order`", nullable=true)
     */
    protected $order = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $placeholder;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * @param string $help
     */
    public function setHelp($help)
    {
        $this->help = $help;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param array $meta
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return bool
     */
    public function isUnique()
    {
        return $this->type === self::TYPE_TEXT && isset($this->meta['unique']) && $this->meta['unique'] == true;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return isset($this->meta['required']) && $this->meta['required'] == true;
    }

    /**
     * @return bool
     */
    protected function isSelection()
    {
        return in_array($this->type, [self::TYPE_CHECKBOX, self::TYPE_RADIO, self::TYPE_SELECT]);
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'     => 'help',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'group',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $group = $entityManager->getRepository('Field\Entity\Group')->find($value);

                                return $group !== null;
                            },
                            'message'  => 'field_field_group_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name'      => 'type',
                'required'  => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if ($value !== self::TYPE_CONSENT) {
                                    return in_array($value, self::getTypes());
                                }

                                if (is_array($context['group'])) {
                                    $context['group'] = $context['group']['id'];
                                }

                                $group = $entityManager->getRepository('Field\Entity\Group')->find($context['group']);

                                return $group->getEntity() === 'Directory\Entity\Contact';
                            },
                            'message'  => 'The input was not found in the haystack',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'meta',
                'required' => false,
            ],
            [
                'name'     => 'order',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse'],
                ],
            ],
            [
                'name'     => 'placeholder',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_TEXT,
            self::TYPE_TEXTAREA,
            self::TYPE_RADIO,
            self::TYPE_CHECKBOX,
            self::TYPE_SELECT,
            self::TYPE_DATE,
            self::TYPE_DATETIME,
            self::TYPE_AMOUNT
        ];
    }

    /**
     * Get the value of placeholder
     *
     * @return  string
     */ 
    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    /**
     * Set the value of placeholder
     *
     * @param  string  $placeholder
     *
     * @return  self
     */ 
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }
}
