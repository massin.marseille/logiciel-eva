<?php

namespace Core\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ServiceManager\ServiceManager;

abstract class AbstractActionSLController extends AbstractActionController
{
    /**
     * @var ServiceManager
     */
    protected $serviceLocator;
    /**
     * @return ServiceManager
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    /**
     * @param mixed $serviceManager
     * @return $this
     */
    public function setServiceLocator($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
}