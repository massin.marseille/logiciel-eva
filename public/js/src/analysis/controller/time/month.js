(function (app) {

    app.controller('monthController', [
        '$timeout', '$scope', '$http', 'translator',
        function monthController($timeout, $scope, $http, translator) {
            var self = this;
            var shouldReload = false;

            self.datas = [];
            self.percentMonth = [];
            self.totalDone = 0;
            $scope.chart = {
                months: {
                    api: {},
                    options: {
                        chart: {
                            type: 'multiBarChart',
                            showControls: false,
                            showValues: true,
                            stacked: true,
                            height: 400,
                            labelsOutside: true,
                            x: function (d) {
                                return d.label;
                            },
                            y: function (d) {
                                return d.value;
                            },
                            yAxis: {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                }
                            }
                        }
                    },
                    data: [
                        {
                            'key': translator('timesheet_type_done'),
                            'color': '#4f99b4',
                            'values': []
                        }
                    ]
                }
            };

            $scope.$on('switchTab', function (event, data) {
                if (data.active === 'month' && shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                }
            });

            $scope.$on('applyFilters', function (event, data) {
                if (data.active === 'month' && data.shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                } else if (data.shouldReload) {
                    shouldReload = true;
                }
            });

            self.applyFilters = function (data) {
                self.loadingOn();
                $http({
                    url: '/analysis/time/months?' + $.param({ filters: data.filters }),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;

                    $scope.chart.months.data = [
                        {
                            'key': translator('timesheet_type_done'),
                            'color': '#4f99b4',
                            'values': []
                        }
                    ];
                    self.totalDone = 0;
                    if (typeof self.datas.res !== 'undefined') {
                        for (var clientId in self.datas.res.months) {
                            var months = self.datas.res.months[clientId];
                            for (var month in months) {
                                self.totalDone += months[month];
                                $scope.chart.months.data[0].values.push({
                                    'label': month,
                                    'value': months[month]
                                });
                            }
                        }
                        for (var clientId in self.datas.res.months) {
                            for (var month in self.datas.res.months[clientId]) {
                                self.percentMonth[month] = self.datas.res.months[clientId][month] / self.totalDone * 100;
                            }
                        }
                    }
                    $timeout(function () {
                        $scope.apiMonths.refresh();
                        self.loadingOff();
                    });
                }, function () { });
            };

            self.export = function (type, parent, group) {
                $scope.$emit('export', { type: type, parent: parent, group: group });
            };

            self.loadingOn = function () {
                self.loading = true;
                $scope.$emit('loadingOn');
            };

            self.loadingOff = function () {
                self.loading = false;
                $scope.$emit('loadingOff');
            };
        }
    ]);

})(window.EVA.app);
