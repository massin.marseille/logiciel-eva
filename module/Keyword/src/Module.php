<?php

namespace Keyword;

use Core\Module\AbstractModule;
use Keyword\Controller\Plugin\AllowedKeywordGroups;
use Keyword\Controller\Plugin\AllowedKeywordGroupsPlugin;
use Keyword\View\Helper\AllowedKeywordGroupsHelper;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'allowedKeywordGroups' => function(ServiceManager $serviceManager) {
                    $env = $serviceManager->get('Environment');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $user = $serviceManager->get('AuthStorage')->getUser();
                    $acl = $serviceManager->get('Acl');

                    return new AllowedKeywordGroups($env->getEntityManager(), $acl, $user);
                }
            ]
        ];
    }

    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'allowedKeywordGroups' => function (ServiceManager $serviceManager) {
                    $allowedKeywordGroups = $serviceManager->get('allowedKeywordGroups');
                    return new AllowedKeywordGroupsPlugin($allowedKeywordGroups);
                },
            ]
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'allowedKeywordGroups' => function (ServiceManager $serviceManager) {
                    $allowedKeywordGroups = $serviceManager->get('allowedKeywordGroups');
                    return new AllowedKeywordGroupsHelper($allowedKeywordGroups);
                },
            ]
        ];
    }
}
