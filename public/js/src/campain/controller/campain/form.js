(function(app) {

    app.controller('campainFormController', [
        'campainService', 'flashMessenger','$timeout', 'translator',
        function campainFormController(campainService, flashMessenger,
			$timeout, translator) {
            var self = this;
            
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'description',
                    'evaluationManagers.id',
                    'evaluationManagers.name',
                    'evaluationManagers.avatar',
                    'startDate',
                    'endDate'
                ]
            };
            
            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.campain = {
                indicators: [],
                groupIndicators: [],
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    campainService.find(id, apiParams).then(function (res) {
                        self.campain = res.object;
                        self.panelTitle = self.campain.name;
                    });
                }
                self.goToHash();
            };
            self.goToHash = function goToHash() {
				$timeout(function() {
					if (window.location.hash) {
						angular
							.element('a[href="' + window.location.hash + '"][data-toggle="tab"]')
							.trigger('click');
					}
				}, 1000);
			};

			angular.element('.project-form-nav a').click(function(e) {
				window.location.hash = this.hash;
			});
            
            self.savecampain = function campain() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.campain.id;

                campainService.save(self.campain, apiParams).then(function (res) {
                    self.campain = res.object;
                    self.panelTitle = self.campain.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.campain.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('campain_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteCampain = function deleteCampain() {
                if (confirm(translator('campain_question_delete').replace('%1', self.campain.name))) {
                    campainService.remove(self.campain).then(function (res) {
                        if (res.success) {
                            window.location = '/campain';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ]);

})(window.EVA.app);