<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 29/04/16
 * Time: 10:10
 */

namespace Directory\Controller\API;

use Core\Controller\BasicRestController;
use Directory\Entity\Job;
use Doctrine\ORM\QueryBuilder;
use Laminas\View\Model\JsonModel;

class JobController extends BasicRestController
{
    protected $entityClass  = 'Directory\Entity\Job';
    protected $repository   = 'Directory\Entity\Job';
    protected $entityChain  = 'directory:e:job';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }


    public function fusionAction()
    {
        $jobToKeep_id = $this->params()->fromRoute('id');
        $jobs_id     = $this->params()->fromPost('jobs_id', null);

        if (
            !$jobs_id ||
            count($jobs_id) === 0 ||
            !$this->acl()->isAllowed('directory:e:job:d')
        ) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var job $jobToKeep */
        $jobToKeep = $this->getEntityManager()
            ->getRepository('Directory\Entity\job')
            ->find($jobToKeep_id);

        if (!$jobToKeep) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Contact[] $contactToKeep */
        $associationsToKeep = $this->getEntityManager()
            ->getRepository('Directory\Entity\ContactJobStructure')
            ->findBy(['job' => $jobToKeep]);

        /** @var Association[] $contactsToChange */
        $associationsToChange = $this->getEntityManager()
            ->getRepository('Directory\Entity\ContactJobStructure')
            ->findBy(['job' => $jobs_id]);

        foreach ($associationsToChange as $association) {
            $association->setJob($jobToKeep);
            $this->getEntityManager()->persist($association);
        }

        $jobsToRemove = $this->getEntityManager()
            ->getRepository('Directory\Entity\Job')
            ->findById($jobs_id);

        foreach ($jobsToRemove as $job) {
            if ($job->getId() === $jobToKeep->getId()) {
                continue;
            }

            $this->getEntityManager()->remove($job);
        }

        $this->getEntityManager()->flush();
        return new JsonModel([
            'success' => true,
        ]);
    }
}


