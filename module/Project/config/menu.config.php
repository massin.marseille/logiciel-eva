<?php

$templates = [
    'icon' => 'project-template',
    'title' => 'template_module_title',
    'right' => 'project:e:template:r',
    'href' => 'project/template',
    'priority' => 6,
];

$project = [
    'icon' => 'project',
    'right' => 'project:e:project:r',
    'title' => 'project_module_title',
    'href' => 'project',
    'priority' => 1010,
];

return [
    'menu' => [
        'client-menu' => [
            'project' => $project,
            'administration' => [
                'children' => [
                    'templates' => $templates,
                ],
            ],
        ],
        'client-menu-parc' => [
            'suivi' => [
                'icon' => 'folder',
                'title' => 'menu_suivi_title',
                'children' => [
                    'project' => array_replace($project, ['priority' => 9999]),
                ]
            ],
            'administration' => [
                'children' => [
                    'templates' => array_replace($templates, ['priority' => 98]),
                ],
            ],
        ],
        'admin-menu' => [],
    ],
];
