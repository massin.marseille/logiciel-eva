<?php

namespace Export;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'export' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/export',
                    'defaults' => [
                        'controller' => 'Export\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'map'         => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/map',
                            'defaults' => [
                                'controller' => 'Export\Controller\Index',
                                'action'     => 'map',
                            ],
                        ],
                    ],
                    'advancement' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/advancement',
                            'defaults' => [
                                'controller' => 'Export\Controller\Index',
                                'action'     => 'advancement',
                            ],
                        ],
                    ],
                    'form'        => [
                        'type'          => 'Segment',
                        'options'       => [
                            'route' => '/form',
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'excel' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/excel[/:id]',
                                    'defaults' => [
                                        'controller' => 'Export\Controller\Index',
                                        'action'     => 'formExcel',
                                    ],
                                ],
                            ],
                            'word' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/word[/:id]',
                                    'defaults' => [
                                        'controller' => 'Export\Controller\Index',
                                        'action'     => 'formWord',
                                    ],
                                ],
                            ], 'publipostages' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/publipostages[/:id]',
                                    'defaults' => [
                                        'controller' => 'Export\Controller\Index',
                                        'action'     => 'formPublipostages',
                                    ],
                                ],
                            ]
                        ],
                    ],
                ],
            ],
            'api'    => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'export' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/export[/:id]',
                            'defaults' => [
                                'controller' => 'Export\Controller\API\Export',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class      => ServiceLocatorFactory::class,
            Controller\API\ExportController::class => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Export\Controller\Index'      => Controller\IndexController::class,
            'Export\Controller\API\Export' => Controller\API\ExportController::class,
        ],
    ],
];
