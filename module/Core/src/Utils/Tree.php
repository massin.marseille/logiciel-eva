<?php namespace Core\Utils;

/**
 * Class Tree
 * @package Core\Utils
 */
class Tree
{
    public static function insertInTree(&$_item, &$tree, $level)
    {
        foreach ($tree as $i => $item) {
            if ($item['id'] == $_item['parent']['id']) {
                $_item['_level']                    = $level;
                $tree[$i]['children'][$_item['id']] = $_item;

                return true;
            }

            if (self::insertInTree($_item, $tree[$i]['children'], $level + 1)) {
                return true;
            }
        }

        return false;
    }

    public static function toTree(array &$flatArray)
    {
        foreach ($flatArray as $i => $item) {
            if ((isset($flatArray[$i]['parent']) && $flatArray[$i]['parent'] != null) || (isset($flatArray[$i]['parent']['id']) && $flatArray[$i]['parent']['id'] != null)) {
                $parentIsInArray = false;
                foreach ($flatArray as $item2) {
                    if ($flatArray[$i]['parent']['id'] == $item2['id']) {
                        $parentIsInArray = true;
                    }
                }
                if (!$parentIsInArray) {
                    unset($flatArray[$i]['parent']);
                }
            }
        }

        $tree = [];
        $n    = sizeOf($flatArray);

        do {
            $relaunch = false;
            foreach ($flatArray as $i => $item) {
                $item['children'] = [];
                $item['_level']   = 0;

                if (!isset($item['parent']) || $item['parent'] == null || !isset($item['parent']['id']) || $item['parent']['id'] == null) {
                    $tree[$i] = $item;
                    $n--;
                    $relaunch = true;
                    unset($flatArray[$i]);
                } else {
                    if (self::insertInTree($item, $tree, 1)) {
                        unset($flatArray[$i]);
                        $n--;
                        $relaunch = true;
                    }
                }
            }
        } while ($n > 0 && $relaunch);

        return $tree;
    }

    /**
     * @todo  Implémenter la fonction d'ordre disponible en JS
     * @param array $flatArray
     * @return array
     */
    public static function toNNTree(array $flatArray)
    {
        $tree    = [];
        $indexed = [];
        foreach ($flatArray as $item) {
            $item['children']     = [];
            $indexed[$item['id']] = $item;
        }

        $shouldBeRemoved = [];
        foreach ($indexed as $id => $item) {
            if (isset($item['parents']) && $item['parents'] && count($item['parents']) > 0) {
                $control = true;

                foreach ($item['parents'] as $parent) {
                    if (isset($indexed[$parent['id']])) {
                        $indexed[$parent['id']]['children'][] = &$indexed[$id];
                    } else {
                        $control = false;
                    }
                }

                if ($control) {
                    $shouldBeRemoved[] = $id;
                }
            }
        }

        foreach ($indexed as $id => $item) {
            if (!in_array($id, $shouldBeRemoved)) {
                $tree[] = $item;
            }
        }

        return $tree;
    }

    public static function toArray($tree)
    {
        $array = [];
        return self::toArrayInternal($tree, $array);
    }

    protected static function toArrayInternal($tree, &$array)
    {
        foreach ($tree as $item) {
            $array[] = $item;
            self::toArrayInternal($item['children'], $array);
        }

        return $array;
    }

    public static function toArrayWithLevels($tree)
    {
        $array = [];
        return self::toArrayWithLevelsInternal($tree, $array, 0);
    }

    protected static function toArrayWithLevelsInternal($tree, &$array, $level)
    {
        foreach ($tree as $item) {
            $item['level'] = $level;
            $array[]       = $item;
            self::toArrayWithLevelsInternal($item['children'], $array, $level + 1);
        }

        return $array;
    }
}
