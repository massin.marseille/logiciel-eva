(function(app) {

    app.controller('taskFormController', [
        'taskService', '$scope', 'flashMessenger', 'translator',
        function taskFormController(taskService, $scope, flashMessenger, translator) {
            var self = this;

            var $modal = angular.element('#taskFormModal');
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'start',
                    'end',
                    'progress',
                    'project.id',
                    'project.name',
                    'users.id',
                    'users.name',
                    'users.avatar',
                    'description',
                    'keywords',
                    'createdAt',
                    'updatedAt'
                ]
            };

            self.task = {};
            self.from    = {
                type: null,
                id: null
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id   = id;
            };

            $scope.$on('create-task', function (event, params) {
                self.task = {
                    _rights: {
                        update: true
                    },
                    users: [params.user],
                    project: params.project,
                    progress: 0
                };

                self.panelTitle = translator('task_nav_form');
                $modal.modal('show');
            });

            $scope.$on('edit-task', function (event, task) {
                self.editedTask = task;
                self.task       = angular.copy(task);
                self.panelTitle = self.task.name;

                $modal.modal('show');
            });

            self.saveTask = function saveTask() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.task.id;

                taskService.save(self.task, apiParams).then(function(res) {
                    if (isCreation) {
                        $scope.$parent.self.tasks.push(res.object);
                    } else {
                        for(var i in $scope.$parent.self.tasks){
                            if($scope.$parent.self.tasks[i].id == res.object.id){
                                $scope.$parent.self.tasks[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('task_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.controlDates = function controlDates() {
                if (self.task.start && self.task.end) {
                    var start = moment(self.task.start, 'DD/MM/YYYY');
                    var end = moment(self.task.end, 'DD/MM/YYYY');

                    var days = end.diff(start, 'days', true);
                    if (days < 0) {
                        self.errors.fields['end'] = [translator('task_field_end_before_start_error')];
                    } else {
                        self.errors.fields['end'] = [];
                    }
                }
            };

            $scope.$watch(function () { return self.task.end}, function () {
                self.controlDates();
            });

            $scope.$watch(function () { return self.task.start}, function () {
                self.controlDates();
            });

        }
    ])

})(window.EVA.app);
