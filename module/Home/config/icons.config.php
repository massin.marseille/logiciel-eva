<?php

return [
    'icons' => [
        'home' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-home',
        ],
        'display' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-tv',
        ],
        'info' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-info-circle',
        ],
        'database' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-database'
        ],
        'folder'  => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-folder',
        ],
    ],
];
