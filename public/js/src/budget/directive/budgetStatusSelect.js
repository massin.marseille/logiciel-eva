(function(app) {

    app.directive('budgetStatusSelect', [
        'budgetStatusService', 'selectFactoryService',
        function budgetStatusSelect(budgetStatusService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: budgetStatusService,
                    apiParams: {
                        col: ['id', 'name', 'lock']
                    }
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
