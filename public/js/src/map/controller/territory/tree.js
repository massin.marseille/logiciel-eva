(function(app) {

    app.controller('territoryTreeController', [
        '$scope', '$q', 'territoryService', 'treeBuilderService',
        function territoryTreeController($scope, $q, territoryService, treeBuilderService) {
            var self = this;

            self.tree = [];
            self.loading = {
                tree: true
            };

            territoryService.findAll({
                col: [
                    'id',
                    'name',
                    'code',
                    'parents.id'
                ]
            }).then(function (res) {
                self.tree = treeBuilderService.toNNTree(res.rows);
                self.loading.tree = false;
            });

        }
    ])

})(window.EVA.app);
