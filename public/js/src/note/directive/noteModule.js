(function(app) {

    app.directive('noteModule', [
        'noteService', 'flashMessenger', 'translator', 'FileUploader',
        function noteModule(noteService, flashMessenger, translator, FileUploader) {
            return {
                restrict    : 'EA',
                require     : '^?note-module',
                replace     : false,
                transclude  : false,
                scope: {
                    entity: '@entity',
                    primary: '=primary',
                    right: '@right'
                },
                templateUrl : '/js/src/note/template/note.html',
                compile: function ($element, attrs) {
                    return function link ($scope, $element, attrs, $controller) {
                        var $modal = angular.element('#noteModal');

                        $scope.loading = {
                            save: false,
                            delete: false
                        };
                        $scope.note  = {
                            entity: $scope.entity,
                            primary: $scope.primary,
                            attachment: {
                                type: 'file',
                                entity: 'note'
                            }
                        };
                        $scope.notes = [];
                        $scope.errors = {
                            fields: {}
                        };
                        $scope.currentUser = null;

                        noteService.findAll({
                            right: $scope.right,
                            col: [
                                'id',
                                'name',
                                'message',
                                'attachment.url',
                                'createdBy.id',
                                'createdBy.name',
                                'createdAt'
                            ],
                            search: {
                                type: 'list',
                                data: {
                                    filters: {
                                        entity: {
                                            val: $scope.entity,
                                            op: 'eq'
                                        },
                                        primary: {
                                            val: $scope.primary,
                                            op: 'eq'
                                        }
                                    }
                                }
                            }
                        }).then(function (res) {
                            $scope.notes = res.rows;
                            $scope.currentUser = res.currentUser;
                        });

                        $modal.on('show.bs.modal', function () {
                            $scope.showNoteForm = false;
                            $scope.note  = {
                                entity: $scope.entity,
                                primary: $scope.primary,
                                attachment: {
                                    type: 'file',
                                    entity: 'note'
                                }
                            };
                        });

                        $scope.showNoteModal = function showNoteModal() {
                            $modal.modal('show');
                        };

                        $scope.saveNote = function saveNote() {
                            $scope.loading.save = true;

                            noteService.save($scope.note, {
                                right: $scope.right,
                                col: [
                                    'id',
                                    'name',
                                    'message',
                                    'attachment.url',
                                    'createdBy.id',
                                    'createdBy.name',
                                    'createdAt'
                                ]
                            }).then(function (res) {
                                $scope.notes.unshift(res.object);
                                $scope.showNoteForm = false;
                                $scope.note  = {
                                    entity: $scope.entity,
                                    primary: $scope.primary,
                                    attachment: {
                                        type: 'file',
                                        entity: 'note'
                                    }
                                };

                                flashMessenger.success(translator('note_message_saved'));

                                $scope.loading.save = false;
                            }, function (err) {
                                for (var field in err.fields) {
                                    $scope.errors.fields[field] = err.fields[field];
                                }
                                $scope.loading.save = false;
                            });
                        };

                        $scope.deleteNote = function deleteNote(note){
                            if(confirm(translator('note_question_delete'))){
                                noteService.remove(note, {
                                    right: $scope.right,
                                    col: [
                                        'id',
                                        'name',
                                        'message',
                                        'attachment.url',
                                        'createdBy.id',
                                        'createdBy.name',
                                        'createdAt'
                                    ]
                                }).then(function(res){
                                    var index = 0;
                                    for(var i = 0; i < $scope.notes.length; ++i){
                                        if($scope.notes[i].id == note.id){
                                            index = i;
                                            break;
                                        }
                                    }

                                    $scope.notes.splice(index, 1);
                                    flashMessenger.success(translator('note_message_deleted'));
                                });
                            }
                        };

                        $scope.uploader = new FileUploader();
                        $scope.reader   = new FileReader();
                        $scope.reader.onloadend = function(e) {
                            $scope.$apply(function() {
                                $scope.note.attachment.file = e.target.result;
                            })
                        };

                        $scope.uploader.onAfterAddingFile = function(fileItem) {
                            $scope.reader.readAsDataURL(fileItem._file);
                            $scope.note.attachment.filename = fileItem.file.name;
                            $scope.note.attachment.size = fileItem.file.size;
                            $scope.errors.fields['attachment'] = null;
                        };
                    }
                }
            }
        }
    ]);

})(window.EVA.app);
