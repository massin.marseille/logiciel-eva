<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 16/08/16
 * Time: 09:34
 */

namespace Budget\Export;

use Budget\Entity\Income;
use Core\Export\IExporter;

abstract class PosteIncome implements IExporter
{
    public static function getConfig()
    {
        $arr = [
            'account.name'            => [
                'name' => 'account',
            ],
            'envelope.name'           => [
                'name' => 'envelope',
            ],
            'financer.name'           => [
                'translation' => 'envelope_field_financer',
            ],
            'project.name'            => [
                'translation' => 'income_field_project',
            ],
            'incomes.convention'      => [
                'translation' => 'income_field_convention',
            ],
            'incomes.conventionTitle' => [
                'translation' => 'income_field_conventionTitle',
            ],
            'incomes.tiers'           => [
                'translation' => 'income_field_tiers',
            ],
            'incomes.anneeExercice'   => [
                'translation' => 'income_field_anneeExercice',
            ],
            'incomes.bordereau'       => [
                'translation' => 'income_field_bordereau',
            ],
            'incomes.mandat'          => [
                'translation' => 'income_field_mandat',
            ],
            'incomes.engagement'      => [
                'translation' => 'income_field_engagement',
            ],
            'incomes.complement'      => [
                'translation' => 'income_field_complement',
            ],
            'incomes.serie'           => [
                'translation' => 'income_field_serie',
            ],
            'incomes.date'            => [
                'translation' => 'income_field_date',
            ],
        ];

        foreach (Income::getTypes() as $type) {
            $arr['amountArbo' . ucfirst($type)] = [
                'translation' => 'income_type_' . $type,
                'property'    => 'amountArbo' . ucfirst($type),
            ];
            $arr['amountHTArbo' . ucfirst($type)] = [
                'translation' => 'income_type_' . $type . '_ht',
                'property'    => 'amountHTArbo' . ucfirst($type),
            ];
        }

        foreach (\Budget\Entity\PosteIncome::getBalances() as $balance) {
            $arr['amountArbo' . ucfirst($balance)] = [
                'translation' => 'poste_income_balance_' . $balance,
                'property'    => 'amountArbo' . ucfirst($balance),
            ];
            $arr['amountHTArbo' . ucfirst($balance)] = [
                'translation' => 'poste_income_balance_' . $balance . '_ht',
                'property'    => 'amountHTArbo' . ucfirst($balance),
            ];
        }

        return $arr;
    }

    public static function getAliases()
    {
        return [
            'financer.name' => 'envelope.financer.name',
        ];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        $linesBolded     = [];
        $total           = [];
        $indexPercentage = null;
        $indexAmount     = null;

        foreach ($cols as $i => $col) {
            if (preg_match('/^amount/', $col)) {
                $total[] = 0;
                if (preg_match('/^amount$/', $col)) {
                    $indexAmount = $i;
                }
            } else if (preg_match('/^id$/', $col)) {
                $total[] = 'Total';
            } else if (preg_match('/^percentage$/', $col)) {
                $indexPercentage = $i;
                $total[] = '';
            } else {
                $total[] = '';
            }
        }

        foreach ($objects as $object) {
            if (!in_array($object->getId(), $itemsRemoved) && ($itemsSelected === [] xor in_array($object->getId(), $itemsSelected))) {
                $row = [];
                $extractor->setObject($object);

                foreach ($cols as $i => $col) {
                    if (/*!preg_match('/^project/', $col) &&*/
                        !preg_match('/^incomes./', $col)
                    ) {
                        $row[] = $extractor->extract($col, $translator->__invoke('exporter_error_value'));

                        if (preg_match('/^amount/', $col)) {
                            $total[$i] += $extractor->extract($col, $translator->__invoke('exporter_error_value'));
                        }
                    } else {
                        $row[] = '';
                    }
                }

                $rows[] = $row;
                end($rows);
                $linesBolded[] = key($rows) + 1;
                reset($rows);

                foreach ($object->getIncomesArbo() as $income) {
                    $row = [];
                    $extractor->setObject($income);

                    $type = $income->getType();
                    foreach ($cols as $i => $col) {
                        if (!preg_match('/^account/', $col) &&
                            !preg_match('/^amount/', $col) &&
                            !preg_match('/^financer/', $col) &&
                            !preg_match('/^percentage$/', $col) &&
                            !preg_match('/^caducite/', $col) &&
                            !preg_match('/Date$/', $col) &&
                            !preg_match('/^incomes./', $col) &&
                            !preg_match('/^envelope/', $col) &&
                            !preg_match('/^arr/', $col) &&
                            !preg_match('/^subventionAmount/', $col)
                        ) {
                            $row[] = $extractor->extract($col, $translator->__invoke('exporter_error_value'));
                        } else if (preg_match('/^amountArbo' . ucfirst($type) . '$/', $col)) {
                            $row[] = $extractor->extract('amount', $translator->__invoke('exporter_error_value'));
                        } else if (preg_match('/^amountHTArbo' . ucfirst($type) . '$/', $col)) {
                            $row[] = $extractor->extract('amountHT', $translator->__invoke('exporter_error_value'));
                        } else if (preg_match('/^incomes./', $col)) {
                            $row[] = $extractor->extract(str_replace('incomes.', '', $col), $translator->__invoke('exporter_error_value'));
                        } else {
                            $row[] = '';
                        }
                    }
                    $rows[] = $row;
                }
            }
        }

        if ($indexPercentage && $indexAmount) {
            foreach ($linesBolded as $index) {
                if ($rows[$index - 1][$indexPercentage] === '') {
                    $rows[$index - 1][$indexPercentage] = round($rows[$index - 1][$indexAmount] * 100 / $total[$indexAmount], 1) . '%';
                }
            }
        }

        $rows[] = $total;
        end($rows);
        $linesBolded[] = key($rows) + 1;
        reset($rows);

        return [
            'rows'        => $rows,
            'linesBolded' => $linesBolded,
        ];
    }
}