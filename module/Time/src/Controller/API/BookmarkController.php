<?php

namespace Time\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class BookmarkController extends BasicRestController
{
    protected $entityClass  = 'Time\Entity\Bookmark';
    protected $repository   = 'Time\Entity\Bookmark';
    protected $entityChain  = 'time:e:bookmark';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        $queryBuilder->join('object.user', 'user');

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }


	/**
	 * Create a bookmark.
	 *
	 * @param array $data
	 *
	 * @return JsonModel
	 */
	public function create($data)
	{
        $bookmark = $this->getEntityManager()->getRepository($this->repository)->findOneBy(['name'=> $data['name']]);
        if ($bookmark){
            return parent::update($bookmark->getId(), $data);
        } else {
            return parent::create($data);
        }
	}
}
