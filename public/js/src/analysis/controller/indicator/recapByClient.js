(function (app) {

    app.controller('recapByClientController', [
        '$http', '$timeout', '$scope', 'queryService', '$rootScope',
        function recapController($http, $timeout, $scope, queryService, $rootScope) {
            var self = this;

            self.view = 'table';
            self.loading = false;
            self.filters = {
                project: {},
                indicator: {},
                measure: {},
                group: 'indicator'
            };
            self.group = null;
            self.indicators = {};
            self.years = [];
            self.clients = [];
            self.nbFilters = 1;

            self.applyFilters = function applyFilters() {
                self.loading = true;
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.indicator).length + Object.keys(self.filters.measure).length;
                var group = self.filters.group;
                $http({
                    url: '/analysis/indicator/recap-by-client?' + $.param({ filters: self.filters }),
                    method: 'GET',
                }).then(function (res) {
                    self.group = group;

                    self.indicators = res.data.indicators;
                    self.years = res.data.years;
                    self.clients = res.data.clients;
                    self.loading = false;

                    self.loadBarChart();
                    self.loadIndicatorBarChart();
                });
            };

            self.resetFilters = function () {
                self.filters.project = {};
                self.filters.indicator = {};
                self.filters.measure = {};
                self.applyFilters();
            };

            self.export = function () {
                window.open('/analysis/indicator/recap-by-client?' + $.param({ filters: self.filters, export: true }));
            };

            self.chart = {
                bar: {
                    options: {
                        chart: {
                            type: 'multiBarChart',
                            height: 600,
                            showDistX: true,
                            showDistY: true,
                            xAxis: {
                                tickFormat: function (d) {
                                    if (d == parseInt(self.years[0] - 1)) {
                                        return 'Initiale'
                                    } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                        return 'Finale';
                                    }
                                    return d;
                                },
                                tickValues: []
                            },
                        }
                    },
                    data: [],
                    api: null
                }
            };

            self.loadBarChart = function () {
                self.chart.bars = [];
                for(const clientId in self.clients){
                    self.chart.bars[clientId] = {
                        options: {
                            chart: {
                                type: 'multiBarChart',
                                height: 600,
                                showDistX: true,
                                showDistY: true,
                                xAxis: {
                                    tickFormat: function (d) {
                                        if (d == parseInt(self.years[0] - 1)) {
                                            return 'Initiale'
                                        } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                            return 'Finale';
                                        }
                                        return d;
                                    },
                                    tickValues: []
                                },
                            }
                        },
                        data: [],
                        api: null
                    };
                    self.chart.bars[clientId].client = self.clients[clientId];
                    self.chart.bars[clientId].options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];

                    for (var y in self.years) {
                        self.chart.bars[clientId].options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                    }
                    self.chart.bars[clientId].options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);

                    for (var i in self.indicators) {
                        var dataT = {
                            key: self.indicators[i].name + ' - Cible',
                            values: []
                        }, dataD = {
                            key: self.indicators[i].name + ' - Réalisé',
                            values: []
                        };

                        if (self.indicators[i].data[clientId].start.target.value) {
                            dataT.values.push({
                                y: self.indicators[i].data[clientId].start.target.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }

                        if (self.indicators[i].data[clientId].start.done.value) {
                            dataD.values.push({
                                y: self.indicators[i].data[clientId].start.done.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }

                        for (var y in self.years) {
                            var year = self.years[y];
                            if (self.indicators[i].data[clientId].intermediate[year] && self.indicators[i].data[clientId].intermediate[year].target.value) {
                                dataT.values.push({
                                    y: self.indicators[i].data[clientId].intermediate[year].target.value,
                                    x: parseInt(year),
                                });
                            }

                            if (self.indicators[i].data[clientId].intermediate[year] && self.indicators[i].data[clientId].intermediate[year].done.value) {
                                dataD.values.push({
                                    y: self.indicators[i].data[clientId].intermediate[year].done.value,
                                    x: parseInt(year),
                                });
                            }
                        }

                        if (self.indicators[i].data[clientId].end.target.value) {
                            dataT.values.push({
                                y: self.indicators[i].data[clientId].end.target.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }

                        if (self.indicators[i].data[clientId].end.done.value) {
                            dataD.values.push({
                                y: self.indicators[i].data[clientId].end.done.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }

                        if (dataT.values.length > 0)
                            self.chart.bars[clientId].data.push(dataT);
                        if (dataD.values.length > 0)
                            self.chart.bars[clientId].data.push(dataD);
                    }
                };
            };

            self.loadIndicatorBarChart = function () {
                self.chart.indicatorBars = [];
                for(const indicatorId in self.indicators){
                    self.chart.indicatorBars[indicatorId] = {
                        options: {
                            chart: {
                                type: 'multiBarChart',
                                height: 600,
                                showDistX: true,
                                showDistY: true,
                                xAxis: {
                                    tickFormat: function (d) {
                                        if (d == parseInt(self.years[0] - 1)) {
                                            return 'Initiale'
                                        } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                            return 'Finale';
                                        }
                                        return d;
                                    },
                                    tickValues: []
                                },
                            }
                        },
                        data: [],
                        api: null
                    };
                    self.chart.indicatorBars[indicatorId].options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];
                    self.chart.indicatorBars[indicatorId].indicator = self.indicators[indicatorId];
                    for (var y in self.years) {
                        self.chart.indicatorBars[indicatorId].options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                    }
                    self.chart.indicatorBars[indicatorId].options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);
                    for (var i in self.clients) {
                        var dataT = {
                            key: self.clients[i].name + ' - Cible',
                            values: []
                        }, dataD = {
                            key: self.clients[i].name + ' - Réalisé',
                            values: []
                        };

                        if (self.indicators[indicatorId].data[i].start.target.value) {
                            dataT.values.push({
                                y: self.indicators[indicatorId].data[i].start.target.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }

                        if (self.indicators[indicatorId].data[i].start.done.value) {
                            dataD.values.push({
                                y: self.indicators[indicatorId].data[i].start.done.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }

                        for (var y in self.years) {
                            var year = self.years[y];
                            if (self.indicators[indicatorId].data[i].intermediate[year] && self.indicators[indicatorId].data[i].intermediate[year].target.value) {
                                dataT.values.push({
                                    y: self.indicators[indicatorId].data[i].intermediate[year].target.value,
                                    x: parseInt(year),
                                });
                            }

                            if (self.indicators[indicatorId].data[i].intermediate[year] && self.indicators[indicatorId].data[i].intermediate[year].done.value) {
                                dataD.values.push({
                                    y: self.indicators[indicatorId].data[i].intermediate[year].done.value,
                                    x: parseInt(year),
                                });
                            }
                        }

                        if (self.indicators[indicatorId].data[i].end.target.value) {
                            dataT.values.push({
                                y: self.indicators[indicatorId].data[i].end.target.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }

                        if (self.indicators[indicatorId].data[i].end.done.value) {
                            dataD.values.push({
                                y: self.indicators[indicatorId].data[i].end.done.value,
                                x: parseInt(self.years[0] - 1)
                            });
                        }
                        if (dataT.values.length > 0)
                            self.chart.indicatorBars[indicatorId].data.push(dataT);
                        if (dataD.values.length > 0)
                            self.chart.indicatorBars[indicatorId].data.push(dataD);
                    }
                };
            };

            $scope.$watch(function () {
                return self.view
            }, function () {
                $timeout(function () {
                    self.applyFilters();
                    if (self.view == 'bar' || self.view == 'indcatorBars'){
                        self.applyFilters();
                    }
                });
            });

            // QUERIES
            self.queries = [];
            self.query = {
                list: 'analysis-indicator-recap-client'
            };
            self.errors = {
                fields: {}
            };
            self.loadingQueries = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService
                    .save(self.query, { col: queryApiCol })
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };
        }
    ]);
})(window.EVA.app);
