<?php

namespace Campain\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Core\Filter\DateTimeFilter;

class MeasureController extends BasicRestController
{
    protected $entityClass  = 'Campain\Entity\Measure';
    protected $repository   = 'Campain\Entity\Measure';
    protected $entityChain  = 'campain:e:campain';
    private $dateFilter;
    
    public function __construct()
    {
        $this->dateFilter = new DateTimeFilter();
    }

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false && $sort != 'ratio') {
            $sort = 'object.' . $sort;
        }
        $queryBuilder->join('object.campain', 'campain');
        $queryBuilder->join('object.indicator', 'indicator');
        $queryBuilder->join('indicator.users', 'user');
        $queryBuilder->join('object.targetMeasure', 'targetMeasure');
        $queryBuilder->join('object.realiseMeasure', 'realiseMeasure');
        $queryBuilder->join('realiseMeasure.indicator', 'realisedIndicator');
        $queryBuilder->leftJoin('realiseMeasure.groupIndicator', 'realisedgroupIndicator');
        $queryBuilder->leftJoin('targetMeasure.project', 'targetproject');
        $queryBuilder->leftJoin('realiseMeasure.project', 'realiseproject');

        if($sort == 'ratio'){
            $queryBuilder->addSelect('(realiseMeasure.value / targetMeasure.value) * 100 AS HIDDEN ratio');
        }
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'realisedIndicator.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }

        if (isset($data['filters']['territories']) && $data['filters']['territories']) {
            $territories = $data['filters']['territories'];

            $territoryRepository = $this->getEntityManager()->getRepository('Map\Entity\Territory');
            $territoriesValues = $territories['val'];
            $shouldTree = isset($territories['tree']) ? $territories['tree'] == 'true' : false;
            if ($shouldTree) {
                $territoriesValues = [];
                if (is_array($territories['val'])) {
                    foreach ($territories['val'] as $id) {
                        $territory = $territoryRepository->find($id);
                        if ($territory) {
                            $territoriesValues = array_merge($territoriesValues, $territory->getFlatTree(true));
                        }
                    }
                } else {
                    $territory = $territoryRepository->find($territories['val']);
                    if ($territory) {
                        $territoriesValues = array_merge($territoriesValues, $territory->getFlatTree(true));
                    }
                }
            }


            $queryBuilder->andWhere(
                '
                targetMeasure.id ' .
                    ($territories['op'] === 'neq' || $territories['op'] === 'isNull' ? 'NOT' : '') .
                    ' IN (SELECT terr.primary FROM Map\Entity\Association terr WHERE terr.entity = :terr_entity AND terr.primary = targetMeasure.id AND terr.territory IN (:terr_values))
            '
            );
            $queryBuilder->setParameter('terr_values', $territoriesValues);
            $queryBuilder->setParameter('terr_entity', 'Indicator\Entity\Measure');
            unset($data['filters']['territories']);
        }
    }

    public function update($id, $data)
    {
        $update = parent::update($id, $data);
        $measureCampain = $this->getEntityManager()
				->getRepository($this->repository)
				->find($id);
        if(isset($data['realiseMeasure']['territories']) && $measureCampain){
            $this->saveTerritories($measureCampain->getRealiseMeasure(), $data['realiseMeasure'], 'Indicator\Entity\Measure');
        }
        if(isset($data['targetMeasure']['territories']) && $measureCampain){
            $this->saveTerritories($measureCampain->getTargetMeasure(), $data['targetMeasure'], 'Indicator\Entity\Measure');
        }
        return $update;
    }

    protected function extract($object)
	{
		//$array = $this->getHydrator()->extract($object);
		$array = parent::extract($object);
        $entityClass = 'Indicator\Entity\Measure';
        $array['realiseMeasure']['territories'] = $this->getTerritories($object->getRealiseMeasure(), $entityClass);
        $array['targetMeasure']['territories'] = $this->getTerritories($object->getTargetMeasure(), $entityClass);
        return $array;
    }
    
    protected function processBodyContent($request){
        $data = parent::processBodyContent($request);
        $data = $this->applyDateFilter($data,'campain', 'startDate');
        $data = $this->applyDateFilter($data,'campain', 'endDate');
        $data = $this->applyDateFilter($data,'realiseMeasure', 'start');
        $data = $this->applyDateFilter($data,'realiseMeasure', 'date');
        $data = $this->applyDateFilter($data,'realiseMeasure', 'updatedAt');
        $data = $this->applyDateFilter($data,'targetMeasure', 'start');
        $data = $this->applyDateFilter($data,'targetMeasure', 'date');
        $data = $this->applyDateFilter($data,'targetMeasure', 'updatedAt');
        return $data;
    }

    private function applyDateFilter($data, $measurekey, $datekey ){
        if(isset($data[$measurekey][$datekey])){
            $data[$measurekey][$datekey] = $this->dateFilter->filter($data[$measurekey][$datekey]);
        }
        return $data;
    }
}
