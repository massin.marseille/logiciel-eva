<?php

namespace Core\Module;

use Bootstrap\Environment\Admin\AdminEnvironment;
use Bootstrap\Environment\Environment;
use Laminas\ModuleManager\ModuleManager;
use Laminas\ModuleManager\ModuleManagerInterface;

/**
 * Class MyModuleManager
 *
 * @package Core\Module
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class MyModuleManager
{
	/**
	 * @var ModuleManager
	 */
	protected $moduleManager;

	/**
	 * @var Environment
	 */
	protected $environment;

	/**
	 * @param ModuleManagerInterface $moduleManager
	 */
	public function __construct(ModuleManagerInterface $moduleManager, Environment $environment)
	{
		$this->moduleManager = $moduleManager;
		$this->environment = $environment;
	}

	/**
	 * @param string $environmentName
	 * @return array
	 */
	public function getModules($environmentName = null)
	{
		$modules = [];

		$environmentName = $environmentName !== null ? $environmentName : $this->environment->getName();

		$loadedModules = $this->moduleManager->getLoadedModules();
		foreach ($loadedModules as $module) {
			if (
				$module instanceof AbstractModule &&
				$module->isFunctional() &&
				$module->isAvailableForEnvironment($environmentName)
			) {
				$modules[] = $module;
			}
		}

		return $modules;
	}

	/**
	 * @return array
	 */
	public function getActiveModules()
	{
		$activeModules = [];
		$modules = $this->getModules();
		foreach ($modules as $module) {
			if ($module->isActive()) {
				$activeModules[] = $module;
			}
		}

		return $activeModules;
	}

	/**
	 * @param $moduleKey
	 * @return AbstractModule
	 * @throws \Exception
	 */
	public function getModule($moduleKey)
	{
		$modules = $this->getModules();
		foreach ($modules as $module) {
			if ($module->getKey() === $moduleKey) {
				return $module;
			}
		}

		throw new \Exception('The module ' . $moduleKey . ' doesn\'t exist');
	}

	/**
	 * Get rights array for all modules
	 * and admin modules
	 *
	 * @param array $modules The other modules to active or not
	 * @return array
	 * @since Version 0.1
	 * @version 0.1
	 */
	public function getModulesAdminRights($modules)
	{
		$rights = [];
		$allModules = $this->getModules('client');
		foreach ($allModules as $module) {
			$roleName = $module->getKey() . ':e:';
			$value = true;

			if (
				isset($modules[$module->getKey()]) &&
				isset($modules[$module->getKey()]['activable']) &&
				isset($modules[$module->getKey()]['active']) &&
				!$modules[$module->getKey()]['activable'] &&
				!$modules[$module->getKey()]['active']
			) {
				$value = false;
			}

			if ($module->getAclConfiguration()['entities']) {
				foreach ($module->getAclConfiguration()['entities'] as $acl) {
					$fullName = $roleName . $acl['name'] . ':';

					$rights[$fullName . 'c'] = [
						'value' => $value,
					];
					$rights[$fullName . 'r'] = [
						'value' => $value,
						'owner' => 'all',
					];
					$rights[$fullName . 'u'] = [
						'value' => $value,
						'owner' => 'all',
					];
					$rights[$fullName . 'd'] = [
						'value' => $value,
						'owner' => 'all',
					];

					if (isset($acl['properties'])) {
						foreach ($acl['properties'] as $property) {
							$rights[$module->getKey() . ':p:' . $acl['name'] . ':' . $property] = [
								'value' => $value,
								'owner' => 'all',
							];
						}
					}
				}
			}
		}
		return $rights;
	}

	/**
	 * @param $entityName
	 * @return null
	 */
	public function getEntityAclForName($entityName)
	{
		$allModules = $this->getModules('client');

		foreach ($allModules as $module) {
			if (isset($module->getAclConfiguration()['entities'])) {
				foreach ($module->getAclConfiguration()['entities'] as $acl) {
					if ($acl['name'] == $entityName) {
						return $acl['class'];
					}
				}
			}
		}

		return null;
	}

	/**
	 * @param $entityName
	 * @return null
	 */
	public function getAclEntityStringForName($entityName)
	{
		$allModules = $this->getModules('client');

		foreach ($allModules as $module) {
			if (isset($module->getAclConfiguration()['entities'])) {
				foreach ($module->getAclConfiguration()['entities'] as $acl) {
					if ($acl['name'] == $entityName) {
						return $module->getKey() . ':e:' . $acl['name'];
					}
				}
			}
		}

		return null;
	}

	/**
	 * @param string $moduleKey
	 * @return bool
	 */
	public function isActive($moduleKey)
	{
		try {
			return $this->getModule($moduleKey)->isActive();
		} catch (\Exception $exception) {
			return false;
		}
	}

	/**
	 * @param string $entityClass
	 * @return bool
	 */
	public function hasKeywords($entityClass)
	{
		foreach ($this->getActiveModules() as $module) {
			if ($module->entityHasKeyword($entityClass)) {
				return true;
			}
		}

		return false;
	}

	public function getClientModuleConfiguration($module, $key)
	{
		if ($this->environment instanceof AdminEnvironment) {
			return false;
		}

		$modules = $this->environment->getClient()->getModules();
		return $modules[$module]['configuration'][$key] ?? null;
	}
}
