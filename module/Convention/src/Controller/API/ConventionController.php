<?php

namespace Convention\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class ConventionController extends BasicRestController
{
    protected $entityClass = 'Convention\Entity\Convention';
    protected $repository  = 'Convention\Entity\Convention';
    protected $entityChain = 'convention:e:convention';

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        // Master can only access projects networkAccessible
        if ($this->isMasterRequest) {
            $data['filters']['networkAccessible'] = [
                'op'  => 'eq',
                'val' => 1
            ];
        }

        $sort = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->leftJoin('object.contractor', 'contractor');
        $queryBuilder->leftJoin('object.members', 'members');

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                    'object.number LIKE :f_full',
                    'contractor.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }

        if (isset($data['filters'])) {
            foreach ($data['filters'] as $property => $filter) {
                if ($property === '_client') {
                    $data['filterClient'] = $data['filters'][$property];
                    unset($data['filters'][$property]);
                    continue;
                }
            }
        }
    }

    public function getList($callbacks = [], $master = false)
    {
        $callbacks[] = function (&$objects, $data) {
            if (isset($data['filterClient'])) {
                if (isset($data['filterClient']['op']) && isset($data['filterClient']['val'])) {
                    $inArray = in_array($this->getClient()->getId(), $data['filterClient']['val']);
                    if ($data['filterClient']['op'] === 'eq' && !$inArray) {
                        $objects = [];
                    } elseif ($data['filterClient']['op'] === 'neq' && $inArray) {
                        $objects = [];
                    }
                }
            }
        };

        $res = parent::getList($callbacks, $master);

        return $res;
    }
}
