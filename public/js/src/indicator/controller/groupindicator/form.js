(function(app) {
    app.controller('filterController', [  
		'$scope',
		'$timeout',
		 function ($scope, $timeout) {
			var self = this;
	
			self.init = function () {
					$timeout(function () {
						if (typeof $scope.__filters === 'undefined') {
							$scope.__filters = {};
						}
					});
			};
		}
    ]);

    app.controller('indicatorsCampainListController', [
        'indicatorService', '$scope', 'flashMessenger', 'translator',
        function indicatorsCampainListController(indicatorService, $scope, flashMessenger, translator) {
            var self = this;
            self.groupIndicatorId = null;
            self.indicators = [];
            self.options = {
                name: 'campain-indicators-list',
                col: [
                    'id',
                    'name',
                    'uom',
                    'type',
                    'operator',
                    'values',
                    'keywords',
                    'archived',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    self.groupIndicatorId = $scope.$parent.self.groupindicator.id;
                    if(self.groupIndicatorId !== null){
                        params.search.data.filters['groupIndicatorId'] = {
                            op: 'eq',
                            val: self.groupIndicatorId
                        };
                    }
                    indicatorService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.init = function init(groupIndicatorId){
                self.groupIndicatorId = groupIndicatorId;
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(indicator) {
                if (angular.isArray(indicator.keywords) || indicator.keywords == null) {
                    indicator.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = indicator;
                self.editedLineFields = angular.copy(indicator);
            };

            self.saveEditedLine = function saveEditedLine(indicator) {
                self.editedLineErrors = {};
                console.log(indicator, self.editedLineFields);
                indicatorService.save(angular.extend({}, indicator, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        indicator = angular.extend(indicator, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('indicator_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.archiveIndicator = function archiveIndicator(indicator) {
                self.editedLineErrors = {};
                indicator.archived = indicator.archived ? 0 : 1;
                indicatorService.save(indicator, {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        indicator = angular.extend(indicator, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;
                        if (indicator.archived) {
                            flashMessenger.success(translator('indicator_message_archived'));
                        } else {
                            flashMessenger.success(translator('indicator_message_unarchived'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteIndicator = function deleteIndicator(indicator) {
                if (confirm(translator('indicator_question_delete').replace('%1', indicator.name))) {
                    indicatorService.remove(indicator).then(function (res) {
                        if (res.success) {
                            self.indicators.splice(self.indicators.indexOf(indicator), 1);
                            flashMessenger.success(translator('indicator_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ]);

    app.controller('measureCampainListController', [
        'measureService', '$scope', 'flashMessenger', 'translator', '$filter', '$timeout', '$rootScope',
        function measureCampainListController(measureService, $scope, flashMessenger, translator, $filter, $timeout, $rootScope) {
            var self = this;

            self.from = {
                type: null,
                id: null
            };
            self.object = null;

            self.measures = [];
            self.options = {
                name: 'campain-measures-list',
                col: [
                    'id',
                    'type',
                    'start',
                    'date',
                    'value',
                    'fixedValue',
                    'territories',
                    'project.id',
                    'project.name',
                    'campain.id',
                    'campain.name',
                    'comment',
                    'source',
                    'transverse',
                    'client',
                    'master',
                    'createdAt',
                    'updatedAt',
                    'indicator.name',
                    'indicator.id'
                ],
                searchType: 'list',
                sort: 'date',
                order: 'asc',
                pager: false,
                getItems: function getData(params, callback, abort) {
                    self.from.id = $scope.$parent.self.groupindicator.id;
                    if (self.from.id) {
                        if(self.from.type == 'indicator'){
                            params.search.data.filters['indicator.id'] = {
                                op: 'eq',
                                val: $scope.$parent.self[self.from.type].id || self.from.id
                            };
                        } else if(self.from.type == 'project'){
                            params.search.data.filters['project.id'] = {
                                op: 'eq',
                                val: $scope.$parent.self[self.from.type].id || self.from.id
                            };
                        } else if(self.from.type == 'group-indicator'){
                            params.search.data.filters['groupIndicator.id'] = {
                                op: 'eq',
                                val: self.from.id
                            };
                        }
                        
                        measureService.findAll(params, abort.promise).then(function(res) {
                            callback(res);
                        })
                    } else {
                        callback({ rows: [] });
                    }
                }
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id = id;

                if (self.from.type == 'project') {
                    self.options.col.push('indicator.id');
                    self.options.col.push('indicator.name');
                    self.options.col.push('indicator.type');
                    self.options.col.push('indicator.values');
                    self.options.col.push('indicator.operator');
                    self.options.col.push('indicator.description');
                    self.options.col.push('indicator.method');
                    self.options.col.push('indicator.definition');
                }
            };

            self.editMeasure = function editMeasure(measure) {
                if (self.from.type == 'indicator') {
                    measure.indicator = $scope.$parent.self[self.from.type];
                }
                $scope.$broadcast('edit-measure', measure);
                $scope.$broadcast('edit-attachement', measure.id);
            };

            self.createMeasure = function createMeasure() {
                $scope.$broadcast('create-measure');
            };

            self.deleteMeasure = function deleteMeasure(measure) {
                if (confirm(translator('measure_question_delete'))) {
                    measureService.remove(measure).then(function(res) {
                        if (res.success) {
                            self.measures.splice(self.measures.indexOf(measure), 1);
                            flashMessenger.success(translator('measure_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(measure) {
                if (self.from.type == 'indicator') {
                    measure.indicator = $scope.$parent.self[self.from.type];
                }
                self.editedLineErrors = {};
                self.editedLine = measure;
                self.editedLineFields = angular.copy(measure);
            };

            self.saveEditedLine = function saveEditedLine(measure) {
                self.editedLineErrors = {};
                measureService.save(angular.extend({}, measure, self.editedLineFields), {
                    col: self.options.col
                }).then(function(res) {
                    if (res.success) {
                        measure = angular.extend(measure, res.object);
                        self.editedLine = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('measure_message_saved'));
                    }
                }, function(err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.getRatio = function(indicator, measures) {
                var done = self.getValue(indicator, measures, 'done');
                var target = self.getValue(indicator, measures, 'target');

                return (target > 0 ? (done / target) * 100 : 0);
            };

            self.getValue = function getValue(indicator, measures, type) {
                var value = null;
                var values = [];

                for (var i in measures) {
                    if (measures[i].type == type) {
                        values.push(measures[i].value);
                    }
                }

                switch (indicator.operator) {
                    case 'sum':
                        var sum = 0;
                        for (var _i = 0; _i < values.length; _i++) {
                            sum += values[_i];
                        }

                        value = sum;
                        break;
                    case 'avg':
                        var sum = 0;
                        for (var _i = 0; _i < values.length; _i++) {
                            sum += values[_i];
                        }

                        value = sum / values.length;
                        break;
                    case 'med':
                        var count = values.length;
                        var middle = Math.floor(count / 2);
                        values.sort(function(a, b) {
                            return a - b;
                        });
                        var median = values[middle];
                        if (count % 2 == 0) {
                            median = (median + values[middle - 1]) / 2;
                        }
                        value = median;
                        break;
                    case 'max':
                        var max = null;
                        for (var _i = 0; _i < values.length; _i++) {
                            if (max == null || values[_i] > max) {
                                max = values[_i];
                            }
                        }

                        value = max;
                        break;
                    case 'min':
                        var min = null;
                        for (var _i = 0; _i < values.length; _i++) {
                            if (min == null || values[_i] < min) {
                                min = values[_i];
                            }
                        }

                        value = min;
                        break;
                }

                return value;
            };

            self.isManager = function isManager(user) {
                for (var i in $scope.$parent.self.project.members) {
                    var member = $scope.$parent.self.project.members[i];
                    if (member.id && member.user.id == user && member.role == 'manager') {
                        return true;
                    }
                }

                return false;
            };


            self.charts = [];
            self.loadCharts = function() {

                self.charts = [];

                var grouped = $filter('groupBy')(self.measures, 'indicator.name');
                for (var indicator in grouped) {
                    var chart = {
                        options: {
                            chart: {
                                type: 'lineChart',
                                height: 400,
                                useInteractiveGuideline: true,
                                x: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.label;
                                    }
                                    return null;
                                },
                                y: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.value;
                                    }
                                    return null;
                                },
                                xAxis: {
                                    tickFormat: function(d) {
                                        return moment(d, 'X').format('DD/MM/YYYY')
                                    }
                                }
                            },
                            title: {
                                enable: true,
                                text: indicator
                            }
                        },
                        api: null,
                        data: [{
                            values: [],
                            key: 'Prévu',
                            color: '#c3c3c3',
                        },
                            {
                                values: [],
                                key: 'Réalisé',
                                color: '#97bbcd'
                            }
                        ]
                    };

                    var measures = $filter('groupBy')(grouped[indicator], function(measure) {
                        return moment(measure.date, 'DD/MM/YYYY HH:mm').format('X')
                    });

                    var lastTarget = null;
                    var lastDone = null;
                    for (var time in measures) {
                        var target = null;
                        var done = null;

                        for (var j in measures[time]) {
                            var measure = measures[time][j];
                            if (measure.type == 'done' && (done == null || measure.value > done.value)) {
                                done = measure;
                            }

                            if (measure.type == 'target' && (target == null || measure.value > target.value)) {
                                target = measure;
                            }
                        }

                        if (target) {
                            lastTarget = target;
                        }

                        if (done) {
                            lastDone = done;
                        }


                        chart.data[0].values.push({
                            value: lastTarget ? lastTarget.value : 0,
                            label: parseInt(time)
                        });


                        chart.data[1].values.push({
                            value: lastDone ? lastDone.value : 0,
                            label: parseInt(time)
                        });
                    }

                    self.charts.push(chart);
                }
            };

            $scope.$watch(function() { return self.measures }, function() {
                self.loadCharts();
            }, true);

            $scope.$watch(function() { return $scope.showChart }, function() {
                self.loadCharts();
                $timeout(function() {
                    for (var i in self.charts) {
                        self.charts[i].api.refresh();
                    }
                });
            });

            $rootScope.$on('filter-transverse-measure', function(event, data) {
                $scope.load({
                    filters: {
                        transverse: {
                            op: 'eq',
                            val: '1'
                        },
                        indicator: {
                            op: 'eq',
                            val: data.id
                        }
                    }
                });
            });
        }
    ]);

    app.controller('groupIndicatorFormController', [
        'groupIndicatorService', '$controller', 'flashMessenger', 'translator',
        function groupIndicatorFormController(groupIndicatorService, $controller, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'operator',
                    'indicators.id',
                    'indicators.name'
                ]
            };
            
            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.groupindicator = {
                indicators: [],
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    groupIndicatorService.find(id, apiParams).then(function (res) {
                        self.groupindicator = res.object;
                        self.panelTitle = self.groupindicator.name;
                    });
                }
            };

            self.saveGroupIndicator = function saveGroupIndicator() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.groupindicator.id;

                groupIndicatorService.save(self.groupindicator, apiParams).then(function (res) {
                    self.groupindicator = res.object;
                    self.panelTitle = self.groupindicator.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var ctr = angular.element("#indicatorsCampainListCtrl");
                        ctr.init(self.groupindicator.id);
                        var url = window.location.pathname + '/' + self.groupindicator.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('group-indicator_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteGroupIndicator = function deleteGroupIndicator() {
                if (confirm(translator('group-indicator_question_delete').replace('%1', self.groupindicator.name))) {
                    groupIndicatorService.remove(self.groupindicator).then(function (res) {
                        if (res.success) {
                            window.location = '/group-indicator';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ]);

})(window.EVA.app);