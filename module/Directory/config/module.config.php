<?php

return [
    'module'       => [
        'directory' => [
            'environments' => [
                'client',
            ],

            'active'   => true,
            'required' => true,

            'acl' => [
                'entities' => [
                    [
                        'name'     => 'contact',
                        'class'    => 'Directory\Entity\Contact',
                        'owner'    => [],
                        'keywords' => true,
                    ],
                    [
                        'name'  => 'job',
                        'class' => 'Directory\Entity\Job',
                        'owner'    => [],
                        'keywords' => true,
                    ],
                    [
                        'name'     => 'structure',
                        'class'    => 'Directory\Entity\Structure',
                        'owner'    => [],
                        'keywords' => true,
                    ],
                    [
                        'name'     => 'contactEmail',
                        'class'    => 'Directory\Entity\ContactEmail',
                        'owner'    => [],
                        'keywords' => false,
                    ],
                    [
                        'name'     => 'contactJobStructure',
                        'class'    => 'Directory\Entity\ContactJobStructure',
                        'owner'    => [],
                        'keywords' => true,
                    ]
                    
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator'   => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
