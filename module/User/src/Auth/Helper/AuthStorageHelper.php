<?php

namespace User\Auth\Helper;

use User\Auth\AuthStorage;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class AuthStorageHelper
 *
 * Access auth storage from view.
 *
 * @package User\Auth\Helper
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AuthStorageHelper extends AbstractHelper
{
    /**
     * @var AuthStorage
     */
    protected $authStorage;

    /**
     * @param AuthStorage $authStorage
     */
    public function __construct(AuthStorage $authStorage)
    {
        $this->authStorage = $authStorage;
    }

    /**
     * @return AuthStorage
     */
    public function __invoke()
    {
        return $this->authStorage;
    }
}
