(function(app) {
    app.controller('managementUnitListController', [
        'managementUnitService',
        '$scope',
        'flashMessenger',
        'translator',
        function (managementUnitService, $scope, flashMessenger, translator) {
            var self = this;

            self.managementUnits = [];
            self.options = {
                name: 'managementUnits-list',
                col: [
                    'id',
                    'code',
                    'name',
                    'parent.id',
                    'parent.code',
                    'parent.name',
                    'createdAt',
                    'updatedAt',
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: false,
                getItems: function (params, callback, abort) {
                    managementUnitService.findAll(params, abort.promise).then(function(res) {
                        callback(res);
                    });
                },
            };

            self.fullTextSearch = function () {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function () {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if (event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function (managementUnit) {
                if (angular.isArray(managementUnit.keywords) || managementUnit.keywords == null) {
                    managementUnit.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine = managementUnit;
                self.editedLineFields = angular.copy(managementUnit);
            };

            self.saveEditedLine = function (managementUnit) {
                self.editedLineErrors = {};
                managementUnitService
                    .save(angular.extend({}, managementUnit, self.editedLineFields), {
                        col: self.options.col,
                    })
                    .then(
                        function(res) {
                            if (res.success) {
                                managementUnit = angular.extend(managementUnit, res.object);
                                self.editedLine = null;
                                self.editedLineFields = null;

                                flashMessenger.success(translator('management_unit_message_saved'));
                            }
                        },
                        function(err) {
                            for (var field in err.fields) {
                                self.editedLineErrors[field] = err.fields[field];
                            }
                        },
                    );
            };

            self.deleteOrganization = function (managementUnit) {
                if (confirm(translator('management_unit_question_delete').replace('%1', managementUnit.name))) {
                    managementUnitService.remove(managementUnit).then(function(res) {
                        if (res.success) {
                            self.managementUnits.splice(self.managementUnits.indexOf(managementUnit), 1);
                            flashMessenger.success(translator('management_unit_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        },
    ]);
})(window.EVA.app);
