<?php

namespace Time\Controller;

use Application\Entity\Query;
use Core\Controller\AbstractActionSLController;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Export\Entity\Custom;
use Export\Entity\Export;
use Keyword\Entity\Association;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('time:e:timesheet:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('timesheet', false);
        }

        $exports = $this->entityManager()->getRepository('Time\Entity\Export')->findAll();

        $customExportsWord  = [];
        $customExportsExcel = [];
        $customExports      = Custom::getExports($this->environment()->getClient()->getKey(), 'time');

        /** @var Custom $export */
        foreach ($customExports as $export) {
            if ($export->getType() === Export::TYPE_WORD) {
                $customExportsWord[] = $export;
            } else if ($export->getType() === Export::TYPE_EXCEL) {
                $customExportsExcel[] = $export;
            }
        }

        return new ViewModel([
            'groups'             => $groups,
            'exports'            => $exports,
            'customExportsWord'  => $customExportsWord,
            'customExportsExcel' => $customExportsExcel,
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('time:e:timesheet', $id)) {
            return $this->notFoundAction();
        }

        $timesheet = $this->entityManager()->getRepository('Time\Entity\Timesheet')->find($id);

        if ($timesheet && !$this->acl()->ownerControl('time:e:timesheet:u', $timesheet) && !$this->acl()->ownerControl('time:e:timesheet:r', $timesheet)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'timesheet' => $timesheet,
        ]);
    }

    /**
     * @return JsonModel
     */
    public function duplicateAction()
    {
        $success = true;
        $errors  = [];
        $newId   = null;

        try {
            $id = $this->params()->fromRoute('id', false);
            /* @var EntityManager $em */
            $em = $this->entityManager();

            /* @var Contact $contact */
            $timesheet = $em->getRepository('Time\Entity\Timesheet')->find($id);

            $newTimesheet = clone $timesheet;
            $newTimesheet->setCreatedAt(new \DateTime());
            $newTimesheet->setCreatedBy($this->authStorage()->getUser());

            $em->persist($newTimesheet);
            $em->flush();

            /**
             * Get all keywords of the timesheet
             * @var Association[] $associations
             */
            $associations = $em->getRepository('Keyword\Entity\Association')->findBy([
                'entity'  => 'Time\Entity\Timesheet',
                'primary' => $timesheet->getId(),
            ]);

            foreach ($associations as $association) {
                $newAssociation = clone $association;
                $newAssociation->setPrimary($newTimesheet->getId());
                $em->persist($newAssociation);
            }

            /**
             * Get all territories of the project
             */
            $territories = $em->getRepository('Map\Entity\Association')->findBy([
                'entity'  => 'Time\Entity\Timesheet',
                'primary' => $timesheet->getId(),
            ]);

            foreach ($territories as $territory) {
                $newTerritory = clone $territory;
                $newTerritory->setPrimary($newTimesheet->getId());
                $em->persist($newTerritory);
            }

            $em->flush();

            $newId = $newTimesheet->getId();
        } catch (\Exception $e) {
            $success  = false;
            $errors[] = $e->getMessage();
        } finally {
            return new JsonModel([
                'success'   => $success,
                'errors'    => $errors,
                'timesheet' => [
                    'id' => $newId,
                ],
            ]);
        }
    }

    public function weeklyAddAction()
    {
        if (!$this->acl()->basicFormAccess('time:e:timesheet', false)) {
            return $this->notFoundAction();
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->entityManager()->createQueryBuilder();
        $queryBuilder->select('object');
        $queryBuilder->from('Application\Entity\Query', 'object');
        $queryBuilder->where('object.list = :list');
        $queryBuilder->setParameter('list', 'projects-list');

        if ($this->acl()->onlyOwned('application:e:query:r')) {
            Query::ownerControlDQL($queryBuilder, $this->authStorage()->getUser(), $this->acl()->getOwningRight('application:e:query:r'));
        }

        $queries = $queryBuilder->getQuery()->getResult();

        return new ViewModel([
            'queries' => $queries,
        ]);
    }
}
