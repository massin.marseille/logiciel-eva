(function(app) {
	app.controller('conventionListController', [
		'conventionService',
		'conventionLineService',
		'$scope',
		'flashMessenger',
		'translator',
		function conventionListController(conventionService, conventionLineService, $scope, flashMessenger, translator) {
			var self = this;

			var apiParams = {
				col: [
					'id',
					'name',
					'number',
					'numberArr',
					'contractor.id',
					'contractor.name',
					'contractor.id',
					'lines.project.id',
					'lines.project.name',
					'members.id',
					'members.name',
					'members.avatar',
					'territories',
					'progress',
					'start',
					'end',
					'decisionDate',
					'notificationDate',
					'returnDate',
					'amount',
					'amountSubv',
					'description',
					'members.id',
					'members.name',
					'members.avatar',
					'financers.id',
					'financers.name',
					'lines.id',
					'lines.percentage',
					'lines.project.id',
					'lines.project.code',
					'lines.project.name',
					'lines.project.amountIncomeArboRequested',
					'lines.project.financerRepartitionIncomeArboRequested',
					'lines.project.timeSpent',
					'lines.project.amountIncomeAutofinancementArboRequested',
					'keywords',
					'territories',
					'networkAccessible'
				]
			};
			self.conventions = [];
			self.total = {
				id: 'total',
				_rights: {
					read: false,
					delete: false,
					update: false,
				},
			};

			const lineCols = [
				'id',
				'percentage',
				'project',
				'convention',
			];

			self.options = {
				name: 'conventions-list',
				col: [
					'id',
					'name',
					'number',
					'numberArr',
					'contractor.id',
					'contractor.name',
					'start',
					'end',
					'members.id',
					'members.name',
					'members.avatar',
					'progress',
					'description',
					'decisionDate',
					'notificationDate',
					'returnDate',
					'amount',
					'amountSubv',
					'partTotalProjects',
					'timeSpent',
					'autofinancement',
					'networkAccessible',
					'lines.id',
					'lines.percentage',
					'lines.project.id',
					'lines.project.code',
					'lines.project.name',
					'lines.project.amountIncomeArboRequested',
					'lines.project.financerRepartitionIncomeArboRequested',
					'lines.project.timeSpent',
					'lines.project.amountIncomeAutofinancementArboRequested',
					'keywords',
					'territories',
					'workflows',
					'createdAt',
					'updatedAt',
				],
				searchType: 'list',
				sort: 'name',
				order: 'asc',
				pager: false,
				getItems: function getData(params, callback, abort) {
					conventionService.findAll(params, abort.promise).then(function(res) {
						res.rows.push(self.getTotal(res.rows));
						callback(res);
					});
				},
			};

			self.fullTextSearch = function fullTextSearch() {
				$scope.__tb.resetFilters();
				$scope.__tb.apiParams.search.data.full = self.fullText;
				$scope.__tb.loadData();
			};

			self.resetfullTextSearch = function resetfullTextSearch() {
				self.fullText = '';
				delete $scope.__tb.apiParams.search.data.full;
			};

			angular.element('#full-text').on('keypress', function(event) {
				if (event.which == 13) {
					self.fullTextSearch();
				}
			});

			self.expenseTypes = {};
			self.init = function(expenseTypes) {
				self.expenseTypes = expenseTypes;
				for (var i in expenseTypes) {
					self.options.col.push('amountExpense' + expenseTypes[i].ucfirst());
					self.options.col.push('amountHTExpense' + expenseTypes[i].ucfirst());
				}
			};

			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};
			self.editLine = function editLine(convention) {
				if (angular.isArray(convention.keywords) || convention.keywords == null) {
					convention.keywords = {};
				}

				self.editedLineErrors = {};
				self.editedLine = convention;
				self.editedLineFields = angular.copy(convention);
			};

			self.saveEditedLine = function saveEditedLine(convention) {
				self.editedLineErrors = {};
				conventionService
					.save(angular.extend({}, convention, self.editedLineFields), {
						col: self.options.col,
					})
					.then(
						function(res) {
							if (res.success) {
								convention = angular.extend(convention, res.object);
								self.editedLine = null;
								self.editedLineFields = null;

								self.getTotal(self.conventions);

								flashMessenger.success(translator('convention_message_saved'));
							}
						},
						function(err) {
							for (var field in err.fields) {
								self.editedLineErrors[field] = err.fields[field];
							}
						},
					);
			};

			self.getTotal = function(rows) {
				self.total.amount = 0;
				self.total.amountSubv = 0;
				self.total.partTotalProjects = 0;
				self.total.timeSpent = 0;
				self.total.autofinancement = 0;

				for (var i in self.expenseTypes) {
					self.total['amountExpense' + self.expenseTypes[i].ucfirst()] = 0;
					self.total['amountHTExpense' + self.expenseTypes[i].ucfirst()] = 0;
				}

				for (var i in rows) {
					if (rows[i].id !== 'total') {
						self.total.amount += rows[i].amount;
						self.total.amountSubv += rows[i].amountSubv;
						self.total.partTotalProjects += rows[i].partTotalProjects;
						self.total.timeSpent += rows[i].timeSpent;
						self.total.autofinancement += rows[i].autofinancement;
						for (var j in self.expenseTypes) {
							self.total['amountExpense' + self.expenseTypes[j].ucfirst()] +=
								rows[i]['amountExpense' + self.expenseTypes[j].ucfirst()];
							self.total['amountHTExpense' + self.expenseTypes[j].ucfirst()] +=
								rows[i]['amountHTExpense' + self.expenseTypes[j].ucfirst()];
						}
					}
				}
				return self.total;
			};
			self.duplicateSelection = async function duplicateSelection() {
				for (const x of self.conventions) {
					if (x.selected)
						await self.duplicate(x, false);
				}
				window.location.reload();

			}

			self.isOneSelected = function isOneSelected () {
				for (const x of self.conventions) {
					if (x.selected)
						return true;
				}
				return false;
			}

			self.duplicate = async function duplicate(convention, withReload) {
				const newConvention = JSON.parse(JSON.stringify(convention));
				delete newConvention.id;
				delete newConvention.createdAt;
				delete newConvention.decisionDate;
				delete newConvention.start;
				delete newConvention.end;
				delete newConvention.progress;
				delete newConvention.returnDate;
				delete newConvention.notificationDate;
				delete newConvention.updatedAt;
				newConvention.amount = 0;
				newConvention.amountSubv = 0;
				newConvention.name = convention.name + ' (copie)';
				await conventionService.save(newConvention, apiParams).then(async res => {
					if (convention.lines) {
						for (const line of convention.lines) {
							let newLine = {
								convention: res.object.id,
								project: line.project.id,
								percentage: line.percentage
							}
							await conventionLineService.save(newLine, lineCols).then(_ => {});
						}

					}
				})
				flashMessenger.success(translator('convention_message_saved'));
				if (withReload)
					window.location.reload();
			};

			self.deleteConvention = function deleteConvention(convention) {
				if (confirm(translator('convention_question_delete').replace('%1', convention.name))) {
					conventionService.remove(convention).then(function(res) {
						if (res.success) {
							self.conventions.splice(self.conventions.indexOf(convention), 1);
							flashMessenger.success(translator('convention_message_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				}
			};

			self.isArray = function(value) {
				return value instanceof Array;
			};
		},
	]);
})(window.EVA.app);
