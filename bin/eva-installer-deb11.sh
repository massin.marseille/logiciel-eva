#!/bin/bash

#
# @author : gael@sigogneau.fr
# @since : 23/06/2020
# @doc : - this script install and configure a new SERVER for QPILOT.
#        - this script need to be exec. on CLEAN VM, backups need to be done before running installation.
#        - DO NOT run this script without read documentation before!
#

# ############ INSTALL VARIABLES # ############
TIMEZONE="Europe/Paris"
DNS_ROOT="siter.fr"
DNS_ADMIN="admin.eva.siter.fr"
APP_NAME="eva"
UNIX_USER_MAINTAINER="siter"
GITLAB_REPOSITORY="logiciel-eva/logiciel-eva"
GITLAB_BRANCH="laminas_php8"
MAIL_DEFAULT_TO="jmassin@siter.fr"
MYSQL_ROOT_PASSWORD="2X6jQxbGVqdurnWW"

# ############ TECHNICAL VARIABLES # ############
# if you change this var, check helper.sh
BACKUP_PATH="/home/automysqlbackup"
DIALOG=${DIALOG=dialog}
NOW=$(date +"%Y-%m-%d-%H-%M-%S")
MYSQL_VERSION="8.22"
BOWER_VERSION="16"
CURRENT_PATH=$(pwd)

checkRequirements() {
  OS_DEBIAN=$(cat /etc/os-release | grep ID=debian)
  VERSION=$(cat /etc/os-release | grep VERSION_ID=\"11\")

  if [[ $EUID -ne 0 ]]; then
    $DIALOG --title "ERROR" --clear --msgbox "PLEASE RUN THIS INSTALLER SCRIPT WITH ROOT USER.\nCurrent user : $EUID" 10 30
    exit 1
  fi
  if [ "$OS_DEBIAN" != "ID=debian" ]; then
    $DIALOG --title "ERROR" --clear --msgbox "DEBIAN OS IS REQUIRED.\nFOUND : $OS_DEBIAN" 10 30
    exit 1
  fi
  if [ "$VERSION" != "VERSION_ID=\"11\"" ]; then
    $DIALOG --title "ERROR" --clear --msgbox "DEBIAN VERSION 11 IS REQUIRED.\nFOUND : $VERSION" 10 30
    exit 1
  fi
  if [ "$CURRENT_PATH" = "/opt" ]; then
    $DIALOG --title "ERROR" --clear --msgbox "YOU CAN'T RUN INSTALLER SCRIPT FROM FOLDER $CURRENT_PATH.\nPlease place installer script on $CURRENT_PATH/install for exemple" 10 30
    exit 1
  fi
  read -p "This script use xDialog, you have to run it with compatible terminal. Press [Enter] to run installation now (last chance)."
}

inputVariables() {
  # prerequis
  apt update && apt install -y dialog iputils-ping

  # TIMEZONE
  exec 3>&1
  TIMEZONE=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "Server/Application timezone:" 15 60 $TIMEZONE 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$TIMEZONE" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi

  # DNS_ROOT
  exec 3>&1
  DNS_ROOT=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "What is you ROOT DNS (not need to be configured):" 15 60 $DNS_ROOT 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$DNS_ROOT" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi

  # DNS_ADMIN
  exec 3>&1
  DNS_ADMIN=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "What is you ADMIN DNS (NEED TO BE CONFIGURED (ping response)):" 15 60 $DNS_ADMIN 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$DNS_ADMIN" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi
  if ping -c 1 $DNS_ADMIN &>/dev/null; then
    echo "[INFO] $DNS_ADMIN have some response on ping"
  else
    echo clear && echo "[ERROR] DNS ZONE for $DNS_ADMIN is not configured" && exit 1
  fi

  # APP_NAME
  exec 3>&1
  APP_NAME=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "What is the name of this instance:" 15 60 $APP_NAME 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$APP_NAME" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi

  # GITLAB_REPOSITORY
  exec 3>&1
  GITLAB_REPOSITORY=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "Gitlab repository REFERENCE (code source).\n\nExemple:\nURL: https://gitlab.com/logiciel-eva/logiciel-eva/\nReference: logiciel-eva/logiciel-eva" 15 60 $GITLAB_REPOSITORY 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$GITLAB_REPOSITORY" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi

  # UNIX_USER_MAINTAINER
  exec 3>&1
  UNIX_USER_MAINTAINER=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "Give a name for unix user (maintainer user):" 15 60 $UNIX_USER_MAINTAINER 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$UNIX_USER_MAINTAINER" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi

  # MAIL_DEFAULT_TO
  exec 3>&1
  MAIL_DEFAULT_TO=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "Give the maintainer email:" 15 60 $MAIL_DEFAULT_TO 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$MAIL_DEFAULT_TO" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi

  # GITLAB_BRANCH
  exec 3>&1
  GITLAB_BRANCH=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "Which Qpilot version do you want?\n\nBETA version: develop\nSTABLE version: master" 15 60 $GITLAB_BRANCH 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$GITLAB_BRANCH" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi

  # MYSQL_ROOT_PASSWORD
  exec 3>&1
  MYSQL_ROOT_PASSWORD=$($DIALOG --title "MANDATORY INFORMATIONS" --clear --inputbox "Database ROOT PASSWORD\n(keep in mind : you need to input again this password on installation process):" 15 60 $MYSQL_ROOT_PASSWORD 2>&1 1>&3)
  exitcode=$?
  exec 3>&-
  if { [ "$exitcode" != 0 ] || [ -z "$MYSQL_ROOT_PASSWORD" ]; }; then
    echo clear && echo "[ERROR] CANCELLED BY USER" && exit 1
  fi
}

#
# Update and install default tools on centos
#
installTools() {
  echo "[START] SETUP ENV"
  echo "[INFO] Configure TimeZone ${TIMEZONE}"
  timedatectl set-timezone ${TIMEZONE}
  echo "[END] SETUP ENV"

  echo "[START] SETUP BASIC TOOLS"
  apt update && apt upgrade -y
  apt install -y curl telnet git wget telnet zip unzip openssl gnupg lsb-release dialog iputils-ping gnupg ssh &&
    ca-certificates apt-transport-https

  echo "[END] SETUP BASIC TOOLS"
}

#
# Config unix user and ssh access.
# - ROOT user need RSA key for ssh connection
# - MAINTAINER user can connect over SSH with RSA key or PASSWORD.
#
configUnixUser() {
  if [ -d "/home/$UNIX_USER_MAINTAINER" ]; then
    echo "[SKIP] UNIX USER $UNIX_USER_MAINTAINER IS ALREADY INSTALLED"
  else
    echo "[START] CONFIG UNIX USER AND ACCESS"
    echo "[INFO] CHANGE ROOT PASSWORD"
    passwd root

    echo "[INFO] CREATE UNIX MAINTAINER USER $UNIX_USER_MAINTAINER"
    useradd -m -s /bin/bash $UNIX_USER_MAINTAINER
    passwd $UNIX_USER_MAINTAINER
    usermod -aG sudo $UNIX_USER_MAINTAINER

    SSH_CONF_FILE="/etc/ssh/sshd_config"
    cp $SSH_CONF_FILE $SSH_CONF_FILE.bak.$NOW

    # temp allow root user to conenct with password only for install rsa key
    find $SSH_CONF_FILE -type f -exec sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' {} \;
    /etc/init.d/ssh reload

    # install ppk key on server at this step
    $DIALOG --title "PAUSE - REQUIRED ACTION" --clear --msgbox "Press [OK] key when you have finish to INSTALL your ROOT RSA and $UNIX_USER_MAINTAINER RSA key on this SERVER\n\nTips : installation is possible with bash or putty" 10 60

    echo "[INFO] disable ssh connection with password for all unix users"
    find $SSH_CONF_FILE -type f -exec sed -i 's/PermitRootLogin yes/PermitRootLogin without-password/g' {} \;
    echo "AllowUsers root $UNIX_USER_MAINTAINER" >>$SSH_CONF_FILE
    /etc/init.d/ssh reload

    $DIALOG --title "PAUSE - REQUIRED ACTION" --clear --msgbox "Press [OK] key when you have finish to TEST your ROOT/$UNIX_USER_MAINTAINER account connection from SSH protocol!\n\nTips: if your installation dont work do not close this SSH session..." 10 60

    echo "[END] CONFIG UNIX USER AND ACCESS"
  fi
}

#
# Install firewalld
#
installFirewall() {
  if [ -d "/etc/firewalld" ]; then
    echo "[SKIP] FIREWALL IS ALREADY INSTALLED"
  else
    echo "[START] SETUP FIREWALLD"
    apt -y install firewalld && firewall-cmd --state && ufw disable || echo "ok no ufw on os"
    firewall-cmd --add-service={ssh,http,https} --permanent --zone=public
    firewall-cmd --add-port=25/tcp --permanent --zone=public
    firewall-cmd --reload
    systemctl restart firewalld && firewall-cmd --list-all && systemctl status firewalld && systemctl enable firewalld
    echo "[END] SETUP FIREWALLD"
  fi
}

#
# Install firewalld
#
installFailToBan() {
  FAIL_CONFIG_FILE="/etc/fail2ban/jail.local"

  if [ -f "$FAIL_CONFIG_FILE" ]; then
    echo "[SKIP] FAIL2BAN IS ALREADY INSTALLED"
  else
    echo "[START] SETUP FAIL2BAN"
    apt -y install fail2ban && systemctl restart fail2ban && systemctl status fail2ban

    wait
    echo "[INFO] create new configuration on ${FAIL_CONFIG_FILE}"
    # override jail.conf by jail.local
    touch $FAIL_CONFIG_FILE
    cat <<-EOF >$FAIL_CONFIG_FILE
# configuration on local server : override of jail.conf
# do not edit directely jail.conf, use this file or folder ./jail.d
#

[DEFAULT]
# Ban IP for a long time
# ban for 1h = 3600
# ban for 1week = 604800
bantime = 604800

# conditions to ban an IP
findtime = 300
maxretry = 3

# Ban all protocol
protocol = tcp

# Destination email address used solely for the interpolations in jail.{conf,local,d/*} configuration files.
destemail = $MAIL_DEFAULT_TO
# Sender email address used solely for some actions
sender = $MAIL_DEFAULT_TO
# E-mail action. Since 0.8.1 Fail2Ban uses sendmail MTA for the mailing.
mta = sendmail

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true
EOF

    systemctl restart fail2ban
    echo "[INFO] fail2ban service's status"
    systemctl status fail2ban
    echo "[INFO] fail2ban config's status"
    fail2ban-client status
    echo "[INFO] you can get more informations with cmd (ex.) : fail2ban-client status sshd"
    fail2ban-client status sshd
    systemctl enable fail2ban

    echo "[INFO] logs is on /var/log/fail2ban.log"
    echo "[INFO] to check iptables config/ban. cmd : iptables -S"
    echo "[INFO] If you want UNBAN an ip use cmd : fail2ban-client set NOM_DU_JAIL unbanip ADRESSE_IP"

    echo "[END] SETUP FAIL2BAN"
  fi

}

#
# Install sendMail
#
installSendmail() {
  if [ -f "/etc/mail/local-host-names" ]; then
    echo "[SKIP] SENDMAIL IS ALREADY INSTALLED"
  else
    echo "[START] SETUP SENDMAIL"

    echo "[INFO] install sendmail..."
    apt install -y sendmail
    systemctl stop sendmail || echo "[INFO] sendmail already stop"
    # SENDMAIL BUG FIX INSTALLATION WITH SSL CERT (after kill -9)
    cd /etc/mail/tls || (echo "[ERROR] Fail on sendmail installation. Installation is not completed" && exit 1)
    openssl dsaparam -out sendmail-common.prm 2048
    chown root:smmsp sendmail-common.prm
    chmod 0640 sendmail-common.prm
    dpkg --configure -a

    echo "$DNS_ROOT" >>/etc/mail/local-host-names

    systemctl start sendmail && systemctl enable sendmail
    echo "[END] SETUP SENDMAIL"
  fi
}

#
# Install Apache AND php.
#
installApache() {
  if [ -d "/etc/php/8.2.6" ]; then
    echo "[SKIP] APACHE HTTPD IS ALREADY INSTALLED"
  else
    echo "[START] INSTALL APACHE HTTPD"

    cd /opt && wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list

    apt update && apt install -y php8.2 php8.2-cli php8.2-intl php8.2-zip php8.2-gd php8.2-ssh2 \
      php8.2-curl php8.2-mbstring php8.2-mysql php8.2-xml php8.2-opcache php8.2-zip \
      php8.2-tidy php8.2-imagick php8.2-apcu php8.2-mysql apache2

    PHP_CONF_FILE="/etc/php/8.2/apache2/php.ini"
    cp $PHP_CONF_FILE $PHP_CONF_FILE.bak.$NOW

    find $PHP_CONF_FILE -type f -exec sed -i 's/max_execution_time = 30/max_execution_time = 60/g' {} \;
    find $PHP_CONF_FILE -type f -exec sed -i 's/memory_limit = 128M/memory_limit = 512M/g' {} \;
    find $PHP_CONF_FILE -type f -exec sed -i 's/post_max_size = 8M/post_max_size = 50M/g' {} \;
    find $PHP_CONF_FILE -type f -exec sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 50M/g' {} \;

    echo "[INFO] disable access with ip address"
    echo "Forbidden access" >/var/www/html/index.html

    # prevent php conflit for cli
    rm -rf /etc/alternatives/php && ln -s /usr/bin/php8.2 /etc/alternatives/php
    # prevent php conflit for web server
    a2enmod php8.2

    a2enmod rewrite
    a2enmod headers
    a2enmod proxy
    a2enmod proxy_http

    systemctl restart apache2
    systemctl status apache2 && systemctl enable apache2

    echo "[END] INSTALL APACHE HTTPD"
  fi
}

installCertbot() {
  CERBOT_EXEC="/usr/bin/certbot"
  if [ -f "$CERBOT_EXEC" ]; then
    echo "[SKIP] CERTBOT IS ALREADY INSTALLED"
  else
    echo "[START] INSTALL CERTBOT"
    apt install -y snapd &&
      snap install core && snap refresh core &&
      snap install --classic certbot &&
      ln -s /snap/bin/certbot $CERBOT_EXEC
    echo "[END] INSTALL CERTBOT"
  fi
}

installNpmBower() {
  if [ -f "/usr/bin/node" ]; then
    echo "[SKIP] NODE IS ALREADY INSTALLED"
  else
    echo "[START] INSTALL NODE"
    apt-get install -y software-properties-common
    curl -sL https://deb.nodesource.com/setup_$BOWER_VERSION.x | sudo -E bash -
    apt install -y nodejs
    node --version && npm --version
    echo "[END] INSTALL NODE"
  fi
  if [ -f "/usr/bin/bower" ]; then
    echo "[SKIP] BOWER IS ALREADY INSTALLED"
  else
    echo "[START] INSTALL BOWER"
    npm install -g bower && bower --version
    echo "[END] INSTALL BOWER"
  fi
}

installDatabase() {
  if [ -d "/etc/mysql" ]; then
    echo "[SKIP] DATABASE IS ALREADY INSTALLED"
  else
    echo "[START] DATABASE INSTALL"
    cd /opt && wget https://dev.mysql.com/get/mysql-apt-config_0.$MYSQL_VERSION-1_all.deb

    read -p "On the next step you need to choose options \"MYSQL-SERVER AND CLUSTER\" + \"MYSQL-8.0\" + \"OK\". Press [Enter] when you are ready"
    # Choose "MYSQL-SERVER AND CLUSTER" option
    # Choose "mysql-8.0" option
    # Choose "ok"
    dpkg -i mysql-apt-config*

    read -p "On the next step you need to input again your mysql root password = $MYSQL_ROOT_PASSWORD AND choose option \"USE LEGACY AUTHENTICATION\" option. Press [Enter] when you are ready"

    apt update && apt install -y mysql-server
    # mysql_secure_installation FIXME NEED?
    systemctl enable mysql
    # test
    mysqladmin -u root -p version
    rm -rf /opt/mysql-apt-config*

    echo "[mysqld]" >>/etc/mysql/my.cnf
    echo "default-authentication-plugin=mysql_native_password" >>/etc/mysql/my.cnf

    echo "[END] DATABASE INSTALL"
  fi
}

installAutoBackup() {
  MYBACKUP_CONF_FILE="/etc/default/automysqlbackup"
  if [ -f "$MYBACKUP_CONF_FILE" ]; then
    echo "[SKIP] AUTOBACKUP IS ALREADY INSTALLED"
  else
    echo "[START] AUTOBACKUP INSTALL"
    # auto backups
    apt install -y automysqlbackup

    # config backup path
    cp $MYBACKUP_CONF_FILE $MYBACKUP_CONF_FILE.bak.$NOW
    find $MYBACKUP_CONF_FILE -type f -exec sed -i 's|BACKUPDIR="/var/lib/automysqlbackup"|BACKUPDIR="'$BACKUP_PATH'"|g' {} \;

    MY_CONF="/etc/mysql/debian.cnf"
    if [ -f "$MY_CONF" ]; then
      mv $MY_CONF $MY_CONF.bak.$NOW
    fi

    # config mysql
    cat <<-EOF >$MY_CONF
        [client]
        host     = localhost
        user     = root
        password = $MYSQL_ROOT_PASSWORD
        socket   = /var/run/mysqld/mysqld.sock
        [mysql_upgrade]
        host     = localhost
        user     = root
        password = $MYSQL_ROOT_PASSWORD
        socket   = /var/run/mysqld/mysqld.sock
        basedir  = /usr
EOF
    systemctl restart mysql
    echo "[INFO] cron job was place on file : cat /etc/cron.daily/automysqlbackup"
    echo "[END] AUTOBACKUP INSTALL"
  fi

}

installApp() {
  QPILOT_PATH="/var/www/$APP_NAME"
  if [ -d "$QPILOT_PATH" ]; then
    echo "[INFO] QPILOT instance \"$APP_NAME\" IS ALREADY INSTALLED"
    $DIALOG --title "DANGEROUS OPERATION" --clear \
      --yesno "QPILOT is already installed, do you want to re-install all instance $APP_NAME\n\n(NOTICE : THIS INSTALLATION WILL REMOVE ALL DATAS !!!!)" 20 60

    case $? in
    0) installAppExec "true" ;;
    1) echo "[INFO] QPILOT re-install is ignored" && return 0 ;;
    esac

  else
    installAppExec "false"
  fi
}

installAppExec() {
  DROP_DB=$1
  QPILOT_PATH="/var/www/$APP_NAME"
  DB_ADMIN_NAME="qpilot_admin_$APP_NAME"

  echo "[START] INSTALL QPILOT INSTANCE \"$APP_NAME\" on folder $QPILOT_PATH"

  echo "[INFO] generate gitlab rsa key (keep default path and without password)"
  ssh-keygen -t rsa -b 2048

  $DIALOG --title "WARNING" --clear --msgbox "On the next step you have to allow server to read your GITLAB repository (need for pull request).\n\nGo to https://gitlab.com/$GITLAB_REPOSITORY/-/settings/repository on DEPLOY KEYS nav-tab.\n\nPress [OK] when you are on your GITLAB repository configuration" 30 60
  cat /root/.ssh/id_rsa.pub
  echo " "
  read -p "PRESS [ENTER] KEY WHEN YOU HAVE PAST YOUR RSA KEY ON GITLAB REPOSITORY DEPLOY KEY WITH READ PERM to https://gitlab.com/$GITLAB_REPOSITORY/-/settings/repository."

  chmod 400 /root/.ssh/id_rsa

  git config --global core.fileMode false

  if [ -d "$QPILOT_PATH" ]; then
    rm -r $QPILOT_PATH
    a2dissite $DNS_ADMIN || echo '[INFO] apache site not enabled'
    rm -rf /etc/apache2/sites-available/$DNS_ADMIN.conf || echo '[INFO] apache site not created'
    systemctl restart apache2
    # if cerbot certificat is created : its not removed
  fi

  GIT_FOLDER_NAME=${GITLAB_REPOSITORY##*/}

  cd /var/www && git clone git@gitlab.com:$GITLAB_REPOSITORY.git || (echo "[ERROR] permissions deny on GITLAB $GITLAB_REPOSITORY. Installer is aborded" && exit 1)
  mv $GIT_FOLDER_NAME $APP_NAME && cd $APP_NAME && git checkout $GITLAB_BRANCH && git config core.fileMode false

  echo "[INFO] install app dependencies"
  cd $QPILOT_PATH && php composer.phar install && bower install --allow-root

  echo "[INFO] add app db configuration"
  DOCTRINE_FILE="$QPILOT_PATH/config/autoload/doctrine.local.php"
  touch $DOCTRINE_FILE
  # notice : php variable is escaped for prevent conflict with unix variable
  cat <<-EOF >$DOCTRINE_FILE
  <?php

  /**
   * Retrieve the APP_ENV value.
   * Defined in public/.htaccess file.
   */
  \$env = getenv('APP_ENV') ? : 'development';

  return [
      'doctrine' => [
          'connection' => [
              'orm_default' => [
                  'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                  'params' => [
                      'host'     => '127.0.0.1',
                      'port'     => '3306',
                      'user'     => 'root',
                      'password' => '$MYSQL_ROOT_PASSWORD',
                      'dbname'   => '$DB_ADMIN_NAME',
                      'driverOptions' => [
                          1002 => 'SET NAMES utf8'
                      ],
                  ],
              ]
          ],
          'entitymanager' => [
              'orm_default' => [
                  'connection'    => 'orm_default',
                  'configuration' => 'orm_default',
              ]
          ],
          'configuration' => [
              'orm_default' => [
                  'metadata_cache'    => (\$env === 'development' ? 'array' : 'apcu'),
                  'query_cache'       => (\$env === 'development' ? 'array' : 'apcu'),
                  'result_cache'      => (\$env === 'development' ? 'array' : 'apcu'),
                  'driver'            => 'orm_default',
                  'generate_proxies'  => (\$env === 'development'),
                  'proxy_dir'         => 'data/DoctrineORMModule/Proxy',
                  'proxy_namespace'   => 'DoctrineORMModule\Proxy',
                  'filters'           => []
              ]
          ],
          'driver' => [
              'orm_default' => [
                  'class'   => 'Doctrine\ORM\Mapping\Driver\DriverChain'
              ]
          ]
      ],
  ];

EOF

  echo "[INFO] add app bootstrap configuration"
  BOOTSTRAP_FILE="$QPILOT_PATH/module/Bootstrap/config/module.config.php"
  touch $BOOTSTRAP_FILE
  cat <<-EOF >$BOOTSTRAP_FILE
  <?php

  return [
      'admin_host' => '$DNS_ADMIN'
  ];

EOF

  if [ "$DROP_DB" = "true" ]; then
    $DIALOG --title "DANGEROUS OPERATION" --clear \
      --yesno "Remove $DB_ADMIN_NAME database?" 20 60

    case $? in
    0) mysql --user=root --password=$MYSQL_ROOT_PASSWORD -e "DROP DATABASE IF EXISTS $DB_ADMIN_NAME;" ;;
    1) echo "[INFO] database was not deleted" && return 0 ;;
    esac
  fi

  echo "[INFO] create database $DB_ADMIN_NAME"
  mysql --user=root --password=$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE IF NOT EXISTS $DB_ADMIN_NAME CHARACTER SET utf8 COLLATE utf8_general_ci;"

  echo "[INFO] create DDL database"
  chmod +x $QPILOT_PATH/bin/update
  # some cmd is duplicate, this is for test and prevent bugs on deploymnt pipeline
  cd $QPILOT_PATH && php composer.phar dump-autoload && php composer.phar install
  bower install --allow-root # FIXME bower fail due to password on remote github dependencie
  ./bin/update

  echo "[INFO] Init default DB"
  INIT_DB_SQL="/opt/temp_init_db.sql"
  touch $INIT_DB_SQL
  cat <<-EOF >$INIT_DB_SQL
INSERT INTO $DB_ADMIN_NAME.user_role (id, name, rights, createdAt, updatedAt, createdBy_id, updatedBy_id)
VALUES (1, 'Administrateur', '{"user:e:user:c":{"value":true},"user:e:role:c":{"value":true},"project:e:project:c":{"value":false},"user:e:user:r":{"value":true,"owner":"all"},"user:e:role:r":{"value":true,"owner":"all"},"project:e:project:r":{"value":false},"project:e:project:u":{"value":false},"project:e:project:d":{"value":false},"user:e:user:u":{"value":true,"owner":"all"},"user:e:role:u":{"value":true,"owner":"all"},"user:e:user:d":{"value":true,"owner":"all"},"user:e:role:d":{"value":true,"owner":"all"},"admin:e:client:c":{"value":true},"admin:e:client:r":{"value":true,"owner":"all"},"admin:e:client:u":{"value":true,"owner":"all"},"admin:e:client:d":{"value":true,"owner":"all"},"admin:e:network:c":{"value":true},"admin:e:network:r":{"value":true},"admin:e:network:u":{"value":true},"admin:e:network:d":{"value":true}}', null, null, null, null);

INSERT INTO $DB_ADMIN_NAME.user_user (id, role_id, login, password, avatar, email, color, disabled, gender, firstName, lastName, name, createdAt, updatedAt, createdBy_id, updatedBy_id)
VALUES (1, 1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '', '', '', 0, '', '', '', '', null, null, null, null);

EOF

  mysql --user=root --password=$MYSQL_ROOT_PASSWORD --connect-expired-password <$INIT_DB_SQL
  rm -rf $INIT_DB_SQL

  # prevent database change
  ./bin/update

  echo "[INFO] set app unix permissions"
  chown -R www-data:www-data $QPILOT_PATH
  chmod -R 775 $QPILOT_PATH

  # config VHOST
  echo "[INFO] install $DNS_ADMIN vhost"
  CONFIG_FILE="/etc/apache2/sites-available/$DNS_ADMIN.conf"
  touch $CONFIG_FILE
  cat <<-EOF >$CONFIG_FILE
<VirtualHost *:80>
  DocumentRoot "/var/www/$APP_NAME/public"
  ServerName $DNS_ADMIN

  <Directory "/var/www/$APP_NAME/public">
      Options -Indexes +Includes +FollowSymLinks +MultiViews
      AllowOverride all
      Require all granted
  </Directory>

  <Location /manager/>
      ProxyPreserveHost Off
      ProxyPass http://127.0.0.1:10000/ timeout=220
      ProxyPassReverse http://127.0.0.1:10000/
  </Location>

  RewriteEngine on

</VirtualHost>
EOF

  a2ensite $DNS_ADMIN

  # generate certificats
  certbot --apache -d $DNS_ADMIN

  systemctl restart apache2

  curl -I https://$DNS_ADMIN

  echo "[END] INSTALL QPILOT INSTANCE \"$APP_NAME\""
}


installAppCron() {
  $DIALOG --title "OPTIONAL" --clear \
      --yesno "Do you want to application CRON JOB now?" 20 60

    case $? in
    0) installAppCronExec ;;
    1) echo "[INFO] CronJob is not installed" ;;
    esac
}

installAppCronExec() {
  echo "[START] install cronjob"
  QPILOT_PATH="/var/www/$APP_NAME"

  mkdir -p /opt/logs/

  # write out current crontab
  crontab -l > mycron

  # echo new cron into cron file
  # TODO : add logs for other cron?
  echo "0 3 * * * php $QPILOT_PATH/vendor/bin/laminas alert:send-alerts >> /opt/logs/send_alerts_$(date +\%Y\%m\%d\%H\%M\%S).log" >> mycron
  echo "0 1 * * * php $QPILOT_PATH/vendor/bin/laminas age:import-age >> /opt/logs/import_age_$(date +\%Y\%m\%d\%H\%M\%S).log" >> mycron
  echo "0 4 * * * php $QPILOT_PATH/vendor/bin/laminas time:import-calendars >> /opt/logs/import_calendars_$(date +\%Y\%m\%d\%H\%M\%S).log" >> mycron
  echo "0 0 * * SUN php $QPILOT_PATH/vendor/bin/laminas post-parc:sync-post-parc >> /opt/logs/import_age_$(date +\%Y\%m\%d\%H\%M\%S).log" >> mycron

  #install new cron file
  crontab mycron
  rm -rf mycron
  echo "[END] install cronjob"
}

#
# Install webmin tools.
# by default webmin is configured for bind on https://mydns.fr/manager
#
installWebmin() {
  if [ -d "/etc/webmin" ]; then
    echo "[SKIP] WEBMIN IS ALREADY INSTALLED"
  else
    $DIALOG --title "OPTIONAL" --clear \
      --yesno "Do you want to install Webmin?\n\nWebmin is a web tool for help on server management.\n\nCheck your security policy before install Webmin." 20 60

    case $? in
    0) installWebminExec ;;
    1) echo "[INFO] Webmin is not installed" ;;
    esac

  fi
}

installWebminExec() {
  if [ -d "/etc/webmin" ]; then
    echo "[SKIP] WEBMIN IS ALREADY INSTALLED"
  else
    echo "[START] WEBMIN INSTALL"
    echo "deb https://download.webmin.com/download/repository sarge contrib " >>/etc/apt/sources.list
    cd /root &&
      wget https://download.webmin.com/jcameron-key.asc &&
      apt-key add jcameron-key.asc
    apt update && apt install -y webmin

    # disable ssl on local vm
    FTP_SRV_CONF_FILE="/etc/webmin/miniserv.conf"
    cp $FTP_SRV_CONF_FILE $FTP_SRV_CONF_FILE.bak.$NOW

    find $FTP_SRV_CONF_FILE -type f -exec sed -i 's/ssl=1/ssl=0/g' {} \;

    # allow proxy
    WEBMIN_CONF_FILE="/etc/webmin/config"
    cp $WEBMIN_CONF_FILE $WEBMIN_CONF_FILE.bak.$NOW

    echo "webprefix=/manager" >>$WEBMIN_CONF_FILE
    echo "webprefixnoredir=1" >>$WEBMIN_CONF_FILE
    echo "referer=$DNS_ADMIN" >>$WEBMIN_CONF_FILE

    /etc/init.d/webmin restart

    # webmin fail to create systemd service. we create a simple alias as sortcut here : webmin-service
    echo "alias webmin-service=/etc/init.d/webmin" >>/root/.bashrc

    echo "[INFO] test webmin response"
    curl -I http://127.0.0.1:10000/

    systemctl restart apache2
    echo "[END] WEBMIN INSTALL"
  fi
}

installFtp() {
  if [ -d "/etc/proftpd" ]; then
    echo "[SKIP] FTP IS ALREADY INSTALLED"
  else

    echo "[START] INSTALL FTP"
    apt install -y proftpd
    addgroup ftpgroup
    firewall-cmd --add-port=49152-65534/tcp --permanent --zone=public && firewall-cmd --add-port=21/tcp --permanent --zone=public && firewall-cmd --reload
    mkdir /ftpshare/ || echo "[INFO] ftpshare folder already exist"

    systemctl stop proftpd

    FTP_CONF_FILE="/etc/proftpd/proftpd.conf"

    if [ -f "$FTP_CONF_FILE" ]; then
      mv $FTP_CONF_FILE $FTP_CONF_FILE.bak.$NOW
    fi

    touch $FTP_CONF_FILE
    echo "" >$FTP_CONF_FILE
    cat <<-EOF >$FTP_CONF_FILE

Include /etc/proftpd/conf.d/

<IfModule mod_ident.c>
IdentLookups			off
</IfModule>

ServerName			"$APP_NAME"
UseIPv6				off
RootLogin	off
RequireValidShell		off
DefaultRoot			~
PassivePorts                  49152 65534

ServerType				standalone
DeferWelcome			off

MultilineRFC2228		on
DefaultServer			on
ShowSymlinks			on

TimeoutNoTransfer		600
TimeoutStalled			600
TimeoutIdle			1200

DisplayLogin                    welcome.msg
DisplayChdir               	.message true
ListOptions                	"-l"

DenyFilter			\*.*/

Port				21

MaxInstances			30

User				proftpd
Group				nogroup

Umask				022  022

AllowOverwrite			on

TransferLog /var/log/proftpd/xferlog
SystemLog   /var/log/proftpd/proftpd.log

<IfModule mod_quotatab.c>
QuotaEngine off
</IfModule>

<IfModule mod_ratio.c>
Ratios off
</IfModule>

<IfModule mod_delay.c>
DelayEngine on
</IfModule>

<IfModule mod_ctrls.c>
ControlsEngine        on
ControlsMaxClients    2
ControlsLog           /var/log/proftpd/controls.log
ControlsInterval      5
ControlsSocket        /var/run/proftpd/proftpd.sock
</IfModule>

<IfModule mod_ctrls_admin.c>
AdminControlsEngine on
</IfModule>

<Limit LOGIN>
    DenyGroup !ftpgroup
</Limit>

Include /etc/proftpd/conf.d/
EOF

    systemctl restart proftpd
    # systemctl status proftpd
    echo "[END] INSTALL FTP"
  fi
}

installHelper() {
  HELPER_FILE_SOURCE="$CURRENT_PATH/helper.sh"
  HELPER_FILE="/opt/helper.sh"
  if [ -f "$HELPER_FILE_SOURCE" ]; then
    if [ -f "$HELPER_FILE" ]; then
      echo "[SKIP] HELPER IS ALREADY INSTALLED on $HELPER_FILE"
    else
      echo "[START] INSTALL HELPER"
      cp $HELPER_FILE_SOURCE/helper.sh $HELPER_FILE
      echo "[INFO] running helper configuration"
      # replace QPILOT word by APP_NAME (notice: QPILOT word hard encoded
      find $HELPER_FILE -type f -exec sed -i 's|APP_NAME="qpilot"|APP_NAME="'$APP_NAME'"|g' {} \;
      chmod +x $HELPER_FILE
      echo "[END] INSTALL HELPER"
    fi
  else
    if [ -f "$HELPER_FILE" ]; then
      echo "[SKIP] HELPER IS ALREADY INSTALLED on $HELPER_FILE"
    else
      $DIALOG --title "ERROR" --clear --msgbox "Helper file is missing on $HELPER_FILE_SOURCE you have to install it manually on location $HELPER_FILE \nPress [OK] to ignore this error and continue" 10 60
    fi
  fi
}

#
# MAIN ENTRY
#
echo "=========================================================================="
echo "=========================================================================="
echo "[START] SCRIPT INSTALLATION ON DEBIAN 11"
echo "=========================================================================="
echo "=========================================================================="

checkRequirements
inputVariables
installTools
configUnixUser
installFirewall
installFailToBan
installSendmail
installApache
installCertbot
installNpmBower
installDatabase
installAutoBackup
installApp
installAppCron
installFtp
installHelper
installWebmin

$DIALOG --title "NOTICE" --clear --msgbox "You have to change Qpilot admin account \"admin/admin\" after on your new instance $DNS_ADMIN" 30 60

echo "=========================================================================="
echo "=========================================================================="
echo "                [END] SCRIPT INSTALLATION ON DEBIAN 11                    "
echo "=========================================================================="
echo "=========================================================================="
echo "                                                * check installation logs "
echo "=========================================================================="
