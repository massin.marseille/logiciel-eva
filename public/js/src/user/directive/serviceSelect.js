(function(app) {

    app.directive('serviceSelect', [
        'serviceService', 'selectFactoryService',
        function serviceSelect(serviceService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: serviceService
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
