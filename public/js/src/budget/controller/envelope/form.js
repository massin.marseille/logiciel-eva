(function(app) {

    app.controller('envelopeFormController', [
        'envelopeService', 'flashMessenger', 'translator',
        function envelopeFormController(envelopeService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'code',
                    'name',
                    'financer.id',
                    'financer.name',
                    'start',
                    'end',
                    'amount',
                    'keywords'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.envelope = {
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    envelopeService.find(id, apiParams).then(function (res) {
                        self.envelope   = res.object;
                        self.panelTitle = self.envelope.name;
                    });
                }
            };

            self.saveEnvelope = function saveEnvelope() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.envelope.id;

                envelopeService.save(self.envelope, apiParams).then(function (res) {
                    self.envelope     = res.object;
                    self.panelTitle   = self.envelope.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.envelope.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('envelope_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteEnvelope = function deleteEnvelope() {
                if (confirm(translator('envelope_question_delete').replace('%1', self.envelope.name))) {
                    envelopeService.remove(self.envelope).then(function (res) {
                        if (res.success) {
                            window.location = '/budget/envelope';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ]);

})(window.EVA.app);
