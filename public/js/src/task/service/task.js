(function(app) {

    app.factory('taskService', [
        'restFactoryService',
        function taskService(restFactoryService) {
            return restFactoryService.create('/api/task');
        }
    ])

})(window.EVA.app);
