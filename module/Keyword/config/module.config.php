<?php

return [
    'module' => [
        'keyword' => [
            'environments' => [
                'admin', 'client'
            ],

            'active'   => true,
            'required' => false,

            'acl' => [
                'entities' => [
                    [
                        'name'  => 'group',
                        'class' => 'Keyword\Entity\Group',
                        'owner'    => ['service']
                    ],
                    [
                        'name'  => 'keyword',
                        'class' => 'Keyword\Entity\Keyword',
                        'owner'    => []
                    ]
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
