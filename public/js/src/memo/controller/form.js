(function(app) {

    app.controller('memoController', [
        'memoService', '$scope',
        function (memoService, $scope) {
            var self = this;
            self.memo = {
                content: ''
            };
            memoService.findAll({
                col: [
                    'content'
                ]
            }).then(function(res) {
                self.memo = res.object;

                var timeout = null;

                $scope.$watch('self.memo.content', function () {
                    clearTimeout(timeout);

                    timeout = setTimeout(function () {
                        memoService.save(self.memo, {
                            col: [
                                'content'
                            ]
                        }).then(function(res) {

                        })
                    }, 500);


                })
            });
        }
    ]);

})(window.EVA.app);
