<?php

namespace Admin\Controller\Migration;

use Advancement\Entity\Advancement;
use Attachment\Core\Uploader;
use Attachment\Entity\Attachment;
use Budget\Entity\Account;
use Budget\Entity\Envelope;
use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Directory\Entity\Contact;
use Directory\Entity\Structure;
use Field\Entity\Field;
use Field\Entity\Value;
use Home\Entity\Home;
use Indicator\Entity\Indicator;
use Indicator\Entity\Measure;
use Map\Entity\Territory;
use Project\Entity\Actor;
use Project\Entity\Member;
use Keyword\Entity\Association;
use Keyword\Entity\Group;
use Keyword\Entity\Keyword;
use Project\Entity\Project;
use Time\Entity\Bookmark;
use Time\Entity\Timesheet;
use User\Entity\User;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class MigrationController extends AbstractActionController
{
    protected $db;
    protected $dbRH;
    protected $urlDocuments;
    protected $clean = true;

    public function __construct()
    {
        $this->db = new \PDO('mysql:dbname=eva3_;host=localhost', 'root', '');
        $this->dbRH = new \PDO('mysql:dbname=e-agenda-eva;host=localhost', 'root', '');
        $this->urlDocuments = 'http://evaparc.net';
    }

    protected function log($line, &$log)
    {
        $log .= '<br />(' . date('H:i:s') . ') ' . $line;
    }

    protected function query($sql, $params = [], $mode = 'fetchAll')
    {
        $statement = $this->db->prepare($sql);
        $statement->execute($params);
        $rows = $statement->{$mode}(\PDO::FETCH_ASSOC);

        if ($mode == 'fetchAll') {
            foreach ($rows as $i => $row) {
                foreach ($row as $j => $col) {
                    $col = str_replace("\'", "'", $col);
                    $col = str_replace("\\\"", "\"", $col);
                    $rows[$i][$j] = mb_convert_encoding($col, 'UTF-8', mb_detect_encoding($col, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
                }
            }
        } else {
            foreach ($rows as $j => $col) {
                $col = str_replace("\'", "'", $col);
                $col = str_replace("\\\"", "\"", $col);
                $rows[$j] = mb_convert_encoding($col, 'UTF-8', mb_detect_encoding($col, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
            }
        }

        return $rows;
    }

    protected function queryRH($sql, $params = [], $mode = 'fetchAll')
    {
        $statement = $this->dbRH->prepare($sql);
        $statement->execute($params);
        $rows = $statement->{$mode}(\PDO::FETCH_ASSOC);

        if ($mode == 'fetchAll') {
            foreach ($rows as $i => $row) {
                foreach ($row as $j => $col) {
                    $col = str_replace("\'", "'", $col);
                    $col = str_replace("\\\"", "\"", $col);
                    $rows[$i][$j] = mb_convert_encoding($col, 'UTF-8', mb_detect_encoding($col, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
                }
            }
        } else {
            foreach ($rows as $j => $col) {
                $col = str_replace("\'", "'", $col);
                $col = str_replace("\\\"", "\"", $col);
                $rows[$j] = mb_convert_encoding($col, 'UTF-8', mb_detect_encoding($col, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
            }
        }

        return $rows;
    }

    protected function convertDate($str)
    {
        if (strlen($str) == '10') {
            if ($str == '0000-00-00') {
                $str = '1960-01-01';
            }
            $date = \DateTime::createFromFormat('Y-m-d', $str);
            if ($date) {
                return $date;
            }
        }

        return null;
    }

    public function indexAction()
    {
        $parcs   = $this->query('SELECT * FROM `parc` ORDER BY `nom_parc` ASC;');
        $clients = $this->entityManager()->getRepository('Bootstrap\Entity\Client')->findBy([], ['name' => 'asc']);
        $log     = null;

        if ($this->getRequest()->isPost()) {
            ini_set('memory_limit', -1);
            ini_set('max_execution_time', -1);
            $log = $this->migrate();
        }

        return new ViewModel([
            'parcs'   => $parcs,
            'clients' => $clients,
            'log'     => $log
        ]);
    }

    public function migrate()
    {
        $idParc = $this->params()->fromPost('parc');
        $parc   = $this->query('SELECT * FROM `parc` WHERE `id_parc` = :id;', ['id' => $idParc], 'fetch');
        $client = $this->entityManager()->getRepository('Bootstrap\Entity\Client')->find($this->params()->fromPost('client'));

        $em = $this->serviceLocator->get('EntityManagerFactory')->getEntityManager($client);

        $log  = 'Migration <b>' . $parc['nom_parc'] . '</b> vers <b>' . $client->getName() . '</b> : <br />';
        $this->log('Start <br />', $log);

        // Migration des mot clefs - ok (sans associations)
        $this->migrateKeywords($em, $idParc, $log);

        // Migration des indicateurs - ok
        $this->migrateIndicators($em, $idParc, $log);

        // Migration des champs perso. - ok
        $this->migrateFields($em, $idParc, $log);

        // Migration des utilisateurs - ok
        $this->migrateUsers($em, $idParc, $log);

        // Migration des contacts - ok
        $this->migrateContacts($em, $idParc, $log);

        // Migration des structures - ok
        $this->migrateStructures($em, $idParc, $log);

        // Migration des comptes - ok ~
        $this->migrateAccounts($em, $idParc, $log);

        // Migration des enveloppes - ok ~
        $this->migrateEnvelopes($em, $idParc, $log);

        // Migration des territoires - ok
        $this->migrateTerritories($em, $idParc, $log);

        // Migration des perimetres - ok
        $this->migratePerimeters($em, $idParc, $log);

        // Migration des fiches - en cours
        $this->migrateProjects($em, $idParc, $log);

        // Download documents
        $this->downloadDocuments($em, $idParc, $log, $client);

        $this->customPNRPREALPES($em, $idParc, $log, $client);

        // Recompute levels
        $recomputeConfiguration = false;
        $maxLevel = 0;

        $projects = $em->getRepository('Project\Entity\Project')->findAll();
        foreach ($projects as $project) {
            $level = $project->computeLevel();
            if ($level > $maxLevel) {
                $maxLevel = $level;
            }
            $recomputeConfiguration = true;
        }

        if ($recomputeConfiguration) {
            $configuration = $em->getRepository('Application\Entity\Configuration')->find(1);
            $modules = $configuration->getModules();
            if (!isset($modules['project']['levels'])) {
                $modules['project']['levels'] = [];
            }

            $baseMaxLevel = $em->getRepository('Project\Entity\Project')->findMaxLevel();
            if ($baseMaxLevel > $maxLevel) {
                $maxLevel = $baseMaxLevel;
            }

            for ($i = 0; $i <= $maxLevel; $i++) {
                if (!isset($modules['project']['levels'][$i])) {
                    $modules['project']['levels'][$i] = [
                        'name' => 'Niveau ' . $i
                    ];
                }
            }

            $configuration->setModules($modules);
        }

        $em->flush();
        $em->clear();

        $this->log('End', $log);

        return $log;
    }

    protected function migrateKeywords($em, $parc, &$log)
    {
        $this->log('<b>Mot clefs</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Keyword\Entity\Association a')->execute();
            $em->createQuery('DELETE FROM Keyword\Entity\Keyword k')->execute();
            $em->createQuery('DELETE FROM Keyword\Entity\Group g')->execute();
        }

        $metadata = $em->getClassMetaData('Keyword\Entity\Group');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $metadataKw = $em->getClassMetaData('Keyword\Entity\Keyword');
        $metadataKw->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadataKw->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $descripteurs = $this->query('SELECT * FROM `descripteur` WHERE `id_parc` = :parc AND `id_secteur` != 3 AND `code_analytique` = 0', ['parc' => $parc]);
        $this->log(sizeof($descripteurs) . ' rows trouvés<br />', $log);

        $parents = [];

        foreach ($descripteurs as $_descripteur) {
            $group = new Group();
            $group->setId($_descripteur['id_descripteur']);
            $group->setName($_descripteur['nom_descripteur']);
            $group->setMultiple($_descripteur['type'] === 'multi');

            $group->setType($_descripteur['id_secteur'] == 1 ? Group::GROUP_TYPE_REFERENCE : Group::GROUP_TYPE_KEYWORD);

            if ($group->getType() === Group::GROUP_TYPE_KEYWORD) {
                $entities = [];
                $modules = $this->query('SELECT * FROM `module_descripteur` WHERE `id_descripteur` = :descripteur', ['descripteur' => $_descripteur['id_descripteur']]);
                foreach ($modules as $_module) {
                    if ($_module['id_module'] == '1') {
                        $entities[] = 'contact';
                    } elseif ($_module['id_module'] == '2') {
                        $entities[] = 'structure';
                    } elseif ($_module['id_module'] == '5') {
                        $entities[] = 'project';
                    } elseif ($_module['id_module'] == '8') {
                        $entities[] = 'project';
                    } elseif ($_module['id_module'] == '9') {
                        $entities[] = 'project';
                    } elseif ($_module['id_module'] == '10') {
                        $entities[] = 'indicator';
                    } elseif ($_module['id_module'] == '11') {
                        $entities[] = 'project';
                    } elseif ($_module['id_module'] == '15') {
                        $entities[] = 'timesheet';
                    } elseif ($_module['id_module'] == '16') {
                        $entities[] = 'envelope';
                    } elseif ($_module['id_module'] == '19') {
                        $entities[] = 'actor';
                    } elseif ($_module['id_module'] == '22') {
                        $entities[] = 'advancement';
                    } elseif ($_module['id_module'] == '22') {
                        $entities[] = 'advancement';
                    } elseif ($_module['id_module'] == '26') {
                        $entities[] = 'posteExpense';
                    } elseif ($_module['id_module'] == '27') {
                        $entities[] = 'posteIncome';
                    } elseif ($_module['id_module'] == '28') {
                        $entities[] = 'expense';
                    } elseif ($_module['id_module'] == '29') {
                        $entities[] = 'income';
                    }
                }

                $group->setEntities(array_unique($entities));
            } else {
                $group->setEntities(['project', 'indicator']);
            }

            $elements = $this->query('SELECT * FROM `element` WHERE `id_descripteur` = :descripteur', ['descripteur' => $_descripteur['id_descripteur']]);

            foreach ($elements as $_element) {
                $keyword = new Keyword();
                $keyword->setId($_element['id_element']);
                $keyword->setName($_element['nom_element']);
                $keyword->setOrder($_element['emplacement']);
                $keyword->setDescription($_element['descriptif']);
                $keyword->setGroup($group);
                $group->getKeywords()->add($keyword);

                if ($_element['id_parent'] !== '' && $_element['id_parent'] !== '0' && $_element['id_parent']) {
                    $parents[$_element['id_element']] = $_element['id_parent'];
                }


                $em->persist($keyword);
            }


            $em->persist($group);

            $em->flush();
            $em->clear();
        }

        foreach ($parents as $keyword => $parent) {
            $kw   = $em->getRepository('Keyword\Entity\Keyword')->find($keyword);
            $par  = $em->getRepository('Keyword\Entity\Keyword')->find($parent);
            if ($kw && $par) {
                $par->getChildren()->add($kw);
            }
        }

        $em->flush();
        $em->clear();

        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_AUTO);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\IdentityGenerator());

        $metadataKw->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_AUTO);
        $metadataKw->setIdGenerator(new \Doctrine\ORM\Id\IdentityGenerator());
    }

    protected function migrateUsers($em, $parc, &$log)
    {
        $this->log('<b>Utilisateurs</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Time\Entity\Bookmark b')->execute();
            $em->createQuery('DELETE FROM Time\Entity\Timesheet t')->execute();
            $em->createQuery('DELETE FROM Home\Entity\Home h')->execute();
            $em->createQuery('DELETE FROM User\Entity\User u')->execute();
        }

        $cache = [
            'users'    => [],
            'keywords' => []
        ];

        $users = $this->query('SELECT * FROM `contact` WHERE `login` != \'\' AND `id_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($users) . ' rows trouvés<br />', $log);

        $role = $em->getRepository('User\Entity\Role')->find(1);

        foreach ($users as $_user) {
            $user = new User();
            $user->setId($_user['id_contact']);
            $user->setGender($_user['civilite'] === 'Mademoiselle' || $_user['civilite'] === 'Madame' ? 'mme' : 'm');
            $user->setEmail($_user['email']);
            $user->setFirstName($_user['prenom']);
            $user->setLastName($_user['nom']);
            $user->setLogin($_user['login']);
            $user->setPassword($_user['login']);
            $user->setRole($role);

            $em->persist($user);

            $cache['users'][$user->getId()] = $user;
        }

        $metadata = $em->getClassMetaData('User\Entity\User');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $em->flush();

        // Mot clefs
        $this->log('<b>Mots clefs utilisateurs</b>', $log);
        $associations = $this->query('SELECT * FROM `contact_element` WHERE `id_contact` IN ( SELECT `id_contact` FROM `contact` WHERE `login` != \'\' AND id_parc = :parc );', ['parc' => $parc]);
        $this->log(sizeof($associations) . ' rows trouvés<br />', $log);

        foreach ($associations as $_association) {
            if (isset($cache['users'][$_association['id_contact']])) {
                if (!isset($cache['keywords'][$_association['id_element']])) {
                    $cache['keywords'][$_association['id_element']] = $em->getRepository('Keyword\Entity\Keyword')->find($_association['id_element']);
                }

                $keyword = $cache['keywords'][$_association['id_element']];

                if ($keyword) {
                    $association = new Association();
                    $association->setEntity('User\Entity\User');
                    $association->setKeyword($keyword);
                    $association->setPrimary($_association['id_contact']);

                    $em->persist($association);
                }
            }
        }

        $em->flush();
        $em->clear();
    }

    protected function migrateIndicators($em, $parc, &$log)
    {
        $this->log('<b>Indicateurs</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Indicator\Entity\Measure m')->execute();
            $em->createQuery('DELETE FROM Indicator\Entity\Indicator i')->execute();
        }

        $cache = [
            'indicators' => [],
            'keywords'   => []
        ];

        $indicators = $this->query('SELECT * FROM `indicateur` WHERE `id_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($indicators) . ' rows trouvés<br />', $log);

        foreach ($indicators as $_indicator) {
            $indicator = new Indicator();
            $indicator->setId($_indicator['id_indicateur']);
            $indicator->setName($_indicator['nom_indicateur']);
            $indicator->setOperator(Indicator::OPERATOR_SUM);
            $indicator->setType(($_indicator['type_valeur'] == 'libre' ? Indicator::TYPE_FREE : Indicator::TYPE_FIXED));
            $indicator->setDescription($_indicator['commentaire']);

            if ($_indicator['type_valeur'] == 'liste') {
                $values = $this->query('SELECT * FROM `indicateur_valeur` WHERE `id_indicateur` = :id', ['id' => $_indicator['id_indicateur']]);
                $v = [];
                foreach ($values as $value) {
                    $v[] = [
                        'value' => $value['valeur_num'],
                        'text'  => $value['valeur_text'],
                        'color' => '#' . $value['code_couleur'],
                    ];
                }
                $indicator->setValues($v);
            }

            $em->persist($indicator);

            $cache['indicators'][$indicator->getId()] = $indicator;
        }

        $metadata = $em->getClassMetaData('Indicator\Entity\Indicator');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $em->flush();

        // Mot clefs
        $this->log('<b>Mots clefs indicateurs</b>', $log);
        $associations = $this->query('SELECT * FROM `indicateur_element` WHERE `id_indicateur` IN ( SELECT `id_indicateur` FROM `indicateur` WHERE id_parc = :parc );', ['parc' => $parc]);
        $this->log(sizeof($associations) . ' rows trouvés<br />', $log);

        foreach ($associations as $_association) {
            if (isset($cache['indicators'][$_association['id_indicateur']])) {
                if (!isset($cache['keywords'][$_association['id_element']])) {
                    $cache['keywords'][$_association['id_element']] = $em->getRepository('Keyword\Entity\Keyword')->find($_association['id_element']);
                }

                $keyword = $cache['keywords'][$_association['id_element']];

                if ($keyword) {
                    $association = new Association();
                    $association->setEntity('Indicator\Entity\Indicator');
                    $association->setKeyword($keyword);
                    $association->setPrimary($_association['id_indicateur']);

                    $em->persist($association);
                }
            }
        }

        $em->flush();
        $em->clear();
    }

    protected function migrateFields($em, $parc, &$log)
    {
        $this->log('<b>Champs personnalisables</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Field\Entity\Value v')->execute();
            $em->createQuery('DELETE FROM Field\Entity\Field f')->execute();
            $em->createQuery('DELETE FROM Field\Entity\Group g')->execute();
        }

        $metadata = $em->getClassMetaData('Field\Entity\Group');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $metadataField = $em->getClassMetaData('Field\Entity\Field');
        $metadataField->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadataField->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());


        $formulaires = $this->query('SELECT * FROM `formulaire` WHERE `id_aut_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($formulaires) . ' rows trouvés<br />', $log);

        foreach ($formulaires as $_formulaire) {
            $group = new \Field\Entity\Group();
            $group->setId($_formulaire['id_formulaire']);
            $group->setName($_formulaire['nom']);
            $group->setEntity('Project\Entity\Project');
            $group->setEnabled($_formulaire['statut'] == 'o');

            $champs = $this->query('SELECT * FROM `formulaire_champs` WHERE `id_formulaire` = :id ORDER BY n_emplacement ASC', ['id' => $_formulaire['id_formulaire']]);
            foreach ($champs as $_champ) {
                if ($_champ['type_champs']) {
                    $field = new Field();
                    $field->setId($_champ['id_champs']);
                    $field->setName(html_entity_decode(strip_tags($_champ['nom_champs'])));
                    $field->setHelp(html_entity_decode(strip_tags($_champ['descriptif_champs'])));

                    $type = '';
                    $meta = [];
                    if ($_champ['type_champs'] == 'ligne') {
                        $type = Field::TYPE_TEXT;
                    } elseif ($_champ['type_champs'] == 'text') {
                        $type = Field::TYPE_TEXTAREA;
                    } elseif ($_champ['type_champs'] == 'select') {
                        $type = Field::TYPE_SELECT;
                        $xml = simplexml_load_string($_champ['option_champ']);
                        $meta['values'] = [];
                        if ($xml) {
                            foreach ($xml->option as $option) {
                                $meta['values'][] = ['v' => $option['valeur']];
                            }
                        }
                    } elseif ($_champ['type_champs'] == 'radio') {
                        $type = Field::TYPE_RADIO;
                        $xml = simplexml_load_string($_champ['option_champ']);
                        $meta['values'] = [];
                        if ($xml) {
                            foreach ($xml->option as $option) {
                                $meta['values'][] = ['v' => $option['valeur'], 'i' => $option['id']];
                            }
                        }
                    }

                    if ($type) {
                        if ($_champ['obligatoire'] == 'O') {
                            $meta['required'] = true;
                        }

                        $field->setType($type);
                        $field->setMeta($meta);

                        $field->setGroup($group);
                        $group->getFields()->add($field);

                        $em->persist($field);
                    }
                }
            }

            $em->persist($group);
        }

        $em->flush();
        $em->clear();

        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_AUTO);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\IdentityGenerator());

        $metadataField->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_AUTO);
        $metadataField->setIdGenerator(new \Doctrine\ORM\Id\IdentityGenerator());
    }

    protected function migrateContacts($em, $parc, &$log)
    {
        $this->log('<b>Contacts</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Directory\Entity\Contact c')->execute();
        }

        $cache = [
            'contacts' => [],
            'keywords' => []
        ];

        $contacts = $this->query('SELECT * FROM `contact` WHERE (`login` = \'\' OR `login` IS NULL) AND `id_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($contacts) . ' rows trouvés<br />', $log);

        foreach ($contacts as $_contact) {
            $contact = new Contact();
            $contact->setId($_contact['id_contact']);
            $contact->setGender($_contact['civilite'] === 'Mademoiselle' || $_contact['civilite'] === 'Madame' ? 'mme' : 'm');
            $contact->setEmail($_contact['email']);
            $contact->setFirstName($_contact['prenom']);
            $contact->setLastName($_contact['nom']);
            $contact->setPhone($_contact['portable']);

            $em->persist($contact);

            $cache['contacts'][$contact->getId()] = $contact;
        }

        $metadata = $em->getClassMetaData('Directory\Entity\Contact');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $em->flush();

        // Mot clefs
        $this->log('<b>Mots clefs contacts</b>', $log);
        $associations = $this->query('SELECT * FROM `contact_element` WHERE `id_contact` IN ( SELECT `id_contact` FROM `contact` WHERE (`login` = \'\' OR `login` IS NULL) AND id_parc = :parc );', ['parc' => $parc]);
        $this->log(sizeof($associations) . ' rows trouvés<br />', $log);

        foreach ($associations as $_association) {
            if (isset($cache['contacts'][$_association['id_contact']])) {
                if (!isset($cache['keywords'][$_association['id_element']])) {
                    $cache['keywords'][$_association['id_element']] = $em->getRepository('Keyword\Entity\Keyword')->find($_association['id_element']);
                }

                $keyword = $cache['keywords'][$_association['id_element']];

                if ($keyword) {
                    $association = new Association();
                    $association->setEntity('Directory\Entity\Contact');
                    $association->setKeyword($keyword);
                    $association->setPrimary($_association['id_contact']);

                    $em->persist($association);
                }
            }
        }

        $em->flush();
        $em->clear();
    }

    protected function migrateStructures($em, $parc, &$log)
    {
        $this->log('<b>Structures</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Budget\Entity\Income i')->execute();
            $em->createQuery('DELETE FROM Budget\Entity\PosteIncome p')->execute();
            $em->createQuery('DELETE FROM Budget\Entity\Expense e')->execute();
            $em->createQuery('DELETE FROM Budget\Entity\PosteExpense p')->execute();
            $em->createQuery('DELETE FROM Budget\Entity\Envelope e')->execute();
            $em->createQuery('DELETE FROM Project\Entity\Actor a')->execute();
            $em->createQuery('DELETE FROM Directory\Entity\Structure s')->execute();
        }

        $cache = [
            'structures' => [],
            'keywords'   => []
        ];

        $structures = $this->query('SELECT * FROM `structure_parent` WHERE `id_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($structures) . ' rows trouvés<br />', $log);

        foreach ($structures as $_structure) {

            $structure = new Structure();
            $structure->setId($_structure['id_structure']);
            $structure->setName($_structure['nom_structure']);
            $structure->setSigle($_structure['sigle']);

            $instances = $this->query('SELECT * FROM `structure_instance` WHERE `id_structure` = :id', ['id' => $_structure['id_structure']]);
            foreach ($instances as $instance) {
                $structure->setEmail($instance['mail']);
                $structure->setPhone($instance['tel']);
                $structure->setPhone2($instance['tel2']);
                $structure->setAddressLine1($instance['adresse1']);
                $structure->setAddressLine2($instance['adresse2']);
                $structure->setZipcode($instance['cp']);
                $structure->setCity($instance['ville']);
                $structure->setCountry($instance['pays']);
            }

            $em->persist($structure);

            $cache['structures'][$structure->getId()] = $structure;
        }

        $metadata = $em->getClassMetaData('Directory\Entity\Structure');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $em->flush();

        // Mot clefs
        $this->log('<b>Mots clefs structures</b>', $log);
        $associations = $this->query('SELECT E.*, I.id_structure FROM `structure_element` E INNER JOIN `structure_instance` I ON I.`id_instance` = E.`id_instance` WHERE I.`id_structure` IN ( SELECT `id_structure` FROM `structure_parent` WHERE id_parc = :parc );', ['parc' => $parc]);
        $this->log(sizeof($associations) . ' rows trouvés<br />', $log);

        foreach ($associations as $_association) {
            if (isset($cache['structures'][$_association['id_structure']])) {
                if (!isset($cache['keywords'][$_association['id_element']])) {
                    $cache['keywords'][$_association['id_element']] = $em->getRepository('Keyword\Entity\Keyword')->find($_association['id_element']);
                }

                $keyword = $cache['keywords'][$_association['id_element']];

                if ($keyword) {
                    $association = new Association();
                    $association->setEntity('Directory\Entity\Structure');
                    $association->setKeyword($keyword);
                    $association->setPrimary($cache['structures'][$_association['id_structure']]->getId());

                    $em->persist($association);
                }
            }
        }

        $em->flush();

        $em->clear();
    }

    protected function migrateAccounts($em, $parc, &$log)
    {
        $this->log('<b>Comptes</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Budget\Entity\Account a')->execute();
        }

        $metadata = $em->getClassMetaData('Budget\Entity\Account');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $descripteurs = $this->query('SELECT * FROM `descripteur` WHERE `id_parc` = :parc AND `id_secteur` = 5', ['parc' => $parc]);


        $parents = [];

        foreach ($descripteurs as $descripteur) {
            $accounts = $this->query('SELECT * FROM `element` WHERE `id_descripteur` = :descripteur', ['descripteur' => $descripteur['id_descripteur']]);
            $this->log(sizeof($accounts) . ' rows trouvés<br />', $log);

            foreach ($accounts as $_account) {
                $account = new Account();
                $account->setId($_account['id_element']);
                $account->setName($_account['nom_element']);
                $account->setCode($_account['nom_element']);
                $account->setDescription($_account['descriptif']);
                $account->setType(Account::TYPE_EXPENSE);

                if ($_account['id_parent'] !== '' && $_account['id_parent'] !== '0' && $_account['id_parent']) {
                    $parents[$_account['id_element']] = $_account['id_parent'];
                }

                $em->persist($account);
            }
        }

        $em->flush();
        $em->clear();

        foreach ($parents as $account => $parent) {
            $acc  = $em->getRepository('Budget\Entity\Account')->find($account);
            $par  = $em->getRepository('Budget\Entity\Account')->find($parent);
            if ($acc && $par) {
                $acc->setParent($par);
            }
        }

        $em->flush();
        $em->clear();

        $accounts = $em->getRepository('Budget\Entity\Account')->findBy([
            'parent' => null
        ]);
        foreach ($accounts as $account) {
            if (in_array($account->getCode(), ['Dépense', 'Dépenses', 'dépense', 'dépenses'])) {
                $account->setTypeRecursive(Account::TYPE_EXPENSE);
            } elseif (in_array($account->getCode(), ['Recette', 'Recettes', 'recette', 'recettes'])) {
                $account->setTypeRecursive(Account::TYPE_INCOME);
            } else {
                $em->remove($account);
            }
        }

        $em->flush();
        $em->clear();

        $accounts = $em->getRepository('Budget\Entity\Account')->findBy([
            'parent' => null
        ]);
        foreach ($accounts as $account) {
            $em->remove($account);
        }

        $em->flush();
        $em->clear();
    }

    protected function migrateEnvelopes($em, $parc, &$log)
    {
        $this->log('<b>Enveloppes</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Budget\Entity\Envelope e')->execute();
        }

        $metadata = $em->getClassMetaData('Budget\Entity\Envelope');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $envelopes = $this->query('SELECT E.*, SI.id_structure FROM `enveloppe` E INNER JOIN `structure_instance` SI ON SI.id_instance = E.id_instance WHERE `id_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($envelopes) . ' rows trouvés<br />', $log);

        $cache = [
            'envelopes' => [],
            'keywords'  => []
        ];

        foreach ($envelopes as $_envelope) {
            $envelope = new Envelope();
            $envelope->setId($_envelope['id_enveloppe']);
            $envelope->setCode($_envelope['code']);
            $envelope->setName($_envelope['titre_convention']);
            $envelope->setAmount($_envelope['montant']);
            if ($_envelope['fin_engagement'] != '0000-00-00') {
                $envelope->setStart($this->convertDate($_envelope['fin_engagement']));
            }

            if ($_envelope['fin_paiement'] != '0000-00-00') {
                $envelope->setEnd($this->convertDate($_envelope['fin_paiement']));
            }

            $structure = $em->getRepository('Directory\Entity\Structure')->find($_envelope['id_structure']);
            if ($structure) {
                $envelope->setFinancer($structure);
            }

            $em->persist($envelope);

            $cache['envelopes'][$envelope->getId()] = $envelope;
        }

        $em->flush();
        $em->clear();
    }

    protected function migrateTerritories($em, $parc, &$log)
    {
        $this->log('<b>Territoires</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Map\Entity\Association a')->execute();
            $em->createQuery('DELETE FROM Map\Entity\Association a')->execute();
            $em->createQuery('DELETE FROM Map\Entity\Territory e')->execute();
        }

        $metadata = $em->getClassMetaData('Map\Entity\Territory');
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());

        $territoires = $this->query('SELECT * FROM `territoire` WHERE `id_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($territoires) . ' rows trouvés<br />', $log);

        foreach ($territoires as $_territoire) {
            $territory = new Territory();
            $territory->setId($_territoire['id_territoire']);
            $territory->setCode($_territoire['code_insee']);
            $territory->setName($_territoire['nom_territoire']);

            $em->persist($territory);
        }

        $em->flush();
        $em->clear();

        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_AUTO);
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\IdentityGenerator());
    }

    protected function migratePerimeters($em, $parc, &$log)
    {
        $this->log('<b>Perimètres</b>', $log);
        $descripteurs = $this->query('SELECT * FROM `descripteur` WHERE `id_parc` = :parc AND `id_secteur` = 3;', ['parc' => $parc]);
        $this->log(sizeof($descripteurs) . ' rows trouvés<br />', $log);

        $parents = [];

        foreach ($descripteurs as $_descripteur) {
            $group = new Territory();
            $group->setName($_descripteur['nom_descripteur']);

            $elements = $this->query('SELECT * FROM `element` WHERE `id_descripteur` = :descripteur', ['descripteur' => $_descripteur['id_descripteur']]);

            foreach ($elements as $_element) {
                $keyword = new Territory();
                $keyword->setName($_element['nom_element']);
                $keyword->setCode('bascule_e_' . $_element['id_element']);

                if ($_element['id_parent'] !== '' && $_element['id_parent'] !== '0' && $_element['id_parent']) {
                    $parents[$_element['id_element']] = $_element['id_parent'];
                } else {
                    $keyword->getParents()->add($group);
                    $group->getChildren()->add($keyword);
                }


                $em->persist($keyword);
            }


            $em->persist($group);

            $em->flush();
        }

        foreach ($parents as $keyword => $parent) {
            $kw   = $em->getRepository('Map\Entity\Territory')->findOneByCode('bascule_e_' . $keyword);
            $par  = $em->getRepository('Map\Entity\Territory')->findOneByCode('bascule_e_' . $parent);
            if ($kw && $par) {
                $par->getChildren()->add($kw);
                $kw->getParents()->add($par);
            }
        }

        $em->flush();


        // Mot clefs
        $associations = $this->query('SELECT * FROM `territoire_element` WHERE `id_territoire` IN ( SELECT `id_territoire` FROM `territoire` WHERE id_parc = :parc );', ['parc' => $parc]);
        foreach ($associations as $_association) {
            $territoire = $em->getRepository('Map\Entity\Territory')->find($_association['id_territoire']);
            $element    = $em->getRepository('Map\Entity\Territory')->findOneByCode('bascule_e_' . $_association['id_element']);
            if ($territoire && $element) {
                $element->getChildren()->add($territoire);
                $territoire->getParents()->add($element);
            }
        }
        $em->flush();

        $em->clear();
    }

    protected function migrateProjects($em, $parc, &$log)
    {
        if ($this->clean) {
            $em->createQuery('DELETE FROM Project\Entity\Project p')->execute();
            $em->createQuery('DELETE FROM Attachment\Entity\Attachment a')->execute();
        }

        $group1 = new \Field\Entity\Group();
        $group1->setName('Evaluation');
        $group1->setEntity('Project\Entity\Project');
        $group1->setEnabled(true);

        $group2 = new \Field\Entity\Group();
        $group2->setName('Indicateurs');
        $group2->setEntity('Project\Entity\Project');
        $group2->setEnabled(true);

        $group3 = new \Field\Entity\Group();
        $group3->setName('Identifiants');
        $group3->setEntity('Project\Entity\Project');
        $group3->setEnabled(true);

        $fields = [
            'ss_obj' => [
                'name'   => 'Sous objectifs',
                'group'  => $group1,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'met_mesure_indic' => [
                'name'   => 'Propositions d\'indicateurs et de méthodes de mesure',
                'group'  => $group1,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'commentaire' => [
                'name'   => 'Commentaire évaluatif',
                'group'  => $group1,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'preconisation' => [
                'name'   => 'Préconisation',
                'group'  => $group1,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'bilan2014' => [
                'name'   => 'Bilan année en cours',
                'group'  => $group1,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'resultat_attendu' => [
                'name'   => 'Résultats attendus',
                'group'  => $group2,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'resultat_effectif' => [
                'name'   => 'Résultats effectifs obtenus',
                'group'  => $group2,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'effets_supposes' => [
                'name'   => 'Effets induits supposés',
                'group'  => $group2,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'effets_constates' => [
                'name'   => 'Effets induits constatés',
                'group'  => $group2,
                'type'   => Field::TYPE_TEXTAREA,
                'unique' => false
            ],
            'code_analytique' => [
                'name'   => 'Code analytique',
                'group'  => $group3,
                'type'   => Field::TYPE_TEXT,
                'unique' => true
            ],
            'code_calendar' => [
                'name'   => 'Code calendrier',
                'group'  => $group3,
                'type'   => Field::TYPE_TEXT,
                'unique' => true
            ],
        ];

        foreach ($fields as $key => $_field) {
            $field = new Field();
            $field->setName($_field['name']);
            $field->setType($_field['type']);
            $field->setGroup($_field['group']);
            $field->setMeta(['unique' => $_field['unique']]);
            $_field['group']->getFields()->add($field);

            $em->persist($field);

            $fields[$key]['field'] = $field;
        }
        $em->persist($group1);
        $em->persist($group2);
        $em->persist($group3);

        $em->flush();

        $cache = [
            'codes'       => [],
            'projects'    => [],
            'keywords'    => [],
            'indicators'  => [],
            'fields'      => [],
            'territories' => [],
            'perimeters'  => []
        ];

        foreach (['' => 'objectif', 'objectif' => 'projet', 'projet' => 'action', 'action' => 'phase'] as $parent => $level) {

            $module = ($level == 'projet' ? 8 : ($level == 'action' ? 5 : ($level == 'phase' ? 11 : 9)));

            $this->log('<b>' . ucfirst($level) . 's</b>', $log);
            $fiches = $this->query('SELECT * FROM `' . $level . '` WHERE `id_parc` = :parc', ['parc' => $parc]);
            $this->log(sizeof($fiches) . ' rows trouvés<br />', $log);

            foreach ($fiches as $fiche) {
                $project = new Project();

                // Champs
                $cache['codes'][] = $fiche['code'];
                $counts  = array_count_values($cache['codes']);
                $code    = $fiche['code'] . ($counts[$fiche['code']] > 1 ? ' (' . $counts[$fiche['code']] . ')' : '');

                $project->setName($fiche['nom_' . $level]);
                $project->setCode($code);

                if ($level !== 'objectif') {
                    $project->setProgrammingDate($this->convertDate($fiche['date_programmation']));
                }

                if($fiche['date_demarrage_prevue'] !== '0000-00-00' && $fiche['date_demarrage_prevue'] !== '') {
                    $project->setPlannedDateStart($this->convertDate($fiche['date_demarrage_prevue']));
                }
                if($fiche['date_demarrage'] !== '0000-00-00' && $fiche['date_demarrage'] !== '') {
                    $project->setRealDateStart($this->convertDate($fiche['date_demarrage']));
                }
                if($fiche['date_fin_demarrage_prevue'] !== '0000-00-00' && $fiche['date_fin_demarrage_prevue'] !== '') {
                    $project->setPlannedDateEnd($this->convertDate($fiche['date_fin_demarrage_prevue']));
                }
                if($fiche['date_fin_demarrage'] !== '0000-00-00' && $fiche['date_fin_demarrage'] !== '') {
                    $project->setRealDateEnd($this->convertDate($fiche['date_fin_demarrage']));
                }

                $project->setGlobalTimeTarget($fiche['temps_prevu']);

                switch($fiche['statut']) {
                    case 'prepa':
                        $project->setPublicationStatus(Project::PUBLICATION_STATUS_DRAFT);
                        break;
                    case 'suspen':
                        $project->setPublicationStatus(Project::PUBLICATION_STATUS_PENDING);
                        break;
                    case 'valid_att':
                        $project->setPublicationStatus(Project::PUBLICATION_STATUS_WAITING_VALIDATION);
                        break;
                    case 'valid':
                        $project->setPublicationStatus(Project::PUBLICATION_STATUS_VALIDATED);
                        break;
                    case 'publie':
                        $project->setPublicationStatus(Project::PUBLICATION_STATUS_PUBLISHED);
                        break;
                    case 'archive':
                        $project->setPublicationStatus(Project::PUBLICATION_STATUS_ARCHIVED);
                        break;
                }

                $project->setObjectifs($fiche['objectif']);
                $project->setDescription($fiche['descriptif']);

                if ($level !== 'phase') {
                    $project->setPrevResult($fiche['historique']);
                    if ($level !== 'objectif') {
                        $project->setContext($fiche['contexte']);
                    }
                }

                $project->setResult($fiche['bilan']);

                // Hierarchie
                $cache['projects'][$level . $fiche['id_' . $level]] = $project;
                if ($level !== 'objectif') {
                    if (isset($cache['projects'][$parent . $fiche['id_' . $parent]])) {
                        $project->setParent($cache['projects'][$parent . $fiche['id_' . $parent]]);
                    }
                }

                $project->computeLevel();

                // Equipe
                $gestionnaires = $this->query('SELECT * FROM `' . $level . '_contact` WHERE `id_' . $level . '` = :id', ['id' => $fiche['id_' . $level]]);
                foreach ($gestionnaires as $gestionnaire) {
                    if ($gestionnaire['id_contact'] != '0') {
                        $user = $em->getRepository('User\Entity\User')->find($gestionnaire['id_contact']);
                        if ($user) {
                            $member = new Member();
                            $member->setUser($user);
                            $member->setRole(Member::ROLE_MANAGER);
                            $member->setProject($project);
                            $project->getMembers()->add($member);

                            $em->persist($member);
                        }
                    }
                }

                $validator = $em->getRepository('User\Entity\User')->find($fiche['id_validateur']);
                if ($validator) {
                    $member = new Member();
                    $member->setUser($validator);
                    $member->setRole(Member::ROLE_VALIDATOR);
                    $member->setProject($project);
                    $project->getMembers()->add($member);

                    $em->persist($member);
                }

                // Acteurs
                if ($level == 'phase') {
                    $actors = $this->query('SELECT P.*, I.id_structure FROM `' . $level . '_structure` P INNER JOIN structure_instance I ON I.id_instance = P.id_instance WHERE `id_' . $level . '` = :id', ['id' => $fiche['id_' . $level]]);
                    foreach ($actors as $_actor) {
                        $structure = $em->getRepository('Directory\Entity\Structure')->find($_actor['id_structure']);
                        if ($structure) {
                            $actor = new Actor();
                            $actor->setProject($project);
                            $actor->setStructure($structure);
                            $project->getActors()->add($actor);

                            $em->persist($actor);
                        }
                    }
                }

                $em->persist($project);
            }
            $em->flush();


            // Champs texte en champs perso
            foreach ($fiches as $fiche) {
                if (isset($cache['projects'][$level . $fiche['id_' . $level]])) {
                    $project = $cache['projects'][$level . $fiche['id_' . $level]];

                    foreach ($fields as $key => $_field) {
                        if (isset($fiche[$key]) && $fiche[$key]) {
                            $fieldValue = new Value();
                            $fieldValue->setEntity('Project\Entity\Project');
                            $fieldValue->setPrimary($project->getId());
                            $fieldValue->setField($_field['field']);
                            $fieldValue->setValue($fiche[$key]);

                            $em->persist($fieldValue);
                        }
                    }
                }
            }
            $em->flush();


            // Mot clefs
            $this->log('<b>Mots clefs ' . ucfirst($level) . 's</b>', $log);
            $associations = $this->query('SELECT * FROM `' . $level . '_element` WHERE `id_' . $level . '` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc );', ['parc' => $parc]);
            $this->log(sizeof($associations) . ' rows trouvés<br />', $log);

            foreach ($associations as $_association) {
                if (isset($cache['projects'][$level . $_association['id_' . $level]])) {
                    if (!isset($cache['keywords'][$_association['id_element']])) {
                        $cache['keywords'][$_association['id_element']] = $em->getRepository('Keyword\Entity\Keyword')->find($_association['id_element']);
                    }

                    $keyword = $cache['keywords'][$_association['id_element']];

                    if ($keyword) {
                        $association = new Association();
                        $association->setEntity('Project\Entity\Project');
                        $association->setKeyword($keyword);
                        $association->setPrimary($cache['projects'][$level . $_association['id_' . $level]]->getId());

                        $em->persist($association);
                    }
                }
            }
            $em->flush();


            // Mesures
            $this->log('<b>Mesures ' . ucfirst($level) . 's</b>', $log);
            $measures = $this->query('SELECT * FROM `impact` WHERE `id_module` = :module AND `id_fiche` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc );', ['parc' => $parc, 'module' => $module]);
            $this->log(sizeof($measures) . ' rows trouvés<br />', $log);

            foreach ($measures as $_measure) {
                if (isset($cache['projects'][$level . $_measure['id_fiche']])) {
                    if (!isset($cache['indicators'][$_measure['id_indicateur']])) {
                        $cache['indicators'][$_measure['id_indicateur']] = $em->getRepository('Indicator\Entity\Indicator')->find($_measure['id_indicateur']);
                    }

                    $indicator = $cache['indicators'][$_measure['id_indicateur']];

                    if ($indicator) {
                        foreach (['source', 'valeur'] as $type) {
                            if ($_measure[$type]) {
                                $measure = new Measure();
                                $measure->setProject($cache['projects'][$level . $_measure['id_fiche']]);
                                $measure->setIndicator($indicator);
                                $measure->setStart($this->convertDate($_measure['dated']));
                                if (!$_measure['datef']) {
                                    $_measure['datef'] = '1960-01-01';
                                }
                                $measure->setDate($this->convertDate($_measure['datef']));
                                $measure->setValue($_measure[$type]);
                                $measure->setType(($type == 'valeur' ? Measure::TYPE_DONE : Measure::TYPE_TARGET));
                                $measure->setPeriod(Measure::PERIOD_INTERMEDIATE);
                                $measure->setComment($_measure['commentaire']);

                                $em->persist($measure);
                            }
                        }
                    }
                }
            }
            $em->flush();


            // Champs personnalisables
            $this->log('<b>Champs personnalisables ' . ucfirst($level) . 's</b>', $log);
            $valeurs = $this->query('SELECT * FROM `formulaire_champs_valeurs` WHERE `module` = :module AND `id_fiche` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc );', ['parc' => $parc, 'module' => $level]);
            $this->log(sizeof($valeurs) . ' rows trouvés<br />', $log);

            foreach ($valeurs as $_valeur) {
                if (isset($cache['projects'][$level . $_valeur['id_fiche']])) {
                    if (!isset($cache['fields'][$_valeur['id_champs']])) {
                        $cache['fields'][$_valeur['id_champs']] = $em->getRepository('Field\Entity\Field')->find($_valeur['id_champs']);
                    }

                    $field = $cache['fields'][$_valeur['id_champs']];
                    if ($field) {
                        $fieldValue = new Value();
                        $fieldValue->setEntity('Project\Entity\Project');
                        $fieldValue->setPrimary($cache['projects'][$level . $_valeur['id_fiche']]->getId());
                        $fieldValue->setField($field);
                        $fieldValue->setValue($_valeur['valeur']);

                        $em->persist($fieldValue);
                    }
                }
            }

            $em->flush();


            // Avancement
            $this->log('<b>Avancement ' . ucfirst($level) . 's</b>', $log);
            $avancements = $this->query('SELECT * FROM `avancement` WHERE `id_module` = :module AND `id_fiche` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc );', ['parc' => $parc, 'module' => $module]);
            $this->log(sizeof($valeurs) . ' rows trouvés<br />', $log);

            foreach ($avancements as $_avancement) {
                if (isset($cache['projects'][$level . $_avancement['id_fiche']])) {
                    foreach (['source', 'valeur'] as $type) {
                        if ($_avancement[$type] && $_avancement[$type] != 0) {
                            $advancement = new Advancement();
                            $advancement->setProject($cache['projects'][$level . $_avancement['id_fiche']]);
                            $advancement->setValue($_avancement[$type]);

                            if (!$_avancement['date_mesure']) {
                                $_avancement['date_mesure'] = '1960-01-01';
                            }
                            $advancement->setDate($this->convertDate($_avancement['date_mesure']));
                            $advancement->setType(($type == 'valeur' ? Advancement::TYPE_DONE : Advancement::TYPE_TARGET));
                            $advancement->setDescription($_avancement['commentaire']);

                            $em->persist($advancement);
                        }
                    }
                }
            }

            $em->flush();

            // Postes de dépense et dépenses
            $this->log('<b>Postes de dépenses ' . ucfirst($level) . 's</b>', $log);
            $or = '';
            if ($level == 'phase') {
                $or = ' OR `id_phase` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc )';
            }
            $postes = $this->query('SELECT * FROM `poste_depense` WHERE (`module_fiche` = :module AND `id_fiche` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc )) ' . $or . ' ;', ['parc' => $parc, 'module' => $level]);
            $this->log(sizeof($valeurs) . ' rows trouvés<br />', $log);

            foreach ($postes as $_poste) {
                $idFiche = $level == 'phase' ? $_poste['id_phase'] : $_poste['id_fiche'];

                if (isset($cache['projects'][$level . $idFiche])) {
                    $poste = new PosteExpense();
                    $poste->setName($_poste['nom_poste']);
                    $poste->setAmount($_poste['montant_total']);
                    $poste->setProject($cache['projects'][$level . $idFiche]);

                    $depenses = $this->query('SELECT * FROM `depense` WHERE id_poste_depense = :id', ['id' => $_poste['id_poste_depense']]);
                    foreach ($depenses as $_depense) {
                        $expense = new Expense();
                        $expense->setPoste($poste);

                        $expense->setName($_depense['titre']);
                        $expense->setDate($this->convertDate($_depense['date']));
                        switch($_depense['type_montant']) {
                            case 'prevu' :
                                $expense->setType(Expense::TYPE_PLANNED);
                                break;
                            case 'engage' :
                                $expense->setType(Expense::TYPE_COMMITTED);
                                break;
                            case 'paye' :
                                $expense->setType(Expense::TYPE_PAID);
                                break;
                            case 'engage_paye':
                                $expense->setType(Expense::TYPE_COMMITTED_PAID);
                                break;
                            case 'libere':
                                $expense->setType(Expense::TYPE_RELEASED);
                                break;
                        }

                        if ($expense->getType() == null) {
                            continue;
                        }

                        $expense->setAmount($_depense['montant']);
                        $expense->setImport($_depense['import'] == '1');

                        if ($_depense['date_fact'] !== '0000-00-00' && $_depense['date_fact']) {
                            $expense->setDateFacture($this->convertDate($_depense['date_fact']));
                        }

                        $expense->setTiers($_depense['beneficiaire_fact']);
                        if ($_depense['anne_mandat'] !== '0') {
                            $expense->setAnneeExercice($_depense['anne_mandat']);
                        }
                        $expense->setBordereau($_depense['num_bordereau_mandat']);
                        $expense->setMandat($_depense['num_mandat']);
                        $expense->setEngagement($_depense['num_engagement']);
                        $expense->setComplement($_depense['complements_libelle']);

                        $em->persist($expense);
                    }

                    $em->persist($poste);
                }
            }

            $em->flush();

            // Postes de recette et recettes
            $this->log('<b>Postes de recettes ' . ucfirst($level) . 's</b>', $log);
            $or = '';
            if ($level == 'phase') {
                $or = ' OR `id_phase` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc )';
            }
            $postes = $this->query('SELECT * FROM `poste_recette` WHERE (`module_fiche` = :module AND `id_fiche` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc )) ' . $or . ' ;', ['parc' => $parc, 'module' => $level]);
            $this->log(sizeof($postes) . ' rows trouvés<br />', $log);

            foreach ($postes as $_poste) {
                $idFiche = $level == 'phase' ? $_poste['id_phase'] : $_poste['id_fiche'];

                if (isset($cache['projects'][$level . $idFiche])) {
                    $poste = new PosteIncome();
                    $poste->setName($_poste['nom_poste']);
                    $poste->setAmount($_poste['montant_total']);
                    $poste->setProject($cache['projects'][$level . $idFiche]);

                    $envelope = $em->getRepository('Budget\Entity\Envelope')->find($_poste['id_enveloppe']);
                    if ($envelope) {
                        $poste->setEnvelope($envelope);
                    }

                    if ($_poste['date_demarrage'] !== '0000-00-00' && $_poste['date_demarrage']) {
                        $poste->setCaduciteStart($this->convertDate($_poste['date_demarrage']));
                    }

                    if ($_poste['date_achevement'] !== '0000-00-00' && $_poste['date_achevement']) {
                        $poste->setCaduciteEnd($this->convertDate($_poste['date_achevement']));
                    }

                    $recettes = $this->query('SELECT * FROM `recette` WHERE id_poste_recette = :id', ['id' => $_poste['id_poste_recette']]);
                    foreach ($recettes as $_recette) {
                        $income = new Income();
                        $income->setPoste($poste);

                        $income->setName($_recette['info']);
                        $income->setDate($this->convertDate($_recette['fin']));
                        switch($_recette['type_montant']) {
                            case 'sollicite' :
                                $income->setType(Income::TYPE_REQUESTED);
                                break;
                            case 'notifie' :
                                $income->setType(Income::TYPE_NOTIFIED);
                                break;
                            case 'attribue' :
                                $income->setType(Income::TYPE_ASSIGNED);
                                break;
                            case 'percu':
                                $income->setType(Income::TYPE_PERCEIVED);
                                break;
                        }

                        $income->setAmount($_recette['montant']);
                        $income->setImport($_recette['import'] == '1');

                        $income->setTiers($_recette['beneficiaire_fact']);
                        if ($_recette['anne_mandat'] !== '0') {
                            $income->setAnneeExercice($_recette['anne_mandat']);
                        }

                        $income->setBordereau($_recette['num_bordereau_mandat']);
                        $income->setMandat($_recette['num_mandat']);
                        $income->setEngagement($_recette['num_engagement']);
                        $income->setComplement($_recette['complements_libelle']);
                        $income->setSerie($_recette['serie_type']);

                        $em->persist($income);
                    }

                    $em->persist($poste);
                }
            }

            $em->flush();

            // Feuilles de temps
            $i = 0;
            foreach (['done', 'target'] as $type) {
                $this->log('<b>Feuilles de temps ' . $type . ' ' . ucfirst($level) . 's</b>', $log);
                $timesheets = $this->queryRH('SELECT * FROM `timesheet_eva_' . ($type == 'done' ? 'time' : 'forecast') . 'sheet` WHERE `moduleFiche` = :module AND `idParc` = :parc;', ['parc' => $parc, 'module' => $level]);
                $this->log(sizeof($timesheets) . ' rows trouvés<br />', $log);

                foreach ($timesheets as $_timesheet) {

                    if (isset($cache['projects'][$level . $_timesheet['idFiche']])) {

                        $user = $em->getRepository('User\Entity\User')->find($_timesheet['evaUser']);

                        $timesheet = new Timesheet();
                        $timesheet->setName($_timesheet['title']);
                        $timesheet->setUser($user);
                        $timesheet->setProject($cache['projects'][$level . $_timesheet['idFiche']]);

                        if ($type == 'done') {
                            $timesheet->setDescription($_timesheet['description']);
                            $timesheet->setSyncHash($_timesheet['icsUid']);
                        }
                        $timesheet->setType($type);
                        $timesheet->setStart(new \DateTime($_timesheet['startDate']));
                        $timesheet->setEnd(new \DateTime($_timesheet['endDate']));
                        $timesheet->setHours($_timesheet[($type == 'done' ? 'timeSpend' : 'estimatedTime')]);

                        $em->persist($timesheet);
                        $i++;

                        if ($i == 1000) {
                            $em->flush();
                            $i = 0;
                        }
                    }
                }

                $em->flush();
            }

            // Territoires
            $this->log('<b>Territoires ' . ucfirst($level) . 's</b>', $log);
            $associations = $this->query('SELECT * FROM `' . $level . '_territoire` WHERE `id_' . $level . '` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc );', ['parc' => $parc]);
            $this->log(sizeof($associations) . ' rows trouvés<br />', $log);

            foreach ($associations as $_association) {
                if (isset($cache['projects'][$level . $_association['id_' . $level]])) {
                    if (!isset($cache['territories'][$_association['id_territoire']])) {
                        $cache['territories'][$_association['id_territoire']] = $em->getRepository('Map\Entity\Territory')->find($_association['id_territoire']);
                    }

                    $territory = $cache['territories'][$_association['id_territoire']];

                    if ($territory) {
                        $association = new \Map\Entity\Association();
                        $association->setEntity('Project\Entity\Project');
                        $association->setTerritory($territory);
                        $association->setPrimary($cache['projects'][$level . $_association['id_' . $level]]->getId());

                        $em->persist($association);
                    }
                }
            }
            $em->flush();

            // Perimètres
            $this->log('<b>Perimètres ' . ucfirst($level) . 's</b>', $log);
            $associations = $this->query('SELECT A.* FROM `' . $level . '_element` A INNER JOIN element E ON E.id_element = A.id_element INNER JOIN descripteur D ON D.id_descripteur = E.id_descripteur WHERE `id_' . $level . '` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc ) AND D.id_secteur = 3;', ['parc' => $parc]);
            $this->log(sizeof($associations) . ' rows trouvés<br />', $log);

            foreach ($associations as $_association) {
                if (isset($cache['projects'][$level . $_association['id_' . $level]])) {
                    if (!isset($cache['perimeters'][$_association['id_element']])) {
                        $cache['perimeters'][$_association['id_element']] = $em->getRepository('Map\Entity\Territory')->findOneByCode('bascule_e_' . $_association['id_element']);
                    }

                    $perimeter = $cache['perimeters'][$_association['id_element']];

                    if ($perimeter) {
                        $association = new \Map\Entity\Association();
                        $association->setEntity('Project\Entity\Project');
                        $association->setTerritory($perimeter);
                        $association->setPrimary($cache['projects'][$level . $_association['id_' . $level]]->getId());

                        $em->persist($association);
                    }
                }
            }
            $em->flush();

            // Documents
            $this->log('<b>Documents ' . ucfirst($level) . 's</b>', $log);
            $documents = $this->query('SELECT * FROM `document_fiche` DF INNER JOIN document D ON D.id_document = DF.id_document WHERE DF.`id_module` = :module AND DF.`id_fiche` IN ( SELECT `id_' . $level . '` FROM `' . $level . '` WHERE id_parc = :parc );', ['parc' => $parc, 'module' => $module]);
            $this->log(sizeof($postes) . ' rows trouvés<br />', $log);
            foreach ($documents as $document) {
                if (isset($cache['projects'][$level . $document['id_fiche']])) {
                    $attachment = new Attachment();
                    $attachment->setType(Attachment::TYPE_URL);
                    $attachment->setName($document['titre']);
                    $attachment->setSize($document['taille']);
                    $attachment->setUrl($this->urlDocuments .'/' . $document['fichier']);
                    $attachment->setPrimary($cache['projects'][$level . $document['id_fiche']]->getId());
                    $attachment->setEntity('Project\Entity\Project');

                    $em->persist($attachment);
                }
            }

            $em->flush();
        }


        // Liaison projet/objectif
        $objectifGroup = new Group();
        $objectifGroup->setName('Fiches objectifs');
        $objectifGroup->setEntities(['project', 'indicators']);
        $objectifGroup->setMultiple(true);
        $objectifGroup->setType(Group::GROUP_TYPE_REFERENCE);
        foreach ($cache['projects'] as $key => $objectif) {
            if (strpos($key, 'objectif') > -1) {
                $projets = $this->query('
                SELECT id_projet FROM eva3_.objectif_projet WHERE id_objectif = :id
                UNION
                SELECT id_projet FROM eva3_.projet WHERE id_objectif = :id
                ', ['id' => str_replace('objectif', '', $key)]);

                $objectifKw = new Keyword();
                $objectifKw->setName($objectif->getName());
                $objectifKw->setGroup($objectifGroup);
                $objectifKw->setOrder(0);

                $em->persist($objectifKw);
                $count = 0;
                foreach ($projets as $idProjet) {
                    if (isset($cache['projects']['projet' . $idProjet['id_projet']])) {
                        $projet = $cache['projects']['projet' . $idProjet['id_projet']];
                        if ($count == 0 && $projet->getParent() == null) {
                            $projet->setParent($objectif);
                        } else {
                            $association = new Association();
                            $association->setEntity('Project\Entity\Project');
                            $association->setKeyword($objectifKw);
                            $association->setPrimary($projet->getId());

                            $em->persist($association);
                        }

                        $count++;
                    }
                }
            }
        }
        $em->persist($objectifGroup);

        $em->flush();
        $em->clear();

        // Nettoyage territoires
        $query = $em->createQuery('SELECT t FROM Map\Entity\Territory t WHERE t.code LIKE :code');
        $query->setParameter('code', 'bascule_e_%');
        $territories = $query->getResult();
        foreach ($territories as $territory) {
            $territory->setCode('');
        }
        $em->flush();
        $em->clear();


        // Semaines pré-enregsitrées
        $weeks = $this->queryRH('SELECT * FROM `timesheet_week` WHERE `idParc` = :parc', ['parc' => $parc]);

        foreach ($weeks as $week) {
            $user = $em->getRepository('User\Entity\User')->find($week['evaUser']);
            if ($user) {
                $bookmark = new Bookmark();
                $bookmark->setName($week['name']);
                $bookmark->setUser($user);

                $config = json_decode($week['week'], true);

                $c = [];
                foreach ($config['timeSpend'] as $i => $_line) {
                    $line = [];
                    $line['days'] = array_values($_line);
                    $line['name'] = $config['title'][$i];
                    $line['project'] = [];

                    if (isset($config['moduleFiche'][$i]) && isset($config['idFiche'][$i])) {
                        if (isset($cache['projects'][$config['moduleFiche'][$i] . $config['idFiche'][$i]])) {
                            $project = $cache['projects'][$config['moduleFiche'][$i] . $config['idFiche'][$i]];
                            $line['project'] = [
                                'id'   => $project->getId(),
                                'name' => $project->getName(),
                                'code' => $project->getCode()
                            ];
                        }
                    }

                    $c[] = $line;
                }
                $bookmark->setConfig($c);
                //$bookmark->setConfig($config);

                $em->persist($bookmark);
            }
        }

        $em->flush();
        $em->clear();
    }

    protected function migrateGlobalImages($em, $parc, &$log, $client)
    {
        $this->log('<b>Images Globales</b>', $log);

        if ($this->clean) {
            $em->createQuery('DELETE FROM Attachment\Entity\Attachment a')->execute();
        }

        $images = $this->query('SELECT * FROM `experso_img` WHERE `id_parc` = :parc', ['parc' => $parc]);
        $this->log(sizeof($images) . ' rows trouvés<br />', $log);

        foreach ($images as $image) {

            $attachment = new Attachment();
            $attachment->setName($image['fichier']);
            $attachment->setType(Attachment::TYPE_FILE);
            $attachment->setEntity('Bootstrap\Entity\Client');
            $attachment->setPrimary($client->getId());

            $path = Uploader::uploadFromUrl([
                'entity' => $attachment->getEntity(),
                'primary' => $attachment->getPrimary(),
                'filename' => $attachment->getName(),
                'url' => $this->urlDocuments . '/plugin/export_perso/upload/' .  $parc. '/'.  $image['fichier']
            ], $client);


            $attachment->setUrl($path);

            $em->persist($attachment);
        }

        $em->flush();
        $em->clear();
    }

    protected function downloadDocuments($em, $idParc, &$log, $client)
    {
        $this->log('<b>Téléchargement des documents </b>', $log);
        $attachements = $em->getRepository('Attachment\Entity\Attachment')->findAll();
        foreach ($attachements as $attachement) {
            $path = Uploader::uploadFromUrl([
                'entity' => $attachement->getEntity(),
                'primary' => $attachement->getPrimary(),
                'filename' => $attachement->getName(),
                'url' => $attachement->getUrl()
            ], $client);

            $attachement->setType(Attachment::TYPE_FILE);
            $attachement->setUrl($path);
        }

        $em->flush();
        $em->clear();
    }

    protected function customPNRPREALPES($em, $idParc, &$log, $client)
    {
        $cache = [
            'associations' => []
        ];

        $objectifsGroup  = $em->getRepository('Keyword\Entity\Group')->findOneByName('Fiches objectifs');
        $charteGroup     = $em->getRepository('Keyword\Entity\Group')->find(1601);

        $objectifs = $em->getRepository('Project\Entity\Project')->findByLevel('0');
        foreach ($objectifs as $objectif) {

            $associations = $em->getRepository('Keyword\Entity\Association')->findBy([
                'primary' => $objectif->getId(),
                'entity' => 'Project\Entity\Project'
            ]);

            $kw = [];
            foreach ($associations as $association) {
                if ($association->getKeyword()->getGroup()->getId() == 1601) {

                    $keyword = new Keyword();
                    $keyword->setName($objectif->getName());
                    $keyword->setGroup($charteGroup);
                    $keyword->getParents()->add($association->getKeyword());
                    $association->getKeyword()->getChildren()->add($keyword);
                    $keyword->setOrder(0);

                    $kw[] = $keyword;
                    $em->persist($keyword);
                }
            }

            if ($kw) {
                $asKeyword = $em->getRepository('Keyword\Entity\Keyword')->findOneBy([
                    'name'  => $objectif->getName(),
                    'group' => $objectifsGroup
                ]);

                $projets = $em->createQuery('
                    SELECT p
                    FROM Project\Entity\Project p
                    WHERE p.id IN (
                      SELECT a.primary
                      FROM Keyword\Entity\Association a 
                      WHERE a.keyword = :keyword
                    )    
                ')->setParameter('keyword', $asKeyword)->getResult();


                foreach ([$objectif->getChildren(), $projets] as $_) {
                    foreach ($_ as $projet) {

                        $__kw = [];
                        foreach ($kw as $keyword) {
                            if (!in_array($keyword->getName() . '---' . $projet->getId(), $cache['associations'])) {
                                $cache['associations'][] = $keyword->getName() . '---' . $projet->getId();

                                $__keyword = new Keyword();
                                $__keyword->setName($projet->getName());
                                $__keyword->setGroup($charteGroup);
                                $__keyword->getParents()->add($keyword);
                                $keyword->getChildren()->add($__keyword);
                                $__keyword->setOrder(0);

                                $__kw[] = $__keyword;
                                $em->persist($__keyword);
                            }
                        }

                        foreach ($projet->getChildren() as $action) {
                            $action->setParent(null);
                            foreach ($__kw as $keyword) {
                                if (!in_array($keyword->getName() . '---' . $action->getId(), $cache['associations'])) {
                                    $cache['associations'][] = $keyword->getName() . '---' . $action->getId();

                                    $association = new Association();
                                    $association->setKeyword($keyword);
                                    $association->setPrimary($action->getId());
                                    $association->setEntity('Project\Entity\Project');

                                    $em->persist($association);
                                }
                            }
                        }

                        $projet->setName('DELETE');
                    }
                }

                $objectif->setName('DELETE');
            }
        }

        $em->flush();

        // delete  objectifs
        // delete  projets
        // regenerate levels
        // delete kws (cf doc)
    }
}
