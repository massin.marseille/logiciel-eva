<?php

namespace Admin\Controller\API;

use Bootstrap\Environment\Client\ClientCreator;
use Core\Controller\BasicRestController;

/**
 * Class NetworkController
 *
 * The Client Entity REST controller.
 *
 * @package Admin\Controller\API
 */
class NetworkController extends BasicRestController
{
    protected $entityClass = 'Bootstrap\Entity\Network';
    protected $repository  = 'Bootstrap\Entity\Network';
    protected $entityChain = 'admin:e:network';

    public function create($data)
    {
        $create = parent::create($data);
        if ($create->getVariable('success') === true) {
            $object = $create->getVariable('object');
            $this->sync($object['id']);
        }
        return $create;
    }

    public function update($id, $data)
    {
        $update = parent::update($id, $data);
        if ($update->getVariable('success') === true) {
            $this->sync($id);
        }
        return $update;
    }

    protected function sync($id)
    {
        $network = $this->getEntityManager()->getRepository($this->repository)->find($id);
        if ($network->getMaster()) {
            $this->environment()->setClient($network->getMaster());

            $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
            $em = $entityManagerFactory->getEntityManager($network->getMaster());

            $groups     = $em->getRepository('Keyword\Entity\Group')->findAll();
            $indicators = $em->getRepository('Indicator\Entity\Indicator')->findAll();

            foreach ($groups as $group) {
                $requestGroup = $this->apiRequest('Keyword\Controller\API\Group', 'PUT', []);
                $requestGroup->getController()->setClient($network->getMaster());
                $requestGroup->getController()->update($group->getId(), [
                    'name'     => $group->getName(),
                    'id'       => $group->getId(),
                    'type'     => $group->getType(),
                    'entities' => $group->getEntities(),
                    'multiple' => $group->getMultiple(),
                ]);

                foreach ($group->getKeywords() as $keyword) {
                    $requestKw = $this->apiRequest('Keyword\Controller\API\Keyword', 'PUT', []);
                    $requestKw->getController()->setColumns(['group.id']);
                    $requestKw->getController()->setClient($network->getMaster());
                    $requestKw->getController()->update($keyword->getId(), []);
                }
            }

            foreach ($indicators as $indicator) {
                $requestInd = $this->apiRequest('Indicator\Controller\API\Indicator', 'PUT', []);
                $requestInd->getController()->setClient($network->getMaster());
                $requestInd->getController()->update($indicator->getId(), [
                    'name'        => $indicator->getName(),
                    'id'          => $indicator->getId(),
                    'uom'         => $indicator->getUom(),
                    'description' => $indicator->getDescription(),
                    'type'        => $indicator->getType(),
                    'values'      => $indicator->getValues(),
                    'operator'    => $indicator->getOperator()
                ]);
            }

            $this->environment()->setClient(null);
        }
    }
}
