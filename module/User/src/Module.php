<?php

namespace User;

use Core\Module\AbstractModule;
use Core\Utils\RequestUtils;
use User\Auth\Acl;
use User\Auth\AuthStorage;
use User\Auth\Helper\AclHelper;
use User\Auth\Helper\AuthStorageHelper;
use User\Auth\Plugin\AclPlugin;
use User\Auth\Plugin\AuthStoragePlugin;
use User\View\Helper\UserFilters;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        $request     = $application->getRequest();

        if (!RequestUtils::isCommandRequest($request)) {
            $serviceManager = $application->getServiceManager();
            $sharedManager = $application->getEventManager()->getSharedManager();

            $sharedManager->attach(
                'Laminas\Mvc\Controller\AbstractActionController',
                'dispatch',
                function (MvcEvent $event) use ($serviceManager) {
                    return $serviceManager->get('ControllerPluginManager')->get('authStorage')->preDispatch($event);
                },
                2
            );
        }
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'AuthStorage' => function (ServiceManager $serviceManager) {
                    $environment = $serviceManager->get('Environment');
                    return new AuthStorage($environment->getEntityManager(), $environment->getSession());
                },
                'Acl' => function (ServiceManager $serviceManager) {
                    $moduleManager = $serviceManager->get('MyModuleManager');
                    $user          = $serviceManager->get('AuthStorage')->getUser();
                    $env           = $serviceManager->get('Environment');
                    return new Acl($moduleManager, $env, $user);
                },
            ]
        ];
    }

    /**
     * @return array
     */
    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'authStorage' => function (ServiceManager $serviceManager) {
                    $authStorage = $serviceManager->get('AuthStorage');
                    return new AuthStoragePlugin($authStorage);
                },
                'acl' => function (ServiceManager $serviceManager) {
                    $acl = $serviceManager->get('Acl');
                    return new AclPlugin($acl);
                },
            ]
        ];
    }

    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'authStorage' => function (ServiceManager $serviceManager) {
                    $authStorage = $serviceManager->get('AuthStorage');
                    return new AuthStorageHelper($authStorage);
                },
                'acl' => function (ServiceManager $serviceManager) {
                    $acl = $serviceManager->get('Acl');
                    return new AclHelper($acl);
                },
                'userFilters' => function (ServiceManager $serviceManager) {
                    $translateViewHelper = $serviceManager->get('ViewHelperManager')->get('translate');
                    $allowedKeywordGroupsViewHelper = $serviceManager->get('ViewHelperManager')->get('allowedKeywordGroups');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $serviceManager->get('ViewHelperManager')->get('entityManager')->__invoke();
                    return new UserFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                },
            ]
        ];
    }
}
