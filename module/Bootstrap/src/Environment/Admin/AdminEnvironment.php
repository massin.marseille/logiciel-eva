<?php

namespace Bootstrap\Environment\Admin;

use Bootstrap\Environment\Environment;
use Bootstrap\Environment\Session;
use Bootstrap\ORM\EntityManagerFactory;
use Laminas\Http\Request as HttpRequest;
use Laminas\Router\Http\TreeRouteStack;

/**
 * Class AdminEnvironment
 *
 * The admin environment will use the default Doctrine Entity Manager ("orm-default").
 * There is no "client" to manage here.
 *
 * @package Bootstrap\Environment\Admin
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AdminEnvironment extends Environment
{
    protected $client;

    public function __construct(array $config, HttpRequest $request, TreeRouteStack $router, EntityManagerFactory $entityManagerFactory)
    {
        $entityManager = $entityManagerFactory->getEntityManager();
        parent::__construct('admin', $config, $request, $router, new Session(), $entityManager, 'admin/layout');
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient($client)
    {
        $this->client = $client;
    }
}
