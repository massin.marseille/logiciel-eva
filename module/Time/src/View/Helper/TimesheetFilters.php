<?php

namespace Time\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use Time\Entity\Timesheet;

class TimesheetFilters extends EntityFilters
{

    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $typeOptions = [];
        foreach(Timesheet::getTypes() as $type){
            $typeOptions[] = [
                'value' => $type,
                'text'  => $this->translator->__invoke('timesheet_type_' . $type)
            ];
        }

        $filters = [
            'name'      => [
                'label' => $this->translator->__invoke('timesheet_field_name'),
                'type'  => 'string',
            ],
            'project'   => [
                'label' => $this->translator->__invoke('timesheet_field_project'),
                'type'  => 'project-select',
            ],
            'user'      => [
                'label' => $this->translator->__invoke('timesheet_field_user'),
                'type'  => 'user-select',
            ],
            'start'     => [
                'label' => $this->translator->__invoke('timesheet_field_start'),
                'type'  => 'date',
            ],
            'end'       => [
                'label' => $this->translator->__invoke('timesheet_field_end'),
                'type'  => 'date',
            ],
            'hours'     => [
                'label' => $this->translator->__invoke('timesheet_field_hours'),
                'type'  => 'number',
            ],
            'type'      => [
                'label'   => $this->translator->__invoke('timesheet_field_type'),
                'type'    => 'manytoone',
                'options' => $typeOptions,
            ],
            'absenceType' => [
                'label'   => $this->translator->__invoke('timesheet_field_absenceType'),
                'type'    => 'absence-type-select',
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('timesheet_field_createdAt'),
                'type'  => 'date',
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('timesheet_field_updatedAt'),
                'type'  => 'date',
            ],
        ];

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('timesheet', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type'  => 'keyword-select',
                    'group' => $group->getId(),
                ];
            }
        }

        if ($this->moduleManager->isActive('map')) {
            $filters['territory'] = [
                'label' => ucfirst($this->translator->__invoke('territory_entity')),
                'type'  => 'territory-select'
            ];
        }

        $this->filters = $filters;
    }
}
