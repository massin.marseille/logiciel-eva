(function (app) {

    app.controller('contactListController', [
        'contactService', '$scope', 'flashMessenger', 'translator', '$http',
        function contactListController(contactService, $scope, flashMessenger, translator, $http) {
            var self = this;

            self.fusion = {
                type: 'duplicate'
            };

            self.loading = {
                fusion: false
            };

            self.contacts = [];
            self.options  = {
                name      : 'contacts-list',
                col       : [
                    'id',
                    'lastName',
                    'firstName',
                    'name',
                    'gender',
                    'vip',
                    'subscriptionStatus',
                    'subscriptionStatusUpdatedAt',
                    'email',
                    'linksArr',
                    'phone',
                    'portable',
                    'zipcode',
                    'city',
                    'country',
                    'addressLine1',
                    'addressLine2',
                    'createdAt',
                    'updatedAt',
                    'keywords'
                ],
                searchType: 'list',
                sort      : 'lastName',
                order     : 'asc',
                pager     : true,
                getItems: function getData(params, callback, abort) {
                    contactService.findAll(params, abort.promise).then(function (res) {
                        res.rows.forEach(x => {
                            x.gender = x.gender ? x.gender.replace('.', '').toLowerCase() : '';
                            if (x.gender === 'non binaire') {
                                x.gender = 'neutral';
                            }
                        });
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function (event) {
                if (event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine         = function editLine(contact) {
                if (angular.isArray(contact.keywords) || contact.keywords == null) {
                    contact.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = contact;
                self.editedLineFields = angular.copy(contact);
            };

            self.saveEditedLine = function saveEditedLine(contact) {
                self.editedLineErrors = {};
                contactService.save(angular.extend({}, contact, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        contact               = angular.extend(contact, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('contact_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.duplicate = function duplicate(contact) {
                $http({
                    method: 'POST',
                    url   : '/directory/contact/duplicate/' + contact.id
                }).then(function (res) {
                    if (res.data.success) {
                        window.location = '/directory/contact/form/' + res.data.contact.id;
                    } else {
                        for (var i in res.data.error) {
                            flashMessenger.error(res.data.error[i]);
                        }
                    }
                });
            };

            self.showFusionModal = function (contactToFusion) {
                angular.element('#fusionModal').modal('show');
                self.fusion.contactToFusion = contactToFusion;
            };

            self.deleteContact = function deleteContact(contact) {
                if (confirm(translator('contact_question_delete').replace('%1', contact.firstName + ' ' + contact.lastName))) {
                    contactService.remove(contact).then(function (res) {
                        if (res.success) {
                            self.contacts.splice(self.contacts.indexOf(contact), 1);
                            flashMessenger.success(translator('contact_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.doFusion = function doFusion() {
                if (
                    !self.fusion.contactToKeep ||
                    !self.fusion.contactToFusion ||
                    !self.fusion.type
                ) {
                    flashMessenger.error(translator('error_occured'));
                    return;
                }

                self.loading.fusion = true;

                contactService
                    .fusion(self.fusion.contactToKeep.id, self.fusion.contactToFusion.id, self.fusion.type)
                    .then(function (res) {
                        self.loading.fusion = false;
                        angular.element('#fusionModal').modal('hide');
                        if (res.success) {
                            if (self.fusion.type === 'transfer') {
                                self.contacts.splice(self.contacts.indexOf(self.fusion.contactToFusion), 1);
                                flashMessenger.success(translator('contact_message_deleted'));
                            } else {
                                $scope.__tb.loadData();
                                flashMessenger.success(translator('contact_message_saved_public'));
                            }
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
            };
        }
    ])

})(window.EVA.app);
