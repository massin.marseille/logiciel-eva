(function(app) {

    app.factory('posteExpenseSimplifiedInputService', [
        '$http', '$q', 'restFactoryService',
        function posteExpenseSimplifiedInputService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/budget/simplified-input'), {
                getReferential: function getReferential(projectId) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/api/budget/simplified-input/referential/' + projectId
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                getTable: function getTable(projectId) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/api/budget/simplified-input/table/' + projectId
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                generateTable: function generateTable(form, projectId) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/api/budget/simplified-input/generate/' + projectId,
                        data: form
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                importTable: function importTable(data, projectId) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/api/budget/simplified-input/import/' + projectId,
                        data: data
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                }
            });
        }
    ]);

})(window.EVA.app);