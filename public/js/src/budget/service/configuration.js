(function(app) {

    app.factory('budgetConfiguration', [
        '$http', '$q', 'restFactoryService',
        function budgetConfiguration($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/budget/configuration'), {
                getCategories: function getCategories() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/api/budget/configuration/categories'
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                getAccounts: function getAccounts() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/api/budget/configuration/accounts'
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                getPostesExpense: function getPostesExpense() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/api/budget/configuration/postes-expense'
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                savePosteExpense: function savePosteExpense(form) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/api/budget/configuration/postes-expense/save',
                        data: form
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                deletePosteExpense: function deletePosteExpense(id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'DELETE',
                        url: '/api/budget/configuration/postes-expense/delete/' + id
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                }
            });
        }
    ]);

})(window.EVA.app);