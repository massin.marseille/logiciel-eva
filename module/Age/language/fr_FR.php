<?php

return [
    'age_module_title' => 'Liaison comptable AGE (SNEG)',

    'age_db_type_configuration'                     => 'Type',
    'age_db_host_configuration'                     => 'Host (entrepôt)',
    'age_db_port_configuration'                     => 'Port (entrepôt)',
    'age_db_name_configuration'                     => 'Base de données (entrepôt)',
    'age_db_user_configuration'                     => 'Utilisateur',
    'age_db_password_configuration'                 => 'Mot de passe',
    'age_portfolio_active'                          => 'Activer la gestion des portefeuilles',
    'age_master_db_host_configuration'              => 'Host (master)',
    'age_master_db_port_configuration'              => 'Port (master)',
    'age_master_db_name_configuration'              => 'Base de données (master)',
    'age_identifier_configuration'                  => 'Identifiant établissement',
    'age_import_first_year'                         => 'Première année d\'exercice à importer',
    'age_import_last_year'                          => 'Dernière année d\'exercice à importer',
    'age_import_allowed_expenseTypes'               => 'Dépenses autorisées à l\'import',
    'age_import_allowed_incomeTypes'                => 'Recettes autorisées à l\'import',
    'age_api_uri_configuration'                     => 'Admilia API uri (pour synchronisation montante)',
    'age_api_auth_token'                            => 'Admilia API Auth Token',
    'age_api_switch_code'                           => 'Colonne code différente ?',
    'age_api_level'                                 => 'Niveau de présentation',

    'non_editable_age_info_message'                 => 'Certains champs ne sont pas éditables car cette recette provient de la liaison AGE.',

    'age_synchro_legend'                            => 'Synchronisation vers AGE',
    'age_synchro_state_label'                       => 'Etat :',
    'age_synchro_state_prevent'                     => 'Pas de synchronisation',
    'age_synchro_state_pending'                     => 'En attente de synchronisation',
    'age_synchro_state_synchronised'                => 'Synchronisé',
    'age_synchro_btn_label'                         => 'Synchroniser',
    'age_synchro_lock_btn_label'                    => 'Bloquer la synchronisation',

    'age_synchro_flag_label'                        => 'Type d\'opération',
    'age_synchro_flag_budgetSeizable'               => 'saisissable au budget',
    'age_synchro_flag_budgetPresented'              => 'présentée au budget',
    'age_synchro_flag_executionSeizable'            => 'saisissable en exécution',
    'age_synchro_validation_error'                  => 'Certaines données nécessaires à la synchronisation ne sont pas remplies :'
];
