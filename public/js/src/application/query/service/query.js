(function(app) {

    app.factory('queryService', [
        'restFactoryService',
        function queryService(restFactoryService) {
            return restFactoryService.create('/api/query');
        }
    ])

})(window.EVA.app);
