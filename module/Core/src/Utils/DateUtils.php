<?php

namespace Core\Utils;

use DateTime;
use Exception;

class DateUtils 
{

    public static function changePattern($dateStr, $sourcePattern, $targetPattern)
    {
        $myDateTime = self::toDate($dateStr, $sourcePattern);
        return $myDateTime->format($targetPattern);
    }

    public static function toDate($dateStr, $pattern)
    {
        return DateTime::createFromFormat($pattern, $dateStr);
    }

    public static function dateDiffInDays($date1Str, $date2Str, $pattern)
    {
        try {
            $date1 = self::toDate($date1Str, $pattern);
            $date2 = self::toDate($date2Str, $pattern);
            if(!$date1 || !$date2) {
                return null;
            }
            $diff = $date1->diff($date2, true);
            return intval($diff->format('%a'));

        } catch(Exception $e) {
            return null;
        }        
    }

}