(function(app) {

    app.controller('noteListController', [
        'noteService', 'projectService', 'conventionService',
        function (noteService, projectService, conventionService) {
            var self = this;

            self.type     = 'note';
            self.notes    = [];
            self.projects = [{}];
            self.loading  = {
                save: false
            };

            var noteParams = {
                right: 'project:e:project',
                col: [
                    'id',
                    'name',
                    'entity',
                    'primary',
                    'message',
                    'attachment.url',
                    'createdBy.id',
                    'createdBy.name',
                    'createdAt'
                ]
            };

            var noteHomeParams = {
                right: 'project:e:project',
                col: [
                    'id',
                    'name',
                    'entity',
                    'primary',
                    'message',
                    'attachment.url',
                    'createdBy.id',
                    'createdBy.name',
                    'createdBy.avatar',
                    'createdAt'
                ]
            };

            self.init = function () {
                noteService.findAll(noteParams).then(function (res) {
                    self.notes = res.rows;

                    for (var i = 0; i < self.notes.length; ++i) {
                        var inTabs = false;

                        for (var j = 0; j < self.projects.length; ++j) {
                            if (self.notes[i].primary == self.projects[j].id) {
                                inTabs = true;
                            }
                        }

                        if (!inTabs) {
                            self.projects.push({id: self.notes[i].primary});
                            inTabs = false;
                        }
                    }
                    self.projects.shift();

                    angular.forEach(self.projects, function (value, key) {
                        projectService.find(self.projects[key].id, {col: ['name']}).then(function (res) {
                            self.projects[key].name = res.object.name;
                        });
                    });
                });
            };

            self.initHome = function (userId) {
                noteService.findAll(noteHomeParams).then(function (res) {
                    var notes = res.rows;

                    angular.forEach(notes, function (value, key) {
                        var note = notes[key];

                        switch (note.entity) {
                            case 'Project\\Entity\\Project':
                                projectService.find(note.primary, {col: ['name', 'members.user.id']}).then(function (res) {
                                    for (var i in res.object.members) {
                                        if (res.object.members[i].user.id == userId) {
                                            note.entityObject = {
                                                type: 'project',
                                                id  : note.primary,
                                                name: res.object.name
                                            };
                                            self.notes.push(note);
                                            break;
                                        }
                                    }
                                }, function () {});
                                break;
                            case 'Convention\\Entity\\Convention':
                                conventionService.find(note.primary, {col: ['name', 'members.user.id']}).then(function (res) {
                                    for (var i in res.object.members) {
                                        if (res.object.members[i].user.id == userId) {
                                            note.entityObject = {
                                                type: 'convention',
                                                id  : note.primary,
                                                name: res.object.name
                                            };
                                            self.notes.push(note);
                                            break;
                                        }
                                    }
                                }, function () {});
                                break;
                        }
                    });
                }, function () {});
            };
        }
    ]);

})(window.EVA.app);
