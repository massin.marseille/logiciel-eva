<?php

namespace Age\Service;

use Project\Entity\Project;

interface AgeSynchronisationServiceInterface
{

    /**
     * Check if the module is enabled
     * 
     * @return boolean
     */
    public function isSynchronisationActive();

    /**
     * Init AGE synchronisation status for given project
     * 
     * @param Project project
     */
    public function initForProject(Project $project);

    /**
     * Create an operation into AGE
     * 
     * @param Project project
     */
    public function createOperation(Project $project);

}
