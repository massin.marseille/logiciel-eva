(function (app) {

    app.controller('contactEmailController', [
        'contactEmailService', 'flashMessenger', 'translator', '$scope',
        function (contactEmailService, flashMessenger, translator, $scope) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'email',
                    'contact.id'
                ]
            };

            self.contact = $scope.$parent.$parent.self.contact;
            self.contactEmails  = [];

            self.errors = {
                fields: {}
            };

            self.loading = {
                save: false
            };

            self.contactEmail = {
                _rights: {
                    update: true
                }
            };

            self.init = function () {
                contactEmailService
                    .findAll({
                        col: apiParams.col,
                        search: {
                            type: 'list',
                            data: {
                                sort: 'object.id',
                                order: 'asc',
                                filters: {
                                    contact: {
                                        val: self.contact.id,
                                        op: 'eq'
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.contactEmails = res.rows;
                    });
            };

            self.openModal = function (contactEmail) {
                if (!contactEmail) {
                    self.contactEmail = {
                        _rights: {
                            update: true
                        }
                    }
                } else {
                    self.contactEmail = contactEmail;
                }

                if (self.contactEmail.id) {
                    self.panelTitle = angular.copy(self.contactEmail.email);
                } else {
                    self.panelTitle = translator('contactEmail_nav_form');
                }

                $('#contactEmailModal').modal('show');
            };

            self.save = function () {
                self.errors.fields = {};
                self.loading.save  = true;

                self.contactEmail.contact = self.contact.id;

                var isCreation = !self.contactEmail.id;

                contactEmailService.save(self.contactEmail, apiParams).then(function (res) {
                    self.loading.save = false;
                    self.contactEmail        = {
                        _rights: {
                            update: true
                        }
                    };

                    if (isCreation) {
                        self.contactEmails.push(res.object);
                    } else {
                        for (var i in self.contact.contactEmails) {
                            if (self.contactEmails[i].id === res.object.id) {
                                self.contactEmails[i] = res.object;
                                break;
                            }
                        }
                    }

                    $('#contactEmailModal').modal('hide');
                    flashMessenger.success(translator('contactEmail_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                });
            };

            self.delete = function (contactEmailToDelete) {
                if (confirm(translator('contactEmail_question_delete').replace('%1', contactEmailToDelete.email))) {
                    contactEmailService
                        .remove(contactEmailToDelete)
                        .then(function () {
                            self.contactEmails = self.contactEmails.filter(function (contactEmail) {
                                return contactEmail.id !== contactEmailToDelete.id;
                            });
                            flashMessenger.success(translator('contactEmail_message_deleted'));
                        });
                }
            };
        }
    ]);
})(window.EVA.app);
