(function(app) {

    app.controller('userListController', [
        'userService', '$scope', 'flashMessenger', 'translator',
        function userListController(userService, $scope, flashMessenger, translator) {
            var self = this;

            self.users = [];
            self.options = {
                name: 'users-list',
                col: [
                    'id',
                    'gender',
                    'password',
                    'firstName',
                    'lastName',
                    'login',
                    'role.id',
                    'role.name',
                    'createdAt',
                    'updatedAt',
                    'avatar',
                    'email',
                    'disabled',
                    'keywords'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    userService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.userConnectedId = null;

            self.init = function (userConnectedId) {
                self.userConnectedId = userConnectedId;
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(user) {
                if (angular.isArray(user.keywords) || user.keywords == null) {
                    user.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = user;
                self.editedLineFields = angular.copy(user);
            };

            self.saveEditedLine = function saveEditedLine(user) {
                self.editedLineErrors = {};
                userService.save(angular.extend({}, user, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        user = angular.extend(user, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('user_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteUser = function deleteUser(user) {
                if (confirm(translator('user_question_delete').replace('%1', user.login))) {
                    userService.remove(user).then(function(res) {
                        if (res.success) {
                            self.users.splice(self.users.indexOf(user), 1);
                            flashMessenger.success(translator('user_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
