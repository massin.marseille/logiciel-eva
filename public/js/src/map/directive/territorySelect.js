(function(app) {

    app.directive('territorySelect', [
        'territoryService', 'selectFactoryService',
        function territorySelect(territoryService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: territoryService,
                    apiParams: {
                        col: ['id', 'name', 'parents.id'],
                        search : {
                            data : {
                                sort: 'name',
                                order: 'asc'
                            }
                        }
                    },
                    tree: true,
                    nnTree: true
                },
                selectize: {
                    deferredSearch: true,
                    deferredSearchMinLength: 3,
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
