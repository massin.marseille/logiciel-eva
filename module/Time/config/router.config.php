<?php

namespace Time;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'timesheet' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/timesheet',
                    'defaults' => [
                        'controller' => 'Time\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form'            => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'Time\Controller\Index',
                                'action'     => 'form',
                            ],
                        ],
                    ],
                    'duplicate'       => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/duplicate/:id',
                            'defaults' => [
                                'controller' => 'Time\Controller\Index',
                                'action'     => 'duplicate',
                            ],
                        ],
                    ],
                    'weekly-add'      => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/weekly-add',
                            'defaults' => [
                                'controller' => 'Time\Controller\Index',
                                'action'     => 'weeklyAdd',
                            ],
                        ],
                    ],
                    'synchronisation' => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/synchronisation',
                            'defaults' => [
                                'controller' => 'Time\Controller\Synchronisation',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form'      => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/form[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\Synchronisation',
                                        'action'     => 'form',
                                    ],
                                ],
                            ],
                            'import'    => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/import',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\Synchronisation',
                                        'action'     => 'import',
                                    ],
                                ],
                            ],
                            'do-import' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route'    => '/do-import',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\Synchronisation',
                                        'action'     => 'doImport',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'export'          => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/export',
                            'defaults' => [
                                'controller' => 'Time\Controller\Export',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/form[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\Export',
                                        'action'     => 'form',
                                    ],
                                ],
                            ],
                            'do'   => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/do',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\Export',
                                        'action'     => 'do',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'absence-type'    => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/absence-type',
                            'defaults' => [
                                'controller' => 'Time\Controller\AbsenceType',
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'settings-time'    => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/settings-time',
                            'defaults' => [
                                'controller' => 'Time\Controller\SettingsTime',
                                'action'     => 'index',
                            ],
                        ],
                    ],
                ],
            ],
            'api'       => [
                'child_routes' => [
                    'timesheet' => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/timesheet',
                            'defaults' => [
                                'controller' => 'Time\Controller\API\Timesheet',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'timesheet'       => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\API\Timesheet',
                                    ],
                                ],
                            ],
                            'bookmark'        => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/bookmark[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\API\Bookmark',
                                    ],
                                ],
                            ],
                            'synchronisation' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/synchronisation[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\API\Synchronisation',
                                    ],
                                ],
                            ],
                            'export'          => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/export[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\API\Export',
                                    ],
                                ],
                            ],
                            'absence-type'          => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/absence-type[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\API\AbsenceType',
                                    ],
                                ],
                            ],
                            'settings-time'          => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/settings-time[/:id]',
                                    'defaults' => [
                                        'controller' => 'Time\Controller\API\SettingsTime',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class               => ServiceLocatorFactory::class,
            Controller\SynchronisationController::class     => ServiceLocatorFactory::class,
            Controller\ExportController::class              => ServiceLocatorFactory::class,
            Controller\AbsenceTypeController::class         => ServiceLocatorFactory::class,
            Controller\SettingsTimeController::class        => ServiceLocatorFactory::class,
            Controller\API\TimesheetController::class       => ServiceLocatorFactory::class,
            Controller\API\BookmarkController::class        => ServiceLocatorFactory::class,
            Controller\API\SynchronisationController::class => ServiceLocatorFactory::class,
            Controller\API\ExportController::class          => ServiceLocatorFactory::class,
            Controller\API\AbsenceTypeController::class     => ServiceLocatorFactory::class,
            Controller\API\SettingsTimeController::class     => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Time\Controller\Index'               => Controller\IndexController::class,
            'Time\Controller\Synchronisation'     => Controller\SynchronisationController::class,
            'Time\Controller\Export'              => Controller\ExportController::class,
            'Time\Controller\AbsenceType'         => Controller\AbsenceTypeController::class,
            'Time\Controller\SettingsTime'        => Controller\SettingsTimeController::class,
            'Time\Controller\API\Timesheet'       => Controller\API\TimesheetController::class,
            'Time\Controller\API\Bookmark'        => Controller\API\BookmarkController::class,
            'Time\Controller\API\Synchronisation' => Controller\API\SynchronisationController::class,
            'Time\Controller\API\Export'          => Controller\API\ExportController::class,
            'Time\Controller\API\AbsenceType'     => Controller\API\AbsenceTypeController::class,
            'Time\Controller\API\SettingsTime'    => Controller\API\SettingsTimeController::class,
        ],
    ],
];
