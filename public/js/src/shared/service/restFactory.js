(function(app) {

    app.factory('restFactoryService', [
        '$http', '$q', 'flashMessenger',
        function restFactoryService($http, $q, flashMessenger) {
            return {
                create: function create(url) {
                    return {
                        find: function find(id, params) {
                            var deferred = $q.defer();

                            $http({
                                method: 'GET',
                                url: url + '/' + id + (params ? '?' + $.param(params) : '')
                            }).then(function(res) {
                                deferred.resolve(res.data)
                            }, function(err) {
                                deferred.reject(err)
                            });

                            return deferred.promise;
                        },
                        findAll: function findAll(params, timeout) {
                            var deferred = $q.defer();

                            if(typeof params.excel === 'boolean' && params.excel){
                                window.open(url + (params ? '?' + $.param(params) : ''));
                            } else {
                                $http({
                                    method: 'GET',
                                    url: url + (params ? '?' + $.param(params) : ''),
                                    timeout : timeout
                                }).then(function(res) {
                                    deferred.resolve(res.data)
                                }, function(err) {
                                    deferred.reject(err)
                                });
                            }

                            return deferred.promise;
                        },
                        save: function save(object, params) {
                            var deferred = $q.defer();
                            $http({
                                method: object.id ? 'PUT' : 'POST',
                                url: url + (object.id ? '/' + object.id : '') + (params ? '?' + $.param(params) : ''),
                                data: object
                            }).then(function(res) {
                                if (res.data.success) {
                                    deferred.resolve(res.data)
                                } else {
                                    if (res.data.errors.exceptions.length > 0) {
                                        var message = '';
                                        for (var i in res.data.errors.exceptions) {
                                            message += res.data.errors.exceptions[i] + '<br />';
                                        }
                                        flashMessenger.error(message);
                                    }
                                    deferred.reject(res.data.errors)
                                }
                            }, function(err) {
                                deferred.reject(err)
                            });

                            return deferred.promise;
                        },
                        saveAll: function saveAll(objects, params) {
                            var deferred = $q.defer();

                            $http({
                                method: 'PUT',
                                url: url + (params ? '?' + $.param(params) : ''),
                                data: objects
                            }).then(function(res) {
                                //todo : improve ?
                                deferred.resolve(res.data)
                            }, function(err) {
                                deferred.reject(err)
                            });

                            return deferred.promise;
                        },
                        remove: function remove(object, params) {
                            var deferred = $q.defer();

                            $http({
                                method: 'DELETE',
                                url: url + '/' + object.id + (params ? '?' + $.param(params) : '')
                            }).then(function(res) {
                                if (res.data.success) {
                                    deferred.resolve(res.data)
                                } else {
                                    if (res.data.errors.exceptions.length > 0) {
                                        var message = '';
                                        for (var i in res.data.errors.exceptions) {
                                            message += res.data.errors.exceptions[i] + '<br />';
                                        }
                                        flashMessenger.error(message);
                                    }
                                    deferred.reject(res.data.errors)
                                }
                            }, function(err) {
                                deferred.reject(err)
                            });

                            return deferred.promise;
                        },
                        removeAll: function removeAll(objects, params) {
                            var deferred = $q.defer();

                            $http({
                                method: 'DELETE',
                                url: url + (params ? '?' + $.param(params) : ''),
                                data: objects,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(res) {
                                //todo : improve ?
                                deferred.resolve(res.data)
                            }, function(err) {
                                deferred.reject(err)
                            });

                            return deferred.promise;
                        }
                    }
                }
            };
        }
    ])

})(window.EVA.app);
