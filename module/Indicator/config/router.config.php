<?php

namespace Indicator;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'indicator' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/indicator',
                    'defaults' => [
                        'controller' => 'Indicator\Controller\Index',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'Indicator\Controller\Index',
                                'action'     => 'form'
                            ]
                        ]
                    ]
                ]
            ],
            'group-indicator' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/group-indicator',
                    'defaults' => [
                        'controller' => 'Indicator\Controller\GroupIndicator',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'Indicator\Controller\GroupIndicator',
                                'action'     => 'form'
                            ]
                        ]
                    ]
                ]
            ],
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'indicator' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/indicator[/:id]',
                            'defaults' => [
                                'controller' => 'Indicator\Controller\API\Indicator'
                            ]
                        ],
                    ],
                    'measure' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/measure[/:id]',
                            'defaults' => [
                                'controller' => 'Indicator\Controller\API\Measure'
                            ]
                        ]
                    ],
                    'deployment' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/deployment[/:id]',
                            'defaults' => [
                                'controller' => 'Indicator\Controller\API\Deployment'
                            ]
                        ]
                    ],
                    'group-indicator' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/group-indicator[/:id]',
                            'defaults' => [
                                'controller' => 'Indicator\Controller\API\GroupIndicator'
                            ]
                        ],
                    ]
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class          => ServiceLocatorFactory::class,
            Controller\API\IndicatorController::class  => ServiceLocatorFactory::class,
            Controller\API\MeasureController::class    => ServiceLocatorFactory::class,
            Controller\API\DeploymentController::class => ServiceLocatorFactory::class,
            Controller\GroupIndicatorController::class => ServiceLocatorFactory::class,
            Controller\API\GroupIndicatorController::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Indicator\Controller\Index'               => Controller\IndexController::class,
            'Indicator\Controller\GroupIndicator'      => Controller\GroupIndicatorController::class,
            'Indicator\Controller\MeasureCampain'      => Controller\MeasureCampainController::class,
            'Indicator\Controller\API\Indicator'       => Controller\API\IndicatorController::class,
            'Indicator\Controller\API\Measure'         => Controller\API\MeasureController::class,
            'Indicator\Controller\API\Deployment'      => Controller\API\DeploymentController::class,
            'Indicator\Controller\API\GroupIndicator'   => Controller\API\GroupIndicatorController::class
        ],
    ],
];
