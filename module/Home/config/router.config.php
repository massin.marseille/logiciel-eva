<?php
namespace Home;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'root' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'Home\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
            ],
            'home' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/home',
                    'defaults' => [
                        'controller' => 'Home\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/form',
                            'defaults' => [
                                'controller' => 'Home\Controller\Index',
                                'action'     => 'form',
                            ],
                        ],
                    ],
                ],
            ],
            'api'  => [
                'child_routes' => [
                    'home'          => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/home[/:id]',
                            'defaults' => [
                                'controller' => 'Home\Controller\API\Home',
                            ],
                        ],
                    ],
                    'homeAdmin'     => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/homeAdmin[/:id]',
                            'defaults' => [
                                'controller' => 'Home\Controller\API\HomeAdmin',
                            ],
                        ],
                    ],
                    'homeContainer' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/homeContainer[/:id]',
                            'defaults' => [
                                'controller' => 'Home\Controller\API\Container',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class         => ServiceLocatorFactory::class,
            Controller\API\HomeController::class      => ServiceLocatorFactory::class,
            Controller\API\HomeAdminController::class => ServiceLocatorFactory::class,
            Controller\API\ContainerController::class => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Home\Controller\Index'         => Controller\IndexController::class,
            'Home\Controller\API\Home'      => Controller\API\HomeController::class,
            'Home\Controller\API\HomeAdmin' => Controller\API\HomeAdminController::class,
            'Home\Controller\API\Container' => Controller\API\ContainerController::class,
        ],
    ],
];
