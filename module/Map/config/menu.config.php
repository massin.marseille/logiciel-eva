<?php

$territories = [
    'icon' => 'territory',
    'title' => 'territory_module_title',
    'right' => 'map:e:territory:r',
    'href' => 'map/territory',
];

return [
    'menu' => [
        'client-menu' => [
            'map' => [
                'icon' => 'map',
                'title' => 'map_module_title',
                'children' => [
                    'territories' => $territories,
                ],
            ],
        ],
        'client-menu-parc' => [
            'data' => [
                'children' => [
                    'territories' => $territories,
                ],
            ],
        ],
    ]
];
