<?php

return [
    'icons' => [
        'users' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-users'
        ],
        'role' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-lock'
        ],
        'service' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-lock'
        ]
    ]
];
