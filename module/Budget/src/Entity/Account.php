<?php

namespace Budget\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JsonSerializable;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Account
 *
 * @package Budget\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity(repositoryClass="Budget\Repository\AccountRepository")
 * @ORM\Table(name="budget_account")
 */
class Account extends BasicRestEntityAbstract implements JsonSerializable
{
    const TYPE_EXPENSE = 'expense';
    const TYPE_INCOME  = 'income';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Account", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $parent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Budget\Entity\Account", mappedBy="parent")
     */
    protected $children;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="default_account", type="boolean", options={"default":"0"}, nullable=false)
     */
    protected $default = false;



    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param string $type
     */
    public function setTypeRecursive($type)
    {
        $this->type = $type;
        foreach ($this->getChildren() as $child) {
            $child->setTypeRecursive($type);
        }
    }

    /**
     * @return Account
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Account $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function addChildren(ArrayCollection $children)
    {
        foreach ($children as $child) {
            $child->setParent($this);
            $this->getChildren()->add($child);
        }
    }

    /**
     * @param ArrayCollection $children
     */
    public function removeChildren(ArrayCollection $children)
    {
        foreach ($children as $child) {
            $child->setParent(null);
            $this->getChildren()->removeElement($child);
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function setDefault($default)
    {
        $this->default = isset($default) ? $default : false;
    }


    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'code',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $queryBuilder = $entityManager->createQueryBuilder();
                                $queryBuilder->select('a')
                                    ->from('Budget\Entity\Account', 'a')
                                    ->where('a.code = :code')
                                    ->andWhere('a.id != :id')
                                    ->setParameters([
                                        'id'    => !isset($context['id']) ? '' : $context['id'],
                                        'code' => $value
                                    ]);

                                try {
                                    $account = $queryBuilder->getQuery()->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $account = null;
                                }

                                return $account === null;
                            },
                            'message' => 'account_field_code_error_unique'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'description',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'type',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getTypes()
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'parent',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $parent = $entityManager->getRepository('Budget\Entity\Account')->find($value);

                                return !empty($parent);
                            },
                            'message' => 'account_field_parent_error_non_exists'
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                return (is_numeric($value) ||
                                    (is_array($value) && isset($value['id']) && is_int($value['id'])) ||
                                    (is_object($value) && method_exists($value, 'getId') && is_numeric($value->getId()))) ;
                            },
                            'message' => 'project_field_id_expected',
                        ],
                    ],
                ],
            ],
            [
                'name'                  => 'default',
                'required'              => false,
                'validators'            => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if($value===1 || $value===0 || $value==='1' || $value==='0' || $value===true || $value===false) {
                                    if($value===1 || $value==='1' || $value===true) {
                                        $entityManager
                                            ->getRepository('Budget\Entity\Account')
                                            ->resetDefault($context['type']);
                                    }
                                    return true;
                                }

                                return false;
                            },
                            'message' => 'account_field_default_type',
                        ],
                    ],
                ]
            ]
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_EXPENSE,
            self::TYPE_INCOME
        ];
    }


    public function jsonSerialize() : mixed
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'default' => $this->default,
            'type' => $this->type,
            'parent' => null !== $this->parent ? $this->parent->getId() : null,
            'children' => $this->children->toArray()
        ];
    }

}
