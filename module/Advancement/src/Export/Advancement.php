<?php

namespace Advancement\Export;

use Core\Export\IExporter;

abstract class Advancement implements IExporter
{
    public static function getConfig()
    {
        return [
            'type'     => [
                'format' => function ($value, $row, $translator) {
                    return $translator('advancement_type_' . $value);
                },
            ],
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
