(function (app) {

    app.controller('exportFormWordController', [
        'exportService', 'attachmentService', '$scope', 'flashMessenger', 'translator', '$timeout', 'FileUploader',
        function exportFormWordController(exportService, attachmentService, $scope, flashMessenger, translator, $timeout, FileUploader) {
            var self       = this;
            var setByWatch = true;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'templates.id',
                    'templates.content',
                    'templates.type',
                    'templates.pageBreak',
                    'reference',
                    'year',
                    'type',
                    'showArbo',
                    'dataArbo',
                    'groupByKeywords',
                    'groupByKeywordsTitles'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.picturesGlobal = [];
            self.idClient       = null;
            self.pictureGlobal  = null;

            self.init = function init(id, idClient, type) {
                if (id) {
                    setByWatch = true;
                    exportService.find(id, apiParams).then(function (res) {
                        self.export     = res.object;
                        self.panelTitle = self.export.name;

                        self.currentTemplate = self.export.templates[0];
                    })
                } else {
                    self.export          = {
                        _rights  : {
                            update: true
                        },
                        reference: 'initial',
                        templates: [
                            {
                                type     : 'leaf',
                                pageBreak: 'both'
                            },
                            {
                                type     : 'node',
                                pageBreak: 'both'
                            },
                            {
                                type     : 'parent',
                                pageBreak: 'both'
                            }
                        ],
                        type: type
                    };
                    self.currentTemplate = self.export.templates[0];
                }

                attachmentService.findAll({
                    right : 'project:e:project',
                    col   : [
                        'id',
                        'name',
                    ],
                    search: {
                        type: 'list',
                        data: {
                            filters: {
                                entity: {
                                    op : 'eq',
                                    val: 'Bootstrap\\Entity\\Client',
                                },
                            },
                            sort   : 'id',
                            order  : 'asc'
                        }
                    }
                }).then(function (res) {
                    self.picturesGlobal = res.rows;
                });

                self.idClient = idClient;
                self.initPictureGlobal();

                self.pictureGlobal.primary = idClient;
            };

            self.initPictureGlobal = function () {
                self.pictureGlobal = {
                    type   : 'file',
                    entity : 'Bootstrap\\Entity\\Client',
                    primary: self.idClient
                };
            };

            self.savePictureGlobal = function savePictureGlobal() {
                self.loading.save = true;

                attachmentService.save(self.pictureGlobal, {
                    right: 'project:e:project',
                    col  : [
                        'id',
                        'entity',
                        'primary',
                        'name',
                        'url',
                        'type',
                        'createdBy.id',
                        'createdBy.name',
                        'createdAt'
                    ]
                }).then(function (res) {
                    self.picturesGlobal.push(res.object);

                    self.initPictureGlobal();

                    flashMessenger.success(translator('picture_message_saved'));
                    angular.element('#params_addPictureGlobal').modal('hide');

                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                });
            };

            self.deletePictureGlobal = function (id, name) {
                if (confirm(translator('attachment_question_delete').replace('%1', name))) {
                    var pictureGlobal = null;
                    var index         = null;
                    for (var i in self.picturesGlobal) {
                        if (id === self.picturesGlobal[i].id) {
                            pictureGlobal = self.picturesGlobal[i];
                            index         = i;
                        }
                    }
                    if (pictureGlobal) {
                        attachmentService.remove(pictureGlobal, {right: 'export:e:export'}).then(function (res) {
                            if (res.success) {
                                self.picturesGlobal.splice(index, 1);
                            } else {
                                flashMessenger.error(translator('error_occured'));
                            }
                        });
                    }
                }
            };

            self.saveExport = function saveExport() {
                self.errors.fields = {};
                self.loading.save  = true;
                self.save();
            };

            self.save = function save() {
                var isCreation = !self.export.id;

                if (self.export.year == 0) {
                    self.export.year = '';
                }

                exportService.save(self.export, apiParams).then(function (res) {
                    self.export       = res.object;
                    self.panelTitle   = self.export.name;
                    self.loading.save = false;
                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.export.id;
                        history.replaceState('', '', url);
                    }

                    self.currentTemplate = self.export.templates[0];

                    flashMessenger.success(translator('export_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteExport = function deleteExport() {
                if (confirm(translator('export_question_delete').replace('%1', self.export.name))) {
                    exportService.remove(self.export).then(function (res) {
                        if (res.success) {
                            window.location = '/export';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.addTemplate = function addTemplate() {
                self.export.templates.push({});
            };

            $scope.$watch(function () {
                return self.export ? self.export.reference : null
            }, function (newVal, oldVal) {
                if (newVal && oldVal && !setByWatch) {
                    if (confirm(translator('export_question_change_reference'))) {
                        if (newVal == 'initial') {
                            self.export.templates = [
                                {
                                    type     : 'leaf',
                                    pageBreak: 'both'
                                },
                                {
                                    type     : 'node',
                                    pageBreak: 'both'
                                },
                                {
                                    type     : 'parent',
                                    pageBreak: 'both'
                                }
                            ];
                        } else {
                            self.export.templates = [
                                {
                                    type     : 'leaf',
                                    pageBreak: 'both'
                                },
                                {
                                    type     : 'leaf_project',
                                    pageBreak: 'both'
                                },
                                {
                                    type     : 'node',
                                    pageBreak: 'both'
                                },
                                {
                                    type     : 'node_project',
                                    pageBreak: 'both'
                                },
                                {
                                    type     : 'parent',
                                    pageBreak: 'both'
                                },
                                {
                                    type     : 'parent_project',
                                    pageBreak: 'both'
                                }
                            ];
                        }

                        self.currentTemplate = self.export.templates[0];
                    } else {
                        self.export.reference = oldVal;
                        setByWatch            = true;
                        return;
                    }
                }
                setByWatch = false;
            });

            /*self.insertVariables = function insertVariables() {
                for(var i in self.variables) {
                    self.editor.insertContent('<p>$' + self.variables[i] + '$</p>');
                }
                angular.element('#variablesModal').modal('hide');
            };

            self.toggleVariable = function toggleVariable(variable) {
                var idx = self.variables.indexOf(variable);
                if (idx > -1) {
                    self.variables.splice(idx, 1);
                } else {
                    self.variables.push(variable);
                }
            };*/

            self.variablesParams    = {};
            self.configuredVariable = null;
            self.configureVariable  = function configureVariable(variable) {
                var $modal = angular.element('#params_' + variable.replace(/(\()(.*)(\))/, '_$2').replace(',', '_'));

                if ($modal.length > 0) {
                    self.configuredVariable = variable;
                    self.variablesParams    = {};

                    var params = $modal.find('input, select');
                    for (var i = 0; i < params.length; i++) {
                        var name                   = $(params[i]).attr('name');
                        self.variablesParams[name] = null;
                    }

                    $modal.modal('show');
                } else {
                    self.insertVariable(variable);
                    $modal.modal('hide');
                }
            };

            self.insertConfiguredVariable = function insertConfiguredVariable() {
                var variable = self.configuredVariable;
                var params   = [];
                for (var i in self.variablesParams) {
                    var param = self.variablesParams[i];
                    if (angular.isArray(param)) {
                        param = '[' + param.join(',') + ']'
                    }
                    params.push(param || '');
                }
                // Check if the variable has already parameters and if it's the case, it adds the new parameters
                if (variable.substr(variable.length - 1) === ')') {
                    variable = variable.replace(/.$/, ",");
                    self.insertVariable(variable + params.join(',') + ')');
                } else {
                    self.insertVariable(variable + '(' + params.join(',') + ')');
                }
            };

            self.insertVariable = function insertVariable(variable) {
                var editor = self.getEditor();
                editor.insertContent('$' + variable + '$');
            };

            self.getEditor = function () {
                var id = $('.tab-pane.active').find('textarea[ui-tinymce]').attr('id');
                return tinyMCE.get(id);
            };

            self.uploader = new FileUploader();
            self.uploader.filters.push({
                name: 'imageChecker',
                fn  : function (item) {
                    return self.fileIsImage(item.name);
                }
            });

            self.reader = new FileReader();

            self.reader.onloadend = function (e) {
                self.pictureGlobal.file = e.target.result;
            };

            self.uploader.onAfterAddingFile = function (fileItem) {
                self.reader.readAsDataURL(fileItem._file);
                self.pictureGlobal.filename      = fileItem.file.name;
                self.pictureGlobal.size          = fileItem.file.size;
                self.errors.fields['attachment'] = null;
            };

            self.fileIsImage = function (filename) {
                var type = '|' + filename.slice(filename.lastIndexOf('.') + 1).toLowerCase() + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            };

            // Permet de sélectionner le même fichier plusieurs fois de suite
            FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
                return true;
            };
        }
    ]);

    app.directive('filterList', function ($timeout) {
        return {
            link: function (scope, element, attrs) {
                var lists = element.find('ul');

                function filterBy(value) {
                    var listsHidden = 0;
                    for (var i = 0; i < lists.length; i++) {
                        var list           = lists[i];
                        var elements       = lists[i].children;
                        var elementsHidden = 0;
                        for (var j = 0; j < elements.length; j++) {
                            var el       = elements[j];
                            var show     = el.textContent.toLowerCase().indexOf(value.toLowerCase()) !== -1;
                            el.className = show ? '' : 'ng-hide';
                            if (!show) {
                                elementsHidden++;
                            }
                        }

                        if (elementsHidden == elements.length) {
                            $(list).parent().addClass('ng-hide');
                            listsHidden++;
                        } else {
                            $(list).parent().removeClass('ng-hide');
                        }
                    }

                    if (listsHidden == lists.length) {
                        element.addClass('ng-hide');
                    } else {
                        element.removeClass('ng-hide');
                    }
                }

                scope.$watch(attrs.filterList, function (newVal, oldVal) {
                    if (newVal !== oldVal) {
                        filterBy(newVal);
                    }
                });
            }
        };
    });

})(window.EVA.app);
