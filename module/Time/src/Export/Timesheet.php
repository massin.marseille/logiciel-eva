<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 16/08/16
 * Time: 10:00
 */

namespace Time\Export;

use Core\Export\IExporter;

abstract class Timesheet implements IExporter
{
    public static function getConfig()
    {
        return [
            'user.name' => [
                'name' => 'user',
            ],
            'project.name' => [
                'name' => 'project',
            ],
            'project.code' => [
                'translation' => 'project_field_code',
            ],
            'project.hierarchySup.name' => [
                'translation' => 'project_field_parent',
            ],
        ];
    }

    public static function getAliases()
    {
        return [
            'project_parent.name' => 'project.parent.name',
        ];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}