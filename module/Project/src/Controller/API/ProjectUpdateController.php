<?php

namespace Project\Controller\API;

use Core\Controller\BasicRestController;
use Laminas\View\Model\JsonModel;

class ProjectUpdateController extends BasicRestController {

    public function getHistoryAction()
    {
        $projectId = $this->params()->fromRoute('id');
        $updates = $this->getEntityManager()
                    ->getRepository('Project\Entity\ProjectUpdate')
                    ->findByProjectId($projectId);

        $rows = array_map(function ($entry) {
            $user = $entry->getUser();
            return [
                'date' => $entry->getDate()->format('c'),
                'user' => $user->getFirstName() . ' ' . $user->getLastName()
            ];
        }, $updates);

        return new JsonModel([
            "rows" => $rows,
            "total" => sizeOf($rows),
            "success" => true
        ]);
    }
}
