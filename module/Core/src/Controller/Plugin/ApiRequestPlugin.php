<?php

namespace Core\Controller\Plugin;

use Laminas\Http\PhpEnvironment\Request;
use Laminas\Mvc\Controller\ControllerManager;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Stdlib\Parameters;
use Laminas\View\Model\JsonModel;

class ApiRequestPlugin extends AbstractPlugin
{
	/**
	 * @var ControllerManager
	 */
	protected $controllerManager;

	protected $request;
	protected $controller;
	protected $event;

	/**
	 * @param ControllerManager $controllerManager
	 */
	public function __construct(ControllerManager $controllerManager)
	{
		$this->controllerManager = $controllerManager;
	}

	public function createRequest($method, $params)
	{
		$request = new Request();
		$request->setMethod($method);
		$request->setQuery(new Parameters($params));

		return $request;
	}

	public function createEvent()
	{
		$routeMatch = new RouteMatch([]);
		$event = new MvcEvent();
		$event->setRouteMatch($routeMatch);

		return $event;
	}

	public function createController($controllerName, $entityManager = null)
	{
		$controller = $this->controllerManager->get($controllerName);

		if ($entityManager) {
			$controller->setEntityManager($entityManager);
		}

		return $controller;
	}

	public function __invoke($controllerName, $method, $params, $entityManager = null)
	{
		$this->request = $this->createRequest($method, $params);
		$this->event = $this->createEvent();
		$this->controller = $this->createController($controllerName, $entityManager);
		$this->controller->setEvent($this->event);

		return $this;
	}

	/**
	 * @return JsonModel
	 */
	public function dispatch()
	{
		return $this->controller->dispatch($this->request);
	}

	/**
	 * @return mixed
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * @return mixed
	 */
	public function getController()
	{
		return $this->controller;
	}

	/**
	 * @return mixed
	 */
	public function getEvent()
	{
		return $this->event;
	}
}
