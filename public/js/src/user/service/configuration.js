(function(app) {

    app.factory('userConfigurationService', [
        '$http', '$q', 'flashMessenger',
        function userConfigurationService($http, $q, flashMessenger) {
            return {
                getConfiguration: function getConfiguration() {
                    var url = '/api/user-configuration/get-configuration';

                    var deferred = $q.defer();

                    $http({
                        method: 'GET',
                        url: url
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                },
                getConfigurationWithTable: function getConfigurationWithTable(name) {
                    var url = '/api/user-configuration/get-configuration-with-table';

                    var params = {
                        name: name
                    };

                    var deferred = $q.defer();

                    $http({
                        method: 'GET',
                        url: url + (params ? '?' + $.param(params) : '')
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                },
                getTable: function getTable (name) {
                    var url = '/api/user-configuration/get-table';

                    var params = {
                        name: name
                    };

                    var deferred = $q.defer();

                    $http({
                        method: 'GET',
                        url: url + (params ? '?' + $.param(params) : '')
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                },
                saveTable: function saveTable(name, cols) {
                    var url = '/api/user-configuration/save-table';

                    var params = {
                        name: name,
                        cols: cols
                    };

                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param(params)
                    }).then(function(res) {
                        if (res.data.success) {
                            deferred.resolve(res.data)
                        } else {
                            if (res.data.errors.exceptions.length > 0) {
                                var message = '';
                                for (var i in res.data.errors.exceptions) {
                                    message += res.data.errors.exceptions[i] + '<br />';
                                }
                                flashMessenger.error(message);
                            }
                            deferred.reject(res.data.errors)
                        }
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                },
                saveCustomisation: function saveCustomisation(customisation) {
                    var url = '/api/user-configuration/save-customisation';

                    var params = {
                        customisation: customisation
                    };
                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param(params)
                    }).then(function(res) {
                        if (res.data.success) {
                            deferred.resolve(res.data)
                        } else {
                            if (res.data.errors.exceptions.length > 0) {
                                var message = '';
                                for (var i in res.data.errors.exceptions) {
                                    message += res.data.errors.exceptions[i] + '<br />';
                                }
                                flashMessenger.error(message);
                            }
                            deferred.reject(res.data.errors)
                        }
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                }
            }
        }
    ])

})(window.EVA.app);
