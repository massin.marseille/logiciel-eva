<?php

namespace Core\View\Helper;

use Core\Module\MyModuleManager;
use Laminas\View\Helper\AbstractHelper;

class Module extends AbstractHelper
{
    /**
     * @var MyModuleManager
     */
    protected $moduleManager;

    public function __construct(MyModuleManager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }

    public function __invoke()
    {
        return $this;
    }

    public function isActive($module)
    {
        return $this->moduleManager->isActive($module);
    }

    public function getClientModuleConfiguration($module, $key)
    {
        return $this->moduleManager->getClientModuleConfiguration($module, $key);
    }
}
