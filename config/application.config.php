<?php

/**
 * Retrieve the APP_ENV value.
 * Defined in public/.htaccess file.
 */
$env = getenv('APP_ENV') ?: 'development';

return [
    // An array of modules namespaces to load.
    'modules' => require __DIR__ . '/modules.config.php',
    'module_listener_options' => [
        // An array of paths in which modules reside.
        'module_paths' => [
            './module',
            './vendor',
        ],
        // An array of configuration files to load after modules are loaded.
        'config_glob_paths' => [
            realpath(__DIR__) . '/autoload/{{,*.}global,{,*.}local}.php',
        ],
        /**
         * Enable or not the configuration cache.
         * Configuration cache is enabled only in production env.
         */
        'config_cache_enabled' => ($env === 'production'),
        /**
         * Enable or not the class map cache.
         * Class map cache is enabled only in production env.
         */
        'module_map_cache_enabled' => ($env === 'production'),
        //The path in which to store cache.
        'cache_dir' => 'data/cache',
        /**
         * Enable modules dependency checking.
         * Modules dependency checking is enabled only in development env.
         */
        'check_dependencies' => ($env === 'development'),
    ]
];
