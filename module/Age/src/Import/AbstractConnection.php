<?php

namespace Age\Import;

/**
 * Class AbstractConnection.
 *
 * Create a connection to a AGE database
 *
 * @package Age\Import
 */
abstract class AbstractConnection extends \PDO
{
    protected $identifier;

    private $financialYears = null;

    // AGE table and column to retrieve project code are specific to PN / Ademe
    protected $switchCode = false;
    protected $codeTable;
    protected $codeJoin;
    protected $codeColumn;
    protected $level;

    protected $debug = false;

    public function __construct($configuration, $dsn, $username = null, $passwd = null, $options = null)
    {
        parent::__construct($dsn, $username, $passwd, $options);

        $this->identifier = $this->etablissement($configuration['identifier']);
        if (!$this->identifier) {
            throw new \Exception('No etablissement with this AGE identifier');
        }
        $this->initCodeTableAndColumn($configuration);
        $this->initFinancialYears($configuration);
    }


    private function initCodeTableAndColumn($configuration)
    {
        /** @var boolean */
        $this->switchCode = $this->checkConfigurationProperty($configuration, 'switchCode') && $configuration['switchCode'];

        $this->codeColumn = $this->switchCode ? "CODE_ANALYTIQUE_CODE" : "CODE";
        $this->codeTable = $this->switchCode ? "ref_analytique" : "ref_operations";
        $this->level = $this->checkConfigurationProperty($configuration, 'level') ? $configuration['level'] : '0';
        $this->codeJoin = $this->switchCode ? "CB.ANALYTIQUE_ID = RO.ID AND RO.NIVEAU_ANALYTIQUE_ORDRE_DE_PRESENTATION = '".$this->level."'" : "RO.ID = CB.OPERATION_ID";
    }

    private function initFinancialYears($configuration)
    {
        if(!$this->checkConfigurationProperty($configuration, 'firstYear')) {
            return;
        }
        $lastYear = $this->checkConfigurationProperty($configuration, 'lastYear') ? $configuration['lastYear'] : date('Y');
        $financialYears = [];
        // create the years chain for the SQL IN WHERE clause
        for ($i = intval($configuration['firstYear']); $i <= intval($lastYear); $i++) {
            $financialYears[] = strval($i);
        }
        $this->financialYears = $financialYears;
    }

    private function checkConfigurationProperty($configuration, $property)
    {
        return array_key_exists($property, $configuration) && null !== $configuration[$property] && '' !== $configuration[$property];
    }

    public function getFinancialYears()
    {
        return $this->financialYears;
    }

    public function getFinancialYearsAsString()
    {
        if (null === $this->financialYears) {
            return null;
        }
        $chain = implode("', '", $this->financialYears);
        return "'" . $chain . "'";
    }

    /**
     * Retrieve the number of operations with its code
     * 
     * @param string code
     * 
     * @return integer
     */
    public function countOperationsByCode($code)
    {
        $statement = $this->prepare('SELECT COUNT(1) FROM ref_operations WHERE CODE = :code;');
        $statement->execute([':code' => $code]);
        return intval($statement->fetchColumn());
    }

    /**
     * Retrieve structure id from libelle.
     * Used to filters rows.
     *
     * @param $libelle
     *
     * @return bool
     */
    abstract protected function etablissement($libelle);

    /**
     * Retrieve budget accounts.
     *
     * @return array
     */
    abstract public function accounts();

    /**
     * Retrieve Payment Request
     * 
     * @return array
     */
    abstract public function expenseDv();

    /**
     * Retrieve TYPE_AE_COMMITTED expenses.
     *
     * @return array
     */
    abstract public function expenseAeCommitted();

    abstract public function expenseAe();

    abstract public function expenseCp();

    /**
     * Retrieve TYPE_CP_ESTIMATED expenses.
     *
     * @return array
     */
    abstract public function expenseCpEstimated();

    /**
     * Retrieve TYPE_CP_COMMITTED expenses.
     *
     * @return array
     */
    abstract public function expenseCpCommitted();

    /**
     * Retrieve Payment Request
     * 
     * @return array
     */
    abstract public function incomeDv();

    /**
     * Retrieve TYPE_PERCEIVED incomes.
     *
     * @return array
     */
    abstract public function incomePerceived();

    /**
     * Retrieve TYPE_REQUESTED incomes.
     *
     * @return array
     */
    abstract public function incomeRequested();
}
