(function(app) {

    app.factory('memberService', [
        'restFactoryService',
        function memberService(restFactoryService) {
            return restFactoryService.create('/api/member');
        }
    ])

})(window.EVA.app);
