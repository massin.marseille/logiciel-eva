(function(app) {

    app.directive('inputDate', [
        '$timeout',
        function inputDate($timeout) {
            return {
                restrict: 'A',
                require: 'ngModel',
                scope: {
                    mask: '@inputDateMask',
                },
                replace: false,
                transclude: true,
                compile: function (element) {
                    var $input = angular.element(element);
                    $input.wrap('<div class="input-group date"></div>');
                    $input.before('<div class="input-group-addon"><span class="fa fa-calendar"></span></div>');
                    $input.removeAttr('input-date');

                    return function (scope, element, attributes, ngModelCtrl) {
                        var format = (scope.mask == 'datetime' ? 'DD/MM/YYYY HH:mm' : 'DD/MM/YYYY');
                        var mask   = (scope.mask == 'datetime' ? 'qdatetime' : 'qdate');

                        angular.element(element).find('input').inputmask(mask);
                        angular.element(element).find('input').parent('.input-group').datetimepicker({
                            format: format
                        }).on('dp.change', function(e) {
                            // update the ng-model value
                            ngModelCtrl.$setViewValue(e.date.format(format));
                            ngModelCtrl.$render();
                        }).on('dp.hide', function(e) {
                            ngModelCtrl.$setViewValue(e.date.format(format));
                            ngModelCtrl.$render();
                        });
                    }
                }
            }
        }
    ])

})(window.EVA.app);
