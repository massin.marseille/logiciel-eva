<?php

namespace Keyword\View\Helper;

use Keyword\Controller\Plugin\AllowedKeywordGroups;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class AllowedKeywordGroupsHelper
 *
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AllowedKeywordGroupsHelper extends AbstractHelper
{
    /**
     * @var AllowedKeywordGroups
     */
    protected $allowedKeywordGroups;

    /**
     * @param AllowedKeywordGroups $allowedKeywordGroups
     */
    public function __construct(AllowedKeywordGroups $allowedKeywordGroups)
    {
        $this->allowedKeywordGroups = $allowedKeywordGroups;
    }

    public function __invoke($entity, $archived = null, $search = null)
    {
        return $this->allowedKeywordGroups->findByEntity($entity, $archived, $search);
    }
}
