<?php

namespace Project\Event;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Project\Entity\Project;
use Laminas\ServiceManager\ServiceLocatorInterface;

class LevelSubscriber implements EventSubscriber
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        $recomputeConfiguration = false;
        $maxLevel = 0;

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Project) {
                $level = $entity->computeLevel();
                if ($level > $maxLevel) {
                    $maxLevel = $level;
                }
                $recomputeConfiguration = true;
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Project) {
                $level = $entity->computeLevel();
                if ($level > $maxLevel) {
                    $maxLevel = $level;
                }
                $recomputeConfiguration = true;
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }

        if ($recomputeConfiguration) {
            $configuration = $this->serviceLocator->get('ClientPrivateConfiguration');
            $modules = $configuration->getModules();
            if (!isset($modules['project']['levels'])) {
                $modules['project']['levels'] = [];
            }

            $baseMaxLevel = $em->getRepository('Project\Entity\Project')->findMaxLevel();
            if ($baseMaxLevel > $maxLevel) {
                $maxLevel = $baseMaxLevel;
            }

            for ($i = 0; $i <= $maxLevel; $i++) {
                if (!isset($modules['project']['levels'][$i])) {
                    $modules['project']['levels'][$i] = [
                        'name' => 'Niveau ' . $i
                    ];
                }
            }

            $configuration->setModules($modules);
        }
    }

    public function getSubscribedEvents()
    {
        return [
            Events::onFlush
        ];
    }
}
