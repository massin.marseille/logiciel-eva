(function(app) {

    app.directive('territoriesFormGroup', [
        function territoriesFormGroup() {
            return {
                restrict: 'E',
                scope: {
                    ngModel: '=ngModel'
                },
                replace: false,
                transclude: true,
                template: '<territory-select multiple="true" ng-model="ngModel.territories" ng-disabled="!ngModel._rights.update"></territory-select>',
                link: function (scope) {
                    scope.$watch('ngModel.territories', function () {
                        if (typeof scope.ngModel.territories == 'undefined') {
                            scope.ngModel.territories = [];
                        }
                    });
                }
            }
        }
    ])

})(window.EVA.app);
