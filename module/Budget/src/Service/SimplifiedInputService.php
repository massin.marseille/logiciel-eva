<?php

namespace Budget\Service;

use ArrayIterator;
use Bootstrap\Entity\Client;
use Budget\Entity\Account;
use Budget\Entity\Expense;
use Budget\Entity\Nature;
use Budget\Entity\PosteExpense;
use DateTime;
use Exception;
use Project\Entity\Project;
use User\Auth\AuthStorage;

class SimplifiedInputService
{

    private $_client;
    private $_entityManager;
    private $_authStorage;
    private $_translator;


    public function __construct(Client $client, $entityManager, AuthStorage $authStorage, $translator)
    {
        $this->_client = $client;
        $this->_entityManager = $entityManager;
        $this->_authStorage = $authStorage;
        $this->_translator = $translator;
    }


    /**
     * Retrieve the write years to generate
     * 
     * @param integer $start, the first year to write
     * @param integer $end, the last year to write
     * 
     * @return mixed the write years
     */
    public function getNewYears($start, $end)
    {
        $years = array($start);

        $increment = $end - $start;
        for ($i = 1; $i < $increment; $i++) {
            $years[] = $start + $i;
        }
        if ($start !== $end) {
            $years[] = $end;
        }
        sort($years);
        return $years;
    }

    /**
     * Retrieve the latest exercise year of project's expenses
     * 
     * @param integer $projectId
     * 
     * @return integer|boolean
     */
    public function getLatestExerciseYear($projectId)
    {
        $postes = $this->getPostes($projectId);
        $years = $this->getExerciseYears($postes);
        return end($years);
    }

    /**
     * Generate the table for read / edition
     * 
     * @param integer   $projectId
     * @param array     $writeYears (optional), defines the years to be edited
     * @param integer   $refId (optional), defines the postes' referential to generate the lines in table
     * 
     * @return mixed the years to display and the generated table
     */
    public function generateSimplifiedView($projectId, $writeYears = [], $refId = null)
    {
        $postes         = $this->getPostes($projectId);
        $readYears      = $this->getExerciseYears($postes);
        $categories     = $this->mergeCategories($postes, $refId);

        $table = $this->generateTable($categories, $readYears, $writeYears);

        return [
            'years' => self::mergeYears($readYears, $writeYears),
            'table' => $table
        ];
    }

    /**
     * Inject edited data into database
     * 
     * @param integer   $projectId
     * @param mixed     $table, the data to import
     * @param mixed     $years, all table years (to loop on)
     * 
     */
    public function importSimplifiedView($projectId, $table, $years)
    {
        $project = $this->getProject($projectId);
        foreach ($table as $line) {
            if ($line->type !== 'poste') {
                continue;   // do not inject total lines
            }
            /** @var PosteExpense */
            $posteExpense = null;
            if (null === $line->poste) {
                $posteExpense = $this->createPosteExpense($line, $project);     // poste expense from referential to be created
            } else {
                $posteExpense = $this->getPoste($line->poste);
                $posteExpense->setFinancing($line->financing);
            }
            $this->_entityManager->persist($posteExpense);

            foreach ($years as $year) {
                if ($line->$year->readOnly) {
                    continue;   // do not import years that cannot be edited (already in db)
                }
                /** @var Expense */
                $expense = $this->createExpense($line, $year, $posteExpense);
                $this->_entityManager->persist($expense);
            }
        }
        $this->_entityManager->flush();
    }


    /**
     * Generate the table for Word export
     * 
     * @param Project $project
     * @param integer $startYear, defines the first year to display in table
     * @param integer $endYear, defines the last year to display in table
     * 
     * @return mixed the years to display, the generated table and the client name
     */
    public function exportSimplifiedView($project, $startYear, $endYear)
    {
        $postes         = $this->getPostesFromProject($project);
        $exportYears    = $this->getExportYears($this->getExerciseYears($postes), $startYear, $endYear);
        $categories     = $this->mergeCategories($postes);

        $table = $this->generateTable($categories, $exportYears, []);

        return [
            'years' => $exportYears,
            'table' => $table,
            'client' => $this->_client->getName()
        ];
    }




    /**
     * Create a new poste expense object from table line
     * 
     * @return PosteExpense
     */
    private function createPosteExpense($line, $project)
    {
        $posteExpense = new PosteExpense();
        $posteExpense->setProject($project);
        $posteExpense->setName($line->name);
        $posteExpense->setAmount(0.0);
        $posteExpense->setFinancing($line->financing);
        $posteExpense->setNature($this->getNature($line->category));
        if (null !== $line->account) {
            $posteExpense->setAccount($this->getAccount($line->account));
        }
        $posteExpense->setCreatedAt(new DateTime('now'));
        $posteExpense->setCreatedBy($this->_authStorage->getUser());

        return $posteExpense;
    }

    /**
     * Create a new expense object from a table line
     * 
     * @return Expense
     */
    private function createExpense($line, $year, $posteExpense)
    {
        $expense = new Expense();
        $expense->setPoste($posteExpense);
        $expense->setName($line->name);
        $expense->setAmount($line->$year->amount);
        $expense->setAnneeExercice(strval($year));
        $expense->setType($this->getExpenseType());
        $expense->setDate(new DateTime('now'));
        $expense->setCreatedAt(new DateTime('now'));
        $expense->setCreatedBy($this->_authStorage->getUser());

        return $expense;
    }


    private function getExerciseYears($postes)
    {
        $years = [];
        foreach ($postes as $poste) {
            $expenses = $poste->getExpenses();
            if (!isset($expenses)) {
                continue;
            }
            foreach ($expenses as $expense) {
                $year = intval($expense->getAnneeExercice());
                if (0 !== $year) {
                    $years[] = $year;
                }
            }
        }
        $years = array_unique($years);
        sort($years);
        return $years;
    }


    /** 
     * @var Project 
     */
    private function getProject($projectId)
    {
        $project = $this->_entityManager->getRepository('Project\Entity\Project')->findOneBy(array('id' => $projectId));
        if (!isset($project)) {
            throw new Exception();
        }
        return $project;
    }

    /**
     * @var PosteExpense[]
     */
    private function getPostes($projectId)
    {
        $project = $this->getProject($projectId);
        return $this->getPostesFromProject($project);
    }

    /**
     * @var PosteExpense[]
     */
    private function getPostesFromProject($project)
    {
        /** @var ArrayIterator */
        $iterator = $project->getPosteExpenses()->getIterator();
        $iterator->uasort(function ($exp1, $exp2) {
            if (null === $exp1->getNature() && null === $exp2->getNature()) {
                return 0;
            }
            if (null === $exp1->getNature()) {
                return -1;
            }
            if (null === $exp2->getNature()) {
                return 1;
            }
            return $exp1->getNature()->getId() - $exp2->getNature()->getId();
        });
        return iterator_to_array($iterator);
    }

    private function getReferenceCategories($id)
    {
        if (!isset($id)) {
            return [];
        }
        $reference = $this->_entityManager->getRepository('Budget\Entity\PosteExpenseRef')->findOneBy(array('id' => $id));
        return isset($reference) ? $reference->getChildren() : [];
    }


    private function mergeCategories($postes, $referenceId = null)
    {
        $referenceCategories = $this->getReferenceCategories($referenceId);
        // index reference categories by category and name
        $refs = [];
        foreach ($referenceCategories as $referenceCategory) {
            $refs[$referenceCategory->getNature()->getId()][$referenceCategory->getName()] = $referenceCategory;
        }

        $categories = [];
        foreach ($postes as $poste) {
            $categoryId = null !== $poste->getNature() ? $poste->getNature()->getId() : 0;
            $categoryName = null !== $poste->getNature() ? $poste->getNature()->getName() : $this->_translator->translate('poste_expense_simplified_input_category_other');
            if (!array_key_exists($categoryId, $categories)) {
                $categories[$categoryId] = array('name' => $categoryName, 'postes' => array());
            }
            $categories[$categoryId]['postes'][] = array('fake' => false, 'data' => $poste);

            // removes reference category if found in poste
            if (array_key_exists($categoryId, $refs) && array_key_exists($poste->getName(), $refs[$categoryId])) {
                unset($refs[$categoryId][$poste->getName()]);
            }
        }
        foreach ($refs as $categoryId => $ref) {
            foreach ($ref as $name => $data) {
                if (!array_key_exists($categoryId, $categories)) {
                    $categories[$categoryId] = array('name' => $data->getNature()->getName(), 'postes' => array());
                }
                $categories[$categoryId]['postes'][] = array(
                    'fake' => true,
                    'data' => [
                        'categoryId' => $categoryId,
                        'name' => $name,
                        'accountId' => null !== $data->getAccount() ? $data->getAccount()->getId() : null
                    ]
                );
            }
        }

        krsort($categories);
        return $categories;
    }


    private function createTotalLine($totals, $readYears, $writeYears, &$table, $name = null, $category = null)
    {
        $financing = 100.0;
        if (0.0 !== ($totals['client'] + $totals['sponsor'])) {
            $financing = round(100 * $totals['client'] / ($totals['client'] + $totals['sponsor']), 2);
        }
        $line = array(
            'category' => $category,
            'type' => 'total',
            'name' => isset($name) ? $this->_translator->translate('poste_expense_simplified_input_subtotal') . ' ' .  $name : strtoupper($this->_translator->translate('poste_expense_simplified_input_total')),
            'total' => $totals['total'],
            'financing' => $financing,
            'client' => $totals['client'],
            'sponsor' => $totals['sponsor']
        );
        foreach ($writeYears as $year) {
            $line[$year] = $totals[$year];
        }
        foreach ($readYears as $year) {
            $line[$year] = $totals[$year];
        }
        $table[] = $line;
    }

    private function getExpenseType()
    {
        $budgetConfig = $this->_client->getModules()['budget']['configuration'];
        return array_key_exists('simplifiedInputExpense', $budgetConfig)
            ? $budgetConfig['simplifiedInputExpense']
            : Expense::TYPE_AE_ESTIMATED;
    }


    private function createRealExpenseLine($poste, $categoryId, $readYears, $expenseType, $writeYears, &$table)
    {
        $totals = self::initTotalObject($readYears, $writeYears);

        $line = array(
            'type' => 'poste',
            'category' => $categoryId,
            'account' => null !== $poste->getAccount() ? $poste->getAccount()->getId() : null,
            'poste' => $poste->getId(),
            'name' => $poste->getName(),
            'financing' => $poste->getFinancing()
        );
        $total = 0.0;
        foreach ($writeYears as $year) {
            $line[$year] = ['amount' => 0.0, 'readOnly' => false];  // init to zero for new columns
        }
        foreach ($readYears as $year) {
            $yearAmount = $poste->getTotalExpenses($expenseType, $year);
            $line[$year] = ['amount' => $yearAmount, 'readOnly' => true];
            $total += $yearAmount;
            $totals[$year]['amount'] = $yearAmount;
        }
        $line['total'] = $total;
        $line['client'] = round($line['total'] * ((100.0 - $line['financing']) / 100.0), 2);
        $line['sponsor'] = round($line['total'] * ($line['financing'] / 100.0), 2);

        $table[] = $line;

        $totals['total'] = $line['total'];
        $totals['client'] = $line['client'];
        $totals['sponsor'] = $line['sponsor'];
        return $totals;
    }


    private function createFakeExpenseLine($poste, $readYears, $writeYears, &$table)
    {
        $totals = self::initTotalObject($readYears, $writeYears);

        $line = array(
            'type' => 'poste',
            'category' => $poste['categoryId'],
            'account' => $poste['accountId'],
            'poste' => null,
            'name' => $poste['name'],
            'total' => 0.0,
            'financing' => 100.0,
            'client' => 0.0,
            'sponsor' => 0.0
        );
        foreach ($writeYears as $year) {
            $line[$year] = ['amount' => 0.0, 'readOnly' => false];
        }
        foreach ($readYears as $year) {
            $line[$year] = ['amount' => 0.0, 'readOnly' => true];
        }

        $table[] = $line;

        return $totals;
    }


    private function generateTable($categories, $readYears, $writeYears)
    {
        $expenseType = $this->getExpenseType();

        $table = [];

        $totals = self::initTotalObject($readYears, $writeYears);
        foreach ($categories as $id => $category) {
            $subTotals = self::initTotalObject($readYears, $writeYears);
            foreach ($category['postes'] as $categoryPoste) {
                $lineTotals = $categoryPoste['fake']
                    ? $this->createFakeExpenseLine($categoryPoste['data'], $readYears, $writeYears, $table)
                    : $this->createRealExpenseLine($categoryPoste['data'], $id, $readYears, $expenseType, $writeYears, $table);
                self::sumTotals($lineTotals, $subTotals, $readYears, $writeYears);
            }
            self::sumTotals($subTotals, $totals, $readYears, $writeYears);
            $this->createTotalLine($subTotals, $readYears, $writeYears, $table, $category['name'], $id);
        }
        $this->createTotalLine($totals, $readYears, $writeYears, $table);

        return $table;
    }


    private function getExportYears($exerciseYears, $startYear, $endYear)
    {
        if (0 === $startYear && 0 === $endYear) {
            return $exerciseYears;
        }
        $exportYears = [];
        foreach ($exerciseYears as $year) {
            if ($year >= $startYear && 0 === $endYear) {
                $exportYears[] = $year;
            } elseif ($year >= $startYear && $year <= $endYear) {
                $exportYears[] = $year;
            }
        }
        sort($exportYears);
        return $exportYears;
    }



    /**
     * @var PosteExpense
     */
    private function getPoste($posteId)
    {
        return $this->_entityManager->getRepository('Budget\Entity\PosteExpense')->findOneBy(array('id' => $posteId));
    }

    /**
     * @var Nature
     */
    private function getNature($natureId)
    {
        return $this->_entityManager->getRepository('Budget\Entity\Nature')->findOneBy(array('id' => $natureId));
    }

    /**
     * @var Account
     */
    private function getAccount($accountId)
    {
        return $this->_entityManager->getRepository('Budget\Entity\Account')->findOneBy(array('id' => $accountId));
    }


    private static function initTotalObject($readYears, $writeYears)
    {
        $total = array(
            'total' => 0.0,
            'client' => 0.0,
            'sponsor' => 0.0
        );
        foreach ($writeYears as $year) {
            $total[$year] = ['amount' => 0.0, 'readOnly' => true];
        }
        foreach ($readYears as $year) {
            $total[$year] = ['amount' => 0.0, 'readOnly' => true];
        }
        return $total;
    }

    private static function sumTotals($source, &$target, $readYears, $writeYears)
    {
        $target['total'] += $source['total'];
        $target['client'] += $source['client'];
        $target['sponsor'] += $source['sponsor'];
        foreach ($writeYears as $year) {
            $target[$year]['amount'] += $source[$year]['amount'];
        }
        foreach ($readYears as $year) {
            $target[$year]['amount'] += $source[$year]['amount'];
        }
    }



    private static function mergeYears($read, $write)
    {
        $mergedYears = array_merge($read, $write);
        $mergedYears = array_unique($mergedYears);
        sort($mergedYears);
        return $mergedYears;
    }
}
