<?php

namespace Project;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'project' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/project',
                    'defaults' => [
                        'controller' => 'Project\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'Project\Controller\Index',
                                'action'     => 'form',
                            ],
                        ],
                    ],
                    'duplicate' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/duplicate/:id',
                            'defaults' => [
                                'controller' => 'Project\Controller\Index',
                                'action'     => 'duplicate',
                            ],
                        ],
                    ],
                    'tree' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/tree',
                            'defaults' => [
                                'controller' => 'Project\Controller\Index',
                                'action'     => 'tree',
                            ],
                        ],
                    ],
                    'export' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/export',
                            'defaults' => [
                                'controller' => 'Project\Controller\Index',
                                'action'     => 'export',
                            ],
                        ],
                    ],
                    'age' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/age-synchronisation'
                        ],
                        'child_routes' => [
                            'read' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '[/:id]',
                                    'defaults' => [
                                        'controller' => 'Project\Controller\AgeSynchronisation',
                                        'action' => 'read'
                                    ]
                                ]
                            ],
                            'prevent' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '[/:id]/prevent',
                                    'defaults' => [
                                        'controller' => 'Project\Controller\AgeSynchronisation',
                                        'action' => 'prevent'
                                    ]
                                ]
                            ],
                            'synchronise' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '[/:id]/synchronise[/:flag]',
                                    'defaults' => [
                                        'controller' => 'Project\Controller\AgeSynchronisation',
                                        'action' => 'synchronise'
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'template' => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/template',
                            'defaults' => [
                                'controller' => 'Project\Controller\Template',
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/form[/:id]',
                                    'defaults' => [
                                        'controller' => 'Project\Controller\Template',
                                        'action'     => 'form',
                                    ],
                                ],
                            ]
                        ],
                    ],
                ],
            ],
            'api'     => [
                'child_routes' => [
                    'project' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/project[/:id]',
                            'defaults' => [
                                'controller' => 'Project\Controller\API\Project',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'update_history' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/update-history',
                                    'defaults' => [
                                        'controller' => 'Project\Controller\API\ProjectUpdate',
                                        'action'     => 'getHistory',
                                    ],
                                ],
                            ]
                        ],
                    ],
                    'age' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/age-synchronisation/:projectId',
                            'defaults' => [
                                'controller' => 'Project\Controller\API\AgeSynchronisation',
                            ],
                        ],
                    ],
                    'template' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/template[/:id]',
                            'defaults' => [
                                'controller' => 'Project\Controller\API\Template',
                            ],
                        ],
                    ],
                    'member'  => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/member[/:id]',
                            'defaults' => [
                                'controller' => 'Project\Controller\API\Member',
                            ],
                        ],
                    ],
                    'actor'  => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/actor[/:id]',
                            'defaults' => [
                                'controller' => 'Project\Controller\API\Actor',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class               => ServiceLocatorFactory::class,
            Controller\TemplateController::class            => ServiceLocatorFactory::class,
            Controller\AgeSynchronisationController::class  => ServiceLocatorFactory::class,
            Controller\API\ProjectController::class         => ServiceLocatorFactory::class,
            Controller\API\ProjectUpdateController::class   => ServiceLocatorFactory::class,
            Controller\API\MemberController::class          => ServiceLocatorFactory::class,
            Controller\API\ActorController::class           => ServiceLocatorFactory::class,
            Controller\API\TemplateController::class        => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Project\Controller\Index'                      => Controller\IndexController::class,
            'Project\Controller\Template'                   => Controller\TemplateController::class,
            'Project\Controller\API\Project'                => Controller\API\ProjectController::class,
            'Project\Controller\API\ProjectUpdate'          => Controller\API\ProjectUpdateController::class,
            'Project\Controller\AgeSynchronisation'         => Controller\AgeSynchronisationController::class,
            'Project\Controller\API\Member'                 => Controller\API\MemberController::class,
            'Project\Controller\API\Actor'                  => Controller\API\ActorController::class,
            'Project\Controller\API\Template'               => Controller\API\TemplateController::class,
        ],
    ],
];
