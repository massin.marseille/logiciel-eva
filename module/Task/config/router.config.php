<?php

namespace Task;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'task' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/task',
                    'defaults' => [
                        'controller' => 'Task\Controller\Index',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true
            ],
            'api' => [
                'child_routes'  => [
                    'task' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/task[/:id]',
                            'defaults' => [
                                'controller' => 'Task\Controller\API\Task'
                            ]
                        ]
                    ],
                ]
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class    => ServiceLocatorFactory::class,
            Controller\API\TaskController::class => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Task\Controller\Index'    => Controller\IndexController::class,
            'Task\Controller\API\Task' => Controller\API\TaskController::class,
        ]
    ]
];
