<?php

namespace Admin;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'admin-routes' => [
            'root' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'Admin\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
            ],
            'client' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/client',
                    'defaults' => [
                        'controller' => 'Admin\Controller\Client',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'users' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/:id/users',
                            'defaults' => [
                                'controller' => 'Admin\Controller\Client',
                                'action'     => 'getUsers'
                            ]
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'users' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/login/:user',
                                    'defaults' => [
                                        'controller' => 'Admin\Controller\Client',
                                        'action'     => 'loginAs'
                                    ]
                                ]
                            ],
                        ]
                    ],
                ]
            ],
            'network' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/network',
                    'defaults' => [
                        'controller' => 'Admin\Controller\Network',
                        'action'     => 'index'
                    ]
                ]
            ],
            'migration' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/migration',
                    'defaults' => [
                        'controller' => 'Admin\Controller\Migration',
                        'action'     => 'index'
                    ]
                ]
            ],
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'client' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/client[/:id]',
                            'defaults' => [
                                'controller' => 'Admin\Controller\API\Client'
                            ]
                        ]
                    ],
                    'network' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/network[/:id]',
                            'defaults' => [
                                'controller' => 'Admin\Controller\API\Network'
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class       => ServiceLocatorFactory::class,
            Controller\ClientController::class      => ServiceLocatorFactory::class,
            Controller\NetworkController::class     => ServiceLocatorFactory::class,
            Controller\MigrationController::class   => ServiceLocatorFactory::class,
            Controller\API\ClientController::class  => ServiceLocatorFactory::class,
            Controller\API\NetworkController::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Admin\Controller\Index'       => Controller\IndexController::class,
            'Admin\Controller\Client'      => Controller\ClientController::class,
            'Admin\Controller\Network'     => Controller\NetworkController::class,
            'Admin\Controller\Migration'   => Controller\MigrationController::class,
            'Admin\Controller\API\Client'  => Controller\API\ClientController::class,
            'Admin\Controller\API\Network' => Controller\API\NetworkController::class
        ],
    ],
];
