<?php

namespace Directory\Controller;

use Core\Controller\AbstractActionSLController;
use Directory\Entity\Contact;
use Doctrine\ORM\EntityManager;
use Field\Entity\Value;
use Keyword\Entity\Association;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class ContactController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('directory:e:contact:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('contact', false);;
        }

        return new ViewModel([
            'groups' => $groups,
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);
        if (!$this->acl()->basicFormAccess('directory:e:contact', $id)) {
            return $this->notFoundAction();
        }

        $contact = $this->entityManager()->getRepository('Directory\Entity\Contact')->find($id);

        if ($contact &&
            !$this->acl()->ownerControl('directory:e:contact:u', $contact) &&
            !$this->acl()->ownerControl('directory:e:contact:r', $contact)
        ) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('contactJobStructure', false);
        }

        return new ViewModel([
            'contact' => $contact,
            'groups'  => $groups,
        ]);
    }

    /**
     * @return JsonModel
     */
    public function duplicateAction()
    {
        $success = true;
        $errors  = [];
        $newId   = null;

        try {
            $id = $this->params()->fromRoute('id', false);
            /* @var EntityManager $em */
            $em = $this->entityManager();

            /* @var Contact $contact  */
            $contact = $em->getRepository('Directory\Entity\Contact')->find($id);

            $newContact = clone $contact;
            $newContact->setCreatedAt(new \DateTime());
            $newContact->setCreatedBy($this->authStorage()->getUser());

            $em->persist($newContact);
            $em->flush();

            /**
             * Get all keywords of the contact
             * @var Association[] $associations
             */
            $associations = $em->getRepository('Keyword\Entity\Association')->findBy([
                'entity'  => 'Directory\Entity\Contact',
                'primary' => $contact->getId(),
            ]);

            foreach ($associations as $association) {
                $newAssociation = clone $association;
                $newAssociation->setPrimary($newContact->getId());
                $em->persist($newAssociation);
            }

            $em->flush();

            $newId = $newContact->getId();
        } catch (\Exception $e) {
            $success  = false;
            $errors[] =  $e->getMessage();
        } finally {
            return new JsonModel([
                'success' => $success,
                'errors'  => $errors,
                'contact' => [
                    'id' => $newId,
                ],
            ]);
        }
    }

    public function unsubscribeAction()
    {
        $id = $this->params()->fromRoute('id', false);

        $contact = $this->entityManager()->getRepository('Directory\Entity\Contact')->find($id);

        if (!$contact) {
            return $this->notFoundAction();
        }

        $this->environment()->setTemplate('layout/invitation');

        if ($this->getRequest()->isPost()) {
            $contact->setSubscriptionStatus(Contact::SUBSCRIPTION_STATUS_DECLINED);
            $contact->setSubscriptionStatusUpdatedAt(new \DateTime());
            $this->entityManager()->flush();
        }

        return new ViewModel([
            'contact' => $contact
        ]);
    }
}
