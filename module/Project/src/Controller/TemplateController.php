<?php

namespace Project\Controller;

use Core\Controller\AbstractActionSLController;
use Project\Entity\Project;
use Laminas\View\Model\ViewModel;

class TemplateController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('project:e:template:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel([]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('project:e:template', $id)) {
            return $this->notFoundAction();
        }

        $template = $this->entityManager()->getRepository('Project\Entity\Template')->find($id);

        if ($template && !$this->acl()->ownerControl('project:e:template:u', $template) && !$this->acl()->ownerControl('project:e:template:r', $template)) {
            return $this->notFoundAction();
        }

        $fields = [];
        $project = new Project();
        foreach ($project->getInputFilter($this->entityManager(), $this->authStorage()->getUser())->getInputs() as $field) {
            if (!$field->isRequired() && $field->getName() !== 'template' && $field->getName() !== 'members') {
                $fields[] = $field->getName();
            }
        }
        
        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('project', false);
        }

        $custom = [];
        if ($this->moduleManager()->isActive('field')) {
            $custom = $this->allowedFieldGroups('Project\Entity\Project');
        }

        return new ViewModel([
            'fields'   => $fields,
            'custom'   => $custom,
            'keywords' => $groups,
            'template' => $template,
        ]);
    }
}
