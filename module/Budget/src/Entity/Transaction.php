<?php

namespace Budget\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Transaction
 *
 * @package Budget\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\MappedSuperclass
 */
abstract class Transaction extends BasicRestEntityAbstract
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $name;

	/**
	 * @var Poste
	 */
	protected $poste;

	/**
	 * @var ManagementUnit
	 *
	 * @ORM\ManyToOne(targetEntity="Budget\Entity\ManagementUnit")
	 */
	protected $managementUnit;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float")
	 */
	protected $amount;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", options={"default" : 0})
	 */
	protected $amountHT = 0;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date")
	 */
	protected $date;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	protected $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $tiers;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $anneeExercice;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $bordereau;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $mandat;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $engagement;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $complement;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean", options={"default" : 0})
	 */
	protected $import = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean", options={"default" : 0})
	 */
	protected $age = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean", options={"default": false})
	 */
	protected $evolution = false;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return Poste
	 */
	public function getPoste()
	{
		return $this->poste;
	}

	/**
	 * @param Poste $poste
	 */
	public function setPoste($poste)
	{
		$this->poste = $poste;
	}

	/**
	 * @return ManagementUnit
	 */
	public function getManagementUnit()
	{
		return $this->managementUnit;
	}

	/**
	 * @param ManagementUnit $managementUnit
	 */
	public function setManagementUnit($managementUnit)
	{
		$this->managementUnit = $managementUnit;
	}

	/**
	 * @return float
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param float $amount
	 */
	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	/**
	 * @return float
	 */
	public function getAmountHT()
	{
		return $this->amountHT;
	}

	/**
	 * @param float $amountHT
	 */
	public function setAmountHT($amountHT)
	{
		$this->amountHT = $amountHT;
	}

	/**
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param \DateTime $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}

	/**
	 * @return string date year
	 */
	public function getYear()
	{
		return $this->date->format('Y');
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getTiers()
	{
		return $this->tiers;
	}

	/**
	 * @param string $tiers
	 */
	public function setTiers($tiers)
	{
		$this->tiers = $tiers;
	}

	/**
	 * @return string
	 */
	public function getAnneeExercice()
	{
		return $this->anneeExercice;
	}

	/**
	 * @param string $anneeExercice
	 */
	public function setAnneeExercice($anneeExercice)
	{
		$this->anneeExercice = $anneeExercice;
	}

	/**
	 * @return string
	 */
	public function getBordereau()
	{
		return $this->bordereau;
	}

	/**
	 * @param string $bordereau
	 */
	public function setBordereau($bordereau)
	{
		$this->bordereau = $bordereau;
	}

	/**
	 * @return string
	 */
	public function getMandat()
	{
		return $this->mandat;
	}

	/**
	 * @param string $mandat
	 */
	public function setMandat($mandat)
	{
		$this->mandat = $mandat;
	}

	/**
	 * @return string
	 */
	public function getEngagement()
	{
		return $this->engagement;
	}

	/**
	 * @param string $engagement
	 */
	public function setEngagement($engagement)
	{
		$this->engagement = $engagement;
	}

	/**
	 * @return string
	 */
	public function getComplement()
	{
		return $this->complement;
	}

	/**
	 * @param string $complement
	 */
	public function setComplement($complement)
	{
		$this->complement = $complement;
	}

	/**
	 * Proxy method
	 * @return \Project\Entity\Project
	 */
	public function getProject()
	{
		return $this->getPoste()->getProject();
	}

	public function isYear($year)
	{
		if ($year !== null) {
			return $this->getDate()->format('Y') == $year;
		}

		return true;
	}

	public function belongsToPreviousYears()
	{
		$currentYear = date('Y');
		return $currentYear > $this->getDate()->format('Y');
	}

	/**
	 * @return boolean
	 */
	public function isImport()
	{
		return $this->import;
	}

	/**
	 * @return boolean
	 */
	public function getImport()
	{
		return $this->isImport();
	}

	/**
	 * @param boolean $import
	 */
	public function setImport($import)
	{
		$this->import = $import;
	}

	/**
	 * @return bool
	 */
	public function isAge()
	{
		return $this->age;
	}

	/**
	 * @return boolean
	 */
	public function getAge()
	{
		return $this->isAge();
	}

	/**
	 * @param bool $age
	 */
	public function setAge($age)
	{
		$this->age = $age;
	}

	/**
	 * @return bool
	 */
	public function isEvolution()
	{
		return $this->evolution;
	}

	/**
	 * @return boolean
	 */
	public function getEvolution()
	{
		return $this->isEvolution();
	}

	/**
	 * @param bool $evolution
	 */
	public function setEvolution($evolution)
	{
		$this->evolution = $evolution;
	}

	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilterFactory = new Factory();
		$inputFilter = $inputFilterFactory->createInputFilter([
			[
				'name' => 'name',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'poste',
				'required' => true,
			],
			[
				'name' => 'amount',
				'required' => true,
				'filters' => [['name' => 'NumberParse']],
			],
			[
				'name' => 'amountHT',
				'required' => false,
				'filters' => [['name' => 'NumberParse']],
			],
			[
				'name' => 'date',
				'required' => true,
				'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
			],
			[
				'name' => 'tiers',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'anneeExercice',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'bordereau',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'mandat',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'engagement',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'complement',
				'required' => false,
				'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			],
			[
				'name' => 'evolution',
				'required' => false,
				'allow_empty' => true,
				'filters' => [['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]]],
			],
			[
				'name'      => 'managementUnit',
				'required'  => false,
				'validators' => [
					[
						'name' => 'Callback',
						'options' => [
							'callback' => function ($value) use ($entityManager) {
								if (is_array($value)) {
									$value = $value['code'];
								}

								return $entityManager->getRepository(ManagementUnit::class)->findOneBy(['code' => $value]) !== null;
							},
							'message' => 'transaction_field_management_unit_error_non_exists'
						],
					],
				],
			],
		]);

		return $inputFilter;
	}

	/**
	 * @param User $user
	 * @param string $crudAction
	 * @param null   $owner
	 * @param null   $entityManager
	 * @return bool
	 */
	public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
	{
		if ($this->getProject()) {
			return $this->getProject()->ownerControl($user, $crudAction, $owner, $entityManager);
		}

		return false;
	}

	public function isFromFinancialYear($financialYear) {
		if(empty($financialYear)) {
			return true;
		}
		return $financialYear == $this->anneeExercice;
	}

}
