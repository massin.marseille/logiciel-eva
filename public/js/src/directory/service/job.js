(function(app) {

    app.factory('jobService', [
        'restFactoryService', '$http', '$q',
        function (restFactoryService, $http, $q) {
            var url = '/api/directory/job';

            var methods = restFactoryService.create(url);

            methods.fusion = function (jobToKeep_id, jobs_id) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: url + '/' + jobToKeep_id + '/fusion',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ jobs_id : jobs_id })
                }).then(function(res) {
                    deferred.resolve(res.data)
                }, function(err) {
                    deferred.reject(err)
                });

                return deferred.promise;
            };

            return methods;
        }
    ])

})(window.EVA.app);

