<?php

namespace Keyword\Controller\API;

use Core\Controller\BasicRestSyncController;

class GroupController extends BasicRestSyncController
{
    protected $entityClass = 'Keyword\Entity\Group';
    protected $repository  = 'Keyword\Entity\Group';
    protected $entityChain = 'keyword:e:group';

    protected function searchList($queryBuilder, &$data)
    {

        if (!isset($data['filters']['archived'])) {
            $data['filters']['archived'] = [
                'op'  => 'eq',
                'val' => false,
            ];
        }

        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        //$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
