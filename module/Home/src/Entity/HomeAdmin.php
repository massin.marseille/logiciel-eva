<?php
namespace Home\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Home
 *
 * @package Home\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="home_home_admin")
 */
class HomeAdmin extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\OneToMany(targetEntity="Container", mappedBy="homeAdmin")
     */
    protected $containers;

    /**
     * Home constructor.
     */
    public function __construct()
    {
        $this->containers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getContainers()
    {
        return $this->containers;
    }

    /**
     * @param ArrayCollection $containers
     */
    public function setContainers($containers)
    {
        $this->containers = $containers;
    }

    /**
     * @param ArrayCollection $containers
     */
    public function addContainers(ArrayCollection $containers)
    {
        foreach ($containers as $container) {
            $this->getContainers()->add($container);
        }
    }

    /**
     * @param ArrayCollection $containers
     */
    public function removeContainers(ArrayCollection $containers)
    {
        foreach ($containers as $container) {
            $this->getContainers()->removeElement($container);
        }
    }

    /**
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'        => 'enabled',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'     => 'containers',
                'required' => false,
            ],
        ]);

        return $inputFilter;
    }
}
