CREATE DATABASE  IF NOT EXISTS `admin` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `admin`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: admin
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eva_client`
--

DROP TABLE IF EXISTS `eva_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eva_client` (
  `id` int NOT NULL AUTO_INCREMENT,
  `network_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `host` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dbHost` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dbPort` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dbUser` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dbPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `dbName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `modules` json NOT NULL,
  `translations` json DEFAULT NULL,
  `icons` json DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E8A781904E645A7E` (`key`),
  UNIQUE KEY `UNIQ_E8A78190CF2713FD` (`host`),
  KEY `IDX_E8A7819034128B91` (`network_id`),
  KEY `IDX_E8A781903174800F` (`createdBy_id`),
  KEY `IDX_E8A7819065FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_E8A781903174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_E8A7819034128B91` FOREIGN KEY (`network_id`) REFERENCES `eva_network` (`id`),
  CONSTRAINT `FK_E8A7819065FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eva_client`
--

LOCK TABLES `eva_client` WRITE;
/*!40000 ALTER TABLE `eva_client` DISABLE KEYS */;
INSERT INTO `eva_client` (`id`, `network_id`, `name`, `key`, `host`, `dbHost`, `dbPort`, `dbUser`, `dbPassword`, `dbName`, `theme`, `modules`, `translations`, `icons`, `email`, `createdAt`, `updatedAt`, `createdBy_id`, `updatedBy_id`) VALUES (182,1,'formation','formation','client1.eva.local','eva-database','3306','root','toor','formation','parc','{\"age\": {\"active\": true, \"activable\": true, \"configuration\": {\"allowedIncomeTypes\": [\"requested\", \"notified\", \"assigned\", \"perceived\", \"dv\"], \"allowedExpenseTypes\": [\"planned\", \"committed\", \"paid\", \"released\", \"committedPaid\", \"aeEstimated\", \"ae\", \"aeCommitted\", \"cpEstimated\", \"cp\", \"cpCommitted\", \"dv\", \"portfolio\"]}}, \"map\": {\"active\": true, \"activable\": true}, \"home\": {\"active\": true, \"activable\": true}, \"memo\": {\"active\": false, \"activable\": false}, \"task\": {\"active\": true, \"activable\": true}, \"time\": {\"active\": true, \"activable\": true}, \"alert\": {\"active\": true, \"activable\": true}, \"field\": {\"active\": false, \"activable\": true}, \"budget\": {\"active\": true, \"activable\": true, \"configuration\": {\"ht\": true, \"code\": true, \"gbcp\": true, \"evolution\": false, \"incomeTypes\": [\"requested\", \"notified\", \"assigned\", \"perceived\"], \"expenseTypes\": [\"aeEstimated\", \"ae\", \"aeCommitted\", \"cpEstimated\", \"cp\", \"cpCommitted\"]}}, \"campain\": {\"active\": true, \"activable\": true}, \"keyword\": {\"active\": true, \"activable\": true}, \"profile\": {\"active\": false, \"activable\": false}, \"project\": {\"active\": true, \"activable\": true}, \"postParc\": {\"active\": true, \"activable\": true, \"configuration\": {\"user\": \"jmassin\", \"apiUrl\": \"https://pnforets.postparc.fr/api\", \"password\": \"EVA2022vert\"}}, \"indicator\": {\"active\": true, \"activable\": true, \"configuration\": {\"analysis_bar\": true, \"analysis_pie\": true, \"analysis_line\": true, \"analysis_bubble\": true, \"analysis_measures_bar\": true}}, \"convention\": {\"active\": true, \"activable\": true}, \"advancement\": {\"active\": true, \"activable\": true}, \"application\": {\"active\": true, \"activable\": false, \"configuration\": {\"mailingHost\": \"eva-mail\", \"mailingPort\": \"25\", \"mailingAuthMode\": \"tls\", \"mailingAuthType\": \"login\", \"mailingAuthUser\": \"eva-mail\", \"mailingDomainName\": \"eva-mail\", \"mailingAuthPassword\": \"eva-mail\"}}}','{\"keyword\": \"Mot-clé\", \"manager\": \"Chef.fe de projet\", \"keywords\": \"Mots-clés\", \"site_title\": \"SANDBOX-EVA-RECETTE\", \"job_nav_form\": \"Ajouter un emploi\", \"job_nav_list\": \"Liste des emplois\", \"role_field_id\": \"ID\", \"role_nav_form\": \"Créer un rôle\", \"role_nav_list\": \"Liste des rôles\", \"user_nav_form\": \"Créer un utilisateur\", \"user_nav_list\": \"Liste des utilisateurs\", \"user_field_role\": \"Rôle\", \"user_nav_import\": \"Importer des utilisateurs\", \"account_field_id\": \"ID\", \"contact_nav_form\": \"Créer un contact\", \"contact_nav_list\": \"Liste des contacts\", \"homeAdmin_entity\": \"Accueil général\", \"keyword_nav_list\": \"Liste des mots-clés\", \"project_nav_form\": \"Créer une fiche\", \"contact_field_job\": \"Emploi\", \"envelope_field_id\": \"ID\", \"project_hierarchy\": \"Arborescence des fiches\", \"role_module_title\": \"Rôles\", \"template_field_id\": \"ID\", \"user_module_title\": \"Utilisateurs\", \"admin_module_title\": \"Paramétrages\", \"contact_field_name\": \"Nom-Prénom\", \"contact_nav_import\": \"Importer des contacts\", \"field_module_title\": \"Bilan et évaluation\", \"project_nav_import\": \"Importer des fiches\", \"structure_nav_form\": \"Créer une structure\", \"structure_nav_list\": \"Liste des structures\", \"job_field_structure\": \"Stucture associée\", \"project_new_project\": \"Nouvelle fiche\", \"contact_module_title\": \"Contacts\", \"keyword_module_title\": \"Mots-clés\", \"project_module_title\": \"Fiches\", \"role_field_createdAt\": \"Créé le\", \"role_field_updatedAt\": \"Modifié le\", \"structure_nav_import\": \"Importer des structures\", \"user_field_createdAt\": \"Créé le\", \"user_field_updatedAt\": \"Modifié le\", \"budgetStatus_field_id\": \"ID\", \"project_field_managers\": \"Chef.fe(s) de projet\", \"structure_module_title\": \"Structures\", \"contact_field_createdAt\": \"Créé le\", \"contact_field_updatedAt\": \"Modifié le\", \"project_field_createdAt\": \"Créé le\", \"project_field_updatedAt\": \"Modifié le\", \"subvention_module_title\": \"[[ subventions_entities | ucfirst]]\", \"convention_line_nav_form\": \"Ajouter une ligne\", \"structure_field_createdAt\": \"Créée le\", \"structure_field_updatedAt\": \"Modifiée le\", \"configuration_module_title\": \"Configuration\", \"administration_module_title\": \"Paramétrages\", \"role_field_name_error_unique\": \"Ce rôle existe déjà\", \"user_field_role_error_non_exists\": \"Ce rôle n\'existe pas ou plus\", \"query_field_owner_error_non_exists\": \"Cet utilisateur n\'existe pas ou plus\", \"keyword_field_group_error_non_exists\": \"Ce groupe n\'existe pas ou plus\", \"project_field_parent_error_non_exists\": \"Cette fiche n\'existe pas ou plus\", \"timesheet_import_caution_not_all_has_project\": \"Certaines [[ timesheet_entities | lcfirst ]] n\'ont pas de fiche. Elles ne seront pas enregistrées. Souhaitez-vous continuer ?\"}','[]',NULL,'2023-11-08 17:15:46','2023-12-27 11:35:42',5,5);
/*!40000 ALTER TABLE `eva_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eva_network`
--

DROP TABLE IF EXISTS `eva_network`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eva_network` (
  `id` int NOT NULL AUTO_INCREMENT,
  `master_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8BA5520613B3DB11` (`master_id`),
  KEY `IDX_8BA552063174800F` (`createdBy_id`),
  KEY `IDX_8BA5520665FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_8BA5520613B3DB11` FOREIGN KEY (`master_id`) REFERENCES `eva_client` (`id`),
  CONSTRAINT `FK_8BA552063174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_8BA5520665FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eva_network`
--

LOCK TABLES `eva_network` WRITE;
/*!40000 ALTER TABLE `eva_network` DISABLE KEYS */;
INSERT INTO `eva_network` (`id`, `master_id`, `name`, `createdAt`, `updatedAt`, `createdBy_id`, `updatedBy_id`) VALUES (1,182,'Réseau DEV','2017-03-21 08:34:11','2017-05-10 07:55:32',1,1);
/*!40000 ALTER TABLE `eva_network` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keyword_association`
--

DROP TABLE IF EXISTS `keyword_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyword_association` (
  `entity` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `keyword_id` int NOT NULL,
  `primary` int NOT NULL,
  PRIMARY KEY (`keyword_id`,`entity`,`primary`),
  KEY `IDX_34CF7409115D4552` (`keyword_id`),
  CONSTRAINT `FK_34CF7409115D4552` FOREIGN KEY (`keyword_id`) REFERENCES `keyword_keyword` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keyword_association`
--

LOCK TABLES `keyword_association` WRITE;
/*!40000 ALTER TABLE `keyword_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `keyword_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keyword_group`
--

DROP TABLE IF EXISTS `keyword_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyword_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `entities` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '(DC2Type:simple_array)',
  `multiple` tinyint(1) NOT NULL,
  `master` int DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_832973643174800F` (`createdBy_id`),
  KEY `IDX_8329736465FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_832973643174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_8329736465FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keyword_group`
--

LOCK TABLES `keyword_group` WRITE;
/*!40000 ALTER TABLE `keyword_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `keyword_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keyword_keyword`
--

DROP TABLE IF EXISTS `keyword_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyword_keyword` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `order` int NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `master` int DEFAULT NULL,
  `externalId` int DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5532B775FE54D947` (`group_id`),
  KEY `IDX_5532B7753174800F` (`createdBy_id`),
  KEY `IDX_5532B77565FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_5532B7753174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_5532B77565FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_5532B775FE54D947` FOREIGN KEY (`group_id`) REFERENCES `keyword_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keyword_keyword`
--

LOCK TABLES `keyword_keyword` WRITE;
/*!40000 ALTER TABLE `keyword_keyword` DISABLE KEYS */;
/*!40000 ALTER TABLE `keyword_keyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keyword_keyword_children`
--

DROP TABLE IF EXISTS `keyword_keyword_children`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyword_keyword_children` (
  `keyword_id` int NOT NULL,
  `child_id` int NOT NULL,
  PRIMARY KEY (`keyword_id`,`child_id`),
  KEY `IDX_8AEF80C1115D4552` (`keyword_id`),
  KEY `IDX_8AEF80C1DD62C21B` (`child_id`),
  CONSTRAINT `FK_8AEF80C1115D4552` FOREIGN KEY (`keyword_id`) REFERENCES `keyword_keyword` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8AEF80C1DD62C21B` FOREIGN KEY (`child_id`) REFERENCES `keyword_keyword` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keyword_keyword_children`
--

LOCK TABLES `keyword_keyword_children` WRITE;
/*!40000 ALTER TABLE `keyword_keyword_children` DISABLE KEYS */;
/*!40000 ALTER TABLE `keyword_keyword_children` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_profile`
--

DROP TABLE IF EXISTS `profile_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile_profile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price_per_hour` double NOT NULL,
  `price_per_day` double NOT NULL,
  `hour_per_day` double NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_52E974933174800F` (`createdBy_id`),
  KEY `IDX_52E9749365FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_52E974933174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_52E9749365FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_profile`
--

LOCK TABLES `profile_profile` WRITE;
/*!40000 ALTER TABLE `profile_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_configuration`
--

DROP TABLE IF EXISTS `user_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_configuration` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `tables` json NOT NULL,
  `resizableTable` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4B6C0887A76ED395` (`user_id`),
  KEY `IDX_4B6C08873174800F` (`createdBy_id`),
  KEY `IDX_4B6C088765FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_4B6C08873174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_4B6C088765FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_4B6C0887A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_configuration`
--

LOCK TABLES `user_configuration` WRITE;
/*!40000 ALTER TABLE `user_configuration` DISABLE KEYS */;
INSERT INTO `user_configuration` (`id`, `user_id`, `tables`, `resizableTable`, `createdAt`, `updatedAt`, `createdBy_id`, `updatedBy_id`) VALUES (1,1,'[]',1,'2022-04-19 18:15:08',NULL,1,NULL),(2,6,'[]',1,'2024-01-02 13:00:15',NULL,6,NULL);
/*!40000 ALTER TABLE `user_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rights` json NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2DE8C6A35E237E06` (`name`),
  KEY `IDX_2DE8C6A33174800F` (`createdBy_id`),
  KEY `IDX_2DE8C6A365FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_2DE8C6A33174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_2DE8C6A365FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `name`, `rights`, `createdAt`, `updatedAt`, `createdBy_id`, `updatedBy_id`) VALUES (1,'Administrateur','{\"user:e:role:c\": {\"value\": true}, \"user:e:role:d\": {\"owner\": \"all\", \"value\": true}, \"user:e:role:r\": {\"owner\": \"all\", \"value\": true}, \"user:e:role:u\": {\"owner\": \"all\", \"value\": true}, \"user:e:user:c\": {\"value\": true}, \"user:e:user:d\": {\"owner\": \"all\", \"value\": true}, \"user:e:user:r\": {\"owner\": \"all\", \"value\": true}, \"user:e:user:u\": {\"owner\": \"all\", \"value\": true}, \"admin:e:client:c\": {\"value\": true}, \"admin:e:client:d\": {\"owner\": \"all\", \"value\": true}, \"admin:e:client:r\": {\"owner\": \"all\", \"value\": true}, \"admin:e:client:u\": {\"owner\": \"all\", \"value\": true}, \"admin:e:network:c\": {\"value\": true}, \"admin:e:network:d\": {\"value\": true}, \"admin:e:network:r\": {\"value\": true}, \"admin:e:network:u\": {\"value\": true}, \"project:e:project:c\": {\"value\": false}, \"project:e:project:d\": {\"value\": false}, \"project:e:project:r\": {\"value\": false}, \"project:e:project:u\": {\"value\": false}}',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_user`
--

DROP TABLE IF EXISTS `user_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int DEFAULT NULL,
  `profile_id` int DEFAULT NULL,
  `login` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `worked_hours` double DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdBy_id` int DEFAULT NULL,
  `updatedBy_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F7129A80AA08CB10` (`login`),
  KEY `IDX_F7129A80D60322AC` (`role_id`),
  KEY `IDX_F7129A80CCFA12B8` (`profile_id`),
  KEY `IDX_F7129A803174800F` (`createdBy_id`),
  KEY `IDX_F7129A8065FF1AEC` (`updatedBy_id`),
  CONSTRAINT `FK_F7129A803174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_F7129A8065FF1AEC` FOREIGN KEY (`updatedBy_id`) REFERENCES `user_user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_F7129A80CCFA12B8` FOREIGN KEY (`profile_id`) REFERENCES `profile_profile` (`id`),
  CONSTRAINT `FK_F7129A80D60322AC` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_user`
--

LOCK TABLES `user_user` WRITE;
/*!40000 ALTER TABLE `user_user` DISABLE KEYS */;
INSERT INTO `user_user` (`id`, `role_id`, `profile_id`, `login`, `password`, `avatar`, `email`, `color`, `disabled`, `worked_hours`, `gender`, `firstName`, `lastName`, `name`, `createdAt`, `updatedAt`, `createdBy_id`, `updatedBy_id`) VALUES (1,1,NULL,'jmassin@siter.fr','e145d255068de70078c9ece1532d6173df744f76','/img/default-avatar.png','jmassin@siter.fr','',0,0,'m','jeremie','massin','jeremie massin',NULL,'2022-04-19 18:15:33',NULL,1),(3,1,0,'info@siter.fr','57f39ec93897f7065addc74ffea48397c65830f4','info@siter.fr','#009688','m',0,0,'m','2020-10-21 08:46:09','','info@siter.fr',NULL,'0000-00-00 00:00:00',NULL,0),(5,1,0,'SITERDEV','e145d255068de70078c9ece1532d6173df744f76','info@siter.fr','','m',0,0,'m','','2022-03-10 10:07:24','info@siter.fr','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,0),(6,1,0,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','','','',0,0,'','','','',NULL,'0000-00-00 00:00:00',NULL,NULL),(100,1,0,'348cb9defeedfcd99a3a80c56f22404749030aa9','348cb9defeedfcd99a3a80c56f22404749030aa9','jmassin@siter.fr','#89e254','m',0,0,'m','','2019-11-04 18:19:54','jmassin@siter.fr','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `user_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-02 13:46:24
