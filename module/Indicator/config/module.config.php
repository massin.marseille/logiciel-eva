<?php

return [
    'module' => [
        'indicator' => [
            'environments' => [
                'client'
            ],

            'active'    => true,
            'required'  => false,

            'acl' => [
                'entities' => [
                    [
                        'name'     => 'indicator',
                        'class'    => 'Indicator\Entity\Indicator',
                        'keywords' => true,
                        'owner'    => ['service']
                    ],
                    [
                        'name'     => 'measure',
                        'class'    => 'Indicator\Entity\Measure',
                        'owner'    => []
                    ],
                    [
                        'name'     => 'group-indicator',
                        'class'    => 'Indicator\Entity\GroupIndicator',
                        'keywords' => false,
                        'owner'    => []
                    ]
                ]
            ],

            'configuration' => [
                'transverse' => [
                    'type'    => 'boolean',
                    'text'    => 'indicator_transverse_configuration'
                ],
                'analysis_bubble' => [
                    'type'    => 'boolean',
                    'text'    => 'indicator_analysis_bubble_configuration'
                ],
                'analysis_line' => [
                    'type'    => 'boolean',
                    'text'    => 'indicator_analysis_line_configuration'
                ],
                'analysis_bar' => [
                    'type'    => 'boolean',
                    'text'    => 'indicator_analysis_bar_configuration'
                ],
                'analysis_pie' => [
                    'type'    => 'boolean',
                    'text'    => 'indicator_analysis_pie_configuration'
                ],
                'analysis_measures_bar' => [
                    'type'    => 'boolean',
                    'text'    => 'indicator_analysis_measures_bar_configuration'
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
