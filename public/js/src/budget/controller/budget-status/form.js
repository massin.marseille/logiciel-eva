(function(app) {

    app.controller('budgetStatusFormController', [
        'budgetStatusService', '$scope', 'flashMessenger', 'translator',
        function budgetStatusFormController(budgetStatusService, $scope, flashMessenger, translator) {
            var self = this;

            var $modal = angular.element('#statusFormModal');
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'lock',
                    'createdAt',
                    'updatedAt'
                ]
            };

            self.status = {};

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            $scope.$on('create-status', function (event) {
                self.status = {
                    _rights: {
                        update: true
                    }
                };

                self.panelTitle = translator('budgetStatus_nav_form');
                $modal.modal('show');
            });

            $scope.$on('edit-status', function (event, status) {
                self.editedStatus = status;
                self.status       = angular.copy(status);
                self.panelTitle   = self.status.name;

                $modal.modal('show');
            });

            self.saveStatus = function saveStatus() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.status.id;

                budgetStatusService.save(self.status, apiParams).then(function(res) {
                    if (isCreation) {
                        $scope.$parent.self.statutes.push(res.object);
                    } else {
                        for(var i in $scope.$parent.self.statutes){
                            if($scope.$parent.self.statutes[i].id == res.object.id){
                                $scope.$parent.self.statutes[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('budgetStatus_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };
        }
    ])

})(window.EVA.app);
