(function(app) {
    app.controller('measureCampainListController', [
        'campain-measureService', '$scope', 'flashMessenger', 'translator', '$filter', '$timeout', '$rootScope',
        function measureListController(measureService, $scope, flashMessenger, translator, $filter, $timeout, $rootScope) {
            var self = this;

            self.from = {
                type: null,
                id: null
            };
            self.object = null;

            self.measures = [];
            self.measure = {
            };
            self.options = {
                name: 'campain-measures-list',
                col: [
                    'id',
                    'supposedDate',
                    'indicator.id',
                    'indicator.users.id',
                    'indicator.users.name',
                    'realiseMeasure.id',
                    'realiseMeasure.fixedValue',
                    'realiseMeasure.value',
                    'realiseMeasure.type',
                    'realiseMeasure.comment',
                    'realiseMeasure.source',
                    'realiseMeasure.groupIndicator.id',
                    'realiseMeasure.groupIndicator.name',
                    'realiseMeasure.indicator.id',
                    'realiseMeasure.indicator.name',
                    'realiseMeasure.indicator.groupIndicators.id',
                    'realiseMeasure.indicator.groupIndicators.name',
                    'realiseMeasure.indicator.type',
                    'realiseMeasure.indicator.values',
                    'realiseMeasure.project.id',
                    'realiseMeasure.project.name',
                    'realiseMeasure.territories',
                    'realiseMeasure.start',
                    'realiseMeasure.date',
                    'realiseMeasure.updatedAt',
                    'targetMeasure.id',
                    'targetMeasure.value',
                    'targetMeasure.type',
                    'targetMeasure.comment',
                    'targetMeasure.source',
                    'targetMeasure.fixedValue',
                    'targetMeasure.groupIndicator.id',
                    'targetMeasure.groupIndicator.name',
                    'targetMeasure.indicator.id',
                    'targetMeasure.indicator.name',
                    'targetMeasure.indicator.groupIndicators.id',
                    'targetMeasure.indicator.groupIndicators.name',
                    'targetMeasure.indicator.type',
                    'targetMeasure.indicator.values',
                    'targetMeasure.project.id',
                    'targetMeasure.project.name',
                    'targetMeasure.territories',
                    'targetMeasure.start',
                    'targetMeasure.date',
                    'targetMeasure.updatedAt'
                ],
                searchType: 'list',
                sort: 'realiseMeasure.date',
                order: 'desc',
                pager: false,
                getItems: function getData(params, callback, abort) {
                    if ($scope.$parent.self[self.from.type].id || self.from.id) {
                        
                        params.search.data.filters['campain.id'] = {
                            op: 'eq',
                            val: $scope.$parent.self[self.from.type].id || self.from.id
                        };
                        measureService.findAll(params, abort.promise).then(function(res) {
                            callback(res);
                        });
                    } else {
                        callback({ rows: [] });
                    }
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id = id;
            };

            self.editMeasure = function editMeasure(measure) {
                if (self.from.type == 'campain') {
                    measure.campain = $scope.$parent.self[self.from.type];
                }
                $scope.$broadcast('edit-measure-campain', measure);
                $scope.$broadcast('edit-attachement', measure.id);
            };
            $scope.$on('update-measure-campain', function (event) {
                $scope.__tb.loadData();
            });
            self.createMeasure = function createMeasure() {
                $scope.$broadcast('create-measure-campain');
            };

            self.deleteMeasure = function deleteMeasure(measure) {
                if (confirm(translator('measure_question_delete'))) {
                    measureService.remove(measure).then(function(res) {
                        if (res.success) {
                            self.measures.splice(self.measures.indexOf(measure), 1);
                            flashMessenger.success(translator('measure_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(measure) {
                if (self.from.type == 'indicator') {
                    measure.indicator = $scope.$parent.self[self.from.type];
                }
                self.editedLineErrors = {};
                self.editedLine = measure;
                self.editedLineFields = angular.copy(measure);
            };

            self.saveEditedLine = function saveEditedLine(measure) {
                self.editedLineErrors = {};
                measureService.save(angular.extend({}, measure, self.editedLineFields), {
                    col: self.options.col
                }).then(function(res) {
                    if (res.success) {
                        measure = angular.extend(measure, res.object);
                        self.editedLine = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('measure_message_saved'));
                    }
                }, function(err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.getRatioLabel = function(measure) {
                var done = measure.realiseMeasure.value;
                var target = measure.targetMeasure.value;

                return ((target !== 0) && (target !== null) && (target !== undefined)&& (done !== null) && (done !== undefined) ? Math.abs(done / target) * 100 :  null);
            };

            self.getRatio = function(indicator, measures) {
                var done = self.getValue(indicator, measures, 'done');
                var target = self.getValue(indicator, measures, 'target');

                return (target != 0 ? Math.abs(done / target) * 100 : 0);
            };

            self.getRatioPercentage = function (measure){
                var done = measure.realiseMeasure.value;
                var target = measure.targetMeasure.value;

                return ((target !== 0) && (target !== null) && (target !== undefined)&& (done !== null) && (done !== undefined)  ? Math.abs(done / target) * 100 :  null);
            }

            self.getValue = function getValue(indicator, measures, type) {
                var value = null;
                var values = [];

                for (var i in measures) {
                    if (measures[i].type == type) {
                        values.push(measures[i].value);
                    }
                }

                switch (indicator.operator) {
                    case 'sum':
                        var sum = 0;
                        for (var _i = 0; _i < values.length; _i++) {
                            sum += values[_i];
                        }

                        value = sum;
                        break;
                    case 'avg':
                        var sum = 0;
                        for (var _i = 0; _i < values.length; _i++) {
                            sum += values[_i];
                        }

                        value = sum / values.length;
                        break;
                    case 'med':
                        var count = values.length;
                        var middle = Math.floor(count / 2);
                        values.sort(function(a, b) {
                            return a - b;
                        });
                        var median = values[middle];
                        if (count % 2 == 0) {
                            median = (median + values[middle - 1]) / 2;
                        }
                        value = median;
                        break;
                    case 'max':
                        var max = null;
                        for (var _i = 0; _i < values.length; _i++) {
                            if (max == null || values[_i] > max) {
                                max = values[_i];
                            }
                        }

                        value = max;
                        break;
                    case 'min':
                        var min = null;
                        for (var _i = 0; _i < values.length; _i++) {
                            if (min == null || values[_i] < min) {
                                min = values[_i];
                            }
                        }

                        value = min;
                        break;
                }

                return value;
            };

            self.isManager = function isManager(user) {
                for (var i in $scope.$parent.self.project.members) {
                    var member = $scope.$parent.self.project.members[i];
                    if (member.id && member.user.id == user && member.role == 'manager') {
                        return true;
                    }
                }

                return false;
            };


            self.charts = [];
            self.loadCharts = function() {

                self.charts = [];

                var grouped = $filter('groupBy')(self.measures, 'realiseMeasure.indicator.name');
                for (var indicator in grouped) {
                    var chart = {
                        options: {
                            chart: {
                                type: 'lineChart',
                                height: 400,
                                useInteractiveGuideline: true,
                                x: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.label;
                                    }
                                    return null;
                                },
                                y: function(d) {
                                    if (typeof d !== 'undefined') {
                                        return d.value;
                                    }
                                    return null;
                                },
                                xAxis: {
                                    tickFormat: function(d) {
                                        return moment(d, 'X').format('DD/MM/YYYY')
                                    }
                                }
                            },
                            title: {
                                enable: true,
                                text: indicator
                            }
                        },
                        api: null,
                        data: [{
                            values: [],
                            key: 'Prévu',
                            color: '#c3c3c3',
                        },
                            {
                                values: [],
                                key: 'Réalisé',
                                color: '#97bbcd'
                            }
                        ]
                    };

                    var measures = $filter('groupBy')(grouped[indicator], function(measure) {
                        return moment(measure.realiseMeasure.date, 'DD/MM/YYYY HH:mm').format('X')
                    });

                    var lastTarget = null;
                    var lastDone = null;
                    for (var time in measures) {
                        var target = measures[time][0].targetMeasure;
                        var done = measures[time][0].realiseMeasure;

                        if (target) {
                            lastTarget = target;
                        }

                        if (done) {
                            lastDone = done;
                        }


                        chart.data[0].values.push({
                            value: lastTarget ? lastTarget.value : 0,
                            label: parseInt(time)
                        });


                        chart.data[1].values.push({
                            value: lastDone ? lastDone.value : 0,
                            label: parseInt(time)
                        });
                    }

                    self.charts.push(chart);
                }
            };

            $scope.$watch(function() { return self.measures }, function() {
                self.loadCharts();
            }, true);

            $scope.$watch(function() { return $scope.showChart }, function() {
                self.loadCharts();
                $timeout(function() {
                    for (var i in self.charts) {
                        self.charts[i].api.refresh();
                    }
                });
            });
        }
    ])

})(window.EVA.app);