<?php

namespace Attachment\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Attachment
 *
 * @package Attachment\Attachment
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="attachment_attachment")
 * @ORM\HasLifecycleCallbacks
 */
class Attachment extends BasicRestEntityAbstract
{
    const TYPE_URL  = 'url';
    const TYPE_FILE = 'file';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $entity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="`primary`")
     */
    protected $primary;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $user;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $size;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="`key`", nullable=true)
     */
    protected $key;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @param int $primary
     */
    public function setPrimary($primary)
    {
        $this->primary = $primary;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return float
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param float $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'entity',
                'required' => true,
            ],
            [
                'name'     => 'primary',
                'required' => true,
            ],
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'type',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getTypes()
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'url',
                'required' => true
            ],
            [
                'name'     => 'user',
                'required' => true,
            ],
            [
                'name'     => 'size',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse']
                ]
            ],
            [
                'name'     => 'key',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ]
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_FILE,
            self::TYPE_URL,
        ];
    }

    /**
     * @ORM\PostRemove
     * @param LifecycleEventArgs $args
     */
    public function deleteFile(LifecycleEventArgs $args)
    {
        $attachment = $args->getEntity();

        if (file_exists(getcwd() . '/public' . $attachment->getUrl())) {
            unlink(getcwd() . '/public' . $attachment->getUrl());
        }
    }
}
