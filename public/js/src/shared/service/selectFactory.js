(function(app) {

    $.extend(Selectize.prototype, {
            getSearchOptions: function () {
                var settings = this.settings;
                var sort = settings.sortField;
                if (typeof sort === 'string') {
                    sort = [{field: sort}];
                }

                return {
                    fields: settings.searchField,
                    conjunction: settings.searchConjunction,
                    sort: sort,
                    nesting: settings.nesting
                };
            }
        }
    );

    app.factory('selectFactoryService', [
        '$compile', '$filter', '$timeout', 'treeBuilderService', '$q', 'translator',
        function selectFactoryService($compile, $filter, $timeout, treeBuilderService, $q, translator) {

            function createConfiguration(configuration, scopeConfiguration) {
                return angular.merge({}, {
                    service: {
                        tree: false,
                        nnTree: false,
                        full: false,
                        treeOrder: null
                    },
                    selectize: {
                        maxItems: 1,
                        create: false,
                        dropdownParent: 'body',
                        nesting: true
                    }
                }, configuration, scopeConfiguration);
            }

            function createTemplate() {
                return '<span class="selectize-control-container"><input type="text" ng-required="!form.selectValidity[name]" ng-model="selectValue" ng-if="form && required" class="select-factory-hidden-required"/><selectize ng-model="selectValue" ng-disabled="ngDisabled" options="selectOptions" config="selectConfiguration" ng-required="required"></selectize><i ng-show="!loading && !ngDisabled" class="fa fa-plus-square-o pointer" ng-click="showSelectionBox(selectConfiguration)"></i><i ng-show="loading" class="fa fa-spin fa-circle-o-notch"></i></span>';
            }

            return {
                create: function create(_configuration) {
                    return {
                        restrict: 'E',
                        scope: {
                            ngModel:       '=ngModel',
                            ngDisabled:    '=ngDisabled',
                            required:      '=required',
                            form:          '=form',
                            filters:       '=filters',
                            configuration: '=configuration',
                            onlyValue:     '=onlyValue',
                            multiple:      '=multiple',
                            optgroups:     '=optgroups',
                            currentUser:   '=currentUser',
                            sort:          '=sort'
                        },
                        replace: false,
                        terminal: true,
                        priority: 1000,
                        link: function link(scope, element, attrs) {
                            var configuration = createConfiguration(_configuration, angular.copy(scope.configuration));
                            if (scope.multiple) {
                                configuration.selectize.maxItems = null;
                            }

                            if (scope.form && attrs.name) {
                                scope.name = attrs.name;
                                var validity = {};
                                validity[attrs.name] = false;
                                scope.form.selectValidity = angular.extend({}, scope.form.selectValidity, validity);
                            }

                            scope.$watch('required', function (newVal, oldVal) {
                                scope.setValidity();
                            });

                            var deferredAbort = $q.defer();
                            configuration.selectize.load = function (query, callback, force) {
                                if (!force && scope.selectConfiguration.deferredSearch && (query == null || query.length < scope.selectConfiguration.deferredSearchMinLength)) {
                                    scope.loading = false;
                                    return;
                                }

                                scope.loading = true;
                                if (query !== null && !query.length) {
                                    return callback();
                                }

                                var filters = scope.filters ? JSON.parse(JSON.stringify(scope.filters)) : {};
                                if (query !== null && !configuration.service.full) {
                                    filters[configuration.selectize.labelField] = {
                                        op: 'cnt',
                                        val: query
                                    };
                                }
                                
                                var scopeApiParams = {};
                                if (typeof scope.configuration !== 'undefined') {
                                    if (typeof scope.configuration.service !== 'undefined') {
                                        if (typeof scope.configuration.service.apiParams !== 'undefined') {
                                            scopeApiParams = scope.configuration.service.apiParams;
                                        }
                                    }
                                }

                                var apiParams = angular.merge({
                                    col: [configuration.selectize.valueField, configuration.selectize.labelField],
                                    search: {
                                        type: 'list',
                                        data: {
                                            sort: configuration.selectize.labelField,
                                            order: 'asc',
                                            filters: filters
                                        }
                                    }
                                }, configuration.service.apiParams, scopeApiParams);                                

                                ///

                                if (query !== null && configuration.service.full) {
                                    apiParams.search.data.full = query;
                                }

                                deferredAbort.resolve();
                                deferredAbort = $q.defer();
                                configuration.service.object.findAll(apiParams, deferredAbort.promise).then(function(res) {
                                    for (var i in res.rows) {
                                        delete res.rows[i]['_rights'];
                                    }

                                    callback(res.rows);
                                    //scope.selectOptions = angular.extends(scope.selectOptions, res.rows);
                                    scope.loading = false;
                                }, function () {
                                    callback();
                                    scope.loading = false;
                                });
                            };

                            configuration.selectize.onChange = function (value) {
                                var realValue = null;
                                if (scope.onlyValue) {
                                    realValue = value;
                                } else {
                                    if (!angular.isArray(value)) {
                                        var filter = {};
                                        filter[configuration.selectize.valueField] = parseInt(value);
                                        var values = $filter('filter')(scope.selectOptions, filter, true);
                                        realValue  = angular.copy(values[0]);
                                        if (typeof values[0] == 'undefined') {
                                            realValue = null;
                                        }
                                    } else {
                                        realValue = [];
                                        for (var i = 0; i < value.length; i++) {
                                            var filter = {};
                                            filter[configuration.selectize.valueField] = parseInt(value[i]);
                                            var values = $filter('filter')(scope.selectOptions, filter, true);
                                            realValue.push(angular.copy(values[0]));
                                        }
                                    }
                                }

                                scope.$apply(function () {
                                    scope.ngModel = realValue;
                                });
                            };

                            scope.selectConfiguration = configuration.selectize;
                            scope.selectOptions       = [];
                            scope.selectValue         = [];

                            var init = false;
                            scope.$watch('ngModel', function () {
                                if (scope.ngModel) {
                                    var model = !angular.isArray(scope.ngModel) ? [scope.ngModel] : scope.ngModel;
                                    var selectValue   = [];
                                    var promises = [];
                                    for (var i = 0 ; i < model.length; i++) {
                                        var value = model[i];
                                        if ((angular.isArray(scope.selectValue) && scope.selectValue.indexOf(value) == -1) || (!angular.isArray(scope.selectValue) && scope.selectValue !== value)) {
                                            if (!angular.isObject(value)) {
                                                promises.push(loadValue(value).then(function (object) {
                                                    if(value==='current-user') {
                                                        scope.selectOptions.push({
                                                            id: value,
                                                            name: 'Utilisateur courant',
                                                            avatar: '/img/default-avatar.png'
                                                        });
                                                        selectValue.push(value);
                                                    } else {
                                                        scope.selectOptions.push(object);
                                                        selectValue.push(object[configuration.selectize.valueField]);
                                                    }
                                                }));
                                            } else if(typeof value[configuration.selectize.valueField] !== 'undefined') {
                                                scope.selectOptions.push(angular.copy(value));
                                                selectValue.push(value[configuration.selectize.valueField]);
                                            }
                                        }
                                        if (!scope.multiple) {
                                            break;
                                        }
                                    }
                                    $q.all(promises).then(function () {
                                        if (selectValue.length > 0) {
                                            scope.selectValue = selectValue;
                                        }
                                    });

                                    if (angular.isArray(scope.ngModel) && scope.ngModel.length == 0) {
                                        scope.selectValue = [];
                                    }
                                } else {
                                    scope.selectValue = [];
                                }

                                scope.setValidity();
                            }, 1);

                            scope.setValidity = function () {
                                if (scope.form) {
                                    if (scope.required) {
                                        var required = true;
                                        if (scope.multiple) {
                                            required = typeof scope.ngModel !== 'undefined' && scope.ngModel.length > 0;
                                        } else {
                                            required = typeof scope.ngModel !== 'undefined' && scope.ngModel !== null;
                                        }
                                        scope.form.selectValidity[attrs.name] = required;
                                    } else {
                                        scope.form.selectValidity[attrs.name] = true;
                                    }

                                    var customValid = true;
                                    for (var j in scope.form.selectValidity) {
                                        customValid = customValid && scope.form.selectValidity[j];
                                    }

                                    scope.form.$setValidity('selectFactory', customValid);
                                }
                            };



                            function loadValue(value) {
                                var defer = $q.defer();
                                var scopeApiParams = {};
                                if (typeof scope.configuration !== 'undefined') {
                                    if (typeof scope.configuration.service !== 'undefined') {
                                        if (typeof scope.configuration.service.apiParams !== 'undefined') {
                                            scopeApiParams = scope.configuration.service.apiParams;
                                        }
                                    }
                                }

                                var apiParams = angular.merge({
                                    col: [configuration.selectize.valueField, configuration.selectize.labelField],
                                    search: {
                                        type: 'list',
                                        data: {
                                            sort: configuration.selectize.labelField,
                                            order: 'asc'
                                        }
                                    }
                                }, configuration.service.apiParams, scopeApiParams);

                                apiParams.search.data.filters = {};

                                configuration.service.object.find(value, apiParams).then(function(res) {
                                    defer.resolve(res.object);
                                }, function () {

                                });

                                return defer.promise;
                            }

                            scope.showSelectionBox = function showSelectionBox(selectConfiguration) {
                                var modalTpl = '<div id="selection-box" class="modal">' +
                                    '<div class="modal-dialog">' +
                                        '<div class="modal-content">' +
                                            '<div class="modal-header">Boîte de sélection</div>' +
                                            '<div class="modal-body">';
                                                if (attrs.sort) {
                                                    var sorts = JSON.parse(attrs.sort);
                                                    sorts.forEach(element => {
                                                        modalTpl += 
                                                            '<button ng-click="sortBy(\'' + element + '\')" class="btn btn-primary mb-1 ml-1" >' + 
                                                            translator('sort_by_'+element) + '<i class="fa fa-filter fa-fw" title="icon : filters"></i>' +
                                                            '</button>'
                                                ;
                                                    });
                                                }
                
                                modalTpl += '<div>' +
                                                    '<div class="input-group">' +
                                                        '<input type="text" placeholder="' + translator('search') + '" ng-model="search" class="form-control" />' +
                                                        '<div ng-if="!selectConfiguration.deferredSearch" class="input-group-addon">' +
                                                            '<i class="fa fa-search"></i>' +
                                                        '</div>' +
                                                        '<div ng-if="selectConfiguration.deferredSearch" class="input-group-addon " style="padding:0px;">' +
                                                        '<button class="btn btn-sm" ng-click="onLoad();" tooltip="Cliquer pour afficher tous les territoires"><i  class="fa fa-bars"></i></button>' +
                                                        '<button class="btn btn-sm" ng-click="onSearch(search);" tooltip="Cliquer pour chercher les territoires"><i  class="fa fa-search"></i></button>' +
                                                            '</div>' +
                                                    '</div>' +
                                                    '<span class="ml-2" ng-if="selectConfiguration.deferredSearch"><small><i> ( ' +  translator('click_to_search')  +')</i> </small></span>' +
                                                '</div>' +
                                                '<div class="selection-container">' +
                                                    '<ul class="selectize-control list-unstyled">' +
                                                        '<li ng-show="loading" class="text-center"><i class="fa fa-spin fa-circle-o-notch"></i></li>' +
                                                        '<ng-include src="\'selection-box-tree.html\'" ng-repeat="row in rows | filter : search"></ng-include>' +
                                                    '</ul>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="modal-footer">' +
                                                '<button class="btn btn-default" data-dismiss="modal">' + translator('close') + '</button>' +
                                                '<button class="btn btn-submit" ng-click="select();">' + translator('select') + '</button>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<script type="text/ng-template" id="selection-box-tree.html">' +
                                    '<li>' +
                                        '<div class="option pointer" ng-class="{selected: row.selected}" ' +
                                             'ng-click="onSelectItem(row)">' +
                                            '<input type="checkbox" ng-checked="row.selected" >' +
                                            '<span compile="render(row)"></span>' +
                                        '</div>' +
                                        '<ul class="list-unstyled" ng-include src="\'selection-box-tree.html\'" ng-repeat="row in row.children | filter : search"></ul>' +
                                    '</li>'+
                                '</script>';

                                var modalScope = scope.$new();

                                modalScope.render = function(row) {
                                    if (typeof configuration.selectize.render !== 'undefined' && typeof configuration.selectize.render.modal === 'function') {
                                        return configuration.selectize.render.modal(row, function(data) { return data; });
                                    }
                                    if (typeof configuration.selectize.render !== 'undefined' && typeof configuration.selectize.render.item === 'function') {
                                        return configuration.selectize.render.item(row, function(data) { return data; });
                                    }
                                    return row[configuration.selectize.labelField];
                                };

                                var order = 1;
                                var recursiveSort = function recursiveSort(rows, key) {
                                    rows.forEach(row => {
                                        if (row.children.length > 0) {
                                            row.children = recursiveSort(row.children, key);
                                        }
                                    })
                                    return rows.sort((a,b) => (a[key] > b[key]) ? (1 * order) : ((b[key] > a[key]) ? ((-1)* order) : 0)); 

                                }
                                modalScope.sortBy = function sortBy(key) {
                                    recursiveSort(modalScope.rows, key);
                                    order *= -1;
                                }
    
                                modalScope.indexOf = function (arr, val) {
                                    for (var i = 0, len = arr.length; i < len; ++i) {
                                        if ( i in arr && arr[i] == val ) {
                                            return i;
                                        }
                                    }
                                    return -1;
                                };

                                modalScope.onSelectItem = function onSelectItem(row) {
                                    if (configuration.selectize.maxItems === 1) {
                                        var rowCopy = angular.copy(row);
                                        modalScope.clear();
                                        if (!rowCopy.selected) {
                                            row.selected = true;
                                        }
                                    } else {
                                        row.selected = !row.selected;
                                    }
                                }

                                modalScope.clear = function clear(rows) {
                                    if (rows == null) {
                                        rows = modalScope.rows;
                                    }
                                    for (var i in rows) {
                                        rows[i].selected = false;
                                        if (typeof rows[i].children !== 'undefined') {
                                            if (rows[i].children.length > 0) {
                                                modalScope.clear(rows[i].children);
                                            }
                                        }
                                    }
                                };

                                modalScope.onSearch = function(query) {
                                    modalScope.load(query, true);
                                };

                                modalScope.onLoad = function() {
                                    modalScope.load('%', true);
                                };

                                
                                modalScope.select = function select() {
                                    var rows = modalScope.rows;
                                    if (configuration.service.tree) {
                                        rows = treeBuilderService.toArray(rows);
                                    }
                                    var selection = $filter('filter')(rows, {selected: true});

                                    scope.selectOptions = [];
                                    scope.selectValue    = [];

                                    var cleaningDoublons = [];

                                    // --> gestion pour les services, on a pas les droit sur tout mais la selection est dejà faite
                                    if (scope.multiple) {
                                        for (var i in scope.ngModel) {
                                            var searchMissing = {};
                                            searchMissing[configuration.selectize.valueField] = scope.ngModel[i][configuration.selectize.valueField];
                                            var missing = $filter('filter')(rows, searchMissing);
                                            if (missing.length === 0) {
                                                selection.push(scope.ngModel[i]);
                                            }
                                        }
                                    }
                                    // <--

                                    for (var i in selection) {
                                        scope.selectOptions.push(angular.copy(selection[i]));
                                        if (configuration.selectize.maxItems == 1) {
                                            scope.selectValue = selection[i][configuration.selectize.valueField];
                                        } else {
                                            if (cleaningDoublons.indexOf(selection[i][configuration.selectize.valueField]) == -1) {
                                                cleaningDoublons.push(selection[i][configuration.selectize.valueField]);
                                                scope.selectValue.push(selection[i][configuration.selectize.valueField]);
                                            }
                                        }
                                    }

                                    $timeout(function() {
                                        configuration.selectize.onChange(scope.selectValue);
                                    });

                                    var $modal = $('#selection-box');
                                    $modal.modal('hide');
                                };

                                modalScope.load = function(query, force) {
                                    modalScope.loading = true;
                                    configuration.selectize.load(query, function (rows) {     
                                        var value = scope.selectValue;
                                        if (!angular.isArray(scope.selectValue)) {
                                            value = [scope.selectValue];
                                        }
    
                                        addCurrentUser(rows);
    
                                        for(var i in rows) {
                                            rows[i].selected = modalScope.indexOf(value,rows[i][configuration.selectize.valueField]) > -1;
                                        }
    
                                        if(configuration.service.tree) {
                                            modalScope.rows = configuration.service.nnTree
                                                ? treeBuilderService.toNNTree(rows)
                                                : treeBuilderService.toTree(rows, configuration.service.treeOrder);
                                        } else {
                                            modalScope.rows = rows;
                                        }
    
                                        modalScope.loading = false;
                                    }, true);
                                }


                                if(! configuration.selectize.deferredSearch) {
                                    modalScope.load(null);
                                }


                                angular.element('body').append($compile(modalTpl)(modalScope));

                                var $modal = angular.element('#selection-box');
                                $modal.modal({
                                    show: true
                                }).on('hidden.bs.modal', function (e) {
                                    $modal.remove();
                                });
                            };

                            element.append($compile(createTemplate())(scope));

                            function addCurrentUser(rows) {
                                if (!scope.currentUser) {
                                    return;
                                }

                                rows.unshift({
                                    id: 'current-user',
                                    name: 'Utilisateur courant',
                                    avatar: '/img/default-avatar.png'
                                });
                            }
                        }
                    }
                }
            }
        }
    ]);

    app.directive('compile', ['$compile', function ($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function(value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);
                }
            );
        };
    }]);
})(window.EVA.app);
