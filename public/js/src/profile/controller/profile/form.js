(function(app) {

    app.controller('profileFormController', [
        'profileService', '$scope', 'flashMessenger', 'translator',
        function profileFormController(profileService, $scope, flashMessenger, translator) {
            var self = this;

            var $modal = angular.element('#profileFormModal');
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'price_per_hour',
                    'price_per_day',
                    'hour_per_day',
                    'createdAt',
                    'updatedAt'
                ]
            };

            self.profile = {};
            self.from    = {
                type: null,
                id: null
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id   = id;

            };

            $scope.$on('create-profile', function (event, params) {
                self.profile = {
                    _rights: {
                        update: true
                    },
                };

                self.panelTitle = translator('profile_nav_form');
                $modal.modal('show');
            });

            $scope.$on('edit-profile', function (event, profile) {
                self.editedprofile = profile;
                self.profile       = angular.copy(profile);
                self.panelTitle = self.profile.name;

                $modal.modal('show');
            });

            self.saveprofile = function saveprofile() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.profile.id;

                profileService.save(self.profile, apiParams).then(function(res) {
                    if (isCreation) {
                        $scope.$parent.self.profiles.push(res.object);
                    } else {
                        for(var i in $scope.$parent.self.profiles){
                            if($scope.$parent.self.profiles[i].id == res.object.id) {
                                $scope.$parent.self.profiles[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('profile_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.controlDates = function controlDates() {
                if (self.profile.start && self.profile.end) {
                    var start = moment(self.profile.start, 'DD/MM/YYYY');
                    var end = moment(self.profile.end, 'DD/MM/YYYY');

                    var days = end.diff(start, 'days', true);
                    if (days < 0) {
                        self.errors.fields['end'] = [translator('profile_field_end_before_start_error')];
                    } else {
                        self.errors.fields['end'] = [];
                    }
                }
            };

        }
    ])

})(window.EVA.app);
