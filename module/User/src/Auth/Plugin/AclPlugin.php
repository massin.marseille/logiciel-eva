<?php

namespace User\Auth\Plugin;

use User\Auth\Acl;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class AclPlugin
 *
 * Access acl from controller.
 *
 * @package User\Auth\Plugin
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AclPlugin extends AbstractPlugin
{
    /**
     * @var Acl
     */
    protected $acl;

    /**
     * @param Acl $acl
     */
    public function __construct(Acl $acl)
    {
        $this->acl = $acl;
    }

    /**
     * @return Acl
     */
    public function __invoke()
    {
        return $this->acl;
    }
}
