<?php

namespace Budget\Export;

use Core\Export\IExporter;

abstract class Nature implements IExporter
{
    public static function getConfig()
    {
        return [
            'parent' => [
                'property' => 'parent.name',
                'format' => 'string',
            ],
        ];
    }

    public static function getAliases()
    {
        return [
            'parent.name' => 'parent',
        ];
    }

    public static function getCustomParsedRows(
        $extractor,
        $rows,
        $objects,
        $cols,
        $itemsRemoved,
        $itemsSelected,
        $translator
    ) {
        return null;
    }
}
