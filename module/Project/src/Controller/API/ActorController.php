<?php

namespace Project\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class ActorController extends BasicRestController
{
    protected $entityClass = 'Project\Entity\Actor';
    protected $repository  = 'Project\Entity\Actor';
    protected $entityChain = 'project:e:actor';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->leftJoin('object.project', 'project');
        $queryBuilder->join('object.structure', 'structure');

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'structure.name LIKE :f_full',
                    'project.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
