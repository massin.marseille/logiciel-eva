(function(app) {

    app.controller('networkController', [
        'networkService', '$filter', '$scope',
        function networkController(networkService, $filter, $scope) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'clients.id',
                    'clients.name',
                    'master.id'
                ]
            };

            self.loading = {
                save: false,
                remove: false
            };

            self.errors = {
                fields: {}
            };

            self.networks = [];
            self.network  = {};

            networkService.findAll(apiParams).then(function(res) {
                self.networks = res.rows;
            });

            self.resetNetwork = function resetNetwork() {
                self.editNetwork({
                    name: 'Nouveau réseau',
                    clients: [],
                    master: {
                        id: null
                    }
                });
                self.clientsToAdd = [];
            };

            self.editNetwork = function editNetwork(network) {
                self.networkName     = network.name;
                self.network         = angular.copy(network);
                self.errors.fields   = {};
                self.clientsToAdd    = [];
            };

            self.saveNetwork = function saveNetwork() {
                self.errors.fields  = {};
                self.loading.save = true;

                var isCreation = self.network.id ? false : true;

                networkService.save(self.network, apiParams).then(function(res) {
                    if (res.success) {
                        if (isCreation) {
                            self.networks.push(res.object);
                            self.editNetwork(res.object);
                        } else {
                            var baseNetwork = $filter('filter')(self.networks, function(item) {
                                return item.id === self.network.id;
                            })[0];
                            self.networks[self.networks.indexOf(baseNetwork)] = res.object;
                            self.networkName = res.object.name;
                        }
                    }

                    self.loading.save = false;
                }, function(err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                });
            };

            self.removeNetwork = function removeNetwork(network) {
                if (confirm('Voulez-vous réellement supprimer ce réseau ?')) {
                    self.loading.remove = true;
                    networkService.remove(network).then(function(res) {
                        if (res.success) {
                            var baseNetwork = $filter('filter')(self.networks, function(item) {
                                return item.id === self.network.id;
                            })[0];
                            self.networks.splice(self.networks.indexOf(baseNetwork), 1);
                            self.resetNetwork();
                        }

                        self.loading.remove = false;
                    }, function() {
                        self.loading.remove = false;
                    });
                }
            };

            self.removeClient = function removeClient(client) {
                if (confirm('Voulez-vous réellement retirer ce client du réseau ?')) {
                    self.network.clients.splice(self.network.clients.indexOf(client), 1);
                }
            };

            self.addClients = function addClients() {
                if (!angular.isArray(self.network.clients)) {
                    self.network.clients = [];
                }
                for (var i = 0; i < self.clientsToAdd.length; i++) {
                    var filter = $filter('filter')(self.network.clients, function(item) {
                        return item.id === self.clientsToAdd[i].id;
                    });
                    if (filter == null || filter.length == 0) {
                        self.network.clients.push(self.clientsToAdd[i]);
                        self.network.clientAdded = true;
                    }
                }

                self.clientsToAdd = [];
            };

            self.resetNetwork();
        }
    ])

})(window.EVA.app);
