<?php

return [
    'icons' => [
        'map' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-map-o'
        ],
        'territory' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-map-marker'
        ],
        'focus' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-crosshairs'
        ]
    ]
];
