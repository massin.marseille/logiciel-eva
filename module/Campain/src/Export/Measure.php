<?php

namespace Campain\Export;

use Core\Export\IExporter;

abstract class Measure implements IExporter
{
    public static function getConfig()
    {
        return [
            'realiseMeasure.indicator' => [
                'property' => 'realiseMeasure.indicator.name'
            ],
            'realiseMeasure.groupIndicator' => [
                'property' => 'realiseMeasure.groupIndicator.name'
            ],
            'realiseMeasure.project' => [
                'property' => 'realiseMeasure.project.name'
            ]
        ];
    }
    
    public static function getAliases()
    {
        return [
            'realisedIndicator.name' => 'realiseMeasure.indicator.name',
            'realisedgroupIndicator.name' => 'realiseMeasure.groupIndicator.name',
            'realiseproject.name' => 'realiseMeasure.project.name'

        ];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
