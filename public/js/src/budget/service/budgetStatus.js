(function(app) {

    app.factory('budgetStatusService', [
        'restFactoryService',
        function budgetStatusService(restFactoryService) {
            return restFactoryService.create('/api/budget/budget-status');
        }
    ])

})(window.EVA.app);
