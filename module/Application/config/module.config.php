<?php

namespace Application;

return [
    'module'       => [
        'application' => [
            'environments' => [
                'client',
            ],

            'active'   => true,
            'required' => true,

            'acl' => [
                'entities' => [
                    [
                        'name'  => 'configuration',
                        'class' => 'Application\Entity\Configuration',
                        'owner' => []
                    ],
                    [
                        'name'  => 'query',
                        'class' => 'Application\Entity\Query',
                        'owner' => ['own', 'service']
                    ],
                ],
            ],
            'configuration' => [
                'mailingDomainName' => [
                    'type' => 'text',
                    'text' => 'Mailing Domain Name'
                ],
                'mailingHost' => [
                    'type' => 'text',
                    'text' => 'Mailing Host',
                ],
                'mailingPort' => [
                    'type' => 'text',
                    'text' => 'Mailing Port'
                ],
                'mailingAuthType' => [
                    'type' => 'unique',
                    'text' => 'Mailing Authentication Type',
                    'options' => [
						[
							'value' => 'smtp',
							'text' => 'SMTP',
						],
						[
							'value' => 'plain',
							'text' => 'Plain',
						],
						[
							'value' => 'login',
							'text' => 'Login',
						],
						[
							'value' => 'crammd5',
							'text' => 'CRAM MD5',
						],
					],
                ],
                'mailingAuthUser' => [
                    'type' => 'text',
                    'text' => 'Mailing Authentication User'
                ],
                'mailingAuthPassword' => [
                    'type' => 'text',
                    'text' => 'Mailing Authentication Password'
                ],
                'mailingAuthMode' => [
                    'type' => 'unique',
                    'text' => 'Mailing Authentication Mode',
                    'options' => [
                        [
							'value' => 'ssl',
							'text' => 'SSL',
						],
						[
							'value' => 'tls',
							'text' => 'TLS',
						]
                    ]
                ]
            ]
        ],
    ],
    'translator'   => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
    'view_manager' => [
        'template_map'        => [
            'layout/layout'     => __DIR__ . '/../view/layout/layout.phtml',
            'error/404'         => __DIR__ . '/../view/error/404.phtml',
            'error/index'       => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
