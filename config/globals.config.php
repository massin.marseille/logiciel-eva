<?php

$configurations = [
    'factories' => [],
    'commands'  => [],
];

// Modules auto-loading
foreach (scandir(getcwd() . '/module') as $module) {
    $pathFile = getcwd() . '/module/' . $module . '/config/global.config.php';
    if (
        $module !== '.' &&
        $module !== '..' &&
        file_exists($pathFile)
    ) {
        $configByModule = require $pathFile;
        if ($configByModule) {
            if (isset($configByModule['service_manager']['factories'])) {
                $configurations['factories'] = array_merge($configurations['factories'], $configByModule['service_manager']['factories']);
            }
            if (isset($configByModule['laminas-cli']['commands'])) {
                $configurations['commands'] = array_merge($configurations['commands'], $configByModule['laminas-cli']['commands']);
            }
        }
    }
}

return $configurations;
