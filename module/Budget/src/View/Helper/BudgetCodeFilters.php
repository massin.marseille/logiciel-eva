<?php

namespace Budget\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class BudgetCodeFilters extends EntityFilters
{
	public function __construct(
		Translate $translator,
		MyModuleManager $moduleManager,
		EntityManager $entityManager
	) {
		parent::__construct($translator, $moduleManager, $entityManager);

		$filters = [
			'name' => [
				'label' => $this->translator->__invoke('budgetCode_field_name'),
				'type' => 'string',
			],
			'createdAt' => [
				'label' => $this->translator->__invoke('budgetCode_field_createdAt'),
				'type' => 'date',
			],
			'updatedAt' => [
				'label' => $this->translator->__invoke('budgetCode_field_updatedAt'),
				'type' => 'date',
			],
		];

		$this->filters = $filters;
	}
}
