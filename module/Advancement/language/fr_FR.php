<?php

return [

    'advancement_module_title' => '[[ advancement_entity | ucfirst ]]',
    'advancement_entity' => 'avancement',

    'advancement_field_project' => '[[ project_entity | ucfirst ]]',
    'advancement_field_date'    => 'Date',
    'advancement_field_value'   => 'Valeur',
    'advancement_field_type'    => 'Type',
    'advancement_field_createdAt'  => '[[ field_updated_at_m ]]',
    'advancement_field_updatedAt'  => '[[ field_created_at_m ]]',
    'advancement_field_name'       => 'Titre',
    'advancement_field_description'=> 'Description',

    'advancement_type_target' => 'Prévu',
    'advancement_type_done'   => 'Réalisé',

    'advancement_nav_form'    => 'Créer un [[ advancement_entity ]]',

    'advancement_question_delete' => 'Voulez-vous réellement supprimer cet [[ advancement_entity ]]',
    'advancement_message_deleted' => '[[ advancement_entity | ucfirst ]] supprimé',
    'advancement_message_saved'   => '[[ advancement_entity | ucfirst ]] enregistré',

    'advancement_field_project_error_non_exists' => 'Cette [[ project_entity ]] n\'existe pas ou plus',

    'tooltip_switch_view_advancement_timeline' => 'Vue timeline',
    'tooltip_switch_view_advancement_list'     => 'Vue liste',
];
