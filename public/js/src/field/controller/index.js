(function(app) {

    app.controller('fieldIndexController', [
        'fieldGroupService', 'templateService','flashMessenger', 'translator', '$rootScope',
        function fieldIndexController(fieldGroupService, templateService, flashMessenger, translator, $rootScope) {
            var self = this;
            self.entity = null;
            self.groups = [];
            self.templates = [];

            self.apiParams = {
                col: [
                    'id',
                    'name',
                    'entity',
                    'tab',
                    'enabled',
                    'fields.id',
                    'fields.name',
                    'fields.help',
                    'fields.type',
                    'fields.group.id',
                    'fields.meta',
                    'fields.order',
                    'fields.placeholder'
                ],
                search: {
                    type: 'list',
                    data: {
                        sort: 'name',
                        order: 'asc',
                        filters: {}
                    }
                }
            };

            self.init = function init(entity) {
                self.entity = entity;
                self.apiParams.search.data.filters = {
                    entity: {
                        op: 'eq',
                        val: entity
                    }
                };
                templateService.findAll({col:['id', 'name', 'config']}).then((templateResult) => {
                   self.templates = templateResult.rows;
                }).then(() => {
                    fieldGroupService.findAll(self.apiParams).then((res) => {
                        self.groups = res.rows;
                        for (var i in self.groups) {
                            for (var j in self.groups[i].fields) {
                                if (typeof self.groups[i].fields[j].meta === 'undefined' || self.groups[i].fields[j].meta.length === 0) {
                                    self.groups[i].fields[j].meta = {};
                                }
                            }
                            if (self.groups[i].entity == 'Project\\Entity\\Project') {
                                self.groups[i].tabname = self.getTabNameProject(self.groups[i].tab);
                            }
                            self.groups[i].templates = [];
                            for (var k in self.templates) {
                                Object.keys(self.templates[k].config.custom).forEach(key => {
                                    if(self.templates[k].config.custom[key].show == "true" && key == self.groups[i].id){
                                        self.groups[i].templates.push(self.templates[k]);
                                    }
                                })
                            };
                        }
                    });
                })
            };
            self.getTabName = function(tab){
                var name = '';
                switch(tab) {
                    case 2:
                        name = translator('fieldGroup_englet_team');
                        break;
                    case 3:
                        name = translator('fieldGroup_englet_indicateur');
                        break;
                    case 4:
                        name = translator('fieldGroup_englet_territoire');
                        break;
                    case 5:
                        name = translator('fieldGroup_englet_acteur');
                        break;
                    case 6:
                        name = translator('fieldGroup_englet_document');
                        break;
                    case 7:
                        name = translator('fieldGroup_englet_fiche');
                        break;
                    default:
                        name = '';
                }
                return name;
            };
            self.getTabNameProject = function(tab){
                var name = '';
                switch (tab) {
                    case 1:
                        name = translator('fieldGroup_tab_project');
                        break;
                    case 2:
                        name = translator('fieldGroup_tab_members');
                        break;
                    case 3:
                        name = translator('fieldGroup_tab_hierarchy');
                        break;
                    case 4:
                        name = translator('fieldGroup_tab_field');
                        break;
                    case 5:
                        name = translator('fieldGroup_tab_actors');
                        break;
                    case 6:
                        name = translator('fieldGroup_tab_territory');
                        break;
                    case 7:
                        name = translator('fieldGroup_tab_time');
                        break;
                    case 8:
                        name = translator('fieldGroup_tab_indicator');
                        break;
                    case 9:
                        name = translator('fieldGroup_tab_budget');
                        break;
                    case 10:
                        name = translator('fieldGroup_tab_convention');
                        break;
                    case 11:
                        name = translator('fieldGroup_tab_advancement');
                        break;
                    case 12:
                        name = translator('fieldGroup_tab_task');
                        break;
                    case 13:
                        name = translator('fieldGroup_tab_attachments');
                        break;

                }
                return name;
            };

            self.editedGroup        = null;
            self.editedGroupFields  = null;
            self.editedGroupErrors  = {};
            self.editedGroupLoading = false;

            self.enableGroup = function (group) {
                self.editedGroupErrors  = {};

                fieldGroupService.save(group, {
                    col: self.apiParams.col
                }).then(function (res) {
                    if (res.success) {
                        group = angular.extend(group, res.object);

                        if (group.enabled === true) {
                            flashMessenger.success(translator('fieldGroup_message_enabled'));
                        } else {
                            flashMessenger.success(translator('fieldGroup_message_disabled'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }
                });
            };

            self.editGroup = function editGroup(group) {
                self.editedGroupLoading = false;
                self.editedGroupErrors  = {};
                self.editedGroup        = group;
                self.editedGroupFields  = angular.copy(group);
                self.editedGroupFields.tab = ""+self.editedGroupFields.tab+""
            };

            self.saveEditedGroup = function saveEditedGroup(group) {
                self.editedGroupErrors  = {};
                self.editedGroupLoading = true;
                fieldGroupService.save(angular.extend({}, group, self.editedGroupFields), {
                    col: self.apiParams.col
                }).then(function (res) {
                    if (res.success) {
                        group = angular.extend(group, res.object);
                        self.editedGroup       = null;
                        self.editedGroupFields = null;
                        if (group.entity == 'Project\\Entity\\Project') {
                            if (!group.tab){
                                group.tab = 4; // Field
                            }
                            group.tabname = self.getTabNameProject(group.tab);
                        }
                        flashMessenger.success(translator('fieldGroup_message_saved'));
                    }
                    self.editedGroupLoading = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }

                    self.editedGroupLoading = false;
                });
                for(let j in self.templates) {
                    Object.keys(self.templates[j].config.custom).forEach(key => {
                        self.templates[j].config.custom[key].show = "false";
                        for(let i in self.groups) {
                            for(let k in self.groups[i].templates) {
                                if (self.groups[i].id == key && self.groups[i].templates[k].id == self.templates[j].id) {
                                    self.templates[j].config.custom[key].show = "true";
                                }
                            }
                        }
                    })

                templateService.save(self.templates[j], {}).then(function () {
                }, function (err) {
                    console.error(err);
                });
                }
            };

            self.deleteGroup = function deleteGroup(group) {
                if (confirm(translator('fieldGroup_question_delete').replace('%1', group.name))) {
                    fieldGroupService.remove(group).then(function (res) {
                        if (res.success) {
                            self.groups.splice(self.groups.indexOf(group), 1);
                            flashMessenger.success(translator('fieldGroup_message_deleted'));
                        } else {
                            flashMessenger.success(translator('error_occured'));
                        }
                    }, function () {});
                }
            };

            self.createGroup = function createGroup() {
                fieldGroupService.save({
                    entity: self.entity,
                    name: 'Nouveau groupe de champs',
                    enabled: true,
                    tab: 0
                }, {
                    col: self.apiParams.col
                }).then(function (res) {
                    if (res.success) {
                        self.groups.push(res.object);
                        self.editGroup(res.object);

                        flashMessenger.success(translator('fieldGroup_message_saved'));
                    }
                }, function () {});
            };

            /***** FIELDS *****/
            self.createField = function createField(group) {
                $rootScope.$broadcast('create-field', {group: group})
            };

            self.editField = function editField(field, group) {
                $rootScope.$broadcast('edit-field', {field: field, group: group})
            };

            self.deleteField = function editField(field, group) {
                $rootScope.$broadcast('delete-field', {field: field, group: group})
            };
        }
    ]);

})(window.EVA.app);
