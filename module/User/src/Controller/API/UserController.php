<?php

namespace User\Controller\API;

use Core\Controller\BasicRestController;

class UserController extends BasicRestController
{
    protected $entityClass = 'User\Entity\User';
    protected $repository  = 'User\Entity\User';
    protected $entityChain = 'user:e:user';

    protected function searchList($queryBuilder, &$data)
    {
        $sort    = isset($data['sort'])    ? $data['sort']    : 'object.login';
        $order   = isset($data['order'])   ? $data['order']   : 'ASC';

        $queryBuilder->join('object.role', 'role');

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        //$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                    'object.login LIKE :f_full',
                    'role.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    public function create($data)
    {
        $this->uploadAvatar($data);
        return parent::create($data);
    }

    public function update($id, $data)
    {
        $this->uploadAvatar($data);
        return parent::update($id, $data);
    }

    protected function uploadAvatar(&$data)
    {
        if (isset($data['avatar'])) {
            $avatar = $data['avatar'];
            if (strpos($avatar, 'data:image') !== false) {
                $filePath = '/clients/' . $this->environment()->getClient()->getKey(). '/avatar/' . uniqid() . '.png';
                $file   = fopen(getcwd() . '/public' . $filePath, 'wb');
                $base64 = explode(',', $avatar);
                fwrite($file, base64_decode($base64[1]));
                fclose($file);

                $data['avatar'] = $filePath;
            }
        }
    }
}
