(function(app) {

    app.directive('posteExpenseSelect', [
        'posteExpenseService', 'selectFactoryService',
        function accountSelect(posteExpenseService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: posteExpenseService,
                    apiParams: {
                        col: ['id', 'account.id', 'account.code', 'name']
                    },
                    full: true
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['account.code', 'name'],
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' + (data.account ? escape(data.account.code) + ' - ' : '') + escape(data.name) + '</div>';
                        },
                        item: function (data, escape) {
                            return '<div class="option">' + (data.account ? escape(data.account.code) + ' - ' : '') + escape(data.name) + '</div>';
                        }
                    }
                }
            });
        }
    ])

})(window.EVA.app);
