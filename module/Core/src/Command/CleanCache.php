<?php

namespace Core\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanCache extends AbstractCommand
{

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[START] Clean minified JS files...');

        $files = glob(getcwd() . '/public/js/min/*.js');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        $output->writeln('Clean Doctrine proxies...');
        $files = glob(getcwd() . '/data/DoctrineORMModule/Proxy/*.php');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        $output->writeln('Clean ZF2 merged configuration...');
        $files = glob(getcwd() . '/data/cache/*.php');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        // if(function_exists('apcu_clear_cache'))
        // {
        //     echo PHP_EOL . 'Clean Apc/Apcu cache...';
        //     apcu_clear_cache();
        //     $cacheDriver = new \Doctrine\Common\Cache\ApcuCache();
        //     $cacheDriver->deleteAll();
        // }

        $output->writeln('[END] Clean minified JS files...');
        return Command::SUCCESS;
    }
}
