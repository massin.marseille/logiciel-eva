(function(app) {

    app.factory('configurationService', [
        'restFactoryService',
        function configurationService(restFactoryService) {
            return restFactoryService.create('/api/configuration');
        }
    ])

})(window.EVA.app);
