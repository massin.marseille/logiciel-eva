<?php

namespace Field\Controller\Plugin;

use Doctrine\ORM\EntityManager;
use Field\Entity\Group;

/**
 * Class AllowedFieldGroups
 *
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AllowedFieldGroups
{
    protected $entityManager;
    protected $user;
    protected $acl;

    public function __construct(EntityManager $entityManager, $acl, $user)
    {
        $this->entityManager = $entityManager;
        $this->user = $user;
        $this->acl = $acl;
    }

    public function findByEntity($entity)
    {
        $user = null;
        return $this->entityManager->getRepository(Group::class)->findByEntity($entity, $user);
    }
}
