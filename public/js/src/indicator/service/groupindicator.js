(function(app) {

    app.factory('groupIndicatorService', [
        'restFactoryService',
        function indicatorService(restFactoryService) {
            return restFactoryService.create('/api/group-indicator');
        }
    ])

})(window.EVA.app);
