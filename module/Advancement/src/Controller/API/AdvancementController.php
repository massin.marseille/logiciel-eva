<?php

namespace Advancement\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class AdvancementController extends BasicRestController
{
    protected $entityClass  = 'Advancement\Entity\Advancement';
    protected $repository   = 'Advancement\Entity\Advancement';
    protected $entityChain  = 'advancement:e:advancement';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.date';
        $order = $data['order'] ? $data['order'] : 'ASC';

        $queryBuilder->join('object.project', 'project');

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.type LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }


}
