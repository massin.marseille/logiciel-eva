<?php

namespace Directory\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractPerson
 * @package Directory\Entity
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractPerson extends AbstractContact
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $lastName;

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        $this->name = $this->getName();
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        $this->name = $this->getName();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);
        
        $inputFilter->get('name')->setRequired(false);

        $inputFilter->add([
            'name'     => 'gender',
            'required' => false
        ], 'gender');

        $inputFilter->add([
            'name'     => 'firstName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
        ], 'firstName');

        $inputFilter->add([
            'name'     => 'lastName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
        ], 'lastName');

        return $inputFilter;
    }
}
