(function(app) {

    app.factory('translator', [
        function translator() {
            var self = this;
            self.translations = typeof translations == 'undefined' ? [] : translations;

            return function translate(key) {
                return typeof self.translations[key] !== 'undefined' ? self.translations[key] : key;
            }
        }
    ]);

    app.filter('translate', [
        'translator',
        function(translator) {
            return function(string) {
                return translator(string)
            }
        }
    ]);

})(window.EVA.app);
