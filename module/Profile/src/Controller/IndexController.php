<?php

namespace Profile\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('profile:e:profile:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }
}



