(function(app) {
	app.controller('templateListController', [
		'templateService',
		'$scope',
		'flashMessenger',
		'translator',
		function templateListController(templateService, $scope, flashMessenger, translator) {
			var self = this;

			self.templates = [];
			self.options = {
				name: 'templates-list',
				col: ['id', 'name', 'config', 'default', 'createdAt', 'updatedAt'],
				searchType: 'list',
				sort: 'name',
				order: 'asc',
				pager: true,
				getItems: function getData(params, callback, abort) {
					templateService.findAll(params, abort.promise).then(function(res) {
						callback(res);
					});
				},
			};

			self.defaultTemplate = function(_template) {
				templateService.save(_template, { col: ['id', 'default'] }).then(function(res) {
					if (res.success) {
						if (_template.default == true) {
							for (var i in self.templates) {
								var template = self.templates[i];
								if (template.id == _template.id) {
									template.default = true;
								} else {
									template.default = false;
								}
							}
							flashMessenger.success(translator('template_message_default'));
						} else {
							flashMessenger.success(translator('template_message_notDefault'));
						}
					}
				});
			};

			self.fullTextSearch = function fullTextSearch() {
				$scope.__tb.resetFilters();
				$scope.__tb.apiParams.search.data.full = self.fullText;
				$scope.__tb.loadData();
			};

			self.resetfullTextSearch = function resetfullTextSearch() {
				self.fullText = '';
				delete $scope.__tb.apiParams.search.data.full;
			};

			angular.element('#full-text').on('keypress', function(event) {
				if (event.which == 13) {
					self.fullTextSearch();
				}
			});

			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};
			self.editLine = function editLine(template) {
				self.editedLineErrors = {};
				self.editedLine = template;
				self.editedLineFields = angular.copy(template);
			};

			self.saveEditedLine = function saveEditedLine(template) {
				self.editedLineErrors = {};
				templateService
					.save(angular.extend({}, template, self.editedLineFields), {
						col: self.options.col,
					})
					.then(
						function(res) {
							if (res.success) {
								template = angular.extend(template, res.object);
								self.editedLine = null;
								self.editedLineFields = null;

								flashMessenger.success(translator('template_message_saved'));
							}
						},
						function(err) {
							for (var field in err.fields) {
								self.editedLineErrors[field] = err.fields[field];
							}
						},
					);
			};

			self.deleteTemplate = function deleteTemplate(template) {
				if (confirm(translator('template_question_delete').replace('%1', template.name))) {
					templateService.remove(template).then(function(res) {
						if (res.success) {
							self.templates.splice(self.templates.indexOf(template), 1);
							flashMessenger.success(translator('template_message_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				}
			};
		},
	]);
})(window.EVA.app);
