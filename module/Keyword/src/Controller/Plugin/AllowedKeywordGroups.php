<?php

namespace Keyword\Controller\Plugin;

use Doctrine\ORM\EntityManager;
use Keyword\Entity\Group;

/**
 * Class AllowedKeywordGroups
 *
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AllowedKeywordGroups
{
    protected $entityManager;
    protected $user;
    protected $acl;

    public function __construct(EntityManager $entityManager, $acl, $user)
    {
        $this->entityManager = $entityManager;
        $this->user = $user;
        $this->acl = $acl;
    }

    public function findByEntity($entity, $archived = null, $search = null)
    {
        $user = null;
        return $this->entityManager->getRepository(Group::class)->findByEntity($entity, $archived, $search, $user);
    }
}
