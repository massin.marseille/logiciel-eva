(function(app) {

    app.factory('bookmarkService', [
        'restFactoryService',
        function bookmarkService(restFactoryService) {
            return restFactoryService.create('/api/timesheet/bookmark');
        }
    ])

})(window.EVA.app);
