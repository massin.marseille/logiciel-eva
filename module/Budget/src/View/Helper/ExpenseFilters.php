<?php

namespace Budget\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class ExpenseFilters extends EntityFilters
{
	public function __construct(
		Translate $translator,
		MyModuleManager $moduleManager,
		EntityManager $entityManager,
		$expenseTypes,
        $allowedKeywordGroups
	) {
		parent::__construct($translator, $moduleManager, $entityManager);

		$typeOptions = [];
		foreach ($expenseTypes as $type) {
			$typeOptions[] = [
				'value' => $type,
				'text'  => $this->translator->__invoke('expense_type_' . $type),
			];
		}

		$filters = [
			'name' => [
				'label' => $this->translator->__invoke('expense_field_name'),
				'type' => 'string',
			],
			'amount' => [
				'label' => $this->translator->__invoke('expense_field_amount'),
				'type' => 'number',
			],
			'type' => [
				'label' => $this->translator->__invoke('income_field_type'),
				'type' => 'manytoone',
				'options' => $typeOptions,
			],
			'tiers' => [
				'label' => $this->translator->__invoke('expense_field_tiers'),
				'type' => 'string',
			],
			'anneeExercice' => [
				'label' => $this->translator->__invoke('expense_field_anneeExercice'),
				'type' => 'string',
			],
			'bordereau' => [
				'label' => $this->translator->__invoke('expense_field_bordereau'),
				'type' => 'string',
			],
			'mandat' => [
				'label' => $this->translator->__invoke('expense_field_mandat'),
				'type' => 'string',
			],
			'engagement' => [
				'label' => $this->translator->__invoke('expense_field_engagement'),
				'type' => 'string',
			],
			'complement' => [
				'label' => $this->translator->__invoke('expense_field_complement'),
				'type' => 'string',
			],
			'managementUnit' => [
				'label' => $this->translator->__invoke('expense_field_managementUnit'),
				'type' => 'management-unit-select'
			],
			'dateFacture' => [
				'label' => $this->translator->__invoke('expense_field_dateFacture'),
				'type' => 'date',
				'onlyPeriod' => true,
			],
			'date' => [
				'label' => $this->translator->__invoke('expense_field_date'),
				'type' => 'date',
				'onlyPeriod' => true,
			],
			'poste.project' => [
				'label' => $this->translator->__invoke('poste_expense_field_project'),
				'type' => 'project-select',
			],
			'createdAt' => [
				'label' => $this->translator->__invoke('expense_field_createdAt'),
				'type' => 'date',
			],
			'updatedAt' => [
				'label' => $this->translator->__invoke('expense_field_updatedAt'),
				'type' => 'date',
			],
		];

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'ht')) {
			$filters['amountHT'] = [
				'label' => $this->translator->__invoke('expense_field_amountHT'),
				'type' => 'number',
			];
		}

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'evolution')) {
			$filters['evolution'] = [
				'label' => $this->translator->__invoke('expense_field_evolution'),
				'type' => 'boolean',
			];
		}

		if ($this->moduleManager->isActive('keyword')) {
			$groups = $allowedKeywordGroups('expense', false);

			foreach ($groups as $group) {
				$filters['keyword.' . $group->getId()] = [
					'label' => $group->getName(),
					'type' => 'keyword-select',
					'group' => $group->getId(),
				];
			}
		}

		if ($this->moduleManager->isActive('age')) {
			$filters['age'] = [
				'label' => 'Importé via AGE',
				'type'  => 'boolean',
			];
		}

		$this->filters = $filters;
	}
}
