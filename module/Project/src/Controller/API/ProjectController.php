<?php

namespace Project\Controller\API;

use Advancement\Entity\Advancement;
use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Core\Controller\BasicRestController;
use Core\Email\Email;
use Core\Utils\MailUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Project\Entity\Member;
use Project\Entity\Project;
use Laminas\Http\Request;
use Laminas\View\Model\JsonModel;

class ProjectController extends BasicRestController
{
	protected $entityClass = 'Project\Entity\Project';
	protected $repository = 'Project\Entity\Project';
	protected $entityChain = 'project:e:project';

	public function create($data)
	{
		$vm = parent::create($data);
		if (count($vm->getVariable('errors')['fields']) > 0) {
			return $vm;
		}
		$object = $vm->getVariable('object');
		$project = $this->getEntityManager()
			->getRepository($this->repository)
			->find($object['id']);

		$this->serviceLocator
			->get('ProjectUpdateService')
			->logProjectUpdate($project);

		if (
			$vm->getVariable('success') === true &&
			$this->serviceLocator->get('MyModuleManager')->isActive('advancement')
		) {			
			// When a project is created, if dates are entered, create advancement
			if ($project->getPlannedDateStart() !== null) {
				$advancement = new Advancement();
				$advancement->setProject($project);
				$advancement->setType(Advancement::TYPE_TARGET);
				$advancement->setValue(0);
				$advancement->setDate($project->getPlannedDateStart());
				$this->getEntityManager()->persist($advancement);
			}

			if ($project->getPlannedDateEnd() !== null) {
				$advancement = new Advancement();
				$advancement->setProject($project);
				$advancement->setType(Advancement::TYPE_TARGET);
				$advancement->setValue(100);
				$advancement->setDate($project->getPlannedDateEnd());
				$this->getEntityManager()->persist($advancement);
			}

			if ($project->getRealDateStart() !== null) {
				$advancement = new Advancement();
				$advancement->setProject($project);
				$advancement->setType(Advancement::TYPE_DONE);
				$advancement->setValue(0);
				$advancement->setDate($project->getRealDateStart());
				$this->getEntityManager()->persist($advancement);
			}

			if ($project->getRealDateEnd() !== null) {
				$advancement = new Advancement();
				$advancement->setProject($project);
				$advancement->setType(Advancement::TYPE_DONE);
				$advancement->setValue(100);
				$advancement->setDate($project->getRealDateEnd());
				$this->getEntityManager()->persist($advancement);
			}

			$this->getEntityManager()->flush();
		}

		return $vm;
	}

	public function update($id, $data)
	{
		$vm = parent::update($id, $data);
		if (count($vm->getVariable('errors')['fields']) > 0) {
			return $vm;
		}

		$object = $vm->getVariable('object');
		$project = $this->getEntityManager()
			->getRepository($this->repository)
			->find($object['id']);
		$object = $this->saveMembersKeywords($data['members'], $project, $object);
		$vm->setVariable('object', $object);

		$this->serviceLocator
			->get('ProjectUpdateService')
			->logProjectUpdate($project);

		if ($this->params()->fromQuery('mail', false) && $vm->getVariable('success') === true) {
			$object = $vm->getVariable('object');
			$project = $this->getEntityManager()
				->getRepository($this->repository)
				->find($object['id']);

			$helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
			$translator = $helperPluginManager->get('translate');

			if ($project->getPublicationStatus() === Project::PUBLICATION_STATUS_WAITING_VALIDATION) {
				$currentUser = $this->authStorage()->getUser();
				foreach ($project->getValidators() as $validator) {
					if ($validator->getUser()->getEmail()) {
						$email = new Email($this->client);
						$email->setSubject(
							ucfirst($translator->__invoke('project_entity')) .
								' avec statut ' .
								$translator->__invoke(
									'project_publicationStatus_' . $project->getPublicationStatus()
								)
						);
						$email->addHtmlContent(
							$translator->__invoke('user_gender_' . $currentUser->getGender()) .
								' ' .
								$currentUser->getFirstName() .
								' ' .
								$currentUser->getLastName() .
								' vous sollicite sur la fiche "' .
								$project->getName() .
								'" qui est "' .
								$translator->__invoke(
									'project_publicationStatus_' . $project->getPublicationStatus()
								) .
								'".
                            <br />
                            Vous pouvez consulter cette fiche en cliquant sur le lien suivant : <a href="http://' .
								$this->environment()
								->getClient()
								->getHost() .
								$this->url()->fromRoute('project/form', ['id' => $project->getId()]) .
								'">http://' .
								$this->environment()
								->getClient()
								->getHost() .
								$this->url()->fromRoute('project/form', ['id' => $project->getId()]) .
								'</a>
                            <br />
                            <br />
                            <small>Ceci est un e-mail automatique</small>
                        '
						);
						$email->setFrom(MailUtils::getFrom($this->environment()->getClient(), 'notification'), 'Notification');
						$email->sendTo($validator->getUser()->getEmail());
					}
				}
			} elseif ($project->getPublicationStatus() === Project::PUBLICATION_STATUS_VALIDATED) {
				$currentUser = $this->authStorage()->getUser();
				foreach ($project->getManagers() as $manager) {
					if ($manager->getUser()->getEmail()) {
						$email = new Email($this->client);
						$email->setSubject(
							ucfirst($translator->__invoke('project_entity')) .
								' avec statut ' .
								$translator->__invoke(
									'project_publicationStatus_' . $project->getPublicationStatus()
								)
						);
						$email->addHtmlContent(
							$translator->__invoke('user_gender_' . $currentUser->getGender()) .
								' ' .
								$currentUser->getFirstName() .
								' ' .
								$currentUser->getLastName() .
								' a répondu sur la fiche "' .
								$project->getName() .
								'" qui est "' .
								$translator->__invoke(
									'project_publicationStatus_' . $project->getPublicationStatus()
								) .
								'".
                            <br />
                            Vous pouvez consulter cette fiche en cliquant sur le lien suivant : <a href="http://' .
								$this->environment()
								->getClient()
								->getHost() .
								$this->url()->fromRoute('project/form', ['id' => $project->getId()]) .
								'">http://' .
								$this->environment()
								->getClient()
								->getHost() .
								$this->url()->fromRoute('project/form', ['id' => $project->getId()]) .
								'</a>
                            <br />
                            <br />
                            <small>Ceci est un e-mail automatique</small>
                        '
						);
						$email->setFrom(MailUtils::getFrom($this->environment()->getClient(), 'notification'), 'Notification');
						$email->sendTo($manager->getUser()->getEmail());
					}
				}
			}
		}

		return $vm;
	}

	protected function searchList(QueryBuilder $queryBuilder, &$data)
	{
		// Master can only access projects networkAccessible
		if ($this->isMasterRequest) {
			$data['filters']['networkAccessible'] = [
				'op' => 'eq',
				'val' => 1,
			];
		}

		$sort = $data['sort'] ? $data['sort'] : 'object.code';
		$order = $data['order'] ? $data['order'] : 'ASC';

		if (strpos($sort, '.') === false) {
			$sort = 'object.' . $sort;
		}

		$queryBuilder->leftJoin('object.ownership', 'ownership');
		$queryBuilder->leftJoin('object.parent', 'parent');
		$queryBuilder->leftJoin('object.template', 'template');

		//if ($this->serviceLocator->get('MyModuleManager')->isActive('budget')) {
		$queryBuilder->leftJoin('object.budgetStatus', 'budgetStatus');
		$queryBuilder->leftJoin('object.posteIncomes', 'posteIncomes');
		$queryBuilder->addSelect('MIN(posteIncomes.caduciteStart) AS HIDDEN caduciteStart');
		$queryBuilder->addSelect('MIN(posteIncomes.caduciteEnd) AS HIDDEN caduciteEnd');
		//}

		//if ($this->serviceLocator->get('MyModuleManager')->isActive('advancement')) {
		$queryBuilder->leftJoin(
			'object.advancements',
			'advancements',
			'WITH',
			'advancements.type = :advancementTypeDone AND advancements.date = (
                SELECT MAX(advMax.date)
                FROM Advancement\Entity\Advancement advMax
                WHERE advMax.project = object.id AND advMax.type = :advancementTypeDone
            ) '
		);
		$queryBuilder->addSelect('MAX(advancements.value) AS HIDDEN advancement');

		$queryBuilder->setParameter('advancementTypeDone', Advancement::TYPE_DONE);
		//}

		$queryBuilder->groupBy('object.id');

		$sort = str_replace('virtual.', '', $sort);

		$queryBuilder->addOrderBy($sort, $order);
		$queryBuilder->addOrderBy('object.id', 'asc');

		if (isset($data['full']) && $data['full']) {
			$queryBuilder->andWhere(
				$queryBuilder
					->expr()
					->orX(
						...[
							'object.code LIKE :f_full',
							'object.name LIKE :f_full',
							'object.objectifs LIKE :f_full',
							'object.description LIKE :f_full',
							'object.context LIKE :f_full',
							'object.result LIKE :f_full',
							'object.prevResult LIKE :f_full',
						]
					)
			);

			$queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
		}

		if (isset($data['filters'])) {
			foreach ($data['filters'] as $property => $filter) {
				if (
					!isset($filter['val']) &&
					!(isset($filter['op']) && $filter['op'] == 'isNull') &&
					!(isset($filter['op']) && $filter['op'] == 'isNotNull')
				) {
					continue;
				}

				if ($property == 'members' && isset($filter['op'])) {
					if ($filter['op'] === 'group') {
						$restriction = 'object.id IN (
                            SELECT p1.id
                            FROM Project\Entity\Member m1
                            JOIN m1.user u1
                            JOIN m1.project p1
                            WHERE ';
						$this->addKeywordFiltersQuery('m1.user', $filter, $restriction, $queryBuilder);
					} elseif ($filter['op'] == 'service') {
						$restriction = 'object.id IN (
                            SELECT p1.id
                            FROM Project\Entity\Member m1
                            JOIN m1.user u1
                            JOIN m1.project p1
                            WHERE ';
						$this->addServiceFiltersQuery('m1.user', $filter, $restriction, $queryBuilder);
					} elseif ($filter['op'] == 'isNull') {
						$queryBuilder->andWhere('
                            object.id NOT IN (SELECT DISTINCT pp1.id FROM Project\Entity\Member mm1 JOIN mm1.project pp1)
                        ');
					} elseif ($filter['op'] == 'isNotNull') {
						$queryBuilder->andWhere('
                            object.id IN (SELECT DISTINCT pp1.id FROM Project\Entity\Member mm1 JOIN mm1.project pp1)
                        ');
					} elseif ($filter['op'] === 'service') {
						$restriction = 'object.id IN (
                            SELECT p1.id
                            FROM Project\Entity\Member m1
                            JOIN m1.user u1
                            JOIN m1.project p1
                            WHERE ';
						$this->addServiceFiltersQuery('m1.user', $filter, $restriction, $queryBuilder);
					} else {
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
						$queryBuilder->andWhere(
							'
                            object.id ' .
								$operator .
								' (SELECT p1.id FROM Project\Entity\Member m1 JOIN m1.user u1 JOIN m1.project p1 WHERE u1.id in( :members))
                        '
						);
						$queryBuilder->andWhere('
                            object.id IN (SELECT DISTINCT pp1.id FROM Project\Entity\Member mm1 JOIN mm1.project pp1)
                        ');
						if (is_array($filter['val'])) {
							$queryBuilder->setParameter('members', $filter['val'], \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
						} else {
							$queryBuilder->setParameter('members', $filter['val']);
						}
					}
					unset($data['filters'][$property]);
					continue;
				}							

				if ($property == 'managers' && isset($filter['op'])) {
					$filterAdded = true;

					if ($filter['op'] === 'group') {
						$restriction = 'object.id IN (
                            SELECT p2.id
                            FROM Project\Entity\Member m2
                            JOIN m2.user u2
                            JOIN m2.project p2
                            WHERE m2.role = :manager_role
                            AND ';
						$filterAdded = $this->addKeywordFiltersQuery(
							'm2.user',
							$filter,
							$restriction,
							$queryBuilder
						);
					} elseif ($filter['op'] == 'isNull') {
						$queryBuilder->andWhere('
                            object.id NOT IN (SELECT DISTINCT pp2.id FROM Project\Entity\Member mm2 JOIN mm2.project pp2 WHERE mm2.role = :manager_role)
                        ');
					} elseif ($filter['op'] == 'isNotNull') {
						$queryBuilder->andWhere('
                            object.id IN (SELECT DISTINCT pp2.id FROM Project\Entity\Member mm2 JOIN mm2.project pp2 WHERE mm2.role = :manager_role)
                        ');
					} else {
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
						$queryBuilder->andWhere(
							'
                            object.id ' .
								$operator .
								' (SELECT p2.id FROM Project\Entity\Member m2 JOIN m2.user u2 JOIN m2.project p2 WHERE u2.id in (:managers) AND m2.role = :manager_role)
                        '
						);
						$queryBuilder->andWhere('
                            object.id IN (SELECT DISTINCT pp2.id FROM Project\Entity\Member mm2 JOIN mm2.project pp2 WHERE mm2.role in( :manager_role))
                        ');
						if (is_array($filter['val'])) {
							$queryBuilder->setParameter('managers', $filter['val'], \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
						} else {
							$queryBuilder->setParameter('managers', $filter['val']);
						}
					}

					if ($filterAdded) {
						$queryBuilder->setParameter('manager_role', Member::ROLE_MANAGER);
					}

					unset($data['filters'][$property]);
					continue;
				}
				if ($property == 'validators' && isset($filter['op'])) {
					$filterAdded = true;

					if ($filter['op'] === 'group') {
						$restriction = 'object.id IN (
                            SELECT p3.id
                            FROM Project\Entity\Member m3
                            JOIN m3.user u3
                            JOIN m3.project p3
                            WHERE m3.role = :validator_role
                            AND ';
						$filterAdded = $this->addKeywordFiltersQuery(
							'm3.user',
							$filter,
							$restriction,
							$queryBuilder
						);
					} elseif ($filter['op'] == 'isNull') {
						$queryBuilder->andWhere('
                            object.id NOT IN (SELECT DISTINCT pp3.id FROM Project\Entity\Member mm3 JOIN mm3.project pp3 WHERE mm3.role = :validator_role)
                        ');
					} elseif ($filter['op'] == 'isNotNull') {
						$queryBuilder->andWhere('
                            object.id IN (SELECT DISTINCT pp3.id FROM Project\Entity\Member mm3 JOIN mm3.project pp3 WHERE mm3.role = :validator_role)
                        ');
					} else {
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
						$queryBuilder->andWhere(
							'
                            object.id ' .
								$operator .
								' (SELECT p3.id FROM Project\Entity\Member m3 JOIN m3.user u3 JOIN m3.project p3 WHERE u3.id in( :validators) AND m3.role = :validator_role)
                        '
						);
						$queryBuilder->andWhere('
                            object.id IN (SELECT DISTINCT pp3.id FROM Project\Entity\Member mm3 JOIN mm3.project pp3 WHERE mm3.role = :validator_role)
                        ');
						if (is_array($filter['val'])) {
							$queryBuilder->setParameter('validators', $filter['val'], \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
						} else {
							$queryBuilder->setParameter('validators', $filter['val']);
						}
					}

					if ($filterAdded) {
						$queryBuilder->setParameter('validator_role', Member::ROLE_VALIDATOR);
					}

					unset($data['filters'][$property]);
					continue;
				}

				if ($property == 'actors' && isset($filter['op'])) {
					if ($filter['op'] == 'isNull') {
						$queryBuilder->andWhere('
                            object.id NOT IN (SELECT pp4.id FROM Project\Entity\Actor aa4 JOIN aa4.project pp4)
                        ');
					} elseif ($filter['op'] == 'isNotNull') {
						$queryBuilder->andWhere('
                            object.id IN (SELECT pp4.id FROM Project\Entity\Actor aa4 JOIN aa4.project pp4)
                        ');
					} else {
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
						$queryBuilder->andWhere(
							'
                            object.id ' .
								$operator .
								' (SELECT p4.id FROM Project\Entity\Actor a4 JOIN a4.structure s4 JOIN a4.project p4 WHERE s4.id in (:actors))
                        '
						);
						$queryBuilder->andWhere('
                            object.id IN (SELECT pp4.id FROM Project\Entity\Actor aa4 JOIN aa4.project pp4)
                        ');
						if (is_array($filter['val'])) {
							$queryBuilder->setParameter('actors', $filter['val'], 'simple_array');
						} else {
							$queryBuilder->setParameter('actors', $filter['val']);
						}
					}
					unset($data['filters'][$property]);
					continue;
				}

				if ($property == 'ageSynchronisation' && isset($filter['op'])) {
					if ($filter['op'] == 'isNull') {
						$queryBuilder->andWhere('
                            object.id NOT IN (SELECT pp4.id FROM Project\Entity\AgeSynchronisation ass4 JOIN ass4.project pp4)
                        ');
					} elseif ($filter['op'] == 'isNotNull') {
						$queryBuilder->andWhere('
                            object.id IN (SELECT pp4.id FROM Project\Entity\AgeSynchronisation ass4 JOIN ass4.project pp4)
                        ');
					} else {
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
						$queryBuilder->andWhere('
						object.id ' .
							$operator .
							' (SELECT pp4.id FROM Project\Entity\AgeSynchronisation ass4 JOIN ass4.project pp4 WHERE ass4.status = :ageSynchronisation)
					');
						if (is_array($filter['val'])) {
							$queryBuilder->setParameter('ageSynchronisation', $filter['val'], 'simple_array');
						} else {
							$queryBuilder->setParameter('ageSynchronisation', $filter['val']);
						}
					}
					unset($data['filters'][$property]);
					continue;
				}

				if ($property == 'virtual.caduciteStart' || $property == 'virtual.caduciteEnd') {
					$data['filters'][$property]['having'] = true;
				}

				if ($property == 'virtual.financer') {
					if ($filter['op'] == 'isNull') {
						$queryBuilder->andWhere('
                                 object.id NOT IN (SELECT pp5.id FROM Budget\Entity\PosteIncome ppi5 JOIN ppi5.project pp5 JOIN ppi5.envelope ee5)
                            ');
					} elseif ($filter['op'] == 'isNotNull') {
						$queryBuilder->andWhere('
                                 object.id IN (SELECT pp5.id FROM Budget\Entity\PosteIncome ppi5 JOIN ppi5.project pp5 JOIN ppi5.envelope ee5)
                            ');
					} else {
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
						$queryBuilder->andWhere(
							'
                                object.id ' .
								$operator .
								' (SELECT p5.id FROM Budget\Entity\PosteIncome pi5 JOIN pi5.project p5 JOIN pi5.envelope e5 JOIN e5.financer f5 WHERE f5 IN (:financers))
                            '
						);
						$queryBuilder->andWhere('
                                object.id IN (SELECT pp5.id FROM Budget\Entity\PosteIncome ppi5 JOIN ppi5.project pp5 JOIN ppi5.envelope ee5)
                            ');
						if (is_array($filter['val'])) {
							$queryBuilder->setParameter('financers', $filter['val'], 'simple_array');
						} else {
							$queryBuilder->setParameter('financers', $filter['val']);
						}
					}
					unset($data['filters'][$property]);
					continue;
				}

				if ($property == 'budgetCodes' && isset($filter['op'])) {
					if ($filter['op'] == 'isNull') {
						$queryBuilder->andWhere('
                object.id NOT IN (SELECT pp6.id FROM Budget\Entity\BudgetCode aa6 JOIN aa6.project pp6)
            ');
					} elseif ($filter['op'] == 'isNotNull') {
						$queryBuilder->andWhere('
                object.id IN (SELECT pp6.id FROM Budget\Entity\BudgetCode aa6 JOIN aa6.project pp6)
            ');
					} else {
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
						$queryBuilder->andWhere(
							'
                 object.id ' .
								$operator .
								' (SELECT p6.id FROM Budget\Entity\BudgetCode a6 JOIN a6.project p6 WHERE a6.id = :budgetCodes)
                        '
						);
						$queryBuilder->andWhere('
                object.id IN (SELECT pp6.id FROM Budget\Entity\BudgetCode aa6 JOIN aa6.project pp6)
            ');
						if (is_array($filter['val'])) {
							$queryBuilder->setParameter('budgetCodes', $filter['val'], 'simple_array');
						} else {
							$queryBuilder->setParameter('budgetCodes', $filter['val']);
						}
					}
					unset($data['filters'][$property]);
					continue;
				}

				//if ($this->serviceLocator->get('MyModuleManager')->isActive('advancement')) {
				if ($property == 'virtual.advancement') {
					$data['filters'][$property]['having'] = true;
				}
				//}

				if (preg_match('/^posteExpenses./', $property)) {
					$queryBuilder->leftJoin('object.posteExpenses', 'posteExpenses');
					continue;
				}

				if ($property === '_client') {
					$data['filterClient'] = $data['filters'][$property];
					unset($data['filters'][$property]);
					continue;
				}

				if (strpos($property, 'member-function.') === 0) {
					$keywordId = (int) str_replace('member-function.', '', $property);
					$qb = $this->getEntityManager()
						->getRepository('Project\Entity\Member')
						->createQueryBuilder('m')
						->select('DISTINCT p.id')
						->leftjoin('m.project', 'p')
						->join('Keyword\Entity\Association', 'a', 'WITH', 'm.id = a.primary')
						->join('Keyword\Entity\Keyword', 'k', 'WITH', 'k.id = a.keyword')
						->where('k.id = :keywordId')
						->setParameter('keywordId', $keywordId);

					if ($filter['op'] == 'isNull' || $filter['op'] == 'isNotNull') {
						$operator = $filter['op'] == 'isNull' ? 'NOT IN' : 'IN';
					} else {
						$qb->andWhere('m.user IN(:userIds)')
							->setParameter('userIds', $filter['val']);
						$operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
					}

					$projectIds = $qb->getQuery()->getArrayResult();
					$vals = array_map(function ($row) {
						return $row['id'];
					}, $projectIds);
					$list = join(', ', $vals);
					if ('' === $list) {
						$list = '0'; // cannot be empty, query now has to return 0 result
					}
					$queryBuilder->andWhere('object.id ' . $operator . ' (' . $list . ')');
					unset($data['filters'][$property]);
					continue;
				}
			}
		}
	}

	public function get($id, $callbacks = [])
	{
		$timesheetFilters = $this->params()->fromQuery('timesheetFilters', null);

		if ($timesheetFilters) {
			$callbacks[] = function (Project $object) use ($timesheetFilters) {
				$this->applyTimesheetFiltersRecursively($object, $timesheetFilters);
			};
		}

		return parent::get($id, $callbacks);
	}

	protected function addServiceFiltersQuery($property, $filter, $restriction, $queryBuilder)
	{
		$serviceFilters = $this->serviceFilters(
			$property,
			$filter['val']['op'],
			$filter['val']['val'],
			null,
			'User\Entity\User'
		);

		if (
			!$serviceFilters ||
			!isset($serviceFilters['restrictions']) ||
			!isset($serviceFilters['parameters']) ||
			count($serviceFilters['restrictions']) === 0 ||
			count($serviceFilters['parameters']) === 0
		) {
			return false;
		}

		$restrictionFinal = $restriction;

		foreach ($serviceFilters['restrictions'] as $key => $restriction) {
			if ($key !== 0) {
				$restrictionFinal .= ' AND ';
			}

			$restrictionFinal .= $restriction;
		}

		$restrictionFinal .= ')';

		$queryBuilder->andWhere($restrictionFinal);

		foreach ($serviceFilters['parameters'] as $key => $value) {
			$queryBuilder->setParameter($key, $value);
		}

		return true;
	}

	/**
	 * Add query for keyword filters.
	 *
	 * @param string       $property
	 * @param array        $filter
	 * @param string       $restriction
	 * @param QueryBuilder $queryBuilder
	 *
	 * @return bool
	 */
	protected function addKeywordFiltersQuery($property, $filter, $restriction, $queryBuilder)
	{
		$keywordFilters = $this->keywordFilters(
			$property,
			$filter['val']['op'],
			$filter['val']['group'],
			$filter['val']['tree'] === 'true',
			$filter['val']['val'],
			null,
			'User\Entity\User'
		);

		if (
			!$keywordFilters ||
			!isset($keywordFilters['restrictions']) ||
			!isset($keywordFilters['parameters']) ||
			count($keywordFilters['restrictions']) === 0 ||
			count($keywordFilters['parameters']) === 0
		) {
			return false;
		}

		$restrictionFinal = $restriction;

		foreach ($keywordFilters['restrictions'] as $key => $restriction) {
			if ($key !== 0) {
				$restrictionFinal .= ' AND ';
			}

			$restrictionFinal .= $restriction;
		}

		$restrictionFinal .= ')';

		$queryBuilder->andWhere($restrictionFinal);

		foreach ($keywordFilters['parameters'] as $key => $value) {
			$queryBuilder->setParameter($key, $value);
		}

		return true;
	}

	protected function applyTimesheetFiltersRecursively(Project $project, $timesheetFilters)
	{
		$timesheetFilters['project'] = [
			'op' => 'eq',
			'val' => $project->getId(),
		];

		$timesheets = $this->apiRequest('Time\Controller\API\Timesheet', Request::METHOD_GET, [
			'col' => ['id'],
			'search' => [
				'type' => 'list',
				'data' => [
					'sort' => 'id',
					'order' => 'asc',
					'filters' => $timesheetFilters,
				],
			],
		])
			->dispatch()
			->getVariable('rows');

		$timesheetsArr = new ArrayCollection();

		foreach ($timesheets as $timesheet) {
			$timesheetsArr->add(
				$this->entityManager->getRepository('Time\Entity\Timesheet')->find($timesheet['id'])
			);
		}

		$project->setTimesheets($timesheetsArr, true);

		foreach ($project->getChildren() as $child) {
			$this->applyTimesheetFiltersRecursively($child, $timesheetFilters);
		}
	}

	public function getList($callbacks = [], $master = false)
	{
		$callbacks[] = function (&$objects, $data) {
			if (isset($data['filterClient'])) {
				if (isset($data['filterClient']['op']) && isset($data['filterClient']['val'])) {
					$inArray = in_array($this->getClient()->getId(), $data['filterClient']['val']);
					if ($data['filterClient']['op'] === 'eq' && !$inArray) {
						$objects = [];
					} elseif ($data['filterClient']['op'] === 'neq' && $inArray) {
						$objects = [];
					}
				}
			}
		};

		$search = $this->params()->fromQuery('search', null);
		$timesheetPreFilters = null;
		$posteExpensePreFilters = null;
		$posteIncomePreFilters = null;
		$expensePreFilters = null;
		$incomePreFilters = null;

		if ($search && isset($search['data']) && isset($search['data']['filters'])) {
			$filters = $search['data']['filters'];

			foreach ($filters as $property => $filter) {
				if (preg_match_all('/(.+)-.+/', $property, $matches)) {
					$entity = $matches[1][0];

					$subFilters = ['timesheet', 'posteExpense', 'posteIncome', 'expense', 'income'];

					foreach ($subFilters as $subFilter) {
						if ($entity === $subFilter) {
							$realProperty = str_replace($entity . '-', '', $property);

							if (!${$subFilter . 'PreFilters'}) {
								${$subFilter . 'PreFilters'} = [];
							}

							${$subFilter . 'PreFilters'}[$realProperty] = $filter;
						}
					}

					unset($filters[$property]);
				}
			}

			if (
				$timesheetPreFilters ||
				$posteExpensePreFilters ||
				$posteIncomePreFilters ||
				$expensePreFilters ||
				$incomePreFilters
			) {
				$filters['id'] = [
					'op' => 'eq',
					'val' => [],
				];

				if ($timesheetPreFilters) {
					$timesheets = $this->apiRequest('Time\Controller\API\Timesheet', Request::METHOD_GET, [
						'col' => ['id', 'project.id'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $timesheetPreFilters,
							],
						],
					])
						->dispatch()
						->getVariable('rows');

					foreach ($timesheets as $timesheet) {
						$filters['id']['val'][] = $timesheet['project']['id'];
					}
				}

				if ($posteExpensePreFilters) {
					$posteExpenses = $this->apiRequest(
						'Budget\Controller\API\PosteExpense',
						Request::METHOD_GET,
						[
							'col' => ['id', 'project.id'],
							'search' => [
								'type' => 'list',
								'data' => [
									'sort' => 'id',
									'order' => 'asc',
									'filters' => $posteExpensePreFilters,
								],
							],
						]
					)
						->dispatch()
						->getVariable('rows');

					foreach ($posteExpenses as $posteExpense) {
						$filters['id']['val'][] = $posteExpense['project']['id'];
					}
				}

				if ($posteIncomePreFilters) {
					$posteIncomes = $this->apiRequest(
						'Budget\Controller\API\PosteIncome',
						Request::METHOD_GET,
						[
							'col' => ['id', 'project.id'],
							'search' => [
								'type' => 'list',
								'data' => [
									'sort' => 'id',
									'order' => 'asc',
									'filters' => $posteIncomePreFilters,
								],
							],
						]
					)
						->dispatch()
						->getVariable('rows');

					foreach ($posteIncomes as $posteIncome) {
						$filters['id']['val'][] = $posteIncome['project']['id'];
					}
				}

				if ($expensePreFilters) {
					$expenses = $this->apiRequest('Budget\Controller\API\Expense', Request::METHOD_GET, [
						'col' => ['id', 'poste.id', 'poste.project.id'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $expensePreFilters,
							],
						],
					])
						->dispatch()
						->getVariable('rows');

					foreach ($expenses as $expense) {
						$filters['id']['val'][] = $expense['poste']['project']['id'];
					}
				}

				if ($incomePreFilters) {
					$incomes = $this->apiRequest('Budget\Controller\API\Income', Request::METHOD_GET, [
						'col' => ['id', 'poste.id', 'poste.project.id'],
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $incomePreFilters,
							],
						],
					])
						->dispatch()
						->getVariable('rows');

					foreach ($incomes as $income) {
						$filters['id']['val'][] = $income['poste']['project']['id'];
					}
				}

				$search['data']['filters'] = $filters;

				$this->request->getQuery()->set('search', $search);
			}
		}

		$col = $this->params()->fromQuery('col');
		$posteExpenseFilters = $this->params()->fromQuery('posteExpenseFilters', null);
		$posteIncomeFilters = $this->params()->fromQuery('posteIncomeFilters', null);
		$expenseFilters = $this->params()->fromQuery('expenseFilters', null);
		$incomeFilters = $this->params()->fromQuery('incomeFilters', null);

		if ($posteExpenseFilters) {
			$callbacks[] = function (&$objects, $data, $entityManager) use ($col, $posteExpenseFilters) {
				$objectsUpdated = new ArrayCollection();

				$arbo = false;
				foreach ($col as $column) {
					if (preg_match('/posteExpenses\./', $column)) {
						$arbo = false;
						break;
					} elseif (preg_match('/posteExpensesArbo\./', $column)) {
						$arbo = true;
						break;
					}
				}

				foreach ($objects as $object) {
					$posteExpenses = $object->{'getPosteExpenses' . ($arbo ? 'Arbo' : '')}();

					if ($posteExpenses) {
						$posteExpenseFilters['id'] = [
							'op' => 'eq',
							'val' => [],
						];
						foreach ($posteExpenses as $posteExpense) {
							$posteExpenseFilters['id']['val'][] = $posteExpense->getId();
						}

						$request = $this->apiRequest(
							'Budget\Controller\API\PosteExpense',
							Request::METHOD_GET,
							[
								'col' => ['id'],
								'search' => [
									'type' => 'list',
									'data' => [
										'sort' => 'id',
										'order' => 'asc',
										'filters' => $posteExpenseFilters,
									],
								],
							],
							$entityManager
						);
						$posteExpenses = $request->dispatch()->getVariable('rows');

						$posteExpensesObjects = new ArrayCollection();
						foreach ($posteExpenses as $posteExpense) {
							$posteExpensesObjects->add(
								$entityManager->getRepository(PosteExpense::class)->find($posteExpense['id'])
							);
						}

						if ($posteExpensesObjects->isEmpty()) {
							continue;
						}

						$object->{'setPosteExpenses' . ($arbo ? 'Arbo' : '')}($posteExpensesObjects);

						$objectsUpdated->add($object);
					}
				}

				$objects = $objectsUpdated;
			};
		}

		if ($posteIncomeFilters) {
			$callbacks[] = function (&$objects, $data, $entityManager) use ($col, $posteIncomeFilters) {
				$objectsUpdated = new ArrayCollection();

				$arbo = false;
				foreach ($col as $column) {
					if (preg_match('/posteIncomes\./', $column)) {
						$arbo = false;
						break;
					} elseif (preg_match('/posteIncomesArbo\./', $column)) {
						$arbo = true;
						break;
					}
				}

				foreach ($objects as $object) {
					$posteIncomes = $object->{'getPosteIncomes' . ($arbo ? 'Arbo' : '')}();

					if ($posteIncomes) {
						$posteIncomeFilters['id'] = [
							'op' => 'eq',
							'val' => [],
						];
						foreach ($posteIncomes as $posteIncome) {
							$posteIncomeFilters['id']['val'][] = $posteIncome->getId();
						}

						$request = $this->apiRequest(
							'Budget\Controller\API\PosteIncome',
							Request::METHOD_GET,
							[
								'col' => ['id'],
								'search' => [
									'type' => 'list',
									'data' => [
										'sort' => 'id',
										'order' => 'asc',
										'filters' => $posteIncomeFilters,
									],
								],
							],
							$entityManager
						);
						$posteIncomes = $request->dispatch()->getVariable('rows');

						$posteIncomesObjects = new ArrayCollection();
						foreach ($posteIncomes as $posteIncome) {
							$posteIncomesObjects->add(
								$entityManager->getRepository(PosteIncome::class)->find($posteIncome['id'])
							);
						}

						if ($posteIncomesObjects->isEmpty()) {
							continue;
						}

						$object->{'setPosteIncomes' . ($arbo ? 'Arbo' : '')}($posteIncomesObjects);

						$objectsUpdated->add($object);
					}
				}

				$objects = $objectsUpdated;
			};
		}

		if ($expenseFilters) {
			$callbacks[] = function (&$objects, $data, $entityManager) use ($col, $expenseFilters) {
				$objectsUpdated = new ArrayCollection();

				$arbo = false;
				foreach ($col as $column) {
					if (preg_match('/expenses\./', $column)) {
						$arbo = false;
						break;
					} elseif (preg_match('/expensesArbo\./', $column)) {
						$arbo = true;
						break;
					}
				}

				foreach ($objects as $object) {
					$posteExpensesUpdated = new ArrayCollection();

					/** @var ArrayCollection $posteExpenses */
					$posteExpenses = $object->{'getPosteExpenses' . ($arbo ? 'Arbo' : '')}();

					foreach ($posteExpenses as $posteExpense) {
						$expenses = $posteExpense->{'getExpenses' . ($arbo ? 'Arbo' : '')}();

						if ($expenses) {
							$expenseFilters['id'] = [
								'op' => 'eq',
								'val' => [],
							];
							foreach ($expenses as $expense) {
								$expenseFilters['id']['val'][] = $expense->getId();
							}

							$request = $this->apiRequest(
								'Budget\Controller\API\Expense',
								Request::METHOD_GET,
								[
									'col' => ['id'],
									'search' => [
										'type' => 'list',
										'data' => [
											'sort' => 'id',
											'order' => 'asc',
											'filters' => $expenseFilters,
										],
									],
								],
								$entityManager
							);
							$expenses = $request->dispatch()->getVariable('rows');

							$expensesObjects = new ArrayCollection();
							foreach ($expenses as $expense) {
								$expensesObjects->add(
									$entityManager->getRepository(Expense::class)->find($expense['id'])
								);
							}

							if ($expensesObjects->isEmpty()) {
								continue;
							}

							$posteExpense->{'setExpenses' . ($arbo ? 'Arbo' : '')}($expensesObjects);
							$posteExpensesUpdated->add($posteExpense);
						}
					}

					if ($posteExpensesUpdated->isEmpty()) {
						continue;
					}

					$object->{'setPosteExpenses' . ($arbo ? 'Arbo' : '')}($posteExpensesUpdated);

					$objectsUpdated->add($object);
				}

				$objects = $objectsUpdated;
			};
		}

		if ($incomeFilters) {
			$callbacks[] = function (&$objects, $data, $entityManager) use ($col, $incomeFilters) {
				$objectsUpdated = new ArrayCollection();

				$arbo = false;
				foreach ($col as $column) {
					if (preg_match('/incomes\./', $column)) {
						$arbo = false;
						break;
					} elseif (preg_match('/incomesArbo\./', $column)) {
						$arbo = true;
						break;
					}
				}

				foreach ($objects as $object) {
					$posteIncomesUpdated = new ArrayCollection();

					/** @var ArrayCollection $posteIncomes */
					$posteIncomes = $object->{'getPosteIncomes' . ($arbo ? 'Arbo' : '')}();

					foreach ($posteIncomes as $posteIncome) {
						$incomes = $posteIncome->{'getIncomes' . ($arbo ? 'Arbo' : '')}();

						if ($incomes) {
							$incomeFilters['id'] = [
								'op' => 'eq',
								'val' => [],
							];
							foreach ($incomes as $income) {
								$incomeFilters['id']['val'][] = $income->getId();
							}

							$request = $this->apiRequest(
								'Budget\Controller\API\Income',
								Request::METHOD_GET,
								[
									'col' => ['id'],
									'search' => [
										'type' => 'list',
										'data' => [
											'sort' => 'id',
											'order' => 'asc',
											'filters' => $incomeFilters,
										],
									],
								],
								$entityManager
							);
							$incomes = $request->dispatch()->getVariable('rows');

							$incomesObjects = new ArrayCollection();
							foreach ($incomes as $income) {
								$incomesObjects->add(
									$entityManager->getRepository(Income::class)->find($income['id'])
								);
							}

							if ($incomesObjects->isEmpty()) {
								continue;
							}

							$posteIncome->{'setIncomes' . ($arbo ? 'Arbo' : '')}($incomesObjects);
							$posteIncomesUpdated->add($posteIncome);
						}
					}

					if ($posteIncomesUpdated->isEmpty()) {
						continue;
					}

					$object->{'setPosteIncomes' . ($arbo ? 'Arbo' : '')}($posteIncomesUpdated);

					$objectsUpdated->add($object);
				}

				$objects = $objectsUpdated;
			};
		}

		return parent::getList($callbacks, $master);
	}

	private function saveMembersKeywords($membersData, $project, $object)
	{
		$keywordByUserId = [];
		foreach ($membersData as $memberData) {
			if (isset($memberData['keywords'])) {
				$keywordByUserId[$memberData['user']['id']] = $memberData['keywords'];
			}
		}

		$newMembers = $project->getMembers()->filter(function ($member) use ($keywordByUserId) {
			return array_key_exists($member->getUser()->getId(), $keywordByUserId);
		});

		foreach ($newMembers as $newMember) {
			$keywords = $keywordByUserId[$newMember->getUser()->getId()];
			$this->saveKeywords(
				$newMember,
				['keywords' => $keywords],
				'Project\Entity\Member'
			);
		}

		// bind keywords in members, for http response payload
		if (array_key_exists('members', $object) && isset($object['members'])) {
			foreach ($object['members'] as $i => $memberItem) {
				$keywords = $this->getKeywords($memberItem['id'], 'Project\Entity\Member');
				$object['members'][$i]['keywords'] = $keywords;
			}
		}

		return $object;
	}
}
