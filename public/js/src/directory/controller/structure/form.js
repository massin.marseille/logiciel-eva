(function(app) {

    app.controller('structureFormController', [
        'structureService', 'jobService', 'flashMessenger', 'translator', '$scope',
        function structureFormController(structureService, jobService, flashMessenger, translator, $scope) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'email',
                    'phone',
                    'phone2',
                    'zipcode',
                    'city',
                    'sigle',
                    'country',
                    'addressLine1',
                    'addressLine2',
                    'links.id',
                    'links.keywords',
                    'links.contact.id',
                    'links.contact.name',
                    'links.job.id',
                    'links.job.name',
                    'financer',
                    'keywords',
                    'siret',
                    'website'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {},
                job: []
            };

            self.structure = {
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if(id) {
                    structureService.find(id, apiParams).then(function(res) {
                        self.structure    = res.object;
                        self.panelTitle = self.structure.name;
                    })
                }
            };

            self.saveStructure = function saveStructure() {
                self.errors.fields  = {};
                self.loading.save   = true;

                var isCreation = !self.structure.id;

                structureService.save(self.structure, apiParams).then(function(res) {
                    self.structure      = res.object;
                    $scope.$broadcast("custom-fields-save", { entity: 'structure', id: self.structure.id });
                    self.panelTitle   = self.structure.name;
                    self.loading.save = false;
                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.structure.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('structure_message_saved'));

                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteStructure = function deleteStructure() {
                if (confirm(translator('structure_question_delete').replace('%1', self.structure.name))) {
                    structureService.remove(self.structure).then(function(res) {
                        if (res.success) {
                            window.location = '/directory/structure';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
