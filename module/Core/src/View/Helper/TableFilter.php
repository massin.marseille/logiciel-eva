<?php

namespace Core\View\Helper;

use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Helper\AbstractHelper;

/**
 * Class TableFilter
 *
 * @package Core\View\Helper
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class TableFilter extends AbstractHelper
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var Translate
     */
    protected $translatorViewHelper;

    public function __construct($translator)
    {
        $this->translatorViewHelper = $translator;
    }

    public function __invoke($ngModel, $filters, $treeView = false)
    {
        $filtersOptions = '';
        $filtersTemplates = '';

        $entitiesFilters = ['default' => []];

        foreach ($filters as $name => $data) {
            if (preg_match_all('/(.+)-.+/', $name, $matches)) {
                $entity = $matches[1][0];

                if (!isset($entitiesFilters[$entity])) {
                    $entitiesFilters[$entity] = [];
                }

                $property = str_replace($entity . '-', '', $name);
                $entitiesFilters[$entity][$property] = $data;
            } else {
                $entitiesFilters['default'][$name] = $data;
            }
        }

        foreach ($entitiesFilters as $i => $entityFilters) {
            uasort($entitiesFilters[$i], function ($a, $b) {
                return strcmp(strtolower($this->unaccent($a['label'])), strtolower($this->unaccent($b['label'])));
            });
        }

        foreach ($entitiesFilters as $entity => $entityFilters) {
            if ($entity !== 'default') {
                $filtersOptions .= '<optgroup label="' . ucfirst($this->translatorViewHelper->__invoke($entity . '_entity')) . '">';
            }

            foreach ($entityFilters as $name => $data) {
                $name = ($entity !== 'default' ? $entity . '-' : '') . $name;

                $inArrayFilter = '[\'' . $name . '\']';
                $ngModelFilter = $ngModel . $inArrayFilter;

                $label = $data['label'];
                $type = $data['type'];
                $ngInit = isset($data['init']) ? $data['init'] : '';
                if ($type == 'keyword-select' || $type == 'territory-select') {
                    $ngInit .= '(!' . $ngModelFilter . '.tree ? ' . $ngModelFilter . '.tree = true : null); (!' . $ngModelFilter . '.op ? ' . $ngModelFilter . '.op = \'eq\' : null)';
                } elseif ($type == 'string') {
                    $ngInit .= '(!' . $ngModelFilter . '.op ? ' . $ngModelFilter . '.op = \'cnt\' : null)';
                } elseif ($type == 'date' || $type == 'datetime') {
                    $ngInit .= '(!' . $ngModelFilter . '.op ? ' . $ngModelFilter . '.op = \'period\' : null)';
                } else {
                    $ngInit .= '(!' . $ngModelFilter . '.op ? ' . $ngModelFilter . '.op = \'eq\' : null)';
                }

                $filtersOptions .= '<option ng-show="!__filters' . $inArrayFilter . '" value="' . $name . '">' . $label . '</option>';

                $operator = $this->operatorField($ngModelFilter, $data, $treeView,'form-control');
                $value = $this->valueField($ngModelFilter, $data, $treeView, 'form-control');

                $filtersTemplates .= '
                    <li ng-' . ($ngInit ? 'if' : 'show') . '="__filters' . $inArrayFilter . '!==undefined" ng-init="' . $ngInit . '">
                        ' . ($entity !== 'default' ? ucfirst($this->translatorViewHelper->__invoke($entity . '_entity')) . ' - ' : '') . $label . '
                        <i class="fa fa-remove pull-right pointer" ng-click="__filters' . $inArrayFilter . ' = false;forceDelete(__filters, \'' . $name . '\'); forceDelete(' . $ngModel . ', \'' . $name . '\');"></i>
                        <div class="row">
                            <div class="col-xs-6">
                            ' . $operator . '
                            </div>';

                if ($type === 'date' || $type === 'datetime') {
                    $filtersTemplates .= '<div ng-class="{\'col-xs-12\': ' . $ngModelFilter . '.op == \'period\', \'col-xs-6\': ' . $ngModelFilter . '.op != \'period\'}">';
                } else if (in_array($type, ['user-select', 'project-select'])) {
                    $filtersTemplates .= '<div ng-class="{\'col-xs-12\': ' . $ngModelFilter . '.op == \'group\' , \'col-xs-6\': ' . $ngModelFilter . '.op != \'group\' }">';
                } else {
                    $filtersTemplates .= '<div class="col-xs-6">';
                }

                $filtersTemplates .= $value . '
                            </div>
                        </div>
                    </li>
                ';
            }

            if ($entity !== 'default') {
                $filtersOptions .= '</optgroup>';
            }
        }

        $html = '
            <select class="form-control" ng-model="__filter" ng-change="__filters[__filter] = true; __filter = null;">
                <option value="" class="hidden">Sélectionnez un filtre</option>
                ' . $filtersOptions . '
            </select>
            <ul>
                ' . $filtersTemplates . '
            </ul>
        ';
        return $html;
    }

    public function operatorField($model, $data, $treeView, $classes = '')
    {
        $type = $data['type'];
        $html = '';
        switch ($type) {
            case 'string':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="cnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="ncnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="scnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_scnt') . '</option>';
                $html .= '<option value="sncnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_sncnt') . '</option>';
                $html .= '<option value="ecnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_ecnt') . '</option>';
                $html .= '<option value="encnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_encnt') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'select':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="cnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="ncnt">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'field-consent':
            case 'number':
            case 'boolean':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_eq') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'territory-select':
                $html .= '<div class="input-group">';
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="neq">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                if (!$treeView) {
                    $html .= '<div class="input-group-addon addon-simple" tooltip="' . $this->translatorViewHelper->__invoke('table_filter_tooltip_tree') . '">';
                    $html .= '<input type="checkbox" ng-model="' . $model . '.tree" />';
                    $html .= '</div>';
                }

                $html .= '</div>';
                break;
            case 'keyword-select':
                $html .= '<div class="input-group">';
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="neq">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="neqOrIsNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncntOrIsNull') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                if (!$treeView) {
                    $html .= '<div class="input-group-addon addon-simple" tooltip="' . $this->translatorViewHelper->__invoke('table_filter_tooltip_tree') . '">';
                    $html .= '<input type="checkbox" ng-model="' . $model . '.tree" />';
                    $html .= '</div>';
                }

                $html .= '</div>';
                break;
            case 'role-select':
            case 'job-select':
            case 'structure-select':
            case 'financer-select':
            case 'contact-select':
            case 'indicator-select':
            case 'group-indicator-select':
            case 'campain-select':
            case 'account-select':
            case 'template-select':
            case 'envelope-select':
            case 'budget-status-select':
            case 'budget-code-select':
            case 'contact-email-select':
            case 'nature-select':
            case 'organization-select':
            case 'destination-select':
            case 'absence-type-select':
            case 'management-unit-select':
            case 'manytoone':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="neq">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'client-select':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="neq">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '</select>';
                break;
            case 'in-array':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="inArray">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="notInArray">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'project-select':
            case 'user-select':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op" ng-change="' . $model . '.op === \'group\' ? ' . $model . '.val = { tree: true } : null">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="neq">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="group">' . $this->translatorViewHelper->__invoke('table_filter_operator_group') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'simple-user-select':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="neq">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'datetime':
            case 'date':
                $onlyPeriod = isset($data['onlyPeriod']) ? $data['onlyPeriod'] : false;

                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="period">' . $this->translatorViewHelper->__invoke('table_filter_operator_period') . '</option>';
                $html .= $onlyPeriod ? '' : '<option value="beforeStrict">' . $this->translatorViewHelper->__invoke('table_filter_operator_before_strict') . '</option>';
                $html .= $onlyPeriod ? '' : '<option value="before">' . $this->translatorViewHelper->__invoke('table_filter_operator_before') . '</option>';
                $html .= $onlyPeriod ? '' : '<option value="lessThan">' . $this->translatorViewHelper->__invoke('table_filter_operator_less_than') . '</option>';
                $html .= $onlyPeriod ? '' : '<option value="afterStrict">' . $this->translatorViewHelper->__invoke('table_filter_operator_after_strict') . '</option>';
                $html .= $onlyPeriod ? '' : '<option value="after">' . $this->translatorViewHelper->__invoke('table_filter_operator_after') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                break;
            case 'event-special':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="invited">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_invited') . '</option>';
                $html .= '<option value="notInvited">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_not_invited') . '</option>';
                $html .= '<option value="present">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_present') . '</option>';
                $html .= '<option value="notPresent">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_not_present') . '</option>';
                $html .= '</select>';
                break;
            case 'mail-status':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.op">';
                $html .= '<option value="sent">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_sent') . '</option>';
                $html .= '<option value="read">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_read') . '</option>';
                $html .= '<option value="nsent">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_not_sent') . '</option>';
                $html .= '<option value="nread">' . $this->translatorViewHelper->__invoke('invitation_filter_operator_not_read') . '</option>';
                $html .= '</select>';
                break;
        }

        return $html;
    }

    public function valueField($model, $data, $treeView,  $classes = '')
    {
        $type = $data['type'];

        $html = '';

        switch ($type) {
            case 'string':
            case 'select':
                $html .= '<input ng-if="' . $model . '.op !== \'isNull\' && ' . $model . '.op !== \'isNotNull\'" type="text" class="' . $classes . '" ng-model="' . $model . '.val" />';
                break;

            case 'user-select':
            case 'simple-user-select':
                $html .= '<div ng-if="' . $model . '.op !== \'group\'">';
                $html .= '<user-select ng-model="' . $model . '.val" only-value="true" multiple="true" current-user="true"></user-select>';
                $html .= '</div>';
                break;

            case 'role-select':
                $html .= '<role-select ng-model="' . $model . '.val" only-value="true" multiple="true"></role-select>';
                break;

            case 'keyword-select':
                $html .= '<keyword-select ng-model="' . $model . '.val" only-value="true" multiple="true" configuration="{ service : { apiParams : { search: { data: { filters: { group : { op: \'eq\', val: ' . $data['group'] . ' } } } } } } }"></keyword-select>';
                break;

            case 'project-select':
                $html .= '<div ng-if="' . $model . '.op !== \'group\'">';
                $html .= '<project-select ng-model="' . $model . '.val" only-value="true" multiple="true" sort=\'["code", "name"]\'></project-select>';
                $html .= '</div>';
                break;

            case 'nature-select':
                $html .= '<div ng-if="' . $model . '.op !== \'group\'">';
                $html .= '<nature-select ng-model="' . $model . '.val" only-value="true" multiple="true"></nature-select>';
                $html .= '</div>';
                break;

            case 'management-unit-select':
                $html .= '<div ng-if="' . $model . '.op !== \'group\'">';
                $html .= '<management-unit-select ng-model="' . $model . '.val" only-value="true" multiple="true"></management-unit-select>';
                $html .= '</div>';
                break;

            case 'organization-select':
                $html .= '<div ng-if="' . $model . '.op !== \'group\'">';
                $html .= '<organization-select ng-model="' . $model . '.val" only-value="true" multiple="true"></organization-select>';
                $html .= '</div>';
                break;

            case 'destination-select':
                $html .= '<div ng-if="' . $model . '.op !== \'group\'">';
                $html .= '<destination-select ng-model="' . $model . '.val" only-value="true" multiple="true"></destination-select>';
                $html .= '</div>';
                break;

            case 'contact-select':
                $html .= '<contact-select ng-model="' . $model . '.val" only-value="true" multiple="true"></contact-select>';
                break;

            case 'structure-select':
                $html .= '<structure-select ng-model="' . $model . '.val" only-value="true" multiple="true"></structure-select>';
                break;

            case 'financer-select':
                $html .= '<structure-select ng-model="' . $model . '.val" only-value="true" multiple="true" configuration="{ service : { apiParams : { search: { data: { filters: { financer : { op: \'eq\', val: \'1\' } } } } } } }"></structure-select>';
                break;

            case 'job-select':
                $html .= '<job-select ng-model="' . $model . '.val" only-value="true" multiple="true"></job-select>';
                break;

            case 'indicator-select':
                $html .= '<indicator-select ng-model="' . $model . '.val" only-value="true" multiple="true"></indicator-select>';
                break;

            case 'group-indicator-select':
                $html .= '<group-indicator-select ng-model="' . $model . '.val" only-value="true" multiple="true"></group-indicator-select>';
                break;
            
            case 'campain-select':
                $html .= '<campain-select ng-model="' . $model . '.val" only-value="true" multiple="true"></campain-select>';
                break;
    
            case 'client-select':
                $html .= '<client-select ng-model="' . $model . '.val" only-value="true" multiple="true"></client-select>';
                break;

            case 'account-select':
                $html .= '<account-select ng-model="' . $model . '.val" only-value="true" multiple="true" configuration="{ service : { apiParams : { search: { data: { filters: { type : { op: \'eq\', val: \'' . $data['accountType'] . '\' } } } } } } }"></account-select>';
                break;

            case 'envelope-select':
                $html .= '<envelope-select ng-model="' . $model . '.val" only-value="true" multiple="true"></envelope-select>';
                break;

            case 'territory-select':
                $html .= '<territory-select ng-model="' . $model . '.val" only-value="true" multiple="true"></territory-select>';
                break;

            case 'budget-status-select':
                $html .= '<budget-status-select ng-model="' . $model . '.val" only-value="true" multiple="true"></budget-status-select>';
                break;

            case 'budget-code-select':
                $html .= '<budget-code-select ng-model="' . $model . '.val" only-value="true" multiple="true"></budget-code-select>';
                break;

            case 'template-select':
                $html .= '<template-select ng-model="' . $model . '.val" only-value="true" multiple="true"></template-select>';
                break;

            case 'mail-status':
                $html .= '<mail-select ng-model="' . $model . '.val" only-value="true" configuration="{ service : { apiParams : { search: { data: { filters: { event : { op: \'eq\', val: $parent.$parent.self.event.id } } } } } } }"></mail-select>';
                break;
            case 'contact-email-select':
                $html .= '<contact-email-select ng-model="' . $model . '.val" only-value="true" multiple="true"></contact- email-select>';
                break;

            case 'absence-type-select':
                $html .= '<absence-type-select ng-model="' . $model . '.val" only-value="true" multiple="true"></absence-type-select>';
                break;
            case 'event-special':
                $html .= '<event-select ng-model="' . $model . '.val"
                                        only-value="true"
                                        multiple="true">
                          </event-select>';
                break;

            case 'manytoone':
            case 'in-array':
                $options = isset($data['options']) ? $data['options'] : '';
                $isArray = is_array($options);

                $html .= '<select ng-if="' . $model . '.op !== \'isNull\' && ' . $model . '.op !== \'isNotNull\'" class="' . $classes . '" multiple ng-model="' . $model . '.val" ' . ($isArray ? '' : 'ng-options="option.value as option.label for option in ' . $options . '"') . '>';

                if ($isArray) {
                    foreach ($options as $option) {
                        $html .= '<option value="' . $option['value'] . '">' . $option['text'] . '</option>';
                    }
                }
                $html .= '</select>';
                break;

            case 'datetime':
            case 'date':
                $html .= '<div ng-if="' . $model . '.op == \'period\'" class="row period">';
                $html .= '<div class="col-xs-6">';
                $html .= '<input type="text" class="' . $classes . '" input-date input-date-mask="' . $type . '" ng-model="' . $model . '.val.start"/>';
                $html .= '</div>';
                $html .= '<div class="col-xs-6">';
                $html .= '<input type="text" class="' . $classes . '" input-date input-date-mask="' . $type . '" ng-model="' . $model . '.val.end"/>';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '<div ng-if="' . $model . '.op != \'period\' && ' . $model . '.op != \'isNull\' && ' . $model . '.op != \'isNotNull\'">';
                $html .= '<div class="input-group">';
                $html .= '<input type="text" class="' . $classes . '" input-mask="decimal" ng-model="' . $model . '.val.days"/>';
                $html .= '<div class="input-group-addon">';
                $html .= 'jours';
                $html .= '</div>';
                $html .= '</div>';
                $html .= '</div>';
                break;

            case 'number':
                $html .= '<div ng-if="' . $model . '.op == \'eq\'" class="row">';
                $html .= '<div class="col-xs-6">';
                $html .= '<input type="text" class="' . $classes . '" input-mask="decimal" ng-model="' . $model . '.val.min" placeholder="Valeur minimum" />';
                $html .= '</div>';
                $html .= '<div class="col-xs-6">';
                $html .= '<input type="text" class="' . $classes . '" input-mask="decimal" ng-model="' . $model . '.val.max" placeholder="Valeur maximum" />';
                $html .= '</div>';
                $html .= '</div>';
                break;

            case 'boolean':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.val">';
                $html .= '<option value="1">Oui</option>';
                $html .= '<option value="0">Non</option>';
                $html .= '</select>';
                break;

            case 'field-consent':
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.val">';
                $html .= '<option value="accepted">' . $this->translatorViewHelper->__invoke('contact_subscription_status_accepted') . '</option>';
                $html .= '<option value="declined">' . $this->translatorViewHelper->__invoke('contact_subscription_status_declined') . '</option>';
                $html .= '<option value="empty">' . $this->translatorViewHelper->__invoke('contact_subscription_status_empty') . '</option>';
                $html .= '</select>';
                break;
        }

        // HTML to add filters on keyword for any entity.
        switch ($type) {
            case 'user-select':
            case 'project-select':
                $html .= '<div ng-if="' . $model . '.op === \'group\'" class="row">';
                $html .= '<div class="col-xs-6 col-xs-offset-6" style="margin-top: -32px; padding-right: 15px;">';
                $html .= '<group-select ng-model="' . $model . '.val.group" only-value="true" multiple="false" configuration="{ service : { apiParams : { search: { data: { filters: { entities : { op: \'sa_cnt\', val: \'' . str_replace('-select', '', $type) . '\' } } } } } } }"></group-select>';
                $html .= '</div>';
                $html .= '</div>';

                $html .= '<div ng-if="' . $model . '.op === \'group\'" class="row period">';
                $html .= '<div class="col-xs-6">';
                $html .= '<div ng-class="{\'input-group\': ' . $model . '.op === \'group\'}">';
                $html .= '<select class="' . $classes . '" ng-model="' . $model . '.val.op">';
                $html .= '<option value="eq">' . $this->translatorViewHelper->__invoke('table_filter_operator_cnt') . '</option>';
                $html .= '<option value="neq">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncnt') . '</option>';
                $html .= '<option value="neqOrIsNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_ncntOrIsNull') . '</option>';
                $html .= '<option value="isNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNull') . '</option>';
                $html .= '<option value="isNotNull">' . $this->translatorViewHelper->__invoke('table_filter_operator_isNotNull') . '</option>';
                $html .= '</select>';
                if (!$treeView) {
                    $html .= '<div ng-if="' . $model . '.op === \'group\'" class="input-group-addon addon-simple" tooltip="' . $this->translatorViewHelper->__invoke('table_filter_tooltip_tree') . '">';
                    $html .= '<input type="checkbox" ng-model="' . $model . '.val.tree" />';
                    $html .= '</div>';
                }

                $html .= '</div>';
                $html .= '</div>';
                $html .= '<div class="col-xs-6" ng-if="' . $model . '.op === \'group\'">';
                $html .= '<keyword-select ng-model="' . $model . '.val.val" only-value="true" multiple="true" configuration="{ service : { apiParams : { search: { data: { filters: { group : { op: \'eq\', val: ' . $model . '.val.group } } } } } } }"></keyword-select>';
                $html .= '</div>';
                $html .= '</div>';
        }

        return $html;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    private function unaccent($string)
    {
        return preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($string, ENT_COMPAT, 'UTF-8'));
    }
}
