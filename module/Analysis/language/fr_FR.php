<?php

return [
	'month'     => 'mois',
	'hours'     => 'heures',
	'year'      => 'année',
	'total'     => 'total',
	'all_years' => 'toutes les années',

	'analysis_module_title'        => 'Analyses',
	'analysis_export_module_title' => 'Export des [[ analysis_module_title ]]',

	'analysis_budget_module_title'     => '[[ budget_module_title ]]',
	'analysis_time_module_title'       => '[[ time_module_title ]]',
	'analysis_indicator_module_title'  => '[[ indicator_module_title ]]',
	'analysis_convention_module_title' => '[[ convention_module_title ]]',
	'analysis_project_module_title'    => '[[ project_module_title ]]',
	'analysis_map_module_title'        => '[[ map_module_title ]]',

	'analysis_budget_gbcp'            => 'GBCP',
	'analysis_budget_financers'       => 'Répartition par [[ envelope_field_financer ]]s',
	'analysis_budget_keywords'        => 'Répartition par [[ keyword_entities ]]',
	'analysis_budget_year'            => 'Répartition par [[ year ]]',
	'analysis_budget_keywords_data'   => 'Donnée financière',
	'analysis_budget_month_cumulated' => 'Mois cumulé ?',
	'analysis_budget_expense_arbo'    => '[[ expense_entities | ucfirst ]] arborescentes ?',
	'analysis_budget_income_arbo'     => '[[ income_entities | ucfirst ]] arborescentes ?',

	'analysis_time_project_repartition'          => 'Répartition par [[ project_entities ]]',
	'analysis_time_user_repartition'             => 'Répartition par [[ user_entities ]]',
	'analysis_time_month_repartition'            => 'Répartition par mois',
	'analysis_time_project_keywords_repartition' => 'Répartition par [[ keyword_entities ]] [[ project_entities ]]',
	'analysis_time_keywords_repartition'         => 'Répartition par [[ keyword_entities ]]',
	'analysis_time_territories_repartition'      => 'Répartition par [[ territory_entities ]]',
	'analysis_time_project_aggregate'            => 'Aggréger les [[ project_entities ]] ?',
	'analysis_time_project_divideTimes'          => 'Diviser les temps des [[ timesheet_entities ]] suivant le nombre de [[ keyword_entities ]] auquel est rattaché la [[ project_entity ]] ?',

	'analysis_indicator_recap'             => 'Récapitulatif',
	'analysis_indicator_recapWithMeasures' => 'Récapitulatif avec les mesures',
	'analysis_indicator_recapByClient' => 'Récapitulatif par client',

	'analysis_keyword_all_title'        => 'Toutes les [[ timesheet_entities ]] par [[ keyword_entities ]]',
	'analysis_projectKeyword_all_title' => 'Toutes les [[ timesheet_entities ]] par [[ keyword_entities ]] [[ project_entities ]]',

	'analysis_map_panel_configuration'     => 'Configuration',
	'analysis_map_panel_territory'         => 'Portrait de territoire',
	'analysis_map_panel_colored_data'      => 'Donnée colorimétrique',
	'analysis_map_panel_proportional_data' => 'Donnée proportionnelle',

	'analysis_map_color_min'    => 'Couleur min.',
	'analysis_map_color_max'    => 'Couleur max.',
	'analysis_map_classes'      => 'Nombre de classes',
	'analysis_map_distribution' => 'Distribution',
	'analysis_map_data'         => 'Donnée',
	'analysis_map_color'        => 'Couleur',

	'analysis_show_all_territories' => 'Afficher tous les territoires',

	'analysis_map_project_rep' => 'Répartition par [[ project_entities ]]',

	'tooltip_config_graph' => 'Options d\'affichage',

    'number_projects' => '{{self.projects.length}} fiches avec {{self.nbFilters}} filtre{{self.nbFilters > 1 ? "s" : ""}}',
    'number_filters' => '{{self.nbFilters}} filtre{{self.nbFilters > 1 ? "s" : ""}} appliqués',

];
