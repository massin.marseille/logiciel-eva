<?php

namespace Convention\Entity;

use Budget\Entity\Expense;
use Core\Entity\BasicRestEntityAbstract;
use Directory\Entity\Structure;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Project;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Line
 *
 * @package Convention\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="convention_line")
 */
class Line extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Convention
     *
     * @ORM\ManyToOne(targetEntity="Convention\Entity\Convention", inversedBy="lines", fetch="EAGER")
     */
    protected $convention;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project\Entity\Project")
     */
    protected $project;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $percentage;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Convention
     */
    public function getConvention()
    {
        return $this->convention;
    }

    /**
     * @param Convention $convention
     */
    public function setConvention($convention)
    {
        $this->convention = $convention;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return float
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param float $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    public function getAmountTotalProject()
    {
        return $this->project->getAmountArboIncomeArboRequested();
    }

    public function getPartTotalProject()
    {
        return ($this->getAmountTotalProject() * $this->percentage) / 100;
    }

    public function getTimeSpent()
    {
        return ($this->project->getTimeSpent() * $this->percentage) / 100;
    }

    public function getAutofinancement()
    {
        return ($this->project->getAmountIncomeAutofinancementArboRequested() * $this->percentage) / 100;
    }

    protected function getAmountExpense($type)
    {
        return ($this->project->{'getAmountExpenseArbo' . ucfirst($type)}() * $this->percentage) / 100;
    }

    protected function getAmountHTExpense($type)
    {
        return ($this->project->{'getAmountHTExpenseArbo' . ucfirst($type)}() * $this->percentage) / 100;
    }

    public function __call($name, $arguments)
    {
        foreach (Expense::getTypes() as $type) {
            if ($name === 'getAmountExpense' . ucfirst($type)) {
                return $this->getAmountExpense($type);
            } else if ($name === 'getAmountHTExpense' . ucfirst($type)) {
                return $this->getAmountHTExpense($type);
            }
        }

        return null;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'convention',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $convention = $entityManager->getRepository('Convention\Entity\Convention')->find($value);

                                return $convention !== null;
                            },
                            'message' => 'convention_line_field_convention_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'project',
                'required' => true,
                'filters'  => [
                    ['name' => 'Core\Filter\IdFilter'],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $project = $entityManager->getRepository('Project\Entity\Project')->find($value);

                                return $project !== null;
                            },
                            'message' => 'convention_line_field_project_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'percentage',
                'required' => true,
                'filters'  => [
                    ['name' => 'NumberParse']
                ]
            ],
        ]);

        return $inputFilter;
    }
}
