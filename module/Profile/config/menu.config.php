<?php

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'profile' => [
                        'icon'     => 'profile',
                        'right'    => 'profile:e:profile:r',
                        'title'    => 'profile_module_title',
                        'href'     => 'profile',
                        'priority' => 1000,
                    ],
                ]
            ],

        ],
    ],
];
