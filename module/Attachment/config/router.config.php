<?php

namespace Attachment;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'api'       => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'attachment' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/attachment[/:id]',
                            'defaults' => [
                                'controller' => 'Attachment\Controller\API\Attachment',
                            ],
                        ],
                    ],
                ],
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\API\AttachmentController::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Attachment\Controller\API\Attachment' => Controller\API\AttachmentController::class
        ]
    ]
];
