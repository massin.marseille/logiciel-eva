-- ADMIN
ALTER TABLE `eva_client` ENGINE=InnoDB ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
ALTER TABLE `eva_client` CHANGE COLUMN `modules` `modules` JSON NOT NULL AFTER `theme`, CHANGE COLUMN `translations` `translations` JSON NULL AFTER `modules`, CHANGE COLUMN `icons` `icons` JSON NULL AFTER `translations`;
ALTER TABLE `user_configuration` CHANGE COLUMN `tables` `tables` JSON NOT NULL AFTER `user_id`;
ALTER TABLE `user_role` CHANGE COLUMN `rights` `rights` JSON NOT NULL AFTER `name`;

-- CLIENTS
ALTER TABLE `application_configuration` CHANGE COLUMN `modules` `modules` JSON NOT NULL AFTER `id`, CHANGE COLUMN `translations` `translations` JSON NOT NULL AFTER `modules`, CHANGE COLUMN `timeGraph` `timeGraph` JSON NULL AFTER `code`;
ALTER TABLE `application_query` CHANGE COLUMN `filters` `filters` JSON NOT NULL AFTER `list`;
ALTER TABLE `field_field` CHANGE COLUMN `meta` `meta` JSON NULL AFTER `type`;
ALTER TABLE `home_container` CHANGE COLUMN `columns` `columns` JSON NULL AFTER `entity`;
ALTER TABLE `indicator_indicator` CHANGE COLUMN `values` `values` JSON NULL AFTER `type`;
ALTER TABLE `map_territory` CHANGE COLUMN `json` `json` JSON NULL AFTER `code`;
ALTER TABLE `project_template` CHANGE COLUMN `config` `config` JSON NULL AFTER `name`;
ALTER TABLE `time_bookmark` CHANGE COLUMN `config` `config` JSON NULL AFTER `name`;
ALTER TABLE `time_synchronisation` CHANGE COLUMN `config` `config` JSON NULL AFTER `type`, CHANGE COLUMN `cronUsers` `cronUsers` JSON NULL AFTER `parseTitle`;
ALTER TABLE `user_configuration` CHANGE COLUMN `tables` `tables` JSON NOT NULL AFTER `user_id`;
ALTER TABLE `user_role` CHANGE COLUMN `rights` `rights` JSON NOT NULL AFTER `name`;

-- QPILOTE TABLES
ALTER TABLE `charge_charge` CHANGE COLUMN `predecessor` `predecessor` JSON NULL DEFAULT NULL AFTER `delay`;
ALTER TABLE `charge_model` CHANGE COLUMN `predecessor` `predecessor` JSON NULL DEFAULT NULL AFTER `started_date`;
ALTER TABLE `event_mail` CHANGE COLUMN `unlayerData` `unlayerData` JSON NOT NULL AFTER `html`;
ALTER TABLE `event_mail_template` CHANGE COLUMN `data` `data` JSON NOT NULL AFTER `name`;
ALTER TABLE `project_configuration`
	CHANGE COLUMN `draft` `draft` JSON NULL DEFAULT NULL AFTER `id`,
	CHANGE COLUMN `pending` `pending` JSON NULL DEFAULT NULL AFTER `draft`,
	CHANGE COLUMN `waiting` `waiting` JSON NULL DEFAULT NULL AFTER `pending`,
	CHANGE COLUMN `validated` `validated` JSON NULL DEFAULT NULL AFTER `waiting`,
	CHANGE COLUMN `published` `published` JSON NULL DEFAULT NULL AFTER `validated`,
	CHANGE COLUMN `rejected` `rejected` JSON NULL DEFAULT NULL AFTER `published`,
	CHANGE COLUMN `archived` `archived` JSON NULL DEFAULT NULL AFTER `rejected`;
ALTER TABLE `workflow_field`
	CHANGE COLUMN `values` `values` JSON NULL DEFAULT NULL AFTER `type`,
	CHANGE COLUMN `valuesColors` `valuesColors` JSON NULL DEFAULT NULL AFTER `values`;
