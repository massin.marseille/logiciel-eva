<?php

use Bootstrap\Entity\Client;
use Core\Controller\Plugin\ApiRequestPlugin;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Export
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translate
     */
    protected $translator;

    /**
     * @var ApiRequestPlugin
     */
    protected $apiRequest;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Export constructor.
     *
     * @param EntityManager    $em
     * @param Translate        $translator
     * @param ApiRequestPlugin $apiRequest
     * @param Client           $client
     */
    public function __construct(EntityManager $em, Translate $translator, ApiRequestPlugin $apiRequest, Client $client)
    {
        $this->em         = $em;
        $this->translator = $translator;
        $this->apiRequest = $apiRequest;
        $this->client     = $client;
    }

    public function doExport($params, $data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);
        setlocale(LC_TIME, 'fr_FR', 'French_France', 'French');

        // Merge values of en expense cpCommited and a cpEstimated.
        foreach ($data as $projectKey => $project) {
            if (!$project['posteExpenses']) {
                continue;
            }

            foreach ($project['posteExpenses'] as $posteKey => $poste) {
                if (!isset($poste['expenses'])) {
                    continue;
                }

                $cpCommitedExpenses = [];
                foreach ($poste['expenses'] as $key => $expense) {
                    if ($expense['type'] === 'cpCommitted') {
                        $cpCommitedExpenses[] = $expense;
                        unset($poste['expenses'][$key]);
                    }
                }

                $cpEstimatedExpenses = [];
                foreach ($poste['expenses'] as $key => $expense) {
                    if ($expense['type'] === 'cpEstimated') {
                        $merge = false;

                        foreach ($cpEstimatedExpenses as $key2 => $cpEstimatedExpense) {
                            if (
                                $cpEstimatedExpense['tiers'] === $expense['tiers'] &&
                                $cpEstimatedExpense['mandat'] === $expense['mandat'] &&
                                $cpEstimatedExpense['complement'] === $expense['complement'] &&
                                $cpEstimatedExpense['dateFacture'] === $expense['dateFacture'] &&
                                $cpEstimatedExpense['date'] === $expense['date']
                            ) {
                                $merge = true;

                                $cpEstimatedExpense['amount'] += $expense['amount'];
                                $cpEstimatedExpenses[$key2] = $cpEstimatedExpense;
                                break;
                            }
                        }

                        if (!$merge) {
                            $cpEstimatedExpenses[] = $expense;
                        }

                        unset($poste['expenses'][$key]);
                    }
                }

                foreach ($cpEstimatedExpenses as $expense) {
                    foreach ($cpCommitedExpenses as $key => $cpCommittedExpense) {
                        if (
                            $cpCommittedExpense['amount'] === $expense['amount'] &&
                            $cpCommittedExpense['tiers'] === $expense['tiers']
                        ) {
                            $merge = true;

                            if (
                                ($cpCommittedExpense['name'] || $expense['name']) &&
                                $cpCommittedExpense['name'] !== $expense['name']
                            ) {
                                $merge = false;
                            }

                            if (!$merge) {
                                continue;
                            }

                            foreach ($cpCommittedExpense as $property => $value) {
                                if (!in_array($property, ['mandat', 'complement', 'dateFacture'])) {
                                    continue;
                                }

                                if (!$value) {
                                    $cpCommittedExpense[$property] = $expense[$property];
                                }
                            }

                            $cpCommitedExpenses[$key] = $cpCommittedExpense;
                        }
                    }
                }

                $data[$projectKey]['posteExpenses'][$posteKey]['expenses'] = $cpCommitedExpenses;
            }
        }

        $accounts = [];
        foreach ($data as $project) {
            if (!$project['posteExpenses']) {
                continue;
            }

            foreach ($project['posteExpenses'] as $poste) {
                if (!$poste['account']) {
                    if (!isset($accounts['Sans compte'])) {
                        $accounts['Sans compte'] = [];
                    }

                    $accounts['Sans compte'][] = $poste;
                    continue;
                }

                if (!isset($accounts[$poste['account']['id']])) {
                    $accounts[$poste['account']['id']] = [];
                }

                $accounts[$poste['account']['id']][] = $poste;
            }
        }

        $lines = [[
                      'Catégorie de dépenses',
                      'Sous catégorie de dépenses',
                      'Numéro de facture',
                      'Intitulé de la dépense',
                      'Tiers',
                      'Date d\'émission de la facture',
                      'Date d\'acquittement de la facture',
                      'Référence de l\'acquittement',
                      'Montant de la facture HT',
                      'TVA',
                      'Montant total de la facture (TTC)',
                  ]];

        $accountLines = [];
        foreach ($accounts as $accountId => $account) {
            $currentLine    = count($lines) + 1;
            $accountLines[] = $currentLine;
            $numberExpenses = $this->getNumberExpenses($account);

            $line = [
                $accountId === 'Sans compte'
                    ? 'Sans compte'
                    : $account[0]['account']['name'] . ' ',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '= SUM(I' . ($currentLine + 1) . ':I' . ($currentLine + $numberExpenses) . ')',
                '= SUM(J' . ($currentLine + 1) . ':J' . ($currentLine + $numberExpenses) . ')',
                '= SUM(K' . ($currentLine + 1) . ':K' . ($currentLine + $numberExpenses) . ')',
            ];

            $lines[] = $line;

            foreach ($account as $posteExpense) {
                foreach ($posteExpense['expenses'] as $expense) {
                    $currentLine = count($lines) + 1;

                    $line = [
                        '',
                        $expense['name'] . ' ',
                        $expense['mandat'] . ' ',
                        $expense['complement'] . ' ',
                        $expense['tiers'] . ' ',
                        str_replace('00:00', '', $expense['dateFacture']),
                        str_replace('00:00', '', $expense['date']),
                        $expense['bordereau'] . ' ',
                        $expense['amountHT'],
                        '= IF(I' . $currentLine . ' <> 0, K' . $currentLine . ' - I' . $currentLine . ', 0)',
                        $expense['amount'],
                    ];

                    $lines[] = $line;
                }
            }
        }

        $totalLine = [
            'Total',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '= ',
            '= ',
            '= ',
        ];

        $numberAccountLines = count($accountLines);
        foreach ($accountLines as $key => $accountLine) {
            $totalLine[8]  .= 'I' . $accountLine;
            $totalLine[9]  .= 'J' . $accountLine;
            $totalLine[10] .= 'K' . $accountLine;

            if ($key !== $numberAccountLines - 1) {
                $totalLine[8]  .= ' + ';
                $totalLine[9]  .= ' + ';
                $totalLine[10] .= ' + ';
            }
        }

        $lines[] = $totalLine;

        $accountLines[] = count($lines);

        $this->download($lines, $accountLines);
    }

    protected function getNumberExpenses($account)
    {
        $number = 0;

        foreach ($account as $posteExpense) {
            $number += count($posteExpense['expenses']);
        }

        return $number;
    }

    protected function download($array, $linesBolded)
    {
        try {
            foreach ($array as $i => $line) {
                foreach ($line as $j => $value) {
                    if (is_numeric($value) && $value == 0) {
                        $array[$i][$j] = '0';
                    }
                }
            }

            $spreadsheet = new Spreadsheet();
            $sheet       = $spreadsheet->getActiveSheet();

            $sheet->fromArray($array);

            foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
                $sheet->getColumnDimension($col)->setAutoSize(true);
            }

            $headerLine = $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . '1');
            $headerLine
                ->getFont()
                ->setBold(true);
            $headerLine
                ->getAlignment()
                ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                ->setVertical(Alignment::VERTICAL_CENTER);

            $sheet
                ->getStyle('A1:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())
                ->getAlignment()
                ->setWrapText(true);

            foreach ($linesBolded as $line) {
                $headerLine = $sheet->getStyle('A' . $line . ':' . $sheet->getHighestDataColumn() . $line);
                $headerLine->getFont()->setBold(true);
            }

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            header('Content-Disposition: attachment; filename="export.xlsx"');
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            ob_end_clean();
            $writer->save('php://output');
            exit;
        } catch (Exception $exception) {
            var_dump($exception->getMessage());
        }
    }
}
