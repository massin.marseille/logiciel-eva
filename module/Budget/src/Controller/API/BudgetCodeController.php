<?php

namespace Budget\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class BudgetCodeController extends BasicRestController
{
	protected $entityClass = 'Budget\Entity\BudgetCode';
	protected $repository = 'Budget\Entity\BudgetCode';
	protected $entityChain = 'budget:e:budgetCode';

	/**
	 * @param QueryBuilder $queryBuilder
	 * @param $data
	 */
	protected function searchList(QueryBuilder $queryBuilder, &$data)
	{
		$sort = $data['sort'] ?: 'object.name';
		$order = $data['order'] ?: 'ASC';

		if (strpos($sort, '.') === false) {
			$sort = 'object.' . $sort;
		}

		$queryBuilder->join('object.project', 'project');

		$queryBuilder->addOrderBy($sort, $order);

		if (isset($data['full']) && $data['full']) {
			$queryBuilder->andWhere($queryBuilder->expr()->orX(...['object.name LIKE :f_full']));

			$queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
		}
	}
}
