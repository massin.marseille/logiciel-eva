(function(app) {

    app.controller('territoriesTabController', [
        'leafletData', '$timeout', '$scope', 'territoryAssociationService', '$filter', 'flashMessenger', 'translator',
        function territoriesTabController(leafletData, $timeout, $scope, territoryAssociationService, $filter, flashMessenger, translator) {
            var self = this;

            self.territories  = [];
            self.associations = [];

            self.options = {
                col: [
                    'id',
                    'entity',
                    'primary',
                    'territory.id',
                    'territory.name',
                    'territory.json'
                ]
            };

            self.loading = {
                save: false
            };

            self.from = {
                type: null,
                id: null,
                right: null,
                object: null,
            };

            self.init = function init(from, right, id, model) {
                self.from.type  = from;
                self.from.id    = id;
                self.from.right = right;

                $scope.$watch(function () { return model }, function (newVal, oldVal) {
                    if (newVal !== null) {
                        self.from.id = newVal.id;
                        self.from.object = newVal;
                        territoryAssociationService.findAll({
                            col: self.options.col,
                            right: self.from.right,
                            search: {
                                type: 'list',
                                data: {
                                    filters: {
                                        entity: {
                                            op: 'eq',
                                            val: self.from.type
                                        },
                                        primary: {
                                            op: 'eq',
                                            val: self.from.id
                                        }
                                    }
                                }
                            }
                        }).then(function(res) {
                            if (!res.success) {
                                flashMessenger.error(translator('error_occured'));
                                return;
                            }

                            self.associations = res.rows;
                            for (var i = 0; i < res.rows.length; i++) {
                                self.territories.push(res.rows[i].territory);
                            }
                        }, function () {
                            flashMessenger.error(translator('error_occured'));
                        });
                    }
                });
            };

            var featureGroup = null;
            $scope.$watch(function () { return self.territories }, function (newVal) {
                leafletData.getMap('map').then(function(map) {
                    if (featureGroup !== null) {
                        map.removeLayer(featureGroup);
                    }
                    var layers = [];
                    for (var i = 0; i < self.territories.length; i++) {
                        var territory = self.territories[i];
                        if (!angular.isArray(territory.json) && angular.isObject(territory.json)) {
                            layers.push(L.geoJson(territory.json, {
                                fillColor: '#ff2424',
                                color: '#ff2424',
                                weight: 2,
                                fillOpacity: 0.5
                            }));
                        }
                    }
                    if (layers.length > 0) {
                        featureGroup = new L.featureGroup(layers).addTo(map);
                        map.fitBounds(featureGroup.getBounds());
                    }
                });
            }, true);

            self.save = function () {
                self.loading.save = true;

                var toDelete = [];
                var associations = angular.copy(self.associations);

                for (var i in associations) {
                    var association = associations[i];
                    var filter = $filter('filter')(self.territories, function(item) {
                        return item.id === association.territory.id;
                    }, true);
                    if (filter && filter.length == 0) {
                        toDelete.push(angular.copy(association));
                        self.associations.splice(self.associations.indexOf(association), 1);
                    }
                }

                for (var i in self.territories) {
                    var territory = self.territories[i];
                    var filter = $filter('filter')(self.associations, function(item) {
                        return item.territory.id === territory.id;
                    }, true);
                    if (filter && filter.length == 0) {
                        self.associations.push({
                            territory: territory,
                            entity: self.from.type,
                            primary: self.from.id
                        });
                    }
                }

                territoryAssociationService.saveAll(self.associations, {
                    col: self.options.col,
                    right: self.from.right
                }).then(function(res) {
                    for (var i in res) {
                        if (!res[i].success) {
                            flashMessenger.error(translator('error_occured'));
                            self.loading.save = false;
                            return;
                        }
                    }

                    self.associations = [];
                    for (var i = 0; i < res.length; i++) {
                        self.associations.push(res[i].object);
                    }

                    territoryAssociationService.removeAll(toDelete, {
                        col: self.options.col,
                        right: self.from.right
                    }).then(function (res) {
                        for (var i in res) {
                            if (!res[i].success) {
                                flashMessenger.error(translator('error_occured'));
                                self.loading.save = false;
                                return;
                            }
                        }

                        self.loading.save = false;
                    }, function () {
                        flashMessenger.error(translator('error_occured'));
                    });
                }, function () {
                    flashMessenger.error(translator('error_occured'));
                });
            };

            $timeout(function() {
                leafletData.getMap('map').then(function(map) {
                    map.invalidateSize();
                });
            });
        }
    ])

})(window.EVA.app);
