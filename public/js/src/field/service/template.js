(function(app) {

    app.factory('templateService', [
        '$http', '$q', 'restFactoryService',
        function templateService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/template'));
        }
    ])

})(window.EVA.app);
