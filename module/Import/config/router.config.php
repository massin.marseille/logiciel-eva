<?php

namespace Import;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'routes' => [
            'import'        => [
                'type'          => 'Segment',
                'options'       => [
                    'route'    => '/import[/:entity]',
                    'defaults' => [
                        'controller' => 'Import\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'parse' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/parse',
                            'defaults' => [
                                'controller' => 'Import\Controller\Index',
                                'action'     => 'parse',
                            ],
                        ],
                    ],
                    'validate' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/validate',
                            'defaults' => [
                                'controller' => 'Import\Controller\Index',
                                'action'     => 'validate',
                            ],
                        ],
                    ],
                    'save' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/save',
                            'defaults' => [
                                'controller' => 'Import\Controller\Index',
                                'action'     => 'save',
                            ],
                        ],
                    ],
                    'save-keyword' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/save-keyword',
                            'defaults' => [
                                'controller' => 'Import\Controller\Index',
                                'action'     => 'saveKeywords',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
          Controller\IndexController::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Import\Controller\Index' => Controller\IndexController::class,
        ],
    ],
];
