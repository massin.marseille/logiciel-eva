<?php

namespace Convention\View\Helper;

use Bootstrap\Entity\Client;
use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class ConventionFilters extends EntityFilters
{
	public function __construct(
		Translate $translator,
		MyModuleManager $moduleManager,
		EntityManager $entityManager,
		Client $client,
		$allowedKeywordGroups
	) {
		parent::__construct($translator, $moduleManager, $entityManager);

		$filters = [
			'name' => [
				'label' => $this->translator->__invoke('convention_field_name'),
				'type' => 'string',
			],
			'networkAccessible' => [
				'label' => $this->translator->__invoke('convention_field_networkAccessible'),
				'type' => 'boolean',
			],
			'contractor' => [
				'label' => $this->translator->__invoke('convention_field_contractor'),
				'type' => 'structure-select',
			],
			'number' => [
				'label' => $this->translator->__invoke('convention_field_number'),
				'type' => 'string',
			],
			'numberArr' => [
				'label' => $this->translator->__invoke('convention_field_numberArr'),
				'type' => 'string',
			],
			'start' => [
				'label' => $this->translator->__invoke('convention_field_start'),
				'type' => 'date',
			],
			'end' => [
				'label' => $this->translator->__invoke('convention_field_end'),
				'type' => 'date',
			],
			'decisionDate' => [
				'label' => $this->translator->__invoke('convention_field_decisionDate'),
				'type' => 'date',
			],
			'notificationDate' => [
				'label' => $this->translator->__invoke('convention_field_notificationDate'),
				'type' => 'date',
			],
			'returnDate' => [
				'label' => $this->translator->__invoke('convention_field_returnDate'),
				'type' => 'date',
			],
			'amount' => [
				'label' => $this->translator->__invoke('convention_field_amount'),
				'type' => 'number',
			],
			'amountSubv' => [
				'label' => $this->translator->__invoke('convention_field_amountSubv'),
				'type' => 'number',
			],
			'members.id' => [
				'label' => $this->translator->__invoke('convention_field_members'),
				'type' => 'user-select',
			],
			'createdAt' => [
				'label' => $this->translator->__invoke('convention_field_createdAt'),
				'type' => 'date',
			],
			'updatedAt' => [
				'label' => $this->translator->__invoke('convention_field_updatedAt'),
				'type' => 'date',
			],
		];

		if ($this->moduleManager->isActive('keyword')) {
			$groups = $allowedKeywordGroups('convention', false);
			foreach ($groups as $group) {
				$filters['keyword.' . $group->getId()] = [
					'label' => $group->getName(),
					'type' => 'keyword-select',
					'group' => $group->getId(),
				];
			}
		}

		if ($this->moduleManager->isActive('map')) {
			$filters['territory'] = [
				'label' => ucfirst($this->translator->__invoke('territory_entity')),
				'type' => 'territory-select',
			];
		}

		if ($client->isMaster()) {
			$clientsOptions = [];

			foreach ($client->getNetwork()->getClients() as $client) {
				if (!$client->isMaster()) {
					$clientsOptions[] = [
						'value' => $client->getId(),
						'text' => $client->getName(),
					];
				}
			}

			$filters['_client'] = [
				'label' => ucfirst($this->translator->__invoke('client_entity')),
				'type' => 'manytoone',
				'options' => $clientsOptions,
			];
		}

		$this->filters = $filters;
	}
}
