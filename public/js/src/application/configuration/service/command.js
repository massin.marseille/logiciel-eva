(function(app) {

    app.factory('commandService', [
        '$http', '$q',
        function commandService($http, $q) {
            return {
                run: function run(command) {
                    var deferred = $q.defer();

                    $http({
                        method: 'GET',
                        url: '/api/command/run/' + command
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                }
            }
        }
    ])

})(window.EVA.app);
