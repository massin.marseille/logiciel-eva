(function(app) {
	app.controller('projectListController', [
		'projectService',
		'queryService',
		'$scope',
		'flashMessenger',
		'translator',
		'$filter',
		'$http',
		'$timeout',
		function projectListController(
			projectService,
			queryService,
			$scope,
			flashMessenger,
			translator,
			$filter,
			$http,
			$timeout,
		) {
			var self = this;

			self.view = 'list';
			self.fromHome = false;
			self.parent = null;
			self.exportModel = null;
			self.keywordsFamily = null;
			self.expenseType = null;
			self.incomeType = null;
			self.financialYear = null;
			self.nature = null;

			self.loading = {};

			self.multipleEdit = {
				use: {},
			};
			self.multipleEditErrors = {};

			self.projects = [];
			self.options = {
				name: 'projects-list',
				col: [
					'id',
					'name',
					'code',
					'publicationStatus',
					'members.id',
					'members.role',
					'members.user.id',
					'members.user.name',
					'members.user.avatar',					
					'members.keywords',
					'ownership.id',
					'ownership.name',
					'external',
					'template.id',
					'template.name',
					'networkAccessible',
					'programmingDate',
					'plannedDateStart',
					'plannedDateEnd',
					'realDateStart',
					'realDateEnd',
					'createdAt',
					'updatedAt',
					'parent.id',
					'parent.code',
					'parent.name',
					'stackedAdvancement',
					'caduciteStart',
					'caduciteEnd',
					'keywords',
					'level',
					'budgetStatus.id',
					'budgetStatus.name',
					'budgetCodes.id',
					'budgetCodes.name',

					'posteExpenses.id',
					'posteExpenses.name',
					'posteExpenses.amountArboAeBalance',
					'posteExpenses.amountArboAeRateBalance',
					'posteExpenses.amountArboAeCommittedBalance',
					'posteExpenses.amountArboTotalPaid',
					'posteExpenses.amountArboCommittedBalance',
					'posteExpenses.amountHTArboCommittedBalance',
					'posteExpenses.amountArboPlannedCommittedPaidBalance',
					'posteExpenses.amountHTArboPlannedCommittedPaidBalance',
					'posteExpenses.amountArboCpBalance',
					'posteExpenses.amountArboAeCommittedCpCommittedBalance',
					'posteExpenses.amountArboCpCommittedAeCommittedRateBalance',
					'posteExpenses.amountArboCpCommitted',
					'posteExpenses.amountArboAeCommitted',
					'posteExpenses.amountArboAe',
					'posteExpenses.amountArboBalance',

					'budgetCodes.id',
					'budgetCodes.name',

					'rateExpenseRealized',
					'rateIncomeRealized',
					'customFields',
					'territories',
				],
				searchType: 'list',
				sort: 'name',
				order: 'asc',
				pager: true,
				getItems: function getData(params, callback, abort) {
					if (self.query != null) {
						params.search.data.filters = self.query.filters;
					}
					if (self.parent != null) {
						params.search.data.filters.parent = self.parent;
					}
					projectService.findAll(params, abort.promise).then(function(res) {
						callback(res);
					});
					params.search.data.nbFilters = Object.keys(params.search.data.filters).length;
				},
			};

			let queryApiParams = {
				col: [
					'id', 'name', 'filters', 'list', 'columns'
				],
			};

			self.init = function(user, indicatorsTransverses) {
				self.currentUser = user;
				if (!self.query) {
					self.query = {
						filters: {
							publicationStatus: {
								op: 'neq',
								val: ['archived'],
							},
						},
					};
					$timeout(function() {
						if (typeof $scope.load === 'function') {
							$scope.load(self.query);
						}
						self.query = null;
					}, 1000);
				}

				if (indicatorsTransverses === true) {
					self.options.col.push('measuresT');
				}
			};

			self.initHome = async function initHome(name, query) {
				self.fromHome = true;
				self.options.name = name;
				self.query = query;
			};

			self.showExportModal = function(type) {
				angular.element('#export' + type + 'Modal').modal('show');
			};

			self.export = function(type) {
				if (self.exportModel === 'default') {
					$scope.__tb.exportExcel();
					self.resetForm();
					angular.element('#export' + type + 'Modal').modal('hide');
				} else {
					angular.element('#export' + type + 'Form').submit();
					self.resetForm();
					angular.element('#export' + type + 'Modal').modal('hide');
				}
			};

			self.resetForm = function() {
				self.exportModel = null;
				self.keywordsFamily = null;
				self.expenseType = null;
				self.incomeType = null;
				self.financialYear = null;
				self.nature = null;
			};

			self.locked = function(project) {
				return (
					project.publicationStatus === 'waiting_validation' ||
					project.publicationStatus === 'validated' ||
					project.publicationStatus === 'published'
				);
			};

			self.fullTextSearch = function fullTextSearch() {
				$scope.__tb.resetFilters();
				$scope.__tb.apiParams.search.data.full = self.fullText;
				$scope.__tb.loadData();
			};

			self.resetfullTextSearch = function resetfullTextSearch() {
				self.fullText = '';
				delete $scope.__tb.apiParams.search.data.full;
			};

			angular.element('#full-text').on('keypress', function(event) {
				if (event.which == 13) {
					self.fullTextSearch();
				}
			});

			self.setParent = function(id) {
				if (typeof id !== 'number' || id == null) {
					id = $scope.$parent.$parent.self.project.id;
				}

				self.parent = {
					op: 'eq',
					val: id,
				};
			};

			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};
			self.editLine = function editLine(project) {
				project.managers = [];
				var managers = $filter('filter')(project.members, { role: 'manager' });
				for (var i in managers) {
					project.managers.push(managers[i].user);
				}

				project.validators = [];
				var validators = $filter('filter')(project.members, { role: 'validator' });
				for (var i in validators) {
					project.validators.push(validators[i].user);
				}

				if (angular.isArray(project.keywords) || project.keywords == null) {
					project.keywords = {};
				}

				if (angular.isArray(project.customFields) || project.customFields == null) {
					project.customFields = {};
				}

				self.editedLineErrors = {};
				self.editedLine = project;
				self.editedLineFields = angular.copy(project);
			};

			self.synchronizeMembers = function(project) {
				if (typeof project.members !== 'object' || project.members == null) {
					project.members = [];
				}

				var prevManagers = $filter('filter')(project.members, { role: 'manager' });
				var newManagers = project.managers;

				for (var i in prevManagers) {
					var prevManager = prevManagers[i];
					var filter = $filter('filter')(newManagers, function(item) { 
						return item.id === prevManager.user.id;
					});
					if (filter && filter.length == 0) {
						project.members.splice(project.members.indexOf(prevManager), 1);
					}
				}

				for (var i in newManagers) {
					var newManager = newManagers[i];
					var filter = $filter('filter')(prevManagers, function(item) { 
						return item.user.id === newManager.id;
					});
					if (filter && filter.length == 0) {
						project.members.push({
							user: {
								id: newManager.id,
								name: newManager.name,
								avatar: newManager.avatar,
							},
							role: 'manager',
						});
					}
				}

				var prevValidators = $filter('filter')(project.members, { role: 'validator' });
				var newValidators = project.validators;

				for (var i in prevValidators) {
					var prevValidator = prevValidators[i];
					var filter = $filter('filter')(newValidators, function(item) { 
						return item.id === prevValidator.user.id;
					});
					if (filter && filter.length == 0) {
						project.members.splice(project.members.indexOf(prevValidator), 1);
					}
				}

				for (var i in newValidators) {
					var newValidator = newValidators[i];
					var filter = $filter('filter')(prevValidators, function(item) { 
						return item.user.id === newValidator.id;
					});
					if (filter && filter.length == 0) {
						project.members.push({
							user: {
								id: newValidator.id,
								name: newValidator.name,
								avatar: newValidator.avatar,
							},
							role: 'validator',
						});
					}
				}
			};

			self.saveEditedLine = function saveEditedLine(project) {
				self.synchronizeMembers(self.editedLineFields);

				self.editedLineErrors = {};
				projectService
					.save(angular.extend({}, project, self.editedLineFields), {
						col: self.options.col,
					})
					.then(
						function(res) {
							if (res.success) {
								project = angular.extend(project, res.object);
								self.editedLine = null;
								self.editedLineFields = null;

								flashMessenger.success(translator('project_message_saved'));
							}
						},
						function(err) {
							for (var field in err.fields) {
								self.editedLineErrors[field] = err.fields[field];
							}
						},
					);
			};

			self.duplicate = function duplicate(project) {
				$http({
					method: 'POST',
					url: '/project/duplicate/' + project.id,
				}).then(function(res) {
					if (res.data.success) {
						projectService.find(res.data.project.id, { col: self.options.col }).then(function(res) {
							var index = self.projects.findIndex(function(element) {
								return element.id === project.id;
							});
							self.projects.splice(index + 1, 0, res.object);
						});
					} else {
						for (var i in res.data.error) {
							flashMessenger.error(res.data.error[i]);
						}
					}
				});
			};

			self.deleteProject = function deleteProject(project) {
				if (
					confirm(
						translator('project_question_delete').replace(
							'%1',
							(project.code ? project.code + ' - ' : '') + project.name,
						),
					)
				) {
					projectService.remove(project).then(function(res) {
						if (res.success) {
							self.projects.splice(self.projects.indexOf(project), 1);
							flashMessenger.success(translator('project_message_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				}
			};

			self.exportExcel = function(type) {
				if (self.view == 'tree') {
					var columns = [];
					for (var i in $scope.__tb.columns) {
						var col = $scope.__tb.columns[i];
						if (col.visible && i != 'selection' && i != 'actions') {
							columns.push(i);
						}
					}
					$scope.$broadcast('export-xls-tree', columns, type);
				} else {
					$scope.__tb.exportExcel();
				}
			};

			$scope.$watch(
				function() {
					return self.multipleEdit;
				},
				function(newVal, oldVal) {
					for (var i in newVal) {
						if (typeof oldVal[i] === 'undefined') {
							self.multipleEdit.use[i] = true;
						}
					}
				},
				true,
			);

			self.showMultipleEditModal = function() {
				angular.element('#multipleEditModal').modal('show');
			};

			self.doMultipleEdit = function() {
				self.loading.multipleEdit = true;
				self.multipleEditErrors = {};

				var projects = [];
				for (var i in self.projects) {
					if (self.projects[i].selected) {
						projects.push(self.projects[i]);
					}
				}

				var projectsCopy = [];
				angular.copy(projects, projectsCopy);

				for (i in projectsCopy) {
					for (var attribute in self.multipleEdit) {
						if (self.multipleEdit.use[attribute]) {
							projectsCopy[i][attribute] = self.multipleEdit[attribute];
						}
					}
				}

				projectService.saveAll(projectsCopy, { col: self.options.col }).then(function(res) {
					var success = false;
					for (var i in res) {
						if (res[i].success) {
							success = true;
							projects[i] = angular.extend(projects[i], res[i].object);
						} else {
							if (res[i].errors.fields instanceof Array && res[i].errors.fields.length === 0) {
								res[i].errors.fields = {};
								res[i].errors.fields[translator('right_errors')] = {
									update: translator('right_update_denied'),
								};
							}

							for (var field in res[i].errors.fields) {
								var fieldTranslation;
								if (
									['publicationStatus'].indexOf(field) !== -1 ||
									/^project_field/.test(translator('project_field_' + field))
								) {
									fieldTranslation = field;
								} else {
									fieldTranslation = translator('project_field_' + field);
								}

								if (!self.multipleEditErrors[fieldTranslation]) {
									self.multipleEditErrors[fieldTranslation] = {};
								}

								var projectName =
									projects[i].id +
									' - ' +
									(projects[i].code ? projects[i].code + ' - ' : '') +
									(projects[i].name ? projects[i].name : '');

								self.multipleEditErrors[fieldTranslation][projectName] = [];

								for (var error in res[i].errors.fields[fieldTranslation]) {
									self.multipleEditErrors[fieldTranslation][projectName].push(
										res[i].errors.fields[fieldTranslation][error],
									);
								}
							}
						}
					}

					if (success) {
						flashMessenger.success(translator('project_message_saved_at_least_one'));
					} else {
						flashMessenger.error(translator('project_message_saved_all_errors'));
					}

					self.loading.multipleEdit = false;
				});
			};

			self.isValidator = function(project) {
				for (var i in project.members) {
					var validator = project.members[i];
					if (validator.user.id == self.currentUser && validator.role == 'validator') {
						return true;
					}
				}
				return false;
			};

			self.isManager = function(project) {
				for (var i in project.members) {
					var manager = project.members[i];
					if (manager.user.id == self.currentUser && manager.role == 'manager') {
						return true;
					}
				}
				return false;
			};

			self.roundToTwo = function(num) {
				return +(Math.round(num + 'e+2') + 'e-2');
			};

			self.arraySum = (arr, field) => {
				if(arr !== null) {
					return arr.reduce(function (acc, val) {
						return acc + val[field];
					}, 0);
				}

				return 0;
			};

			self.calculateAverageRateBalance = (project, field) => {
				let average = 0;
				if (project && project.posteExpenses && Array.isArray(project.posteExpenses)) {
					let totalRateBalance = 0;
					let count = 0;

					project.posteExpenses.forEach(function(item) {
						if (typeof item[field] === 'number') {
							totalRateBalance += item[field];
							count++;
						}
					});

					if (count > 0) average = totalRateBalance / count;

				}
				return average.toFixed(2);
			}

			self.financialRealization = (project) => {
				let cp = self.arraySum(project, 'amountArboCpCommitted');
				let ae = self.arraySum(project, 'amountArboAeCommitted');

				if(ae===0) return 0;

				return ((cp/ae)*100).toFixed(2);
			}

			self.calculateAe = (project) => {
				let aeo = self.arraySum(project, 'amountArboAe');
				let ae = self.arraySum(project, 'amountArboAeCommitted');

				if(aeo===0) return 0;

				return (ae*100/aeo).toFixed(2);
			}

			self.getCodesLines = (project) => {
				let lineCode = "";

				if(project.budgetCodes) {
					lineCode = project.budgetCodes.map(function (item) {
						return item.name;
					}).join(',');
				}
				return lineCode;
			};

			self.isArray = function(value) {
				return value instanceof Array;
			};

			self.hasRole = function(member) {
				return member.keywords && Object.keys(member.keywords).length
			};

			self.getRole = function(member) {
               return member.keywords[Object.keys(member.keywords)[0]][0].name;
			}

		},
	]);
})(window.EVA.app);
