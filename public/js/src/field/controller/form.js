(function(app) {

    app.controller('fieldFormController', [
        'fieldService', 'flashMessenger', 'translator', '$scope',
        function fieldFormController(fieldService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal = angular.element('#fieldFormModal');

            self.field = {};
            self.errors = {
                fields: {}
            };
            self.loading = {
                save: false
            };
            self.group = null;
            $scope.$on('create-field', function (event, params) {
                self.field = {};
                self.group = params.group;
                self.field.group = self.group.id;
                self.field.order = 0;

                self.panelTitle = translator('field_nav_form');

                $modal.modal('show');
            });

            $scope.$on('edit-field', function (event, params) {
                self.field = angular.copy(params.field);
                self.panelTitle = params.field.name;
                self.group = params.group;

                $modal.modal('show');
            });

            $scope.$on('delete-field', function (event, params) {
                var field = params.field;
                var group = params.group;

                if (confirm(translator('field_question_delete').replace('%1', field.name))) {
                    fieldService.remove(field).then(function (res) {
                        if (res.success) {
                            group.fields.splice(group.fields.indexOf(field), 1);
                            flashMessenger.success(translator('field_message_deleted'));
                        } else {
                            flashMessenger.success(translator('error_occured'));
                        }
                    }, function () {});
                }
            });

            self.saveField = function saveField() {
                self.loading.save = true;
                var isCreation = !self.field.id;

                fieldService.save(self.field, {
                    col: [
                        'id',
                        'name',
                        'help',
                        'type',
                        'group.id',
                        'meta',
                        'order',
                        'placeholder',
                    ]
                }).then(function(res) {
                    if (isCreation) {
                        if (!angular.isArray(self.group.fields)) {
                            self.group.fields = [];
                        }
                        self.group.fields.push(res.object);
                    } else {
                        for (var i in self.group.fields) {
                            if (self.group.fields[i].id == res.object.id) {
                                self.group.fields[i] = res.object;
                            }
                        }
                    }

                    $modal.modal('hide');

                    flashMessenger.success(translator('field_message_saved'));
                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                });
            };

            self.addValue = function addValue() {
                if (!angular.isObject(self.field.meta) || angular.isArray(self.field.meta)) {
                    self.field.meta = {};
                }
                if (!angular.isArray(self.field.meta.values)) {
                    self.field.meta.values = [];
                }
                self.field.meta.values.push({v: ''});
            };

            self.deleteValue = function deleteValue(value) {
                self.field.meta.values.splice(self.field.meta.values.indexOf(value), 1);
            };
        }
    ]);

})(window.EVA.app);
