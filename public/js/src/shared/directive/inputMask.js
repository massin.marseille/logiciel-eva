(function(app) {

    Inputmask.extendAliases({
        amount: {
            prefix: '',
            groupSeparator: '.',
            alias: 'numeric',
            placeholder: '0',
            autoGroup: !0,
            digits: 2,
            digitsOptional: !1,
            clearMaskOnLostFocus: !1
        }
    });

    Inputmask.extendAliases({
        qdate: {
            inputFormat: 'dd/mm/yyyy',
            alias: 'datetime',
        },
        qdatetime: {
            inputFormat: 'dd/mm/yyyy HH:MM',
            alias: 'datetime',
        }
    });


    app.directive('inputMask', [
        function inputMask() {
            return {
                require: 'ngModel',
                restrict: 'A',
                replace: false,
                transclude: true,
                compile: function () {
                    return function (scope, element, attributes, ngModelCtrl) {
                        element.inputmask(attributes.inputMask);
                        element.on('keyup paste focus blur', function() {
                            var val = $(this).val();

                            // update the ng-model value
                            ngModelCtrl.$setViewValue(val);
                            ngModelCtrl.$render();
                        });
                    }
                }
            }
        }
    ])

})(window.EVA.app);
