<?php

namespace Core\Filter;

use Laminas\Filter\FilterInterface;

class IdFilter implements FilterInterface
{
    public function filter($value)
    {
        if (!$value || !isset($value['id'])) {
            return $value;
        }

        return $value['id'];
    }
}
