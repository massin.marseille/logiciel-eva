<?php

namespace Keyword\Repository;

use Doctrine\ORM\EntityRepository;
use Keyword\Entity\Group;

/**
 * Class GroupRepository
 *
 * @package Keyword\Repository
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 */
class GroupRepository extends EntityRepository
{
    public function findByEntity($entity, $archived = null, $search = null, $servicesUser = null)
    {
        $queryBuilder = $this->createQueryBuilder('object');
        $queryBuilder->where('(
            object.entities LIKE :start
            OR
            object.entities LIKE :end
            OR
            object.entities LIKE :middle
            OR
            object.entities = :entity
        )');

        $queryBuilder->setParameter('start', $entity . ',%');
        $queryBuilder->setParameter('middle', '%,' . $entity . ',%');
        $queryBuilder->setParameter('end', '%,' . $entity);
        $queryBuilder->setParameter('entity', $entity);

        if ($search) {
            foreach ($search as $property => $value) {
                $queryBuilder->andWhere('object.' . $property . ' = :' . $property);
                $queryBuilder->setParameter($property, $value);
            }
        }

        if (isset($archived)) {
            $queryBuilder->andWhere('object.archived = :archived');
            $queryBuilder->setParameter('archived', $archived);
        }

        if ($servicesUser) {
            Group::ownerControlDQL($queryBuilder, $servicesUser, 'service');
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
