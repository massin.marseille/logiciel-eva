<?php
namespace Home\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use User\Entity\User;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Home
 *
 * @package Home\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="home_home")
 */
class Home extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\OneToMany(targetEntity="Container", mappedBy="home")
     */
    protected $containers;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * Home constructor.
     */
    public function __construct()
    {
        $this->containers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getContainers()
    {
        return $this->containers;
    }

    /**
     * @param ArrayCollection $containers
     */
    public function setContainers($containers)
    {
        $this->containers = $containers;
    }

    /**
     * @param ArrayCollection $containers
     */
    public function addContainers(ArrayCollection $containers)
    {
        foreach ($containers as $container) {
            $this->getContainers()->add($container);
        }
    }

    /**
     * @param ArrayCollection $containers
     */
    public function removeContainers(ArrayCollection $containers)
    {
        foreach ($containers as $container) {
            $this->getContainers()->removeElement($container);
        }
    }

    /**
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'        => 'enabled',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'     => 'containers',
                'required' => false,
            ],
            [
                'name'       => 'user',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $user = $entityManager->getRepository('User\Entity\User')->find($value);

                                return $user !== null;
                            },
                            'message'  => 'home_field_user_error_non_exists',
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
