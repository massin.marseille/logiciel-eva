(function (app) {

    app.controller('timesheetFormController', [
        'timesheetService', 'userService', 'projectService', 'flashMessenger', 'translator', '$scope', '$timeout', '$rootScope',
        function timesheetFormController(timesheetService, userService, projectService, flashMessenger, translator, $scope, $timeout, $rootScope) {
            var self = this;

            self.timesheetFormModalName = 'timesheetFormModal' + moment().format('x');
            self.settingsTime = {};
            var $modal = null;

            $timeout(function () {
                $modal = angular.element('#' + self.timesheetFormModalName);
            });

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'user.id',
                    'user.name',
                    'user.avatar',
                    'user.color',
                    'project.id',
                    'project.code',
                    'project.name',
                    'start',
                    'end',
                    'hours',
                    'type',
                    'absenceType.id',
                    'absenceType.name',
                    'description',
                    'keywords',
                    'territories'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.timesheet = {
                _rights: {
                    update: true
                },
                type   : 'done'
            };

            self.userSearch = {
                'project.id': {
                    op : 'cnt',
                    val: null
                }
            };

            self.init = function (id, user, user_id, c, r, u, d) {
                self.right = {c,r,u,d};

                if (id) {
                    timesheetService.find(id, apiParams).then(function (res) {
                        self.timesheet  = res.object;

                        self.panelTitle = self.timesheet.name;

                        if (typeof self.timesheet.project !== 'undefined' && self.timesheet.project != null) {
                            self.userSearch['project.id'].val = self.timesheet.project.id;
                        }
                    });
                }

                if (typeof user === 'object') {
                    self.timesheet.user = user;
                }

                if (user_id) {
                    userService.find(user_id, {col: ['id', 'name', 'avatar', 'color']}).then(function (res) {
                        if (res.success) {
                            self.timesheet.user = res.object;
                        }
                    });
                }
            };

            $scope.$on('create-timesheet-modal', function (event, start, end, calendarName, user_id, project_id, modalName) {
                if(!self.right.c) return;

                self.calendarName = calendarName;
                self.timesheet    = {
                    start  : start,
                    end    : end,
                    _rights: {
                        update: true
                    },
                    type   : 'done'
                };

                if (user_id) {
                    userService.find(user_id, {col: ['id', 'name', 'avatar', 'color']}).then(function (res) {
                        if (res.success) {
                            self.timesheet.user = res.object;
                        }
                    });
                }

                if (project_id) {
                    projectService.find(project_id, {col: ['id', 'name']}).then(function (res) {
                        if (res.success) {
                            self.timesheet.project = res.object;
                        }
                    });
                }

                if (modalName) {
                    self.timesheetFormModalName = modalName;
                    $modal = angular.element('#' + modalName);
                }

                $modal.modal('show');
            });

            $scope.$on('edit-timesheet-modal', function (event, timesheet, calendarName) {
                self.calendarName = calendarName;
                self.timesheet    = angular.copy(timesheet);
                $modal.modal('show');
            });

            self.deleteTimesheetModal = function (timesheet) {
                if(!self.right.d) return;

                var calendarName = self.calendarName;
                if (confirm(translator('timesheet_question_delete').replace('%1', timesheet.name))) {
                    timesheetService.remove(timesheet).then(function (res) {
                        if (res.success) {
                            $rootScope.$broadcast('delete-timesheet-modal', timesheet, calendarName);
                            $modal.modal('hide');
                            flashMessenger.success(translator('timesheet_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.saveTimesheetModal = function () {
                if(!self.right.c) return;

                var calendarName   = self.calendarName;
                self.errors.fields = {};
                self.loading.save  = true;

                var isCreation = !self.timesheet.id;

                timesheetService.save(self.timesheet, apiParams).then(function (res) {
                    var timesheet = res.object;
                    $rootScope.$broadcast('save-timesheet-modal', timesheet, calendarName, isCreation);
                    $modal.modal('hide');
                    flashMessenger.success(translator('timesheet_message_saved'));
                    self.loading.save = false;
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                });
            };

            self.saveTimesheet = function saveTimesheet() {
                self.errors.fields = {};
                self.loading.save  = true;

                var isCreation = !self.timesheet.id;

                timesheetService.save(self.timesheet, apiParams).then(function (res) {
                    self.timesheet    = res.object;
                    self.panelTitle   = self.timesheet.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.timesheet.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('timesheet_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteTimesheet = function deleteTimesheet() {
                if (confirm(translator('timesheet_question_delete').replace('%1', self.timesheet.name))) {
                    timesheetService.remove(self.timesheet).then(function (res) {
                        if (res.success) {
                            window.location = '/timesheet';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.computeHours = function computeHours() {
                if (self.timesheet.start && self.timesheet.end) {
                    var start = moment(self.timesheet.start, 'DD/MM/YYYY H:m');
                    var end   = moment(self.timesheet.end, 'DD/MM/YYYY H:m');

                    var days  = end.diff(start, 'days', true);

                    if (self.settingsTime.hours && days >= 1) {
                        var hours = days * self.settingsTime.hours;
                    }
                    else {
                        var hours = end.diff(start, 'hours', true);
                    }
                    self.timesheet.hours = Math.round(hours * 100) / 100;
                }
            };

            self.controlDates = function controlDates() {
                if (self.timesheet.start && self.timesheet.end) {
                    var start = moment(self.timesheet.start, 'DD/MM/YYYY H:m');
                    var end   = moment(self.timesheet.end, 'DD/MM/YYYY H:m');

                    var hours = end.diff(start, 'hours', true);
                    if (hours < 0) {
                        self.errors.fields['end'] = [translator('timesheet_field_end_before_start_error')];
                    } else {
                        self.errors.fields['end'] = [];
                    }
                }
            };

            $scope.$watch(function () { return self.timesheet.end}, function () {
                self.controlDates();
            });

            $scope.$watch(function () { return self.timesheet.start}, function () {
                self.controlDates();
            });


            $scope.$on('create-timesheet', function () {
                if (typeof $scope.$parent.$parent.$parent.self.project !== 'undefined') {
                    self.timesheet.project = $scope.$parent.$parent.$parent.self.project;
                }
            });
        }
    ])

})(window.EVA.app);
