<?php

namespace Budget\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class IncomeFilters extends EntityFilters
{
	public function __construct(
		Translate $translator,
		MyModuleManager $moduleManager,
		EntityManager $entityManager,
		$incomeTypes,
        $allowedKeywordGroups
	) {
		parent::__construct($translator, $moduleManager, $entityManager);

		$typeOptions = [];
		foreach ($incomeTypes as $type) {
			$typeOptions[] = [
				'value' => $type,
				'text'  => $this->translator->__invoke('income_type_' . $type),
			];
		}

		$filters = [
			'name'            => [
				'label' => $this->translator->__invoke('income_field_name'),
				'type'  => 'string',
			],
			'amount'          => [
				'label' => $this->translator->__invoke('income_field_amount'),
				'type'  => 'number',
			],
			'type'            => [
				'label'   => $this->translator->__invoke('income_field_type'),
				'type'    => 'manytoone',
				'options' => $typeOptions,
			],
			'tiers'           => [
				'label' => $this->translator->__invoke('income_field_tiers'),
				'type'  => 'string',
			],
			'anneeExercice'   => [
				'label' => $this->translator->__invoke('income_field_anneeExercice'),
				'type'  => 'string',
			],
			'bordereau'       => [
				'label' => $this->translator->__invoke('income_field_bordereau'),
				'type'  => 'string',
			],
			'mandat'          => [
				'label' => $this->translator->__invoke('income_field_mandat'),
				'type'  => 'string',
			],
			'engagement'      => [
				'label' => $this->translator->__invoke('income_field_engagement'),
				'type'  => 'string',
			],
			'complement'      => [
				'label' => $this->translator->__invoke('income_field_complement'),
				'type'  => 'string',
			],
			'managementUnit' => [
				'label' => $this->translator->__invoke('income_field_managementUnit'),
				'type' => 'management-unit-select'
			],
			'serie'           => [
				'label' => $this->translator->__invoke('income_field_serie'),
				'type'  => 'string',
			],
			'convention'      => [
				'label' => $this->translator->__invoke('income_field_convention'),
				'type'  => 'string',
			],
			'conventionTitle' => [
				'label' => $this->translator->__invoke('income_field_conventionTitle'),
				'type'  => 'string',
			],
			'date'            => [
				'label'      => $this->translator->__invoke('income_field_date'),
				'type'       => 'date',
				'onlyPeriod' => true,
			],
			'poste.project' => [
				'label' => $this->translator->__invoke('poste_income_field_project'),
				'type' => 'project-select',
			],
			'createdAt' => [
				'label' => $this->translator->__invoke('income_field_createdAt'),
				'type' => 'date',
			],
			'updatedAt' => [
				'label' => $this->translator->__invoke('income_field_updatedAt'),
				'type' => 'date',
			],
		];

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'ht')) {
			$filters['amountHT'] = [
				'label' => $this->translator->__invoke('income_field_amountHT'),
				'type' => 'number',
			];
		}

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'evolution')) {
			$filters['evolution'] = [
				'label' => $this->translator->__invoke('income_field_evolution'),
				'type' => 'boolean',
			];
		}

		if ($this->moduleManager->isActive('keyword')) {
			$groups = $allowedKeywordGroups('income', false);;

			foreach ($groups as $group) {
				$filters['keyword.' . $group->getId()] = [
					'label' => $group->getName(),
					'type' => 'keyword-select',
					'group' => $group->getId(),
				];
			}
		}

		if ($this->moduleManager->isActive('age')) {
			$filters['age'] = [
				'label' => 'Importé via AGE',
				'type'  => 'boolean',
			];
		}

		$this->filters = $filters;
	}
}
