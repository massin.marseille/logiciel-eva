<?php

namespace User\Repository;

use Doctrine\ORM\EntityRepository;
use User\Entity\User;

/**
 * Class UserRepository
 *
 * @package User\Repository
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class UserRepository extends EntityRepository
{
    /**
     * @param string $login
     * @param string $password
     * @return null|User
     */
    public function authenticate($login, $password)
    {
        return $this->findOneBy([
            'login'    => $login,
            'password' => sha1($password)
        ]);
    }
}
