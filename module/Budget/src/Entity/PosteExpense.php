<?php

namespace Budget\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Project;
use Laminas\Validator\Callback;

/**
 * Class PosteExpense
 *
 * @package Budget\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="budget_poste_expense")
 */
class PosteExpense extends Poste
{
	const BALANCE_TOTAL_PAID = 'totalPaid';
	const BALANCE_BALANCE = 'balance';
	const BALANCE_COMMITTED_BALANCE = 'committedBalance';
	const BALANCE_AE_BALANCE = 'aeBalance';
	const BALANCE_CP_BALANCE = 'cpBalance';
	const BALANCE_AE_COMMITTED_BALANCE = 'aeCommittedBalance';
	const BALANCE_AE_COMMITTED_CP_COMMITTED_BALANCE = 'aeCommittedCpCommittedBalance';
	const BALANCE_AE_RATE_BALANCE = 'aeRateBalance';
	const BALANCE_CP_COMMITTED_AE_COMMITTED_RATE_BALANCE = 'cpCommittedAeCommittedRateBalance';
	const BALANCE_PLANNED_COMMITTED_PAID_BALANCE = 'plannedCommittedPaidBalance';


	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Budget\Entity\Expense", mappedBy="poste", cascade={"all"})
	 */
	protected $expenses;

	protected $expensesArbo;

	/**
	 * @var Project
	 *
	 * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="posteExpenses")
	 */
	protected $project;

	/**
	 * @var PosteExpense
	 *
	 * @ORM\ManyToOne(targetEntity="Budget\Entity\PosteExpense")
	 */
	protected $parent;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", options={"default": 100.0})
	 */
	protected $financing = 100.0;	// percent

	// For expense filter
	protected $filtered = false;

	/**
	 * @return boolean
	 */
	public function isFiltered()
	{
		return $this->filtered;
	}

	/**
	 * @param boolean $filtered
	 */
	public function setFiltered($filtered)
	{
		$this->filtered = $filtered;
	}

	/**
	 * @return PosteExpense
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param PosteExpense $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

	public function findParent()
	{
		if ($this->getProject()) {
			if ($this->getProject()->getParent()) {
				foreach ($this->getProject()
					->getParent()
					->getPosteExpenses()
					as $poste) {
					if ($poste->getAccount() === $this->getAccount()) {
						return $poste;
					}
				}
			}
		}

		return null;
	}

	public function getAmountArbo()
	{
		$amount = $this->getAmount();

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteExpenses() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountArbo();
					}
				}
			}
		}

		return $amount;
	}

	public function getAmountHTArbo()
	{
		$amount = $this->getAmountHT();

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteExpenses() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountHTArbo();
					}
				}
			}
		}

		return $amount;
	}

	public function getAmountAggreg()
	{
		$amount = 0;

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteExpenses() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountAggreg() + $poste->getAmount();
					}
				}
			}
		}

		return $amount;
	}

	public function getAmountHTAggreg()
	{
		$amount = 0;

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteExpenses() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountHTAggreg() + $poste->getAmountHT();
					}
				}
			}
		}

		return $amount;
	}

	public function setExpensesArbo($expenses)
	{
		$this->expensesArbo = $expenses;
	}

	/**
	 * @return ArrayCollection | Expense[]
	 */
	public function getExpensesArbo()
	{
		// For filter
		if ($this->expensesArbo) {
			return $this->expensesArbo;
		}

		$expenses = $this->expenses ? clone $this->expenses : new ArrayCollection();

		if ($this->getProject() && !$this->isFiltered()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteExpenses() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						foreach ($poste->getExpensesArbo() as $expense) {
							// With recursive arbo, we can have doublon
							if (!$expenses->contains($expense)) {
								$expenses->add($expense);
							}
						}
					}
				}
			}
		}

		return $expenses;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getExpenses()
	{
		return $this->expenses;
	}

	/**
	 * @param ArrayCollection $expenses
	 */
	public function setExpenses($expenses)
	{
		$this->expenses = $expenses;
	}

	/**
	 * @param ArrayCollection $expenses
	 */
	public function addExpenses(ArrayCollection $expenses)
	{
		foreach ($expenses as $expense) {
			$expense->setPoste($this);
			$this->getExpenses()->add($expense);
		}
	}

	/**
	 * @param ArrayCollection $expenses
	 */
	public function removeExpenses(ArrayCollection $expenses)
	{
		foreach ($expenses as $expense) {
			$expense->setPoste(null);
			$this->getExpenses()->removeElement($expense);
		}
	}

	public function getTotalExpenses($type, $year)
	{
		$amount = 0.0;
		if (null !== $this->expenses) {
			foreach ($this->expenses as $expense) {
				$expenseYear = intval($expense->getAnneeExercice());
				if ($type === $expense->getType() && $year === $expenseYear) {
					$amount += $expense->getAmount();
				}
			}
		}
		return $amount;
	}

	public function getAmountTotalPaid()
	{
		return $this->getAmountPaid() + $this->getAmountCommittedPaid();
	}

	public function getAmountHTTotalPaid()
	{
		return $this->getAmounHTPaid() + $this->getAmountHTCommittedPaid();
	}

	public function getAmountArboTotalPaid()
	{
		return $this->getAmountArboPaid() + $this->getAmountArboCommittedPaid();
	}

	public function getAmountHTArboTotalPaid()
	{
		return $this->getAmountHTArboPaid() + $this->getAmountHTArboCommittedPaid();
	}

	public function getAmountBalance()
	{
		return $this->getAmountPlanned() - ($this->getAmountCommitted() + $this->getAmountPaid() + $this->getAmountReleased());
	}

	public function getAmountHTBalance()
	{
		return $this->getAmountHTPlanned() - ($this->getAmountHTCommitted() +
			$this->getAmountHTPaid() +
			$this->getAmountHTReleased());
	}

	public function getAmountArboBalance()
	{
		return $this->getAmountArboPlanned() - ($this->getAmountArboCommitted() +
			$this->getAmountArboPaid() +
			$this->getAmountArboReleased());
	}

	public function getAmountHTArboBalance()
	{
		return $this->getAmountHTArboPlanned() - ($this->getAmountHTArboCommitted() +
			$this->getAmountHTArboPaid() +
			$this->getAmountHTArboReleased());
	}

	public function getAmountCommittedBalance()
	{
		return $this->getAmountCommitted() - $this->getAmountCommittedPaid();
	}

	public function getAmountHTCommittedBalance()
	{
		return $this->getAmountHTCommitted() - $this->getAmountHTCommittedPaid();
	}

	public function getAmountArboCommittedBalance()
	{
		return $this->getAmountArboCommitted() - $this->getAmountArboCommittedPaid();
	}

	public function getAmountHTArboCommittedBalance()
	{
		return $this->getAmountHTArboCommitted() - $this->getAmountHTArboCommittedPaid();
	}

	public function getAmountArboPlannedCommittedPaidBalance()
    {
        return $this->getAmountPlanned() - $this->getAmountCommittedPaid();
    }

    public function getAmountHTArboPlannedCommittedPaidBalance()
    {
        return $this->getAmountHTPlanned() - $this->getAmountHTCommittedPaid();
    }


	public function getAmountAeBalance()
	{
		return $this->getAmountAe() - $this->getAmountAeCommitted();
	}

	public function getAmountHTAeBalance()
	{
		return $this->getAmountHTAe() - $this->getAmountHTAeCommitted();
	}

	public function getAmountArboAeBalance()
	{
		return $this->getAmountArboAe() - $this->getAmountArboAeCommitted();
	}

	public function getAmountHTArboAeBalance()
	{
		return $this->getAmountHTArboAe() - $this->getAmountHTArboAeCommitted();
	}

	public function getAmountAeRateBalance()
	{
		return ($this->getAmountAe() > 0 ? $this->getAmountAeCommitted() * 100 / $this->getAmountAe() : 0.0);
	}

	public function getAmountHTAeRateBalance()
	{
		return ($this->getAmountHTAe() > 0 ? $this->getAmountHTAeCommitted() * 100 / $this->getAmountHTAe() : 0.0);
	}

	public function getAmountArboAeRateBalance()
	{
		return ($this->getAmountArboAe() > 0 ? $this->getAmountArboAeCommitted() * 100 / $this->getAmountArboAe() : 0.0);
	}

	public function getAmountHTArboAeRateBalance()
	{
		return ($this->getAmountHTArboAe() > 0 ? $this->getAmountHTArboAeCommitted() * 100 / $this->getAmountHTArboAe() : 0.0);
	}

	public function getAmountCpBalance()
	{
		return $this->getAmountCp() - $this->getAmountCpCommitted();
	}

	public function getAmountHTCpBalance()
	{
		return $this->getAmountHTCp() - $this->getAmountHTCpCommitted();
	}

	public function getAmountArboCpBalance()
	{
		return $this->getAmountArboCp() - $this->getAmountArboCpCommitted();
	}

	public function getAmountHTArboCpBalance()
	{
		return $this->getAmountHTArboCp() - $this->getAmountHTArboCpCommitted();
	}

	public function getAmountAeCommittedBalance()
	{
		return $this->getAmountAeEstimated() - $this->getAmountAeCommitted();
	}

	public function getAmountHTAeCommittedBalance()
	{
		return $this->getAmountHTAeEstimated() - $this->getAmountHTAeCommitted();
	}

	public function getAmountArboAeCommittedBalance()
	{
		return $this->getAmountArboAeEstimated() - $this->getAmountArboAeCommitted();
	}

	public function getAmountHTArboAeCommittedBalance()
	{
		return $this->getAmountHTArboAeEstimated() - $this->getAmountHTArboAeCommitted();
	}

	public function getAmountAeCommittedCPCommittedBalance()
	{
		return $this->getAmountAeCommitted() - $this->getAmountCpCommitted();
	}

	public function getAmountHTAeCommittedCPCommittedBalance()
	{
		return $this->getAmountHTAeCommitted() - $this->getAmountHTCpCommitted();
	}

	public function getAmountArboAeCommittedCPCommittedBalance()
	{
		return $this->getAmountArboAeCommitted() - $this->getAmountArboCpCommitted();
	}

	public function getAmountHTArboAeCommittedCPCommittedBalance()
	{
		return $this->getAmountHTArboAeCommitted() - $this->getAmountHTArboCpCommitted();
	}

	public function getAmountCpCommittedAeCommittedRateBalance()
	{
		$aeCommittedAmount = $this->getAmountAeCommitted();
		if (0 == $aeCommittedAmount) {	// no division by zero | '==' matching float and integer as well
			return 0.0;
		}
		return ($this->getAmountCpCommitted() / $aeCommittedAmount) * 100;
	}

	public function getAmountHTCpCommittedAeCommittedRateBalance()
	{
		$aeCommittedAmount = $this->getAmountHTAeCommitted();
		if (0 == $aeCommittedAmount) {	// no division by zero | '==' matching float and integer as well
			return 0.0;
		}
		return ($this->getAmountHTCpCommitted() / $aeCommittedAmount) * 100;
	}

	public function getAmountArboCpCommittedAeCommittedRateBalance()
	{
		$aeCommittedAmount = $this->getAmountArboAeCommitted();
		if (0 == $aeCommittedAmount) {	// no division by zero | '==' matching float and integer as well
			return 0.0;
		}
		return ($this->getAmountArboCpCommitted() / $aeCommittedAmount) * 100;
	}

	public function getAmountHTArboCpCommittedAeCommittedRateBalance()
	{
		$aeCommittedAmount = $this->getAmountHTArboAeCommitted();
		if (0 == $aeCommittedAmount) {	// no division by zero | '==' matching float and integer as well
			return 0.0;
		}
		return ($this->getAmountHTArboCpCommitted() / $aeCommittedAmount) * 100;
	}


	protected function getAmountInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$expenses = $this->getExpenses();

		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $expense->getEvolution()) {
					return false;
				}

				if ($evolution === 'only_evolutions' && !$expense->getEvolution()) {
					return false;
				}

				return $expense->getType() === $type && $expense->isYear($year);
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmount();
			}
		}
		return $amount;
	}

	protected function getPreviousYearsAmountInternal($type)
	{
		$amount = 0.0;
		$expenses = $this->getExpenses();
		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type) {
				return $expense->getType() === $type && $expense->belongsToPreviousYears();
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmount();
			}
		}
		return $amount;
	}

	protected function getAmountHTInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$expenses = $this->getExpenses();

		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $expense->getEvolution()) {
					return false;
				}

				if ($evolution === 'only_evolutions' && !$expense->getEvolution()) {
					return false;
				}

				return $expense->getType() === $type && $expense->isYear($year);
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmountHT();
			}
		}
		return $amount;
	}

	protected function getPreviousYearsAmountHTInternal($type)
	{
		$amount = 0.0;
		$expenses = $this->getExpenses();

		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type) {
				return $expense->getType() === $type && $expense->belongsToPreviousYears();
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmountHT();
			}
		}
		return $amount;
	}

	protected function getAmountArboInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$expenses = $this->getExpensesArbo();

		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $expense->getEvolution()) {
					return false;
				}

				if ($evolution === 'only_evolutions' && !$expense->getEvolution()) {
					return false;
				}

				return $expense->getType() === $type && $expense->isYear($year);
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmount();
			}
		}
		return $amount;
	}


	protected function getPreviousYearsAmountArboInternal($type)
	{
		$amount = 0.0;
		$expenses = $this->getExpensesArbo();

		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type) {
				return $expense->getType() === $type && $expense->belongsToPreviousYears();
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmount();
			}
		}
		return $amount;
	}

	protected function getAmountHTArboInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$expenses = $this->getExpensesArbo();

		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $expense->getEvolution()) {
					return false;
				}

				if ($evolution === 'only_evolutions' && !$expense->getEvolution()) {
					return false;
				}

				return $expense->getType() === $type && $expense->isYear($year);
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmountHT();
			}
		}
		return $amount;
	}

	protected function getPreviousYearsAmountHTArboInternal($type)
	{
		$amount = 0.0;
		$expenses = $this->getExpensesArbo();

		if ($expenses) {
			$filteredExpenses = $expenses->filter(function ($expense) use ($type) {
				return $expense->getType() === $type && $expense->belongsToPreviousYears();
			});

			foreach ($filteredExpenses as $expense) {
				$amount += $expense->getAmountHT();
			}
		}
		return $amount;
	}

	public function __call($name, $arguments)
	{
		foreach (Expense::getTypes() as $type) {
			if (
				$name === 'getAmount' . ucfirst($type) ||
				$name === 'getPreviousYearsAmount' . ucfirst($type) ||
				$name === 'getAmountHT' . ucfirst($type) ||
				$name === 'getPreviousYearsAmountHT' . ucfirst($type) ||
				$name === 'getAmountArbo' . ucfirst($type) ||
				$name === 'getPreviousYearsAmountArbo' . ucfirst($type) ||
				$name === 'getAmountHTArbo' . ucfirst($type) ||
				$name === 'getPreviousYearsAmountHTArbo' . ucfirst($type)
			) {
				$method = str_replace(ucfirst($type), '', $name) . 'Internal';
				array_unshift($arguments, $type);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

		return null;
	}

	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilter = parent::getInputFilter($entityManager);

		$inputFilter->add([
			'name' => 'parent',
			'required' => false,
		]);

		return $inputFilter;
	}

	/**
	 * @return array
	 */
	public static function getBalances()
	{
		return [
			self::BALANCE_TOTAL_PAID,
			self::BALANCE_BALANCE,
			self::BALANCE_COMMITTED_BALANCE,
			self::BALANCE_AE_BALANCE,
			self::BALANCE_CP_BALANCE,
			self::BALANCE_AE_COMMITTED_BALANCE,
			self::BALANCE_AE_COMMITTED_CP_COMMITTED_BALANCE,
			self::BALANCE_AE_RATE_BALANCE,
			self::BALANCE_CP_COMMITTED_AE_COMMITTED_RATE_BALANCE,
            self::BALANCE_PLANNED_COMMITTED_PAID_BALANCE
		];
	}

	/**
	 * Get the value of financing
	 *
	 * @return  float
	 */
	public function getFinancing()
	{
		return $this->financing;
	}

	/**
	 * Set the value of financing
	 *
	 * @param  float  $financing
	 *
	 * @return  self
	 */
	public function setFinancing($financing)
	{
		$this->financing = $financing;
		return $this;
	}
}
