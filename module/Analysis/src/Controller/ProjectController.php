<?php

namespace Analysis\Controller;

use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;

class ProjectController extends AbstractActionSLController
{
    public function onDispatch(MvcEvent $e)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        return parent::onDispatch($e);
    }

    public function indexAction()
    {

    }

    // Only export
    public function projectsAction()
    {
        $isMaster = $this->environment()->getClient()->isMaster();
        $filters  = $this->params()->fromQuery('filters', []);

        $projects = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
            'col'    => [
                'id',
                'name',
                'actors.structure.id',
            ],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort'    => 'name',
                    'order'   => 'asc',
                    'filters' => $filters,
                ],
            ],
            'master' => $isMaster
        ])->dispatch()->getVariable('rows');

        $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
        $translator = $helperPluginManager->get('translate');

        $xlsLines = [];

        $header = [];
        if($isMaster){
            $header[] = ucfirst($translator->__invoke('client'));
            $header[] = ucfirst($translator->__invoke('project_entities'));
            $header[] = ucfirst($translator->__invoke('actor_entities'));
        }

        $header[] = ucfirst($translator->__invoke('project_entity'));
        $xlsLines[] = $header;

        $clients = [];
        $clientsLines = [];
        foreach ($projects as $project) {
            if ($isMaster && !array_key_exists($project['_client']['id'], $clientsLines)) {
                $clients[$project['_client']['id']] = [
                    'name'     => $project['_client']['name'],
                    'projects' => 0,
                    'actors'   => 0,
                    '_actors'  => []
                ];
                $clientsLines[$project['_client']['id']] = [];
            }

            $projectLines = [];

            $projectLine = [];
            if ($isMaster) {
                $clients[$project['_client']['id']]['projects'] += 1;

                foreach ($project['actors'] as $actor) {
                    if (!in_array($actor['structure']['id'], $clients[$project['_client']['id']]['_actors'])) {
                        $clients[$project['_client']['id']]['actors'] += 1;
                        $clients[$project['_client']['id']]['_actors'][] = $actor['structure']['id'];
                    }
                }

                // colonne client
                $projectLine[] = '';
                // colonnes projects
                $projectLine[] = '';
                // colonnes actors
                $projectLine[] = sizeOf($project['actors']);
            }
            $projectLine[]  = $project['name'];
            $projectLines[] = $projectLine;


            if ($isMaster) {
                $clientsLines[$project['_client']['id']] = array_merge($clientsLines[$project['_client']['id']], $projectLines);
            } else {
                $clientsLines = array_merge($clientsLines, $projectLines);
            }
        }

        if ($isMaster) {
            foreach($clientsLines as $client => $lines) {
                $xlsLines[] = [$clients[$client]['name'], $clients[$client]['projects'], $clients[$client]['actors']];
                $xlsLines = array_merge($xlsLines, $lines);
            }
        } else {
            $xlsLines = array_merge($xlsLines, $clientsLines);
        }

        ExcelExporter::download($xlsLines);
    }
}
