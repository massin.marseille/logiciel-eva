(function(app) {
	app.directive('projectUpdateHistory', [
		'projectUpdateService',
		function (projectUpdateService) {
		return {
			restrict: 'EA',
			require: '^?project-updates-history',
			replace: false,
			transclude: false,
			scope: {
				primary: '=primary',
			},
			templateUrl: '/js/src/project/template/project-update-history.html',
			compile: function ($element, attrs) {
				return function link ($scope) {
					var $modal = angular.element('#projectUpdateHistoryModal');


					$scope.showUpdateHistoryModal = function () {
						$scope.isLoading = true;

						$modal.modal('show');
						projectUpdateService.findByProject($scope.primary)
						.then(
							function (history) {
								$scope.history = history;
								$scope.isLoading = false;
							 },
						);
					};

				}
			}
		}
	}])
})(window.EVA.app);
