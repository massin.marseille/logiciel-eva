<?php

namespace Map\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Map\Entity\Association;
use Map\Entity\Territory;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Model\JsonModel;

class TerritoryController extends BasicRestController
{
    protected $entityClass = 'Map\Entity\Territory';
    protected $repository  = 'Map\Entity\Territory';
    protected $entityChain = 'map:e:territory';

    public function onDispatch(MvcEvent $event)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);
        return parent::onDispatch($event);
    }

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }
        $queryBuilder->addOrderBy($sort, $order);

        $queryBuilder->leftJoin('object.children', 'children');

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                    'object.code LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    public function fusionAction()
    {
        $territoryToKeep_id = $this->params()->fromRoute('id');
        $territories_id     = $this->params()->fromPost('territories_id', null);

        if (
            !$territories_id ||
            count($territories_id) === 0 ||
            !$this->acl()->isAllowed('map:e:territory:d')
        ) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Territory $territoryToKeep */
        $territoryToKeep = $this->getEntityManager()
            ->getRepository('Map\Entity\Territory')
            ->find($territoryToKeep_id);

        if (!$territoryToKeep) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Association[] $associationsToKeep */
        $associationsToKeep = $this->getEntityManager()
            ->getRepository('Map\Entity\Association')
            ->findBy(['territory' => $territoryToKeep]);

        /** @var Association[] $associationsToChange */
        $associationsToChange = $this->getEntityManager()
            ->getRepository('Map\Entity\Association')
            ->findBy(['territory' => $territories_id]);

        foreach ($associationsToChange as $association) {
            foreach ($associationsToKeep as $associationToKeep) {
                if (
                    $associationToKeep->getEntity() === $association->getEntity() &&
                    $associationToKeep->getPrimary() === $association->getPrimary()
                ) {
                    $this->getEntityManager()->remove($association);
                    continue 2;
                }
            }

            $association->setTerritory($territoryToKeep);
            $this->getEntityManager()->persist($association);
        }

        $territoriesToRemove = $this->getEntityManager()
            ->getRepository('Map\Entity\Territory')
            ->findById($territories_id);

        foreach ($territoriesToRemove as $territory) {
            if ($territory->getId() === $territoryToKeep->getId()) {
                continue;
            }

            $this->getEntityManager()->remove($territory);
        }

        $this->getEntityManager()->flush();
        return new JsonModel([
            'success' => true,
        ]);
    }
}
