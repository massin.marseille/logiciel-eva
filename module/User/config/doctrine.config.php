<?php

namespace User;

return [
    'doctrine' => [
        'driver' => [
            'user_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'user_entities2' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_default' => [
                'drivers' => [
                    'User\Entity' => 'user_entities2'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'User\Entity' => 'user_entities'
                ]
            ]
        ],
    ]
];
