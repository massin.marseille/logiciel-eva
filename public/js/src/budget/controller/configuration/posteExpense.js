(function (app) {

    app.controller('posteExpenseReferentialController', [
        'budgetConfiguration', 'flashMessenger', 'translator', '$scope',
        function posteExpenseReferentialController(budgetConfiguration, flashMessager, translator, $scope) {
            self = this;

            self.categories = [];
            self.accounts = [];
            self.groups = [];

            self.init = function () {
                budgetConfiguration.getCategories()
                    .then(function (res) {
                        self.categories = res;
                    });

                budgetConfiguration.getAccounts()
                    .then(function (res) {
                        self.accounts = res;
                    });

                budgetConfiguration.getPostesExpense()
                    .then(function (res) {
                        self.groups = res;
                    });
            };

            self.createGroup = function () {
                if (undefined === self.groups || null === self.groups) {
                    self.groups = [];
                }
                self.groups.unshift({
                    id: null,
                    name: null,
                    category: null,
                    children: [],
                    parent: null,
                    edit: true
                });
            };

            self.createPoste = function (group) {
                if (undefined === group.children || null === group.children) {
                    group.children = [];
                }
                group.children.unshift({
                    id: null,
                    name: null,
                    category: null,
                    parent: group.id,
                    children: null,
                    edit: true
                });
                group.open = true;
            };

            self.save = function (item) {
                budgetConfiguration.savePosteExpense(item)
                    .then(function (res) {
                        item.id = res.id;   // for newly created items
                        item.edit = false;
                        flashMessager.success(translator('poste_expense_message_saved'));
                    }, function (err) {
                        console.error(err);
                        flashMessager.error(translator('error_occured'));
                    });
            };

            self.deleteGroup = function (group, index) {
                if (confirm(translator('poste_expense_question_delete').replace('%1', group.name))) {
                    budgetConfiguration.deletePosteExpense(group.id)
                        .then(function () {
                            self.groups.splice(index, 1);
                            flashMessager.success(translator('poste_expense_message_deleted'));
                        }, function (err) {
                            console.error(err);
                            flashMessager.error(translator('error_occured'));
                        });
                }
            };

            self.deletePoste = function (poste, index, parent) {
                if (confirm(translator('poste_expense_question_delete').replace('%1', poste.name))) {
                    budgetConfiguration.deletePosteExpense(poste.id)
                        .then(function () {
                            parent.children.splice(index, 1);
                            flashMessager.success(translator('poste_expense_message_deleted'));
                        }, function (err) {
                            console.error(err);
                            flashMessager.error(translator('error_occured'));
                        });
                }
            };

        }
    ]);

})(window.EVA.app);