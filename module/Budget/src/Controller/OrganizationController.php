<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class OrganizationController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:organization:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('budget:e:organization', $id)) {
            return $this->notFoundAction();
        }

        $organization = $this->entityManager()
            ->getRepository('Budget\Entity\Organization')
            ->find($id);

        if (
            $organization &&
            !$this->acl()->ownerControl('budget:e:organization:u', $organization) &&
            !$this->acl()->ownerControl('budget:e:organization:r', $organization)
        ) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'organization' => $organization,
        ]);
    }
}
