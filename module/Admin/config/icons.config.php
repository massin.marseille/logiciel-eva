<?php

return [
    'icons' => [
        'clients' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-wrench'
        ],
        'networks' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-sitemap'
        ],
    ]
];
