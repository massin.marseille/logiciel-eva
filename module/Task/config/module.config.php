<?php

return [
    'module' => [
        'task' => [
            'environments' => [
                'client'
            ],
            'active'   => true,
            'required' => false,
            'acl' => [
                'entities' => [
                    [
                        'name'     => 'task',
                        'class'    => 'Task\Entity\Task',
                        'keywords' => true,
                        'owner'    => ['own', 'service']
                    ]
                ]
            ],
            'configuration' => [
                'kanban' => [
                    'type'    => 'boolean',
                    'text'    => 'task_kanban_configuration'
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
