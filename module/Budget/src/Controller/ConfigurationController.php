<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class ConfigurationController extends AbstractActionSLController 
{

    public function indexAction()
    {
        if(!$this->acl()->isAllowed('budget:e:posteExpenseReferential:r')) {
            return $this->notFoundAction();
        }
        return new ViewModel();
    }

}