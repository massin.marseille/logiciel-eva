(function(app) {

    app.controller('taskListController', [
        'taskService', '$scope', 'flashMessenger', 'translator', '$timeout',
        function taskListController(taskService, $scope, flashMessenger, translator, $timeout) {
            var self = this;

            self.currentUser = null;
            self.view = 'list';
            self.tasks   = [];
            self.options = {
                name: 'tasks-list',
                col: [
                    'id',
                    'name',
                    'start',
                    'end',
                    'progress',
                    'project.id',
                    'project.name',
                    'users.id',
                    'users.name',
                    'users.avatar',
                    'description',
                    'keywords',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'start',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    if (self.query != null) {
                        params.search.data.filters = self.query.filters;
                    }

                    if (self.from.type == 'project') {
                        if (self.from.object.id || self.from.id) {
                            params.search.data.filters['project'] = {
                                op: 'eq',
                                val: self.from.object.id || self.from.id
                            };
                        }
                    }

                    taskService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    });
                    params.search.data.nbFilters = Object.keys(params.search.data.filters).length;
                }
            };

            self.from   = {
                type: null,
                id: null,
                object: null
            };

            self.init = function init(currentUser, from, id) {
                self.currentUser = currentUser;

                self.from.type = from;
                self.from.id   = id;
                if (self.from.type == 'project') {
                    self.from.object = $scope.$parent.self[self.from.type];
                    self.options.pager = false;
                }

                if (self.currentUser && self.from.type !== 'project') {
                    $timeout(function () {
                        $scope.load({
                            filters: {
                                'users.id': {
                                    op: 'eq',
                                    val: [self.currentUser]
                                }
                            }
                        });
                        self.query = null;
                    }, 1000);

                }
            };

            self.initHome = function (name, query) {
                self.options.name = name;
                self.query = query;
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(task) {
                if (angular.isArray(task.keywords) || task.keywords == null) {
                    task.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = task;
                self.editedLineFields = angular.copy(task);
            };

            self.saveEditedLine = function saveEditedLine(task) {
                self.editedLineErrors = {};
                taskService.save(angular.extend({}, task, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        task = angular.extend(task, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('task_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                });
            };

            self.createTask = function () {
                $scope.$broadcast('create-task', {user: self.currentUser, project: self.from.object ? self.from.object.id : null});
            };

            self.editTask = function editTask(task) {
                $scope.$broadcast('edit-task', task);
            };

            self.deleteTask = function deleteTask(task) {
                if (confirm(translator('task_question_delete').replace('%1', task.name))) {
                    taskService.remove(task).then(function (res) {
                        if (res.success) {
                            self.tasks.splice(self.tasks.indexOf(task), 1);
                            flashMessenger.success(translator('task_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.isMember = function isMember(user) {
                for (var i in $scope.$parent.$parent.self.project.members) {
                    var member = $scope.$parent.$parent.self.project.members[i];
                    if (member.id && member.user.id == user) {
                        return true;
                    }
                }

                return false;
            };

            $scope.$watch(function () {
                return self.view
            }, function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    self.options.pager = self.view === 'list';
                    $scope.__tb.loadData();
                }
            });


        }
    ])

})(window.EVA.app);
