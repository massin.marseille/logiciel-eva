<?php

namespace Core\Command;

use Bootstrap\Environment\Client\ClientCreator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateLanguage extends AbstractCommand
{

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[START] Generating JS client language files...');

        $em      = $this->getEmDefault();
        $clients = $em->getRepository('Bootstrap\Entity\Client')->findAll();

        foreach ($clients as $client) {
            $creator = new ClientCreator($client, $this->serviceLocator);
            $creator->createJSLanguage();
        }

        $output->writeln('[END] Generating JS client language files...');
        return Command::SUCCESS;
    }
}
