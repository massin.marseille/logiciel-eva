<?php

namespace Time\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class AbsenceType
 *
 * @package Time\AbsenceType
 *
 * @ORM\Entity
 * @ORM\Table(name="time_absence_type")
 */
class AbsenceType extends BasicRestEntityAbstract {
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    protected $initialLetters;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getInitialLetters()
    {
        return $this->initialLetters;
    }

    /**
     * @param string $initialLetters
     */
    public function setInitialLetters($initialLetters)
    {
        $this->initialLetters = $initialLetters;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter        = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $queryBuilder = $entityManager->createQueryBuilder();
                                $queryBuilder->select('o')
                                             ->from('Time\Entity\AbsenceType', 'o')
                                             ->where('o.name = :name')
                                             ->andWhere('o.id != :id')
                                             ->setParameters([
                                                 'id'   => !isset($context['id']) ? '' : $context['id'],
                                                 'name' => $value,
                                             ]);

                                try {
                                    $absenceType = $queryBuilder->getQuery()->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $absenceType = null;
                                }
                                return $absenceType === null;
                            },
                            'message'  => 'time_absence_type_field_name_error_unique',
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'initialLetters',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $queryBuilder = $entityManager->createQueryBuilder();
                                $queryBuilder->select('o')
                                             ->from('Time\Entity\AbsenceType', 'o')
                                             ->where('o.initialLetters = :initialLetters')
                                             ->andWhere('o.id != :id')
                                             ->setParameters([
                                                 'id'   => !isset($context['id']) ? '' : $context['id'],
                                                 'initialLetters' => $value,
                                             ]);

                                try {
                                    $absenceType = $queryBuilder->getQuery()->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $absenceType = null;
                                }
                                return $absenceType === null;
                            },
                            'message'  => 'time_absence_type_field_initialLetters_error_unique',
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}