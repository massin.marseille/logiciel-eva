(function(app) {
	app.factory('budgetCodeService', [
		'restFactoryService',
		restFactoryService => restFactoryService.create('/api/budget/code'),
	]);
})(window.EVA.app);
