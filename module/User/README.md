# User Module

Le module User est responsable de gérer les utilisateurs d'EVA, d'une par les CRUD des objets `User` et d'autre par l'authentification.
**Ce module est partagé dans les différents environnements.**

## Authentification

L'authentification se fait via le controller `AuthController`. Une fois que l'utilisateur a saisi ses identifiants, et qu'ils ont été validés, l'objet `User` est passé à l'`AuthStorage`.

L'`AuthStorage` est un service qui est initialisé via un `EntityManager` et une `Session`.
L'`EntityManager` et la `Session` doivent être ceux de l'environnement courant (voir module Bootstrap).
L'identifiant de l'utiliateur est stocké dans la `Session` sous la clé "user".

## Droits d'accès

Le contrôle de droits d'accès (ACL) est géré par la classe `Acl` (`User\Auth\Acl`).
Chaque règle est une chaine formattée de type "module:type:clef:droit".
Exemples : 
- "user:e:user:r" = module : user, type: entité, clef: user, droit: voir (droit de voir un user)
- "user:e:role:c" = module : user, type: entité, clef: role, droit: créer (droit de créer un user)
- "user:a:validate" = module : user, type: action, clef: validate (droit d'effectuer l'action "validate")

Les droits sont découpés en 2 types : 
- "e" : les entités avec les droits c/r/u/d
- "a" : les actions (pas de droit spécifique car uniquement oui/non)

Il n'y a aucun système automatisé pour le contrôle d'accès au niveau des actions/controllers, ceci permet d'être aussi fin que possible dans le contrôle.

Le stockage de ses règles se trouve dans l'entité `Role` lié au `User` (propriété `$rights` de type `array` stocké sous forme de json)
Exemple :
```php
    [
        'user:e:user:r' => [
            'value' => true
        ],
        'user:e:role:c' => [
            'value' => false
        ],
    ]
```

Nous partons du principe que pour avoir le droit de C/U/D, alors le droit R est obligatoire.

Voir le module `Core` pour la définition des droits.

## Aide

### L'utilisateur est-t-il connecté ?

- Dans un controller : `$this->authStorage()->isAuthenticated()`
- Dans une vue : `$this->authStorage()->isAuthenticated()`

### Récupérer l'utilisateur (objet) :

- Dans un controller : `$this->authStorage()->getUser()`
- Dans une vue : `$this->authStorage()->getUser()`

### Stocker un utilisateur :

- Dans un controller : `$this->authStorage()->store($user)`
- Dans une vue : ceci ne devrait jamais arriver !


### Reinitialiser l'AuthStorage (déconnecter l'utilisateur) :

- Dans un controller : `$this->authStorage()->clear()`
- Dans une vue : ceci ne devrait jamais arriver !

### Est-ce que l'utilisateur a le droit d'accès à la règle 'user:e:user:u' :

- Dans un controller : `$this->acl()->isAllowed('user:e:user:u')`
- Dans une vue : `$this->acl()->isAllowed('user:e:user:u')`

### Droit d'accès basique sur un formulaire entité (droit de lecture sans id, droit de mise à jour uniquement avec id)

- Dans un controller : `$this->acl()->basicFormAccess('user:e:user')`
- Dans une vue : `$this->acl()->basicFormAccess('user:e:user')`
