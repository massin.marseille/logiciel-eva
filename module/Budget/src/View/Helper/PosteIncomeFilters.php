<?php

namespace Budget\View\Helper;

use Budget\Entity\Account;
use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class PosteIncomeFilters extends EntityFilters
{
	public function __construct(
		Translate $translator,
		MyModuleManager $moduleManager,
		EntityManager $entityManager,
        $allowedKeywordGroups
	) {
		parent::__construct($translator, $moduleManager, $entityManager);

		$filters = [
			'name' => [
				'label' => $this->translator->__invoke('poste_income_field_name'),
				'type' => 'string',
			],
			'account' => [
				'label' => $this->translator->__invoke('poste_income_field_account'),
				'type' => 'account-select',
				'accountType' => Account::TYPE_INCOME,
			],
			'nature' => [
				'label' => $this->translator->__invoke('poste_income_field_nature'),
				'type' => 'nature-select',
			],
			'organization' => [
				'label' => $this->translator->__invoke('poste_income_field_organization'),
				'type' => 'organization-select',
			],
			'destination' => [
				'label' => $this->translator->__invoke('poste_income_field_destination'),
				'type' => 'destination-select',
			],
			'envelope.financer' => [
				'label' => $this->translator->__invoke('envelope_field_financer'),
				'type' => 'financer-select',
			],
			'envelope' => [
				'label' => $this->translator->__invoke('poste_income_field_envelope'),
				'type' => 'envelope-select',
			],
			'amount' => [
				'label' => $this->translator->__invoke('poste_income_field_amount'),
				'type' => 'number',
			],
			'caduciteStart' => [
				'label' => $this->translator->__invoke('poste_income_field_caduciteStart'),
				'type' => 'date',
			],
			'caduciteEnd' => [
				'label' => $this->translator->__invoke('poste_income_field_caduciteEnd'),
				'type' => 'date',
			],
			'caduciteSendProof' => [
				'label' => $this->translator->__invoke('poste_income_field_caduciteSendProof'),
				'type' => 'date',
			],
			'folderSendingDate' => [
				'label' => $this->translator->__invoke('poste_income_field_folderSendingDate'),
				'type' => 'date',
			],
			'folderReceiptDate' => [
				'label' => $this->translator->__invoke('poste_income_field_folderReceiptDate'),
				'type' => 'date',
			],
			'deliberationDate' => [
				'label' => $this->translator->__invoke('poste_income_field_deliberationDate'),
				'type' => 'date',
			],
			'arrDate' => [
				'label' => $this->translator->__invoke('poste_income_field_arrDate'),
				'type' => 'date',
			],
			'arrNumber' => [
				'label' => $this->translator->__invoke('poste_income_field_arrNumber'),
				'type' => 'string',
			],
			'project' => [
				'label' => $this->translator->__invoke('poste_income_field_project'),
				'type' => 'project-select',
			],
			'createdAt' => [
				'label' => $this->translator->__invoke('poste_income_field_createdAt'),
				'type' => 'date',
			],
			'updatedAt' => [
				'label' => $this->translator->__invoke('poste_income_field_updatedAt'),
				'type' => 'date',
			],
		];

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'ht')) {
			$filters['amountHT'] = [
				'label' => $this->translator->__invoke('poste_income_field_amountHT'),
				'type' => 'number',
			];
		}

		if ($this->moduleManager->getClientModuleConfiguration('budget', 'gbcp')) {
			$filters['nature'] = [
				'label' => $this->translator->__invoke('poste_income_field_nature'),
				'type' => 'nature-select',
			];
			$filters['organization'] = [
				'label' => $this->translator->__invoke('poste_income_field_organization'),
				'type' => 'organization-select',
			];
			$filters['destination'] = [
				'label' => $this->translator->__invoke('poste_income_field_destination'),
				'type' => 'destination-select',
			];
		}

		if ($this->moduleManager->isActive('keyword')) {
			$groups = $allowedKeywordGroups('posteIncome', false);;

			foreach ($groups as $group) {
				$filters['keyword.' . $group->getId()] = [
					'label' => $group->getName(),
					'type' => 'keyword-select',
					'group' => $group->getId(),
				];
			}
		}

		$this->filters = $filters;
	}
}
