(function(app) {
    app.factory('organizationService', [
        'restFactoryService',
        function (restFactoryService) {
            return restFactoryService.create('/api/budget/organization');
        },
    ]);
})(window.EVA.app);
