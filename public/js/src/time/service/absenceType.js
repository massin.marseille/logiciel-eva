(function(app) {

    app.factory('absenceTypeService', [
        'restFactoryService',
        function absenceTypeService(restFactoryService) {
            return restFactoryService.create('/api/timesheet/absence-type');
        }
    ])

})(window.EVA.app);
