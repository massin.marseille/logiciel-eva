<?php

namespace Core\Command;

use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateDatabases extends AbstractCommand
{
    public function getDefaultCommandName(): string
    {
        return 'update-dbs';
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[INFO] Updating admin database...');

        exec(getcwd() . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'doctrine-module orm:schema-tool:update -f');

        $output->writeln('[INFO] Updating clients database...');

        $em                   = $this->getEmDefault();
        $entityManagerFactory = $this->getEmFactory();

        $icons        = $this->serviceLocator->get('Config')['icons'];
        $translations = $this->serviceLocator->get('Translator')->getAllMessages()->getArrayCopy();

        $clients = $em->getRepository('Bootstrap\Entity\Client')->findAll();
        foreach ($clients as $client) {
            $output->writeln('[INFO] Starting update client - ' . $client->getName());

            $clientEm   = $entityManagerFactory->getEntityManager($client);
            $schemaTool = new SchemaTool($clientEm);

            try {
                $metadatas = $clientEm->getMetadataFactory()->getAllMetadata();
                $destPath  = $em->getConfiguration()->getProxyDir();
                $output->writeln('[INFO] Starting generateProxyClasses - ' . $client->getName());

                $clientEm->getProxyFactory()->generateProxyClasses($metadatas, $destPath);
                $output->writeln('[INFO] Starting updateSchema - ' . $client->getName());
                $schemaTool->updateSchema($metadatas);
            } catch (\Exception $e) {
                $output->writeln('[ERROR] Fail on ' . __FILE__ . ':' . __LINE__ . ' - ' . $e->getMessage());
            }

            $clientIcons        = $client->getIcons();
            $clientTranslations = $client->getTranslations();

            foreach ($clientIcons as $key => $value) {
                if (!array_key_exists($key, $icons) || $value == $icons[$key]) {
                    unset($clientIcons[$key]);
                }
            }

            foreach ($clientTranslations as $key => $value) {
                if (!array_key_exists($key, $translations) || $value == $translations[$key]) {
                    unset($clientTranslations[$key]);
                }
            }

            $client->setIcons($clientIcons);
            $client->setTranslations($clientTranslations);
        }

        $em->flush();
        $output->writeln('[INFO] Updating End');

        return Command::SUCCESS;
    }
}
