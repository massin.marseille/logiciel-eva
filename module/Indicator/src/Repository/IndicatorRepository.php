<?php

namespace Indicator\Repository;

use Doctrine\ORM\EntityRepository;
use Indicator\Entity\Indicator;

/**
 * Class IndicatorRepository
 *
 * @package Indicator\Repository
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class IndicatorRepository extends EntityRepository
{
    public function findTransverse()
    {
        $queryBuilder = $this->createQueryBuilder('i');
        $queryBuilder->select('i')
                     ->innerJoin('i.deployments', 'deployments');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findTransverseByService($user)
    {

        $queryBuilder = $this->createQueryBuilder('object');
        $queryBuilder->select('object')
            ->innerJoin('object.deployments', 'deployments');

        Indicator::ownerControlDQL($queryBuilder, $user, 'service');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findByService($user)
    {
        $queryBuilder = $this->createQueryBuilder('object');
        $queryBuilder->select('object');

        Indicator::ownerControlDQL($queryBuilder, $user, 'service');

        return $queryBuilder->getQuery()->getResult();
    }
}
