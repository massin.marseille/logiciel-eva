<?php

return [
    'icons' => [
        'indicator' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-tachometer',
        ],
        'measure' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-signal',
        ],
        'deployment' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-wifi',
        ],
        'groupindicator' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-cubes',
        ]
    ]
];
