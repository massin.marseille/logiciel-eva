<?php

namespace Age\Model;

use JsonSerializable;

class AgeOperation implements JsonSerializable
{

    private $referenceExterne;
    private $codeOperation;
    private $codeOperationParent;
    private $referenceExterneOperationParent;
    private $libelle;
    private $typeOperation;
    private $saisissableBudget;
    private $presenteeBudget;
    private $saisissableExecution;
    private $dateDebut;
    private $dateFin;
    private $limitativite;
    private $montantGlobal;
    private $description;


    /**
     * Get the value of referenceExterne
     */
    public function getReferenceExterne()
    {
        return $this->referenceExterne;
    }

    /**
     * Set the value of referenceExterne
     *
     * @return  self
     */
    public function setReferenceExterne($referenceExterne)
    {
        $this->referenceExterne = $referenceExterne;

        return $this;
    }

    /**
     * Get the value of codeOperation
     */
    public function getCodeOperation()
    {
        return $this->codeOperation;
    }

    /**
     * Set the value of codeOperation
     *
     * @return  self
     */
    public function setCodeOperation($codeOperation)
    {
        $this->codeOperation = $codeOperation;

        return $this;
    }

    /**
     * Get the value of codeOperationParent
     */
    public function getCodeOperationParent()
    {
        return $this->codeOperationParent;
    }

    /**
     * Set the value of codeOperationParent
     *
     * @return  self
     */
    public function setCodeOperationParent($codeOperationParent)
    {
        $this->codeOperationParent = $codeOperationParent;

        return $this;
    }

    /**
     * Get the value of referenceExterneOperationParent
     */
    public function getReferenceExterneOperationParent()
    {
        return $this->referenceExterneOperationParent;
    }

    /**
     * Set the value of referenceExterneOperationParent
     *
     * @return  self
     */
    public function setReferenceExterneOperationParent($referenceExterneOperationParent)
    {
        $this->referenceExterneOperationParent = $referenceExterneOperationParent;

        return $this;
    }

    /**
     * Get the value of libelle
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get the value of typeOperation
     */
    public function getTypeOperation()
    {
        return $this->typeOperation;
    }

    /**
     * Set the value of typeOperation
     *
     * @return  self
     */
    public function setTypeOperation($typeOperation)
    {
        $this->typeOperation = $typeOperation;

        return $this;
    }

    /**
     * Get the value of saisissableBudget
     */
    public function getSaisissableBudget()
    {
        return $this->saisissableBudget;
    }

    /**
     * Set the value of saisissableBudget
     *
     * @return  self
     */
    public function setSaisissableBudget($saisissableBudget)
    {
        $this->saisissableBudget = $saisissableBudget;

        return $this;
    }

    /**
     * Get the value of presenteeBudget
     */
    public function getPresenteeBudget()
    {
        return $this->presenteeBudget;
    }

    /**
     * Set the value of presenteeBudget
     *
     * @return  self
     */
    public function setPresenteeBudget($presenteeBudget)
    {
        $this->presenteeBudget = $presenteeBudget;

        return $this;
    }

    /**
     * Get the value of saisissableExecution
     */
    public function getSaisissableExecution()
    {
        return $this->saisissableExecution;
    }

    /**
     * Set the value of saisissableExecution
     *
     * @return  self
     */
    public function setSaisissableExecution($saisissableExecution)
    {
        $this->saisissableExecution = $saisissableExecution;

        return $this;
    }

    /**
     * Get the value of dateDebut
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set the value of dateDebut
     *
     * @return  self
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get the value of dateFin
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set the value of dateFin
     *
     * @return  self
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get the value of limitativite
     */
    public function getLimitativite()
    {
        return $this->limitativite;
    }

    /**
     * Set the value of limitativite
     *
     * @return  self
     */
    public function setLimitativite($limitativite)
    {
        $this->limitativite = $limitativite;

        return $this;
    }

    /**
     * Get the value of montantGlobal
     */
    public function getMontantGlobal()
    {
        return $this->montantGlobal;
    }

    /**
     * Set the value of montantGlobal
     *
     * @return  self
     */
    public function setMontantGlobal($montantGlobal)
    {
        $this->montantGlobal = $montantGlobal;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
