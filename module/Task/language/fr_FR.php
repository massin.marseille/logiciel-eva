<?php

return [
    'task_module_title' => '[[ task_entities | ucfirst ]]',
    'task_entity'       => 'tâche',
    'task_entities'     => 'tâches',

    'task_nav_list'   => 'Liste des {{ __tb.total }} [[ task_entities ]]',
    'task_nav_form'   => 'Créer une [[ task_entity ]]',
    'task_nav_import' => 'Importer des [[ task_entities ]]',

    'task_field_name'        => 'Titre',
    'task_field_project'     => '[[ project_entity | ucfirst ]]',
    'task_field_start'       => 'Début',
    'task_field_end'         => 'Fin',
    'task_field_progress'    => 'Avancement',
    'task_field_description' => 'Description',
    'task_field_users'       => '[[ user_entities | ucfirst ]]',
    'task_field_createdAt'   => '[[ field_created_at_f ]]',
    'task_field_updatedAt'   => '[[ field_updated_at_f ]]',

    'task_message_saved'   => '[[ task_entity | ucfirst ]] enregistrée',
    'task_message_deleted' => '[[ task_entity | ucfirst ]] supprimée',
    'task_question_delete' => 'Voulez-vous réellement supprimer la tâche %1 ?',

    'task_field_end_before_start_error' => 'Attention, la date de fin est située avant la date de début',

    'task_kanban_configuration'       => 'Kanban',
    'tooltip_switch_view_task_kanban' => 'Vue Kanban',
    'tooltip_switch_view_task_list'   => 'Vue liste',
    'task_nav_list_with_criteria'  => 'Liste des {{__tb.total }} [[ task_entities ]] avec {{__tb.apiParams.search.data.nbFilters}} filtre{{__tb.apiParams.search.data.nbFilters > 1 ? "s" : ""}}',

];
