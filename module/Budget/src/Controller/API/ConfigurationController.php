<?php

namespace Budget\Controller\API;

use Budget\Entity\Account;
use Budget\Entity\PosteExpenseRef;
use Core\Controller\BasicRestController;
use Laminas\View\Model\JsonModel;

class ConfigurationController extends BasicRestController
{

    public function categoriesAction()
    {
        $categories = $this->entityManager()->getRepository('Budget\Entity\Nature')->findAll();
        return new JsonModel($categories);
    }

    public function accountsAction()
    {
        $accounts = $this->entityManager()->getRepository('Budget\Entity\Account')->findBy(array('type' => Account::TYPE_EXPENSE));
        return new JsonModel($accounts);
    }

    public function readExpensesAction()
    {
        if (!$this->acl()->isAllowed('budget:e:posteExpenseReferential:r')) {
            return $this->notFoundAction();
        }
        $posteExenseGroups = $this->entityManager()->getRepository('Budget\Entity\PosteExpenseRef')->findBy(array('parent' => null));
        return new JsonModel($posteExenseGroups);
    }

    public function saveExpenseAction()
    {
        if (!$this->acl()->isAllowed('budget:e:posteExpenseReferential:c') || !$this->acl()->isAllowed('budget:e:posteExpenseReferential:u')) {
            return $this->notFoundAction();
        }
        $data = json_decode($this->getRequest()->getContent());

        $poste = $this->entityManager()->getRepository('Budget\Entity\PosteExpenseRef')->findOneBy(array('id' => $data->id));
        if (null === $poste) {
            $poste = new PosteExpenseRef();
        }
        $poste->setName($data->name);
        if (null !== $data->parent) {
            $parent = $this->entityManager()->getRepository('Budget\Entity\PosteExpenseRef')->findOneBy(array('id' => $data->parent));
            $poste->setParent($parent);

            if (null !== $data->category) {
                $category = $this->entityManager()->getRepository('Budget\Entity\Nature')->findOneBy(array('id' => $data->category->id));
                $poste->setNature($category);
            }       
            
            if(null !== $data->account) {
                $account = $this->entityManager()->getRepository('Budget\Entity\Account')->findOneBy(array('id' => $data->account->id));
                $poste->setAccount($account);
            }
        }
        $this->entityManager()->persist($poste);
        $this->entityManager()->flush();

        return new JsonModel(['id' => $poste->getId()]);
    }

    public function deleteExpenseAction()
    {
        if (!$this->acl()->isAllowed('budget:e:posteExpenseReferential:d')) {
            return $this->notFoundAction();
        }
        $id = $this->params()->fromRoute('id', false);

        $poste = $this->entityManager()->getRepository('Budget\Entity\PosteExpenseRef')->findOneBy(array('id' => $id));
        $this->entityManager()->remove($poste);
        $this->entityManager()->flush();

        return new JsonModel();
    }
}
