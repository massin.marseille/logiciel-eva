<?php

namespace Attachment\Controller\API;

use Attachment\Core\Uploader;
use Attachment\Entity\Attachment;
use Core\Controller\BasicRestController;
use Laminas\Mvc\MvcEvent;

class AttachmentController extends BasicRestController
{
    protected $entityClass = 'Attachment\Entity\Attachment';
    protected $repository  = 'Attachment\Entity\Attachment';
    protected $entityChain = '';

    public function onDispatch(MvcEvent $event)
    {
        $this->entityChain = $this->params()->fromQuery('right', null);
        if ($this->entityChain === null) {
            $request = $event->getRequest();
            $data = $this->processBodyContent($request);
            if (!isset($data['right'])) {
                return $this->notFoundAction();
            }
            $this->entityChain = $data['right'];
        }
        return parent::onDispatch($event);
    }

    protected function searchList($queryBuilder, &$data)
    {
        $sort    = isset($data['sort'])    ? $data['sort']    : 'object.name';
        $order   = isset($data['order'])   ? $data['order']   : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        //$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    public function create($data)
    {
        $data['user'] = $this->authStorage()->getUser();
        if ($data['type'] == Attachment::TYPE_URL) {
            $data['size'] = 0;
        } elseif ($data['type'] == Attachment::TYPE_FILE) {
            if (isset($data['file']) && $data['file'] != null) {
                $data['url'] = Uploader::uploadFromBase64($data, $this->environment()->getClient());
            }
        }
        
        return parent::create($data);
    }

    public function update($id, $data)
    {
        if ($data['type'] == Attachment::TYPE_URL) {
            $data['size'] = 0;
        } elseif ($data['type'] == Attachment::TYPE_FILE) {
            if (isset($data['file']) && $data['file'] != null) {
                $data['url'] = Uploader::uploadFromBase64($data, $this->environment()->getClient());
            }
        }

        return parent::update($id, $data);
    }
}
