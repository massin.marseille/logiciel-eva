<?php

namespace PostParc\Service;

use PostParc\Client\PostParcApiClientService;
use PostParc\Helper\DataMapper;
use PostParc\Helper\DataProcessor;

class PostParcSynchronisationService
{
    private $_entityManager;
    private $_configuration;
    private $_postParcClient;
    private $_processor;

    public function __construct($configuration, $entityManager)
    {
        $this->_entityManager = $entityManager;
        $this->_postParcClient = new PostParcApiClientService($configuration);
        $this->_configuration = $configuration;
    }

    public function run()
    {
        $groups = $this->_postParcClient->getGroups();
        $pfos = $this->_postParcClient->getPfos();
        $organisations = $this->_postParcClient->getOrganizations();
        $persons = $this->_postParcClient->getPersons();
        $functions = $this->_postParcClient->getFunctions();
        $organisationsTypes = $this->_postParcClient->getOrganizationsTypes();

        $this->_processor = new DataProcessor(
            $this->_entityManager,
            new DataMapper(),
            $organisationsTypes,
            $groups,
            $organisations,
            $persons,
            $functions,
            $pfos
        );

        $this->_processor->process();
    }

}
