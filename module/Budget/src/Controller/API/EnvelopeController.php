<?php

namespace Budget\Controller\API;

use Budget\Entity\Envelope;
use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Laminas\View\Model\JsonModel;

class EnvelopeController extends BasicRestController
{
    protected $entityClass = 'Budget\Entity\Envelope';
    protected $repository  = 'Budget\Entity\Envelope';
    protected $entityChain = 'budget:e:envelope';

    public function checkAction() {
		$entityManager = $this->getEntityManager();
        $id = $this->params()->fromRoute('id', false);
        
        /** @var Envelope */
        $envelope = $entityManager->getRepository('Budget\Entity\Envelope')->find($id);

        $posteIncomeTotalAmount = $entityManager->getRepository('Budget\Entity\PosteIncome')->getAmountByEnvelope($envelope);

		return new JsonModel([
            'envelope_amount' => $envelope->getAmount(),
            'envelope_use_amount' => floatval($posteIncomeTotalAmount)
		]);
	}

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->join('object.financer', 'financer');

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                    'object.code LIKE :f_full',
                    'financer.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
