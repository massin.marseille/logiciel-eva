(function (app) {

    app.factory('ageSynchroService', [
        '$http', '$q', 'restFactoryService',
        function ageSynchroService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/project'), {
                getAgeSynchronisation: function getAgeSynchronisation(projectId) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: '/project/age-synchronisation/' + projectId
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                preventAgeSynchronisation: function preventAgeSynchronisation(projectId) {
                    var deferred = $q.defer();
                    $http({
                        method: 'PATCH',
                        url: '/project/age-synchronisation/' + projectId + '/prevent'
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                },
                synchroniseWithAge: function synchroniseWithAge(projectId, synchronisation) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: '/project/age-synchronisation/' + projectId + '/synchronise',
                        data: synchronisation
                    }).then(function (res) {
                        deferred.resolve(res.data)
                    }, function (err) {
                        deferred.reject(err)
                    });
                    return deferred.promise;
                }
            });
        }
    ]);

})(window.EVA.app);