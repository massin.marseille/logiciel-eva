(function(app) {

    app.factory('exportService', [
        'restFactoryService',
        function exportService(restFactoryService) {
            return restFactoryService.create('/api/export');
        }
    ])

})(window.EVA.app);
