<?php

namespace Campain\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use BadMethodCallException;
use Indicator\Entity\Measure;
use Campain\Entity\Indicator;
use Campain\Entity\Campain;
use Campain\Entity\Measure as CampainMeasure;
class IndicatorController extends BasicRestController
{
    protected $entityClass = 'Campain\Entity\Indicator';
    protected $repository  = 'Campain\Entity\Indicator';
    protected $entityChain = 'campain:e:campain';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }
       
        $queryBuilder->addOrderBy($sort, $order);
        $queryBuilder->join('object.campain', 'campain');
        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'campain.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    protected function extract($object)
    {
        $array = parent::extract($object);
        $objectAsArray = [];
        if(isset($array['entity']) && isset($array['primary'])){
            $entityObject = $this->entityManager()
            ->getRepository($array['entity'])
            ->find($array['primary']);
            $array['entityDetails'] = parent::extractInternal($entityObject, 'name', $objectAsArray);
            if($array['entity'] == 'Indicator\Entity\Indicator'){
                $array['type'] = 'indicator';
                $array['indicatorPrimary']['id'] = $array['primary'];
                $array['indicatorPrimary']['name'] = $array['entityDetails']['name'];
            }else if($array['entity'] == 'Indicator\Entity\GroupIndicator'){
                $array['type'] = 'indicator-group';
                $array['groupIndicatorPrimary']['id'] = $array['primary'];
                $array['groupIndicatorPrimary']['name'] = $array['entityDetails']['name'];
            }
        }
        return $array;
    }

    public function create($data)
	{
        if($data['type'] == 'indicator' && isset($data['indicatorPrimary'])){
            $data['primary'] = $data['indicatorPrimary']['id'];
            $data['entity'] = 'Indicator\Entity\Indicator';
        }else if($data['type'] == 'indicator-group' && isset($data['groupIndicatorPrimary'])){
            $data['primary'] = $data['groupIndicatorPrimary']['id'];
            $data['entity'] = 'Indicator\Entity\GroupIndicator';
        }
		return parent::create($data);
    }

/**
	 * Hydrate a record from a data array.
	 * This method can be overridden to use another logic than the Doctrine Hydrator.
	 *
	 * @param array $data
	 * @param mixed $object
	 */
	protected function hydrate($data, $object)
	{
		parent::hydrate($data, $object);
        $this->createMeasures($object);
	}

     /**
     * @param Campain\Entity\Indicator $campainIndicator
     */
    private function createMeasures($campainIndicator){
        $entityObject = $this->entityManager()
        ->getRepository($campainIndicator->getEntity())
        ->find($campainIndicator->getPrimary());

        switch($campainIndicator->getEntity()){
            case 'Indicator\Entity\Indicator':
                $this->createMeasuresByIndicator($entityObject, null, $campainIndicator);
                break;
            case 'Indicator\Entity\GroupIndicator':
                $this->createMeasuresByIndicatorGroup($entityObject, $campainIndicator);
                break;
            default: throw new BadMethodCallException(
                'Invalid campain indicator entity'
            );
            break;
        }
    }
    
    /**
     * @param Indicator\Entity\Indicator $indicator
     * @param Indicator\Entity\GroupIndicator $indicatorGroup
     * @param Campain\Entity\Indicator $campainIndicator
     */
    private function createMeasuresByIndicator($indicator, $indicatorGroup, $campainIndicator){
        if(is_null($indicatorGroup)){
                   // On supprime les mesures non modifiées
            $em = $this->getEntityManager();
            $em ->createQuery('DELETE Indicator\Entity\Measure m WHERE m.indicator = :indicator_id AND m.campainIndicator = :campain_indicator_id AND m.updatedAt is NULL')
            ->setParameter('indicator_id', $indicator->getId())
            ->setParameter("campain_indicator_id", $campainIndicator->getId())
            ->execute();
        }
        $campainObject = $this->entityManager()
        ->getRepository('Campain\Entity\Campain')
        ->find($campainIndicator->getCampain()->getId());
        
        if($campainIndicator->getFrequency() && $campainIndicator->getFrequencyValue()>0){
            for($measureDate = $campainIndicator->getTargetDate(); 
                $measureDate < $campainObject->getEndDate(); 
                $measureDate = $this->getNextDateByFrequency(clone $measureDate, $campainIndicator->getFrequency(), $campainIndicator->getFrequencyValue())){
                    $campainIndicator->getCampainMeasures()->add(
                        $this->createCampainMeasureObject(
                            $measureDate,
                            $this->createMeasureObject($measureDate, Measure::TYPE_TARGET, $campainObject, $campainIndicator, $indicator, $indicatorGroup),
                            $this->createMeasureObject($measureDate, Measure::TYPE_DONE, $campainObject, $campainIndicator, $indicator, $indicatorGroup),
                            $campainIndicator
                        )
                    );
            }
        } else if($campainIndicator->getTargetDate()){
            $campainIndicator->getCampainMeasures()->add(
                $this->createCampainMeasureObject(
                    $campainIndicator->getTargetDate(),
                    $this->createMeasureObject($campainIndicator->getTargetDate(), Measure::TYPE_TARGET, $campainObject, $campainIndicator, $indicator, null),
                    $this->createMeasureObject($campainIndicator->getTargetDate(), Measure::TYPE_DONE, $campainObject, $campainIndicator, $indicator, null),
                    $campainIndicator
                )
            );
        }  
    }

    private function createCampainMeasureObject($date, $targetMeasure, $realizedMeasure, $campainIndicator){
        $campainMeasureObject = new CampainMeasure();
        $campainMeasureObject->setSupposedDate($date);
        $campainMeasureObject->setTargetMeasure($targetMeasure);
        $campainMeasureObject->setRealiseMeasure($realizedMeasure);
        $campainMeasureObject->setIndicator($campainIndicator);
        $campainMeasureObject->setCampain($campainIndicator->getCampain());
        $targetMeasure->setCampainMeasure($campainMeasureObject);
        $realizedMeasure->setCampainMeasure($campainMeasureObject);
        return $campainMeasureObject;
    }

    private function createMeasureObject($date, $type, $campainObject, $campainIndicator, $indicator, $indicatorGroup){
        $measureObject = new Measure();
        $measureObject->setType($type);
        $client = $this->environment()->getClient(); 
        $measureObject->setClient($client);
        $measureObject->setCampain($campainObject);
        $measureObject->setIndicator($indicator);
        $measureObject->setCampainIndicator($campainIndicator);
        $measureObject->setGroupIndicator($indicatorGroup);
        return $measureObject;
    }

    private function getNextDateByFrequency($date, $frequency, $value){
        switch($frequency){
            case Indicator::FREQUENCY_WEEKLY:
                return $date->modify('+'.$value.' week');
            case Indicator::FREQUENCY_MONTHLY:
                return $date->modify('+'.$value.' month');
            case Indicator::FREQUENCY_TRIMONTHLY:
                return $date->modify('+'.(3*$value).' month');
            case Indicator::FREQUENCY_BIMANNUALLY:
                return $date->modify('+'.(6*$value).' month');
            case Indicator::FREQUENCY_ANNUALLY:
                return $date->modify('+'.$value.' year');
            default: throw new BadMethodCallException('Invalid frequency');
            break;
        }
    }

    /**
     * @param Indicator\Entity\GroupIndicator $indicatorGroup
     */
    private function createMeasuresByIndicatorGroup($indicatorGroup, $campainIndicator){
        // On supprime les mesures non modifiées
        $em = $this->getEntityManager();
        $em ->createQuery('DELETE Indicator\Entity\Measure m WHERE m.groupIndicator = :group_id AND m.campainIndicator = :campain_indicator_id AND m.updatedAt is NULL')
        ->setParameter('group_id', $indicatorGroup->getId())
        ->setParameter("campain_indicator_id", $campainIndicator->getId())
        ->execute();

        foreach($indicatorGroup->getIndicators() as $indicator){
            $this->createMeasuresByIndicator($indicator, $indicatorGroup, $campainIndicator);
        }
    }


}
