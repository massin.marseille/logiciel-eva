<?php

namespace Profile;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router'      => [
        'client-routes' => [
            'api'     => [
                'child_routes' => [
                    'profile' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/profile[/:id]',
                            'defaults' => [
                                'controller' => 'Profile\Controller\API\Profile',
                            ],
                        ],
                    ],
                ],
            ],
            'profile' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/profile',
                    'defaults' => [
                        'controller' => 'Profile\Controller\Index',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class    => ServiceLocatorFactory::class,
            Controller\API\ProfileController::class => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Profile\Controller\Index'    => Controller\IndexController::class,

            'Profile\Controller\API\Profile' => Controller\API\ProfileController::class,
        ],
    ],
];
