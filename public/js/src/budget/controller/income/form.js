(function(app) {

    app.controller('incomeFormController', [
        'incomeService', 'flashMessenger', 'translator', '$scope',
        function incomeFormController(incomeService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal = angular.element('#incomeFormModal');
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'poste.id',
                    'date',
                    'amount',
                    'amountHT',
                    'type',
                    'keywords',
                    'createdAt',
                    'updatedAt',
                    'convention',
                    'conventionTitle',
                    'tiers',
                    'anneeExercice',
                    'bordereau',
                    'mandat',
                    'complement',
                    'engagement',
                    'serie',
                    'transfer.id',
                    'managementUnit.id',
                    'age',
                    'evolution',
                    'project.id',
                    'project.name',
                ]
            };

            self.income = {};

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.from    = {
                type: null,
                id: null
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id   = id;
                if (self.from.type == 'project') {
                    self.object = $scope.$parent.$parent.$parent.self[self.from.type];
                }
            };

            self.saveIncome = function saveIncome() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.income.id;

                var row = angular.copy(self.income);
                row.poste = row.poste.id;
                incomeService.save(row, apiParams).then(function(res) {
                    /**
                    if (isCreation) {
                        self.poste.incomesArbo.push(res.object);
                    } else {
                        for(var i in self.poste.incomesArbo){
                            if(self.poste.incomesArbo[i].id == res.object.id){
                                var posteOldAmount = 'amountArbo' + self.poste.incomesArbo[i].type.toCamelCase().ucfirst();
                                self.poste[posteOldAmount] = parseFloat(self.poste[posteOldAmount]) - parseFloat(self.poste.incomesArbo[i].amount);
                                self.poste.incomesArbo[i] = res.object;
                                break;
                            }
                        }
                    }

                    var posteAmount = 'amountArbo' + res.object.type.toCamelCase().ucfirst();
                    if (typeof self.poste[posteAmount] == 'undefined') { // if the poste is newly created
                        self.poste[posteAmount] = 0;
                    }
                    self.poste[posteAmount] = parseFloat(self.poste[posteAmount]) + parseFloat(res.object.amount);
                    **/

                    var reload = [res.object.poste.id];
                    if (self.poste.id  !== res.object.poste.id) {
                        reload.push(self.poste.id );
                    }
                    $scope.$emit('reload-poste-income', reload);

                    flashMessenger.success(translator('income_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            $scope.$on('create-income', function (event, poste) {
                self.poste = poste;
                self.income = {
                    _rights: {
                        update: true
                    },
                    amount: 0,
                    amountHT: 0,
                    subventionAmount: 0,
                    subventionAmountHT: 0,
                    poste: poste,
                    date: null,
                    project: {
                        id: self.from.id
                    }
                };

                self.errors = {
                    fields: {}
                };

                self.panelTitle = translator('income_nav_form');
                $modal.modal('show');
            });

            $scope.$on('edit-income', function (event, params) {
                self.poste  = params.poste;
                self.income = angular.copy(params.income);

                if (self.poste.id === self.income.poste.id) {
                    self.income.poste = self.poste;
                }
                //self.income._rights = angular.copy(self.poste._rights);
                //self.income.poste = self.poste.id;

                self.errors = {
                    fields: {}
                };

                self.panelTitle = params.income.name;
                $modal.modal('show');
            });

            $scope.$on('duplicate-income', function (event, params) {
                self.poste   = params.poste;
                self.income = angular.copy(params.income);
                self.income.id = null;
                self.income._rights = angular.copy(self.poste._rights);
                //self.expense.poste = self.poste.id;

                self.errors = {
                    fields: {}
                };

                self.panelTitle = params.income.name;
                $modal.modal('show');
            });
        }
    ]);

})(window.EVA.app);
