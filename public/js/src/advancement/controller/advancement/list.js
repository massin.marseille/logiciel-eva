(function(app) {

    app.controller('advancementListController', [
        'advancementService', '$scope', 'translator', 'flashMessenger', '$timeout',
        function advancementListController(advancementService, $scope, translator, flashMessenger, $timeout) {
            var self = this;

            self.from   = {
                type: null,
                id: null
            };

            self.view = 'table';

            self.showAdvancements = true;

            self.advancements = [];
            self.options = {
                name: 'advancements-list',
                col: [
                    'id',
                    'type',
                    'date',
                    'value',
                    'name',
                    'description',
                    'createdAt',
                    'updatedAt',
                    'project.id'
                ],
                searchType: 'list',
                sort: 'date',
                order: 'asc',
                pager: false,
                getItems: function getData(params, callback, abort) {
                    if ($scope.$parent.self[self.from.type].id || self.from.id) {
                        if (self.from.type == 'project') {
                            params.search.data.filters['project.id'] = {
                                op: 'eq',
                                val: $scope.$parent.self[self.from.type].id || self.from.id
                            };
                        }

                        advancementService.findAll(params, abort.promise).then(function (res) {
                            callback(res);
                            self.stackedBar = self.getStackedBar();
                        })
                    } else {
                        callback({rows: []});
                    }
                }
            };

            self.init = function init(from, id) {
                self.from.type = from;
                self.from.id   = id;
            };

            self.editAdvancement = function editAdvancement(advancement) {
                $scope.$broadcast('edit-advancement', advancement);
            };

            self.createAdvancement = function createAdvancement(value) {
                $scope.$broadcast('create-advancement', value);
            };

            self.deleteAdvancement = function deleteAdvancement(advancement) {
                if (confirm(translator('advancement_question_delete'))) {
                    advancementService.remove(advancement).then(function (res) {
                        if (res.success) {
                            self.advancements.splice(self.advancements.indexOf(advancement), 1);
                            flashMessenger.success(translator('advancement_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(advancement) {
                self.editedLineErrors = {};
                self.editedLine       = advancement;
                self.editedLineFields = angular.copy(advancement);
            };

            self.saveEditedLine = function saveEditedLine(advancement) {
                self.editedLineErrors = {};
                advancementService.save(angular.extend({}, advancement, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        advancement = angular.extend(advancement, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('advancement_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };


            self.getLastAdvancement = function getLastAdvancement(type) {
                var last = null;
                for (var i in self.advancements) {
                    var advancement = self.advancements[i];
                    if (advancement.type == type) {
                        if (last == null) {
                            last = advancement;
                        } else if (moment(advancement.date, 'DD/MM/YYYY HH:mm').isAfter(moment(last.date, 'DD/MM/YYYY HH:mm')) ) {
                            last = advancement;
                        }
                    }
                }

                return last;
            };

            self.getClosestsAdvancements = function getClosestsAdvancements(advancement, type) {
                var lower     = null;
                var lowerDiff = null;
                var upper = null;
                var upperDiff = null;

                if (advancement) {

                    var date = moment(advancement.date, 'DD/MM/YYYY HH:mm');
                    for (var i in self.advancements) {
                        var _adv  = self.advancements[i];
                        var _date = moment(_adv.date, 'DD/MM/YYYY HH:mm');
                        if (type == _adv.type) {
                            var _diff = date.diff(_date);
                            if (_diff > 0) { // lower
                                if (lowerDiff === null || _diff < lowerDiff) {
                                    lowerDiff = _diff;
                                    lower     = _adv;
                                }
                            } else { // upper
                                if (upperDiff === null || _diff > upperDiff) {
                                    upperDiff = _diff;
                                    upper     = _adv;
                                }
                            }
                        }
                    }
                }

                return {
                    upper: upper,
                    lower: lower
                };
            };

            self.stackedBar = {};
            self.getStackedBar = function () {
                var lastDone   = self.getLastAdvancement('done');
                var lastTarget = self.getLastAdvancement('target');
                var closests   = self.getClosestsAdvancements(lastDone, 'target');

                var _done        = lastDone ? lastDone.value : 0;
                var _upperTarget = closests.upper ? closests.upper.value : 0;
                var _late        = (closests.lower ? (closests.lower.value > _done ? closests.lower.value : 0) : 0);
                var _totalTarget = lastTarget ? lastTarget.value : 0;


                var done        = _done;
                var late        = (_late - _done) > 0 ? (_late - _done) : 0;
                var upperTarget = _upperTarget - (late + done) > 0 ? _upperTarget - (late + done) : 0;
                var totalTarget = _totalTarget - (done + late + upperTarget);

                return {
                    done : done,
                    late : late,
                    upperTarget: upperTarget,
                    totalTarget : totalTarget
                }
            };

            $scope.$watch(function() {return self.advancements}, function () {
                self.stackedBar = self.getStackedBar();

                // OK c'est pas fou mais ça fonctionne !
                $timeout(function() {
                    $('.advancement-timeline-container').find('.timeline-panel').on('mousedown', function (e) {
                        if (!$(e.target).is('i.fa')) {
                            var $jalon     = $(this).parent('.timeline-jalon');
                            var $container = $jalon.parents('.advancement-timeline-container');

                            var containerWidth = $container.width();
                            var jalonLeft = parseFloat($jalon.css('left'));

                            e = e || window.event;
                            var start = 0;
                            if( e.pageX) {
                                start = e.pageX;
                            } else if( e.clientX) {
                                start = e.clientX;
                            }

                            document.body.onmousemove = function(e) {
                                e = e || window.event;
                                var end = 0;
                                if(e.pageX) {
                                    end = e.pageX;
                                } else if(e.clientX) {
                                    end = e.clientX;
                                }

                                var diff = jalonLeft  + (end - start);
                                if (diff >= 0 && diff <= containerWidth) {
                                    $jalon.css('left', diff + 'px');
                                    $jalon.find('.value').text(Math.round(diff * 100 / containerWidth) + '%');
                                }
                            };

                            document.body.onmouseup = function() {
                                document.body.onmousemove = document.body.onmouseup = null;

                                var _jalonLeft = parseFloat($jalon.css('left'));
                                var id = $jalon.attr('id');

                                for (var i in self.advancements) {
                                    if (self.advancements[i].id == id && self.advancements[i]._rights.update) {
                                        $scope.$apply(function () {
                                            if (_jalonLeft !== jalonLeft) {
                                                self.advancements[i].value = Math.round(_jalonLeft * 100 / containerWidth);
                                                advancementService.save(self.advancements[i], {
                                                    col: self.options.col
                                                }).then(function () {});
                                            } else {
                                                self.editAdvancement(self.advancements[i]);
                                            }
                                        });
                                    }
                                }
                            };
                        }
                    });
                });
            }, true);

            var $timelineCreate   = angular.element('.timeline-create');
            $timelineCreate.tooltip();
            var $timelineProgress = angular.element('.advancement-timeline-container .advancement-progress-project-tab');
            $timelineProgressWidth = $timelineProgress.width();
            $timelineProgress.on('mousemove', function (e) {
                e = e || window.event;
                var end = 0;
                if(e.pageX) {
                    end = e.pageX;
                } else if(e.clientX) {
                    end = e.clientX;
                }

                var x = end - $timelineProgress.offset().left - ($timelineCreate.width() / 2);
                var value =  Math.round((x + ($timelineCreate.width() / 2) ) * 100 / $timelineProgress.width());
                $timelineCreate.css('left',   x + 'px')
                               .attr('title', value + '%')
                               .data('value', value)
                               .tooltip('fixTitle').tooltip('show');
            });

            self.createAdvancementFromTimeline = function createAdvancementFromTimeline() {
                self.createAdvancement($timelineCreate.data('value'));
            };

            self.isManager = function isManager(user) {
                for (var i in $scope.$parent.$parent.self.project.members) {
                    var member = $scope.$parent.$parent.self.project.members[i];
                    if (member.id && member.user.id == user && member.role == 'manager') {
                        return true;
                    }
                }

                return false;
            };
        }
    ])

})(window.EVA.app);
