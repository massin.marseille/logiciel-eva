(function(app) {

    app.directive('querySelect', [
        'queryService', 'selectFactoryService',
        function querySelect(queryService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: queryService
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
