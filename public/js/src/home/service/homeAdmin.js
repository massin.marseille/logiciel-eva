(function(app) {

    app.factory('homeAdminService', [
        '$http', '$q', 'restFactoryService',
        function homeAdminService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/homeAdmin'));
        }
    ])

})(window.EVA.app);
