(function(app) {
	app.controller('filterController', [function filterController() {}]);

	app.controller('posteExpenseListController', [
		'posteExpenseService',
		'expenseService',
		'$scope',
		'flashMessenger',
		'translator',
		'$timeout',
		'$rootScope',
		function posteExpenseListController(
			posteExpenseService,
			expenseService,
			$scope,
			flashMessenger,
			translator,
			$timeout,
			$rootScope,
		) {
			var self = this;

			self.lock = $rootScope.lockBudget;
			self.groupBy = 'poste';
			self.cacheGroupBy = [];

			self.from = {
				type: null,
				id: null,
			};
			self.object = null;

			self.total = {
				id: 'total',
				_rights: {
					read: false,
					delete: false,
					update: false,
				},
			};

			self.expenseParams = {
				search: {
					data: {
						filters: {},
					},
				},
			};
			self.exportModel = 'default';

			
			self.posteExpenses = [];
			self.options = {
				name: 'poste-expenses-list',
				col: [
					'id',
					'name',
					'amount',
					'amountHT',
					'amountArbo',
					'amountHTArbo',
					'amountAggreg',
					'amountHTAggreg',
					'account.id',
					'account.code',
					'account.name',
					'nature.id',
					'nature.code',
					'nature.name',
					'organization.id',
					'organization.code',
					'organization.name',
					'managementUnit.id',
					'managementUnit.code',
					'managementUnit.name',
					'destination.id',
					'destination.code',
					'destination.name',
					'parent.id',
					'parent.name',
					'parent.account.id',
					'parent.account.code',
					'project.id',
					'project.name',
					'project.parent.id',
					'envelope.id',
					'envelope.name',
					'expensesArbo.id',
					'expensesArbo.name',
					'expensesArbo.amount',
					'expensesArbo.amountHT',
					'expensesArbo.date',
					'expensesArbo.type',
					'expensesArbo.poste.id',
					'expensesArbo.keywords',
					'expensesArbo.tiers',
					'expensesArbo.anneeExercice',
					'expensesArbo.bordereau',
					'expensesArbo.mandat',
					'expensesArbo.complement',
					'expensesArbo.engagement',
					'expensesArbo.dateFacture',
					'expensesArbo.createdAt',
					'expensesArbo.updatedAt',
					'expensesArbo.project.id',
					'expensesArbo.project.name',
					'expensesArbo.age',
					'expensesArbo.evolution',
					'expensesArbo.account.id',
					'expensesArbo.account.code',
					'expensesArbo.account.name',
					'expensesArbo.nature.id',
					'expensesArbo.nature.code',
					'expensesArbo.nature.name',
					'expensesArbo.organization.id',
					'expensesArbo.organization.code',
					'expensesArbo.organization.name',
					'expensesArbo.destination.id',
					'expensesArbo.destination.code',
					'expensesArbo.destination.name',
					'expensesArbo.managementUnit.id',
					'expensesArbo.managementUnit.code',
					'expensesArbo.managementUnit.name',
					'keywords',
					'createdAt',
					'updatedAt',
				],
				searchType: 'list',
				sort: 'name',
				order: 'asc',
				pager: false,
				getItems: function getItems(params, callback, abort) {
					if ($scope.$parent.self[self.from.type].id || self.from.id) {
						if (self.from.type === 'project') {
							params.search.data.filters['project.id'] = {
								op: 'eq',
								val: self.object.id || self.from.id,
							};
						}

						params.expenseFilters = self.expenseParams.search.data.filters;
						params.groupBy = self.groupBy;

						posteExpenseService.findAll(params, abort.promise).then(function(res) {
							res.rows.push(self.getTotal(res.rows));
							callback({ rows: res.rows });
						});
					} else {
						callback({ rows: [] });
					}
				},
			};

			self.resetExpenseFilters = function() {
				for (var i in self.expenseParams.search.data.filters) {
					if (i !== 'id') {
						self.expenseParams.search.data.filters[i] = {};
					}
				}
			};

			$scope.$watch(
				function() {
					return self.posteExpenses;
				},
				function() {
					self.getTotal(self.posteExpenses);
					$scope.chart.data = self.getChartData();
				},
				true,
			);

			self.expenseTypes = [];
			self.init = function init(from, id, types, balances) {
				self.from.type = from;
				self.from.id = id;
				if (self.from.type === 'project') {
					self.object = $scope.$parent.self[self.from.type];
				}

				self.expenseTypes = types;
				for (var i in types) {
					self.options.col.push('amountArbo' + types[i].ucfirst());
					self.options.col.push('amountHTArbo' + types[i].ucfirst());
				}

				self.posteExpenseBalances = balances;
				for (var j in balances) {
					self.options.col.push('amountArbo' + balances[j].ucfirst());
					self.options.col.push('amountHTArbo' + balances[j].ucfirst());
				}
			};

			self.editPosteExpense = function editPosteExpense(posteExpense) {
				$scope.$broadcast('edit-poste-expense', posteExpense);
			};

			self.createPosteExpense = function createPosteExpense() {
				$scope.$broadcast('create-poste-expense');
			};

			self.deletePosteExpense = function deletePosteExpense(posteExpense) {
				if (confirm(translator('poste_expense_question_delete').replace('%1', posteExpense.name))) {
					posteExpenseService.remove(posteExpense).then(function(res) {
						if (res.success) {
							self.posteExpenses.splice(self.posteExpenses.indexOf(posteExpense), 1);
							flashMessenger.success(translator('poste_expense_message_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				}
			};

			self.createExpense = function createExpense(poste) {
				if (poste.expensesArbo === null) {
					poste.expensesArbo = [];
				}
				$scope.$broadcast('create-expense', poste);
			};

			self.editExpense = function editExpense(expense, poste) {
				$scope.$broadcast('edit-expense', {
					expense: expense,
					poste: poste,
				});
			};

			self.duplicateExpense = function(expense, poste) {
				$scope.$broadcast('duplicate-expense', {
					expense: expense,
					poste: poste,
				});
			};

			self.deleteExpense = function deleteExpense(expense, poste) {
				if (confirm(translator('expense_question_delete').replace('%1', expense.name))) {
					expenseService.remove(expense).then(function(res) {
						if (res.success) {
							var posteOldAmount = 'amountArbo' + expense.type.toCamelCase().ucfirst();
							poste[posteOldAmount] =
								parseFloat(poste[posteOldAmount]) - parseFloat(expense.amount);
							poste.expensesArbo.splice(poste.expensesArbo.indexOf(expense), 1);
							flashMessenger.success(translator('expense_message_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				}
			};

			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};
			self.editLine = function editLine(object) {
				if (angular.isArray(object.keywords) || object.keywords == null) {
					object.keywords = {};
				}

				self.editedLineErrors = {};
				self.editedLine = object;
				self.editedLineFields = angular.copy(object);
			};

			self.saveEditedLine = function saveEditedLine(object, type, _poste) {
				var service = null;
				var cols = [];
				var message = '';
				var pattern = /^expensesArbo./;

				if (type === 'poste') {
					service = posteExpenseService;
					message = 'poste_expense_message_saved';
					cols = self.options.col;
				} else if (type === 'expense') {
					service = expenseService;
					message = 'expense_message_saved';

					for (var i in self.options.col) {
						if (pattern.test(self.options.col[i])) {
							cols.push(self.options.col[i].replace('expensesArbo.', ''));
						}
					}
				}

				self.editedLineErrors = {};
				service
					.save(angular.extend({}, object, self.editedLineFields), {
						col: cols,
					})
					.then(
						function(res) {
							if (res.success) {
								if (type == 'expense') {
									var posteOldAmount = 'amountArbo' + self.editedLine.type.toCamelCase().ucfirst();
									var posteOldAmountHT =
										'amountHTArbo' + self.editedLine.type.toCamelCase().ucfirst();
									_poste[posteOldAmount] =
										parseFloat(_poste[posteOldAmount]) - parseFloat(self.editedLine.amount);
									_poste[posteOldAmountHT] =
										parseFloat(_poste[posteOldAmountHT]) - parseFloat(self.editedLine.amountHT);

									var posteNewAmount = 'amountArbo' + res.object.type.toCamelCase().ucfirst();
									var posteNewAmountHT = 'amountHTArbo' + res.object.type.toCamelCase().ucfirst();
									_poste[posteNewAmount] =
										parseFloat(_poste[posteNewAmount]) + parseFloat(res.object.amount);
									_poste[posteNewAmountHT] =
										parseFloat(_poste[posteNewAmountHT]) + parseFloat(res.object.amountHT);
								}

								object = angular.extend(object, res.object);

								self.editedLine = null;
								self.editedLineFields = null;

								flashMessenger.success(translator(message));
							}
						},
						function(err) {
							for (var field in err.fields) {
								self.editedLineErrors[field] = err.fields[field];
							}
						},
					);
			};

			self.getTotal = function(rows) {
				self.total['amount'] = 0;
				self.total['amountHT'] = 0;
				self.total['amountArbo'] = 0;
				self.total['amountHTArbo'] = 0;
				self.total['amountAggreg'] = 0;
				self.total['amountHTAggreg'] = 0;

				for (var i in self.expenseTypes) {
					self.total['amountArbo' + self.expenseTypes[i].ucfirst()] = 0;
					self.total['amountHTArbo' + self.expenseTypes[i].ucfirst()] = 0;
				}

				for (i in self.posteExpenseBalances) {
					self.total['amountArbo' + self.posteExpenseBalances[i].ucfirst()] = 0;
					self.total['amountHTArbo' + self.posteExpenseBalances[i].ucfirst()] = 0;
				}

				for (i in rows) {
					if (rows[i].id !== 'total') {
						self.total['amount'] += rows[i]['amount'];
						self.total['amountHT'] += rows[i]['amountHT'];
						self.total['amountArbo'] += rows[i]['amountArbo'];
						self.total['amountHTArbo'] += rows[i]['amountHTArbo'];
						self.total['amountAggreg'] += rows[i]['amountAggreg'];
						self.total['amountHTAggreg'] += rows[i]['amountHTAggreg'];

						for (var j in self.expenseTypes) {
							self.total['amountArbo' + self.expenseTypes[j].ucfirst()] +=
								rows[i]['amountArbo' + self.expenseTypes[j].ucfirst()];
							self.total['amountHTArbo' + self.expenseTypes[j].ucfirst()] +=
								rows[i]['amountHTArbo' + self.expenseTypes[j].ucfirst()];
						}

						for (j in self.posteExpenseBalances) {
							if (!self.posteExpenseBalances[j].match(/Rate/)) {
								self.total['amountArbo' + self.posteExpenseBalances[j].ucfirst()] +=
									rows[i]['amountArbo' + self.posteExpenseBalances[j].ucfirst()];
								self.total['amountHTArbo' + self.posteExpenseBalances[j].ucfirst()] +=
									rows[i]['amountHTArbo' + self.posteExpenseBalances[j].ucfirst()];
							}
						}
					}
				}

				for (j in self.posteExpenseBalances) {
					if (self.posteExpenseBalances[j].match(/aeRateBalance/)) {
						self.total['amountArbo' + self.posteExpenseBalances[j].ucfirst()] =
							self.total['amountArboAe'] > 0
								? (self.total['amountArboAeCommitted'] * 100) / self.total['amountArboAe']
								: 0;
						self.total['amountHTArbo' + self.posteExpenseBalances[j].ucfirst()] =
							self.total['amountHTArboAe'] > 0
								? (self.total['amountHTArboAeCommitted'] * 100) / self.total['amountHTArboAe']
								: 0;
					}
                    if (self.posteExpenseBalances[j].match(/cpCommittedAeCommittedRateBalance/)) {
						self.total['amountArbo' + self.posteExpenseBalances[j].ucfirst()] =
							self.total['amountArboAeCommitted'] > 0
								? (self.total['amountArboCpCommitted'] * 100) / self.total['amountArboAeCommitted']
								: 0;
						self.total['amountHTArbo' + self.posteExpenseBalances[j].ucfirst()] =
							self.total['amountHTArboAeCommitted'] > 0
								? (self.total['amountHTArboCpCommitted'] * 100) / self.total['amountHTArboAeCommitted']
								: 0;
					}
				}

				return self.total;
			};

			/*** SIMPLIFIED INPUT ***/
			self.openSimplifiedInput = function() {
				$scope.$broadcast('open-poste-expense-simplified-input');
			};

			/*** CHART ***/
			self.getChartData = function() {
				var data = [];

				var preData = {};
				for (var i in self.posteExpenses) {
					var posteExpense = self.posteExpenses[i];
					for (var j in posteExpense.expensesArbo) {
						var expense = posteExpense.expensesArbo[j];
						var time = moment(expense.date, 'DD/MM/YYYY HH:mm');
						time.set({
							date: 1,
							hour: 0,
							minute: 0,
							second: 0,
							millisecond: 0,
						});

						if (typeof preData[expense.type] == 'undefined') {
							preData[expense.type] = {};
						}

						if (typeof preData[expense.type][time.format('X')] == 'undefined') {
							preData[expense.type][time.format('X')] = 0;
						}

						preData[expense.type][time.format('X')] += expense.amount;
					}
				}

				for (var type in preData) {
					var typeData = {
						key: translator('expense_type_' + type),
						values: [],
					};

					for (var time in preData[type]) {
						typeData.values.push({
							x: time,
							y: preData[type][time],
						});
					}
					data.push(typeData);
				}

				return data;
			};

			self.showExportExpenseModal = function() {
				angular.element('#exportExcelModal3').modal('show');
				self.exportFilters = {
					id: self.from.id,
					tree: 'initial',
					reference: 1,
					levels: [
						{
							value: 1,
							mode: 'parent',
							filters: {
								id: {
									op: 'eq',
									val: self.from.id,
								},
							},
						},
					],
				};
			};


			self.export = function(type) {
				if (self.exportModel == 'default'){
					$scope.__tb.exportExcel();
				}
				else {
					console.log(self.exportModel);
					angular.element('#export' + type + 'Form3').submit();
				}
				angular.element('#export' + type + 'Modal3').modal('hide');
			};

			$scope.chart = {};
			$timeout(function() {
				$scope.chart = {
					api: {},
					options: {
						chart: {
							type: 'multiBarChart',
							showControls: false,
							height: 300,
							xAxis: {
								tickFormat: function(d) {
									return moment(d, 'X').format('MM/YYYY');
								},
							},
							yAxis: {
								tickFormat: function(d) {
									return d3.format(',')(d) + ' €';
								},
							},
						},
					},
					data: [],
				};
			});

			$scope.$watch('showChart', function(newVal) {
				if (newVal === true) {
					$timeout(function() {
						$scope.api.refresh();
					});
				}
			});

			self.isManager = function isManager(user) {
				for (var i in $scope.$parent.self.project.members) {
					var member = $scope.$parent.self.project.members[i];
					if (member.id && member.user.id == user && member.role == 'manager') {
						return true;
					}
				}

				return false;
			};

			$scope.$on('reload-poste-expense', function(event, params) {
				for (var i in params) {
					posteExpenseService
						.find(params[i], {
							col: self.options.col,
						})
						.then(function(res) {
							for (var j in self.posteExpenses) {
								if (self.posteExpenses[j].id == res.object.id) {
									self.posteExpenses[j] = res.object;
									break;
								}
							}
						});
				}
			});

			$scope.$watch(
				function() {
					return self.groupBy;
				},
				function(newVal, oldVal) {
					if (newVal !== oldVal) {
						$scope.__tb.loadData();
					}
				},
			);
		},
	]);
})(window.EVA.app);
