(function(app) {

    app.directive('absenceTypeSelect', [
        'absenceTypeService', 'selectFactoryService',
        function absenceTypeSelect(absenceTypeService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: absenceTypeService,
                    apiParams: {
                        col: ['id', 'name'],
                        search : {
                            data : {
                                sort: 'name',
                                order: 'asc'
                            }
                        }
                    },
                    tree: true,
                    nnTree: true
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
