<?php

namespace Field\Controller\Plugin;

use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class AllowedKeywordGroupsPlugin
 *
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class AllowedFieldGroupsPlugin extends AbstractPlugin
{
    /**
     * @var AllowedFieldGroups
     */
    protected $allowedFieldGroups;

    /**
     * @param AllowedFieldGroups $allowedFieldGroups
     */
    public function __construct(AllowedFieldGroups $allowedFieldGroups)
    {
        $this->allowedFieldGroups = $allowedFieldGroups;
    }

    public function __invoke($entity)
    {
        return $this->allowedFieldGroups->findByEntity($entity);
    }
}
