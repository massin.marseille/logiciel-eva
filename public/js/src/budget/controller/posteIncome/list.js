(function (app) {

    app.controller('filterController', [
        function filterController() {}
    ]);

    app.controller('posteIncomeListController', [
        'posteIncomeService', 'incomeService', '$scope', 'flashMessenger', 'translator', '$timeout',
        function posteIncomeListController(posteIncomeService, incomeService, $scope, flashMessenger, translator, $timeout) {
            var self = this;

            self.from   = {
                type: null,
                id  : null
            };
            self.object = null;
            self.groupBy = 'poste';
            self.cacheGroupBy = [];

            self.total = {
                id     : 'total',
                _rights: {
                    read  : false,
                    delete: false,
                    update: false
                }
            };

            self.incomeParams = {
                search: {
                    data: {
                        filters: {}
                    }
                }
            };
			self.exportModel = 'default';
            self.posteIncomes = [];
            self.options      = {
                name      : 'poste-incomes-list',
                col       : [
                    'id',
                    'name',
                    'autofinancement',
                    'amount',
                    'amountHT',
                    'amountArbo',
                    'amountHTArbo',
                    'amountAggreg',
                    'amountHTAggreg',
                    'amountArboBalance',
                    'amountArboBalanceToBePerceived',
                    'amountArboBalanceNotified',
                    'account.id',
                    'account.code',
                    'account.name',
                    'nature.id',
                    'nature.code',
                    'nature.name',
                    'organization.id',
                    'organization.code',
                    'organization.name',
					'managementUnit.id',
					'managementUnit.code',
					'managementUnit.name',
                    'destination.id',
                    'destination.code',
                    'destination.name',
                    'parent.id',
                    'parent.name',
                    'parent.account.id',
                    'parent.account.code',
                    'project.id',
                    'project.name',
                    'project.parent.id',
                    'envelope.id',
                    'envelope.name',
                    'envelope.financer.id',
                    'envelope.financer.name',
                    'incomesArbo.id',
                    'incomesArbo.name',
                    'incomesArbo.amount',
                    'incomesArbo.amountHT',
                    'incomesArbo.date',
                    'incomesArbo.type',
                    'incomesArbo.poste.id',
                    'incomesArbo.keywords',
                    'incomesArbo.convention',
                    'incomesArbo.conventionTitle',
                    'incomesArbo.tiers',
                    'incomesArbo.anneeExercice',
                    'incomesArbo.bordereau',
                    'incomesArbo.mandat',
                    'incomesArbo.complement',
                    'incomesArbo.engagement',
                    'incomesArbo.serie',
                    'incomesArbo.createdAt',
                    'incomesArbo.updatedAt',
                    'incomesArbo.age',
                    'incomesArbo.evolution',
                    'incomesArbo.transfer.id',
                    'incomesArbo.transfer.name',
                    'incomesArbo.transfer.code',
                    'incomesArbo.project.id',
                    'incomesArbo.project.name',
                    'incomesArbo.nature.id',
                    'incomesArbo.nature.code',
                    'incomesArbo.nature.name',
                    'incomesArbo.organization.id',
                    'incomesArbo.organization.code',
                    'incomesArbo.organization.name',
                    'incomesArbo.destination.id',
                    'incomesArbo.destination.code',
                    'incomesArbo.destination.name',
					'incomesArbo.managementUnit.id',
					'incomesArbo.managementUnit.code',
					'incomesArbo.managementUnit.name',
                    'caduciteStart',
                    'caduciteEnd',
                    'caduciteSendProof',
                    'arrDate',
                    'folderSendingDate',
                    'folderReceiptDate',
                    'deliberationDate',
                    'arrNumber',
                    'subventionAmount',
                    'subventionAmountHT',
                    'keywords',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort      : 'name',
                order     : 'asc',
                pager     : false,
                getItems  : function getItems(params, callback, abort) {
                    if ($scope.$parent.self[self.from.type].id || self.from.id) {
                        if (self.from.type === 'project') {
                            params.search.data.filters['project.id'] = {
                                op : 'eq',
                                val: self.object.id || self.from.id
                            };
                        }

                        params.incomeFilters = self.incomeParams.search.data.filters;
                        params.groupBy = self.groupBy;

                        posteIncomeService.findAll(params, abort.promise).then(function (res) {
                            res.rows.push(self.getTotal(res.rows));
                            callback({rows: res.rows});
                        });
                    } else {
                        callback({rows: []});
                    }
                }
            };

            self.resetIncomeFilters = function () {
                for (var i in self.incomeParams.search.data.filters) {
                    if (i !== 'id') {
                        self.incomeParams.search.data.filters[i] = {};
                    }
                }
            };

            $scope.$watch(function () { return self.posteIncomes }, function () {
                self.getTotal(self.posteIncomes);
                $scope.chart.data = self.getChartData();
            }, true);

            self.incomeTypes = [];
            self.init         = function init(from, id, types, balances) {
                self.from.type = from;
                self.from.id   = id;
                if (self.from.type === 'project') {
                    self.object = $scope.$parent.self[self.from.type]
                }

                self.incomeTypes = types;
                for (var i in types) {
                    self.options.col.push('amountArbo' + types[i].ucfirst());
                    self.options.col.push('amountHTArbo' + types[i].ucfirst());
                }

                self.posteIncomeBalances = balances;
                for (var j in balances) {
                    self.options.col.push('amountArbo' + balances[j].ucfirst());
                    self.options.col.push('amountHTArbo' + balances[j].ucfirst());
                }
            };

            self.editPosteIncome = function editPosteIncome(posteIncome) {
                $scope.$broadcast('edit-poste-income', posteIncome);
            };

            self.createPosteIncome = function createPosteIncome() {
                $scope.$broadcast('create-poste-income');
            };

            self.deletePosteIncome = function deletePosteIncome(posteIncome) {
                if (confirm(translator('poste_income_question_delete').replace('%1', posteIncome.name))) {
                    posteIncomeService.remove(posteIncome).then(function (res) {
                        if (res.success) {
                            self.posteIncomes.splice(self.posteIncomes.indexOf(posteIncome), 1);
                            flashMessenger.success(translator('poste_income_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.createIncome = function createIncome(poste) {
                if (poste.incomesArbo === null) {
                    poste.incomesArbo = [];
                }
                $scope.$broadcast('create-income', poste);
            };

            self.editIncome = function editIncome(income, poste) {
                $scope.$broadcast('edit-income', {
                    income: income,
                    poste : poste
                });
            };

            self.duplicateIncome = function (income, poste) {
                $scope.$broadcast('duplicate-income', {
                    income: income,
                    poste : poste
                });
            };

            self.deleteIncome = function deleteIncome(income, poste) {
                if (confirm(translator('income_question_delete').replace('%1', income.name))) {
                    incomeService.remove(income).then(function (res) {
                        if (res.success) {
                            var posteOldAmount    = 'amountArbo' + income.type.toCamelCase().ucfirst();
                            poste[posteOldAmount] = parseFloat(poste[posteOldAmount]) - parseFloat(income.amount);
                            poste.incomesArbo.splice(poste.incomesArbo.indexOf(income), 1);
                            flashMessenger.success(translator('income_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine         = function editLine(object) {
                if (angular.isArray(object.keywords) || object.keywords == null) {
                    object.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = object;
                self.editedLineFields = angular.copy(object);
            };

            self.saveEditedLine = function saveEditedLine(object, type, _poste) {
                var service = null;
                var cols    = [];
                var message = '';
                var pattern = /^incomesArbo./;

                if (type === 'poste') {
                    service = posteIncomeService;
                    message = 'poste_income_message_saved';
                    cols    = self.options.col;
                } else if (type === 'income') {
                    service = incomeService;
                    message = 'income_message_saved';

                    for (var i in self.options.col) {
                        if (pattern.test(self.options.col[i])) {
                            cols.push(self.options.col[i].replace('incomesArbo.', ''));
                        }
                    }
                }

                self.editedLineErrors = {};
                service.save(angular.extend({}, object, self.editedLineFields), {
                    col: cols
                }).then(function (res) {
                    if (res.success) {
                        if (type === 'income') {
                            var posteOldAmount       = 'amountArbo' + self.editedLine.type.toCamelCase().ucfirst();
                            var posteOldAmountHT     = 'amountHTArbo' + self.editedLine.type.toCamelCase().ucfirst();
                            _poste[posteOldAmount]   = parseFloat(_poste[posteOldAmount]) - parseFloat(self.editedLine.amount);
                            _poste[posteOldAmountHT] = parseFloat(_poste[posteOldAmountHT]) - parseFloat(self.editedLine.amountHT);

                            var posteNewAmount       = 'amountArbo' + res.object.type.toCamelCase().ucfirst();
                            var posteNewAmountHT     = 'amountHTArbo' + res.object.type.toCamelCase().ucfirst();
                            _poste[posteNewAmount]   = parseFloat(_poste[posteNewAmount]) + parseFloat(res.object.amount);
                            _poste[posteNewAmountHT] = parseFloat(_poste[posteNewAmountHT]) + parseFloat(res.object.amountHT);
                        }

                        object = angular.extend(object, res.object);

                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator(message));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.getTotal = function (rows) {
                self.total['amount']         = 0;
                self.total['amountHT']       = 0;
                self.total['amountArbo']     = 0;
                self.total['amountHTArbo']   = 0;
                self.total['amountAggreg']   = 0;
                self.total['amountHTAggreg'] = 0;

                for (var i in self.incomeTypes) {
                    self.total['amountArbo' + self.incomeTypes[i].ucfirst()] = 0;
                    self.total['amountHTArbo' + self.incomeTypes[i].ucfirst()] = 0;
                }

                for (i in self.posteIncomeBalances) {
                    self.total['amountArbo' + self.posteIncomeBalances[i].ucfirst()] = 0;
                    self.total['amountHTArbo' + self.posteIncomeBalances[i].ucfirst()] = 0;
                }

                for (i in rows) {
                    if (rows[i].id !== 'total') {
                        self.total['amount']         += rows[i]['amount'];
                        self.total['amountHT']       += rows[i]['amountHT'];
                        self.total['amountArbo']     += rows[i]['amountArbo'];
                        self.total['amountHTArbo']   += rows[i]['amountHTArbo'];
                        self.total['amountAggreg']   += rows[i]['amountAggreg'];
                        self.total['amountHTAggreg'] += rows[i]['amountHTAggreg'];

                        for (var j in self.incomeTypes) {
                            self.total['amountArbo' + self.incomeTypes[j].ucfirst()] += rows[i]['amountArbo' + self.incomeTypes[j].ucfirst()];
                            self.total['amountHTArbo' + self.incomeTypes[j].ucfirst()] += rows[i]['amountHTArbo' + self.incomeTypes[j].ucfirst()];
                        }

                        for (j in self.posteIncomeBalances) {
                            self.total['amountArbo' + self.posteIncomeBalances[j].ucfirst()] += rows[i]['amountArbo' + self.posteIncomeBalances[j].ucfirst()];
                            self.total['amountHTArbo' + self.posteIncomeBalances[j].ucfirst()] += rows[i]['amountHTArbo' + self.posteIncomeBalances[j].ucfirst()];
                        }
                    }
                }

                return self.total;
            };

            /*** CHART ***/
            self.getChartData = function () {
                var data = [];

                var preData = {};
                for (var i in self.posteIncomes) {
                    var posteIncomes = self.posteIncomes[i];
                    for (var j in posteIncomes.incomesArbo) {
                        var income = posteIncomes.incomesArbo[j];
                        var time   = moment(income.date, 'DD/MM/YYYY HH:mm');
                        time.set({
                            date       : 1,
                            hour       : 0,
                            minute     : 0,
                            second     : 0,
                            millisecond: 0
                        });

                        if (typeof preData[income.type] == 'undefined') {
                            preData[income.type] = {};
                        }

                        if (typeof preData[income.type][time.format('X')] == 'undefined') {
                            preData[income.type][time.format('X')] = 0;
                        }

                        preData[income.type][time.format('X')] += income.amount;
                    }
                }

                for (var type in preData) {
                    var typeData = {
                        key   : translator('income_type_' + type),
                        values: []
                    };

                    for (var time in preData[type]) {
                        typeData.values.push({
                            x: time,
                            y: preData[type][time]
                        });
                    }
                    data.push(typeData);
                }

                return data;
            };
            
            self.showExportExpenseModal = function() {
				angular.element('#exportExcelModal4').modal('show');
				self.exportFilters = {
					id: self.from.id,
					tree: 'initial',
					reference: 1,
					levels: [
						{
							value: 1,
							mode: 'parent',
							filters: {
								id: {
									op: 'eq',
									val: self.from.id,
								},
							},
						},
					],
				};
			};


			self.export = function(type) {
				if (self.exportModel == 'default'){
					$scope.__tb.exportExcel();
				}
				else {
					angular.element('#export' + type + 'Form4').submit();
				}
				angular.element('#export' + type + 'Modal4').modal('hide');
			};


            $scope.chart = {};
            $timeout(function () {
                $scope.chart = {
                    api    : {},
                    options: {
                        chart: {
                            type        : 'multiBarChart',
                            showControls: false,
                            height      : 300,
                            xAxis       : {
                                tickFormat: function (d) {
                                    return moment(d, 'X').format('MM/YYYY')
                                }
                            },
                            yAxis       : {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' €';
                                }
                            },
                        }
                    },
                    data   : []
                };
            });

            $scope.$watch('showChart', function (newVal) {
                if (newVal === true) {
                    $timeout(function () {
                        $scope.api.refresh();
                    });
                }
            });

            self.isManager = function isManager(user) {
                for (var i in $scope.$parent.self.project.members) {
                    var member = $scope.$parent.self.project.members[i];
                    if (member.id && member.user.id == user && member.role == 'manager') {
                        return true;
                    }
                }

                return false;
            };

            $scope.$on('reload-poste-income', function (event, params) {
                for (var i in params) {
                    posteIncomeService.find(params[i], {
                        col: self.options.col
                    }).then(function(res) {
                        for (var j in self.posteIncomes) {
                            if (self.posteIncomes[j].id == res.object.id) {
                                self.posteIncomes[j] = res.object;
                                break;
                            }
                        }
                    });
                }
            });

            $scope.$watch(function() { return self.groupBy }, function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    $scope.__tb.loadData();
                }
            });
        }
    ])

})(window.EVA.app);
