<?php

namespace Indicator\Controller\API;

use Core\Controller\BasicRestSyncController;
use Doctrine\ORM\QueryBuilder;

class IndicatorController extends BasicRestSyncController
{
    protected $entityClass  = 'Indicator\Entity\Indicator';
    protected $repository   = 'Indicator\Entity\Indicator';
    protected $entityChain  = 'indicator:e:indicator';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->leftJoin('object.groupIndicators', 'groupIndicators');

        //$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }

        if ( isset($data['filters']['archived'])) {
            if ($data['filters']['archived'] == 'false'){
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->orX(...[
                        'object.archived=0 OR object.archived IS NULL'
                    ])
                );
            } else {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->orX(...[
                        'object.archived=1'
                    ])
                );    
            }
        }
        if(isset($data['filters']['groupIndicatorId'])){
            $groupindicatorquery = $this->entityManager()->getrepository('Indicator\Entity\GroupIndicator')->createquerybuilder('gi');
            $indicatorIds = $groupindicatorquery->select('ind.id')
                ->join('gi.indicators', 'ind')
                ->where('gi in (:groupIndicatorId)')
                ->setParameter('groupIndicatorId', $data['filters']['groupIndicatorId'])
                ->getQuery()->getResult();

            $queryBuilder->andWhere('object.id in (:ids)')
            ->setParameter('ids', array_values($indicatorIds));
            unset($data['filters']['groupIndicatorId']);
        } else if(isset($data['filters']['campainId'])){
            $em = $this->entityManager();
            $indicatorIds = $em ->createQuery('SELECT i.primary FROM Campain\Entity\Indicator i WHERE i.campain = :campainId AND i.entity = :entityName')
            ->setParameter('campainId', $data['filters']['campainId'])
            ->setParameter('entityName', 'Indicator\Entity\Indicator')
            ->getResult();

            $queryBuilder->andWhere('object.id in (:ids)')
            ->setParameter('ids', array_values($indicatorIds)); 
            unset($data['filters']['campainId']);
        }
    }

    public function delete($id)
    {
        $response = parent::delete($id);

        if ($response->getVariable('success', false) === true) {
            $em = $this->entityManager();

            // On supprime les keywords
            $em ->createQuery('DELETE Keyword\Entity\Association e WHERE e.entity = :entity AND e.primary = :primary')
                ->setParameter('entity', $this->repository)
                ->setParameter('primary', $id)
                ->execute();
        }

        return $response;
    }
}
