<?php

return [
    'module' => [
        'export' => [
            'environments' => [
                'client'
            ],
            'active'   => true,
            'required' => true,
            'acl' => [
                'entities' => [
                    [
                        'name'  => 'export',
                        'class' => 'Export\Entity\Export',
                        'owner'    => [],
                    ],
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
