<?php

namespace Analysis\Controller;

use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use Indicator\Entity\Indicator;
use Indicator\Entity\Measure;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;
use Laminas\Router\RouteMatch;
use Laminas\Stdlib\Parameters;
use Laminas\View\Model\JsonModel;

class IndicatorController extends AbstractActionSLController

{
    public function onDispatch(MvcEvent $e)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
    }

    public function recapAction()
    {
        $filters = $this->params()->fromQuery('filters', []);
        $groupBy = $filters['group'];
        $export  = $this->params()->fromQuery('export', false);

        $data = [
            'indicators' => [],
            'years'      => [],
        ];

        $controllerManager   = $this->serviceLocator->get('ControllerManager');
        $controllerProject   = $controllerManager->get('Project\Controller\API\Project');
        $controllerIndicator = $controllerManager->get('Indicator\Controller\API\Indicator');
        $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters([
            'col'    => [
                'id',
            ],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort'    => 'id',
                    'order'   => 'asc',
                    'filters' => isset($filters['indicator']) ? $filters['indicator'] : [],
                ],
            ],
        ]));

        $routeMatch = new RouteMatch([]);
        $e          = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $controllerIndicator->setEvent($e);
        $_indicators = $controllerIndicator->dispatch($request)->getVariable('rows');
        $indicators  = [];
        foreach ($_indicators as $_indicator) {

            $indicator    = $this->entityManager()->getRepository('Indicator\Entity\Indicator')->find($_indicator['id']);
            $indicators[] = $indicator;

            $data['indicators'][$indicator->getId()] = [
                'id'        => $indicator->getId(),
                'name'      => $indicator->getName(),
                'uom'       => $indicator->getUom(),
                'type'      => $indicator->getType(),
                'indicator' => $indicator,
                'data' => [],
            ];

            
            $data['indicators'][$indicator->getId()]['data'][Measure::PERIOD_INTERMEDIATE] = [];
        }

        $isMaster = $this->environment()->getClient()->isMaster();
        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
            $clients[] = $this->environment()->getClient();
            if (!isset($filters['project'])) {
                $filters['project'] = [];
            }

            $filters['project']['networkAccessible'] = [
                'op'  => 'eq',
                'val' => 1,
            ];
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach ($clients as $client) {
            $bypass = false;

            if (!isset($filters['measure'])) {
                $measuresFilters['measure'] = [];
            } else {
                $measuresFilters = $filters['measure'];
            }

            if (!$isMaster) {
                $measuresFilters['indicator.id'] = [
                    'op'  => 'eq',
                    'val' => array_keys($data['indicators']),
                ];
            } else {
                $request = new Request();
                $request->setMethod(Request::METHOD_GET);
                $request->setQuery(new Parameters([
                    'col'    => [
                        'id',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'id',
                            'order'   => 'asc',
                            'filters' => [
                                'master' => [
                                    'op'  => 'eq',
                                    'val' => array_keys($data['indicators']),
                                ],
                            ],
                        ],
                    ],
                ]));

                $routeMatch = new RouteMatch([]);
                $e          = new MvcEvent();
                $e->setRouteMatch($routeMatch);
                $e->setParam('master', $isMaster);
                $controllerIndicator->setEvent($e);

                $controllerIndicator->setClient($client);
                $_indicators = $controllerIndicator->dispatch($request)->getVariable('rows');

                $measuresFilters['indicator.id'] = [
                    'op'  => 'eq',
                    'val' => [],
                ];

                foreach ($_indicators as $_indicator) {
                    $measuresFilters['indicator.id']['val'][] = $_indicator['id'];
                }
            }

            if (isset($filters['project']) && !$isMaster) {
                $request = new Request();
                $request->setMethod(Request::METHOD_GET);
                $request->setQuery(new Parameters([
                    'col'    => [
                        'id', 'code', 'name',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'id',
                            'order'   => 'asc',
                            'filters' => $filters['project'],
                        ],
                    ],
                ]));

                $routeMatch = new RouteMatch([]);
                $e          = new MvcEvent();
                $e->setRouteMatch($routeMatch);
                $e->setParam('master', $isMaster);
                $controllerProject->setEvent($e);

                $controllerProject->setClient($client);
                $projects = $controllerProject->dispatch($request)->getVariable('rows');

                $measuresFilters['project.id'] = [
                    'op'  => 'eq',
                    'val' => [],
                ];

                foreach ($projects as $project) {
                    $measuresFilters['project.id']['val'][] = $project['id'];
                }
            }


            $request = new Request();
            $request->setMethod(Request::METHOD_GET);
            $request->setQuery(new Parameters([
                'col'    => [
                    'id',
                    'indicator.id',
                    'indicator.master',
                    'period',
                    'type',
                    'date',
                    'value',
                    'territories'
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'id',
                        'order'   => 'asc',
                        'filters' => $measuresFilters,
                    ],
                ],
            ]));

            $routeMatch = new RouteMatch([]);
            $e          = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $controllerMeasure->setEvent($e);

            $controllerMeasure->setClient($client);
            $measures = $controllerMeasure->dispatch($request)->getVariable('rows');
            foreach ($measures as $measure) {
                $indicator = $isMaster ? $measure['indicator']['master'] : $measure['indicator']['id'];

                
                    $date = \DateTime::createFromFormat('d/m/Y H:i', $measure['date']);
                    $year = $date->format('Y');
                    if (!isset($data['indicators'][$indicator]['data'][Measure::PERIOD_INTERMEDIATE][$year])) {
                        $data['indicators'][$indicator]['data'][Measure::PERIOD_INTERMEDIATE][$year] = [
                            'number' => 0
                        ];
                        foreach (Measure::getTypes() as $type) {
                            $data['indicators'][$indicator]['data'][Measure::PERIOD_INTERMEDIATE][$year][$type] = [
                                'value'    => null,
                                'measures' => [],
                            ];
                        }
                        $data['years'][] = $year;
                    }
                    $data['indicators'][$indicator]['data'][Measure::PERIOD_INTERMEDIATE][$year][$measure['type']]['measures'][] = $measure;
                    $data['indicators'][$indicator]['data'][Measure::PERIOD_INTERMEDIATE][$year]['number']++;
            }
        }


        if ($groupBy == 'territory') {
            $data['territories'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                foreach ($dataIndicator['data'] as $period => $dataPeriod) {
                    foreach ($dataPeriod as $year => $dataYear) {
                        foreach (Measure::getTypes() as $type) {
                            foreach ($dataYear[$type]['measures'] as $measure) {
                                if ($measure['territories']) {
                                    foreach ($measure['territories'] as $territory) {
                                        $identifier = $id . '-' . $territory['id'];

                                        if (!isset($data['territories'][$identifier])) {
                                            $data['territories'][$identifier] = $dataIndicator;
                                            $data['territories'][$identifier]['territory'] = $territory['name'];

                                            foreach ($dataIndicator['data'] as $_period => $_dataPeriod) {
                                                foreach ($_dataPeriod as $_year => $_dataYear) {
                                                    $data['territories'][$identifier]['data'][$_period][$_year]['number'] = 0;
                                                    foreach (Measure::getTypes() as $_type) {
                                                        $data['territories'][$identifier]['data'][$_period][$_year][$_type]['measures'] = [];
                                                    }
                                                }
                                            }
                                        }

                                        $data['territories'][$identifier]['data'][$period][$year][$type]['measures'][] = $measure;
                                        $data['territories'][$identifier]['data'][$period][$year]['number']++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            usort($data['territories'], function ($a, $b) {
                $groupA = $a['territory'];
                $groupB = $b['territory'];

                return $groupA < $groupB ? -1 : 1;
            });

            $data['indicators'] = $data['territories'];
            unset($data['territories']);
        }

        foreach ($data['indicators'] as $identifier => $dataIndicator) {
            //$identifier = $_indicator->getId();
            $indicator  = $dataIndicator['indicator'];
            $hasValue = false;
            
            foreach ($data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE] as $year => $yearData) {
                foreach (Measure::getTypes() as $type) {
                    $measures = $yearData[$type]['measures'];
                    $value    = $indicator->getValue($measures);
                    if ($value !== null){
                        $hasValue = true;
                    }
                    $data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year][$type]['value']   = $value;
                    $data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year][$type]['content'] = $value;
                    if ($indicator->getType() === Indicator::TYPE_FIXED) {
                        $data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year][$type]['content'] = $indicator->getFixedValue($value);
                    }
                    unset($data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year][$type]['measures']);
                }

                if ($data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year]['target']['value'] > 0) {
                    $data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year]['ratio'] = round($data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year]['done']['value'] * 100 / $data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year]['target']['value']);
                } else {
                    $data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year]['ratio'] = $data['indicators'][$identifier]['data'][Measure::PERIOD_INTERMEDIATE][$year]['done']['value'] > 0 ? 100 : null;
                }
            }
            
            unset($data['indicators'][$identifier]['indicator']);
            if (!$hasValue){
                unset($data['indicators'][$identifier]);    
            }
        }
        $data['years'] = array_unique($data['years']);
        sort($data['years']);

        if ($groupBy == 'keyword') {
            $data['keywords'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                $associations = $this->entityManager()->getRepository('Keyword\Entity\Association')->findBy([
                    'primary' => $id,
                    'entity'  => 'Indicator\Entity\Indicator',
                ]);
                foreach ($associations as $association) {
                    $keyword = $association->getKeyword();
                    $group   = $keyword->getGroup();

                    if (
                        !$group->isArchived()
                        && !$keyword->isArchived()
                    ) {
                        $dataIndicator['group']   = $group->getName();
                        $dataIndicator['keyword'] = $keyword->getName();

                        $data['keywords'][] = $dataIndicator;
                    }
                }

                /*if (!$associations) {
                    $dataIndicator['group']   = '';
                    $dataIndicator['keyword'] = '';
                    $data['keywords'][]       = $dataIndicator;
                }*/
            }

            usort($data['keywords'], function ($a, $b) {
                $groupA = $a['group'];
                $groupB = $b['group'];

                $keywordA = $a['keyword'];
                $keywordB = $b['keyword'];

                if ($groupA == $groupB) {
                    if ($keywordA == $keywordB) {
                        return 0;
                    }

                    return $keywordA < $keywordB ? -1 : 1;
                }

                return $groupA < $groupB ? -1 : 1;
            });

            $data['indicators'] = $data['keywords'];
            unset($data['keywords']);
        }
        if (isset($data['clients'])){
            foreach($data['clients'] as $key => $client) {
                $data['clients'][$key]['has'] = [];
                $data['clients'][$key]['has']['start'] = false;
                $data['clients'][$key]['has']['end'] = false;
                foreach ($data['years'] as $year) {
                    $data['clients'][$key]['has'][$year] = false;
                }
                foreach ($data['indicators'] as $indicator) {
                    $indicatorData = $indicator['data'][$data['clients'][$key]['id']];
                    if ($indicatorData['start']['done']['value'] || $indicatorData['start']['target']['value']) {
                        $data['clients'][$key]['has']['start'] = true;
                    }
                    if ($indicatorData['end']['done']['value'] || $indicatorData['end']['target']['value']) {
                        $data['clients'][$key]['has']['end'] = true;
                    }
                    foreach ($data['years'] as $year) {
                        if (isset($indicatorData['intermediate'][$year])
                            && $indicatorData['intermediate'][$year]
                            && ($indicatorData['intermediate'][$year]['done']['value']
                                || $indicatorData['intermediate'][$year]['target']['value'])) {
                            $data['clients'][$key]['has'][$year] = true;
                        }
                    }
                }
            }
            foreach($data['clients'] as $key => $client) {
                $data['clients'][$key]['total'] = 0;
                foreach($data['clients'][$key]['has'] as $keyHas => $has){
                    if ($data['clients'][$key]['has'][$keyHas])
                        $data['clients'][$key]['total']++;
                }
            }
        }

        if ($export) {
            $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
            $translator          = $helperPluginManager->get('translate');

            $xlsLines = [];

            $header    = [];
            $subHeader = [];
            if ($groupBy == 'keyword') {
                $header[]    = '';
                $header[]    = '';
                $subHeader[] = ucfirst($translator->__invoke('group_entity'));
                $subHeader[] = ucfirst($translator->__invoke('keyword_entity'));
            }
            if ($groupBy == 'territory') {
                $header[]    = '';
                $subHeader[] = ucfirst($translator->__invoke('territory_entity'));
            }
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_entity'));

            $header[]    = ucfirst($translator->__invoke('measure_period_start'));
            $header[]    = '';
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
            $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
            $subHeader[] = '%';

            foreach ($data['years'] as $year) {
                $header[]    = $year;
                $header[]    = '';
                $header[]    = '';
                $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                $subHeader[] = '%';
            }

            $header[]    = ucfirst($translator->__invoke('measure_period_end'));
            $header[]    = '';
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
            $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
            $subHeader[] = '%';

            $xlsLines[] = $header;
            $xlsLines[] = $subHeader;

            foreach ($data['indicators'] as $indicatorData) {
                $indicatorLine = [];
                if ($groupBy == 'keyword') {
                    $indicatorLine[] = $indicatorData['group'];
                    $indicatorLine[] = $indicatorData['keyword'];
                }
                if ($groupBy == 'territory') {
                    $indicatorLine[] = $indicatorData['territory'];
                }
                $indicatorLine[] = $indicatorData['name'];

                $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['start']['target']['content']['text'] : $indicatorData['data']['start']['target']['value'];
                $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['start']['done']['content']['text'] : $indicatorData['data']['start']['done']['value'];
                $indicatorLine[] = $indicatorData['data']['start']['ratio'] !== null ? $indicatorData['data']['start']['ratio'] . '%' : '';

                foreach ($data['years'] as $year) {
                    if (isset($indicatorData['data']['intermediate'][$year])) {
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['intermediate'][$year]['target']['content']['text'] : $indicatorData['data']['intermediate'][$year]['target']['value'];
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['intermediate'][$year]['done']['content']['text'] : $indicatorData['data']['intermediate'][$year]['done']['value'];
                        $indicatorLine[] = $indicatorData['data']['intermediate'][$year]['ratio'] !== null ? $indicatorData['data']['intermediate'][$year]['ratio'] . '%' : '';
                    } else {
                        $indicatorLine[] = '';
                        $indicatorLine[] = '';
                        $indicatorLine[] = '';
                    }
                }

                $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['end']['target']['content']['text'] : $indicatorData['data']['end']['target']['value'];
                $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data']['end']['done']['content']['text'] : $indicatorData['data']['end']['done']['value'];
                $indicatorLine[] = $indicatorData['data']['end']['ratio'] !== null ? $indicatorData['data']['end']['ratio'] . '%' : '';

                $xlsLines[] = $indicatorLine;
            }

            ExcelExporter::download($xlsLines);
            return null;
        } else {
            return new JsonModel($data);
        }
    }

    public function getClientsAction(){
        $isMaster = $this->environment()->getClient()->isMaster();
        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
            $clients[] = $this->environment()->getClient();
        } else {
            $clients = [$this->environment()->getClient()];
        }

        foreach($clients as $client) {
            $clientMeasures[] = array('id' => $client->getId(), 'name' => $client->getName());
        }
        
        return new JsonModel(array('rows' => $clientMeasures));
    }

    public function recapWithMeasuresAction()
    {
        $filters         = $this->params()->fromQuery('filters', []);

        $export          = $this->params()->fromQuery('export', false);
        $measuresFilters = [];
        $controllerManager   = $this->serviceLocator->get('ControllerManager');
        $controllerProject   = $controllerManager->get('Project\Controller\API\Project');
        $controllerIndicator = $controllerManager->get('Indicator\Controller\API\Indicator');
        $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');

        $isMaster = $this->environment()->getClient()->isMaster();
        if ($isMaster) {
            $clients = $this->environment()->getClient()->getNetwork()->getClients();
            $clients[] = $this->environment()->getClient();
            if (isset($filters['project'])) {
                $filters['project']['networkAccessible'] = [
                    'op'  => 'eq',
                    'val' => 1,
                ];
            }
        } else {
            $clients = [$this->environment()->getClient()];
        }

        $measures = [];

        foreach ($clients as $client) {
            if ((isset($filters['measure']['client']) 
                && $filters['measure']['client']['op'] == 'neq'
                && in_array($client->getId(), $filters['measure']['client']['val']))
                ||
                (isset($filters['measure']['client']) 
                && $filters['measure']['client']['op'] == 'eq'
                && !in_array($client->getId(), $filters['measure']['client']['val']))
                ||
                (isset($filters['project']['_client']) 
                && $filters['project']['_client']['op'] == 'neq'
                && in_array($client->getId(), $filters['project']['_client']['val']))
                ||
                (isset($filters['project']['_client']) 
                && $filters['project']['_client']['op'] == 'eq'
                && !in_array($client->getId(), $filters['project']['_client']['val'])))
            {
                continue;
            }
            if (!isset($filters['measure'])) {
                $measuresFilters['measure'] = [];
            } else {
                foreach($filters['measure'] as $key => $filter){
                    if ($key != "client") {
                        $measuresFilters[$key] = $filter;
                    }    
                }
            }

            // project filters
            if (isset($filters['project'])) {
                $projectRequest = new Request();
                $projectRequest->setMethod(Request::METHOD_GET);
                $projectRequest->setQuery(new Parameters(([
                    'col'    => [
                        'id',
                        'name',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'name',
                            'order'   => 'asc',
                            'filters' => $filters['project'],
                        ],
                    ],
                ])));

                $routeMatch = new RouteMatch([]);
                $e          = new MvcEvent();
                $e->setRouteMatch($routeMatch);
                $controllerProject->setEvent($e);
                $controllerProject->setClient($client);
                $projects = $controllerProject->dispatch($projectRequest)->getVariable('rows');

                if (isset($projects)) {
                    $measuresFilters['project.id'] = [
                        'op'  => 'eq',
                        'val' => [],
                    ];
                    foreach ($projects as $project) {
                        $measuresFilters['project.id']['val'][] = $project['id'];
                    }
                }
            }
            // end of project filters

            // indicator filters
            if (isset($filters['indicator'])) {
                $indicatorRequest = new Request();
                $indicatorRequest->setMethod(Request::METHOD_GET);
                $indicatorRequest->setQuery(new Parameters([
                    'col'    => [
                        'id',
                        'name',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'name',
                            'order'   => 'asc',
                            'filters' => $filters['indicator'],
                        ],
                    ],
                ]));
                $routeMatch = new RouteMatch([]);
                $e          = new MvcEvent();
                $e->setRouteMatch($routeMatch);
                $controllerIndicator->setEvent($e);
                $controllerIndicator->setClient($client);
                $indicators = $controllerIndicator->dispatch($indicatorRequest)->getVariable('rows');

                if (isset($indicators)) {
                    $measuresFilters['indicator.id'] = [
                        'op'  => 'eq',
                        'val' => [],
                    ];
                    foreach ($indicators as $indicator) {
                        $measuresFilters['indicator.id']['val'][] = $indicator['id'];
                    }
                }
            }
            // end of indicator filters     
            if (isset($filters['measure']) && !isset($filters['measure']['client'])) {
                $measuresFilters += $filters['measure'];
            }

            // measures request
            $measureRequest = new Request();
            $measureRequest->setMethod(Request::METHOD_GET);
            $measureRequest->setQuery(new Parameters([
                'col'    => [
                    'id',
                    'fixedValue',
                    'period',
                    'type',
                    'start',
                    'date',
                    'comment',
                    'source',
                    'indicator.id',
                    'indicator.name',
                    'indicator.description',
                    'indicator.uom',
                    'project.id',
                    'project.name',
                    'territories',
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'indicator.id',
                        'order'   => 'asc',
                        'filters' => $measuresFilters,
                    ],
                ],
            ]));
            $routeMatch = new RouteMatch([]);
            $e          = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $controllerMeasure->setEvent($e);
            $controllerMeasure->setClient($client);
            $clientMeasures = $controllerMeasure->dispatch($measureRequest)->getVariable('rows');
            for($i = 0; isset($clientMeasures[$i]); $i++){
                $clientMeasures[$i]['client'] = array('id' => $client->getId(), 'name' => $client->getName());
            }

            if (isset($clientMeasures)) {
                $measures = array_merge($measures, $clientMeasures);
            }
        }


        if ($export) {
            $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
            $translator          = $helperPluginManager->get('translate');

            $xlsLines = [];
            $header   = [];

            $header[] = ucfirst($translator->__invoke('indicator_entity'));
            $header[] = ucfirst($translator->__invoke('indicator_field_description'));
            $header[] = ucfirst($translator->__invoke('measure_field_period'));
            $header[] = ucfirst($translator->__invoke('measure_field_type'));
            $header[] = ucfirst($translator->__invoke('measure_field_date'));
            $header[] = ucfirst($translator->__invoke('measure_field_value'));
            $header[] = ucfirst($translator->__invoke('measure_field_project'));
            $header[] = ucfirst($translator->__invoke('measure_field_territories'));
            $header[] = ucfirst($translator->__invoke('measure_field_comment'));
            $header[] = ucfirst($translator->__invoke('measure_field_source'));

            $xlsLines[] = $header;

            $measuresGroupByIndicator = [];
            foreach ($measures as $measure) {
                $index   = -1;
                $inArray = false;

                foreach ($measuresGroupByIndicator as $item) {
                    if (isset($item['name']) && $item['name'] == $measure['indicator']['name']) {
                        $inArray = true;
                    }
                    $index++;
                }

                $territories = '';
                if ($measure['territories']) {
                    foreach ($measure['territories'] as $territory) {
                        $territories .= $territory['name'] . ' ';
                    }
                }

                if (!$inArray) {
                    $measuresGroupByIndicator[] = [
                        'name'     => $measure['indicator']['name'],
                        'description'     => $measure['indicator']['description'],
                        'uom'      => $measure['indicator']['uom'],
                        'measures' => [
                            [
                                'period'      => $measure['period'],
                                'type'        => $measure['type'],
                                'date'        => $measure['date'],
                                'value'       => $measure['fixedValue']['text'],
                                'project'     => $measure['project']['name'],
                                'territories' => $territories,
                                'comment'     => $measure['comment'],
                                'source'      => $measure['source'],
                            ],
                        ],
                    ];
                } else {
                    $measuresGroupByIndicator[$index]['measures'][] = [
                        'period'      => $measure['period'],
                        'type'        => $measure['type'],
                        'date'        => $measure['date'],
                        'value'       => $measure['fixedValue']['text'],
                        'project'     => $measure['project']['name'],
                        'territories' => $territories,
                        'comment'     => $measure['comment'],
                        'source'      => $measure['source'],
                    ];
                }
            }

            foreach ($measuresGroupByIndicator as $indicator) {

                $indicatorLine   = [];
                $indicatorLine[] = $indicator['name'] . ($indicator['uom'] !== null ? ' (' . $indicator['uom'] . ')' : '');
                $indicatorLine[] = $indicator['description'];
                $xlsLines[]      = $indicatorLine;

                foreach ($indicator['measures'] as $measure) {
                    $measureLine   = [];
                    $measureLine[] = '';
                    $measureLine[] = '';
                    $measureLine[] = $translator->__invoke('measure_period_' . $measure['period']);
                    $measureLine[] = $translator->__invoke('measure_type_' . $measure['type']);
                    $measureLine[] = \DateTime::createFromFormat('d/m/Y H:i', $measure['date'])->format('d/m/Y');
                    $measureLine[] = $measure['value'];
                    $measureLine[] = $measure['project'] !== null ? $measure['project'] : '';
                    $measureLine[] = $measure['territories'];
                    $measureLine[] = $measure['comment'] !== null ? preg_replace('/[^A-Za-z0-9\-2éèàùêô]+/', ' ',  $measure['comment']) : '';
                    $measureLine[] = $measure['source'] !== null ? preg_replace('/[^A-Za-z0-9\-2éèàùêô]+/', ' ', $measure['source']) : '';
                    $xlsLines[]    = $measureLine;
                }
            }

            ExcelExporter::download($xlsLines);

            return null;
        } else {
            $data['measures']   = $measures;
            return new JsonModel($data);
        }
    }

    public function recapByClientAction()
    {
        $filters = $this->params()->fromQuery('filters', []);
        $groupBy = $filters['group'];
        $export  = $this->params()->fromQuery('export', false);

        $data = [
            'indicators' => [],
            'years'      => [],
            'clients'    => [],
        ];

        $controllerManager   = $this->serviceLocator->get('ControllerManager');
        $controllerProject   = $controllerManager->get('Project\Controller\API\Project');
        $controllerIndicator = $controllerManager->get('Indicator\Controller\API\Indicator');
        $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');

        $request = new Request();
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters([
            'col'    => [
                'id',
            ],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort'    => 'id',
                    'order'   => 'asc',
                    'filters' => isset($filters['indicator']) ? $filters['indicator'] : [],
                ],
            ],
        ]));

        $routeMatch = new RouteMatch([]);
        $e          = new MvcEvent();
        $e->setRouteMatch($routeMatch);
        $controllerIndicator->setEvent($e);
        $_indicators = $controllerIndicator->dispatch($request)->getVariable('rows');
        $indicators  = [];
        $clients = $this->environment()->getClient()->getNetwork()->getClients();
        foreach ($_indicators as $_indicator) {

            $indicator    = $this->entityManager()->getRepository('Indicator\Entity\Indicator')->find($_indicator['id']);
            $indicators[] = $indicator;

            $data['indicators'][$indicator->getId()] = [
                'id'                => $indicator->getId(),
                'name'              => $indicator->getName(),
                'uom'               => $indicator->getUom(),
                'type'              => $indicator->getType(),
                'description'       => $indicator->getDescription(),
                'method'            => $indicator->getMethod(),
                'definition'        => $indicator->getDefinition(),
                'interpretation'    => $indicator->getInterpretation(),
                'indicator'         => $indicator,
                'data'              => [],
            ];

            foreach ($clients as $client) {
                $data['indicators'][$indicator->getId()]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE] = [];
            }
        }

        foreach ($clients as $client) {
            $bypass = false;
            if (isset($filters['measure']['client']) 
                && $filters['measure']['client']['op'] == 'neq'
                && in_array($client->getId(), $filters['measure']['client']['val']))
            {
                continue;
            }
            if (isset($filters['measure']['client']) 
                && $filters['measure']['client']['op'] == 'eq'
                && !in_array($client->getId(), $filters['measure']['client']['val']))
            {
                continue;
            }
            $data['clients'][$client->getId()] = array('id' => $client->getId(), 'name' => $client->getName());

            if (!isset($filters['measure'])) {
                $measuresFilters['measure'] = [];
            } else {
                foreach($filters['measure'] as $key => $filter){
                    if ($key != "client") {
                        $measuresFilters[$key] = $filter;
                    }    
                }
            }


            $request = new Request();
            $request->setMethod(Request::METHOD_GET);
            $request->setQuery(new Parameters([
                'col'    => [
                    'id',
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'id',
                        'order'   => 'asc',
                        'filters' => [
                            'master' => [
                                'op'  => 'eq',
                                'val' => array_keys($data['indicators']),
                            ],
                        ],
                    ],
                ],
            ]));

            $routeMatch = new RouteMatch([]);
            $e          = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $e->setParam('master', true);
            $controllerIndicator->setEvent($e);

            $controllerIndicator->setClient($client);
            $_indicators = $controllerIndicator->dispatch($request)->getVariable('rows');

            $measuresFilters['indicator.id'] = [
                'op'  => 'eq',
                'val' => [],
            ];

            foreach ($_indicators as $_indicator) {
                $measuresFilters['indicator.id']['val'][] = $_indicator['id'];
            }

            $request = new Request();
            $request->setMethod(Request::METHOD_GET);
            $request->setQuery(new Parameters([
                'col'    => [
                    'id',
                    'indicator.id',
                    'indicator.master',
                    'period',
                    'type',
                    'date',
                    'value',
                    'territories'
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'id',
                        'order'   => 'asc',
                        'filters' => $measuresFilters,
                    ],
                ],
            ]));

            $routeMatch = new RouteMatch([]);
            $e          = new MvcEvent();
            $e->setRouteMatch($routeMatch);
            $controllerMeasure->setEvent($e);

            $controllerMeasure->setClient($client);
            $measures = $controllerMeasure->dispatch($request)->getVariable('rows');
            foreach ($measures as $measure) {
                //TODO not sure here
                // $indicator = $isMaster ? $measure['indicator']['master'] : $measure['indicator']['id'];
                $indicator = $measure['indicator']['master'];

                $date = \DateTime::createFromFormat('d/m/Y H:i', $measure['date']);
                $year = $date->format('Y');
                if (!isset($data['indicators'][$indicator]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year])) {
                    $data['indicators'][$indicator]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year] = [
                        'number' => 0
                    ];
                    foreach (Measure::getTypes() as $type) {
                        $data['indicators'][$indicator]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year][$type] = [
                            'value'    => null,
                            'measures' => [],
                        ];
                    }
                    $data['years'][] = $year;
                }
                $data['indicators'][$indicator]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year][$measure['type']]['measures'][] = $measure;
                $data['indicators'][$indicator]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year]['number']++;
            }
        }

        if ($groupBy == 'territory') {
            $data['territories'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                foreach ($dataIndicator['data'] as $period => $dataPeriod) {
                    foreach ($dataPeriod as $year => $dataYear) {
                        foreach (Measure::getTypes() as $type) {
                            foreach ($dataYear[$type]['measures'] as $measure) {
                                if ($measure['territories']) {
                                    foreach ($measure['territories'] as $territory) {
                                        $identifier = $id . '-' . $territory['id'];

                                        if (!isset($data['territories'][$identifier])) {
                                            $data['territories'][$identifier] = $dataIndicator;
                                            $data['territories'][$identifier]['territory'] = $territory['name'];

                                            foreach ($dataIndicator['data'] as $_period => $_dataPeriod) {
                                                if ($_period === Measure::PERIOD_INTERMEDIATE) {
                                                    foreach ($_dataPeriod as $_year => $_dataYear) {
                                                        $data['territories'][$identifier]['data'][$_period][$_year]['number'] = 0;
                                                        foreach (Measure::getTypes() as $_type) {
                                                            $data['territories'][$identifier]['data'][$_period][$_year][$_type]['measures'] = [];
                                                        }
                                                    }
                                                } else {
                                                    foreach (Measure::getTypes() as $_type) {
                                                        $data['territories'][$identifier]['data'][$_period][$_type]['measures'] = [];
                                                    }
                                                }
                                            }
                                        }

                                        $data['territories'][$identifier]['data'][$period][$year][$type]['measures'][] = $measure;
                                        $data['territories'][$identifier]['data'][$period][$year]['number']++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            usort($data['territories'], function ($a, $b) {
                $groupA = $a['territory'];
                $groupB = $b['territory'];

                return $groupA < $groupB ? -1 : 1;
            });
            unset($data['territories']);
        }


        foreach ($data['indicators'] as $identifier => $dataIndicator) {
            foreach($clients as $client) {
                //$identifier = $_indicator->getId();
                $indicator  = $dataIndicator['indicator'];
                if ($indicator != null){
                    foreach ($data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE] as $year => $yearData) {
                        foreach (Measure::getTypes() as $type) {
                            $measures = $yearData[$type]['measures'];
                            $value    = $indicator->getValue($measures);

                            $data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year][$type]['value']   = $value;
                            $data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year][$type]['content'] = $value;
                            if ($indicator->getType() === Indicator::TYPE_FIXED) {
                                $data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year][$type]['content'] = $indicator->getFixedValue($value);
                            }

                            unset($data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year][$type]['measures']);
                        }

                        if ($data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year]['target']['value'] > 0) {
                            $data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year]['ratio'] = round($data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year]['done']['value'] * 100 / $data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year]['target']['value']);
                        } else {
                            $data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year]['ratio'] = $data['indicators'][$identifier]['data'][$client->getId()][Measure::PERIOD_INTERMEDIATE][$year]['done']['value'] > 0 ? 100 : null;
                        }
                    }
                    unset($data['indicators'][$identifier]['indicator']);
                }

            }
        }

        $data['years'] = array_unique($data['years']);
        sort($data['years']);

        if ($groupBy == 'keyword') {
            $data['keywords'] = [];
            foreach ($data['indicators'] as $id => $dataIndicator) {
                $associations = $this->entityManager()->getRepository('Keyword\Entity\Association')->findBy([
                    'primary' => $id,
                    'entity'  => 'Indicator\Entity\Indicator',
                ]);
                foreach ($associations as $association) {
                    $keyword = $association->getKeyword();
                    $group   = $keyword->getGroup();

                    if (
                        !$group->isArchived()
                        && !$keyword->isArchived()
                    ) {
                        $dataIndicator['group']   = $group->getName();
                        $dataIndicator['keyword'] = $keyword->getName();

                        $data['keywords'][] = $dataIndicator;
                    }
                }

                /*if (!$associations) {
                    $dataIndicator['group']   = '';
                    $dataIndicator['keyword'] = '';
                    $data['keywords'][]       = $dataIndicator;
                }*/
            }

            usort($data['keywords'], function ($a, $b) {
                $groupA = $a['group'];
                $groupB = $b['group'];

                $keywordA = $a['keyword'];
                $keywordB = $b['keyword'];

                if ($groupA == $groupB) {
                    if ($keywordA == $keywordB) {
                        return 0;
                    }

                    return $keywordA < $keywordB ? -1 : 1;
                }

                return $groupA < $groupB ? -1 : 1;
            });

            $data['indicators'] = $data['keywords'];
            unset($data['keywords']);
        }
        foreach($data['clients'] as $key => $client) {
            $data['clients'][$key]['has'] = [];
            $data['clients'][$key]['has']['start'] = false;
            $data['clients'][$key]['has']['end'] = false;
            foreach ($data['years'] as $year) {
                $data['clients'][$key]['has'][$year] = false;
            }
            foreach ($data['indicators'] as $indicator) {
                $indicatorData = $indicator['data'][$data['clients'][$key]['id']];
                if ($indicatorData['start']['done']['value'] || $indicatorData['start']['target']['value']) {
                    $data['clients'][$key]['has']['start'] = true;
                }
                if ($indicatorData['end']['done']['value'] || $indicatorData['end']['target']['value']) {
                    $data['clients'][$key]['has']['end'] = true;
                }
                foreach ($data['years'] as $year) {
                    if (isset($indicatorData['intermediate'][$year])
                        && $indicatorData['intermediate'][$year]
                        && ($indicatorData['intermediate'][$year]['done']['value']
                            || $indicatorData['intermediate'][$year]['target']['value'])) {
                        $data['clients'][$key]['has'][$year] = true;
                    }
                }
            }
        }
        foreach($data['clients'] as $key => $client) {
            $data['clients'][$key]['total'] = 0;
            foreach($data['clients'][$key]['has'] as $keyHas => $has){
                if ($data['clients'][$key]['has'][$keyHas])
                    $data['clients'][$key]['total']++;
            }
        }

        if ($export) {
            $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
            $translator          = $helperPluginManager->get('translate');

            $xlsLines = [];

            $aboveHeader = [];
            $header    = [];
            $subHeader = [];
            if ($groupBy == 'keyword') {
                $aboveHeader[] = '';
                $aboveHeader[] = '';
                $header[]    = '';
                $header[]    = '';
                $subHeader[] = ucfirst($translator->__invoke('group_entity'));
                $subHeader[] = ucfirst($translator->__invoke('keyword_entity'));
            }
            if ($groupBy == 'territory') {
                $aboveHeader[] = '';
                $header[]    = '';
                $subHeader[] = ucfirst($translator->__invoke('territory_entity'));
            }
            $aboveHeader[] = '';
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_entity'));

            $aboveHeader[] = '';
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_description'));

            $aboveHeader[] = '';
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_definition'));

            $aboveHeader[] = '';
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_method'));

            $aboveHeader[] = '';
            $header[]    = '';
            $subHeader[] = ucfirst($translator->__invoke('indicator_field_interpretation'));

            foreach($data['clients'] as $client){
                if ($client['total'] > 0){
                    $aboveHeader[] = $client['name'];
                    $aboveHeader[] = '';
                    $aboveHeader[] = '';
                    $header[]    = ucfirst($translator->__invoke('measure_period_start'));
                    $header[]    = '';
                    $header[]    = '';
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                    $subHeader[] = '%';

                    foreach ($data['years'] as $year) {
                        if ($client['has'][$year]) {
                            $aboveHeader[] = '';
                            $aboveHeader[] = '';
                            $aboveHeader[] = '';
                            $header[]    = $year;
                            $header[]    = '';
                            $header[]    = '';
                            $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                            $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                            $subHeader[] = '%';
                        }
                    }

                    $aboveHeader[] = '';
                    $aboveHeader[] = '';
                    $aboveHeader[] = '';
                    $header[]    = ucfirst($translator->__invoke('measure_period_end'));
                    $header[]    = '';
                    $header[]    = '';
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_target'));
                    $subHeader[] = ucfirst($translator->__invoke('measure_type_done'));
                    $subHeader[] = '%';
                }
            }
            $xlsLines[] = $aboveHeader;
            $xlsLines[] = $header;
            $xlsLines[] = $subHeader;

            foreach ($data['indicators'] as $indicatorData) {
                $indicatorLine = [];
                if ($groupBy == 'keyword') {
                    $indicatorLine[] = $indicatorData['group'];
                    $indicatorLine[] = $indicatorData['keyword'];
                }
                if ($groupBy == 'territory') {
                    $indicatorLine[] = $indicatorData['territory'];
                }
                $indicatorLine[] = $indicatorData['name'];
                $indicatorLine[] = $indicatorData['description'];
                $indicatorLine[] = $indicatorData['definition'];
                $indicatorLine[] = $indicatorData['method'];
                $indicatorLine[] = $indicatorData['interpretation'];
                foreach($data['clients'] as $client) {
                    if ($client['total'] > 0){
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['start']['target']['content']['text'] : $indicatorData['data'][$client['id']]['start']['target']['value'];
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['start']['done']['content']['text'] : $indicatorData['data'][$client['id']]['start']['done']['value'];
                        $indicatorLine[] = $indicatorData['data'][$client['id']]['start']['ratio'] !== null ? $indicatorData['data'][$client['id']]['start']['ratio'] . '%' : '';

                        foreach ($data['years'] as $year) {
                            if ($client['has'][$year]){
                                if (isset($indicatorData['data'][$client['id']]['intermediate'][$year])) {
                                    $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['intermediate'][$year]['target']['content']['text'] : $indicatorData['data'][$client['id']]['intermediate'][$year]['target']['value'];
                                    $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['intermediate'][$year]['done']['content']['text'] : $indicatorData['data'][$client['id']]['intermediate'][$year]['done']['value'];
                                    $indicatorLine[] = $indicatorData['data'][$client['id']]['intermediate'][$year]['ratio'] !== null ? $indicatorData['data'][$client['id']]['intermediate'][$year]['ratio'] . '%' : '';
                                } else {
                                    $indicatorLine[] = '';
                                    $indicatorLine[] = '';
                                    $indicatorLine[] = '';
                                }
                            }
                        }

                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['end']['target']['content']['text'] : $indicatorData['data'][$client['id']]['end']['target']['value'];
                        $indicatorLine[] = $indicatorData['type'] == Indicator::TYPE_FIXED ? $indicatorData['data'][$client['id']]['end']['done']['content']['text'] : $indicatorData['data'][$client['id']]['end']['done']['value'];
                        $indicatorLine[] = $indicatorData['data'][$client['id']]['end']['ratio'] !== null ? $indicatorData['data'][$client['id']]['end']['ratio'] . '%' : '';
                    }
                }
                $xlsLines[] = $indicatorLine;
            }

            ExcelExporter::download($xlsLines);
            return null;
        } else {
            return new JsonModel($data);
        }
    }
}
