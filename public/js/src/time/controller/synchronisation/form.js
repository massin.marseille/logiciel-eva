(function(app) {

    app.controller('synchronisationFormController', [
        'userService', 'synchronisationService', 'flashMessenger', 'translator', '$scope',
        function synchronisationFormController(userService, synchronisationService, flashMessenger, translator, $scope) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'link',
                    'type',
                    'timezone',
                    'descriptionMaxLength',
                    'parseTitle',
                    'config',
                    'cronUsers',
                    'cronSchedule',
                    'cronLastSync',
                    'user.id',
                    'user.name',
                    'createdAt',
                    'updatedAt',
                    'settingsTime.id',
                    'settingsTime.name',
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.synchronisation = {
                _rights: {
                    update: true
                }
            };

            self.init = function init(user, id) {
                if(user){
                    self.synchronisation.user = {
                        id: user
                    };
                }

                if (id) {
                    synchronisationService.find(id, apiParams).then(function (res) {

                        if (res.object?.cronUsers && res.object.cronUsers[0]) {
                            userService.findAll({
                                col: [
                                    'id',
                                    'name',
                                    'avatar',
                                ],
                                search: {
                                    type: 'list',
                                    data: {
                                        sort: 'name',
                                        order: 'asc',
                                        filters: {
                                            id: {
                                                op: 'eq',
                                                val: res.object.cronUsers.map(us => us.id),
                                            },
                                        }
                                    }
                                }
                            }).then(users => {
                                res.object.cronUsers = users.rows;
                                self.synchronisation = res.object;
                                self.synchronisation.type = '' + self.synchronisation.type;
                                self.panelTitle = self.synchronisation.name;
                            });
                        } else {
                            self.synchronisation = res.object;
                            self.synchronisation.type = '' + self.synchronisation.type;
                            self.panelTitle = self.synchronisation.name;
                        }

                        if (angular.isArray(self.synchronisation.config)) {
                            self.synchronisation.config = {};
                        }
                    });
                }
            };

            self.saveSynchronisation = function saveSynchronisation() {
                self.errors.fields = {};
                self.loading.save = true;

                if (self.synchronisation.cronUsers && self.synchronisation.cronUsers[0]) {
                    self.synchronisation.cronUsers = self.synchronisation.cronUsers.map(user => {return {id: user.id}});
                }

                var isCreation = !self.synchronisation.id;
                delete self.synchronisation.cronLastSync;
                synchronisationService.save(self.synchronisation, apiParams).then(function (res) {
                    self.synchronisation = res.object;
                    self.panelTitle = self.synchronisation.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.synchronisation.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('synchronisation_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteSynchronisation = function deleteSynchronisation() {
                if (confirm(translator('synchronisation_question_delete').replace('%1', self.synchronisation.name))) {
                    synchronisationService.remove(self.synchronisation).then(function (res) {
                        if (res.success) {
                            window.location = '/synchronisation';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ]);

})(window.EVA.app);
