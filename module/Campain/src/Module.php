<?php

namespace Campain;

use Core\Module\AbstractModule;
use Campain\View\Helper\CampainFilters;
use Campain\View\Helper\MeasureFilters;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    /**
     * @return array
     */
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'campainFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    return new CampainFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                },
                'measurecampainFilters' => function (ServiceManager $serviceManager) {
                    $helperPluginManager = $serviceManager->get('ViewHelperManager');
                    $allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
                    $translateViewHelper = $helperPluginManager->get('translate');
                    $moduleManager       = $serviceManager->get('MyModuleManager');
                    $entityManager       = $helperPluginManager->get('entityManager')->__invoke();
                    return new MeasureFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
                }
            ]
        ];
    }
}
