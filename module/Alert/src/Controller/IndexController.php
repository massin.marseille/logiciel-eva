<?php

namespace Alert\Controller;

use Alert\Command\SendAlerts;
use Alert\Entity\Alert;
use Core\Controller\AbstractActionSLController;
use Laminas\Http\Request;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
	    if (!$this->acl()->isAllowed('alert:e:alert:r')) {
            return $this->notFoundAction();
        }

        $periodicities = Alert::getPeriodicities();

        return new ViewModel([
            'periodicities' => $periodicities,
        ]);
    }

    public function formAction()
    {
        $periodicities = Alert::getPeriodicities();

        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('alert:e:alert', $id)) {
            return $this->notFoundAction();
        }

        $alert = $this->entityManager()->getRepository('Alert\Entity\Alert')->find($id);

        if ($alert && !$this->acl()->ownerControl('alert:e:alert:u', $alert) && !$this->acl()->ownerControl('alert:e:alert:r', $alert)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'periodicities' => $periodicities,
            'alert'         => $alert,
        ]);
    }

    public function formcampainAction()
    {
        $periodicities = Alert::getPeriodicities();

        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('alert:e:alert', $id)) {
            return $this->notFoundAction();
        }

        $alert = $this->entityManager()->getRepository('Alert\Entity\Alert')->find($id);

        if ($alert && !$this->acl()->ownerControl('alert:e:alert:u', $alert) && !$this->acl()->ownerControl('alert:e:alert:r', $alert)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'periodicities' => $periodicities,
            'alert'         => $alert,
        ]);
    }

    public function sendAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('alert:e:alert', $id)) {
            return $this->notFoundAction();
        }

        $alert = $this->entityManager()->getRepository('Alert\Entity\Alert')->find($id);

        if ($alert && !$this->acl()->ownerControl('alert:e:alert:u', $alert) && !$this->acl()->ownerControl('alert:e:alert:r', $alert)) {
            return $this->notFoundAction();
        }

        $success = false;
        $message = '';

        try {
            $client = $this->environment()->getClient();

            switch ($alert->getQuery()->getList()) {
                case "projects-list":
                    $api = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, []);
                    break;
                    case "campain-measures-list":
                        $api = $this->apiRequest('Campain\Controller\API\Measure', Request::METHOD_GET, []);
                    break;
            }
            
            $api->getController()->setEntityManager($this->entityManager());
            $api->getController()->setClient($client);
            
            $success = SendAlerts::sendAlert($api, $client, $alert, $this->entityManager());
            if (!$success) $message = 'Votre requête n\'a donné aucun résultat';
        } catch (\Exception $e) {
            $success = false;
            $message = 'Un problème a été rencontré lors de l\'envoi';
        } finally {
            return new JsonModel(['success' => $success, 'message' => $message]);
        }
    }
}
