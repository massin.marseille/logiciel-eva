<?php
namespace Home\Entity;

use Application\Entity\Query;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContainerGraphic
 *
 * @package Home\Entity
 *
 * @ORM\Entity
 */
class ContainerGraphic extends Container
{
    const GRAPHIC_TYPE_BY_MONTH = 'by_month';
    const GRAPHIC_TYPE_PIE = 'pie';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $entity;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $graphicType;

    /**
     * @var Query
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Query")
     */
    protected $query;

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getGraphicType()
    {
        return $this->graphicType;
    }

    /**
     * @param string $graphicType
     */
    public function setGraphicType($graphicType)
    {
        $this->graphicType = $graphicType;
    }

    /**
     * @return Query
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param Query $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this::TYPE_GRAPHIC;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name'     => 'entity',
            'required' => true,
        ]);

        $inputFilter->add([
            'name'      => 'graphicType',
            'required'  => true,
            'validator' => [
                [
                    'name'    => 'InArray',
                    'options' => [
                        'haystack' => self::getGraphicTypes(),
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'       => 'query',
            'required'   => false,
            'validators' => [
                [
                    'name'    => 'Callback',
                    'options' => [
                        'callback' => function ($value) use ($entityManager) {
                            if (is_array($value)) {
                                $value = $value['id'];
                            }

                            $query = $entityManager->getRepository('Application\Entity\Query')->find($value);

                            return $query !== null;
                        },
                        'message'  => 'container_field_query_error_non_exists',
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getGraphicTypes()
    {
        return [
            self::GRAPHIC_TYPE_BY_MONTH,
            self::GRAPHIC_TYPE_PIE,
        ];
    }
}
