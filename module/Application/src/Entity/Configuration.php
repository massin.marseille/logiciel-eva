<?php

namespace Application\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;
use Laminas\Mvc\Controller\Plugin\PluginInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="application_configuration")
 */
class Configuration extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $modules;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $translations;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $code;

    /**
     * @var array
     *
     * @ORM\Column(type="json", nullable=true)
     */
    protected $timeGraph;

    public function __construct()
    {
        $this->modules = [];
        $this->translations = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @param array $modules
     */
    public function setModules($modules)
    {
        $this->modules = $modules;
    }

    /**
     * @return array
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     */
    public function setTranslations($translations)
    {
        foreach ($translations as $key => $value) {
            if ($value == '') {
                unset($translations[$key]);
            }
        }
        $this->translations = $translations;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'modules',
                'required' => false
            ],
            [
                'name'     => 'translations',
                'required' => false
            ],
            [
                'name'     => 'code',
                'required' => false
            ],
            [
                'name'     => 'timeGraph',
                'required' => false
            ]
        ]);

        return $inputFilter;
    }

    /**
     * Get the value of code
     *
     * @return  int
     */ 
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @param  int  $code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of timeGraph
     *
     * @return  array
     */ 
    public function getTimeGraph()
    {
        return $this->timeGraph;
    }

    /**
     * Set the value of timeGraph
     *
     * @param  array  $timeGraph
     *
     * @return  self
     */ 
    public function setTimeGraph($timeGraph)
    {
        $this->timeGraph = $timeGraph;

        return $this;
    }
}
