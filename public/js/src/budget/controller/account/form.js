(function(app) {

    app.controller('accountFormController', [
        'accountService', 'flashMessenger', 'translator', '$scope',
        function accountFormController(accountService, flashMessenger, translator, $scope) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'code',
                    'name',
                    'type',
                    'default',
                    'description',
                    'parent.id',
                    'parent.name',
                    'parent.code',
                    'parent.type'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.account = {
                default: false,
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    accountService.find(id, apiParams).then(function (res) {
                        self.account = res.object;
                        self.panelTitle = self.account.code + ' - ' + self.account.name;
                    });
                }
            };

            self.saveaccount = function saveAccount() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.account.id;

                accountService.save(self.account, apiParams).then(function (res) {
                    self.account = res.object;
                    self.panelTitle = self.account.code + ' - ' + self.account.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.account.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('account_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteAccount = function deleteAccount() {
                if (confirm(translator('account_question_delete').replace('%1', self.account.code + ' - ' + self.account.name))) {
                    accountService.remove(self.account).then(function (res) {
                        if (res.success) {
                            window.location = '/budget/account';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            $scope.$watch(function () { return self.account.parent }, function (newVal, oldVal) {
                if (newVal) {
                    self.account.type = newVal.type;
                }
            });
        }
    ]);

})(window.EVA.app);
