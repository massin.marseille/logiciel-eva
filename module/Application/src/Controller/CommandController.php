<?php

namespace Application\Controller;

use Admin\Command\Runner;
use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\JsonModel;

class CommandController extends AbstractActionSLController
{
    public function runAction()
    {
        ini_set('max_execution_time', -1);
        $name = $this->params()->fromRoute('name', null);

        $runner = new Runner($this->entityManager(), $this->environment()->getClient());
        $success = $runner->run($name);

        return new JsonModel([
            'success' => $success
        ]);
    }
}
