<?php

namespace Admin\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class NetworkController extends AbstractActionSLController
{
    public function indexAction()
    {
        $themes       = scandir(getcwd() . '/public/theme');
        $modules      = $this->moduleManager()->getModules('client');
        $icons        = $this->serviceLocator->get('Config')['icons'];
        $translations = $this->serviceLocator->get('Translator')->getAllMessages()->getArrayCopy();

        ksort($icons);
        ksort($translations);

        return new ViewModel([
            'themes'       => $themes,
            'modules'      => $modules,
            'icons'        => $icons,
            'translations' => $translations
        ]);
    }
}
