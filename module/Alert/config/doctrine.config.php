<?php

namespace Alert;

return [
    'doctrine' => [
        'driver' => [
            'alert_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Alert\Entity' => 'alert_entities'
                ]
            ]
        ]
    ]
];
