<?php

namespace Budget\Controller\API;

use Budget\Entity\Expense;
use Budget\Entity\PosteExpense;
use Budget\Helper\PosteSorter;
use Core\Controller\BasicRestController;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Laminas\Http\Request;

class PosteExpenseController extends BasicRestController
{
	protected $entityClass = 'Budget\Entity\PosteExpense';
	protected $repository = 'Budget\Entity\PosteExpense';
	protected $entityChain = 'budget:e:posteExpense';

	protected function extract($object)
	{
		$array = parent::extract($object);
		$done = false;
		if (!$this->fromConsole) {
			if ($this->columns) {
				foreach ($this->columns as $column) {
					if (strpos($column, 'expensesArbo') !== false && $done === false) {
						foreach ($object->getExpensesArbo() as $i => $expense) {
							$array['expensesArbo'][$i]['_rights'] = [
								'read' => $this->acl()->ownerControl('budget:e:expense:r', $expense),
								'update' => $this->acl()->ownerControl('budget:e:expense:u', $expense),
								'delete' => $this->acl()->ownerControl('budget:e:expense:d', $expense),
							];
						}
						$done = true;
					}
				}
			}
		}

		return $array;
	}

	/**
	 * @param QueryBuilder $queryBuilder
	 * @param $data
	 */
	protected function searchList(QueryBuilder $queryBuilder, &$data)
	{
		$sort = $data['sort'] ? $data['sort'] : 'object.name';
		$order = $data['order'] ? $data['order'] : 'ASC';

		if (strpos($sort, '.') === false) {
			$sort = 'object.' . $sort;
		}

		$queryBuilder->join('object.project', 'project');
		$queryBuilder->leftJoin('object.account', 'account');
		$queryBuilder->leftJoin('object.expenses', 'expenses');
		$queryBuilder->groupBy('object.id');

		/*foreach (Expense::getTypes() as $type) {
            $queryBuilder->addSelect('SUM(CASE WHEN expenses.type = \'' . $type . '\' THEN expenses.amount ELSE 0 END) AS HIDDEN amount' . ucfirst($type));
        }*/

		$sort = str_replace('virtual.', '', $sort);

		//$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
		if (!preg_match('/^computed\./', $sort)) {	// do not try to sort when the property belongs to expenses sub object
			$queryBuilder->addOrderBy($sort, $order);
		}
		$sort = str_replace('computed.', '', $sort);	// mandatory here: computed amounts must not be sorted (error)

		if (isset($data['full']) && $data['full']) {
			if (isset($data['filters']['project'])) {
				$data['filters']['name'] = [
					'name' => [
						'op' => 'cnt',
						'val' => $data['full'],
					],
				];
			} else {
				$queryBuilder->andWhere(
					$queryBuilder->expr()->orX(...['object.name LIKE :f_full', 'account.name LIKE :f_full'])
				);

				$queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
			}
		}

		if (isset($data['filters'])) {
			foreach ($data['filters'] as $property => $filter) {
				// We send virtual.amount{ExpenseType}
				if (
					in_array(strtolower(str_replace('virtual.amount', '', $property)), Expense::getTypes())
				) {
					$data['filters'][$property]['having'] = true;
					continue;
				}

				if ($property === 'expenses.date') {
					$data['filterTransactionsDate'] = $data['filters'][$property];
					unset($data['filters'][$property]);
					continue;
				}

				// Get sub postes
				if ($property === 'project.id') {
					$project = $this->entityManager
						->getRepository('Project\Entity\Project')
						->find($data['filters'][$property]['val']);
					$hierarchy = $project->getHierarchySub();
					$data['filters'][$property]['val'] = [];
					foreach ($hierarchy as $child) {
						$data['filters'][$property]['val'][] = $child->getId();
					}
					$data['filters'][$property]['op'] = 'in';

					if ($hierarchy->count() > 0) {
						$queryBuilder->andWhere(
							$queryBuilder
								->expr()
								->orX(
									'project.id = ' . $project->getId(),
									'project.id IN (' .
										implode(',', $data['filters'][$property]['val']) .
										') AND object.parent IS NULL'
								)
						);
					} else {
						$queryBuilder->andWhere('project.id = ' . $project->getId());
					}

					unset($data['filters'][$property]);
				}
			}
		}
	}

	public function getList($callbacks = [], $master = false)
	{
		$callbacks[] = function (&$objects, $data) {
			if (isset($data['filterTransactionsDate'])) {
				if (
					isset($data['filterTransactionsDate']['op']) &&
					$data['filterTransactionsDate']['op'] == 'period'
				) {
					$start = isset($data['filterTransactionsDate']['val']['start'])
						? \DateTime::createFromFormat('d/m/Y', $data['filterTransactionsDate']['val']['start'])
						: false;
					$end = isset($data['filterTransactionsDate']['val']['end'])
						? \DateTime::createFromFormat('d/m/Y', $data['filterTransactionsDate']['val']['end'])
						: false;

					foreach ($objects as $object) {
						$transactions = $object->getExpenses();
						if ($transactions) {
							foreach ($transactions as $transaction) {
								$keep = true;
								if ($start && $transaction->getDate() < $start) {
									$keep = false;
								}

								if ($end && $transaction->getDate() > $end) {
									$keep = false;
								}

								if (!$keep) {
									$transactions->removeElement($transaction);
								}

								$object->setExpenses($transactions);
								$object->setFiltered(true);
							}
						}
					}
				}
			}
		};

		$col = $this->params()->fromQuery('col');
		$expenseFilters = $this->params()->fromQuery('expenseFilters', null);

		if ($expenseFilters) {
			$callbacks[] = function (&$objects) use ($col, $expenseFilters) {
				$expenseCol = [];
				foreach ($col as $column) {
					if (preg_match('/^expensesArbo./', $column)) {
						$expenseCol[] = str_replace('expensesArbo.', '', $column);
					}
				}

				foreach ($objects as $object) {
					$expenses = $object->getExpensesArbo();

					$expenseFilters['id'] = [
						'op' => 'eq',
						'val' => [],
					];
					foreach ($expenses as $expense) {
						$expenseFilters['id']['val'][] = $expense->getId();
					}

					$request = $this->apiRequest('Budget\Controller\API\Expense', Request::METHOD_GET, [
						'col' => $expenseCol,
						'search' => [
							'type' => 'list',
							'data' => [
								'sort' => 'id',
								'order' => 'asc',
								'filters' => $expenseFilters,
							],
						],
					]);
					$expenses = $request->dispatch()->getVariable('rows');

					$expensesObjects = new ArrayCollection();
					foreach ($expenses as $expense) {
						$expensesObjects->add(
							$this->entityManager()
								->getRepository('Budget\Entity\Expense')
								->find($expense['id'])
						);
					}

					$object->setExpenses($expensesObjects);
					$object->setFiltered(true);
				}
			};
		}

		$groupBy = $this->params()->fromQuery('groupBy', 'poste');

		if ($groupBy === 'tiers' || $groupBy === 'date' || $groupBy === 'anneeExercice' ||
		    $groupBy === 'mandat' || $groupBy === 'complement' || $groupBy === 'engagement' ||
			$groupBy === 'dateFacture' || $groupBy === 'evolution' || $groupBy === 'bordereau') {

			$callbacks[] = function (&$objects) use ($groupBy) {
				$grouped = [];

				foreach ($objects as $poste) {
					foreach ($poste->getExpensesArbo() as $expense) {
						$key = '';

						switch ($groupBy) {
							case 'tiers':
								$key = $expense->getTiers();
								break;
							case 'date':
								$key = $expense->getDate()->format('Y');
								break;
							case 'anneeExercice':
								$key = $expense->getAnneeExercice();
								break;
							case 'mandat':
								$key = $expense->getMandat();
								break;
							case 'complement':
								$key = $expense->getComplement();
								break;
							case 'engagement':
								$key = $expense->getEngagement();
								break;
							case 'dateFacture':
								$key = is_null($expense->getDateFacture()) ? '': $expense->getDateFacture()->format('Y');
								break;
							case 'evolution':
								$key = $expense->getEvolution() ? 'Oui' : 'Non';
								break;
							case 'bordereau':
								$key = $expense->getBordereau();
								break;
						}

						if (!isset($grouped[$key])) {
							$grouped[$key] = new PosteExpense();
							$grouped[$key]->setName($key);
							$grouped[$key]->setProject($poste->getProject());
							$grouped[$key]->setAmount(0);
							$grouped[$key]->setExpensesArbo(new ArrayCollection());
							$grouped[$key]->setFiltered(true);
						}

						$grouped[$key]->getExpensesArbo()->add($expense);
					}
				}

				$objects = $grouped;
			};
		}

		if ($groupBy === 'gbcp') {
			$callbacks[] = function (&$objects) {
				$grouped = [];

				foreach ($objects as $poste) {
					if (!$poste->getNature() || !$poste->getOrganization() || !$poste->getDestination()) {
						continue;
					}

					$key =
						$poste->getProject()->getId() .
						'-' .
						$poste->getNature()->getId() .
						'-' .
						$poste->getOrganization()->getId() .
						'-' .
						$poste->getDestination()->getId();

					if (!isset($grouped[$key])) {
						$grouped[$key] = new PosteExpense();
						$grouped[$key]->setNature($poste->getNature());
						$grouped[$key]->setOrganization($poste->getOrganization());
						$grouped[$key]->setDestination($poste->getDestination());
						$grouped[$key]->setAmount(0);
						$grouped[$key]->setExpensesArbo(new ArrayCollection());
						$grouped[$key]->setFiltered(true);
					}

					foreach ($poste->getExpensesArbo() as $expense) {
						$expenseType = $expense->getType();

						if (
							$expenseType === Expense::TYPE_AE ||
							$expenseType === Expense::TYPE_AE_COMMITTED ||
							$expenseType === Expense::TYPE_AE_ESTIMATED ||
							$expenseType === Expense::TYPE_CP ||
							$expenseType === Expense::TYPE_CP_COMMITTED ||
							$expenseType === Expense::TYPE_CP_ESTIMATED
						) {
							$grouped[$key]->getExpensesArbo()->add($expense);
						}
					}
				}

				$objects = $grouped;
			};
		}

		$res = parent::getList($callbacks, $master);

		// expenses sorting
		$res->setVariable(
			'rows',
			PosteSorter::sortPostes(
				$res->getVariable('rows'),
				$this->params()->fromQuery('search'),
				'expenses'
			)
		);

		if ($groupBy === 'tiers' || $groupBy === 'gbcp') {
			$rows = $res->getVariable('rows');
			foreach ($rows as $i => $row) {
				$rows[$i]['project']['name'] = '';
				$rows[$i]['_rights']['update'] = false;
				$rows[$i]['_rights']['delete'] = false;
				$rows[$i]['_rights']['read'] = false;
			}

			$res->setVariable('rows', $rows);
		}

		return $res;
	}
}
