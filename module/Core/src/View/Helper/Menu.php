<?php

/**
 * File Menu.php
 *
 * PHP version 5
 *
 * @category Core
 * @package EVA
 * @license http://opensource.org/licenses/MIT
 * @link /
 * @since Version 0.1
 * @version 0.1
 */
namespace Core\View\Helper;

use Bootstrap\Entity\Client;
use Bootstrap\Environment\Environment;
use Core\Module\MyModuleManager;
use Core\Utils\ArrayUtils;
use User\Auth\Helper\AclHelper;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Url;

/**
 * Class Menu, draw the menu navbar.
 *
 * @category Core
 * @package EVA
 * @since Version 0.1
 * @version 0.1
 * @author Curtis Pelissier <curtis.pelissier@lignusdev.com>
 */
class Menu extends AbstractHelper
{
    /**
     * ServiceLocator
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * MyModuleManager
     * @var MyModuleManager
     */
    protected $moduleManager;

    /**
     * ACL
     * @var AclHelper
     */
    protected $acl;

    /**
     * Icon
     * @var Icon
     */
    protected $icon;

    /**
     * Icon
     * @var Icon
     */
    protected $translate;

    /**
     * URL
     * @var Url
     */
    protected $url;

    /**
     * @var Environment
     */
    protected $env;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Constructor of the class
     *
     * @param mixed  the param
     * @since Version 0.1
     * @version 0.1
     */
    public function __construct(ServiceLocatorInterface $serviceLocator, $translateViewHelper, $iconViewHelper, $urlViewHelper)
    {
        $this->serviceLocator = $serviceLocator;
        $this->moduleManager = $this->serviceLocator->get('MyModuleManager');
        $this->acl = $serviceLocator->get('Acl');
        $this->icon = $iconViewHelper;
        $this->translate = $translateViewHelper;
        $this->url = $urlViewHelper;
        $this->env = $this->serviceLocator->get('Environment')->getName();
        $this->client = $this->serviceLocator->get('Environment')->getClient();
    }

    /**
     * ToString method, return the menu navbar draw.
     *
     * @return string
     * @since Version 0.1
     * @version 0.1
     */
    public function __toString()
    {
        try {
            $menu = '';
            $confMenu = [];

            $theme = $this->serviceLocator->get('Environment')->getClient()->getTheme();
            $default_menu_name = $this->env . '-menu';
            $theme_menu_name = $default_menu_name.'-'.$theme;

            foreach ($this->moduleManager->getModules() as $module) {
                if ($module->isActive()) {
                    $config = $module->getConfig();
                    if (isset($config['menu'])) {
                        if(isset($config['menu'][$theme_menu_name])) {
                            $confMenu = ArrayUtils::arrayMergeRecursiveDistinct($confMenu, $config['menu'][$theme_menu_name]);
                        } elseif(isset($config['menu'][$default_menu_name])) {
                            $confMenu = ArrayUtils::arrayMergeRecursiveDistinct($confMenu, $config['menu'][$default_menu_name]);
                        }
                    }
                }
            }

            $this->orderBy($confMenu, 'priority');

            foreach ($confMenu as $menuLink) {
                $menu .= $this->getLink($menuLink);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $menu;
    }

    /**
     * Order the array by a specific variable
     *
     * @param array $menu The menu array
     * @param string $param The param to sort with
     * @since Version 0.1
     * @version 0.1
     */
    protected function orderBy(&$menu, $param = 'priority')
    {
        usort($menu, function ($a, $b) use ($param) {
            if (!isset($a[$param])) {
                return 1;
            }

            if (!isset($b[$param])) {
                return -1;
            }

            $priorityA = intval($a[$param]);
            $priorityB = intval($b[$param]);
            if ($priorityA === $priorityB) {
                return 0;
            }

            return ($priorityA > $priorityB) ? -1 : 1;
        });
    }

    /**
     * Get link element
     * If it has children, check their right.
     *
     * @param array $menuLink The menu link
     * @return string
     * @since Version 0.1
     * @version 0.1
     */
    protected function getLink($menuLink)
    {
        $forceMaster = isset($menuLink['force-master']) && $menuLink['force-master'] === true && $this->client->isMaster();
        $onlyMaster  = isset($menuLink['only-master'])  && $menuLink['only-master']  === true;

        $link = null;
        if ($onlyMaster && $this->client->isMaster() || !$onlyMaster) {
            if (!isset($menuLink['module']) || $this->moduleManager->isActive($menuLink['module']) || $forceMaster) {
                if (!isset($menuLink['right']) || $this->acl->isAllowed($menuLink['right']) || $forceMaster) {
                    $subLinks = $this->getSubLinks($menuLink);
                    if (!empty($subLinks) && $subLinks != null || !$this->hasChildren($menuLink)) {
                        $link = '<li>
                            <a href="' . $this->buildUrl($menuLink) . '" data-toggle="tooltip" data-placement="right" data-template="<div class=\'tooltip tooltip-nav\'><div class=\'tooltip-arrow\'></div><div class=\'tooltip-inner\'></div></div>" data-delay="1000" title="' . $this->translate->__invoke($menuLink['title']) . '" data-container="body">
                                <span class="nav-link-icon">' . $this->icon->__invoke($menuLink['icon'], 'fa-fw') . '</span>
                                <span class="nav-link-text">' . $this->translate->__invoke($menuLink['title']) . '</span>
                                ' . $this->getArrowNav($menuLink) . '
                            </a>
                            ' . $subLinks . '
                        </li>';
                    }
                }
            }
        }

        return $link;
    }

    /**
     * Get sub link elements
     *
     * @param array $menuLink The menu link
     * @return string
     * @since Version 0.1
     * @version 0.1
     */
    protected function getSubLinks($menuLink)
    {
        if ($this->hasChildren($menuLink)) {
            $links = null;
            $this->orderBy($menuLink['children'], 'priority');
            foreach ($menuLink['children'] as $child) {
                $links .= $this->getLink($child);
            }

            if ($links) {
                return '<ul class="nav">' . $links . '</ul>';
            }
        }

        return null;
    }

    /**
     * Check if the menu link has children
     *
     * @param array $menuLink The menu link
     * @return bool
     * @since Version 0.1
     * @version 0.1
     */
    protected function hasChildren($menuLink)
    {
        return isset($menuLink['children']) && is_array($menuLink['children']) && count($menuLink['children']) > 0;
    }

    /**
     * Build the url of the menuLink
     *
     * @param array $menuLink The menu link
     * @return string
     * @since Version 0.1
     * @throws \Exception
     * @version 0.1
     */
    protected function buildUrl($menuLink)
    {
        if (isset($menuLink['href'])) {
            try {
                return (strpos($menuLink['href'], 'https') === 0 ) ?
                    $menuLink['href'] : $this->url->__invoke($menuLink['href']);
            } catch (\Exception $e) {
                $error = "Le lien ' " . $menuLink['href'] . " ' ne fonctionne pas ";
                $error .= 'pour le menu : ' . $menuLink['title'];
                throw new \Exception($error, 45000);
            }
        }
        return '#';
    }

    /**
     * Gets the Arrow Nav
     *
     * @param array $menuLink The menu link
     * @return string
     * @since Version 0.0.1
     * @version 0.0.1
     */
    protected function getArrowNav($menuLink)
    {
        if ($this->hasChildren($menuLink)) {
            return '<i class="fa fa-arrow-circle-right nav-arrow"></i>';
        }
        return null;
    }
}
