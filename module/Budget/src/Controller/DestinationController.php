<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class DestinationController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:destination:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('budget:e:destination', $id)) {
            return $this->notFoundAction();
        }

        $destination = $this->entityManager()
            ->getRepository('Budget\Entity\Destination')
            ->find($id);

        if (
            $destination &&
            !$this->acl()->ownerControl('budget:e:destination:u', $destination) &&
            !$this->acl()->ownerControl('budget:e:destination:r', $destination)
        ) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'destination' => $destination,
        ]);
    }
}
