<?php

namespace Advancement;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'api'       => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'advancement' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/advancement[/:id]',
                            'defaults' => [
                                'controller' => 'Advancement\Controller\API\Advancement',
                            ],
                        ],
                    ],
                ],
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\API\AdvancementController::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Advancement\Controller\API\Advancement' => Controller\API\AdvancementController::class
        ]
    ]
];
