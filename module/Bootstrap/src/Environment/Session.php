<?php

namespace Bootstrap\Environment;

use Laminas\Session\Container;

/**
 * Class Session
 *
 * This class is a wrapper responsible to manage the current PHP session.
 *
 * @package Bootstrap\Environment
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class Session
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param string $name
     */
    public function start($name)
    {
        $this->container = new Container($name);
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function store($key, $value)
    {
        $this->container->offsetSet($key, $value);
    }

    /**
     * @param string $key
     */
    public function clear($key)
    {
        $this->container->offsetUnset($key);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->container->offsetGet($key);
    }
}
