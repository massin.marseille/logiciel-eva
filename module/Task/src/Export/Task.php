<?php

namespace Task\Export;

use Core\Export\IExporter;

abstract class Task implements IExporter
{
    public static function getConfig()
    {
        return [
            'progress'     => [
                'format' => function ($value) {
                    return $value . '%';
                },
            ],
            'project.name' => [
                'name' => 'project',
            ],
            'users.name' => [
                'name' => 'users'
            ]
        ];
    }

    public static function getAliases()
    {
        return [

        ];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
