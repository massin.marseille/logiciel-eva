(function (app) {

    app.controller('unsubscribeController', [
        '$location', '$scope', 'controlService',
        function ($location, $scope, controlService) {
            controlService.desactivate();
        }
    ]);

})(window.EVA.app);
