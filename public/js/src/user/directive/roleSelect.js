(function(app) {

    app.directive('roleSelect', [
        'roleService', 'selectFactoryService',
        function roleSelect(roleService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: roleService
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);
