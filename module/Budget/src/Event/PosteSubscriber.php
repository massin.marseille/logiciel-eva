<?php

namespace Budget\Event;

use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Project\Entity\Project;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Class PosteSubscriber
 *
 * -- Not used anymore --
 *
 * @package Budget\Event
 */
class PosteSubscriber implements EventSubscriber
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof PosteExpense || $entity instanceof PosteIncome) {
                $this->synchronizePostes($entity, $uow, $em);
            } else if ($entity instanceof Project) {
                if ($entity->getParent()) {
                    foreach ($entity->getParent()->getPosteIncomes() as $poste) {
                        $this->synchronizePostes($poste, $uow, $em, [$entity]);
                    }

                    foreach ($entity->getParent()->getPosteExpenses() as $poste) {
                        $this->synchronizePostes($poste, $uow, $em, [$entity]);
                    }
                }
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof PosteExpense || $entity instanceof PosteIncome) {
                $this->synchronizePostes($entity, $uow, $em);
            } else if ($entity instanceof Project) {
                if ($entity->getParent()) {
                    foreach ($entity->getParent()->getPosteIncomes() as $poste) {
                        $this->synchronizePostes($poste, $uow, $em, [$entity]);
                    }

                    foreach ($entity->getParent()->getPosteExpenses() as $poste) {
                        $this->synchronizePostes($poste, $uow, $em, [$entity]);
                    }
                }
            }
        }
    }

    public function getSubscribedEvents()
    {
        return [
            Events::onFlush
        ];
    }

    protected function synchronizePostes($entity, $uow, $em, $projects = null)
    {
        if ($entity->getAccount()) {
            $getPostesFn = 'getPoste' . ($entity instanceof PosteExpense ? 'Expenses' : 'Incomes');
            if ($projects === null) {

                $projects = array_merge($entity->getProject()->getHierarchySup()->toArray(), $entity->getProject()->getHierarchySub()->toArray());
                foreach ($entity->getProject()->getHierarchySup()->toArray() as $parent) {
                    $projects[] = $parent;
                    foreach ($parent->getHierarchySub()->toArray() as $child) {
                        $projects[] = $child;
                    }
                    //$projects = array_merge($projects, $parent->getHierarchySub()->toArray());
                }

                foreach ($entity->getProject()->getHierarchySub()->toArray() as $child) {
                    $projects[] = $child;
                }
            }
            foreach ($projects as $project) {
                if ($project !== $entity->getProject()) {
                    $projectPoste = null;
                    $postes = $project->$getPostesFn();
                    if ($postes) {
                        foreach ($postes as $poste) {
                            if ($poste->getAccount() && $entity->getAccount()) {
                                if ($poste->getAccount()->getId() == $entity->getAccount()->getId()) {
                                    $projectPoste = $poste;
                                    break;
                                }
                            }
                        }
                    }

                    if ($projectPoste) {
                        $projectPoste->setName($entity->getName());
                        //$projectPoste->setAmount($entity->getAmount());
                        if ($entity instanceof PosteIncome) {
                            $projectPoste->setArrDate($entity->getArrDate());
                            $projectPoste->setArrNumber($entity->getArrNumber());
                            $projectPoste->setAutofinancement($entity->getAutofinancement());
                            $projectPoste->setCaduciteEnd($entity->getCaduciteEnd());
                            $projectPoste->setCaduciteSendProof($entity->getCaduciteSendProof());
                            $projectPoste->setCaduciteStart($entity->getCaduciteStart());
                            $projectPoste->setDeliberationDate($entity->getDeliberationDate());
                            $projectPoste->setEnvelope($entity->getEnvelope());
                            $projectPoste->setFolderReceiptDate($entity->getFolderReceiptDate());
                            $projectPoste->setFolderSendingDate($entity->getFolderSendingDate());
                        }
                        $uow->computeChangeSet($em->getClassMetadata(get_class($projectPoste)), $projectPoste);
                    } else {
                        $poste = clone $entity;
                        $poste->setId(null);
                        $poste->setProject($project);
                        $poste->setAmount(0);
                        $poste->setCreatedAt(null);
                        $poste->setUpdatedAt(null);
                        $project->$getPostesFn()->add($poste);
                        $em->persist($poste);
                        $uow->computeChangeSet($em->getClassMetadata(get_class($poste)), $poste);
                    }
                }
            }
        }
    }
}
