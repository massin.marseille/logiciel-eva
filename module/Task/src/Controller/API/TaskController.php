<?php

namespace Task\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class TaskController extends BasicRestController
{
    protected $entityClass  = 'Task\Entity\Task';
    protected $repository   = 'Task\Entity\Task';
    protected $entityChain  = 'task:e:task';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.start';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if ($this->serviceLocator->get('MyModuleManager')->isActive('project')) {
            $queryBuilder->leftJoin('object.project', 'project');
        }

        $queryBuilder->leftJoin('object.users', 'users');

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
