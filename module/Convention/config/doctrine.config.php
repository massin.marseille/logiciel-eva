<?php

namespace Convention;

return [
    'doctrine' => [
        'driver' => [
            'convention_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Convention\Entity' => 'convention_entities'
                ]
            ]
        ]
    ]
];
