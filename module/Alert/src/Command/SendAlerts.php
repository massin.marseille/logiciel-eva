<?php

namespace Alert\Command;

use Alert\Entity\Alert;
use Core\Command\AbstractCommand;
use Laminas\Http\Request;
use Laminas\Stdlib\Parameters;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendAlerts extends AbstractCommand
{

    /**
     * Call : php vendor/bin/laminas alert:send-alerts
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[START] Sending alerts...');

        $em                   = $this->serviceLocator->get('DefaultEntityManager');
        $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
        $apiRequest           = $this->serviceLocator->get('ControllerPluginManager')->get('apiRequest');
        $exitCode = Command::SUCCESS;

        $clients = $em->getRepository('Bootstrap\Entity\Client')->findAll();
        foreach ($clients as $client) {
            $output->writeln('[ ' . $client->getName() . ' ] - START');

            try {
                $clientEm = $entityManagerFactory->getEntityManager($client);

                /** @var Alert[] $alerts */
                $alerts = $clientEm->getRepository('Alert\Entity\Alert')->findAll();
                foreach ($alerts as $alert) {
                    try {
                        if ($alert->isSendable()) {
                            $output->writeln('[ ' . $client->getName() . ' ] ALERTE : ' . $alert->getName());
                            $api = $apiRequest('Project\Controller\API\Project', Request::METHOD_GET, []);
                            $api->getController()->setFromConsole(true);
                            $api->getController()->setEntityManager($clientEm);
                            $api->getController()->setClient($client);

                            self::sendAlert($api, $client, $alert, $em, true);
                        }
                    } catch (\Throwable|\Exception $ee) {
                        $exitCode = Command::FAILURE;
                        $output->writeln('[ERROR] - [ ' . $client->getName() . ' ] FAIL TO SEND ALERTE - ' . $alert->getName() . ' : ' . $ee->getMessage());
                        if ($ee->getPrevious() && $ee->getPrevious()->getMessage()) {
                            $output->writeln('[ERROR] - [ ' . $client->getName() . ' ] FAIL TO SEND ALERTE - ' . $alert->getName() . ' - Previous exception : ' . $ee->getPrevious()->getMessage());
                        }
                    }
                }

            } catch (\Throwable|\Exception $e) {
                $exitCode = Command::FAILURE;
                $output->writeln('[ERROR] - ' . '[ ' . $client->getName() . ' ] FAIL : ' . $e->getMessage());
                if ($e->getPrevious() && $e->getPrevious()->getMessage()) {
                    $output->writeln('[ERROR] - ' . '[ ' . $client->getName() . ' ] FAIL - Previous exception : ' . $e->getPrevious()->getMessage());
                }
            }

            $output->writeln('[ ' . $client->getName() . ' ] - END');
        }

        $output->writeln('[END] Sending alerts...');
        return $exitCode;
    }

    public static function sendAlert($api, $client, $alert, $em, $console = false)
    {   
        switch ($alert->getQuery()->getList()) {
            case "projects-list":
                $api->getRequest()->setQuery(new Parameters([
                    'col'    => [
                        'id',
                        'name',
                        'code',
                        'members.user.id',
                        'members.user.name',
                        'members.user.email',
                        'members.role',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'name',
                            'order'   => 'asc',
                            'filters' => $alert->getQuery()->getFilters(),
                        ],
                    ],
                ]));
                break;
            case "campain-measures-list":
                
                $filters = $alert->getQuery()->getFilters();
                unset($filters['campain.id']);
                $indicators = $alert->getIndicators();
                $groupIndicators = $alert->getGroupIndicators();
                if($indicators->count()>0 || $groupIndicators->count()>0){
                    $filters['realiseMeasure.indicator'] = [];
                    $filters['realiseMeasure.indicator']['op'] = 'eq';
                    $filters['realiseMeasure.indicator']['val'] = [];
                    if($indicators->count()>0){
                        foreach($indicators as $indicator){
                            $filters['realiseMeasure.indicator']['val'][] = $indicator->getId();
                        }
                    }
                    if($groupIndicators->count()>0){
                        foreach($groupIndicators as $groupIndicator){
                            foreach($groupIndicator->getIndicators() as $indicator){
                                $filters['realiseMeasure.indicator']['val'][] = $indicator->getId();
                            }
                        }
                    }
                }
                $api->getRequest()->setQuery(new Parameters([
                    'col' => [
                        'campain.id',
                        'campain.name',
                        'campain.evaluationManagers.id',
                        'indicator.users.id',
                        'realiseMeasure.indicator.id',
                        'realiseMeasure.indicator.name',
                    ],
                    'search' => [
                        'type' => 'list',
                        'data' => [
                            'sort'    => 'realiseMeasure.date',
                            'order'   => 'asc',
                            'filters' => $filters,
                        ],
                    ],
                ]));
                break;
        }
        
        $objects = $api->dispatch()->getVariable('rows');
        
        if ($objects) {
            return $alert->send($objects, $client, $em, $console);
        }

        return false;
    }
}
