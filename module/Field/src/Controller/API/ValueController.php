<?php

namespace Field\Controller\API;

use Core\Controller\BasicRestController;
use Field\Entity\Value;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Model\JsonModel;

class ValueController extends BasicRestController
{
    protected $entityClass = 'Field\Entity\Value';
    protected $repository  = 'Field\Entity\Value';
    protected $entityChain = '';
    protected $isGeneric = true;

    public function onDispatch(MvcEvent $event)
    {
        $this->entityChain = $this->params()->fromQuery('right', null);
        if ($this->entityChain === null) {
            $request = $event->getRequest();
            $data = $this->processBodyContent($request);
            if (!isset($data['right'])) {
                return $this->notFoundAction();
            }
            $this->entityChain = $data['right'];
        }
        return parent::onDispatch($event);
    }

    protected function searchList($queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->orderBy($sort, $order);
    }

    public function create($data)
    {
        // Doctrine check the type when we hydrate the object
        if (isset($data['value']) && is_array($data['value'])) {
            $data['value'] = json_encode($data['value']);
        }

        if ($this->acl()->getUser()) {
            if (isset($data['field'])) {
                if (is_array($data['field']) && isset($data['field']['id'])) {
                    $data['field'] = $data['field']['id'];
                }

                if (!is_numeric($data['field'])) {
                    $field = $this
                        ->getEntityManager()
                        ->getRepository('Field\Entity\Field')
                        ->findOneBy(['uuid' => $data['field']]);

                    if ($field) {
                        $data['field'] = $field->getId();
                    }
                }
            }

            if (isset($data['primary']) && !is_numeric($data['primary']) && $this->entityChain === 'directory:e:contact') {
                $contact = $this
                    ->getEntityManager()
                    ->getRepository('Directory\Entity\Contact')
                    ->findOneBy(['uuid' => $data['primary']]);

                if ($contact) {
                    $data['primary'] = $contact->getId();
                }
            }

            return parent::create($data);
        }

        if (isset($data['field'])) {
            $field = $this
                ->getEntityManager()
                ->getRepository('Field\Entity\Field')
                ->findOneBy(['uuid' => $data['field']]);

            if ($field) {
                $data['field'] = $field->getId();
            }
        }

        if (isset($data['primary']) && $this->entityChain === 'directory:e:contact') {
            $contact = $this
                ->getEntityManager()
                ->getRepository('Directory\Entity\Contact')
                ->findOneBy(['uuid' => $data['primary']]);

            if ($contact) {
                $data['primary'] = $contact->getId();
            }
        }

        $object        = new $this->entityClass();
        $objectAsArray = [];
        $success       = false;
        $errors = [
            'fields'     => [],
            'exceptions' => [],
        ];

        try {
            $inputFilter = $object->getInputFilter($this->getEntityManager());
            $inputFilter->setData($data);

            if ($inputFilter->isValid()) {
                $this->hydrate($inputFilter->getValues(), $object);

                $this->getEntityManager()->persist($object);
                $this->getEntityManager()->flush();

                if ($this->columns) {
                    foreach ($this->columns as $column) {
                        $this->extractInternal($object, $column, $objectAsArray);
                    }
                }

                $success = true;
            }

            $errors['fields'] = $inputFilter->getMessages();
        } catch (\Exception $e) {
            $errors['exceptions'][] = $e->getMessage();
        } finally {
            return new JsonModel([
                'errors'  => $errors,
                'success' => $success,
                'object'  => $objectAsArray,
            ]);
        }
    }

    public function update($id, $data)
    {
        // Doctrine check the type when we hydrate the object
        if (isset($data['value']) && is_array($data['value'])) {
            $data['value'] = json_encode($data['value']);
        }
        
        if ($data['entity'] == 'Project\Entity\Project'){
            // add to the history of the project
            $project = $this->getEntityManager()
                ->getRepository('Project\Entity\Project')
                ->find($data['primary']);
            $this->serviceLocator
            ->get('ProjectUpdateService')
            ->logProjectUpdate($project);            
        }

        if ($this->acl()->getUser()) {
            /** @var Value $value */
            $value = $this
                ->getEntityManager()
                ->getRepository('Field\Entity\Value')
                ->findOneBy(['uuid' => $id]);

            if (!$value) {
                $value = $this
                    ->getEntityManager()
                    ->getRepository('Field\Entity\Value')
                    ->find($id);
            }

            if (!$value) {
                return parent::update($id, $data);
            }

            if (isset($data['field'])) {
                if (is_array($data['field']) && isset($data['field']['id'])) {
                    $data['field'] = $data['field']['id'];
                }

                if (!is_numeric($data['field'])) {
                    $field = $this
                        ->getEntityManager()
                        ->getRepository('Field\Entity\Field')
                        ->findOneBy(['uuid' => $data['field']]);

                    if ($field) {
                        $data['field'] = $field->getId();
                    }
                }
            }

            if (isset($data['primary'])) {
                if (!is_numeric($data['primary'])) {
                    $contact = $this
                        ->getEntityManager()
                        ->getRepository('Directory\Entity\Contact')
                        ->findOneBy(['uuid' => $data['primary']]);

                    if ($contact) {
                        $data['primary'] = $contact->getId();
                    }
                }
            }

            return parent::update($value->getId(), $data);
        }

        if (isset($data['field'])) {
            $field = $this
                ->getEntityManager()
                ->getRepository('Field\Entity\Field')
                ->findOneBy(['uuid' => $data['field']]);

            if ($field) {
                $data['field'] = $field->getId();
            }
        }

        if (isset($data['primary'])) {
            $contact = $this
                ->getEntityManager()
                ->getRepository('Directory\Entity\Contact')
                ->findOneBy(['uuid' => $data['primary']]);

            if ($contact) {
                $data['primary'] = $contact->getId();
            }
        }

        $objectAsArray = [];
        $success       = false;
        $errors        = [
            'fields'     => [],
            'exceptions' => [],
        ];

        try {
            $object = $this
                ->getEntityManager()
                ->getRepository($this->repository)
                ->findOneBy(['uuid' => $id]);

            if ($object) {
                $inputFilter = $object->getInputFilter($this->getEntityManager());
                $data        = array_merge($this->hydratorExtract($object), $data);
                $inputFilter->setData($data);

                if ($inputFilter->isValid()) {
                    $this->hydrate($inputFilter->getValues(), $object);
                    $this->getEntityManager()->flush();

                    if ($this->columns) {
                        foreach ($this->columns as $column) {
                            $this->extractInternal($object, $column, $objectAsArray);
                        }
                    }

                    $success = true;
                }

                $errors['fields'] = $inputFilter->getMessages();
            }
        } catch (\Exception $e) {
            $errors['exceptions'][] = $e->getMessage();
        } finally {
            return new JsonModel([
                'errors'  => $errors,
                'success' => $success,
                'object'  => $objectAsArray,
            ]);
        }
    }
}
