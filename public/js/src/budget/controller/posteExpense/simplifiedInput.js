(function (app) {

    app.controller('posteExpenseSimplifiedInputController', [
        'posteExpenseSimplifiedInputService', 'flashMessenger', 'translator', '$scope',
        function posteExpenseSimplifiedInputController(posteExpenseSimplifiedInputService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal = angular.element('#posteExpenseSimplifiedInputModal');

            self.readOnly = true;
            self.projectId;

            // GENERATION FORM
            self.referential = [];
            self.currentYear = null;    // to avoid form start date to start before the current year
            self.form = {
                type: null,
                start: null,
                end: null
            };
            // TABLE
            self.years = [];
            self.table = [];

            // SAVE
            self.loading = {
                save: false
            };


            self.reset = function () {
                self.referential = [];
                self.form = {
                    type: null,
                    duration: 1
                };
                self.years = [];
                self.table = [];
            };

            self.init = function (readOnly) {
                self.projectId = $scope.$parent.$parent.$parent.self['project'].id;
                self.reset();

                self.readOnly = !!readOnly;
            };

            self.load = function () {
                self.reset();

                posteExpenseSimplifiedInputService.getReferential(self.projectId)
                    .then(function (res) {
                        self.referential = res.referential;
                        self.currentYear = res.currentYear;
                        self.form.start = res.startYear;
                        self.form.end = res.endYear;
                    });

                posteExpenseSimplifiedInputService.getTable(self.projectId)
                    .then(function (res) {
                        self.years = res.years;
                        self.table = res.table;
                        self.updateTable();
                    });
            };

            self.generateTable = function () {
                posteExpenseSimplifiedInputService.generateTable(self.form, self.projectId)
                    .then(function (res) {
                        self.years = res.years;
                        self.table = res.table;
                        self.updateTable();
                    });
            };

            self.updateTable = function () {
                var subTotal = initTotalObject();
                var total = initTotalObject();

                // all lines are recomputed
                for (var i = 0; i < self.table.length; i++) {
                    var line = self.table[i];
                    // compute the total of the years for all lines (column)
                    var lineTotalExpenses = 0.0;
                    for (var y = 0; y < self.years.length; y++) {
                        var year = self.years[y];
                        var yearAmount = parseFloat(line[year].amount);
                        // on a poste expense line, increment the total year amounts
                        if (isPosteLine(line)) {
                            incrementTotalYearAmount(subTotal, year, yearAmount);  // sub total
                            incrementTotalYearAmount(total, year, yearAmount);     // total                            
                        }
                        // on a total line, set the year amount from the incremented objects     
                        else if (isTotalLine(line)) {
                            line[year] = isSubTotalLine(line) ? subTotal[year] : total[year];
                        }

                        // for each line (poste or total, increment the total expenses amount)
                        lineTotalExpenses += yearAmount;
                    }
                    // set the total expenses amount for the line (poste or total)
                    line.total = lineTotalExpenses;

                    // compute the financing portions
                    if (isPosteLine(line)) {
                        // poste expense -> compute with financing percent
                        line.client = line.total * ((100.0 - line.financing) / 100.0);
                        line.sponsor = line.total * (line.financing / 100.0);

                        // for total lines, increment amounts (client and sponsor)
                        incrementTotalFinancing(subTotal, line);
                        incrementTotalFinancing(total, line);
                    } else if (isTotalLine(line)) {
                        if (isSubTotalLine(line)) {
                            setTotalFinancing(line, subTotal);
                            subTotal = initTotalObject();
                        } else {
                            setTotalFinancing(line, total);
                        }
                    }
                }
            };

            function incrementTotalYearAmount(total, year, amount) {
                if (undefined === total[year]) {
                    total[year] = { amount: 0.0, readOnly: true };
                }
                total[year].amount += amount;
            }

            function incrementTotalFinancing(total, line) {
                total.client += line.client;
                total.sponsor += line.sponsor;
            }

            function setTotalFinancing(line, total) {
                line.client = total.client;
                line.sponsor = total.sponsor;
                if (0.0 !== (line.client + line.sponsor)) {
                    // compute financing percent from total pieces with a precision of 2 decimals
                    line.financing = (100 * line.client / (line.client + line.sponsor)).toFixed(2);
                }
            }

            function isPosteLine(line) {
                return 'poste' === line.type;
            }

            function isTotalLine(line) {
                return 'total' === line.type;
            }

            function isSubTotalLine(line) {
                return 'total' === line.type && null !== line.category;
            }

            function initTotalObject() {
                return { client: 0.0, sponsor: 0.0 };
            }


            self.saveTable = function () {
                posteExpenseSimplifiedInputService.importTable({ table: self.table, years: self.years }, self.projectId)
                    .then(function (res) {
                        $modal.modal('hide');
                        flashMessenger.success(translator('poste_expense_simplified_input_save_success'));
                        $scope.$parent.__tb.loadData();
                    }, function(err) {
                        console.error(err);
                        flashMessenger.error(translator('error_occured'));
                    });
            };


            $scope.$on('open-poste-expense-simplified-input', function () {
                self.load();
                $modal.modal('show');
            });

        }
    ]);

})(window.EVA.app);