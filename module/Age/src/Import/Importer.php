<?php

namespace Age\Import;

use Age\Import\Connection\AGEServerConnection;
use Age\Import\Connection\MysqlConnection;
use Age\Import\Connection\SQLServerConnection;
use Budget\Entity\Account;
use Budget\Entity\BudgetCode;
use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Budget\Helper\PosteFinder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Driver\DrizzlePDOMySql\Connection;
use Doctrine\ORM\EntityManager;
use Project\Entity\Project;

/**
 * Age importer service.
 *
 * @package Age\Importer
 */
class Importer
{
	/**
	 * @var AbstractConnection
	 */
	protected $connection;
	/**
	 * @var AGEServerConnection
	 */
	private $masterConnection = null;

	/**
	 * @var EntityManager
	 */
	protected $entityManager;

	/**
	 * @var string
	 */
	protected $identifier;


	private $allowedExpenses;
	private $allowedIncomes;

	public function __construct($configuration, $entityManager)
	{
		if (!isset($configuration['dbType'])) {
			$configuration['dbType'] = 'mysql';
		}

		switch ($configuration['dbType']) {
			case 'mysql':
				$this->connection = new MysqlConnection($configuration);
				break;
			case 'sql-server':
				$this->connection = new SQLServerConnection($configuration);
				if (isset($configuration['portfolioActive']) && $configuration['portfolioActive']) {
					$this->masterConnection = new AGEServerConnection($configuration);
				}
				break;
			default:
				throw new \Exception('This DB type is not handled');
		}

		$this->allowedExpenses = isset($configuration['allowedExpenseTypes']) ? $configuration['allowedExpenseTypes'] : [];
		$this->allowedIncomes = isset($configuration['allowedIncomeTypes']) ? $configuration['allowedIncomeTypes'] : [];

		$this->entityManager = $entityManager;
	}

	public function run()
	{
		ini_set('memory_limit', -1);

		$this->importAccounts();

		$financialYears = $this->connection->getFinancialYears();

		$query = 'DELETE Budget\Entity\Expense e WHERE e.age = :age';
		if (isset($financialYears)) {
			$query .= ' AND e.anneeExercice IN (:financialYears)';
		}
		$queryBuilder = $this->entityManager->createQuery($query)->setParameter('age', true);
		if (isset($financialYears)) {
			$queryBuilder->setParameter('financialYears', $financialYears);
		}

		$queryBuilder->execute();
		foreach (Expense::getTypes() as $type) {
			if (in_array($type, $this->allowedExpenses)) {
				$this->importExpenses($type);
			}
		}
		// manage portfolio type here because the query data are loaded from another datasource
		if (in_array(Expense::TYPE_PORTFOLIO, $this->allowedExpenses)) {
			$this->importPortfolios($financialYears);
		}


		/*$this->entityManager ->createQuery('DELETE Budget\Entity\Income i WHERE i.age = :age')
             ->setParameter('age', true)
             ->execute();*/
		foreach (Income::getTypes() as $type) {
			if (in_array($type, $this->allowedIncomes)) {
				$this->importIncomes($type);
			}
		}
	}

	protected function importExpenses($type)
	{
		/**
		 * @var BudgetCode $budgetCode
		 */

		$res = [];

		switch ($type) {
				// DV
			case Expense::TYPE_DV:
				$res = $this->connection->expenseDv();
				break;
				// AE
			case Expense::TYPE_AE:
				$res = $this->connection->expenseAe();
				break;
				// AE Engagés
			case Expense::TYPE_AE_COMMITTED:
				$res = $this->connection->expenseAeCommitted();
				break;
				// AE
			case Expense::TYPE_CP:
				$res = $this->connection->expenseCp();
				break;
				// CP Facturés
			case Expense::TYPE_CP_ESTIMATED:
				$res = $this->connection->expenseCpEstimated();
				break;
				// CP consommés
			case Expense::TYPE_CP_COMMITTED:
				$res = $this->connection->expenseCpCommitted();
				break;
		}

		$managementUnits = [];
		foreach ($this->entityManager->getRepository('Budget\Entity\ManagementUnit')->findAll() as $managementUnit) {
			$managementUnits[$managementUnit->getCode()] = $managementUnit;
		}

		foreach ($res as $row) {
			if(floatval($row['amount']) === floatval(0)) {
				continue;	// remove 0 amount lines
			}
			/** @var Project $project */
			$project = $this->entityManager
				->getRepository('Project\Entity\Project')
				->findOneByCode($row['project_code']);

			if (!$project) {
				$budgetCode = $this->entityManager
					->getRepository(BudgetCode::class)
					->findOneBy(['name' => $row['project_code']]);

				if ($budgetCode) {
					$project = $budgetCode->getProject();
				}
			}

			if ($project) {
				$this->importExpensesInProject($project, $row, $type, $managementUnits);
			} else {
				// ERROR, project not found
			}
		}

		$this->entityManager->flush();
	}

	private function importExpensesInProject($project, $data, $type, $managementUnits)
	{
		$accountCode = $this->getAccountCode($data['account_code'], Account::TYPE_EXPENSE);
		$account = $this->entityManager->getRepository('Budget\Entity\Account')->findOneBy([
			'code' => $accountCode,
			'type' => Account::TYPE_EXPENSE,
		]);
		if (!isset($account)) {
			return;
		}
		$postes = $project->getPosteExpenses()->filter(function ($poste) use ($account) {
			if ($poste->getAccount()) {
				return $poste->getAccount() === $account;
			}
			return false;
		});

		if ($postes->count() > 0) {
			$poste = $postes->first();
		} else {
			// find parent account if any to get an existing "poste"
			$poste = PosteFinder::findPosteByParentAccount(
				$account->getParent(),
				$project,
				$this->entityManager->getRepository('Budget\Entity\PosteExpense')
			);

			if (!isset($poste)) {
				// create a new "poste"
				$poste = new PosteExpense();
				$poste->setAccount($account);
				$poste->setExpenses(new ArrayCollection());
				$poste->setAmount(0);

				$poste->setProject($project);
				$project->getPosteExpenses()->add($poste);

				$this->entityManager->persist($poste);
			}
		}

		$expense = new Expense();
		$expense->setName($data['name']);
		$expense->setType($type);
		$expense->setAmount($data['amount']);
		$expense->setDate(new \DateTime($data['date']));
		$expense->setAnneeExercice($data['exercice']);
		$expense->setAge(true);

		if (isset($data['managementUnit']) && array_key_exists($data['managementUnit'], $managementUnits)) {
			$expense->setManagementUnit($managementUnits[$data['managementUnit']]);
		}

		if (isset($data['date_facture'])) {
			$expense->setDateFacture(new \DateTime($data['date_facture']));
		}
		if (isset($data['mandat'])) {
			$expense->setMandat($data['mandat']);
		}
		if (isset($data['complement'])) {
			$expense->setComplement($data['complement']);
		}
		if (isset($data['bordereau'])) {
			$expense->setBordereau($data['bordereau']);
		}
		if (isset($data['tiers'])) {
			$expense->setTiers($data['tiers']);
		}
		if (isset($data['engagement'])) {
			$expense->setEngagement($data['engagement']);
		}

		$expense->setPoste($poste);
		$poste->getExpenses()->add($expense);

		$this->entityManager->persist($expense);
	}

	private function importPortfolios($years)
	{
		if (null === $this->masterConnection) {
			return;
		}
		$projects = $this->entityManager->getRepository('Project\Entity\Project')->findAll();
		foreach ($projects as $project) {
			foreach ($years as $year) {
				$results = $this->masterConnection->getPortfolio($project->getCode(), $year);
				foreach ($results as $result) {
					$amount = floatval($result['PortefeuilleNonRetenu']) + floatval($result['PortefeuilleRetenu']) + floatval($result['EnValidation']);
					if ($amount === floatval(0)) {
						continue;
					}
					$data = [
						'name' => $result['OrgaBudL'],
						'amount' => $amount,
						'date' => (new \DateTime('now'))->format('Y-m-d'),
						'exercice' => strval($year),
						'account_code' => $result['Nature']
					];
					$this->importExpensesInProject($project, $data, Expense::TYPE_PORTFOLIO, []);
				}
			}
		}
		$this->entityManager->flush();
	}

	protected function importIncomes($type)
	{
		$res = [];

		switch ($type) {
				// DV
			case Income::TYPE_DV:
				$res = $this->connection->incomeDv();
				break;
				// perçu
			case Income::TYPE_PERCEIVED:
				$res = $this->connection->incomePerceived();
				break;
			case Income::TYPE_REQUESTED:
				$res = $this->connection->incomeRequested();
				break;
		}

		$managementUnits = [];
		foreach ($this->entityManager->getRepository('Budget\Entity\ManagementUnit')->findAll() as $managementUnit) {
			$managementUnits[$managementUnit->getCode()] = $managementUnit;
		}

		foreach ($res as $row) {
			if(floatval($row['amount']) === floatval(0)) {
				continue;	// remove 0 amount lines
			}
			/** @var Project $project */
			$project = $this->entityManager
				->getRepository('Project\Entity\Project')
				->findOneByCode($row['project_code']);

			if (!$project) {
				$budgetCode = $this->entityManager
					->getRepository(BudgetCode::class)
					->findOneBy(['name' => $row['project_code']]);

				if ($budgetCode) {
					$project = $budgetCode->getProject();
				}
			}

			if ($project) {
				$accountCode = $this->getAccountCode($row['account_code'], Account::TYPE_INCOME);
				$account = $this->entityManager->getRepository('Budget\Entity\Account')->findOneBy([
					'code' => $accountCode,
					'type' => Account::TYPE_INCOME,
				]);
				if ($account) {
					$income = new Income();
					foreach ($project->getPosteIncomes() as $poste) {
						foreach ($poste->getIncomes() as $_income) {
							if (
								$_income->getAge() == true &&
								$_income->getType() == $type &&
								$_income->getAmount() == $row['amount'] &&
								$_income->getDate() == new \DateTime($row['date']) &&
								$_income->getAnneeExercice() == $row['exercice'] &&
								$_income->getConventionTitle() == $row['convention_title'] &&
								$_income->getTiers() == $row['tiers'] &&
								$_income->getEngagement() == $row['engagement']
							) {
								$income = $_income;
							}
						}
					}

					if (!$income->getId()) {
						$postes = $project->getPosteIncomes()->filter(function ($poste) use ($account) {
							if ($poste->getAccount()) {
								return $poste->getAccount() === $account;
							}
							return false;
						});

						if ($postes->count() > 0) {
							$poste = $postes->first();
						} else {
							$poste = PosteFinder::findPosteByParentAccount(
								$account->getParent(),
								$project,
								$this->entityManager->getRepository('Budget\Entity\PosteIncome')
							);

							if (!isset($poste)) {
								// create a new "poste"
								$poste = new PosteIncome();
								$poste->setAccount($account);
								$poste->setIncomes(new ArrayCollection());
								$poste->setAmount(0);

								$poste->setProject($project);
								$project->getPosteIncomes()->add($poste);

								$this->entityManager->persist($poste);
							}
						}

						$income->setPoste($poste);
						$poste->getIncomes()->add($income);
					}

					$income->setName($row['name']);
					$income->setType($type);
					$income->setAmount($row['amount']);
					$income->setDate(new \DateTime($row['date']));
					$income->setAnneeExercice($row['exercice']);
					$income->setAge(true);

					$income->setConventionTitle($row['convention_title']);
					$income->setTiers($row['tiers']);
					$income->setEngagement($row['engagement']);

					if (isset($row['managementUnit']) && array_key_exists($row['managementUnit'], $managementUnits)) {
						$income->setManagementUnit($managementUnits[$row['managementUnit']]);
					}

					if (!$income->getId()) {
						$this->entityManager->persist($income);
					}
				} else {
					// ERROR, account not found
				}
			} else {
				// ERROR, project not found
			}
		}

		$this->entityManager->flush();
	}


	private function getAccountCode($ageAccountCode, $type)
	{
		$accountCode = $ageAccountCode;
		if (empty($accountCode)) {
			$defaultAccount = $this->entityManager->getRepository('Budget\Entity\Account')->findOneBy(['default' => true, 'type' => $type]);
			if (isset($defaultAccount)) {
				$accountCode = $defaultAccount->getCode();
			}
		}
		return $accountCode;
	}

	/**
	 * @todo : arborescence des comptes
	 */
	protected function importAccounts()
	{
		$res = $this->connection->accounts();

		foreach ($res as $row) {
			$type = $row['EST_DEPENSE'] == 0 ? Account::TYPE_INCOME : Account::TYPE_EXPENSE;
			$account = $this->entityManager->getRepository('Budget\Entity\Account')->findOneBy([
				'code' => $row['CODE'],
				'type' => $type,
			]);
			if (!$account) {
				$account = new Account();
				$account->setType($type);
				$account->setCode($row['CODE']);
				$account->setName($row['LIBELLE']);

				$this->entityManager->persist($account);
			}
		}

		$this->entityManager->flush();
	}
}
