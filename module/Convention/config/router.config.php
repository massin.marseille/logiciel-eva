<?php

namespace Convention;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'convention'   => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/convention',
                    'defaults' => [
                        'controller' => 'Convention\Controller\Convention',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'Convention\Controller\Convention',
                                'action'     => 'form',
                            ],
                        ],
                    ],
                ],
            ],
            'api'       => [
                'type'          => 'Literal',
                'options'       => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'convention' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/convention[/:id]',
                            'defaults' => [
                                'controller' => 'Convention\Controller\API\Convention',
                            ],
                        ],
                    ],
                    'convention-line' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/convention-line[/:id]',
                            'defaults' => [
                                'controller' => 'Convention\Controller\API\Line',
                            ],
                        ],
                    ],
                ],
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
           Controller\ConventionController::class     => ServiceLocatorFactory::class,
           Controller\API\ConventionController::class => ServiceLocatorFactory::class,
           Controller\API\LineController::class       => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Convention\Controller\Convention'     => Controller\ConventionController::class,
            'Convention\Controller\API\Convention' => Controller\API\ConventionController::class,
            'Convention\Controller\API\Line'       => Controller\API\LineController::class
        ]
    ]
];
