(function (app) {

    app.controller('importController', [
        'FileUploader', '$scope', '$http', 'flashMessenger', 'translator',
        function importController(FileUploader, $scope, $http, flashMessenger, translator) {
            var self = this;

            self.error_general = [];
            self.url = '';
            self.modal = $('#infoTooltipModal');

            self.entity  = null;
            self.loading = {
                upload: false,
                parse: false,
                validate: false,
                save: false
            };
            self.options = {
                delimiter: ';',
                firstLine: false,
                file: null,
                isParsed: false,
                isValidated: false
            };
            self.data = {};
            self.uploader = new FileUploader();
            self.reader   = new FileReader();


            self.uploader.filters.push({
                name: 'csvFilter',
                fn: function(item) {
                    //var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    var type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase() + '|';
                    return '|csv|vnd.ms-excel|'.indexOf(type) !== -1;
                }
            });
            self.uploader.onAfterAddingFile = function(fileItem) {
                self.loading.upload = true;
                self.reader.readAsDataURL(fileItem._file);
            };
            self.uploader.onWhenAddingFileFailed  = function(fileItem) {
                self.loading.upload      = false;
                self.options.isParsed    = false;
                self.options.isValidated = false;
                self.options.file        = null;
            };

            self.reader.onloadend = function(e) {
                $scope.$apply(function() {
                    self.options.file = e.target.result;
                    self.loading.upload      = false;
                    self.options.isParsed    = false;
                    self.options.isValidated = false;
                });
            };

            self.init = function init(entity){
                self.entity = entity;
                self.url = '/import/' + self.entity;
            };

            self.parse = function parse() {
                self.loading.parse = true;
                self.errors        = [];
                $http({
                    method: 'POST',
                    url: self.url + '/parse',
                    data: self.options
                }).then(function(res) {
                    if (res.data.success) {
                        self.options.isParsed    = true;
                        self.options.isValidated = false;
                        self.lines               = res.data.lines;
                        self.properties          = res.data.properties;
                    } else {
                        flashMessenger.error(translator('import_message_file_not_valid'))
                    }

                    self.loading.parse = false;
                }, function(err) {
                    self.loading.parse = false;
                    flashMessenger.error(translator('import_message_file_not_valid'))
                });
            };
            self.parseKeyword = function parseKeyword() {
                self.loading.parse = true;
                self.errors        = [];
                $http({
                    method: 'POST',
                    url: self.url + '/parse',
                    data: self.options
                }).then(function(res) {
                    if (res.data.success) {
                        try {
                            let group = self.keywordToTree(res.data.lines);
                        } catch (e) {
                            self.loading.parse = false;
                            flashMessenger.error(translator('import_message_file_not_valid'))
                            return;
                        }
                        self.options.isParsed    = true;
                        self.options.isValidated = false;
                        self.groups               = self.keywordToTree(res.data.lines);
                        self.properties          = res.data.properties;
                    } else {
                        flashMessenger.error(translator('import_message_file_not_valid'))
                    }

                    self.loading.parse = false;
                }, function(err) {
                    self.loading.parse = false;
                    flashMessenger.error(translator('import_message_file_not_valid'))
                });
            };

            /**
             * Transform a csv file with keyword to a tree
             * @param $keyword
             * @returns {*}
             */
            self.keywordToTree = function keywordToTree($keyword) {
                let words = [];
                $keyword.forEach((array, i) => {
                    let index = self.getExistingIndexOnArray(array);
                    if (array[index] !== null)
                        words.push({name: array[index], rank: parseInt(index), keywords: [], id: i, children: []});
                });
                try {
                    const maxRank = Math.max.apply(Math, words.map(x => x.rank));
                    for (let i = maxRank; i > 0; i--) {
                        words.filter(x => x.rank === i).forEach(x => {
                            let possibility = words.filter(y => y.id < x.id && y.rank === x.rank - 1);
                            let parentNode = possibility[possibility.length - 1];
                            if (parentNode.rank === 0) {
                                parentNode.keywords.push(x);
                            } else {
                                parentNode.children.push(x);
                            }
                            words[parentNode.index] = parentNode;
                        })
                    }
                    return words.filter(x => x.rank === 0);
                } catch (e) {
                    throw new Error();
                }
            }

            /**
             * Get index of the keyword array
             */
            self.getExistingIndexOnArray = function getExistingIndexOnArray($array) {
                let simplifiedArray = $array.filter(x => x !== '')
                return $array.findIndex(x => x === simplifiedArray[simplifiedArray.length - 1]);
            }

            self.validate = function validate() {
                self.loading.validate = true;
                self.errors           = [];
                $http({
                    method: 'POST',
                    url: self.url + '/validate',
                    data: {
                        properties: self.properties,
                        lines: self.lines
                    }
                }).then(function(res) {
                    self.options.isValidated = true;
                    self.error_general = [];
                    for (const property in res.data.errors[0]) {
                        if(!self.properties.includes(property)) {
                            self.error_general.push({ [res.data.errors[0][property]['__translate_field']] : res.data.errors[0][property]});
                            delete res.data.errors[0][property]['__translate_field'];
                        }
                    }

                    if (!res.data.success) {
                        self.errors = res.data.errors;
                    }

                    self.loading.validate = false;
                }, function(err) {
                    self.loading.validate = false;
                });
            };

            self.addLine = function addLine() {
                var line = [];
                var errors = [];
                for (var i in self.properties) {
                    line.push(null);
                    errors.push(null);
                }

                self.lines.push(line);
                self.errors.push(errors);
            };

            self.addColumn = function addColumn() {
                self.properties.push(null);
                for (var i in self.lines) {
                    self.lines[i].push(null);
                    if (typeof self.errors[i] == 'undefined') {
                        self.errors[i] = [];
                    }
                    self.errors[i].push(null);
                }
            };

            self.deleteLine = function deleteLine(index) {
                if (confirm(translator('import_question_delete_line'))) {
                    self.lines.splice(index, 1);
                    self.errors.splice(index, 1);
                }
            };

            self.save = function save() {
                if (confirm(translator('import_question_lets_save').replace('%1', self.lines.length))) {
                    self.loading.save = true;
                    $http({
                        method: 'POST',
                        url: self.url + '/save',
                        data: {
                            properties: self.properties,
                            lines: self.lines
                        }
                    }).then(function (res) {
                        self.errors = res.data.errors;

                        var saved = 0;
                        var total = 0;
                        var ok    = [];
                        for (var i in self.lines) {
                            if (res.data.errors[i] === null && res.data.exceptions[i] === null) {
                                ok.push(i);
                                saved++;
                            }
                            total++;
                        }

                        var deleted = 0;
                        for (var i in ok) {
                            var index = ok[i] -= deleted;
                            self.lines.splice(index, 1);
                            self.errors.splice(index, 1);

                            deleted++;
                        }

                        self.loading.save = false;
                        alert(translator('import_message_lines_saved').replace('%1', total).replace('%2', saved));
                    }, function (err) {
                        self.loading.save = false;
                    });
                }
            };

            self.saveKeyword = function saveKeyword($type) {
                if (confirm(translator('import_question_lets_save').replace('%1', self.groups.length))) {
                    self.loading.save = true;
                    console.log($type);
                    $http({
                        method: 'POST',
                        url: self.url + '/save-keyword',
                        data: {
                            properties: self.properties,
                            groups: self.groups,
                            type: $type
                        }
                    }).then(function (res) {
                        self.errors = res.data.errors;

                        var saved = 0;
                        var total = 0;
                        var ok    = [];
                        for (var i in self.lines) {
                            if (res.data.errors[i] === null && res.data.exceptions[i] === null) {
                                ok.push(i);
                                saved++;
                            }
                            total++;
                        }

                        var deleted = 0;
                        for (var i in ok) {
                            var index = ok[i] -= deleted;
                            self.lines.splice(index, 1);
                            self.errors.splice(index, 1);

                            deleted++;
                        }

                        self.loading.save = false;
                        alert(translator('import_message_lines_saved').replace('%1', total).replace('%2', saved));
                    }, function (err) {
                        self.loading.save = false;
                    });
                }
            };


            self.disableOption = function (index, option) {
                return (self.properties.indexOf(option) > -1 && self.properties.indexOf(option) !== index);
            };

            self.openModal = function openModal () {
                self.modal.modal('show');
            }

            self.closeModal = function openModal () {
                self.modal.modal('hide');
            }

        }
    ]);

})(window.EVA.app);
