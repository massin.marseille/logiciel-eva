<?php

namespace PostParc\Helper;

use Directory\Entity\Contact;
use Directory\Entity\ContactJobStructure;
use Directory\Entity\Job;
use Directory\Entity\Structure;
use Doctrine\Common\Collections\ArrayCollection;
use Keyword\Entity\Association;
use Keyword\Entity\Group;
use Keyword\Entity\Keyword;

class DataProcessor
{
    private $_entityManager;
    private $_dataMapper;

    private $_organisationsTypes;
    private $_groups;
    private $_pfos;
    private $_organisations;
    private $_persons;
    private $_functions;

    private const DEFAULT_JOB = 'membre';

    private const GROUP_POST_PARC = 'Groupes POSTPARC';
    private const ORGA_TYPE_POST_PARC = 'Types d\'organismes POSTPARC';

    private const IMPORT_KEYWORDS_GRP = 'IMPORT POSTPARC';
    private const SYNC_KEYWORD = 'SYNC-POSTPARC';
    private const DLT_KEYWORD = 'SUPP-POSTPARC';

    public function __construct($entityManager, $dataMapper, $organisationsTypes, $groups, $organisations, $persons, $functions, $pfos)
    {
        $this->_entityManager = $entityManager;
        $this->_dataMapper = $dataMapper;
        $this->_organisationsTypes = $organisationsTypes;
        $this->_groups = $groups;
        $this->_pfos = $pfos;
        $this->_organisations = $organisations;
        $this->_persons = $persons;
        $this->_functions = $functions;
    }

    public function process()
    {
        $this->initSynchroKeywords();
        $this->processOrganisationsTypes();
        $this->processGroups();
        $this->processStructures();
        $this->processContacts();
        $this->processJobs();
        $this->processContactJobStructures();
    }

    private function initSynchroKeywords()
    {
        $synchroKeyrowdsGroup = $this->getSynchroKeywordsGroup();
        if (null == $synchroKeyrowdsGroup) {
            $this->createSynchroKeywordsGroup();
        }
    }

    private function processOrganisationsTypes()
    {
        $group = $this->getPostParcOrganizationsTypesReferential();

        if (null == $group) {
            $group = $this->createPostParcOrganizationsTypesReferential();
        }

        foreach ($this->_organisationsTypes as $orgaType) {
            $keyword = $this->getKeywordByGroupAndExternalId($group, $orgaType->id);
            if (null == $keyword) {
                $keyword = new Keyword();
                $keyword->setExternalId($orgaType->id);
            }

            $keyword->setGroup($group);
            $keyword->setName($orgaType->name);
            $keyword->setOrder(1);

            $this->_entityManager->persist($keyword);
        }

        $this->_entityManager->flush();
    }

    public function processGroups()
    {
        $keywordGroup = $this->getPostParcGroupesReferential();
        if (null == $keywordGroup) {
            $keywordGroup = $this->createPostParcGroupesReferential();
        }

        if (!in_array('contact', $keywordGroup->getEntities())) {
            $keywordGroup->setEntities(['structure', 'contact']);
            $this->_entityManager->persist($keywordGroup);
            $this->_entityManager->flush();
        }

        $rootGroups = array_filter($this->_groups, function ($group) {
            return $group->level == 0;
        });

        $this->addGroupKeywordArbo($rootGroups, null);
    }

    public function addGroupKeywordArbo($postParcGroups, $parent)
    {
        foreach ($postParcGroups as $postParcGroup) {

            $keyword = $this->getKeywordByGroupAndExternalId($this->getPostParcGroupesReferential(), $postParcGroup->id);
            if (null == $keyword) {
                $keyword = new Keyword();
                $keyword->setExternalId($postParcGroup->id);
                if (isset($parent)) {
                    $keyword->addParent($parent);
                }
            }

            $keyword->setGroup($this->getPostParcGroupesReferential());
            $keyword->setName($postParcGroup->name);
            $keyword->setOrder(1);

            $this->_entityManager->persist($keyword);
            $this->_entityManager->flush();

            if (isset($postParcGroup->__children)) {
                $this->addGroupKeywordArbo($postParcGroup->__children, $keyword);
            }
        }

        $this->_entityManager->flush();
    }

    private function processContactJobStructures()
    {
        foreach ($this->_pfos as $entity) {
            $this->addOrUpdateContactJobStructure($entity);
        }

        $this->_entityManager->flush();
    }

    public function processStructures()
    {
        $synchronizedPostParcIds = new ArrayCollection();

        foreach ($this->_organisations as $entity) {
            $this->addOrUpdateStructure($entity);
            $synchronizedPostParcIds->add($entity->id);
        }

        $this->_entityManager->flush();

        $this->updateStructuresSynchroKeywords($synchronizedPostParcIds);
    }

    public function processContacts()
    {
        $synchronizedPostParcIds = new ArrayCollection();
        foreach ($this->_persons as $entity) {
            $this->addOrUpdateContact($entity);
            $synchronizedPostParcIds->add($entity->id);
        }

        $this->_entityManager->flush();
        $this->updateContactsSynchroKeywords($synchronizedPostParcIds);
    }

    public function processJobs()
    {
        foreach ($this->_functions as $entity) {
            $this->addOrUpdateJob($entity);
        }
    }

    private function addOrUpdateContact($postPerson)
    {
        if (null == $postPerson) {
            return null;
        }

        $contact = $this->getContactByPostParcId($postPerson->id);
        if (null == $contact) {
            $contact = new Contact();
            $contact->setPostParcId($postPerson->id);
        }

        $this->_dataMapper->mapContact($postPerson, $contact);
        $this->_entityManager->persist($contact);
        $this->_entityManager->flush();

        $this->updateEntityGroups('Directory\Entity\Contact', $contact->getId(), $postPerson->groups);
        return $contact;
    }

    public function addOrUpdateStructure($postParcOrganisation)
    {
        if (null == $postParcOrganisation) {
            return null;
        }

        $structure = $this->getStructureByPostParcId($postParcOrganisation->id);
        if (null == $structure) {
            $structure = new Structure();
            $structure->setPostParcId($postParcOrganisation->id);
        }

        $this->_dataMapper->mapStructure($postParcOrganisation, $structure);

        $this->_entityManager->persist($structure);
        $this->_entityManager->flush();

        $this->updateEntityGroups('Directory\Entity\Structure', $structure->getId(), $postParcOrganisation->groups);

        $this->updateStructureType($structure, $postParcOrganisation->organizationType);

        return $structure;
    }

    private function updateStructureType($structure, $postParcOrgaType)
    {
        $group = $this->getPostParcOrganizationsTypesReferential();
        $queryBuilder = $this->_entityManager->createQueryBuilder();

        $queryBuilder
            ->select('o')
            ->from(Association::class, 'o')
            ->join('o.keyword', 'k')
            ->where('o.primary = :id')
            ->andWhere('o.entity = :entity')
            ->andWhere('k.group = :group')
            ->setParameters([
                'id' => $structure->getId(),
                'entity' => 'Directory\Entity\Structure',
                'group' => $group,
            ]);

        $association = $queryBuilder->getQuery()->getOneOrNullResult();

        if (null == $postParcOrgaType) {
            if (null !== $association) {
                $this->_entityManager->remove($association);
            }
        } else {
            if (null == $association) {
                $association = new Association();
                $association->setEntity('Directory\Entity\Structure');
                $association->setPrimary($structure->getId());
            }

            $association->setKeyword($this->getKeywordByGroupAndExternalId($group, $postParcOrgaType->id));
            $this->_entityManager->persist($association);
        }

        $this->_entityManager->flush();
    }

    private function updateEntityGroups($entity, $entityId, $postParcGroups)
    {
        $this->clearGroupAssociationsForEntity($entity, $entityId);
        $group = $this->getPostParcGroupesReferential();
        $groupsIds = new ArrayCollection();

        foreach ($postParcGroups as $postParcGroup) {
            // some data from postparc are duplicated
            if (!$groupsIds->contains($postParcGroup->id)) {
                $keyword = $this->getKeywordByGroupAndExternalId($group, $postParcGroup->id);

                if (null !== $keyword) {
                    $association = new Association();
                    $association->setEntity($entity);
                    $association->setPrimary($entityId);
                    $association->setKeyword($keyword);
                    $this->_entityManager->persist($association);
                }

                $groupsIds->add($postParcGroup->id);
            }

        }

        $this->_entityManager->flush();
    }

    private function addOrUpdateContactJobStructure($postParcEntity)
    {
        if (null == $postParcEntity->person || null == $postParcEntity->organization) {
            return;
        }

        $contact = $this->getContactByPostParcId($postParcEntity->person->id);
        if (null == $contact) {
            return;
        }

        $structure = $this->getStructureByPostParcId($postParcEntity->organization->id);
        if (null == $structure) {
            return;
        }

        if (null == $postParcEntity->personFunction) {
            $job = $this->getDefaultJob();
        } else {
            $job = $this->getJobByName($postParcEntity->personFunction);
        }

        $contactJobStructure = $this->_entityManager->getRepository('Directory\Entity\ContactJobStructure')->findOneBy([
            'job' => $job,
            'structure' => $structure,
            'contact' => $contact,
        ]);

        if (null !== $contactJobStructure) {
            return;
        }

        $contactJobStructure = new ContactJobStructure();
        $contactJobStructure->setJob($job);
        $contactJobStructure->setStructure($structure);
        $contactJobStructure->setContact($contact);

        $this->_entityManager->persist($contactJobStructure);

    }

    private function addOrUpdateJob($postParcFunction)
    {
        if (null == $postParcFunction) {
            return null;
        }

        $job = $this->getJobByName($postParcFunction->name);
        if (null == $job) {
            $job = new Job();
            $job->setPostParcId($postParcFunction->id);
        }

        $job->setName($postParcFunction->name);

        $this->_entityManager->persist($job);
        return $job;
    }

    private function updateContactsSynchroKeywords($synchronizedPostParcIds)
    {
        $this->updateSynchroKeywords($this->getPostParcContacts(), 'Directory\Entity\Contact', $synchronizedPostParcIds);
    }

    private function updateStructuresSynchroKeywords($synchronizedPostParcIds)
    {
        $this->updateSynchroKeywords($this->getPostParcStructures(), 'Directory\Entity\Structure', $synchronizedPostParcIds);
    }

    private function updateSynchroKeywords($entities, $entityName, $synchronizedPostParcIds)
    {
        $syncKeyword = $this->getSyncKeyword();
        $deletedKeyword = $this->getDeletedKeyword();

        foreach ($entities as $persistedEntity) {
            $existingDeletedKeyword = $this->_entityManager->getRepository('Keyword\Entity\Association')
                ->findOneBy(['primary' => $persistedEntity->getId(), 'entity' => $entityName, 'keyword' => $deletedKeyword]);

            $existingSyncKeyword = $this->_entityManager->getRepository('Keyword\Entity\Association')
                ->findOneBy(['primary' => $persistedEntity->getId(), 'entity' => $entityName, 'keyword' => $syncKeyword]);

            $association = null;
            if (!isset($existingDeletedKeyword) && !isset($existingSyncKeyword)) {
                $association = new Association();
                $association->setEntity($entityName);
                $association->setPrimary($persistedEntity->getId());
            } else {
                $association = isset($existingSyncKeyword) ? $existingSyncKeyword : $existingDeletedKeyword;
            }

            if (!$synchronizedPostParcIds->contains($persistedEntity->getPostParcId())) {
                $association->setKeyword($deletedKeyword);
            } else {
                $association->setKeyword($syncKeyword);
            }

            $this->_entityManager->persist($association);
        }

        $this->_entityManager->flush();
    }

    private function getStructureByPostParcId($id)
    {
        return $this->_entityManager->getRepository('Directory\Entity\Structure')->findOneByPostParcId($id);
    }

    private function getPostParcStructures()
    {
        $qb = $this->_entityManager->createQueryBuilder();
        return $qb->select("s")->from("Directory\Entity\Structure", 's')
            ->where($qb->expr()->isNotNull("s.postParcId"))
            ->getQuery()->getResult();
    }

    private function getPostParcContacts()
    {
        $qb = $this->_entityManager->createQueryBuilder();
        return $qb->select("c")->from("Directory\Entity\Contact", 'c')
            ->where($qb->expr()->isNotNull("c.postParcId"))
            ->getQuery()->getResult();
    }

    private function getContactByPostParcId($id)
    {
        return $this->_entityManager->getRepository('Directory\Entity\Contact')->findOneByPostParcId($id);
    }

    private $keyWordGrpExternalIdCache = [[]];
    private function getKeywordByGroupAndExternalId($group, $id)
    {
        if (isset($keyWordGrpExternalIdCache[$group->getId()][$id])) {
            return $keyWordGrpExternalIdCache[$group->getId()][$id];
        }

        $keyword = $this->_entityManager->getRepository('Keyword\Entity\Keyword')->findOneBy([
            'group' => $group,
            'externalId' => $id,
        ]);

        $keyWordGrpExternalIdCache[$group->getId()][$id] = $keyword;
        return $keyword;
    }

    private function clearGroupAssociationsForEntity($entity, $entityId)
    {
        $group = $this->getPostParcGroupesReferential();

        $queryBuilder = $this->_entityManager->createQueryBuilder();

        $queryBuilder
            ->select('o')
            ->from(Association::class, 'o')
            ->join('o.keyword', 'k')
            ->where('o.primary = :id')
            ->andWhere('k.group = :group')
            ->andWhere('o.entity = :entity')
            ->setParameters([
                'id' => $entityId,
                'group' => $group,
                'entity' => $entity,
            ]);

        $associations = $queryBuilder->getQuery()->getResult();

        foreach ($associations as $association) {
            $this->_entityManager->remove($association);
        }

        $this->_entityManager->flush();
    }

    private function getJobByName($name)
    {
        return $this->_entityManager->getRepository('Directory\Entity\Job')->findOneByName($name);
    }

    private function getSynchroKeywordsGroup()
    {
        return $this->getKeywordGroup(self::IMPORT_KEYWORDS_GRP);
    }

    private function getPostParcGroupesReferential()
    {
        return $this->getKeywordGroup(self::GROUP_POST_PARC);
    }

    private function getPostParcOrganizationsTypesReferential()
    {
        return $this->getKeywordGroup(self::ORGA_TYPE_POST_PARC);
    }

    private $keywordGroupsCache = [];
    private function getKeywordGroup($name)
    {
        if (isset($keywordGroupsCache[$name])) {
            return $keywordGroupsCache[$name];
        }

        $grp = $this->_entityManager->getRepository('Keyword\Entity\Group')->findOneByName($name);
        $keywordGroupsCache[$name] = $grp;

        return $grp;
    }

    private function getSyncKeyword()
    {
        return $this->_entityManager->getRepository('Keyword\Entity\Keyword')->findOneByName(self::SYNC_KEYWORD);
    }

    private function getDeletedKeyword()
    {
        return $this->_entityManager->getRepository('Keyword\Entity\Keyword')->findOneByName(self::DLT_KEYWORD);
    }

    private function createSynchroKeywordsGroup()
    {
        $this->createKeywordsGroup(self::IMPORT_KEYWORDS_GRP, ['structure', 'contact'], false, false);
        $group = $this->getKeywordGroup(self::IMPORT_KEYWORDS_GRP);
        $this->createKeyword($group, self::SYNC_KEYWORD, 1);
        $this->createKeyword($group, self::DLT_KEYWORD, 2);
    }

    private function createPostParcGroupesReferential()
    {
        return $this->createKeywordsGroup(self::GROUP_POST_PARC, ['structure', 'contact'], true, true);
    }

    private function createPostParcOrganizationsTypesReferential()
    {
        return $this->createKeywordsGroup(self::ORGA_TYPE_POST_PARC, ['structure'], true, true);
    }

    private function createKeywordsGroup($name, $entities, $multiple, $isReference)
    {
        $group = new Group();
        $group->setName($name);
        $group->setMultiple($multiple);
        $group->setEntities($entities);
        if ($isReference) {
            $group->setType(Group::GROUP_TYPE_REFERENCE);
        }

        $this->_entityManager->persist($group);
        $this->_entityManager->flush();
        return $group;
    }

    private function createKeyword($group, $name, $order)
    {
        $keyword = new Keyword();
        $keyword->setGroup($group);
        $keyword->setName($name);
        $keyword->setOrder($order);
        $this->_entityManager->persist($keyword);
    }

    private function getDefaultJob()
    {
        $job = $this->getJobByName(self::DEFAULT_JOB);
        if (null !== $job) {
            return $job;
        }

        $job = new Job();
        $job->setName(self::DEFAULT_JOB);
        $this->_entityManager->persist($job);
        $this->_entityManager->flush();
        return $job;
    }
}
