(function(app) {

    app.controller('alertListController', [
        'alertService', '$scope', 'flashMessenger', 'translator',
        function alertListController(alertService, $scope, flashMessenger, translator) {
            var self = this;

            self.alerts = [];
            self.options = {
                name: 'alerts-list',
                col: [
                    'id',
                    'name',
                    'category',
                    'recipients.id',
                    'recipients.name',
                    'recipients.avatar',
                    'toManagers',
                    'toValidators',
                    'toMembers',
                    'toWorkFlows',
                    'subject',
                    'enabled',
                    'periodicity',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    alertService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(alert) {
                self.editedLineErrors = {};
                self.editedLine       = alert;
                self.editedLineFields = angular.copy(alert);
            };

            self.saveEditedLine = function saveEditedLine(alert) {
                self.editedLineErrors = {};
                alertService.save(angular.extend({}, alert, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        alert = angular.extend(alert, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('alert_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteAlert = function deleteAlert(alert) {
                if (confirm(translator('alert_question_delete').replace('%1', alert.name))) {
                    alertService.remove(alert).then(function (res) {
                        if (res.success) {
                            self.alerts.splice(self.alerts.indexOf(alert), 1);
                            flashMessenger.success(translator('alert_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.enableAlert = function (alert) {
                self.editedGroupErrors  = {};

                alertService.save(alert, {
                    col: ['enabled']
                }).then(function (res) {
                    if (res.success) {
                        alert = angular.extend(alert, res.object);

                        if (alert.enabled == true) {
                            flashMessenger.success(translator('alert_message_enabled'));
                        } else {
                            flashMessenger.success(translator('alert_message_disabled'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }
                });
            };

            self.enableToManagers = function (alert) {
                self.editedGroupErrors  = {};

                alertService.save(alert, {
                    col: ['toManagers']
                }).then(function (res) {
                    if (res.success) {
                        alert = angular.extend(alert, res.object);

                        if (alert.toManagers == true) {
                            flashMessenger.success(translator('alert_toManagers_enabled'));
                        } else {
                            flashMessenger.success(translator('alert_toManagers_disabled'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }
                });
            };

            self.enableToValidators = function (alert) {
                self.editedGroupErrors  = {};

                alertService.save(alert, {
                    col: ['toValidators']
                }).then(function (res) {
                    if (res.success) {
                        alert = angular.extend(alert, res.object);

                        if (alert.toValidators == true) {
                            flashMessenger.success(translator('alert_toValidators_enabled'));
                        } else {
                            flashMessenger.success(translator('alert_toValidators_disabled'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }
                });
            };

            self.enableToMembers = function (alert) {
                self.editedGroupErrors  = {};

                alertService.save(alert, {
                    col: ['toMembers']
                }).then(function (res) {
                    if (res.success) {
                        alert = angular.extend(alert, res.object);

                        if (alert.toMembers == true) {
                            flashMessenger.success(translator('alert_toMembers_enabled'));
                        } else {
                            flashMessenger.success(translator('alert_toMembers_disabled'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }
                });
            };
            self.enableToWorkFlows = function (alert) {
                self.editedGroupErrors  = {};

                alertService.save(alert, {
                    col: ['toMembers']
                }).then(function (res) {
                    if (res.success) {
                        alert = angular.extend(alert, res.object);

                        if (alert.toWorkFlows == true) {
                            flashMessenger.success(translator('alert_toWorkflows_enabled'));
                        } else {
                            flashMessenger.success(translator('alert_toWorkflows_disabled'));
                        }
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedGroupErrors[field] = err.fields[field];
                    }
                });
            };
        }
    ])

})(window.EVA.app);
