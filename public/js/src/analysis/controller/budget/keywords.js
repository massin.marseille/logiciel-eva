(function (app) {

    app.controller('keywordsController', [
        '$http', 'groupService', 'translator', 'treeBuilderService', '$scope', 'queryService', '$rootScope',
        function keywordsController($http, groupService, translator, treeBuilderService, $scope, queryService, $rootScope) {
            var self = this;

            self.filters = {
                project: {},
                group  : null,
                data   : null
            };
            self.nbFilters = 1;

            self.groups   = [];
            self.keywords = [];
            self.charts   = [];

            self.loading = false;

            self.init = function () {
                self.initQueries();
                groupService.findAll({
                    col   : ['id', 'name', 'type'],
                    search: {
                        type: 'list',
                        data: {
                            sort   : 'name',
                            order  : 'asc',
                            filters: {
                                entities: {
                                    op : 'sa_cnt',
                                    val: 'project'
                                }
                            }
                        }
                    }
                }).then(function (res) {
                    self.groups        = res.rows;
                    self.filters.group = self.groups[0].id;
                });
            };

            self.loadChart = function (parent, level) {
                var data = [];
                if (parent == null) {
                    for (var i in self.keywords) {
                        var keyword = self.keywords[i];
                        data.push({
                            label  : keyword.name,
                            value  : keyword.data,
                            keyword: keyword
                        });
                    }
                } else {
                    for (var i in parent.children) {
                        var keyword = parent.children[i];
                        data.push({
                            label  : keyword.name,
                            value  : keyword.data,
                            keyword: keyword
                        });
                    }
                }

                self.charts.splice(level);
                self.charts.push({
                    api    : {},
                    parent : parent,
                    level  : level,
                    options: {
                        chart: {
                            type                   : 'pieChart',
                            showControls           : false,
                            showValues             : true,
                            stacked                : true,
                            height                 : 400,
                            useInteractiveGuideline: true,
                            labelsOutside          : true,
                            x                      : function (d) {
                                return d.label;
                            },
                            y                      : function (d) {
                                return d.value;
                            },
                            yAxis                  : {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' €';
                                }
                            },
                            tooltip                : {
                                valueFormatter: function (d) {
                                    return d3.format(',')(d) + ' €';
                                }
                            },
                            callback               : function (chart) {
                                chart.pie.dispatch.on('elementClick', function (e) {
                                    $(e.element).parents('svg').find('.slice-selected').removeClass('slice-selected');
                                    $scope.$apply(function () {
                                        if (e.data.keyword.children.length > 0) {
                                            $(e.element).find('path').addClass('slice-selected');
                                            self.loadChart(e.data.keyword, level + 1);
                                        } else {
                                            self.charts.splice(level + 1);
                                        }
                                    });
                                });

                            }
                        }
                    },
                    data   : data
                });
            };

            self.applyFilters = function applyFilters() {
                if (self.filters.group !== null && self.filters.data !== null) {
                    self.loading = true;
                    self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.group).length + Object.keys(self.filters.data).length

                    var params           = angular.copy({filters: self.filters});
                    //params.filters.group = params.filters.group.id;

                    $http({
                        url   : '/analysis/budget/keywords?' + $.param(params),
                        method: 'GET'
                    }).then(function (res) {
                        self.keywords = treeBuilderService.toNNTree(res.data.res.keywords);
                        self._group   = angular.copy(self.filters.group);

                        self.loadChart(null, 0);

                        self.loading = false;
                    });
                }
            };

            self.resetFilters = function () {
                self.filters.project = {};
                self.filters.group   = self.groups[0].id;
                self.filters.data    = null;
                self.applyFilters();
            };


            self.export = function (parent, group) {
                var params           = angular.copy({filters: self.filters});
                params.filters.group = group;
                params.parent        = parent;
                params.export        = true;

                window.open('/analysis/budget/keywords?' + $.param(params));
            };

            self.getGroupType = function getGroupType(type) {
                return translator('group_type_' + type);
            };

            // QUERIES
            self.queries          = [];
            self.query            = {
                list: 'analysis-budget-keywords'
            };
            self.errors           = {
                fields: {}
            };
            self.loadingQueries   = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries        = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };
        }
    ]);
})(window.EVA.app);
