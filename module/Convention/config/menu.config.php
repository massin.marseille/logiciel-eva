<?php

$convention = [
    'icon' => 'convention',
    'title' => 'convention_module_title',
    'href' => 'convention',
    'right' => 'convention:e:convention:r',
    'priority' => 980,
];

return [
    'menu' => [
        'client-menu' => [
            'convention' => $convention,
        ],
        'client-menu-parc' => [
            'data' => [
                'children' => [
                    'convention' => $convention,
                ],
            ],
        ],
    ],
];
