(function (app) {

    app.controller('projectKeywordController', [
        '$timeout', '$scope', '$http', 'treeBuilderService', 'groupService',
        function projectKeywordController($timeout, $scope, $http, treeBuilderService, groupService) {
            var self         = this;
            var first        = true;
            var shouldReload = false;

            self.datas = [];

            self.divideTimes = false;

            self.makeDivideTimes = function () {
                $scope.$emit('divideTimes', {divideTimes: self.divideTimes});
            };

            self.sunburst = {
                api    : {},
                options: {
                    chart: {
                        groupColorByParent: false,
                        type    : 'sunburstChart',
                        height  : 400,
                        color   : d3.scale.category20(),
                        duration: 500,
                        mode    : 'size',
                        tooltip : {
                            contentGenerator: function (obj) {
                                return '' +
                                    '<table>' +
                                    '    <tr>' +
                                    '        <td class="legend-color-guide">' +
                                    '            <div style="background-color:' + obj.color + '"></div>' +
                                    '        </td>' +
                                    '        <td class="key">' + obj.data.name + '</td>' +
                                    '        <td class="value">' + obj.data.realSize + '</td>' +
                                    '    </tr>' +
                                    '</table>';
                            }
                        },
                        key     : function (d) {
                            return d.name + d.i;
                        }
                    }
                }
            };

            $scope.$on('switchTab', function (event, data) {
                if (data.active === 'projectKeyword' && shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                }
            });

            $scope.$on('applyFilters', function (event, data) {
                if ((data.active === 'projectKeyword' && data.shouldReload) || (typeof data.divideTimes !== 'undefined')) {
                    shouldReload = false;
                    data.divideTimes = self.divideTimes;
                    self.applyFilters(data);
                } else if (data.shouldReload) {
                    shouldReload = true;
                }
            });

            self.applyFilters = function (data) {
                self.loadingOn();
                self.i = 0;
                $http({
                    url   : '/analysis/time/projectKeywords?' + $.param({filters: data.filters, divideTimes: data.divideTimes}),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;

                    for (var i in self.datas.res.projectKeywords) {
                        self.datas.res.projectKeywords[i] = treeBuilderService.toNNTree(self.datas.res.projectKeywords[i]);
                    }

                    if (first) {
                        groupService.findAll({
                            col   : ['id', 'name', 'type'],
                            search: {
                                type: 'list',
                                data: {
                                    sort   : 'name',
                                    order  : 'asc',
                                    filters: {
                                        entities: {
                                            op : 'sa_cnt',
                                            val: 'project'
                                        }
                                    }
                                }
                            }
                        }).then(function (res) {
                            self.projectKeywords = res.rows;
                            if (self.projectKeywords.length > 0) {
                                self.loadAll();
                                self.loadSunburstChart(res.rows[0]);
                            }
                        }, function () {});

                        first = false;
                    } else {
                        self.loadAll();
                        self.loadSunburstChart(self.group);
                    }
                }, function () {});
            };

            self.loadAll = function () {
                self.loadingOn();
                self.all = [];
                for (var i in self.projectKeywords) {
                    var group = self.projectKeywords[i];
                    self.all.push({
                        id             : group.id,
                        name           : group.name,
                        totalDone      : 0,
                        totalTarget    : 0,
                        projectKeywords: []
                    });
                    var index_group = self.all.length - 1;

                    var projectKeywords = self.datas.res.projectKeywords[group.id];
                    projectKeywords     = treeBuilderService.toArrayWithlevels(projectKeywords);

                    for (var j in projectKeywords) {
                        var projectKeyword = projectKeywords[j];
                        self.all[index_group].projectKeywords.push({
                            id    : projectKeyword.id,
                            name  : projectKeyword.name,
                            level : projectKeyword.level,
                            done  : projectKeyword.done,
                            target: projectKeyword.target
                        });
                        self.all[index_group].totalDone += projectKeyword.done;
                        self.all[index_group].totalTarget += projectKeyword.target;
                    }
                }
                for (var i in self.all) {
                    for(var j in self.all[i].projectKeywords){
                        self.all[i].projectKeywords[j].percentDone = self.all[i].projectKeywords[j].done / self.all[i].totalDone * 100 || 0;
                        self.all[i].projectKeywords[j].percentTarget = self.all[i].projectKeywords[j].target / self.all[i].totalTarget * 100 || 0;
                    }
                }
                self.loadingOff();
            };

            self.loadSunburstChart = function (group) {
                self.loadingOn();
                self.group         = group;
                self.sunburst.data = [{
                    name    : group.name,
                    group_id: group.id,
                    realSize: 0,
                    children: [],
                    i       : self.i
                }];

                self.i++;

                for (var i in self.datas.res.projectKeywords[group.id]) {
                    var keyword = self.datas.res.projectKeywords[group.id][i];

                    self.sunburst.data[0].children.push(loadSunburstData(keyword, [])[0]);
                    if (self.divideTimes) {
                        self.sunburst.data[0].realSize += keyword.done;
                    }
                    else if (keyword.done > 0) {
                        self.sunburst.data[0].realSize = keyword.done;
                    }
                }

                self.sunburst.data[0].children = removeKeywordSize0(self.sunburst.data[0].children);

                self.loadingOff();
            };

            function loadSunburstData(keyword, res) {
                res.push({
                    name    : keyword.name,
                    size    : keyword.done,
                    children: [],
                    realSize: keyword.done,
                    i       : self.i
                });

                self.i++;
                var last_id = res.length - 1;

                if (keyword.children.length > 0) {
                    for (var i in keyword.children) {
                        loadSunburstData(keyword.children[i], res[last_id].children);
                    }
                }

                return res;
            }

            function removeKeywordSize0(keywords) {
                var i = keywords.length;

                while (i--) {
                    if (keywords[i].realSize === 0) {
                        keywords.splice(keywords.indexOf(keywords[i]), 1);
                    } else if (keywords[i].children.length > 0) {
                        keywords[i].children = removeKeywordSize0(keywords[i].children);
                    }
                }

                return keywords;
            }

            self.groupFilter = function (item) {
                return item.id === self.sunburst.data[0].group_id;
            };

            self.export = function (type, parent, group, all) {
                $scope.$emit('export', {type: type, parent: parent, group: group, all: all, divideTimes: self.divideTimes});
            };

            self.loadingOn = function () {
                self.loading = true;
                $scope.$emit('loadingOn');
            };

            self.loadingOff = function () {
                self.loading = false;
                $scope.$emit('loadingOff');
            };
        }
    ]);

})(window.EVA.app);
