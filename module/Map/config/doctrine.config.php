<?php

namespace Map;

return [
    'doctrine' => [
        'driver' => [
            'map_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Map\Entity' => 'map_entities'
                ]
            ]
        ]
    ]
];
