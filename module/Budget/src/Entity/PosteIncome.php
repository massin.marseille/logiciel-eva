<?php

namespace Budget\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Project\Entity\Project;
use Laminas\Filter\Boolean;
use Laminas\Validator\Callback;

/**
 * Class PosteIncome
 *
 * @package Budget\Entity
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity(repositoryClass="Budget\Repository\PosteIncomeRepository")
 * @ORM\Table(name="budget_poste_income")
 */
class PosteIncome extends Poste
{
	const BALANCE_BALANCE = 'balance';
	const BALANCE_TO_BE_PERCEIVED = 'balanceToBePerceived';
	const BALANCE_NOTIFIED = 'balanceNotified';

	/**
	 * @var Envelope
	 *
	 * @ORM\ManyToOne(targetEntity="Budget\Entity\Envelope")
	 */
	protected $envelope;

	/**
	 * @var Project
	 *
	 * @ORM\ManyToOne(targetEntity="Project\Entity\Project", inversedBy="posteIncomes")
	 */
	protected $project;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Budget\Entity\Income", mappedBy="poste", cascade={"all"})
	 */
	protected $incomes;

	protected $incomesArbo;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean")
	 */
	protected $autofinancement = false;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $caduciteStart;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $caduciteEnd;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $caduciteSendProof;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $folderSendingDate;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $folderReceiptDate;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $deliberationDate;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	protected $arrDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $arrNumber;

	/**
	 * @var PosteIncome
	 *
	 * @ORM\ManyToOne(targetEntity="Budget\Entity\PosteIncome")
	 */
	protected $parent;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", options={"default" : 0})
	 */
	protected $subventionAmount = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", options={"default" : 0})
	 */
	protected $subventionAmountHT = 0;

	// For expense filter
	protected $filtered = false;

	/**
	 * @return boolean
	 */
	public function isFiltered()
	{
		return $this->filtered;
	}

	/**
	 * @param boolean $filtered
	 */
	public function setFiltered($filtered)
	{
		$this->filtered = $filtered;
	}

	/**
	 * @return PosteIncome
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param PosteIncome $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

	/**
	 * @return Envelope
	 */
	public function getEnvelope()
	{
		return $this->envelope;
	}

	/**
	 * @param Envelope $envelope
	 */
	public function setEnvelope($envelope)
	{
		$this->envelope = $envelope;
	}

	public function findParent()
	{
		if ($this->getProject()->getParent()) {
			foreach (
				$this->getProject()
					->getParent()
					->getPosteIncomes()
				as $poste
			) {
				if (
					$poste->getAccount() === $this->getAccount() &&
					$poste->getEnvelope() === $this->getEnvelope()
				) {
					return $poste;
				}
			}
		}

		return null;
	}

	public function getAmountArbo()
	{
		$amount = $this->getAmount();

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteIncomes() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountArbo();
					}
				}
			}
		}

		return $amount;
	}

	public function getAmountHTArbo()
	{
		$amount = $this->getAmountHT();

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteIncomes() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountHTArbo();
					}
				}
			}
		}

		return $amount;
	}

	public function getAmountAggreg()
	{
		$amount = 0;

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteIncomes() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountAggreg() + $poste->getAmount();
					}
				}
			}
		}

		return $amount;
	}

	public function getAmountHTAggreg()
	{
		$amount = 0;

		if ($this->getProject()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteIncomes() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						$amount += $poste->getAmountHTAggreg() + $poste->getAmountHT();
					}
				}
			}
		}

		return $amount;
	}

	public function setIncomesArbo($incomes)
	{
		$this->incomesArbo = $incomes;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getIncomesArbo()
	{
		// For filter
		if ($this->incomesArbo) {
			return $this->incomesArbo;
		}

		$incomes = $this->incomes ? clone $this->incomes : new ArrayCollection();
		if ($this->getProject() && !$this->isFiltered()) {
			foreach ($this->getProject()->getChildren() as $child) {
				foreach ($child->getPosteIncomes() as $poste) {
					if ($poste->getParent() && $poste->getParent() === $this) {
						foreach ($poste->getIncomesArbo() as $income) {
							// With recursive arbo, we can have doublon
							if (!$incomes->contains($income)) {
								$incomes->add($income);
							}
						}
					}
				}
			}
		}

		return $incomes;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getIncomes()
	{
		return $this->incomes;
	}

	/**
	 * @param ArrayCollection $incomes
	 */
	public function setIncomes($incomes)
	{
		$this->incomes = $incomes;
	}

	/**
	 * @param ArrayCollection $incomes
	 */
	public function addIncomes(ArrayCollection $incomes)
	{
		foreach ($incomes as $income) {
			$income->setPoste($this);
			$this->getIncomes()->add($income);
		}
	}

	/**
	 * @param ArrayCollection $incomes
	 */
	public function removeIncomes(ArrayCollection $incomes)
	{
		foreach ($incomes as $income) {
			$income->setPoste(null);
			$this->getIncomes()->removeElement($income);
		}
	}

	/**
	 * @return boolean
	 */
	public function isAutofinancement()
	{
		return $this->getAutofinancement();
	}

	/**
	 * @return boolean
	 */
	public function getAutofinancement()
	{
		return $this->autofinancement;
	}

	/**
	 * @param boolean $autofinancement
	 */
	public function setAutofinancement($autofinancement)
	{
		$this->autofinancement = $autofinancement;
	}

	/**
	 * @return \DateTime
	 */
	public function getCaduciteStart()
	{
		return $this->caduciteStart;
	}

	/**
	 * @param \DateTime $caduciteStart
	 */
	public function setCaduciteStart($caduciteStart)
	{
		$this->caduciteStart = $caduciteStart;
	}

	/**
	 * @return Project
	 */
	public function getProject()
	{
		return $this->project;
	}

	/**
	 * @param Project $project
	 */
	public function setProject($project)
	{
		$this->project = $project;
	}

	/**
	 * @return \DateTime
	 */
	public function getCaduciteEnd()
	{
		return $this->caduciteEnd;
	}

	/**
	 * @param \DateTime $caduciteEnd
	 */
	public function setCaduciteEnd($caduciteEnd)
	{
		$this->caduciteEnd = $caduciteEnd;
	}

	/**
	 * @return \DateTime
	 */
	public function getCaduciteSendProof()
	{
		return $this->caduciteSendProof;
	}

	/**
	 * @param \DateTime $caduciteSendProof
	 */
	public function setCaduciteSendProof($caduciteSendProof)
	{
		$this->caduciteSendProof = $caduciteSendProof;
	}

	/**
	 * @return \DateTime
	 */
	public function getFolderSendingDate()
	{
		return $this->folderSendingDate;
	}

	/**
	 * @param \DateTime $folderSendingDate
	 */
	public function setFolderSendingDate($folderSendingDate)
	{
		$this->folderSendingDate = $folderSendingDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getFolderReceiptDate()
	{
		return $this->folderReceiptDate;
	}

	/**
	 * @param \DateTime $folderReceiptDate
	 */
	public function setFolderReceiptDate($folderReceiptDate)
	{
		$this->folderReceiptDate = $folderReceiptDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getDeliberationDate()
	{
		return $this->deliberationDate;
	}

	/**
	 * @param \DateTime $deliberationDate
	 */
	public function setDeliberationDate($deliberationDate)
	{
		$this->deliberationDate = $deliberationDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getArrDate()
	{
		return $this->arrDate;
	}

	/**
	 * @param \DateTime $arrDate
	 */
	public function setArrDate($arrDate)
	{
		$this->arrDate = $arrDate;
	}

	/**
	 * @return string
	 */
	public function getArrNumber()
	{
		return $this->arrNumber;
	}

	/**
	 * @param string $arrNumber
	 */
	public function setArrNumber($arrNumber)
	{
		$this->arrNumber = $arrNumber;
	}

	/**
	 * Solde sollicité - perçu
	 * @return mixed
	 */
	public function getAmountBalance()
	{
		return $this->getAmountRequested() - $this->getAmountPerceived();
	}

	/**
	 * Solde sollicité - perçu
	 * @return mixed
	 */
	public function getAmountHTBalance()
	{
		return $this->getAmountHTRequested() - $this->getAmountHTPerceived();
	}

	/**
	 * Solde sollicité - perçu (arbo)
	 * @return mixed
	 */
	public function getAmountArboBalance()
	{
		return $this->getAmountArboRequested() - $this->getAmountArboPerceived();
	}

	/**
	 * Solde sollicité - perçu (arbo)
	 * @return mixed
	 */
	public function getAmountHTArboBalance()
	{
		return $this->getAmountHTArboRequested() - $this->getAmountHTArboPerceived();
	}

	/**
	 * Solde attribué - perçu
	 * @return mixed
	 */
	public function getAmountBalanceToBePerceived()
	{
		return $this->getAmountAssigned() - $this->getAmountPerceived();
	}

	/**
	 * Solde attribué - perçu
	 * @return mixed
	 */
	public function getAmountHTBalanceToBePerceived()
	{
		return $this->getAmountHTAssigned() - $this->getAmountHTPerceived();
	}

	/**
	 * Solde attribué - perçu (arbo)
	 * @return mixed
	 */
	public function getAmountArboBalanceToBePerceived()
	{
		return $this->getAmountArboAssigned() - $this->getAmountArboPerceived();
	}

	/**
	 * Solde attribué - perçu (arbo)
	 * @return mixed
	 */
	public function getAmountHTArboBalanceToBePerceived()
	{
		return $this->getAmountHTArboAssigned() - $this->getAmountHTArboPerceived();
	}

	/**
	 * Solde notifié - perçu
	 * @return mixed
	 */
	public function getAmountBalanceNotified()
	{
		return $this->getAmountNotified() - $this->getAmountPerceived();
	}

	/**
	 * Solde notifié - perçu
	 * @return mixed
	 */
	public function getAmountHTBalanceNotified()
	{
		return $this->getAmountHTNotified() - $this->getAmountHTPerceived();
	}

	/**
	 * Solde notifié - perçu (arbo)
	 * @return mixed
	 */
	public function getAmountArboBalanceNotified()
	{
		return $this->getAmountArboNotified() - $this->getAmountArboPerceived();
	}

	/**
	 * Solde notifié - perçu (arbo)
	 * @return mixed
	 */
	public function getAmountHTArboBalanceNotified()
	{
		return $this->getAmountHTArboNotified() - $this->getAmountHTArboPerceived();
	}

	protected function getAmountArboInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$incomes = $this->getIncomesArbo();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $income->getEvolution())	{
					return false;
				}

				if ($evolution === 'only_evolutions' && !$income->getEvolution())	{
					return false;
				}

				return $income->getType() === $type && $income->isYear($year);
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmount();
			}
		}

		return $amount;
	}

	protected function getPreviousYearsAmountArboInternal($type)
	{
		$amount = 0.0;
		$incomes = $this->getIncomesArbo();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type) {
				return $income->getType() === $type && $income->belongsToPreviousYears();
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmount();
			}
		}

		return $amount;
	}

	protected function getAmountHTArboInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$incomes = $this->getIncomesArbo();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $income->getEvolution())	{
					return false;
				}

				if ($evolution === 'only_evolutions' && !$income->getEvolution())	{
					return false;
				}

				return $income->getType() === $type && $income->isYear($year);
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmountHT();
			}
		}

		return $amount;
	}

	protected function getPreviousYearsAmountHTArboInternal($type)
	{
		$amount = 0.0;
		$incomes = $this->getIncomesArbo();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type) {
				return $income->getType() === $type && $income->belongsToPreviousYears();
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmountHT();
			}
		}

		return $amount;
	}

	protected function getAmountInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$incomes = $this->getIncomes();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $income->getEvolution())	{
					return false;
				}

				if ($evolution === 'only_evolutions' && !$income->getEvolution())	{
					return false;
				}

				return $income->getType() === $type && $income->isYear($year);
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmount();
			}
		}

		return $amount;
	}

	protected function getPreviousYearsAmountInternal($type)
	{
		$amount = 0.0;
		$incomes = $this->getIncomes();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type) {
				return $income->getType() === $type && $income->belongsToPreviousYears();
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmount();
			}
		}

		return $amount;
	}

	protected function getAmountHTInternal($type, $year = null, $evolution = null)
	{
		$amount = 0.0;
		$incomes = $this->getIncomes();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type, $year, $evolution) {
				if ($evolution === 'without_evolutions' && $income->getEvolution())	{
					return false;
				}

				if ($evolution === 'only_evolutions' && !$income->getEvolution())	{
					return false;
				}

				return $income->getType() === $type && $income->isYear($year);
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmountHT();
			}
		}

		return $amount;
	}

	protected function getPreviousAmountHTInternal($type)
	{
		$amount = 0.0;
		$incomes = $this->getIncomes();

		if ($incomes) {
			$filteredIncomes = $incomes->filter(function ($income) use ($type) {
				return $income->getType() === $type && $income->belongsToPreviousYears();
			});

			foreach ($filteredIncomes as $income) {
				$amount += $income->getAmountHT();
			}
		}

		return $amount;
	}

	/**
	 * @return float
	 */
	public function getSubventionAmount()
	{
		return $this->subventionAmount;
	}

	/**
	 * @param float $subventionAmount
	 */
	public function setSubventionAmount($subventionAmount)
	{
		$this->subventionAmount = isset($subventionAmount) ? $subventionAmount : 0;
	}

	/**
	 * @return float
	 */
	public function getSubventionAmountHT()
	{
		return $this->subventionAmountHT;
	}

	/**
	 * @param float $subventionAmountHT
	 */
	public function setSubventionAmountHT($subventionAmountHT)
	{
		$this->subventionAmountHT = isset($subventionAmountHT) ? $subventionAmountHT : 0;
	}

	public function __call($name, $arguments)
	{
		foreach (Income::getTypes() as $type) {
			if (
				$name === 'getAmount' . ucfirst($type) ||
				$name === 'getPreviousYearsAmount' . ucfirst($type) ||
				$name === 'getAmountHT' . ucfirst($type) ||
				$name === 'getPreviousYearsAmountHT' . ucfirst($type) ||
				$name === 'getAmountArbo' . ucfirst($type) ||
				$name === 'getPreviousYearsAmountArbo' . ucfirst($type) ||
				$name === 'getAmountHTArbo' . ucfirst($type) ||
				$name === 'getPreviousYearsAmountHTArbo' . ucfirst($type)
			) {
				$method = str_replace(ucfirst($type), '', $name) . 'Internal';
				array_unshift($arguments, $type);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

		return null;
	}

	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilter = parent::getInputFilter($entityManager);

		$inputFilter->add([
			'name' => 'envelope',
            'required' => false,
            'allow_empty' => true,
        ]);

		$inputFilter->add([
			'name' => 'autofinancement',
			'required' => false,
			'allow_empty' => true,
			'filters' => [['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]]],
		]);

		$inputFilter->add([
			'name' => 'caduciteStart',
			'required' => false,
			'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
		]);

		$inputFilter->add([
			'name' => 'caduciteEnd',
			'required' => false,
			'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
		]);

		$inputFilter->add([
			'name' => 'caduciteSendProof',
			'required' => false,
			'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
		]);

		$inputFilter->add([
			'name' => 'arrDate',
			'required' => false,
			'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
		]);

		$inputFilter->add([
			'name' => 'folderSendingDate',
			'required' => false,
			'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
		]);

		$inputFilter->add([
			'name' => 'folderReceiptDate',
			'required' => false,
			'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
		]);

		$inputFilter->add([
			'name' => 'deliberationDate',
			'required' => false,
			'filters' => [['name' => 'Core\Filter\DateTimeFilter']],
		]);

		$inputFilter->add([
			'name' => 'arrNumber',
			'required' => false,
			'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
		]);

		$inputFilter->add([
			'name' => 'parent',
			'required' => false,
		]);

		$inputFilter->add([
			'name' => 'subventionAmount',
			'required' => false,
			'filters' => [['name' => 'NumberParse']],
		]);

		$inputFilter->add([
			'name' => 'subventionAmountHT',
			'required' => false,
			'filters' => [['name' => 'NumberParse']],
		]);

		return $inputFilter;
	}

	/**
	 * @return array
	 */
	public static function getBalances()
	{
		return [self::BALANCE_BALANCE, self::BALANCE_NOTIFIED, self::BALANCE_TO_BE_PERCEIVED];
	}
}
