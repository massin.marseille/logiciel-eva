(function (app) {

    app.factory('treeBuilderService', [
        '$filter',
        function treeBuilderService($filter) {
            var self = this;

            self.insertInTree = function insertInTree(_item, tree, _level) {
                for (var i in tree) {
                    var item = tree[i];
                    if (item.id === _item.parent.id) {
                        item.children.push(_item);
                        _item.parentOld = _item.parent;
                        delete _item.parent;
                        _item._level  = _level;
                        var order     = typeof item.order == 'undefined' ? 'name' : ['order', 'name'];
                        item.children = $filter('orderBy')(item.children, order);
                        return true;
                    }
                    if (self.insertInTree(_item, item.children, _level + 1)) {
                        return true;
                    }
                }

                return false;
            };

            self.toArray = function toArray(tree, array) {
                for (var i in tree) {
                    var item = angular.copy(tree[i]);
                    array.push(item);
                    self.toArray(item.children, array);
                }

                return array;
            };

            self.toArrayWithLevels = function toArray(tree, array, level) {
                for (var i in tree) {
                    var item   = angular.copy(tree[i]);
                    item.level = level;
                    array.push(item);
                    self.toArrayWithLevels(item.children, array, level + 1);
                }

                return array;
            };

            return {
                toNNTree         : function toNNTree(flatArray) {
                    var tree    = [];
                    var indexed = {};
                    for (var i in flatArray) {
                        flatArray[i].children       = [];
                        indexed[flatArray[i]['id']] = flatArray[i];
                    }

                    var shouldBeRemoved = [];
                    for (var id in indexed) {
                        var item  = indexed[id];
                        var order = typeof item.order == 'undefined' ? 'name' : 'order';
                        if (typeof item.parents != 'undefined' && item.parents !== null && item.parents.length > 0) {
                            var control = true;

                            for (var i in item.parents) {
                                if (typeof indexed[item.parents[i].id] !== 'undefined') {
                                    indexed[item.parents[i].id].children.push(item);
                                    indexed[item.parents[i].id].children = $filter('orderBy')(indexed[item.parents[i].id].children, order);
                                } else {
                                    control = false;
                                }
                            }

                            if (control) {
                                shouldBeRemoved.push(id);
                            }
                        }
                    }

                    for (var id in indexed) {
                        if (shouldBeRemoved.indexOf(id) == -1) {
                            tree.push(indexed[id]);
                        }
                        var order = typeof indexed[id] == 'undefined' ? 'name' : ['order', 'name'];
                        tree      = $filter('orderBy')(tree, order);
                    }

                    return tree;
                },
                toTree           : function toTree(flatArray, order) {
                    for (var i in flatArray) {
                        if (flatArray[i].parent !== null && flatArray[i].parent.id !== null) {
                            var parentIsInArray = false;
                            for (var j in flatArray) {
                                if (flatArray[i].parent.id == flatArray[j].id) {
                                    parentIsInArray = true;
                                }
                            }
                            if (!parentIsInArray) {
                                flatArray[i].parentOld = flatArray[i].parent;
                                flatArray[i].parent = null;
                            }
                        }
                    }

                    var tree = [];
                    var n    = angular.copy(flatArray.length);

                    do {
                        var relaunch = false;
                        for (var i in flatArray) {
                            var item      = flatArray[i];
                            item.children = [];
                            item._level   = 0;
                            if (!order) {
                                order = typeof item.order === 'undefined' ? 'name' : ['order', 'name'];
                            }
                            if (item.parent === null) {
                                tree.push(item);
                                delete item.parent;
                                tree = $filter('orderBy')(tree, order);
                                flatArray.splice(flatArray.indexOf(item), 1);
                                n--;
                                relaunch = true;
                            } else {
                                if (self.insertInTree(item, tree, 1)) {
                                    flatArray.splice(flatArray.indexOf(item), 1);
                                    n--;
                                    relaunch = true;
                                }
                            }
                        }
                    } while (n > 0 && relaunch);

//                    // Les élements dont le parent n'est pas présent
//                    for (var i in flatArray) {
//                        var item      = flatArray[i];
//                        item.children = [];
//                        tree.push(item);
//                        delete item.parent;
//                        var order = typeof item.order == 'undefined' ? 'name' : 'order';
//                        tree      = $filter('orderBy')(tree, order);
//                        flatArray.splice(flatArray.indexOf(item), 1);
//                    }

                    // Permet de trier les éléments "racines" par ordre alphabétique
                    /*tree.sort(function (a, b) {
                     var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase();
                     if (nameA < nameB)
                     return -1;
                     if (nameA > nameB)
                     return 1;
                     return 0;
                     });*/

                    return tree;
                },
                toArray          : function (tree) {
                    var array = [];
                    self.toArray(tree, array);
                    return array;
                },
                toArrayWithlevels: function (tree) {
                    var array = [];
                    self.toArrayWithLevels(tree, array, 0);
                    return array;
                }
            };
        }
    ])

})(window.EVA.app);
