<?php

use Bootstrap\Entity\Client;
use Core\Controller\Plugin\ApiRequestPlugin;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use User\Entity\User;

class Export
{
    const ABSENCE_WEEKEND  = 'WE';
    const ABSENCE_HOLIDAYS = 'F';

    const STYLE_TABLE        = [
        'borders' => [
            'outline' => [
                'borderStyle' => Style\Border::BORDER_MEDIUM,
                'color'       => ['rgb' => '000000'],
            ],
            'inside'  => [
                'borderStyle' => Style\Border::BORDER_THIN,
                'color'       => ['rgb' => '000000'],
            ],
        ],
    ];
    const STYLE_SIGNATURE    = [
        'borders' => [
            'outline' => [
                'borderStyle' => Style\Border::BORDER_MEDIUM,
                'color'       => ['rgb' => '000000'],
            ],
        ],
    ];
    const STYLE_ALIGN_LEFT   = [
        'alignment' => [
            'horizontal' => Style\Alignment::HORIZONTAL_LEFT,
        ],
    ];
    const STYLE_ALIGN_CENTER = [
        'alignment' => [
            'horizontal' => Style\Alignment::HORIZONTAL_CENTER,
            'vertical'   => Style\Alignment::VERTICAL_CENTER,
        ],
    ];
    const STYLE_ALIGN_RIGHT  = [
        'alignment' => [
            'horizontal' => Style\Alignment::HORIZONTAL_RIGHT,
        ],
    ];
    const STYLE_BOLD         = [
        'font' => [
            'bold' => true,
        ],
    ];

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translate
     */
    protected $translator;

    /**
     * @var ApiRequestPlugin
     */
    protected $apiRequest;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $fseName;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var string
     */
    protected $contractType;

    /**
     * @var string
     */
    protected $function;

    /**
     * @var float
     */
    protected $base;

    /**
     * @var DateTime
     */
    protected $start;

    /**
     * @var DateTime
     */
    protected $end;

    /**
     * @var array
     */
    protected $projectIds;

    /**
     * @var DateTime
     */
    protected $currentStart;

    /**
     * @var DateTime
     */
    protected $currentEnd;

    /**
     * @var int
     */
    protected $currentNumberOfDays;

    /**
     * Export constructor.
     *
     * @param EntityManager    $em
     * @param Translate        $translator
     * @param ApiRequestPlugin $apiRequest
     * @param Client           $client
     */
    public function __construct(EntityManager $em, Translate $translator, ApiRequestPlugin $apiRequest, Client $client)
    {
        $this->em         = $em;
        $this->translator = $translator;
        $this->apiRequest = $apiRequest;
        $this->client     = $client;
    }

    public function doExport($params, $data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);
        setlocale(LC_TIME, 'fr_FR', 'French_France', 'French');

        $this->fseName      = $params['fseName'];
        $this->user         = $this->em->getRepository('User\Entity\User')->find(isset($params['user']['id']) ? $params['user']['id'] : $params['user']);
        $this->contractType = $params['contractType'];
        $this->function     = $params['function'];
        $this->base         = $params['base'];
        $this->start        = DateTime::createFromFormat('d/m/Y', $params['start'])->setTime(0, 0);
        $this->end          = DateTime::createFromFormat('d/m/Y', $params['end'])->setTime(0, 0);

        $sheets = [];

        $startYear = $this->start->format('Y');
        $endYear   = $this->end->format('Y');

        $yearReviewsData = [];
        for ($currentYear = $startYear; $currentYear <= $endYear; $currentYear++) {
            $startMonth = '1';
            $endMonth   = '12';
            if ($currentYear == $startYear) {
                $startMonth = $this->start->format('n');
            }
            if ($currentYear == $endYear) {
                $endMonth = $this->end->format('n');
            }

            $yearReviewsData[$currentYear] = [];
            for ($currentMonth = $startMonth; $currentMonth <= $endMonth; $currentMonth++) {
                $currentMonthYear = DateTime::createFromFormat(
                    'j/n/Y',
                    1 . '/' . $currentMonth . '/' . $currentYear
                )->setTime(0, 0);

                $this->currentStart = clone $currentMonthYear;
                if ($currentMonth == $startMonth && $currentYear == $startYear) {
                    $this->currentStart = $this->start;
                }
                if ($currentMonth == $endMonth && $currentYear == $endYear) {
                    $this->currentEnd = $this->end;
                } else {
                    $daysToAdd        = $currentMonthYear->format('t') - 1;
                    $this->currentEnd = $currentMonthYear->add(DateInterval::createFromDateString($daysToAdd . ' days'));
                }

                $this->projectIds          = [];
                $this->currentNumberOfDays = $this->currentStart->diff($this->currentEnd)->days + 1;

                $lines = [['Date', 'Objet', 'Durée (heures)', 'Livrable disponible, le cas échéant']];

                $this->getLines($lines, $data);

                $month     = utf8_encode(ucfirst(strftime('%B', $currentMonthYear->getTimestamp())));
                $year      = $currentYear;
                $sheetName = $month . ' ' . $year;

                $header = $this->getHeader($year, $month);
                $footer = $this->getFooter();

                $sheets[$sheetName] = [$header, $lines, $footer];

                $yearReviewsData[$currentYear][$currentMonth] = [];
                foreach ($lines as $key => $line) {
                    // Remove the string Total from the array
                    if ($key !== 0) {
                        $yearReviewsData[$currentYear][$currentMonth][] = $month . ' ' . $year;
                    }
                }
            }
        }

        $this->getYearReviews($sheets, $yearReviewsData);

        $this->download($sheets);
    }

    protected function getLines(&$lines, $timesheets)
    {
        foreach ($timesheets as $timesheet) {
            $start = DateTime::createFromFormat('d/m/Y H:i', $timesheet['start'])->setTime(0, 0);
            if ($this->currentStart <= $start && $start <= $this->currentEnd) {
                $line   = [];
                $line[] = $timesheet['start'];
                $line[] = $timesheet['name'];
                $line[] = $timesheet['hours'];
                $line[] = $timesheet['description'] ? $timesheet['description'] : '';

                $lines[] = $line;
            }
        }
    }

    protected function getHeader($year, $month = null)
    {
        $header = [
            [''],
            [''],
            [''],
            ['Porteur de projet'],
            [''],
            [''],
            [''],
            [$this->fseName],
            ['Fiche d\'Affectation Charges de Personnel'],
        ];

        if ($month) {
            $header[] = [$this->user->getLastName() . ' ' . $this->user->getFirstName() . ' ' . $this->contractType . ' - ' . $this->function];
            $header[] = [$month . ' ' . $year];
        }

        return $header;
    }

    protected function getFooter()
    {
        $footer   = [];
        $footer[] = ['Signature du salarié', 'Signature du responsable légal'];

        return $footer;
    }

    protected function getYearReviews(&$sheets, $yearReviewsData)
    {
        $yearReviews = [];
        foreach ($yearReviewsData as $year => $months) {
            $yearReviews['Bilan ' . $year]   = [];
            $yearReviews['Bilan ' . $year][] = $this->getHeader($year);
            $yearReviews['Bilan ' . $year][] = [['Récapitulatif heures travaillées sur l\'année', '', 'Heures']];

            foreach ($months as $month => $totals) {
                $month = DateTime::createFromFormat(
                    'j/n/Y',
                    1 . '/' . $month . '/' . $year
                )->setTime(0, 0);
                $month = utf8_encode(ucfirst(strftime('%B', $month->getTimestamp())));

                $yearReviews['Bilan ' . $year][1][] = [$month . ' ' . $year, '', ''];
            }

            $yearReviews['Bilan ' . $year][1][] = [
                $this->user->getLastName() . ' ' . $this->user->getFirstName(),
                $this->contractType . ' - ' . $this->function,
                '',
            ];

            if ($year == $this->start->format('Y')) {
                $month = DateTime::createFromFormat(
                    'j/n/Y',
                    1 . '/' . $this->start->format('n') . '/' . $year
                )->setTime(0, 0);
                $month = lcfirst(utf8_encode(ucfirst(strftime('%B', $month->getTimestamp()))));
                $start = $this->start->format('j') . ' ' . $month . ' ' . $year;
            } else {
                $start = '1 janvier ' . $year;
            }

            if ($year == $this->end->format('Y')) {
                $month = DateTime::createFromFormat(
                    'j/n/Y',
                    1 . '/' . $this->end->format('n') . '/' . $year
                )->setTime(0, 0);
                $month = lcfirst(utf8_encode(ucfirst(strftime('%B', $month->getTimestamp()))));
                $end   = $this->end->format('j') . ' ' . $month . ' ' . $year;
            } else {
                $end = '31 décembre ' . $year;
            }

            $yearReviews['Bilan ' . $year][1][] = [
                'Du ' . $start,
                'au ' . $end .
                '',
            ];

            $yearReviews['Bilan ' . $year][1][] = [
                'Base ' . $this->base . ' heures - ' . count($months) . ' mois',
                '',
                '',
            ];

            $yearReviews['Bilan ' . $year][1][] = [
                '',
                (count($months) / 12) * $this->base,
                '',
            ];

            $yearReviews['Bilan ' . $year][1][] = [
                'Total heures travaillées sur l\'année et sur l\'opération',
                '',
                '',
            ];

            $yearReviews['Bilan ' . $year][] = $this->getFooter();
        }

        $sheets = array_merge($sheets, $yearReviews);
    }

    protected function download($array)
    {
        try {
            foreach ($array as $i => $tables) {
                foreach ($tables as $j => $lines) {
                    foreach ($lines as $k => $row) {
                        foreach ($row as $l => $value) {
                            if (is_numeric($value) && $value == 0) {
                                $array[$i][$j][$k][$l] = '0';
                            }
                        }
                    }
                }
            }

            $totalLinesColBySheet = [];
            $reviewSheetNames     = [];

            $spreadsheet = new Spreadsheet();

            foreach ($array as $worksheetName => $tables) {
                $worksheet = $spreadsheet->createSheet();
                $worksheet->setTitle($worksheetName);

                $isReview = preg_match('/^Bilan/', $worksheetName);
                if ($isReview) {
                    $reviewSheetNames[] = $worksheetName;
                }

                foreach ($tables as $key => $lines) {
                    switch ($key) {
                        // Header
                        case 0:
                            $worksheet->fromArray($lines, null, 'A1');

                            $worksheet
                                ->getStyle('A4')
                                ->applyFromArray(self::STYLE_BOLD);

                            $logo1 = new Drawing();
                            $logo1->setName('CCI PACA');
                            $logo1->setPath(__DIR__ . '/img/cci-paca.png');
                            $logo1->setWidthAndHeight(150, 150);
                            $logo1->setCoordinates('A1');
                            $logo1->setWorksheet($worksheet);

                            $logo2 = new Drawing();
                            $logo2->setName('CCI Pays d\'Arles');
                            $logo2->setPath(__DIR__ . '/img/cci-pays-arles.png');
                            $logo2->setWidthAndHeight(150, 150);
                            $logo2->setCoordinates('A5');
                            $logo2->setWorksheet($worksheet);

                            $logo3 = new Drawing();
                            $logo3->setName('L\'Europe s\'engage en PACA avec le FSE');
                            $logo3->setPath(__DIR__ . '/img/paca-fse.png');
                            $logo3->setWidthAndHeight(120, 120);
                            $logo3->setCoordinates('B1');
                            $logo3->setWorksheet($worksheet);

                            $logo4 = new Drawing();
                            $logo4->setName('Union Européenne');
                            $logo4->setPath(__DIR__ . '/img/ue.jpg');
                            $logo4->setWidthAndHeight(240, 240);
                            $logo4->setOffsetX(120);
                            $logo4->setCoordinates('B1');
                            $logo4->setWorksheet($worksheet);
                            break;
                        // Body
                        case 1:
                            // Link reviews sheets to month values
                            if ($isReview) {
                                for ($i = 1; $i < count($lines) - 5; $i++) {
                                    $lines[$i][2] = '=\'' . $lines[$i][0] . '\'!' . $totalLinesColBySheet[$lines[$i][0]];
                                }
                                $lines[count($lines) - 1][2] = '=SUM(C5:C' . (count($lines) - 6 + 11) . ')';
                                $lines[count($lines) - 2][2] = '=C' . (count($lines) - 1 + 11) . '/B' . (count($lines) - 2 + 11);
                            }

                            $rowToBegin = (int)$worksheet->getHighestDataRow() + 2;
                            $worksheet->fromArray($lines, null, 'A' . $rowToBegin);

                            // Adding Total Line
                            if (!$isReview) {
                                $total         = [['Total (en heures)', '']];
                                $rowToBeginSum = $rowToBegin + 1;
                                $rowToEndSum   = (int)$worksheet->getHighestDataRow();
                                if ($rowToEndSum > $rowToBeginSum) {
                                    $total[0][] = '=SUM(C' . $rowToBeginSum . ':C' . $rowToEndSum . ')';
                                } else {
                                    $total[0][] = '0';
                                }

                                $worksheet->fromArray($total, null, 'A' . ((int)$worksheet->getHighestDataRow() + 1));
                                $totalLinesColBySheet[$worksheetName] = 'C' . (int)$worksheet->getHighestDataRow();
                            }

                            if ($isReview) {
                                $worksheet->mergeCells('A8:C8');
                                $worksheet->mergeCells('A9:C9');
                                $worksheet
                                    ->getStyle('A8:C9')
                                    ->applyFromArray(array_merge(
                                        self::STYLE_ALIGN_CENTER,
                                        self::STYLE_BOLD
                                    ));

                                $worksheet->mergeCells('A11:B11');
                                $worksheet
                                    ->getStyle('A11:C11')
                                    ->applyFromArray(self::STYLE_BOLD);

                                for ($i = 12; $i <= (int)$worksheet->getHighestDataRow() - 5; $i++) {
                                    $worksheet->mergeCells('A' . $i . ':B' . $i);
                                    $worksheet
                                        ->getStyle('A' . $i . ':B' . $i)
                                        ->applyFromArray(self::STYLE_BOLD);
                                    $worksheet
                                        ->getStyle('C' . $i)
                                        ->getNumberFormat()
                                        ->setFormatCode('0.00');
                                }

                                $rowBase = (int)$worksheet->getHighestDataRow() - 2;
                                $worksheet->mergeCells('A' . $rowBase . ':B' . $rowBase);

                                $worksheet
                                    ->getStyle('A11:C' . $worksheet->getHighestDataRow())
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_ALIGN_CENTER
                                    ));

                                $rowPercentage = (int)$worksheet->getHighestDataRow() - 1;
                                $worksheet
                                    ->getStyle('B' . $rowPercentage)
                                    ->getNumberFormat()
                                    ->setFormatCode('0.00');
                                $worksheet
                                    ->getStyle('C' . $rowPercentage)
                                    ->getNumberFormat()
                                    ->setFormatCode('0.00%');

                                $rowTotal = (int)$worksheet->getHighestDataRow();
                                $worksheet->mergeCells('A' . $rowTotal . ':B' . $rowTotal);
                                $worksheet
                                    ->getStyle('A' . $rowTotal . ':B' . $rowTotal)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_ALIGN_RIGHT,
                                        self::STYLE_BOLD
                                    ));
                                $worksheet
                                    ->getStyle('C' . $rowTotal)
                                    ->applyFromArray(self::STYLE_BOLD)
                                    ->getNumberFormat()
                                    ->setFormatCode('0.00');
                            } else {
                                $worksheet->mergeCells('A8:D8');
                                $worksheet->mergeCells('A9:D9');
                                $worksheet->mergeCells('A10:D10');
                                $worksheet->mergeCells('A11:D11');
                                $worksheet
                                    ->getStyle('A8:D11')
                                    ->applyFromArray(array_merge(
                                        self::STYLE_ALIGN_CENTER,
                                        self::STYLE_BOLD
                                    ));

                                $worksheet
                                    ->getStyle('A13:D13')
                                    ->applyFromArray(self::STYLE_BOLD);

                                $worksheet
                                    ->getStyle('A13:D' . ((int)$worksheet->getHighestDataRow() - 1))
                                    ->applyFromArray(array_merge(
                                        self::STYLE_TABLE,
                                        self::STYLE_ALIGN_CENTER
                                    ));

                                for ($i = 14; $i <= (int)$worksheet->getHighestDataRow(); $i++) {
                                    $worksheet
                                        ->getStyle('C' . $i)
                                        ->getNumberFormat()
                                        ->setFormatCode('0.00');
                                }

                                $totalRow = (int)$worksheet->getHighestDataRow();
                                $worksheet->mergeCells('A' . $totalRow . ':' . 'B' . $totalRow);
                                $worksheet
                                    ->getStyle('A' . $totalRow . ':' . 'B' . $totalRow)
                                    ->applyFromArray(array_merge(
                                        self::STYLE_BOLD,
                                        self::STYLE_ALIGN_RIGHT
                                    ));

                                $worksheet
                                    ->getStyle('C' . $totalRow)
                                    ->applyFromArray(self::STYLE_BOLD);

                                $worksheet
                                    ->getStyle('A' . $totalRow . ':' . 'C' . $totalRow)
                                    ->applyFromArray(self::STYLE_TABLE);
                            }

                            break;
                        // Footer
                        case 2:
                            $rowToBegin = (int)$worksheet->getHighestDataRow() + 2;
                            $worksheet->fromArray($lines, null, 'A' . $rowToBegin);

                            $worksheet
                                ->getStyle('A' . $rowToBegin . ':B' . $rowToBegin)
                                ->applyFromArray(array_merge(
                                    self::STYLE_BOLD,
                                    self::STYLE_ALIGN_CENTER
                                ));

                            $worksheet->mergeCells('A' . ((int)$rowToBegin + 1) . ':A' . ((int)$rowToBegin + 4));
                            $worksheet->mergeCells('B' . ((int)$rowToBegin + 1) . ':B' . ((int)$rowToBegin + 4));

                            $worksheet
                                ->getStyle('A' . $rowToBegin . ':B' . ((int)$rowToBegin + 4))
                                ->applyFromArray(self::STYLE_TABLE);

                            break;
                    }
                }

                $worksheet
                    ->getColumnDimension('A')
                    ->setAutoSize(true);
                $worksheet
                    ->getColumnDimension('B')
                    ->setAutoSize(true);

                $worksheet
                    ->getColumnDimension('C')
                    ->setAutoSize(true);
                $worksheet
                    ->getColumnDimension('D')
                    ->setAutoSize(true);
            }

            foreach (array_reverse($reviewSheetNames) as $reviewSheetName) {
                $sheet = $spreadsheet->getSheetByName($reviewSheetName);
                $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($sheet));
                $spreadsheet->addSheet($sheet, 0);
            }

            $spreadsheet->removeSheetByIndex($spreadsheet->getIndex($spreadsheet->getSheetByName('Worksheet')));
            $spreadsheet->setActiveSheetIndex(0);

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
            header('Content-Disposition: attachment; filename="export.xlsx"');
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            ob_end_clean();
            $writer->save('php://output');
            exit;
        } catch (Exception $exception) {
            dump($exception);
        }
    }
}