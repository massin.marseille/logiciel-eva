<?php

namespace Convention\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class LineController extends BasicRestController
{
    protected $entityClass = 'Convention\Entity\Line';
    protected $repository  = 'Convention\Entity\Line';
    protected $entityChain = 'convention:e:convention';

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort = $data['sort'] ? $data['sort'] : 'object.id';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->join('object.convention', 'convention');
        $queryBuilder->join('object.project', 'project');

        $queryBuilder->addOrderBy($sort, $order);
    }
}
