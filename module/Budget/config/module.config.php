<?php

use Budget\Entity\Expense;
use Budget\Entity\Income;

return [
	'module' => [
		'budget' => [
			'environments' => ['client'],

            'active'   => true,
            'required' => false,

            'acl' => [
                'entities' => [
                    [
                        'name'  => 'account',
                        'class' => 'Budget\Entity\Account',
                        'owner' => [],
                    ],
                    [
                        'name' => 'budgetCode',
                        'class' => 'Budget\Entity\BudgetCode',
                        'control' => [
                            'code' => true
                        ]
                    ],
                    [
                        'name'     => 'envelope',
                        'class'    => 'Budget\Entity\Envelope',
                        'owner' => [],
                        'keywords' => true,
                    ],
                    [
                        'name'     => 'nature',
                        'class'    => 'Budget\Entity\Nature',
                        'owner' => [],
                        'keywords' => true,
                        'control' => [
                            'gbcp' => true
                        ]
                    ],
                    [
                        'name'     => 'organization',
                        'class'    => 'Budget\Entity\Organization',
                        'owner' => [],
                        'keywords' => true,
                        'control' => [
                            'gbcp' => true
                        ]
                    ],
                    [
                        'name'     => 'managementUnit',
                        'class'    => 'Budget\Entity\ManagementUnit',
                        'owner' => []
                    ],
                    [
                        'name'     => 'destination',
                        'class'    => 'Budget\Entity\Destination',
                        'owner' => [],
                        'keywords' => true,
                        'control' => [
                            'gbcp' => true
                        ]
                    ],
                    [
                        'name'     => 'posteExpense',
                        'class'    => 'Budget\Entity\PosteExpense',
                        'owner' => [],
                        'keywords' => true,
                    ],
                    [
                        'name'      => 'posteExpenseSimplified',
                        'class'     => '',
                        'owner'     => [],
                    ],
                    [
                        'name'      => 'posteExpenseReferential',
                        'class'     => 'Budget\Entity\PosteExpenseRef',
                        'owner'     => [],
                    ],
                    [
                        'name'     => 'posteIncome',
                        'class'    => 'Budget\Entity\PosteIncome',
                        'owner' => [],
                        'keywords' => true,
                    ],
                    [
                        'name'       => 'expense',
                        'class'      => 'Budget\Entity\Expense',
                        'owner' => [],
                        'keywords'   => true,
                        'conditions' => [
                            [
                                'property' => 'type',
                                'type'     => 'multiple',
                                'options'  => array_map(function ($type) {
                                    return [
                                        'value' => $type,
                                        'text'  => 'expense_type_' . $type,
                                    ];
                                }, Expense::getTypes()),
                            ],
                        ],
                    ],
                    [
                        'name'       => 'income',
                        'class'      => 'Budget\Entity\Income',
                        'owner' => [],
                        'keywords'   => true,
                        'conditions' => [
                            [
                                'property' => 'type',
                                'options'  => array_map(function ($type) {
                                    return [
                                        'value' => $type,
                                        'text'  => 'income_type_' . $type,
                                    ];
                                }, Income::getTypes()),
                            ],
                        ],
                    ],
                    [
                        'name'  => 'budgetStatus',
                        'class' => 'Budget\Entity\BudgetStatus',
                        'owner' => [],
                    ],
                ],
            ],

            'configuration' => [
                'ht'           => [
                    'type' => 'boolean',
                    'text' => 'budget_ht_configuration',
                ],
                'gbcp'         => [
                    'type' => 'boolean',
                    'text' => 'budget_gbcp_configuration',
                ],
                'code' => [
                    'type' => 'boolean',
                    'text' => 'budget_code_configuration',
                ],
                'evolution' => [
                    'type' => 'boolean',
                    'text' => 'budget_evolution_configuration',
                ],
                'expenseTypes' => [
                    'type'    => 'multiple',
                    'text'    => 'budget_expenseTypes_configuration',
                    'options' => array_map(function ($type) {
                        return [
                            'value' => $type,
                            'text'  => 'expense_type_' . $type,
                        ];
                    }, Expense::getTypes()),
                ],
                'simplifiedInputExpense' => [
                    'type'  => 'unique',
                    'text'  => 'budget_simplified_input_expense_configuration',
                    'options'   => array_map(function($type) {
                        return [
                            'value' => $type,
                            'text'  => 'expense_type_' . $type,
                        ];
                    }, Expense::getTypes())
                ],
                'incomeTypes'  => [
                    'type'    => 'multiple',
                    'text'    => 'budget_incomeTypes_configuration',
                    'options' => array_map(function ($type) {
                        return [
                            'value' => $type,
                            'text'  => 'income_type_' . $type,
                        ];
                    }, Income::getTypes()),
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator'   => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
