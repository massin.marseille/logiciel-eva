<?php

return [
    'icons' => [
        'field' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-paragraph'
        ]
    ]
];
