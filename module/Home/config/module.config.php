<?php

return [
    'module'       => [
        'home' => [
            'environments' => [
                'client',
            ],
            'active'       => true,
            'required'     => false,
            'acl'          => [
                'entities' => [
                    [
                        'name'     => 'home',
                        'class'    => 'Home\Entity\Home',
                        'keywords' => false,
                         'owner'   => []
                    ],
                    [
                        'name'     => 'homeAdmin',
                        'class'    => 'Home\Entity\HomeAdmin',
                        'keywords' => false,
                         'owner'   => []
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator'   => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
