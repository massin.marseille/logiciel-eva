<?php

namespace Budget\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Expense
 *
 * @package Budget\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="budget_expense")
 */
class Expense extends Transaction
{
    // Please use camel case notation for values
    const TYPE_PLANNED        = 'planned';
    const TYPE_COMMITTED      = 'committed';
    const TYPE_PAID           = 'paid';
    const TYPE_RELEASED       = 'released';
    const TYPE_COMMITTED_PAID = 'committedPaid';

    // Please use camel case notation for values
    const TYPE_AE_ESTIMATED  = 'aeEstimated';
    const TYPE_AE            = 'ae';
    const TYPE_AE_COMMITTED  = 'aeCommitted';
    const TYPE_CP_ESTIMATED  = 'cpEstimated';
    const TYPE_CP            = 'cp';
    const TYPE_CP_COMMITTED  = 'cpCommitted';
    const TYPE_DV            = 'dv';
    const TYPE_PORTFOLIO     = 'portfolio';

    /**
     * @var PosteExpense
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\PosteExpense", inversedBy="expenses")
     */
    protected $poste;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $dateFacture;

    /**
     * @return \DateTime
     */
    public function getDateFacture()
    {
        return $this->dateFacture;
    }

    /**
     * @param \DateTime $dateFacture
     */
    public function setDateFacture($dateFacture)
    {
        $this->dateFacture = $dateFacture;
    }

    public function getAccount()
    {
        return $this->getPoste()->getAccount();
    }

    public function getNature()
    {
        return $this->getPoste()->getNature();
    }

    public function getOrganization()
    {
        return $this->getPoste()->getOrganization();
    }

    public function getDestination()
    {
        return $this->getPoste()->getDestination();
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name'     => 'type',
            'required' => true,
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => self::getTypes()
                    ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'dateFacture',
            'required' => false,
            'filters' => [
                ['name' => 'Core\Filter\DateTimeFilter'],
            ]
        ]);

        $inputFilter->add([
            'name'     => 'amountHT',
            'required' => false,
            'filters' => [
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function ($value) {
                            // Si la valeur de amountHT est null, la convertir en 0
                            if ($value === null) {
                                return 0;
                            }
                            // Sinon, retourner la valeur originale
                            return $value;
                        },
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PLANNED,
            self::TYPE_COMMITTED,
            self::TYPE_PAID,
            self::TYPE_RELEASED,
            self::TYPE_COMMITTED_PAID,
            self::TYPE_AE_ESTIMATED,
            self::TYPE_AE,
            self::TYPE_AE_COMMITTED,
            self::TYPE_CP_ESTIMATED,
            self::TYPE_CP,
            self::TYPE_CP_COMMITTED,
            self::TYPE_DV,
            self::TYPE_PORTFOLIO
        ];
    }
}
