<?php

namespace Home\Controller\API;

use Core\Controller\BasicRestController;

class HomeAdminController extends BasicRestController
{
    protected $entityClass = 'Home\Entity\HomeAdmin';
    protected $repository  = 'Home\Entity\HomeAdmin';
    protected $entityChain = 'home:e:homeAdmin';
}
