<?php

namespace Budget;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
	'router' => [
		'client-routes' => [
			'budget' => [
				'type' => 'Literal',
				'options' => [
					'route' => '/budget',
				],
				'may_terminate' => false,
				'child_routes' => [
					'account' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/account',
							'defaults' => [
								'controller' => 'Budget\Controller\Account',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'form' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/form[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\Account',
										'action' => 'form',
									],
								],
							],
						],
					],
					'configuration' => [
						'type'          => 'Literal',
						'options'       => [
							'route'    => '/configuration',
							'defaults' => [
								'controller' => 'Budget\Controller\Configuration',
								'action'     => 'index',
							]
						]
					],
					'envelope' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/envelope',
							'defaults' => [
								'controller' => 'Budget\Controller\Envelope',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'form' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/form[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\Envelope',
										'action' => 'form',
									],
								],
							],
						],
					],
					'nature' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/nature',
							'defaults' => [
								'controller' => 'Budget\Controller\Nature',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'form' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/form[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\Nature',
										'action' => 'form',
									],
								],
							],
						],
					],
					'organization' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/organization',
							'defaults' => [
								'controller' => 'Budget\Controller\Organization',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'form' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/form[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\Organization',
										'action' => 'form',
									],
								],
							],
						],
					],
					'management-unit' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/management-unit',
							'defaults' => [
								'controller' => 'Budget\Controller\ManagementUnit',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'form' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/form[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\ManagementUnit',
										'action' => 'form',
									],
								],
							],
						],
					],
					'destination' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/destination',
							'defaults' => [
								'controller' => 'Budget\Controller\Destination',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'form' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/form[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\Destination',
										'action' => 'form',
									],
								],
							],
						],
					],
					'budget-status' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/budget-status',
							'defaults' => [
								'controller' => 'Budget\Controller\BudgetStatus',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'form' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/form[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\BudgetStatus',
										'action' => 'form',
									],
								],
							],
						],
					],
					'import' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/import',
							'defaults' => [
								'controller' => 'Budget\Controller\Import',
								'action' => 'index',
							],
						],
						'may_terminate' => true,
						'child_routes' => [
							'parse' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/:entity/parse',
									'defaults' => [
										'controller' => 'Budget\Controller\Import',
										'action' => 'parse',
									],
								],
							],
							'validate' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/:entity/validate',
									'defaults' => [
										'controller' => 'Budget\Controller\Import',
										'action' => 'validate',
									],
								],
							],
							'save' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/:entity/save',
									'defaults' => [
										'controller' => 'Budget\Controller\Import',
										'action' => 'save',
									],
								],
							],
						],
					],
				],
			],
			'api' => [
				'type' => 'Literal',
				'options' => [
					'route' => '/api',
				],
				'may_terminate' => false,
				'child_routes' => [
					'budget' => [
						'type' => 'Literal',
						'options' => [
							'route' => '/budget',
						],
						'may_terminate' => false,
						'child_routes' => [
							'configuration' => [
								'type' => 'Literal',
								'options' => [
									'route' => '/configuration'
								],
								'may_terminate' => false,
								'child_routes' => [
									'categories' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/categories',
											'defaults' => [
												'controller' => 'Budget\Controller\API\Configuration',
												'action' => 'categories'
											]
										]
									],
									'accounts' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/accounts',
											'defaults' => [
												'controller' => 'Budget\Controller\API\Configuration',
												'action' => 'accounts'
											]
										]
									],
									'postes-expense' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/postes-expense',
											'defaults' => [
												'controller' => 'Budget\Controller\API\Configuration',
												'action' => 'readExpenses'
											]
										],
										'may_terminate' => true,
										'child_routes' => [
											'save' => [
												'type' => 'Segment',
												'options' => [
													'route' => '/save',
													'defaults' => [
														'controller' => 'Budget\Controller\API\Configuration',
														'action' => 'saveExpense'
													]
												]
											],
											'delete' => [
												'type' => 'Segment',
												'options' => [
													'route' => '/delete[/:id]',
													'defaults' => [
														'controller' => 'Budget\Controller\API\Configuration',
														'action' => 'deleteExpense'
													]
												]
											]
										]
									]
								]
							],
							'simplified-input' => [
								'type' => 'Literal',
								'options' => [
									'route' => '/simplified-input'
								],
								'may_terminate' => false,
								'child_routes' => [
									'referential' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/referential[/:projectId]',
											'defaults' => [
												'controller' => 'Budget\Controller\API\SimplifiedInput',
												'action' => 'referential'
											]
										]
									],
									'table' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/table[/:projectId]',
											'defaults' => [
												'controller' => 'Budget\Controller\API\SimplifiedInput',
												'action' => 'table'
											]
										]
									],
									'generate' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/generate[/:projectId]',
											'defaults' => [
												'controller' => 'Budget\Controller\API\SimplifiedInput',
												'action' => 'generate'
											]
										]
									],
									'import' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/import[/:projectId]',
											'defaults' => [
												'controller' => 'Budget\Controller\API\SimplifiedInput',
												'action' => 'import'
											]
										]
									]
								]
							],
							'account' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/account[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Account',
									],
								],
								'may_terminate' => true,
								'child_routes' => [
									'form' => [
										'type' => 'Segment',
										'options' => [
											'route' => '/fusion',
											'defaults' => [
												'controller' => 'Budget\Controller\API\Account',
												'action' => 'fusion',
											],
										],
									],
								],
							],
							'code' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/code[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\BudgetCode',
									],
								],
							],
							'envelope' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/envelope[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Envelope',
									],
								],
							],
							'envelope-check' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/envelope[/:id]/check',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Envelope',
										'action' => 'check'
									],
								],
							],
							'nature' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/nature[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Nature',
									],
								],
							],
							'organization' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/organization[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Organization',
									],
								],
							],
							'management-unit' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/management-unit[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\ManagementUnit',
									],
								],
							],
							'destination' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/destination[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Destination',
									],
								],
							],
							'poste-expense' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/poste-expense[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\PosteExpense',
									],
								],
							],
							'poste-income' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/poste-income[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\PosteIncome',
									]
								],
							],
							'expense' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/expense[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Expense',
									],
								],
							],
							'income' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/income[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\Income',
									],
								],
							],
							'budget-status' => [
								'type' => 'Segment',
								'options' => [
									'route' => '/budget-status[/:id]',
									'defaults' => [
										'controller' => 'Budget\Controller\API\BudgetStatus',
									],
								],
							],
						],
					],
				],
			],
		],
	],
	'controllers' => [
		'factories' => [
			Controller\AccountController::class => ServiceLocatorFactory::class,
			Controller\ConfigurationController::class => ServiceLocatorFactory::class,
			Controller\EnvelopeController::class => ServiceLocatorFactory::class,
			Controller\NatureController::class => ServiceLocatorFactory::class,
			Controller\OrganizationController::class => ServiceLocatorFactory::class,
			Controller\ManagementUnitController::class => ServiceLocatorFactory::class,
			Controller\DestinationController::class => ServiceLocatorFactory::class,
			Controller\BudgetStatusController::class => ServiceLocatorFactory::class,
			Controller\ImportController::class => ServiceLocatorFactory::class,
			Controller\API\AccountController::class => ServiceLocatorFactory::class,
			Controller\API\BudgetCodeController::class => ServiceLocatorFactory::class,
			Controller\API\BudgetStatusController::class => ServiceLocatorFactory::class,
			Controller\API\EnvelopeController::class => ServiceLocatorFactory::class,
			Controller\API\NatureController::class => ServiceLocatorFactory::class,
			Controller\API\OrganizationController::class => ServiceLocatorFactory::class,
			Controller\API\ManagementUnitController::class => ServiceLocatorFactory::class,
			Controller\API\DestinationController::class => ServiceLocatorFactory::class,
			Controller\API\PosteExpenseController::class => ServiceLocatorFactory::class,
			Controller\API\PosteIncomeController::class => ServiceLocatorFactory::class,
			Controller\API\ExpenseController::class => ServiceLocatorFactory::class,
			Controller\API\IncomeController::class => ServiceLocatorFactory::class,
			Controller\API\SimplifiedInputController::class => ServiceLocatorFactory::class,
			Controller\API\ConfigurationController::class => ServiceLocatorFactory::class
		],
		'aliases' => [
			'Budget\Controller\Account' => Controller\AccountController::class,
			'Budget\Controller\Configuration' => Controller\ConfigurationController::class,
			'Budget\Controller\Envelope' => Controller\EnvelopeController::class,
			'Budget\Controller\Nature' => Controller\NatureController::class,
			'Budget\Controller\Organization' => Controller\OrganizationController::class,
			'Budget\Controller\ManagementUnit' => Controller\ManagementUnitController::class,
			'Budget\Controller\Destination' => Controller\DestinationController::class,
			'Budget\Controller\BudgetStatus' => Controller\BudgetStatusController::class,
			'Budget\Controller\Import' => Controller\ImportController::class,
			'Budget\Controller\API\Account' => Controller\API\AccountController::class,
			'Budget\Controller\API\BudgetCode' => Controller\API\BudgetCodeController::class,
			'Budget\Controller\API\BudgetStatus' => Controller\API\BudgetStatusController::class,
			'Budget\Controller\API\Envelope' => Controller\API\EnvelopeController::class,
			'Budget\Controller\API\Nature' => Controller\API\NatureController::class,
			'Budget\Controller\API\Organization' => Controller\API\OrganizationController::class,
			'Budget\Controller\API\ManagementUnit' => Controller\API\ManagementUnitController::class,
			'Budget\Controller\API\Destination' => Controller\API\DestinationController::class,
			'Budget\Controller\API\PosteExpense' => Controller\API\PosteExpenseController::class,
			'Budget\Controller\API\PosteIncome' => Controller\API\PosteIncomeController::class,
			'Budget\Controller\API\Expense' => Controller\API\ExpenseController::class,
			'Budget\Controller\API\Income' => Controller\API\IncomeController::class,
			'Budget\Controller\API\SimplifiedInput' => Controller\API\SimplifiedInputController::class,
			'Budget\Controller\API\Configuration' => Controller\API\ConfigurationController::class
		],
	],
];
