<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 04/08/16
 * Time: 13:44
 */

namespace Time\Entity;

use DateInterval;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use User\Entity\User;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Bookmark
 * @package Time\Entity
 * @author Curtis Pelissier <curtis.pelissier@laposte.net>
 *
 * @ORM\Entity
 * @ORM\Table(name="time_synchronisation")
 */
class Synchronisation extends BasicRestEntityAbstract
{
    const SYNC_TYPE_GOOGLE = 'google';
    const SYNC_TYPE_PHENIX = 'phenix';
    const SYNC_TYPE_ZIMBRA = 'zimbra';
    const SYNC_TYPE_ZIMBRA_REST = 'zimbra_rest';
    const SYNC_TYPE_OUTLOOK = 'outlook';

    const SYNC_CONFIG_FIELD_SUMMARY = 'summary';
    const SYNC_CONFIG_FIELD_DESCRIPTION = 'description';
    const SYNC_CONFIG_FIELD_LOCATION = 'location';
    const SYNC_CONFIG_FIELD_TAGS = 'tags';

    const SYNC_CONFIG_CLOSING_BRACKETS = 'brackets';
    const SYNC_CONFIG_CLOSING_HASHTAGS = 'hashtags';

    const SYNC_CONFIG_JOIN_CODE = 'code';
    const SYNC_CONFIG_JOIN_NAME = 'name';
    const SYNC_CONFIG_JOIN_DESCRIPTION = 'description';
    const SYNC_CONFIG_JOIN_CUSTOM = 'custom';

    const SCHEDULE_DAILY = "daily";
    const SCHEDULE_WEEKLY = "weekly";
    const SCHEDULE_MONTHLY = "monthly";
    const SCHEDULE_QUARTERLY = "quaterly";

    // date start
    const SCHEDULE_DAILY_INTERVAL = 1; // every day
    const SCHEDULE_WEEKLY_INTERVAL = 1; // every 1 week
    const SCHEDULE_MONTHLY_INTERVAL = 1; // every 1 month
    const SCHEDULE_QUARTERLY_INTERVAL = 3; // every 3 month

    // for prevent abuse sync volumetrie we sync only INTERVAL * 2 day/week/month
    const SCHEDULE_SYNC_DATE_START_MULTIPLICATOR = 2;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * Link of synchronisation
     * @var array
     * @ORM\Column(type="string")
     */
    protected $link;

    /**
     * Type of synchronisation
     * @var int
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * Default timezone
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $timezone;

    /**
     * Default descriptionMaxLength
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $descriptionMaxLength;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $parseTitle;

    /**
     * Config
     * @var array
     * @ORM\Column(type="json", nullable=true)
     */
    protected $config;

    /**
     * Bookmark's User
     * @var User
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * CronJob : users to sync on this calendar.
     * @var array
     *
     * @ORM\Column(type="json", nullable=true)
     */
    protected $cronUsers;

    /**
     * CronJob : scheduler condition
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $cronSchedule;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $cronLastSync;

    /**
     * Settings Time
     * @var SettingsTime
     * @ORM\ManyToOne(targetEntity="Time\Entity\SettingsTime", cascade={"persist"})
     */
    protected $settingsTime;

    public function __construct()
    {
    }

    public static function getConfigOptions()
    {
        return [
            'field' => [
                self::SYNC_CONFIG_FIELD_SUMMARY     => 'summary',
                self::SYNC_CONFIG_FIELD_DESCRIPTION => 'description',
                self::SYNC_CONFIG_FIELD_LOCATION    => 'location',
                self::SYNC_CONFIG_FIELD_TAGS        => 'tags',
            ],
            'closing' => [
                self::SYNC_CONFIG_CLOSING_BRACKETS => 'brackets',
                self::SYNC_CONFIG_CLOSING_HASHTAGS => 'hashtags',
            ],
            'join' => [
                self::SYNC_CONFIG_JOIN_CODE => 'code',
                self::SYNC_CONFIG_JOIN_NAME => 'name',
                self::SYNC_CONFIG_JOIN_CUSTOM => 'custom',
            ],
        ];
    }

    /**
     * Return the array of all types.
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::SYNC_TYPE_GOOGLE => 'Google',
            self::SYNC_TYPE_PHENIX => 'Phenix',
            self::SYNC_TYPE_ZIMBRA => 'Zimbra',
            self::SYNC_TYPE_ZIMBRA_REST => 'Zimbra (REST)',
            self::SYNC_TYPE_OUTLOOK => 'Outlook',
        ];
    }

    /**
     * Return the array of all schedule type.
     * @return string[]
     */
    static function getSchedulerType()
    {
        return [
            self::SCHEDULE_DAILY,
            self::SCHEDULE_WEEKLY,
            self::SCHEDULE_MONTHLY,
            self::SCHEDULE_QUARTERLY,
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param array $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone($tz)
    {
        $this->timezone = $tz;
    }

    /**
     * @return int
     */
    public function getDescriptionMaxLength()
    {
        return $this->descriptionMaxLength;
    }

    /**
     * @param int $descriptionMaxLength
     */
    public function setDescriptionMaxLength($descriptionMaxLength)
    {
        $this->descriptionMaxLength = $descriptionMaxLength;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getCronUsers()
    {
        return $this->cronUsers;
    }

    /**
     * @param array $cronUsers
     */
    public function setCronUsers($cronUsers)
    {
        $this->cronUsers = $cronUsers;
    }

    /**
     * @return string
     */
    public function getCronSchedule()
    {
        return $this->cronSchedule;
    }

    /**
     * @param string $cronSchedule
     */
    public function setCronSchedule($cronSchedule)
    {
        $this->cronSchedule = $cronSchedule;
    }

    /**
     * @return \DateTime
     */
    public function getCronLastSync()
    {
        return $this->cronLastSync;
    }

    /**
     * @param \DateTime $cronLastSync
     */
    public function setCronLastSync($cronLastSync)
    {
        $this->cronLastSync = $cronLastSync;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get settings Time
     *
     * @return  SettingsTime
     */
    public function getSettingsTime()
    {
        return $this->settingsTime;
    }

    /**
     * Set settings Time
     *
     * @param  SettingsTime  $settingsTime  Settings Time
     *
     * @return  self
     */
    public function setSettingsTime(SettingsTime $settingsTime)
    {
        $this->settingsTime = $settingsTime;

        return $this;
    }
    /**
     * Get parse title
     *
     * @return  boolean
     */
    public function getParseTitle()
    {
        return $this->parseTitle;
    }

    /**
     * Set parse title
     *
     * @param  boolean  $parseTitle  Settings Time
     *
     * @return  self
     */
    public function setParseTitle($parseTitle)
    {
        $this->parseTitle = $parseTitle;

        return $this;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'link',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'type',
                'required' => true,
            ],
            [
                'name'     => 'timezone',
                'required' => false,
            ],            [
                'name'     => 'descriptionMaxLength',
                'required' => false,
                'filters'     => [
                    ['name' => 'NumberParse'],
                ],
            ],
            [
                'name'     => 'parseTitle',
                'required' => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'     => 'config',
                'required' => false,
            ],
            [
                'name'     => 'cronUsers',
                'required' => false,
            ],
            [
                'name'     => 'cronLastSync',
                'required' => false,
            ],
            [
                'name'     => 'cronSchedule',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                if ($value) {
                                    return in_array($value, self::getSchedulerType());
                                }
                                return true;
                            },
                            'message' => 'timesheet_synchronisation_cron_schedule_err'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'settingsTime',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $settingsTime = $entityManager->getRepository('Time\Entity\SettingsTime')->find($value);

                                return $settingsTime !== null;
                            },
                            'message' => 'timesheet_field_settings_time_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'user',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $user = $entityManager->getRepository('User\Entity\User')->find($value);

                                return $user !== null;
                            },
                            'message' => 'timesheet_field_user_error_non_exists'
                        ],
                    ]
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * Get the type name with the type id parameter.
     *
     * @param int $type
     * @return mixed|null
     */
    public static function getTypeName($type)
    {
        if (is_numeric($type) && isset(self::getTypes()[$type])) {
            return self::getTypes()[$type];
        }

        return null;
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return $this->user == $user;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $user
     * @param string $owner
     */
    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        $queryBuilder->andWhere('object.user = :ownerControl');
        $queryBuilder->setParameter('ownerControl', $user);
    }

    /**
     * Check if schedule condition allow to exec. cron synchronisation.
     *
     * @return bool
     */
    public function canSyncOnCronByScheduleRules()
    {
        if (empty($this->getCronLastSync())) {
            return true;
        }
        $now = (new \DateTime('now'))->setTime(0, 0, 0);
        $lastSync = $this->getCronLastSync()->setTime(0, 0, 0);
        switch ($this->getCronSchedule()) {
            case self::SCHEDULE_DAILY:
                return $lastSync->add(DateInterval::createFromDateString(self::SCHEDULE_DAILY_INTERVAL . ' days')) <= $now;
            case self::SCHEDULE_WEEKLY:
                return $lastSync->add(DateInterval::createFromDateString(self::SCHEDULE_WEEKLY_INTERVAL . ' weeks')) <= $now;
            case self::SCHEDULE_MONTHLY:
                return $lastSync->add(DateInterval::createFromDateString(self::SCHEDULE_MONTHLY_INTERVAL . ' months')) <= $now;
            case self::SCHEDULE_QUARTERLY:
                return $lastSync->add(DateInterval::createFromDateString(self::SCHEDULE_QUARTERLY_INTERVAL . ' months')) <= $now;
        }
        return false;
    }

    /**
     * Compute date start for cron job.
     *
     * @return null|\DateTime
     */
    public function getSyncDateStartOnCron()
    {
        if(empty($this->getCronSchedule())) {
            return null;
        }
        $now = (new \DateTime('now'))->setTime(0, 0, 0);
        switch ($this->getCronSchedule()) {
            case self::SCHEDULE_DAILY:
                return $now->sub(DateInterval::createFromDateString((self::SCHEDULE_DAILY_INTERVAL * self::SCHEDULE_SYNC_DATE_START_MULTIPLICATOR) . ' days'));
            case self::SCHEDULE_WEEKLY:
                return $now->sub(DateInterval::createFromDateString((self::SCHEDULE_WEEKLY_INTERVAL * self::SCHEDULE_SYNC_DATE_START_MULTIPLICATOR) . ' weeks'));
            case self::SCHEDULE_MONTHLY:
                return $now->sub(DateInterval::createFromDateString((self::SCHEDULE_MONTHLY_INTERVAL * self::SCHEDULE_SYNC_DATE_START_MULTIPLICATOR) . ' months'));
            case self::SCHEDULE_QUARTERLY:
                return $now->sub(DateInterval::createFromDateString((self::SCHEDULE_QUARTERLY_INTERVAL * self::SCHEDULE_SYNC_DATE_START_MULTIPLICATOR) . ' months'));
        }
        return null;
    }
}
