<?php

return [
    'attachments_entities' => 'documents',
    'attachment_entity'    => 'document',
    'attachment_nav_form'  => 'Uploader un [[ attachment_entity ]]',

    'attachment_field_name'      => 'Nom',
    'attachment_field_url'       => 'Fichier',
    'attachment_field_key'       => 'Identifiant',
    'attachment_field_type'      => 'Type',
    'attachment_field_size'      => 'Taille',
    'attachment_field_user'      => '[[ user_entity | ucfirst ]]',
    'attachment_field_createdAt' => 'Uploadé le',
    'attachment_field_updatedAt' => '[[ field_updated_at_m ]]',
    'attachment_message_saved'   => '[[ attachment_entity ]] enregistré',

    'attachment_type_url'  => 'Lien',
    'attachment_type_file' => 'Fichier',

    'attachment_question_delete' => 'Voulez-vous réellement supprimer le [[ attachment_entity ]] %1 ?',
    'attachment_message_deleted' => '[[attachment_entity | ucfirst]] supprimé',
];
