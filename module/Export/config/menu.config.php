<?php

$export = [
    'icon' => 'word',
    'title' => 'export_module_title',
    'right' => 'export:e:export:r',
    'href' => 'export',
    'priority' => 2,
];

return [
    'menu' => [
        'client-menu' => [
            'administration' => [
                'children' => [
                    'export' => $export,
                ],
            ],
        ],
        'client-menu-parc' => [
            'administration' => [
                'children' => [
                    'export' => array_replace($export, ['priority' => 96]),
                ],
            ],
        ]
    ]
];
