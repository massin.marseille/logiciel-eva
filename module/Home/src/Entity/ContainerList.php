<?php
namespace Home\Entity;

use Application\Entity\Query;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContainerList
 *
 * @package Home\Entity
 *
 * @ORM\Entity
 */
class ContainerList extends Container
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $entity;

    /**
     * @var Query
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Query")
     */
    protected $query;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $columns = [];

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return Query
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param Query $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this::TYPE_LIST;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name'     => 'entity',
            'required' => true
        ]);

        $inputFilter->add([
            'name'       => 'query',
            'required'   => false,
            'validators' => [
                [
                    'name'    => 'Callback',
                    'options' => [
                        'callback' => function ($value, $context) use ($entityManager) {
                            if (is_array($value)) {
                                $value = $value['id'];
                            }

                            $query = $entityManager->getRepository('Application\Entity\Query')->find($value);

                            return $query !== null;
                        },
                        'message'  => 'container_field_query_error_non_exists',
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'columns',
            'required' => false
        ]);

        return $inputFilter;
    }
}
