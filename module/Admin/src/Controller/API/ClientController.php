<?php

namespace Admin\Controller\API;

use Bootstrap\Environment\Client\ClientCreator;
use Core\Controller\BasicRestController;

/**
 * Class ClientController
 *
 * The Client Entity REST controller.
 *
 * @package Admin\Controller\API
 */
class ClientController extends BasicRestController
{
    protected $entityClass = 'Bootstrap\Entity\Client';
    protected $repository  = 'Bootstrap\Entity\Client';
    protected $entityChain = 'admin:e:client';

    protected function extract($object)
    {
        $array         = parent::extract($object);
        $array['logo'] = $object->getLogo();

        return $array;
    }

    public function create($data)
    {
        ini_set('max_execution_time', -1);

        $this->cleanData($data);

        $create = parent::create($data);

        if ($create->success) {
            $client = $this->entityManager()->getRepository($this->repository)->find($create->object['id']);

            try {
                $creator = new ClientCreator($client, $this->serviceLocator);
                $creator->createDatabase();
                $creator->createFolders();
                $creator->createJSLanguage();
            } catch (\Exception $e) {
                $create->success = false;
                $create->error   = $e->getMessage();
            }
        }

        return $create;
    }

    public function update($id, $data)
    {
        $this->cleanData($data);

        $update = parent::update($id, $data);

        if ($update->success) {
            $client = $this->entityManager()->getRepository($this->repository)->find($update->object['id']);

            try {
                $creator = new ClientCreator($client, $this->serviceLocator);
                $creator->createJSLanguage();
            } catch (\Exception $e) {
                $update->success = false;
                $update->error   = $e->getMessage();
            }
        }

        return $update;
    }

    public function cleanData(&$data)
    {
        $icons        = $this->serviceLocator->get('Config')['icons'];
        $translations = $this->serviceLocator->get('Translator')->getAllMessages()->getArrayCopy();

        foreach ($data['icons'] as $key => $value) {
            if (!array_key_exists($key, $icons) || $value == $icons[$key]) {
                unset($data['icons'][$key]);
            }
        }

        foreach ($data['translations'] as $key => $value) {
            if (!array_key_exists($key, $translations) || $value == $translations[$key]) {
                unset($data['translations'][$key]);
            }
        }
    }
}
