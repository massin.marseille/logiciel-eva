<?php

namespace Core\Email;

use Laminas\Mail\Transport\Sendmail;
use Laminas\Mail\Message;
use Laminas\Mail\Transport\Smtp;
use Laminas\Mail\Transport\SmtpOptions;
use Laminas\Mail\Transport\TransportInterface;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Mime;
use Laminas\Mime\Part;


class Email
{
    /**
     * @var TransportInterface
     */
    protected $sender;

    /**
     * @var Message
     */
    protected $message;

    /**
     * @var MimeMessage
     */
    protected $mimeMessage;

    /**
     * @var array
     */
    protected $parts;

    const ENCODING = 'UTF-8';

    public function __construct($client = null)
    {
        $modules = isset($client) ? $client->getModules() : null;
        if(isset($modules) && isset($modules['application']) && !empty($modules['application']['mailingHost'])) {
            $smtpOptions = new SmtpOptions(
                array(
                    'name'              => $modules['application']['mailingDomainName'],
                    'host'              => $modules['application']['mailingHost'],
                    'port'              => $modules['application']['mailingPort'],
                    'connection_class'  => $modules['application']['mailingAuthType'], // smtp, plain, login, or crammd5
                    'connection_config' => array(
                        'username' => $modules['application']['mailingAuthUser'],
                        'password' => $modules['application']['mailingAuthPassword'],
                        'ssl' => $modules['application']['mailingAuthMode']  // ssl, tls
                    ),
                )
            );
            $this->sender = new Smtp($smtpOptions);
        } else {
            $this->sender = new Sendmail();
        }

        $this->message     = new Message();
        $this->mimeMessage = new MimeMessage();
        $this->parts       = [];

        $this->message->setEncoding(self::ENCODING);
    }

    public function setSubject($subject)
    {
        $this->message->setSubject($subject);
        return $this;
    }

    public function addHtmlContent($content, $withHeaders = false)
    {
        if ($withHeaders) {
            $body = new Part($content);
        } else {
            $body = new Part('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                </head>
                <body>'
                . $content .
                '</body>
    	    </html>');
        }

        $body->type    = 'text/html';
        $body->charset = self::ENCODING;

        $this->parts[] = $body;
        return $this;
    }

    public function setFrom($address, $name)
    {
        $this->message->setFrom($address, $name);
        return $this;
    }

    public function sendTo($recipient, $cc = null)
    {
        $this->mimeMessage->setParts($this->parts);

        $this->message->addTo($recipient)
            ->setBody($this->mimeMessage);

        if ($cc) {
            $this->message->addCc($cc);
        }

        $this->sender->send($this->message);
    }

    public function addAttachment($content, $filename, $type)
    {
        $attachment              = new Part($content);
        $attachment->filename    = $filename;
        $attachment->type        = $type;
        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
        $attachment->encoding    = Mime::ENCODING_BASE64;

        $this->parts[] = $attachment;
    }
}