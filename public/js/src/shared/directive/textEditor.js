(function (app) {

    app.directive('textEditor', [
        '$timeout',
        function textEditor($timeout) {
            return {
                restrict: 'E',
                scope: {
                    ngModel: '=ngModel',
                    placeholder: '@placeholderIfDefined',
                    type: '@type',
                    ngReadonly: '=?ngReadonly',
                    ngDisabled: '=?ngDisabled',
                    editor: '=?editor',
                    setup: '=?setup',
                    customButtons: '=?customButtons'
                },
                replace: false,
                transclude: false,
                template: '<span ng-if="tinymceOptions"><textarea placeholder="{{definedPlaceholder}}" ng-model="$parent.ngModel" ui-tinymce="$parent.tinymceOptions" ng-disabled="ngDisabled"></textarea></span>',
                compile: function compile() {
                    return function link($scope) {
                        var plugins  = [];
                        var menubar  = null;
                        var toolbar  = null;
                        var toolbar2 = null;
                        var force_p_newlines = true;

                        if ($scope.type == 'advanced') {
                            plugins = ['placeholder', 'fullscreen', 'code', 'paste', 'textcolor'];
                            menubar = '';
                            toolbar = 'undo redo | styleselect forecolor | bold italic underline | bullist numlist outdent indent | removeformat | code | fullscreen';
                        } else if ($scope.type == 'export') {
                            plugins = ['placeholder', 'fullscreen', 'code', 'textcolor', 'colorpicker', 'preview', 'table', 'visualblocks', 'visualchars', 'paste'];
                            menubar = 'format | table | view';
                            toolbar = 'undo redo | bold italic underline fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | code | fullscreen';
                            toolbar2 = 'preview | forecolor backcolor | variables';
                            force_p_newlines = false;
                        } else {
                            plugins = ['placeholder', 'fullscreen', 'code', 'paste'];
                            menubar = '';
                            toolbar = 'undo redo | bold italic underline | bullist numlist outdent indent | removeformat | code | fullscreen';
                        }



                        if ($scope.placeholder) {
                            if (!/_placeholder_/.test($scope.placeholder)) {
                                $scope.definedPlaceholder = $scope.placeholder;
                            }
                        }

                        if ($scope.customButtons) {
                            toolbar += ' | ';

                            for (var i in $scope.customButtons) {
                                var customButton = $scope.customButtons[i];

                                toolbar += customButton.name + ' ';
                            }
                        }

                        var editor = null;
                        $scope.tinymceOptions = {
                            language: 'fr_FR',
                            language_url: '/js/tinymce/langs/fr_FR.js',
                            plugins: plugins,
                            menubar: menubar,
                            toolbar1: toolbar,
                            toolbar2: toolbar2,
                            statusbar: true,
                            min_height: 200,
                            resize: true,
                            content_style: 'body {font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;font-size: 80% !important;}',
                            browser_spellcheck: true,
                            //forced_root_block : force_p_newlines,
                            paste_remove_styles_if_webkit: true,
                            formats: {
                                underline: {inline: 'u'}
                            },
                            init_instance_callback: function (_editor) {
                                $scope.$apply(function () {
                                    editor = _editor;
                                    $scope.editor = editor;

                                    $scope.$watch('ngReadonly', function (newVal) {
                                        if (editor || $scope.editor) {
                                            editor.setMode(newVal ? 'readonly' : 'design');
                                            $scope.editor.setMode(newVal ? 'readonly' : 'design');
                                        }
                                    });
                                });
                            },
                            setup: function (editor) {
                                for (var i in $scope.customButtons) {
                                    var customButton = $scope.customButtons[i];

                                    editor.addButton(customButton.name, {
                                        text: customButton.text,
                                        classes: customButton.showFullScreen ? 'show-full-screen' : '',
                                        icon: false,
                                        onclick: customButton.function
                                    });
                                }
                            }
                        };

                        if ($scope.setup) {
                            $scope.tinymceOptions.setup = $scope.setup;
                        }
                    }
                }
            }
        }
    ])

})(window.EVA.app);
