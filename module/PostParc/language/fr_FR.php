<?php

return [
    'postParc_module_title' => 'Liaison POSTPARC',
    
    'post_parc_api_url'      => 'Url de l\'API',
    'post_parc_api_user'     => 'Utilisateur',
    'post_parc_api_password' => 'Mot de passe'

];
