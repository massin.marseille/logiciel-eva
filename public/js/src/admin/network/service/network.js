(function(app) {

    app.factory('networkService', [
        'restFactoryService',
        function networkService(restFactoryService) {
            return restFactoryService.create('/api/network');
        }
    ])

})(window.EVA.app);
