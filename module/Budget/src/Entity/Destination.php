<?php

namespace Budget\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Laminas\InputFilter\Factory;
use Laminas\InputFilter\InputFilter;

/**
 * @ORM\Entity
 * @ORM\Table(name="budget_destination")
 */
class Destination extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var self
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Destination", inversedBy="children")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $parent;

    /**
     * @var self[] | ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Budget\Entity\Destination", mappedBy="parent")
     */
    protected $children;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return self
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param self $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return self[] | ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param self[] | ArrayCollection $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @param self[] | ArrayCollection $children
     */
    public function addChildren($children)
    {
        foreach ($children as $child) {
            $child->setParent($this);
            $this->getChildren()->add($child);
        }
    }

    /**
     * @param self[] | ArrayCollection $children
     */
    public function removeChildren($children)
    {
        foreach ($children as $child) {
            $child->setParent(null);
            $this->getChildren()->removeElement($child);
        }
    }

    /**
     * @param EntityManager $entityManager
     *
     * @return InputFilter
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();

        /** @var InputFilter $inputFilter */
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name' => 'code',
                'required' => false,
                'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $queryBuilder = $entityManager->createQueryBuilder();
                                $queryBuilder
                                    ->select('o')
                                    ->from(self::class, 'o')
                                    ->where('o.code = :code')
                                    ->andWhere('o.id != :id')
                                    ->setParameters([
                                        'id' => !isset($context['id']) ? '' : $context['id'],
                                        'code' => $value,
                                    ]);

                                try {
                                    $object = $queryBuilder->getQuery()->getOneOrNullResult();
                                } catch (Exception $e) {
                                    $object = null;
                                }

                                return $object === null;
                            },
                            'message' => 'destination_field_code_error_unique',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'name',
                'required' => true,
                'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
            ],
            [
                'name' => 'parent',
                'required' => false,
                'filters' => [['name' => 'Core\Filter\IdFilter']],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                return $entityManager->getRepository(self::class)->find($value) !==
                                    null;
                            },
                            'message' => 'destination_field_parent_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                if (isset($context['id'])) {
                                    $object = $entityManager
                                        ->getRepository(self::class)
                                        ->find($value);

                                    $same = $value == $context['id'];
                                    $hierarchy = false;

                                    while ($object->getParent() !== null && !$hierarchy) {
                                        $hierarchy =
                                            $object->getParent()->getId() == $context['id'];
                                        $object = $object->getParent();
                                    }

                                    return !$same && !$hierarchy;
                                }

                                return true;
                            },
                            'message' => 'destination_field_parent_error_hierarchy',
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
