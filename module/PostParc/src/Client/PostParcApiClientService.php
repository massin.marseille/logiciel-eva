<?php

namespace PostParc\Client;

use Bootstrap\Entity\Client;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client as GuzzleHttpClient;
use Guzzle\Http\Exception\ClientErrorResponseException;

class PostParcApiClientService
{
    private $_httpClient = null;
    private $_configuration;
    private $_baseUrl;

    public function __construct($configuration)
    {
        $this->_httpClient = new GuzzleHttpClient(['verify' => false, 'http_errors' => false]);
        $this->_configuration = $configuration;
        $this->_baseUrl = $configuration['apiUrl'];
    }

    public function getPfos()
    {

        try
        {
            $token = $this->getToken();
            $page = 1;

            $pfos = new ArrayCollection();
            $stop = false;
            do {
                $response = json_decode($this->_httpClient->get($this->_baseUrl . '/pfos?page=' . $page, [
                    'headers' => ['Content-Type' => 'application/json', 'X-Auth-Token' => $token],
                ])->getBody());

                if (!isset($response->items)) {
                    return new ArrayCollection();
                }

                if (count($response->items) === 0) {
                    $stop = true;
                } else {
                    foreach ($response->items as $item) {
                        $pfos->add($item);
                    }
                    $page = $page + 1;
                }

            } while ($stop === false);

            return $pfos;
        } catch (ClientErrorResponseException $e) {
            return new ArrayCollection();
        }
    }

    public function getGroups()
    {
        $token = $this->getToken();

        $response = json_decode($this->_httpClient->get($this->_baseUrl . '/groups', [
            'headers' => ['Content-Type' => 'application/json', 'X-Auth-Token' => $token],
        ])->getBody());

        return $response;
    }

    public function getFunctions()
    {
        $token = $this->getToken();

        $response = json_decode($this->_httpClient->get($this->_baseUrl . '/personFunctions', [
            'headers' => ['Content-Type' => 'application/json', 'X-Auth-Token' => $token],
        ])->getBody());

        return $response;
    }

    public function getOrganizationsTypes()
    {
        $token = $this->getToken();

        $response = json_decode($this->_httpClient->get($this->_baseUrl . '/organizationTypes', [
            'headers' => ['Content-Type' => 'application/json', 'X-Auth-Token' => $token],
        ])->getBody());

        return $response;
    }

    public function getOrganizations()
    {
        $token = $this->getToken();
        $page = 1;
        $items = new ArrayCollection();
        $stop = false;
        do {
            $response = json_decode($this->_httpClient->get($this->_baseUrl . '/organizations?page=' . $page, [
                'headers' => ['Content-Type' => 'application/json', 'X-Auth-Token' => $token],
            ])->getBody());

            if (count($response->items) === 0) {

                $stop = true;
            } else {
                foreach ($response->items as $item) {
                    $items->add($item);
                }
                $page = $page + 1;
            }
        } while ($stop === false);

        return $items;
    }

    public function getPersons()
    {
        try
        {
            $token = $this->getToken();
            $page = 1;
            $items = new ArrayCollection();
            $stop = false;
            do {
                $response = json_decode($this->_httpClient->get($this->_baseUrl . '/persons?page=' . $page, [
                    'headers' => ['Content-Type' => 'application/json', 'X-Auth-Token' => $token],
                ])->getBody());

                if (!isset($response->items)) {
                    return new ArrayCollection();
                }

                if (count($response->items) === 0) {

                    $stop = true;
                } else {
                    foreach ($response->items as $item) {
                        $items->add($item);
                    }
                    $page = $page + 1;
                }
            } while ($stop === false);

            return $items;
        } catch (ClientErrorResponseException $e) {
            return new ArrayCollection();
        }
    }

    private function getToken()
    {
        $response = json_decode($this->_httpClient->post($this->_baseUrl . '/auth-tokens', [
            'form_params' => [
                'login' => $this->_configuration['user'],
                'password' => $this->_configuration['password'],
            ],
        ])->getBody());

        return $response->token;
    }

}
