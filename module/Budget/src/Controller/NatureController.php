<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class NatureController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:nature:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('budget:e:nature', $id)) {
            return $this->notFoundAction();
        }

        $nature = $this->entityManager()
            ->getRepository('Budget\Entity\Nature')
            ->find($id);

        if (
            $nature &&
            !$this->acl()->ownerControl('budget:e:nature:u', $nature) &&
            !$this->acl()->ownerControl('budget:e:nature:r', $nature)
        ) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'nature' => $nature,
        ]);
    }
}
