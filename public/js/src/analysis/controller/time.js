(function (app) {

    app.controller('timeController', [
        '$timeout', '$scope', '$rootScope', 'queryService',
        function timeController($timeout, $scope, $rootScope, queryService) {
            var self   = this;
            var active = null;

            self.datas   = [];
            self.options = {};

            self.filters = {
                user: {},
                project: {},
                timesheet: {},
                group: null
            };
            self.nbFilters = 1;

            self.filtersApplied = {
                user: {},
                project: {},
                timesheet: {},
                group: null
            };

            self.currentUser = null;

            self.init = function (currentUser) {
                self.switchTab('timesheet');
                self.initQueries();
                self.currentUser = currentUser;
            };

            $scope.$on('aggregateProject', function (event, data) {
                $scope.$broadcast('applyFilters', {active: active, filters: self.filtersApplied, projectAggregate: data.projectAggregate});
            });

            $scope.$on('divideTimes', function (event, data) {
                $scope.$broadcast('applyFilters', {active: active, filters: self.filtersApplied, divideTimes: data.divideTimes});
            });

            self.applyFilters = function () {
                self.filtersApplied = angular.copy(self.filters);
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.user).length + Object.keys(self.filters.timesheet).length;

                $timeout(function () {
                    $scope.$broadcast('applyFilters', {active: active, filters: self.filtersApplied, shouldReload: true});
                });
            };

            self.switchTab = function (name) {
                active = name;
                $timeout(function () {
                    $scope.$broadcast('switchTab', {active: name, filters: self.filtersApplied});
                });
            };

            self.showExportModal = function () {
                angular.element('#exportModal').modal('show');
            };

            self.export = function () {
                angular.element('#exportForm').submit();
                self.exportModel = null;
                angular.element('#exportModal').modal('hide');
            };

            self.resetFilters = function () {
                self.filtersApplied = self.filters = {
                    user: {},
                    project: {},
                    timesheet: {}
                };
                self.nbFilters = 0;
            };

            $scope.$on('export', function (event, data) {
                window.open('/analysis/time/' + data.type + '?' + $.param({
                    filters: self.filtersApplied,
                    projectAggregate: data.projectAggregate,
                    parent: data.parent,
                    group: data.group,
                    export: data.type,
                    all: data.all,
                    group_type: data.group_type,
                    divideTimes: data.divideTimes
                }));
            });

            $scope.$on('loadingOn', function () {
                self.loading = true;
            });

            $scope.$on('loadingOff', function () {
                self.loading = false;
            });

            // QUERIES
            self.queries          = [];
            self.query            = {
                list: 'analysis-time'
            };
            self.errors           = {
                fields: {}
            };
            self.loadingQueries   = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries        = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = async function (query) {
                self.loadingQueries = true;
                self.resetFilters();
                const goodQuery = (await queryService.find(query.id, {col: queryApiCol})).object;
                for (const key of Object.keys(goodQuery.filters)) {
                    if(Array.isArray(goodQuery.filters[key])) {
                        goodQuery.filters[key] = {};
                    }
                    // Empty filters are stored as arrays io object
                    // and this causes some issues with the filtering (combining queries with filters)
                    // thus the transformation to object
                    query.filters[key] = self.toObject(goodQuery.filters[key])
                }

                self.filters = angular.copy(goodQuery.filters);
                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
                self.loadingQueries = false;

            };

            self.toObject = function(arr) {
                if(null === arr || undefined === arr) {
                    return arr;
                }

                var obj = {};
                for (var i = 0; i < arr.length; ++i)
                  if (arr[i] !== undefined) obj[i] = arr[i];
                return obj;
            }

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters    = self.filters;
                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };
        }
    ]);

    app.controller('filterController', [
        '$scope',
        function ($scope) {
            var self = this;

            self.filtersKey = null;

            self.init = function (filtersKey) {
                self.filtersKey = filtersKey;
                if (self.filters) {
					$timeout(function () {
						if (typeof $scope.__filters === 'undefined') {
							$scope.__filters = {};
						}

                        if (typeof $scope.__filters[filters] === 'undefined') {
							$scope.__filters[filters] = {};
						}
					});
				}
            };

            var events = [
                'analysis-time-query-loaded'
            ];

            events.forEach(function (event) {
                $scope.$on(event, function (e, filters) {
                    showFilters(filters[self.filtersKey]);
                });
            });

            function showFilters(filters) {
                $scope.__filters = {};
                for (var i in filters) {
                    if (
                        filters[i] !== null &&
                        (
                            typeof filters[i].val !== 'undefined' ||
                            filters[i].op === 'isNull'
                        )
                    ) {
                        $scope.__filters[i] = true;
                    } else {
                        $scope.__filters[i] = false;
                    }
                }
            }
        }
    ]);
})(window.EVA.app);
