<?php

namespace Age\Import\Connection;

use Age\Import\AbstractConnection;

/**
 * Create a pdo Mysql instance to the AGE database.
 *
 * @package Age\Importer\Connection
 */
class MysqlConnection extends AbstractConnection
{
  public function __construct($configuration)
  {
    parent::__construct(
      $configuration,
      "mysql:host={$configuration['dbHost']};port={$configuration['dbPort']};dbname={$configuration['dbName']}",
      $configuration['dbUser'],
      $configuration['dbPassword'],
      [
        1002 => 'SET NAMES utf8',
      ]
    );
  }


  /**
   * Retrieve structure id from libelle.
   * Used to filters rows.
   *
   * @param $libelle
   *
   * @return bool
   */
  protected function etablissement($libelle)
  {
    $statement = $this->prepare('
      SELECT 
        ID
      FROM
        ref_etablissements
      WHERE
        LIBELLE = :libelle;
    ');

    $statement->execute([
      ':libelle' => $libelle,
    ]);

    if ($row = $statement->fetch()) {
      return $row['ID'];
    }

    return false;
  }

  /**
   * Retrieve budget accounts.
   *
   * @return array
   */
  public function accounts()
  {
    $statement = $this->prepare('
      SELECT 
        CODE, LIBELLE, EST_DEPENSE
      FROM 
        ref_natures
      WHERE 
        ETABLISSEMENT_ID = :identifier
        AND EST_SAISISSABLE_AU_BUDGET <> 0;
    ');

    $statement->execute([
      ':identifier' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function expenseDv()
  {
    $query = '
      SELECT
        SUM(TL.VAL_COMPTA_MONTANT) AS "amount",
        TP.DATE_COMPTABLE AS "date",
        TV.EXERCICE AS "exercice",
        RO.CODE AS "project_code",
        RN.CODE AS "account_code",
        TP.NUMERO_PIECE AS "engagement",
        TP.OBJET AS "name",
        TP.OBJET AS "complement",
        RT.LIBELLE AS "tiers"
      FROM 
        TRESO_DV_VENTILATIONS TV
        INNER JOIN REF_OPERATIONS RO ON RO.ID = TV.OPERATION_ID
        INNER JOIN TRESO_DV_LIGNES TL ON TL.ID = TV.LIGNE_ID 
        INNER JOIN TRESO_DV_PIECES TP ON TP.ID = TL.PIECE_ID 
        LEFT JOIN REF_NATURES RN ON RN.ID = TV.NATURE_ID 
        INNER JOIN REF_TIERS RT ON RT.ID = TL.TIERS_ID
      WHERE 
        TV.ETABLISSEMENT_ID = :identifier1
        AND RO.TYPE_OPERATION = 1
        AND TP.NATURE = 4';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND TV.EXERCICE IN(' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY TP.DATE_COMPTABLE, TV.EXERCICE, RO.CODE, RN.CODE, TP.NUMERO_PIECE, TP.OBJET, RT.LIBELLE;
    ';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);
    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_AE_COMMITTED expenses.
   *
   * @return array
   */
  public function expenseAeCommitted()
  {
    $query = '
      SELECT 
        SUM(CB.MONTANT_COMPTABILISATION) AS "amount", 
        CB.DATE_ECRITURE AS "date",
        CB.EXERCICE as "exercice",
        RO.' . $this->codeColumn . ' AS "project_code",
        RN.CODE AS "account_code",
        CONCAT(CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \') AS "engagement",
        IF(CB.PIECE_NATURE_DOCUMENT = \'DP\', DP.OBJET, IF(CB.PIECE_NATURE_DOCUMENT = \'OD\', OD.OBJET , EP.OBJET)) AS "name",
        IF(CB.PIECE_NATURE_DOCUMENT = \'DP\', DP.OBJET, IF(CB.PIECE_NATURE_DOCUMENT = \'OD\', OD.OBJET , EP.OBJET)) AS "complement",
        IF(
          EP.NATURE_DOCUMENT = \'TRANCHE\', RT2.LIBELLE, 
          IF(
            CB.PIECE_NATURE_DOCUMENT = \'DP\', RT3.LIBELLE, 
            IF(
              CB.PIECE_NATURE_DOCUMENT = \'REJ\', RT4.LIBELLE,
              IF(
                CB.PIECE_NATURE_DOCUMENT = \'OD\' AND EP3.ID IS NOT NULL, RT5.LIBELLE,
                IF(
        CB.PIECE_NATURE_DOCUMENT = \'OD\' AND DP2.ID IS NOT NULL, RT6.LIBELLE,
        RT.LIBELLE
        ) 
              ) 
            )
          )
        ) AS "tiers"
      FROM 
        comptabilites_budgetaires CB
        INNER JOIN ' . $this->codeAeTable . ' RO ON RO.' . $this->codeIdColumn . ' = CB.' . $this->budgIdColumn . '
        INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = :identifier1
        LEFT JOIN ej_pieces EP ON EP.ID = CB.PIECE_ID AND EP.ETABLISSEMENT_ID = :identifier2 
        LEFT JOIN ref_tiers RT ON RT.ID = EP.TIERS_ID 
        LEFT JOIN marches_marches MM ON MM.ID = EP.PIECE_MARCHE_ID AND MM.ETABLISSEMENT_ID = :identifier3
        LEFT JOIN ref_tiers RT2 ON RT2.ID = MM.TIERS_ID
        LEFT JOIN dp_pieces DP ON DP.ID = CB.PIECE_ID AND DP.ETABLISSEMENT_ID = :identifier4 
        LEFT JOIN ref_tiers RT3 ON RT3.ID = DP.TIERS_ID
        LEFT JOIN ej_pieces EP2 ON EP2.ID = EP.PIECE_REFERENCE_ID AND EP.ETABLISSEMENT_ID = :identifier5
        LEFT JOIN ref_tiers RT4 ON RT4.ID = EP2.TIERS_ID
        LEFT JOIN od_pieces OD ON OD.ID = CB.PIECE_ID AND OD.ETABLISSEMENT_ID = :identifier6
        LEFT JOIN od_pieces_liees ODP ON ODP.PIECE_OD_ID = OD.ID AND ODP.ETABLISSEMENT_ID = :identifier7
        LEFT JOIN ej_pieces EP3 ON EP3.ID = ODP.PIECE_LIEE_ID AND EP3.ETABLISSEMENT_ID = :identifier8
        LEFT JOIN ref_tiers RT5 ON RT5.ID = EP3.TIERS_ID
        LEFT JOIN dp_pieces DP2 ON DP2.ID = ODP.PIECE_LIEE_ID AND DP2.ETABLISSEMENT_ID = :identifier9
        LEFT JOIN ref_tiers RT6 ON RT6.ID = DP2.TIERS_ID 
      WHERE 
        CB.TYPE_COMPTA = \'AE\'
        AND CB.EST_CONSOMMATION = 1
        AND CB.EST_BUDGET = 0
        AND CB.ETABLISSEMENT_ID = :identifier10';

    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }    
    $query .= '
      GROUP BY CB.DATE_ECRITURE, CB.EXERCICE, RO.' . $this->codeColumn . ', RN.CODE, CB.PIECE_NUMERO_PIECE, CB.PIECE_NATURE_DOCUMENT, DP.OBJET, OD.OBJET, EP.OBJET, EP.NATURE_DOCUMENT, RT2.LIBELLE, RT3.LIBELLE, RT4.LIBELLE, EP3.ID, RT5.LIBELLE, DP2.ID, RT6.LIBELLE, RT.LIBELLE;
    ';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier,
      ':identifier3' => $this->identifier,
      ':identifier4' => $this->identifier,
      ':identifier5' => $this->identifier,
      ':identifier6' => $this->identifier,
      ':identifier7' => $this->identifier,
      ':identifier8' => $this->identifier,
      ':identifier9' => $this->identifier,
      ':identifier10' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function expenseAe()
  {
    $query = '
      SELECT
          RO.' . $this->codeColumn . ' AS "project_code",
          CB.PIECE_OBJET AS "name",
          CB.EXERCICE AS "exercice",
          CB.DATE_ECRITURE AS "date",
          SUM(CB.MONTANT_COMPTABILISATION) AS "amount",
          CONCAT(CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \') AS "engagement",
          RN.CODE AS "account_code"
      FROM
          comptabilites_budgetaires CB
          INNER JOIN ' . $this->codeAeTable . ' RO ON RO.' . $this->codeIdColumn . ' = CB.' . $this->budgIdColumn . '
          INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = :identifier1
      WHERE
          CB.EST_BUDGET = 1 
          AND CB.ETABLISSEMENT_ID = :identifier2 
          AND CB.TYPE_COMPTA = \'AE\'';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY RO.' . $this->codeColumn . ', CB.PIECE_OBJET, CB.EXERCICE, CB.DATE_ECRITURE, CB.PIECE_NUMERO_PIECE, CB.PIECE_NATURE_DOCUMENT, RN.CODE;
    ';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function expenseCp()
  {
    $query = '
      SELECT
          RO.' . $this->codeColumn . ' AS "project_code",
          CB.PIECE_OBJET AS "name",
          CB.EXERCICE AS "exercice",
          CB.DATE_ECRITURE AS "date",
          SUM(CB.MONTANT_COMPTABILISATION) AS "amount",
          CONCAT(CB.PIECE_NUMERO_PIECE, \' (\', CB.PIECE_NATURE_DOCUMENT, \') \') AS "engagement",
          RN.CODE AS "account_code"
      FROM
          comptabilites_budgetaires CB
          INNER JOIN ' . $this->codeCpTable . ' RO ON RO.' . $this->codeIdColumn . ' = CB.' . $this->budgIdColumn . '
          INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = :identifier1
      WHERE
          CB.EST_BUDGET = 1 
          AND CB.ETABLISSEMENT_ID = :identifier2 
          AND CB.TYPE_COMPTA = \'CP\'';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY RO.' . $this->codeColumn . ', CB.PIECE_OBJET, CB.EXERCICE, CB.DATE_ECRITURE, CB.PIECE_NUMERO_PIECE, CB.PIECE_NATURE_DOCUMENT, RN.CODE;
    ';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_CP_ESTIMATED expenses.
   *
   * @return array
   */
  public function expenseCpEstimated()
  {
    $query = '
      SELECT 
        SUM(DV.VAL_COMPTA_CHARGE_BUD) AS "amount", 
        RO.' . $this->codeColumn . ' AS "project_code",
        RN.CODE AS "account_code",
        DP.EXERCICE as "exercice",
        DP.DATE_RECEPTION AS "date",
        FF.DATE_FACTURE AS "date_facture",
        DP.REFERENCE_FACTURE AS "mandat",
        DP.OBJET AS "name",
        DP.OBJET AS "complement",
        RT.LIBELLE AS "tiers"
      FROM 
        dp_ventilations DV
        INNER JOIN ' . $this->codeCpEstTable . ' RO ON RO.' . $this->codeIdCpEstColumn . ' = DV.' . $this->budgIdColumn . '
        INNER JOIN ref_natures RN ON RN.ID = DV.NATURE_ID
        INNER JOIN dp_pieces DP ON DP.ID = DV.PIECE_ID
        INNER JOIN ref_tiers RT ON RT.ID = DP.TIERS_ID
        LEFT JOIN ff_pieces FF ON FF.ID = DP.PIECE_FF_ID AND FF.ETABLISSEMENT_ID = :identifier1
      WHERE 
        DV.EST_ANNULE = 0
        AND DV.ETABLISSEMENT_ID = :identifier2
        AND RN.ETABLISSEMENT_ID = :identifier3
        AND DP.ETABLISSEMENT_ID = :identifier4';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND DV.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY RO.' . $this->codeColumn . ', DP.EXERCICE, DP.DATE_RECEPTION, FF.DATE_FACTURE, DP.REFERENCE_FACTURE, DP.OBJET, RN.CODE, RT.LIBELLE;
    ';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier,
      ':identifier3' => $this->identifier,
      ':identifier4' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_CP_COMMITTED expenses.
   *
   * @return array
   */
  public function expenseCpCommitted()
  {
    $query = '
      SELECT 
        SUM(CB.MONTANT_COMPTABILISATION) AS "amount", 
        CB.DATE_ECRITURE AS "date",
        CB.EXERCICE as "exercice",
        RO.' . $this->codeColumn . ' AS "project_code",
        RN.CODE AS "account_code",
        RT.LIBELLE AS "tiers",
        CASE CB.PIECE_NATURE_DOCUMENT
          WHEN \'OD\'
          THEN OD.NUMERO_PIECE
          ELSE DP.NUMERO_PIECE
        END AS "bordereau",
        CASE CB.PIECE_NUMERO_PIECE
          WHEN \'OD\'
          THEN OD.OBJET
          ELSE DP.OBJET
        END AS "complement",
        CASE CB.PIECE_NUMERO_PIECE
          WHEN \'OD\'
          THEN OD.OBJET
          ELSE DP.OBJET
        END AS "name"
      FROM 
        comptabilites_budgetaires CB
        INNER JOIN ' . $this->codeCpTable . ' RO ON RO.' . $this->codeIdColumn . ' = CB.' . $this->budgIdColumn . '
        INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID AND RN.ETABLISSEMENT_ID = :identifier1
        LEFT JOIN dp_ventilations DV ON DV.ID = CB.DP_VENTILATIONS_ID AND DV.ETABLISSEMENT_ID = :identifier2
        LEFT JOIN dp_pieces DP ON DP.ID = DV.PIECE_ID AND DP.ETABLISSEMENT_ID = :identifier3
        LEFT JOIN ref_tiers RT ON RT.ID = DP.TIERS_ID
        LEFT JOIN od_pieces OD ON OD.ID = CB.PIECE_ID AND OD.ETABLISSEMENT_ID = :identifier4
      WHERE 
        CB.TYPE_COMPTA = \'CP\'
        AND CB.EST_CONSOMMATION = 1
        AND CB.EST_BUDGET = 0
        AND CB.ETABLISSEMENT_ID = :identifier5
        AND RN.ETABLISSEMENT_ID = :identifier6';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY CB.DATE_ECRITURE, CB.EXERCICE, RO.' . $this->codeColumn . ', RN.CODE, RT.LIBELLE, CASE CB.PIECE_NATURE_DOCUMENT WHEN \'OD\' THEN OD.NUMERO_PIECE ELSE DP.NUMERO_PIECE END, CASE CB.PIECE_NUMERO_PIECE WHEN \'OD\' THEN OD.OBJET ELSE DP.OBJET END;';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier,
      ':identifier3' => $this->identifier,
      ':identifier4' => $this->identifier,
      ':identifier5' => $this->identifier,
      ':identifier6' => $this->identifier
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }


  public function incomeDv()
  {
    $query = '
      SELECT
        SUM(TL.VAL_COMPTA_MONTANT) AS "amount",
        TP.DATE_COMPTABLE AS "date",
        TV.EXERCICE AS "exercice",
        RO.CODE AS "project_code",
        RN.CODE AS "account_code",
        TP.NUMERO_PIECE AS "convention_title",
        TP.OBJET AS "name",
        TP.OBJET AS "engagement",
        RT.LIBELLE AS "tiers"
      FROM 
        TRESO_DV_VENTILATIONS TV
        INNER JOIN REF_OPERATIONS RO ON RO.ID = TV.OPERATION_ID
        INNER JOIN TRESO_DV_LIGNES TL ON TL.ID = TV.LIGNE_ID 
        INNER JOIN TRESO_DV_PIECES TP ON TP.ID = TL.PIECE_ID 
        LEFT JOIN REF_NATURES RN ON RN.ID = TV.NATURE_ID 
        INNER JOIN REF_TIERS RT ON RT.ID = TL.TIERS_ID
      WHERE 
        TV.ETABLISSEMENT_ID = :identifier1
        AND RO.TYPE_OPERATION = 1
        AND TP.NATURE = 3';
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND TV.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY TP.DATE_COMPTABLE, TV.EXERCICE, RO.CODE, RN.CODE, TP.NUMERO_PIECE, TP.OBJET, RT.LIBELLE;';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier
    ]);
    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }


  /**
   * Retrieve TYPE_REQUESTED incomes.
   *
   * @return array
   */
  public function incomeRequested()
  {
    $codeColumn = $this->switchCode ? 'CODE_ANALYTIQUE_CODE' : 'CODE';
    $codeTable = $this->switchCode ? 'ref_analytique' : 'ref_operations';
    $budgIdColumn = $this->switchCode ? 'ANALYTIQUE_ID' : 'OPERATION_ID';

    $query = '
      SELECT
          SUM(TL.VAL_COMPTA_MONTANT_TTC_A_ENCAISSER) AS "amount",
          TP.DATE_COMPTABLE AS "date",
          TV.EXERCICE as "exercice",
          RO.' . $codeColumn . ' AS "project_code",
          RN.CODE AS "account_code",
          TP.NUMERO_PIECE AS "convention_title",
          TP.OBJET AS "name",
          TP.OBJET AS "engagement",
          RT.LIBELLE AS "tiers"
      FROM
          titres_ventilations TV
          INNER JOIN titres_lignes TL ON TL.ID = TV.LIGNE_ID
          INNER JOIN titres_pieces TP ON TP.ID = TL.PIECE_ID
          INNER JOIN ref_tiers RT ON RT.ID = TP.TIERS_ID
          INNER JOIN ' . $codeTable . '  RO ON RO.ID = TV.' . $budgIdColumn . '
          INNER JOIN ref_natures RN ON RN.ID = TV.NATURE_ID
      WHERE
          TL.VAL_COMPTA_MONTANT_TTC_A_ENCAISSER <> 0
          AND TV.ETABLISSEMENT_ID = :identifier1
          AND RN.ETABLISSEMENT_ID = :identifier2
          AND TL.ETABLISSEMENT_ID = :identifier3
          AND TP.ETABLISSEMENT_ID = :identifier4';
    if ($this->switchCode) {
      $query .= '
        AND RO.NIVEAU_ANALYTIQUE_ORDRE_DE_PRESENTATION = 1';
    }
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND TV.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY TP.DATE_COMPTABLE, TV.EXERCICE, RO.' . $codeColumn . ', RN.CODE, TP.NUMERO_PIECE, TP.OBJET, RT.LIBELLE;';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier,
      ':identifier3' => $this->identifier,
      ':identifier4' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Retrieve TYPE_PERCEIVED incomes.
   *
   * @return array
   */
  public function incomePerceived()
  {
    $codeColumn = $this->switchCode ? 'CODE_ANALYTIQUE_CODE' : 'CODE';
    $codeTable = $this->switchCode ? 'ref_analytique' : 'ref_operations';
    $budgIdColumn = $this->switchCode ? 'ANALYTIQUE_ID' : 'OPERATION_ID';

    $query = '
      SELECT 
        SUM(CB.MONTANT_COMPTABILISATION) AS "amount", 
        CB.DATE_ECRITURE AS "date",
        CB.EXERCICE as "exercice",
        RO.' . $codeColumn . ' AS "project_code",
        RN.CODE AS "account_code",
        TP.NUMERO_PIECE AS "convention_title",
        TP.OBJET AS "name",
        TP.OBJET AS "engagement",
        RT.LIBELLE AS "tiers"
      FROM 
        comptabilites_budgetaires CB
        INNER JOIN ' . $codeTable . '  RO ON RO.ID = CB.' . $budgIdColumn . '
        INNER JOIN ref_natures RN ON RN.ID = CB.NATURE_ID
        INNER JOIN titres_ventilations TV ON TV.ID = CB.TITRES_VENTILATIONS_ID
        INNER JOIN titres_pieces TP ON TP.ID = TV.PIECE_ID
        INNER JOIN ref_tiers RT ON RT.ID = TP.TIERS_ID
      WHERE 
        CB.TYPE_COMPTA = \'RECETTE\'
        AND CB.EST_CONSOMMATION = 1
        AND CB.EST_BUDGET = 0
        AND CB.ETABLISSEMENT_ID = :identifier1
        AND RN.ETABLISSEMENT_ID = :identifier2
        AND TV.ETABLISSEMENT_ID = :identifier3
        AND TP.ETABLISSEMENT_ID = :identifier4';
    if ($this->switchCode) {
      $query .= '
        AND RO.NIVEAU_ANALYTIQUE_ORDRE_DE_PRESENTATION = 1';
    }
    if (null !== $this->getFinancialYearsAsString()) {
      $query .= '
        AND CB.EXERCICE IN (' . $this->getFinancialYearsAsString() . ')';
    }
    $query .= '
      GROUP BY TP.NUMERO_PIECE, RO.' . $codeColumn . ';';

    $statement = $this->prepare($query);

    $statement->execute([
      ':identifier1' => $this->identifier,
      ':identifier2' => $this->identifier,
      ':identifier3' => $this->identifier,
      ':identifier4' => $this->identifier,
    ]);

    return $statement->fetchAll(\PDO::FETCH_ASSOC);
  }
}
