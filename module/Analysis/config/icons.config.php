<?php

return [
    'icons' => [
        'analysis' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-pie-chart'
        ],
    ]
];
