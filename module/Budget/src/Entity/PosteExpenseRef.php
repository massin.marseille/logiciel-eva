<?php

namespace Budget\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Class PosteExpense
 *
 * @package Budget\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="budget_poste_expense_ref")
 */
class PosteExpenseRef implements JsonSerializable
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var PosteExpenseRef
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\PosteExpenseRef")
     */
    private $parent;

    /**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(targetEntity="Budget\Entity\PosteExpenseRef", mappedBy="parent", cascade={"remove"})
	 */
	protected $children;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var Nature
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Nature")
     */
    private $nature;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Budget\Entity\Account")
     */
    private $account;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    public function getChildren()
    {
        return null !== $this->children ? $this->children : new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getNature()
    {
        return $this->nature;
    }

    public function setNature($nature) {
        $this->nature = $nature;
    }

    public function getAccount()
    {
        return $this->account;
    }

    public function setAccount($account)
    {
        $this->account = $account;
    }

    public function jsonSerialize() : mixed
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'account' => $this->account,
            'category' => $this->nature,
            'parent' => null !== $this->parent ? $this->parent->getId() : null,
            'children' => $this->children->toArray()
        ];
    }

}
