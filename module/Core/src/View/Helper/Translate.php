<?php

namespace Core\View\Helper;

use Laminas\I18n\Exception;

/**
 * View helper for translating messages.
 */
class Translate extends \Laminas\I18n\View\Helper\Translate
{
    /**
     * @var string
     */
    protected $env;
    
    /**
     * TextDomain
     * @var string
     */
    protected $textDomain;

    /**
     * Locale
     * @var string
     */
    protected $locale;

    /**
     * Not in Html
     * @var bool
     */
    protected $notInHtml;

    public function __construct()
    {
        $this->env = getenv('APP_ENV') ? : 'development';
    }

    /**
     * Translate a message
     *
     * @param  string $message
     * @param  string $textDomain
     * @param  string $locale
     * @param  bool   $notInHtml
     * @throws Exception\RuntimeException
     * @return string
     */
    public function __invoke($message, $textDomain = null, $locale = null, $notInHtml = true)
    {
        $this->textDomain = $textDomain;
        $this->locale = $locale;
        $this->notInHtml = $notInHtml;
        
        $translation = $this->selfTranslate($message);

        $translation = $this->translateCascade($translation);

        /*if ($this->env == 'development' && $notInHtml) {
            $translation = '<debug title="translation : ' . $message . '">' . $translation . '</debug>';
        } elseif ($this->env == 'development') {
            $translation .= ' (translation : ' . $message . ')';
        }*/

        return $translation;
    }

    /**
     * Translate in cascade a text
     * @param $translation
     * @return mixed
     */
    protected function translateCascade($translation)
    {
        $matches = [];
        $nbMatches = preg_match_all('#\[\[.*\]\]#Usi', $translation, $matches);
        if ($nbMatches == 0) {
            return $translation;
        }

        foreach ($matches[0] as $match) {
            $translation = str_replace(
                $match,
                $this->callFunction(preg_replace('#^\[\[(.*)\]\]$#Usi', '$1', $match)),
                $translation
            );
        }

        return $this->translateCascade($translation);
    }

    /**
     * Check if a function must be call (to alter translation).
     * If there is one, use it.
     *
     * @param $param
     * @return string
     */
    protected function callFunction($param)
    {
        $param = trim($param);
        if (preg_match('#(.*)\|(.*)#', $param)) {
            $matches = [];
            preg_match_all('#(.*)\|(.*)#', $param, $matches);

            $tr = $this->selfTranslate(trim($matches[1][0]));
            return $this->useFunction(trim($matches[2][0]), $tr);
        }

        return $this->selfTranslate($param);
    }

    /**
     * Use functions to alter the final translation.
     * If you want to add functions, add it into the
     * switch/case.
     *
     * @param $name
     * @param $string
     * @return mixed
     */
    protected function useFunction($name, $string)
    {
        $string = trim($string);
        switch ($name) {
            case 'ucfirst':
                return mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
            case 'lcfirst':
                return lcfirst($string);
            case 'toUpper':
            case 'toupper':
            case 'upper':
                return strtoupper($string);
            case 'toLower':
            case 'tolower':
            case 'lower':
                return strtolower($string);
            default:
                return $string;
        }
    }

    /**
     * Translate a message.
     *
     * @param $message
     * @return string
     */
    protected function selfTranslate($message)
    {
        return parent::__invoke(
            $message,
            $this->textDomain,
            $this->locale,
            $this->notInHtml
        );
    }
}
