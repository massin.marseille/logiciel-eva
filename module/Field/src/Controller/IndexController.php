<?php

namespace Field\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('field:e:fieldGroup:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel([]);
    }
}
