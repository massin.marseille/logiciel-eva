<?php

namespace Bootstrap\Environment\Plugin;

use Bootstrap\Environment\EnvironmentInterface;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class EntityManagerPlugin
 *
 * Access Doctrine entity Manager from controller easier.
 *
 * @package Bootstrap\Environment\Plugin
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class EntityManagerPlugin extends AbstractPlugin
{
    /**
     * @var EnvironmentInterface
     */
    protected $environment;

    /**
     * @param EnvironmentInterface $environment
     */
    public function __construct(EnvironmentInterface $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @return EnvironmentInterface
     */
    public function __invoke()
    {
        return $this->environment->getEntityManager();
    }
}
