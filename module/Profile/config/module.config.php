<?php

return [
    'module' => [
        'profile' => [
            'environments' => [
                'client'
            ],
            'active'   => false,
            'required' => false,
            'activable' => false,
            'acl' => [
                'entities' => [
                    [
                        'name'     => 'profile',
                        'class'    => 'Profile\Entity\Profile',
                        'keywords' => true,
                        'owner'    => ['own', 'service']
                    ]
                ]
            ],
            'configuration' => []
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
