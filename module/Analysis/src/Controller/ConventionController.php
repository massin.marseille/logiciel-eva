<?php

namespace Analysis\Controller;

use Budget\Entity\Expense;
use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use Laminas\Http\Request;
use Laminas\Mvc\MvcEvent;

class ConventionController extends AbstractActionSLController
{
    public function onDispatch(MvcEvent $e)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);

        return parent::onDispatch($e);
    }

    public function indexAction()
    {

    }

    // Only export
    public function conventionsAction()
    {
        $budgetHTEnabled = $this->serviceLocator->get('MyModuleManager')->getClientModuleConfiguration('budget', 'ht');
        $isMaster = $this->environment()->getClient()->isMaster();
        $filters  = $this->params()->fromQuery('filters', []);

        $cols = [
            'id',
            'name',
            'start',
            'end',
            'amount',
            'amountSubv',
            'progress',
            'lines.project.name',
            'lines.project.stackedAdvancement',
            'contractor.id',
            'contractor.name',
            'territories'
        ];
        foreach (Expense::getTypes() as $type) {
            $cols[] = 'lines.amountExpense' . ucfirst($type);

            if ($budgetHTEnabled) {
                $cols[] = 'lines.amountHTExpense' . ucfirst($type);
            }
        }

        $conventions = $this->apiRequest('Convention\Controller\API\Convention', Request::METHOD_GET, [
            'col'    => $cols,
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort'    => 'name',
                    'order'   => 'asc',
                    'filters' => $filters,
                ],
            ],
            'master' => $isMaster
        ])->dispatch()->getVariable('rows');

        $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
        $translator = $helperPluginManager->get('translate');

        $xlsLines = [];

        $header = [];
        if($isMaster){
            $header[] = ucfirst($translator->__invoke('client'));
            $header[] = ucfirst($translator->__invoke('convention_entities'));
            $header[] = ucfirst($translator->__invoke('convention_field_contractors'));
            $header[] = ucfirst($translator->__invoke('territory_entities'));
        }

        $header[] = ucfirst($translator->__invoke('convention_entity'));
        $header[] = ucfirst($translator->__invoke('convention_field_start'));
        $header[] = ucfirst($translator->__invoke('convention_field_end'));
        $header[] = ucfirst($translator->__invoke('convention_field_contractor'));
        $header[] = ucfirst($translator->__invoke('convention_field_amount'));
        $header[] = ucfirst($translator->__invoke('convention_field_amountSubv'));
        $header[] = ucfirst($translator->__invoke('convention_field_progress'));
        $header[] = ucfirst($translator->__invoke('project_entity'));

        foreach (Expense::getTypes() as $type) {
            $header[] = ucfirst($translator->__invoke('expense_type_' . $type));
            if ($budgetHTEnabled) {
                $header[] = ucfirst($translator->__invoke('expense_type_' . $type . '_ht'));
            }
        }

        $xlsLines[] = $header;

        $clients = [];
        $clientsLines = [];
        foreach ($conventions as $convention) {
            if ($isMaster && !array_key_exists($convention['_client']['id'], $clientsLines)) {
                $clients[$convention['_client']['id']] = [
                    'name' => $convention['_client']['name'],
                    'conventions' => 0,
                    'contractors' => 0,
                    '_contractors' => [],
                    'territories' => 0,
                    '_territories' => []
                ];
                $clientsLines[$convention['_client']['id']] = [];
            }

            $conventionLines = [];

            $conventionLine = [];
            if ($isMaster) {
                $clients[$convention['_client']['id']]['conventions'] += 1;
                if (!in_array($convention['contractor']['id'], $clients[$convention['_client']['id']]['_contractors'])) {
                    $clients[$convention['_client']['id']]['contractors'] += 1;
                    $clients[$convention['_client']['id']]['_contractors'][] = $convention['contractor']['id'];
                }

                foreach ($convention['territories'] as $territory) {
                    if (!in_array($territory['id'], $clients[$convention['_client']['id']]['_territories'])) {
                        $clients[$convention['_client']['id']]['territories'] += 1;
                        $clients[$convention['_client']['id']]['_territories'][] = $territory['id'];
                    }
                }


                // colonne client
                $conventionLine[] = '';
                // colonnes conventions
                $conventionLine[] = '';
                // colonnes contractors
                $conventionLine[] = '';
                // colonnes territories
                $conventionLine[] = sizeOf($convention['territories']);
            }
            $conventionLine[]  = $convention['name'];
            $conventionLine[]  = $convention['start'];
            $conventionLine[]  = $convention['end'];
            $conventionLine[]  = $convention['contractor']['name'];
            $conventionLine[]  = $convention['amount'];
            $conventionLine[]  = $convention['amountSubv'];
            $conventionLine[]  = $convention['progress'];
            $conventionLine[]  = $convention['lines'] ? sizeOf($convention['lines']) : 0;
            $conventionLines[] = $conventionLine;

            if ($convention['lines']) {
                foreach ($convention['lines'] as $line) {
                    $lineLine = [];
                    if ($isMaster) {
                        // colonne client
                        $lineLine[] = '';
                        // colonnes conventions
                        $lineLine[] = '';
                        // colonnes contractors
                        $lineLine[] = '';
                        // colonnes territories
                        $lineLine[] = '';
                    }
                    // colonne convention
                    $lineLine[] = '';
                    // colonne start
                    $lineLine[] = '';
                    // colonne end
                    $lineLine[] = '';
                    // colonne contractor
                    $lineLine[] = '';
                    // colonne amount
                    $lineLine[] = '';
                    // colonne amount subv
                    $lineLine[] = '';
                    // colonne progress
                    $lineLine[] = $line['project']['stackedAdvancement']['done'] . '%';

                    $lineLine[] = $line['project']['name'];

                    // colonne montant poste
                    foreach (Expense::getTypes() as $type) {
                        $lineLine[] = $line['amountExpense' . ucfirst($type)];

                        if ($budgetHTEnabled) {
                            $lineLine[] = $line['amountHTExpense' . ucfirst($type)];
                        }
                    }

                    $conventionLines[] = $lineLine;
                }
            }

            if ($isMaster) {
                $clientsLines[$convention['_client']['id']] = array_merge($clientsLines[$convention['_client']['id']], $conventionLines);
            } else {
                $clientsLines = array_merge($clientsLines, $conventionLines);
            }
        }

        if ($isMaster) {
            foreach($clientsLines as $client => $lines) {
                $xlsLines[] = [$clients[$client]['name'], $clients[$client]['conventions'], $clients[$client]['contractors'], $clients[$client]['territories']];
                $xlsLines = array_merge($xlsLines, $lines);
            }
        } else {
            $xlsLines = array_merge($xlsLines, $clientsLines);
        }

        ExcelExporter::download($xlsLines);
    }
}
