<?php

return [
    'note_entity'   => 'note',
    'note_entities' => 'notes',

    'note_field_name'       => 'Titre',
    'note_field_message'    => 'Message',
    'note_field_attachment' => 'Fichier',

    'note_message_saved'   => '[[ note_entity | ucfirst ]] enregistrée',
    'note_message_deleted' => '[[ note_entity | ucfirst ]] supprimée',
    'note_question_delete' => 'Voulez-vous réellement supprimer la [[ note_entity ]] %1 ?',

    'add_note' => 'Ajouter une [[ note_entity ]]',

    'note_module_title' => 'Notes',
    'note_nav_list'     => 'Liste des {{ __tb.total }} [[ note_entities ]]',

    'group_field_note' => 'Notes'
];
