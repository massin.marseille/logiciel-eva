<?php

namespace Time\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class SettingsTime
 *
 * @package Time\SettingsTime
 *
 * @ORM\Entity
 * @ORM\Table(name="time_settings_time")
 */
class SettingsTime extends BasicRestEntityAbstract {
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    protected $name;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $hours;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * Get the value of hours
     *
     * @return  string
     */ 
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set the value of hours
     *
     * @param  string  $hours
     *
     * @return  self
     */ 
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'hours',
                'required' => true
            ],
        ]);

        return $inputFilter;
    }
}