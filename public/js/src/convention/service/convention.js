(function(app) {

    app.factory('conventionService', [
        'restFactoryService',
        function conventionService(restFactoryService) {
            return restFactoryService.create('/api/convention');
        }
    ])

})(window.EVA.app);
