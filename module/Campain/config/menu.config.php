<?php

return [
    'menu' => [
        'client-menu-parc' => [
                'data' => [
                    'children' => [
                        'indicator'=>[
                            'children' => [
                                'campain-list' => [
                                    'icon'     => 'campain',
                                    'right'    => 'campain:e:campain:r',
                                    'title'    => 'campain_module_title',
                                    'href'     => 'campain',
                                    'priority' => 997
                                ]
                            ]
                        ]
                    ]
                ]
        ]
    ]
];
