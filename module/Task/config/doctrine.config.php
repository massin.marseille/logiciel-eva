<?php

namespace Task;

return [
    'doctrine' => [
        'driver' => [
            'task_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Task\Entity' => 'task_entities'
                ]
            ]
        ]
    ]
];
