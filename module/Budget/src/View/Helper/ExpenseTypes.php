<?php

namespace Budget\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class ExpenseTypes extends AbstractHelper
{
    protected $types;

    public function __construct($types)
    {
        $this->types = $types;
    }

    public function __invoke()
    {
        return $this->types;
    }
}
