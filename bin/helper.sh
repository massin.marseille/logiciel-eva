#!/bin/bash

#
# @author : gael@sigogneau.fr
# @since : 17/03/2021
# @doc : helper script for common unix operation
#        helper.sh WAS NOT updated by pipeline process
#

APP_NAME="eva"
QPILOT_PATH="/var/www/$APP_NAME"
BACKUP_PATH="/home/automysqlbackup"
DATA_PATH="/var/www/$APP_NAME/public/clients"

checkAsRoot() {
	if [[ $EUID -ne 0 ]]; then
	   echo "[ERROR] This script must be run as root"
	   exit 1
	fi
}

#
# Add new FTP access.
# Usage : ./helper.sh addFtp db_name/username data_folder_name
#
addFtp() {
	checkAsRoot
	DB_NAME=$1
	BACKUP_DAILY_FOLDER_PATH=$BACKUP_PATH/daily/$DB_NAME
	DATA_FOLDER=$2
	DATA_FOLDER_PATH=$DATA_PATH/$2
	# password lenght = 12
	PASSWORD=$(date +%s | sha256sum | base64 | head -c 12 ; echo)
	PASSBCRYPT=$(perl -e 'print crypt($ARGV[0], "password")' $PASSWORD)

	echo "[INFO] check mandatory params"
	if [ -z "$DB_NAME" ]
	then
		  echo "[ERROR] DB_NAME is missing. sample usage : ./helper.sh addFtp db_name/username data_folder_name"
		  exit 1
	fi
	if [ -z "$DATA_FOLDER" ]
	then
		  echo "[ERROR] DATA_FOLDER is missing. sample usage : ./helper.sh addFtp db_name/username data_folder_name"
		  exit 1
	fi
	if [ -z "$PASSWORD" ]
	then
		  echo "[ERROR] Clear ASSWORD is empty"
		  exit 1
	fi
	if [ -z "$PASSBCRYPT" ]
	then
		  echo "[ERROR] Encrypted PASSWORD is empty (key: password)"
		  exit 1
	fi

	echo "[INFO] check folders"
	if test -d "$BACKUP_DAILY_FOLDER_PATH"; then
		echo "[INFO] Backups $BACKUP_DAILY_FOLDER_PATH found."
	else
		echo "[ERROR] Backups folder $BACKUP_DAILY_FOLDER_PATH not found"
		exit 1
	fi
	if test -d "$DATA_FOLDER_PATH"; then
		echo "[INFO] Client folder $DATA_FOLDER_PATH found."
	else
		echo "[ERROR] Client folder $DATA_FOLDER_PATH not found"
		exit 1
	fi

	echo "[INFO] check unix user"
	if [ `sed -n "/^$DB_NAME/p" /etc/passwd` ]
	then
		echo "[ERROR] User $DB_NAME already exists"
		exit 1
	else
		echo "[INFO] User $DB_NAME doesn't exist"
	fi

	echo "[INFO] Create user $DB_NAME"
	useradd --home-dir /ftpshare/$DB_NAME --password "$PASSBCRYPT" --shell /bin/false -g ftpgroup "$DB_NAME" \
		&& rm -rf /ftpshare/$DB_NAME/.bash_logout /ftpshare/$DB_NAME/.bashrc /ftpshare/$DB_NAME/.profile \
		&& echo "[INFO] Mount daily folder" \
		&& mkdir -p /ftpshare/$DB_NAME/backup_db_daily \
		&& mount --bind $BACKUP_DAILY_FOLDER_PATH /ftpshare/$DB_NAME/backup_db_daily \
		&& mkdir -p /ftpshare/$DB_NAME/data \
		&& mount --bind $DATA_FOLDER_PATH /ftpshare/$DB_NAME/data \
		&& chmod -R 755 /ftpshare/$DB_NAME \
		|| (echo "[ERROR] Fail to create FTP" && exit 1)

	# service proftpd restart

	echo "=========================="
	echo "HOST : DNS"
	echo "PORT : 21"
	echo "LOGIN : $DB_NAME"
	echo "PASS : $PASSWORD"
	echo "FOLDER : /ftpshare/$DB_NAME"
	echo "=========================="

	echo "[END] FTP created"
}

#
# Add new DNS on qpilot app.
# Usage : ./helper.sh addDns mynewsubdomaine.qpilot.fr
#
addDns() {
	checkAsRoot
	DNS_NAME=$1
	if [ -z "$DNS_NAME" ]
	then
		  echo "[ERROR] DNS is missing. sample usage : ./helper.sh addDns mynewsubdomaine.qpilot.fr"
		  exit 1
	fi

	if ping -c 1 $DNS_NAME &> /dev/null
	then
	  echo "[INFO] DNS $DNS_NAME have some ping response"
	else
	  echo "[ERROR] DNS $DNS_NAME dont PING"
	  exit 1
	fi

	# Create TEMP CONFIGURATION FOR GENERATE SSL CERTIFICAT
	echo "[INFO] Create new VHOST for $DNS_NAME"
	CONFIG_FILE="/etc/apache2/sites-available/$DNS_NAME.conf"
    touch $CONFIG_FILE
cat <<- EOF > $CONFIG_FILE
<VirtualHost *:80>
	DocumentRoot "$QPILOT_PATH/public"
	ServerName $DNS_NAME

	<Directory "$QPILOT_PATH/public">
		Options -Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride all
		Require all granted
	</Directory>

</VirtualHost>
EOF

	echo "[INFO] Enable VHOST $DNS_NAME"
	a2ensite $DNS_NAME

	echo "[INFO] Reload apache configuration"
	systemctl reload apache2

	echo "[INFO] Create new SSL certificat for $DNS_NAME"
	certbot certonly --webroot -w ${QPILOT_PATH}/public -d $DNS_NAME || exit 1

	echo "[INFO] Create final configuration"
	CONFIG_FILE="/etc/apache2/sites-available/$DNS_NAME.conf"
	echo "" > $CONFIG_FILE
    touch $CONFIG_FILE
cat <<- EOF > $CONFIG_FILE
<VirtualHost *:80>
	DocumentRoot "$QPILOT_PATH/public"
	ServerName $DNS_NAME

	<Directory $QPILOT_PATH/public>
	    Options -Indexes +Includes +FollowSymLinks +MultiViews
        AllowOverride all
        Require all granted
    </Directory>

	RewriteEngine on
	RewriteCond %{SERVER_NAME} =$DNS_NAME
	RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>

EOF

	CONFIG_FILE="/etc/apache2/sites-available/$DNS_NAME-le-ssl.conf"
	echo "" > $CONFIG_FILE
    touch $CONFIG_FILE
cat <<- EOF > $CONFIG_FILE
<IfModule mod_ssl.c>
	<VirtualHost *:443>
		DocumentRoot "$QPILOT_PATH/public"
		ServerName $DNS_NAME

		<Directory "$QPILOT_PATH/public">
			Options -Indexes +Includes +FollowSymLinks +MultiViews
			AllowOverride all
			Require all granted
		</Directory>

		RewriteEngine on

		Include /etc/letsencrypt/options-ssl-apache.conf
		SSLCertificateFile /etc/letsencrypt/live/$DNS_NAME/fullchain.pem
		SSLCertificateKeyFile /etc/letsencrypt/live/$DNS_NAME/privkey.pem
	</VirtualHost>
</IfModule>

EOF

	echo "[INFO] enable ssl $DNS_NAME-le-ssl"
	a2ensite $DNS_NAME-le-ssl

	echo "[INFO] restart apache"
	service apache2 restart

	# echo "[INFO] apache  status"
	# systemctl status apache2

	echo "[END] $DNS_NAME is deploy "
	exit 0
}

#
# Remove DNS on qpilot app.
# Usage : ./helper.sh removeDns mynewsubdomaine.qpilot.fr
#
removeDns() {
	checkAsRoot
	DNS_NAME=$1
	CONFIG_FILE=/etc/apache2/sites-available/$DNS_NAME

	if test -f "$CONFIG_FILE.conf"; then
		echo "[INFO] Configuration $CONFIG_FILE found."
	else
		echo "[ERROR] Configuration $CONFIG_FILE.conf dont exist"
	fi

	echo "[INFO] Remove VHOST configuration"
	a2dissite $DNS_NAME
	rm -rf $CONFIG_FILE.conf

	if test -f "$CONFIG_FILE-le-ssl.conf"; then
		echo "[INFO] SSL certificat is found. Uninstall..."
		a2dissite $DNS_NAME-le-ssl
		rm -rf $CONFIG_FILE-le-ssl.conf
		certbot --non-interactive delete --cert-name $DNS_NAME
	fi

	echo "[INFO] restart apache"
	service apache2 restart

	echo "[END] $DNS_NAME is removed "
	exit 0
}

echo "[START] EntryPoint :: $@ "
"$@"
