<?php

namespace Directory\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class StructureFilters extends EntityFilters
{

    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'id' => [
                'label' => $this->translator->__invoke('structure_field_id'),
                'type'  => 'string'
            ],
            'name' => [
                'label' => $this->translator->__invoke('structure_field_name'),
                'type'  => 'string'
            ],
            'sigle' => [
                'label' => $this->translator->__invoke('structure_field_sigle'),
                'type'  => 'string'
            ],
            'email' => [
                'label'    => $this->translator->__invoke('structure_field_email'),
                'type'     => 'string',
            ],
            'phone' => [
                'label'    => $this->translator->__invoke('structure_field_phone'),
                'type'     => 'string',
            ],
            'phone2' => [
                'label'    => $this->translator->__invoke('structure_field_phone2'),
                'type'     => 'string',
            ],
            'zipcode' => [
                'label'    => $this->translator->__invoke('structure_field_zipcode'),
                'type'     => 'string',
            ],
            'country' => [
                'label'    => $this->translator->__invoke('structure_field_country'),
                'type'     => 'string',
            ],
            'city' => [
                'label'    => $this->translator->__invoke('structure_field_city'),
                'type'     => 'string',
            ],
            'addressLine1' => [
                'label'    => $this->translator->__invoke('structure_field_addressLine1'),
                'type'     => 'string',
            ],
            'addressLine2' => [
                'label'    => $this->translator->__invoke('structure_field_addressLine2'),
                'type'     => 'string',
            ],
            'createdAt' => [
                'label' => $this->translator->__invoke('structure_field_createdAt'),
                'type'  => 'date'
            ],
            'updatedAt' => [
                'label' => $this->translator->__invoke('structure_field_updatedAt'),
                'type'  => 'date'
            ],
            'job' => [
                'label'    => $this->translator->__invoke('contact_field_job'),
                'type'     => 'job-select',
            ],
            'contact' => [
                'label'    => $this->translator->__invoke('job_field_contact'),
                'type'     => 'contact-select',
            ],
            'siret' => [
                'label' => $this->translator->__invoke('structure_field_siret'),
                'type'  => 'string'
            ],
            'website' => [
                'label' => $this->translator->__invoke('structure_field_website'),
                'type'  => 'string'
            ],
        ];

        if ($this->moduleManager->isActive('budget')) {
            $filters['financer'] = [
                'label'    => $this->translator->__invoke('structure_field_financer'),
                'type'     => 'boolean',
            ];
        }

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('structure', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type'  => 'keyword-select',
                    'group' => $group->getId()
                ];
            }
        }

        $this->filters = $filters;
    }
}
