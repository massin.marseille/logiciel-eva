(function(app) {
    app.factory('natureService', [
        'restFactoryService',
        function (restFactoryService) {
            return restFactoryService.create('/api/budget/nature');
        },
    ]);
})(window.EVA.app);
