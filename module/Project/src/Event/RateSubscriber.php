<?php

namespace Project\Event;

use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\UnitOfWork;
use Project\Entity\Project;
use Laminas\ServiceManager\ServiceLocatorInterface;

class RateSubscriber implements EventSubscriber
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected $projects = [];

    protected $refreshedProjects_id = [];

    /**
     * RateSubscriber constructor.
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::onFlush,
            Events::postFlush,
        ];
    }

    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof PosteExpense || $entity instanceof PosteIncome) {
                $this->projects[] = $entity->getProject();

                continue;
            }

            if ($entity instanceof Expense) {
                if (in_array($entity->getType(), [Expense::TYPE_PAID])) {
                    $this->projects[] = $entity->getProject();
                }

                continue;
            }

            if ($entity instanceof Income) {
                if (in_array($entity->getType(), [Income::TYPE_PERCEIVED])) {
                    $this->projects[] = $entity->getProject();
                }

                continue;
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Project) {
                $changes = $uow->getEntityChangeSet($entity);

                if (isset($changes['parent'])) {
                    if ($changes['parent'][0]) {
                        $this->projects[] = $changes['parent'][0];
                    }

                    if ($changes['parent'][1]) {
                        $this->projects[] = $changes['parent'][1];
                    }
                }

                continue;
            }

            if ($entity instanceof PosteExpense || $entity instanceof PosteIncome) {
                $changes = $uow->getEntityChangeSet($entity);

                if (isset($changes['amount'])) {
                    $this->projects[] = $entity->getProject();
                }

                continue;
            }

            if ($entity instanceof Expense) {
                $changes = $uow->getEntityChangeSet($entity);

                if (
                    in_array($entity->getType(), [Expense::TYPE_PAID]) ||
                    (isset($changes['type']) && in_array($changes['type'][0], [Expense::TYPE_PAID]))
                ) {
                    if (
                        isset($changes['amount']) ||
                        isset($changes['type']) ||
                        isset($changes['poste'])
                    ) {
                        $this->projects[] = $entity->getProject();
                    }
                }

                continue;
            }

            if ($entity instanceof Income) {
                $changes = $uow->getEntityChangeSet($entity);

                if (
                    in_array($entity->getType(), [Income::TYPE_PERCEIVED]) ||
                    (isset($changes['type']) && in_array($changes['type'][0], [Income::TYPE_PERCEIVED]))
                ) {
                    if (
                        isset($changes['amount']) ||
                        isset($changes['type']) ||
                        isset($changes['poste'])
                    ) {
                        $this->projects[] = $entity->getProject();
                    }
                }

                continue;
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof Project) {
                if ($entity->getParent()) {
                    $this->projects[] = $entity->getParent();
                }

                continue;
            }

            if ($entity instanceof PosteExpense || $entity instanceof PosteIncome) {
                $this->projects[] = $entity->getProject();

                continue;
            }

            if ($entity instanceof Expense) {
                if (in_array($entity->getType(), [Expense::TYPE_PAID])) {
                    $this->projects[] = $entity->getProject();
                }

                continue;
            }

            if ($entity instanceof Income) {
                if (in_array($entity->getType(), [Income::TYPE_PERCEIVED])) {
                    $this->projects[] = $entity->getProject();
                }

                continue;
            }
        }
    }

    /**
     * @param PostFlushEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if ($this->projects) {
            $em  = $args->getEntityManager();
            $uow = $em->getUnitOfWork();

            foreach ($this->projects as $project) {
                $em->refresh($project);
                $this->refreshRates($project, $uow, $em);
            }

            $this->projects = [];
            $this->refreshedProjects_id = [];

            $em->flush();
        }
    }

    /**
     * @param Project  $project
     * @param UnitOfWork        $uow
     * @param EntityManager        $em
     */
    protected function refreshRates($project, $uow, $em)
    {
        if (in_array($project->getId(), $this->refreshedProjects_id)) {
            return;
        }

        $this->refreshedProjects_id[] = $project->getId();

        $project->setRateExpenseRealized();
        $project->setRateIncomeRealized();

        $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($project)), $project);

        if ($project->getParent()) {
            $this->refreshRates($project->getParent(), $uow, $em);
        }
    }
}
