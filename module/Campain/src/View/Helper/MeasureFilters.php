<?php

namespace Campain\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class MeasureFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'realiseMeasure.indicator' => [
                'label' => $this->translator->__invoke('measure_field_indicator'),
                'type'  => 'indicator-select'
            ],
            'realiseMeasure.groupIndicator' => [
                'label' => $this->translator->__invoke('measure_field_groupIndicator'),
                'type'  => 'group-indicator-select'
            ],
            'realiseMeasure.project' => [
                'label' => $this->translator->__invoke('measure_field_project'),
                'type'  => 'project-select'
            ],
            'realiseMeasure.value' => [
                'label' => $this->translator->__invoke('measure_type_done'),
                'type'  => 'number'
            ],
            'user.id' => [
                'label' => $this->translator->__invoke('campain_field_referent'),
                'type'  => 'user-select'
            ],
            'targetMeasure.value' => [
                'label' => $this->translator->__invoke('measure_type_target'),
                'type'  => 'number'
            ],
            'ratio' => [
                'label' => $this->translator->__invoke('measure_ratio'),
                'type'  => 'number'
            ],
            'supposedDate' => [
                'label' => $this->translator->__invoke('measure_campain_field_supposed_date'),
                'type'  => 'date'
            ],
            'targetMeasure.start' => [
                'label' => $this->translator->__invoke('measure_field_start'),
                'type'  => 'date'
            ],
            'targetMeasure.date' => [
                'label' => $this->translator->__invoke('measure_field_date'),
                'type'  => 'date'
            ],
            'targetMeasure.updatedAt' => [
                'label' => $this->translator->__invoke('measure_field_updatedAt'),
                'type'  => 'date'
            ]
            
        ];

        if ($this->moduleManager->isActive('map')) {
            $filters['territories'] = [
                'label' => ucfirst($this->translator->__invoke('territory_entities')),
                'type'  => 'territory-select',
            ];
        }

        $this->filters = $filters;
    }
}
