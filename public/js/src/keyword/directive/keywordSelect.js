(function(app) {

    app.directive('keywordSelect', [
        'keywordService', 'selectFactoryService',
        function keywordSelect(keywordService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: keywordService,
                    apiParams: {
                        col: ['id', 'name', 'description', 'parents.id', 'order'],
                        search : {
                            data : {
                                sort: 'name',
                                order: 'asc'
                            }
                        }
                    },
                    tree: true,
                    nnTree: true
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name', 'group.name'],
                    render: {
                        item: function (data, escape) {
                            var tooltip = '';
                            if (data.description) {
                                tooltip = ' tooltip="' + escape(data.description) + '" ';
                            }
                            return '' +
                            '<div class="option">' +
                                '<span class="name"' + tooltip + '>' +
                                    (data.group ? escape(data.group.name) + ' - ' : '' ) + escape(data.name) +
                                '</span>' +
                            '</div>';
                        }
                    }
                }
            });
        }
    ])

})(window.EVA.app);
