(function(app) {

    app.factory('posteIncomeService', [
        'restFactoryService',
        function posteIncomeService(restFactoryService) {
            return restFactoryService.create('/api/budget/poste-income');
        }
    ])

})(window.EVA.app);
