<?php

namespace Budget\Export;

use Core\Export\IExporter;

abstract class Envelope implements IExporter
{
    public static function getConfig()
    {
        return [
            'financer.name' => [
                'name' => 'financer',
            ],
            'amount' => [
                'format' => function ($value) {
                    if (0 != $value) {
                        return $value . '€';
                    }
                    return $value;
                }
            ]
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
