(function(app) {
	app.controller('natureListController', [
		'natureService',
		'$scope',
		'flashMessenger',
		'translator',
		function(natureService, $scope, flashMessenger, translator) {
			var self = this;

			self.natures = [];
			self.options = {
				name: 'natures-list',
				col: [
					'id',
					'code',
					'name',
					'parent.id',
					'parent.code',
					'parent.name',
					'createdAt',
					'updatedAt',
				],
				searchType: 'list',
				sort: 'name',
				order: 'asc',
				getItems: function(params, callback, abort) {
					natureService.findAll(params, abort.promise).then(function(res) {
						callback(res);
					});
				},
			};

			self.fullTextSearch = function() {
				$scope.__tb.resetFilters();
				$scope.__tb.apiParams.search.data.full = self.fullText;
				$scope.__tb.loadData();
			};

			self.resetfullTextSearch = function() {
				self.fullText = '';
				delete $scope.__tb.apiParams.search.data.full;
			};

			angular.element('#full-text').on('keypress', function(event) {
				if (event.which == 13) {
					self.fullTextSearch();
				}
			});

			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};
			self.editLine = function(nature) {
				if (angular.isArray(nature.keywords) || nature.keywords == null) {
					nature.keywords = {};
				}

				self.editedLineErrors = {};
				self.editedLine = nature;
				self.editedLineFields = angular.copy(nature);
			};

			self.saveEditedLine = function(nature) {
				self.editedLineErrors = {};
				natureService
					.save(angular.extend({}, nature, self.editedLineFields), {
						col: self.options.col,
					})
					.then(
						function(res) {
							if (res.success) {
								nature = angular.extend(nature, res.object);
								self.editedLine = null;
								self.editedLineFields = null;

								flashMessenger.success(translator('nature_message_saved'));
							}
						},
						function(err) {
							for (var field in err.fields) {
								self.editedLineErrors[field] = err.fields[field];
							}
						},
					);
			};

			self.deleteNature = function(nature) {
				if (confirm(translator('nature_question_delete').replace('%1', nature.name))) {
					natureService.remove(nature).then(function(res) {
						if (res.success) {
							self.natures.splice(self.natures.indexOf(nature), 1);
							flashMessenger.success(translator('nature_message_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				}
			};
		},
	]);
})(window.EVA.app);
