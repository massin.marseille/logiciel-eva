(function(app) {

    app.controller('deploymentController', [
        'deploymentService', 'groupService', 'flashMessenger', 'translator',
        function deploymentController(deploymentService, groupService, flashMessenger, translator) {
            var self = this;

            self.from = {
                id: null
            };

            self.groups = [];

            self.init = function init(id) {
                self.from.id = id;

                groupService.findAll({
                    col: [
                        'id',
                        'name',
                        'maxLevel'
                    ],
                    search: {
                        type: 'list',
                        data: {
                            sort   : 'name',
                            order  : 'asc',
                            filters: {
                                type: {
                                    op : 'eq',
                                    val: 'reference'
                                }
                            }
                        }
                    }
                }).then(function(res) {
                    self.groups = res.rows;
                });

                if (id) {
                    deploymentService.findAll({
                        col: [
                            'id',
                            'indicator.id',
                            'group.id',
                            'level'
                        ],
                        search: {
                            type: 'list',
                            data: {
                                filters: {
                                    indicator: {
                                        op : 'eq',
                                        val: id
                                    }
                                }
                            }
                        }
                    }).then(function(res) {
                        self.deployments = res.rows;
                    });
                }
            };

            self.asArray = function asArray(number) {
                return new Array(number+1);
            };

            self.toggleDeployment = function toggleDeployment(group, level) {
                for (var i in self.deployments) {
                    var deployment = self.deployments[i];
                    if (deployment.group.id === group && deployment.level === level) {
                        deploymentService.remove(deployment).then(function(res) {
                            if (res.success) {
                                self.deployments.splice(i, 1);
                                flashMessenger.success(translator('deployment_message_saved'));
                            }
                        });
                        return;
                    }
                }

                deploymentService.save({
                    indicator: self.from.id,
                    group: group,
                    level: level
                },{
                    col: [
                        'id',
                        'indicator.id',
                        'group.id',
                        'level'
                    ]
                }).then(function(res) {
                    if (res.success) {
                        self.deployments.push(res.object);
                        flashMessenger.success(translator('deployment_message_saved'));
                    }
                });
            };

            self.isChecked = function(group, level) {
                for (var i in self.deployments) {
                    var deployment = self.deployments[i];
                    if (deployment.group.id === group && deployment.level === level) {
                        return true;
                    }
                }

                return false;
            }
        }
    ])

})(window.EVA.app);
