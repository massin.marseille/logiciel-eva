(function(app) {

    app.directive('groupSelect', [
        'groupService', 'selectFactoryService',
        function (groupService, selectFactoryService) {
            return selectFactoryService.create({
                service: {
                    object: groupService,
                    apiParams: {
                        col: ['id', 'name']
                    },
                    full: true
                },
                selectize: {
                    searchField: ['name'],
                    valueField: 'id',
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' + escape(data.name) + '</div>';
                        },
                        item: function (data, escape) {
                            return '<div class="option">' + escape(data.name) + '</div>';
                        }
                    }
                }
            });
        }
    ])

})(window.EVA.app);
