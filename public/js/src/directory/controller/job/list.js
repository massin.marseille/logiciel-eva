(function(app) {

    app.controller('jobListController', [
        'jobService', '$scope', 'flashMessenger', 'translator',
        function jobListController(jobService, $scope, flashMessenger, translator) {
            var self = this;

            self.view = 'list';
            self.fusion = {};
            self.loading = {};

            self.jobs = [];

            self.indicators = [];
            self.options = {
                name: 'jobs-list',
                col: [
                    'id',
                    'name',
                    'keywords',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    jobService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(job) {

                if (angular.isArray(job.keywords) || job.keywords == null) {
                    job.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = job;
                self.editedLineFields = angular.copy(job);
            };

            self.saveEditedLine = function saveEditedLine(job) {
                self.editedLineErrors = {};
                jobService.save(angular.extend({}, job, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        job = angular.extend(job, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('job_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteJob = function deleteJob(job) {
                if (confirm(translator('job_question_delete').replace('%1', job.name))) {
                    jobService.remove(job).then(function (res) {
                        if (res.success) {
                            self.jobs.splice(self.jobs.indexOf(job), 1);
                            flashMessenger.success(translator('job_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.showFusionModal = function () {
                angular.element('#fusionModal').modal('show');
            };

            self.doFusion = function () {
                if (!self.fusion.jobToKeep) {
                    flashMessenger.error(translator('error_occured'));
                    return;
                }

                self.loading.fusion = true;

                var jobs_id = [];
                for (var i in self.jobs) {
                    if (self.jobs[i].selected && self.jobs[i].id !== self.fusion.jobToKeep.id) {
                        jobs_id.push(self.jobs[i].id);
                    }
                }

                if (jobs_id.length === 0) {
                    flashMessenger.success(translator('fusion_message_success'));
                    return;
                }

                jobService
                    .fusion(self.fusion.jobToKeep.id, jobs_id)
                    .then(function (res) {
                        self.loading.fusion = false;
                        $scope.__tb.loadData();
                        angular.element('#fusionModal').modal('hide');

                        if (res.success) {
                            flashMessenger.success(translator('fusion_message_success'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
            };
        }
    ])

})(window.EVA.app);
