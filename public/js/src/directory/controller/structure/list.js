(function (app) {

    app.controller('structureListController', [
        'structureService', '$scope', 'flashMessenger', 'translator', '$http',
        function structureListController(structureService, $scope, flashMessenger, translator, $http) {
            var self = this;

            self.loading = {
                fusion: false
            };

            self.fusion = {
                type: 'duplicate'
            };

            self.structures = [];
            self.options = {
                name: 'structures-list',
                col: [
                    'id',
                    'name',
                    'sigle',
                    'email',
                    'phone',
                    'phone2',
                    'portable',
                    'zipcode',
                    'city',
                    'country',
                    'addressLine1',
                    'addressLine2',
                    'createdAt',
                    'updatedAt',
                    'financer',
                    'keywords',
                    'siret',
                    'website'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    structureService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function (event) {
                if (event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(structure) {
                if (angular.isArray(structure.keywords) || structure.keywords == null) {
                    structure.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine = structure;
                self.editedLineFields = angular.copy(structure);
            };

            self.saveEditedLine = function saveEditedLine(structure) {
                self.editedLineErrors = {};
                structureService.save(angular.extend({}, structure, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        structure = angular.extend(structure, res.object);
                        self.editedLine = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('structure_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.duplicate = function duplicate(structure) {
                $http({
                    method: 'POST',
                    url: '/directory/structure/duplicate/' + structure.id
                }).then(function (res) {
                    if (res.data.success) {
                        window.location = '/directory/structure/form/' + res.data.structure.id;
                    } else {
                        for (var i in res.data.error) {
                            flashMessenger.error(res.data.error[i]);
                        }
                    }
                });
            };

            self.deleteStructure = function deleteStructure(structure) {
                if (confirm(translator('structure_question_delete').replace('%1', structure.name))) {
                    structureService.remove(structure).then(function (res) {
                        if (res.success) {
                            self.structures.splice(self.structures.indexOf(structure), 1);
                            flashMessenger.success(translator('structure_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.showFusionModal = function (structureToFusion) {
                angular.element('#fusionModal').modal('show');
                self.fusion.structureToFusion = structureToFusion;
            };

            self.doFusion = function doFusion() {
                if (
                    !self.fusion.structureToKeep ||
                    !self.fusion.structureToFusion ||
                    !self.fusion.type
                ) {
                    flashMessenger.error(translator('error_occured'));
                    return;
                }

                self.loading.fusion = true;

                structureService
                    .fusion(self.fusion.structureToKeep.id, self.fusion.structureToFusion.id, self.fusion.type)
                    .then(function (res) {
                        self.loading.fusion = false;
                        angular.element('#fusionModal').modal('hide');

                        if (res.success) {
                            if (self.fusion.type === 'transfer') {
                                self.structures.splice(self.structures.indexOf(self.fusion.structureToFusion), 1);
                                flashMessenger.success(translator('structure_message_deleted'));
                            }
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
            };
        }
    ])

})(window.EVA.app);
