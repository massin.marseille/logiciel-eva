(function(app) {
    app.directive('managementUnitSelect', [
        'managementUnitService',
        'selectFactoryService',
        function(managementUnitService, selectFactoryService) {
            return selectFactoryService.create({
                service: {
                    object: managementUnitService,
                    apiParams: {
                        col: ['id', 'code', 'name', 'parent.id'],
                    },
                    full: true,
                    tree: true,
                    treeOrder: 'code',
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['code', 'name'],
                    render: {
                        option: (data, escape) => `
                            <div class="option">
                                ${escape(data.code)} - ${escape(data.name)}
                            </div>
                        `,
                        item: (data, escape) => `
                            <div class="option">
                                ${escape(data.code)} - ${escape(data.name)}
                            </div>
                        `,
                    },
                },
            });
        },
    ]);
})(window.EVA.app);
