<?php

namespace Map\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class TerritoryController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('map:e:territory:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('territory', false);
        }

        return new ViewModel([
            'groups'  => $groups,
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('map:e:territory', $id)) {
            return $this->notFoundAction();
        }

        $territory = $this->entityManager()->getRepository('Map\Entity\Territory')->find($id);

        if ($territory && !$this->acl()->ownerControl('map:e:territory:u', $territory) && !$this->acl()->ownerControl('map:e:territory:r', $territory)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'territory' => $territory
        ]);
    }

    public function importAction()
    {
        if (!$this->acl()->isAllowed('map:e:territory:c')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }
}
