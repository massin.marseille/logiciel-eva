<?php

namespace Directory\Controller\API;

use Core\Controller\BasicRestController;
use Directory\Entity\ContactJobStructure;
use Directory\Entity\Structure;
use Doctrine\ORM\QueryBuilder;
use Keyword\Entity\Association;
use Project\Entity\Actor;
use Laminas\View\Model\JsonModel;

class StructureController extends BasicRestController
{
    protected $entityClass  = 'Directory\Entity\Structure';
    protected $repository   = 'Directory\Entity\Structure';
    protected $entityChain  = 'directory:e:structure';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        //$queryBuilder->orderBy('LENGTH(' . $sort .')', $order);
        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                    'object.sigle LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }

        if (isset($data['filters'])) {
            foreach ($data['filters'] as $property => $filter) {
                if ($property == 'contact' && isset($filter['op'])) {
                    $operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
                    $queryBuilder->andWhere('
                        object.id ' . $operator . ' (SELECT s1.id FROM Directory\Entity\ContactJobStructure cjs1 JOIN cjs1.contact c1 JOIN cjs1.structure s1 WHERE c1.id IN (:contact) )
                    ');
                    if (is_array($filter['val'])) {
                        $queryBuilder->setParameter('contact', $filter['val'], 'simple_array');
                    } else {
                        $queryBuilder->setParameter('contact', $filter['val']);
                    }
                    unset($data['filters'][$property]);
                    continue;
                }
                if ($property == 'job' && isset($filter['op'])) {
                    $operator = $filter['op'] == 'eq' ? 'IN' : 'NOT IN';
                    $queryBuilder->andWhere('
                        object.id ' . $operator . ' (SELECT s1.id FROM Directory\Entity\ContactJobStructure cjs1 JOIN cjs1.job j1 JOIN cjs1.structure s1 WHERE j1.id IN (:job) )
                    ');
                    if (is_array($filter['val'])) {
                        $queryBuilder->setParameter('job', $filter['val'], 'simple_array');
                    } else {
                        $queryBuilder->setParameter('job', $filter['val']);
                    }
                    unset($data['filters'][$property]);
                    continue;
                }
            }
        }
    }

    public function fusionAction()
    {
        $structureToKeep_id   = $this->params()->fromRoute('id');
        $structureToFusion_id = $this->params()->fromPost('structureToFusion_id', null);
        $type               = $this->params()->fromPost('type', null);

        if (
            !$structureToFusion_id ||
            $structureToKeep_id == $structureToFusion_id ||
            !$this->acl()->isAllowed('directory:e:structure:d') ||
            !in_array($type, ['duplicate', 'transfer'])
        ) {
            echo $structureToKeep_id;
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Structure $structureToKeep */
        $structureToKeep = $this->getEntityManager()
            ->getRepository('Directory\Entity\Structure')
            ->find($structureToKeep_id);

        $structureToFusion = $this->getEntityManager()
            ->getRepository('Directory\Entity\Structure')
            ->find($structureToFusion_id);

        if (!$structureToKeep) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Association[] $associationsToKeep */
        $associationsToKeep = $this->getEntityManager()
            ->getRepository('Keyword\Entity\Association')
            ->findBy(['primary' => $structureToKeep_id]);

        /** @var Association[] $associationsToChange */
        $associationsToChange = $this->getEntityManager()
            ->getRepository('Keyword\Entity\Association')
            ->findBy(['primary' => $structureToFusion_id]);

        foreach ($associationsToChange as $association) {
            foreach ($associationsToKeep as $associationToKeep) {
                if (
                    $associationToKeep->getEntity() === $association->getEntity() &&
                    $associationToKeep->getPrimary() === $association->getPrimary()
                ) {
                    if ($type === 'transfer') {
                        $this->getEntityManager()->remove($association);
                    }
                    continue 2;
                }
            }

            $isAssociationExist = $this->getEntityManager()
                ->getRepository('Keyword\Entity\Association')
                ->findOneBy(['primary' => $structureToKeep_id, 'entity' => $association->getEntity(), 'keyword' => $association->getKeyword()]);

            if (!isset($isAssociationExist)) {
                if ($type === 'transfer') {
                    $association->setPrimary($structureToKeep_id);
                    $this->getEntityManager()->persist($association);
                } else {
                    $newAssociation = new Association();
                    $newAssociation->setEntity($association->getEntity());
                    $newAssociation->setPrimary($structureToKeep_id);
                    $newAssociation->setKeyword($association->getKeyword());
                    $this->getEntityManager()->persist($newAssociation);
                }
            }
        }

        /** @var ContactJobStructure[] $associationsToKeep */
        $jobAssociationsToKeep = $this->getEntityManager()
            ->getRepository('Directory\Entity\ContactJobStructure')
            ->findBy(['structure' => $structureToKeep_id]);

        /** @var ContactJobStructure[] $associationsToChange */
        $jobAssociationsToChange = $this->getEntityManager()
            ->getRepository('Directory\Entity\ContactJobStructure')
            ->findBy(['structure' => $structureToFusion_id]);

        foreach ($jobAssociationsToChange as $association) {
            foreach ($jobAssociationsToKeep as $associationToKeep) {
                if (
                    $associationToKeep->getStructure()->getId() === $association->getStructure()->getId() &&
                    $associationToKeep->getContact()->getId() === $association->getContact()->getId()
                ) {
                    if ($type === 'transfer') {
                        $this->getEntityManager()->remove($association);
                    }
                    continue 2;
                }
            }

            if ($type === 'transfer') {
                $association->setStructure($structureToKeep);
                $this->getEntityManager()->persist($association);
                $this->getEntityManager()->flush();
            } else {
                $newAssociation = new ContactJobStructure();
                $newAssociation->setContact($association->getContact());
                $newAssociation->setJob($association->getJob());
                $newAssociation->setStructure($structureToKeep);
                $this->getEntityManager()->persist($newAssociation);
            }
        }

        /** @var Actor[] $actorsToKeep */
        $actorsToKeep = $this->getEntityManager()
            ->getRepository('Project\Entity\Actor')
            ->findBy(['structure' => $structureToKeep_id]);

        /** @var Actor[] $actorsToChange */
        $actorsToChange = $this->getEntityManager()
            ->getRepository('Project\Entity\Actor')
            ->findBy(['structure' => $structureToFusion_id]);

        foreach ($actorsToChange as $association) {
            foreach ($actorsToKeep as $associationToKeep) {
                if (
                    $associationToKeep->getProject()->getId() === $association->getProject()->getId() &&
                    $associationToKeep->getStructure()->getId() === $association->getStructure()->getId()
                ) {
                    if ($type === 'transfer') {
                        $this->getEntityManager()->remove($association);
                    }
                    continue 2;
                }
            }

            if ($type === 'transfer') {
                $association->setStructure($structureToKeep);
                $this->getEntityManager()->persist($association);
            } else {
                $newAssociation = new Actor();
                $newAssociation->setStructure($structureToKeep);
                $newAssociation->setProject($association->getProject());
                $this->getEntityManager()->persist($newAssociation);
            }
        }

        if ($type === 'transfer') {
            /** @var Actor[] $actorsToKeep */
            $envelopes = $this->getEntityManager()
                ->getRepository('Budget\Entity\Envelope')
                ->findBy(['financer' => $structureToFusion_id]);

            foreach ($envelopes as $envelope) {
                $envelope->setFinancer($structureToKeep);
                $this->getEntityManager()->persist($envelope);
            }
        }

        if ($type === 'transfer') {
            /** @var Actor[] $actorsToKeep */
            $conventions = $this->getEntityManager()
                ->getRepository('Convention\Entity\Convention')
                ->findBy(['contractor' => $structureToFusion_id]);

            foreach ($conventions as $convention) {
                $convention->setContractor($structureToKeep);
                $this->getEntityManager()->persist($convention);
            }
        }

        if ($type === 'transfer') {
            $this->getEntityManager()->remove($structureToFusion);
        }

        $this->getEntityManager()->flush();

        return new JsonModel([
            'success' => true,
        ]);
    }
}
