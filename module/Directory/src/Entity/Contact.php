<?php

namespace Directory\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\Filter\Boolean;

/**
 * Class Contact manages a contact of a directory
 *
 * @category Directory
 * @package EVA
 * @since Version 4
 * @version 4
 * @author Curtis Pelissier <curtis.pelissier@lignusdev.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="directory_contact")
 */
class Contact extends AbstractPerson
{
    const SUBSCRIPTION_STATUS_EMPTY = 'empty';
    const SUBSCRIPTION_STATUS_ACCEPTED = 'accepted';
    const SUBSCRIPTION_STATUS_DECLINED = 'declined';

    /**
     * Unique ID
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $uuid;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $vip;

    /**
     * @var string
     * @ORM\Column(type="string",  nullable=true, options={"default": "empty"})
     */
    protected $subscriptionStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $subscriptionStatusUpdatedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $avatar = '/img/default-avatar.png';

    /**
     * Contact Job(s) in Structure(s)
     * @var ArrayCollection|ContactJobStructure[]
     * @ORM\OneToMany(targetEntity="Directory\Entity\ContactJobStructure", mappedBy="contact", orphanRemoval=true, cascade={"all"})
     */
    protected $links;

    /**
     * @var ArrayCollection|ContactEmail[]
     * @ORM\OneToMany(targetEntity="ContactEmail", mappedBy="contact", cascade={"all"})
     */
    protected $contactEmails;

    /**
     * PostParc ID
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $postParcId;

    /**
     * Constructor of the class
     *
     * @since Version 4
     * @version 4
     */
    public function __construct()
    {
        $this->links = new ArrayCollection();
        $this->contactEmails = new ArrayCollection();
    }

    /**
     * Gets the Unique ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Unique ID.
     *
     * @param int $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return bool
     */
    public function isVip()
    {
        return $this->vip;
    }

    /**
     * @return bool
     */
    public function getVip()
    {
        return $this->vip;
    }

    /**
     * @param bool $vip
     */
    public function setVip($vip)
    {
        $this->vip = $vip;
    }

    /**
     * @return string
     */
    public function getSubscriptionStatus()
    {
        return $this->subscriptionStatus;
    }

    /**
     * @param string $subscriptionStatus
     */
    public function setSubscriptionStatus($subscriptionStatus)
    {
        $this->subscriptionStatus = $subscriptionStatus;
    }

    /**
     * @return \DateTime
     */
    public function getSubscriptionStatusUpdatedAt()
    {
        return $this->subscriptionStatusUpdatedAt;
    }

    /**
     * @param \DateTime $subscriptionStatusUpdatedAt
     */
    public function setSubscriptionStatusUpdatedAt($subscriptionStatusUpdatedAt)
    {
        $this->subscriptionStatusUpdatedAt = $subscriptionStatusUpdatedAt;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        if (!$this->avatar) {
            $this->avatar = '/img/default-avatar.png';
        }
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        if (!$avatar) {
            $avatar = '/img/default-avatar.png';
        }
        $this->avatar = $avatar;
    }

    /**
     * Gets the Contact Job(s) in Structure(s).
     *
     * @return ArrayCollection|ContactJobStructure[]
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Sets the Contact Job(s) in Structure(s).
     *
     * @param ArrayCollection|ContactJobStructure[] $jobs the jobs
     *
     * @return self
     */
    public function setLinks($jobs)
    {
        $this->links = $jobs;

        return $this;
    }

    /**
     * Add Jobs(s) to Job's ArrayCollection
     *
     * @param ArrayCollection|ContactJobStructure[] $links The jobs arraycollection
     * @return self
     * @since Version 4
     * @version 4
     */
    public function addLinks(ArrayCollection $links)
    {
        foreach ($links as $link) {
            $link->setContact($this);
            $this->links->add($link);
        }
        return $this;
    }

    /**
     * Remove Jobs(s) to Job's ArrayCollection
     *
     * @param ArrayCollection|ContactJobStructure[] $links The jobs arraycollection
     * @return self
     * @since Version 4
     * @version 4
     */
    public function removeLinks(ArrayCollection $links)
    {
        foreach ($links as $link) {
            $link->setContact(null);
            $this->links->removeElement($link);
        }
        return $this;
    }

    public function getLinksArr()
    {
        $ret = [];

        foreach ($this->getLinks() as $i => $link) {
            $ret[] = (null !== $link->getJob() ? $link->getJob()->getName() : 'Fonction indisponible') . ' chez ' . (null !== $link->getStructure() ? $link->getStructure()->getName() : 'Structure indisponible');
        }

        return $ret;
    }

    public function getLinksStr()
    {
        return implode(', ', $this->getLinksArr());
    }

    /**
     * @return ContactEmail[]|ArrayCollection
     */
    public function getContactEmails()
    {
        return $this->contactEmails;
    }

    /**
     * @param ContactEmail[]|ArrayCollection $contactEmails
     */
    public function setContactEmails($contactEmails)
    {
        $this->contactEmails = $contactEmails;
    }

    /**
     * @param ArrayCollection|ContactEmail[] $contactEmails
     */
    public function addContactEmails($contactEmails)
    {
        foreach ($contactEmails as $contactEmail) {
            $contactEmail->setContact($this);
            $this->contactEmails->add($contactEmail);
        }
    }

    /**
     * @param ArrayCollection|ContactEmail[] $contactEmails
     */
    public function removeContactEmail($contactEmails)
    {
        foreach ($contactEmails as $contactEmail) {
            $contactEmail->setContact(null);
            $this->contactEmails->removeElement($contactEmail);
        }
    }

    public function getPostParcId()
    {
        return $this->postParcId;
    }

    public function setPostParcId($id)
    {
        $this->postParcId = $id;

        return $this;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilter = $this->getInputFilterPublic($entityManager);

        $inputFilter->add([
            'name' => 'vip',
            'required' => false,
            'allow_empty' => true,
            'filters' => [
                [
                    'name' => 'Boolean',
                    'options' => [
                        'type' => Boolean::TYPE_ALL,
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilterPublic(EntityManager $entityManager)
    {
        $inputFilter = parent::getInputFilter($entityManager);

        $inputFilter->add([
            'name' => 'avatar',
            'required' => false,
        ]);

        $inputFilter->add([
            'name' => 'subscriptionStatus',
            'required' => false,
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [
                        'haystack' => self::getSubscriptionStatutes(),
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'subscriptionStatusUpdatedAt',
            'required' => false,
            'filters' => [
                ['name' => 'Core\Filter\DateTimeFilter'],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getSubscriptionStatutes()
    {
        return [
            self::SUBSCRIPTION_STATUS_EMPTY,
            self::SUBSCRIPTION_STATUS_ACCEPTED,
            self::SUBSCRIPTION_STATUS_DECLINED,
        ];
    }
}
