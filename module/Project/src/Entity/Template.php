<?php

namespace Project\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Directory\Entity\Structure;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use User\Entity\User;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;
use Laminas\InputFilter\InputFilter;

/**
 * Class Template
 *
 * @package Project\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="project_template")
 */
class Template extends BasicRestEntityAbstract
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var array
     * @ORM\Column(type="json", nullable=true)
     */
    protected $config;

    /**
     * @var bool
     *
     * @ORM\Column(name="`default`", type="boolean")
     */
    protected $default;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->default;
    }

    /**
     * @return bool
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param bool $default
     */
    public function setDefault($default)
    {
        $this->default = $default;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'config',
                'required' => false,
            ],
            [
                'name' => 'default',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if ($value == true) {
                                    /** @var Template $templates */
                                    $templates = $entityManager->getRepository('Project\Entity\Template')->findAll();

                                    foreach ($templates as $template) {
                                        $template->setDefault(false);
                                    }
                                }

                                return true;
                            },
                            'message'  => 'A problem occured',
                        ],
                    ],
                ],
            ]
        ]);

        return $inputFilter;
    }

    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return true;
    }

    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        
    }
}
