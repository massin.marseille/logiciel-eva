<?php

namespace Budget\Controller;

use Budget\Entity\Account;
use Budget\Entity\BudgetCode;
use Budget\Helper\PosteFinder;
use Doctrine\ORM\EntityManager;
use Doctrine\Laminas\Hydrator\DoctrineObject as DoctrineHydrator;
use Import\Controller\IndexController;
use Project\Entity\Project;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class ImportController extends IndexController
{
	protected $action = '';

	public function indexAction()
	{
		$expenseMeta = $this->getEntityMeta('expense');
		$incomeMeta = $this->getEntityMeta('income');

		$yearsExpense = array_map(
			'current',
			$this->entityManager()
				->createQuery(
					'
            SELECT DISTINCT YEAR(e.date)
            FROM Budget\Entity\Expense e
            WHERE e.import = true
        '
				)
				->getResult()
		);

		$yearsIncomes = array_map(
			'current',
			$this->entityManager()
				->createQuery(
					'
            SELECT DISTINCT YEAR(i.date)
            FROM Budget\Entity\Income i
            WHERE i.import = true
        '
				)
				->getResult()
		);

		return new ViewModel([
			'customFields' => $this->getCustomFields(),
			'inputs' => [
				'expense' => $expenseMeta['inputFilter']->getInputs(),
				'income' => $incomeMeta['inputFilter']->getInputs(),
			],
			'years' => [
				'expense' => $yearsExpense,
				'income' => $yearsIncomes,
			],
		]);
	}

	public function validateAction()
	{
		$this->action = 'validate';

		return parent::validateAction();
	}

	public function saveAction()
	{
		/**
		 * @var EntityManager $em
		 * @var BudgetCode $budgetCode
		 */

		ini_set('memory_limit', -1);
		ini_set('max_execution_time', -1);

		$helper = $this->serviceLocator->get('ViewHelperManager');
		$translator = $helper->get('translate');

		$this->action = 'save';

		$em = $this->entityManager();

		$entityName = $this->params()->fromRoute('entity', false);
		if (!$entityName) {
			return $this->notFoundAction();
		}

		$meta = $this->getEntityMeta($entityName);
		if (!$meta) {
			return $this->notFoundAction();
		}
		$inputFilter = $meta['inputFilter'];
		$entityClass = $meta['entityClass'];

		$hydrator = new DoctrineHydrator($em);

		$success = true;
		$errors = [];
		$exceptions = [];

		$post = json_decode($this->getRequest()->getContent(), true);
		if ($post['options']['mode'] == 'eraseAll') {
			$dql = 'DELETE FROM ' . $entityClass . ' t WHERE t.import = true AND t.type = :type';
			$params = ['type' => $post['options']['type']];

			if ($post['options']['year'] !== 'all' && $post['options']['year'] !== '') {
				$dql .= ' AND YEAR(t.date) = :year';
				$params['year'] = $post['options']['year'];
			}

			$query = $this->entityManager()->createQuery($dql);
			$query->setParameters($params);
			$query->execute();
		}

		$status = null;
		if (isset($post['options']['status']) && $post['options']['status']) {
			$status = $this->entityManager()
				->getRepository('Budget\Entity\BudgetStatus')
				->find($post['options']['status']);
		}

		$lines = $this->parseLines($meta);

		$now = new \DateTime();
		$eraseProjects = [];
		$postes = [];

		foreach ($lines as $line) {
			if (isset($line['amount'])) {
				$line['amount'] = str_replace(',', '.', $line['amount']);
			}

			$inputFilter->setData($line);
			if ($inputFilter->isValid()) {
				$object = new $entityClass();
				$values = $inputFilter->getValues();
				$values['import'] = true;

				/***
				 *  ICI FAIRE LES SAVE DES OBJECT EN CASCADE
				 */

				$poste = null;
				$account = null;
				$project = null;

				$hasCustom = false;
				$customFields = $this->getCustomFields();
				foreach ($customFields as $field) {
					if (isset($line['project_field_' . $field->getId()])) {
						$value = $em->getRepository('Field\Entity\Value')->findOneBy([
							'entity' => Project::class,
							'value' => $line['project_field_' . $field->getId()],
							'field' => $field,
						]);

						if ($value) {
							$project = $em->getRepository($value->getEntity())->find($value->getPrimary());
						}
					}
				}
				if (!$hasCustom) {
					if (isset($line['project_id'])) {
						$project = $em->getRepository('Project\Entity\Project')->find($line['project_id']);
					} elseif (isset($line['project_code'])) {
						$project = $em
							->getRepository('Project\Entity\Project')
							->findOneByCode($line['project_code']);

						if (!$project) {
							$budgetCode = $em
								->getRepository(BudgetCode::class)
								->findOneBy(['name' => $line['project_code']]);

							if ($budgetCode) {
								$project = $budgetCode->getProject();
							}
						}
					} elseif (isset($line['project_name'])) {
						$project = $em
							->getRepository('Project\Entity\Project')
							->findOneByName($line['project_name']);
					}
				}

				if ($project && $line['account']) {
					$account = $em->getRepository('Budget\Entity\Account')->findOneByCode($line['account']);
					if (!$account) {
						$account = new Account();
						$account->setType(
							$meta['entityName'] === 'expense' ? Account::TYPE_EXPENSE : Account::TYPE_INCOME
						);
						$account->setCode($line['account']);
						$account->setName($line['account']);

						$em->persist($account);
						$em->flush();
					}

					$posteClass = 'Budget\Entity\Poste' . ucfirst($meta['entityName']);
					$poste = $em->getRepository($posteClass)->findOneBy([
						'account' => $account,
						'project' => $project,
					]);

					if ($post['options']['inPosteAccountParent'] && !$poste) {
						$poste = PosteFinder::findPosteByParentAccount(
							$account->getParent(),
							$project,
							$em->getRepository($posteClass)
						);
					}

					if (!$poste) {
						$poste = new $posteClass();
						$poste->setAccount($account);
						$poste->setName($account->getName());
						$poste->setProject($project);
						$poste->setAmount(0);

						$em->persist($poste);
						$em->flush();
					}

					$values['poste'] = $poste;

					if ($post['options']['mode'] == 'eraseProjects') {
						$eraseProjects[] = $project;
					}

					if ($status !== null) {
						$project->setBudgetStatus($status);
					}
				}

				if (!isset($values['poste']) || !$values['poste']) {
					$errors[] = [
						'poste' => [
							'callback' => $translator->__invoke('transaction_field_poste_error_non_exists'),
						],
					];
					continue;
				}

				$hydrator->hydrate($values, $object);

				
				if (isset($line['managementUnit'])) {
					$managementUnit = $em->getRepository('Budget\Entity\ManagementUnit')->findOneBy(['code' => $line['managementUnit']]);
					$object->setManagementUnit($managementUnit);
				}


				$em->persist($object);
				$errors[] = null;

				$exception = null;
				try {
					$em->flush();
					$postes[] = $poste;
				} catch (\Exception $e) {
					$exception = $e->getMessage();
					$success = false;
				} finally {
					$exceptions[] = $exception;
				}
			} else {
				$success = false;
				$errors[] = $inputFilter->getMessages();
				$exceptions[] = null;
			}
		}

		if ($post['options']['mode'] == 'eraseProjects') {
			$posteClass =
				$entityClass == 'Budget\Entity\Expense'
				? 'Budget\Entity\PosteExpense'
				: 'Budget\Entity\PosteIncome';
			$dql =
				'DELETE FROM ' .
				$entityClass .
				' t WHERE t.import = true AND t.type = :type AND t.poste IN (SELECT p.id FROM ' .
				$posteClass .
				' p WHERE p.project IN (:projects)) AND t.createdAt < :now';
			$params = ['type' => $post['options']['type'], 'projects' => $eraseProjects, 'now' => $now];

			if ($post['options']['year'] !== 'all' && $post['options']['year'] !== '') {
				$dql .= ' AND YEAR(t.date) = :year';
				$params['year'] = $post['options']['year'];
			}

			$query = $this->entityManager()->createQuery($dql);
			$query->setParameters($params);
			$query->execute();
		}

		// Auto link postes with relative parent
		foreach ($postes as $poste) {
			$parent = $poste->findParent();
			if ($parent && $poste->getParent() === null) {
				$poste->setParent($parent);
			}
		}

		$this->entityManager()->flush();

		return new JsonModel([
			'success' => $success,
			'errors' => $errors,
			'exceptions' => $exceptions,
		]);
	}

	protected function parseLines($meta)
	{
		/**
		 * En fonction du champs liaison projet qui est envoyé, on modifie l'input filter
		 */
		$post = json_decode($this->getRequest()->getContent(), true);
		$properties = $post['properties'];

		$entityManager = $this->entityManager();

		if ($this->action == 'validate') {
			$hasCustom = false;
			$customFields = $this->getCustomFields();
			foreach ($customFields as $field) {
				if (in_array('project_field_' . $field->getId(), $properties)) {
					$meta['inputFilter']->add([
						'name' => 'project_field_' . $field->getId(),
						'required' => true,
						'validators' => [
							[
								'name' => 'Callback',
								'options' => [
									'callback' => function ($value) use ($entityManager, $field) {
										if (is_array($value)) {
											$value = $value['id'];
										}
										$value = $entityManager->getRepository('Field\Entity\Value')->findOneBy([
											'entity' => Project::class,
											'value' => $value,
											'field' => $field,
										]);

										if ($value) {
											$project = $entityManager
												->getRepository($value->getEntity())
												->find($value->getPrimary());

											return $project !== null;
										}

										return false;
									},
									'message' => 'poste_field_project_error_non_exists',
								],
							],
						],
					]);

					$hasCustom = true;
					break;
				}
			}

			if (!$hasCustom) {
				if (in_array('project_id', $properties)) {
					$meta['inputFilter']->add([
						'name' => 'project_id',
						'required' => true,
						'validators' => [
							[
								'name' => 'Callback',
								'options' => [
									'callback' => function ($value) use ($entityManager) {
										if (is_array($value)) {
											$value = $value['id'];
										}
										$project = $entityManager
											->getRepository('Project\Entity\Project')
											->find($value);

										return $project !== null;
									},
									'message' => 'poste_field_project_error_non_exists',
								],
							],
						],
					]);
				} elseif (in_array('project_code', $properties)) {
					$meta['inputFilter']->add([
						'name' => 'project_code',
						'required' => true,
						'validators' => [
							[
								'name' => 'Callback',
								'options' => [
									'callback' => function ($value) use ($entityManager) {
										if (is_array($value)) {
											$value = $value['id'];
										}
										$project = $entityManager
											->getRepository('Project\Entity\Project')
											->findOneByCode($value);

										if(!isset($project)) {
											$queryBuilder = $entityManager->createQueryBuilder();
		
											$queryBuilder
												->select('o')
												->from(BudgetCode::class, 'o')
												->where('o.name = :code')
												->setParameters([
													'code' => $value,
												]);
			
											try {
												$entity = $queryBuilder->getQuery()->getOneOrNullResult();
											} catch (\Exception $e) {
												$entity = null;
											}

											return $entity !== null;
										}

										return $project !== null;
									},
									'message' => 'poste_field_project_error_non_exists',
								],
							],
						],
					]);
				} elseif (in_array('project_name', $properties)) {
					$meta['inputFilter']->add([
						'name' => 'project_name',
						'required' => true,
						'validators' => [
							[
								'name' => 'Callback',
								'options' => [
									'callback' => function ($value) use ($entityManager) {
										if (is_array($value)) {
											$value = $value['id'];
										}
										$project = $entityManager
											->getRepository('Project\Entity\Project')
											->findOneByName($value);

										return $project !== null;
									},
									'message' => 'poste_field_project_error_non_exists',
								],
							],
						],
					]);
				} else {
					$meta['inputFilter']->add([
						'name' => 'project',
						'required' => true,
					]);
				}
			}
		}

		$lines = parent::parseLines($meta);
		$type = $post['options']['type'];

		foreach ($lines as $i => $line) {
			$lines[$i]['type'] = $type;
		}

		return $lines;
	}

	protected function getEntityMeta($entityName)
	{
		$budgetHTEnabled = $this->serviceLocator
			->get('MyModuleManager')
			->getClientModuleConfiguration('budget', 'ht');

		$meta = parent::getEntityMeta($entityName);

		$meta['inputFilter']->remove('poste');
		if ($this->action !== 'save') {
			$meta['inputFilter']->remove('type');
		}

		$meta['inputFilter']->add([
			'name' => 'account',
			'required' => true,
		]);

		if (!$budgetHTEnabled) {
			$meta['inputFilter']->remove('amountHT');
		}

		return $meta;
	}

	protected function getCustomFields()
	{
		$customGroups = $this->entityManager()
			->getRepository('Field\Entity\Group')
			->findByEnabled(true);
		$customFields = [];
		foreach ($customGroups as $group) {
			foreach ($group->getFields() as $field) {
				if ($field->isUnique()) {
					$customFields[] = $field;
				}
			}
		}

		return $customFields;
	}
}
