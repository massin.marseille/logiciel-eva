<?php

namespace Budget\Controller\API;

use Budget\Service\SimplifiedInputService;
use Core\Controller\BasicRestController;
use DateTime;
use Laminas\View\Model\JsonModel;

class SimplifiedInputController extends BasicRestController
{

    public function referentialAction()
    {
        if(!$this->acl()->isAllowed('budget:e:posteExpenseSimplified:r')) {
            return $this->notFoundAction();
        }

        $projectId = $this->params()->fromRoute('projectId', false);

        $referential = $this->getEntityManager()->getRepository('Budget\Entity\PosteExpenseRef')->findBy(array('parent' => null));

        /** @var SimplifiedInputService */
        $service = $this->serviceLocator->get('simplifiedInputService');
        $latestYear = $service->getLatestExerciseYear($projectId);
        $startYear = $latestYear ? $latestYear + 1 : intval((new DateTime('now'))->format('Y'));

        return new JsonModel([
            'referential' => $referential,
            'currentYear' => $startYear,
            'startYear' => $startYear,
            'endYear' => $startYear + 1
        ]);
    }

    public function tableAction()
    {        
        if(!$this->acl()->isAllowed('budget:e:posteExpenseSimplified:r')) {
            return $this->notFoundAction();
        }

        $projectId = $this->params()->fromRoute('projectId', false);

        /** @var SimplifiedInputService */
        $service = $this->serviceLocator->get('simplifiedInputService');
        $result = $service->generateSimplifiedView($projectId);
        return new JsonModel($result);
    }

    public function generateAction()
    {
        if(!$this->acl()->isAllowed('budget:e:posteExpenseSimplified:c')) {
            return $this->notFoundAction();
        }

        $projectId = $this->params()->fromRoute('projectId', false);
        $data = json_decode($this->getRequest()->getContent());

        /** @var SimplifiedInputService */
        $service = $this->serviceLocator->get('simplifiedInputService');
        $writeYears = $service->getNewYears($data->start, $data->end);
        $result = $service->generateSimplifiedView($projectId, $writeYears, $data->type);
        return new JsonModel($result);
    }

    public function importAction()
    {
        if(!$this->acl()->isAllowed('budget:e:posteExpenseSimplified:c')) {
            return $this->notFoundAction();
        }

        $projectId = $this->params()->fromRoute('projectId', false);
        $data = json_decode($this->getRequest()->getContent());

        /** @var SimplifiedInputService */
        $service = $this->serviceLocator->get('simplifiedInputService');
        $service->importSimplifiedView($projectId, $data->table, $data->years);

        return new JsonModel();
    }
}
