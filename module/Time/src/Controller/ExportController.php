<?php

namespace Time\Controller;

use Core\Controller\AbstractActionSLController;
use Core\Export\ExcelExporter;
use Export\Entity\Custom;
use Project\Entity\Project;
use Time\Entity\Export;
use Time\Entity\Timesheet;
use Laminas\Http\Request;
use Laminas\View\Model\ViewModel;

/**
 * Class ExportController
 * @package Time\Controller
 */
class ExportController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('time:e:export:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->isAllowed('time:e:export:r')) {
            return $this->notFoundAction();
        }

        $timeExport = $this->entityManager()->getRepository('Time\Entity\Export')->find($id);

        if ($timeExport && !$this->acl()->ownerControl('time:e:export:u', $timeExport) && !$this->acl()->ownerControl('time:e:export:r', $timeExport)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'timeExport' => $timeExport,
        ]);
    }

    public function insertInTree(&$_item, &$tree, $level)
    {
        foreach ($tree as $id => $item) {
            if ($item['id'] == $_item['parent']) {
                unset($_item['parent']);
                $_item['level']                      = $level;
                $tree[$id]['children'][$_item['id']] = $_item;

                return true;
            }

            if ($this->insertInTree($_item, $tree[$id]['children'], $level + 1)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Inserts a new key/value after the key in the array.
     *
     * @param mixed $key       The key to insert after.
     * @param array $array     An array to insert in to.
     * @param mixed $new_key   The key to insert.
     * @param mixed $new_value An value to insert.
     * @return array|bool The new array if the key exists, FALSE otherwise.
     */
    public function array_insert_after($key, array &$array, $new_key, $new_value)
    {
        if (array_key_exists($key, $array)) {
            $new = [];
            foreach ($array as $k => $value) {
                $new[$k] = $value;
                if ($k === $key) {
                    $new[$new_key] = $new_value;
                }
            }
            return $new;
        }
        return false;
    }

    /**
     * @param $needle
     * @param $haystack
     * @return bool|int|string
     */
    public function recursive_array_search($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value OR (is_array($value) && recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

    public function doAction()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);
        $helperPluginManager = $this->serviceLocator->get('ViewHelperManager');
        $translator          = $helperPluginManager->get('translate');
        $bypassTimesheet     = false;
        $export              = $this->params()->fromPost('export', false);
        $userFilters         = $this->params()->fromPost('userFilters', '{}');
        $projectFilters      = $this->params()->fromPost('projectFilters', '{}');
        $filters             = $this->params()->fromPost('filters', '{}');
        $filters             = json_decode($filters, true);
        $customExport        = json_decode($this->params()->fromPost('customExport', false), true);

        /** @var Export $timeExport */
        $timeExport = $this->entityManager()->getRepository('Time\Entity\Export')->find($export);

        if (preg_match_all('/[a-zA-Z]+/', $export)) {
            $filters['user'] = [
                'op' => 'eq',
                'val' => isset($customExport['user']['id']) ? $customExport['user']['id'] : $customExport['user']
            ];
        } else {
            $userFilters = json_decode($userFilters, true);
            $request   = $this->apiRequest('User\Controller\API\User', Request::METHOD_GET, [
                'col'    => [
                    'id', 'name',
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => 'id',
                        'order'   => 'asc',
                        'filters' => $userFilters,
                    ],
                ],
            ]);
            $tempUsers = $request->dispatch()->getVariable('rows');
            if (count($tempUsers) != 0) {
                $filters['user.id'] = [
                    'op'  => 'eq',
                    'val' => [],
                ];
                foreach ($tempUsers as $i => $user) {
                    $filters['user.id']['val'][] = $user['id'];
                }
            } else {
                $bypassTimesheet = true;
            }
        }

        $projectFilters = json_decode($projectFilters, true);
        $request      = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
            'col'    => [
                'id', 'code', 'name',
                'parent.id',
                'hierarchySup.id', 'hierarchySup.name', 'hierarchySup.parent.id',
                'keywords',
            ],
            'search' => [
                'type' => 'list',
                'data' => [
                    'sort'    => 'id',
                    'order'   => 'asc',
                    'filters' => $projectFilters,
                ],
            ],
        ]);
        $tempProjects = $request->dispatch()->getVariable('rows');
        if (count($tempProjects) != 0) {
            $filters['project.id'] = [
                'op'  => 'eq',
                'val' => [],
            ];
            foreach ($tempProjects as $i => $project) {
                $filters['project.id']['val'][] = $project['id'];
            }
        } else {
            $bypassTimesheet = true;
        }

        if (!$bypassTimesheet) {
            if (preg_match_all('/[a-zA-Z]+/', $export)) {
                $filters['start'] = [
                    'op'  => 'period',
                    'val' => [
                        'start' => $customExport['start'],
                        'end'   => $customExport['end'],
                    ],
                ];
            }

            $request    = $this->apiRequest('Time\Controller\API\Timesheet', Request::METHOD_GET, [
                'col'    => [
                    'id',
                    'name',
                    'project.id',
                    'project.code',
                    'project.name',
                    'start',
                    'end',
                    'hours',
                    'type',
                    'description',
                ],
                'search' => [
                    'type' => 'list',
                    'data' => [
                        'sort'    => preg_match_all('/[a-zA-Z]+/', $export) ? 'start' : 'name',
                        'order'   => 'asc',
                        'filters' => $filters,
                    ],
                ],
            ]);
            $timesheets = $request->dispatch()->getVariable('rows');

            if (preg_match_all('/[a-zA-Z]+/', $export)) {
                Custom::doExport(
                    $this->environment()->getClient(),
                    $export,
                    $customExport,
                    $timesheets,
                    $this->entityManager(),
                    $translator,
                    $this->serviceLocator->get('ControllerPluginManager')->get('apiRequest')
                );
            } else if ($timeExport && $timesheets) {
                /**
                 * First, we build a 2 dim. array (1dim. = lines, 2dim. = columns) named $groupedContent.
                 * If the orientation is landscape the groupBy key is the key for the lines, else, it's the date key.
                 * The $allColumns array contains all the columns keys (they're not present for every lines so we have to store
                 * them somewhere).
                 */
                $groupedContent = [];
                $allColumns     = [];
                $totals         = [
                    'cols'  => [],
                    'lines' => [],
                ];
                $linesBolded    = [];
                if ($timeExport->getOrientation() == 'portrait') {
                    $projects = [];
                }
                foreach ($timesheets as $timesheet) {
                    /** @var Timesheet $timesheet */
                    $timesheet = $this->entityManager()->getRepository('Time\Entity\Timesheet')->find($timesheet['id']);
                    switch ($timeExport->getGroupByUnit()) {
                        case 'days' :
                            $keyDate = $timesheet->getEnd()->format('Y-m-d');
                            break;
                        case 'weeks' :
                            $keyDate = $timesheet->getEnd()->format('Y \S\e\m\a\i\n\e W');
                            break;
                        case 'months' :
                            $keyDate = $timesheet->getEnd()->format('Y-m');
                            break;
                        case 'years' :
                            $keyDate = $timesheet->getEnd()->format('Y');
                            break;
                    }

                    if ($timeExport->getOrientation() == 'landscape') {
                        if ($timesheet->getProject()) {
                            $keyProject = $timesheet->getProject()->getId();
                        } else {
                            $keyProject = $timesheet->getName();
                        }
                    } else {
                        if ($timesheet->getProject()) {
                            $keyProject = $timesheet->getProject()->getName();
                            if ($timeExport->getOrientation() == 'portrait') {
                                $projects[$timesheet->getProject()->getId()] = $keyProject;
                            }
                        } else {
                            $keyProject = $timesheet->getName();
                        }
                    }

                    // Landscape or portrait ?
                    $keyCol  = $timeExport->getOrientation() == 'landscape' ? $keyDate : $keyProject;
                    $keyLine = $timeExport->getOrientation() == 'landscape' ? $keyProject : $keyDate;

                    if (!isset($groupedContent[$keyLine][$keyCol])) {
                        $groupedContent[$keyLine][$keyCol] = 0;
                    }
                    $groupedContent[$keyLine][$keyCol] += $timesheet->getHours();

                    if (!in_array($keyCol, $allColumns)) {
                        $allColumns[] = $keyCol;
                    }

                    // Totals.
                    if (!isset($totals['cols'][$keyCol])) {
                        $totals['cols'][$keyCol] = 0;
                    }

                    if (!isset($totals['lines'][$keyLine])) {
                        $totals['lines'][$keyLine] = 0;
                    }

                    $totals['cols'][$keyCol]   += $timesheet->getHours();
                    $totals['lines'][$keyLine] += $timesheet->getHours();
                }

                /**
                 * We are building an associative array with 0 as values to merge it in the grouped content lines.
                 */
                $allColumns = array_flip($allColumns);
                foreach ($allColumns as $key => $value) {
                    $allColumns[$key] = 0;
                }

                // Sort the projects
                if ($timeExport->getOrientation() == 'portrait') {
                    $projectIds = [];
                    foreach ($projects as $id => $project) {
                        $projectIds[] = $id;
                    }
                    // Sort the projects
                    $projectsSorted = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
                        'col'    => ['id'],
                        'search' => [
                            'type' => 'list',
                            'data' => [
                                'sort'    => $timeExport->getSortByProperty(),
                                'order'   => 'asc',
                                'filters' => [
                                    'id' => [
                                        'op'  => 'in',
                                        'val' => $projectIds,
                                    ],
                                ],
                            ],
                        ],
                    ])->dispatch()->getVariable('rows');

                    $allColumnsSorted = [];
                    foreach ($projectsSorted as $projectSorted) {
                        $allColumnsSorted[$projects[$projectSorted['id']]] = 0;
                        unset($allColumns[$projects[$projectSorted['id']]]);
                    }

                    $allColumns = array_merge($allColumnsSorted, $allColumns);
                }

                // Make a hierarchy of the projects
                if ($timeExport->getOrientation() == 'landscape') {
                    $flatArray           = $groupedContent;
                    $timesheetsWOProject = [];

                    foreach ($flatArray as $project_id => $values) {
                        // Replacing int 0 by string 0, otherwise the value is not seen on Excel.
                        foreach ($values as $key => $value) {
                            if ($value == 0) {
                                $flatArray[$project_id][$key] = '0';
                            }

                        }

                        $project = $this->entityManager()->getRepository('Project\Entity\Project')->find($project_id);

                        if ($project) {
                            $projects_sup = $project->getHierarchySup();
                            $projects_sup = $projects_sup->toArray();
                            $projects_sup = array_reverse($projects_sup);

                            foreach ($projects_sup as $project_sup) {
                                if (!isset($flatArray[$project_sup->getId()])) {
                                    $flatArray[$project_sup->getId()] = [];
                                }
                            }
                        } else {
                            $timesheetsWOProject[$project_id] = $flatArray[$project_id];
                            unset($flatArray[$project_id]);
                        }
                    }

                    foreach ($flatArray as $project_id => $values) {
                        $project = $this->entityManager()->getRepository('Project\Entity\Project')->find($project_id);
                        if ($project->getParent()) {
                            $flatArray[$project_id]['parent'] = $project->getParent()->getId();
                        }
                        $flatArray[$project_id]['id'] = $project_id;
                    }

                    // Sort the projects
                    $projectIds = [];
                    foreach ($flatArray as $projectId => $values) {
                        $projectIds[] = $projectId;
                    }
                    $projects        = $this->apiRequest('Project\Controller\API\Project', Request::METHOD_GET, [
                        'col'    => ['id'],
                        'search' => [
                            'type' => 'list',
                            'data' => [
                                'sort'    => $timeExport->getSortByProperty(),
                                'order'   => 'asc',
                                'filters' => [
                                    'id' => [
                                        'op'  => 'in',
                                        'val' => $projectIds,
                                    ],
                                ],
                            ],
                        ],
                    ])->dispatch()->getVariable('rows');
                    $flatArraySorted = [];
                    foreach ($projects as $project) {
                        $flatArraySorted[$project['id']] = $flatArray[$project['id']];
                    }
                    $flatArray = $flatArraySorted;

                    $tree = [];
                    $n    = sizeOf($flatArray);
                    do {
                        $relaunch = false;
                        foreach ($flatArray as $i => $item) {
                            $item['children'] = [];
                            $item['level']    = 0;
                            if (!isset($item['parent']) || $item['parent'] == null) {
                                $tree[$i] = $item;
                                $n--;
                                $relaunch = true;
                                unset($flatArray[$i]);
                            } else {
                                if ($this->insertInTree($item, $tree, 1)) {
                                    unset($flatArray[$i]);
                                    $n--;
                                    $relaunch = true;
                                }
                            }
                        }
                    } while ($n > 0 && $relaunch);

                    $totals_parent = [];
                    foreach ($tree as $key => $parent) {
                        $total = [];
                        array_walk_recursive($parent, function ($item, $key) use (&$total) {
                            $total[$key] = isset($total[$key]) ? $item + $total[$key] : $item;
                        });
                        unset($total['id'], $total['level']);
                        $total['parent'] = $key;
                        $totals_parent[] = $total;
                    }

                    foreach ($totals_parent as $total) {
                        $parent = $total['parent'];
                        unset($total['parent']);
                        $tree = $this->array_insert_after($parent, $tree, 'Total ' . $parent, $total);
                    }

                    do {
                        $repeat = false;
                        foreach ($tree as $key_parent => $parent) {
                            if (isset($parent['children'])) {
                                if ($parent['children'] != []) {
                                    $children = array_reverse($parent['children']);
                                    foreach ($children as $child) {
                                        $tree = $this->array_insert_after($key_parent, $tree, $child['id'], $child);
                                        if (isset($child['level'])) {
                                            if ($child['level'] != 0) {
                                                $repeat = true;
                                            }
                                        }
                                    }
                                }
                                unset($tree[$key_parent]['children']);
                            }
                        }
                    } while ($repeat == true);

                    foreach ($tree as $key => $values) {
                        if (isset($values['id'])) {
                            unset($values['id']);
                        }

                        if (isset($values['level'])) {
                            /** @var Project $project */
                            $project             = $this->entityManager()->getRepository('Project\Entity\Project')->find($key);
                            $project_name        = $project->getName();
                            $project_name_indent = str_repeat('    ', $values['level']) . $project_name;
                            $total_project_id    = 'Total ' . $key;
                            $total_project_name  = 'Total ' . $project_name;

                            $tree = $this->array_insert_after($key, $tree, $project_name_indent, $values);
                            unset($tree[$key]);

                            if (isset($tree[$total_project_id])) {
                                $tree = $this->array_insert_after($total_project_id, $tree, $total_project_name, $tree[$total_project_id]);
                                unset($tree[$total_project_id]);

                                $totals['lines'][$total_project_name] = 0;
                                foreach ($tree[$total_project_name] as $hours) {
                                    $totals['lines'][$total_project_name] += $hours;
                                }
                            }

                            if (isset($totals['lines'][$key])) {
                                $totals['lines'] = $this->array_insert_after($key, $totals['lines'], $project_name_indent, $totals['lines'][$key]);
                                unset($totals['lines'][$key]);
                            } else {
                                // Add the line to array totals when there is no timesheet to a project parent
                                $totals['lines'][$project_name_indent] = 0;
                            }
                            unset($tree[$project_name_indent]['level']);
                        }
                    }

                    $i = 1;
                    foreach ($tree as $key => $value) {
                        if (preg_match('/^Total/', $key) == 1) {
                            $linesBolded[] = $i + 1;
                        }
                        $i++;
                    }

                    $groupedContent = $tree + $timesheetsWOProject;
                }

                if ($timeExport->getOrientation() == 'landscape') {
                    ksort($allColumns);
                } else {
                    ksort($groupedContent);
                }

                if ($timeExport->getAddMissingDays()
                    || $timeExport->getCompleteMonths()
                    || $timeExport->getAllYear()) {
                    if ($timeExport->getOrientation() == 'landscape') {
                        $n     = sizeOf($allColumns);
                        $keys  = array_keys($allColumns);
                        $first = $keys[0];
                        $last  = $keys[$n - 1];
                    } else {
                        $n     = sizeOf($groupedContent);
                        $keys  = array_keys($groupedContent);
                        $first = $keys[0];
                        $last  = $keys[$n - 1];
                    }

                    $format   = '';
                    $interval = null;
                    switch ($timeExport->getGroupByUnit()) {
                        case 'days' :
                            $format    = 'Y-m-d';
                            $interval  = \DateInterval::createFromDateString('1 day');
                            $firstDate = \DateTime::createFromFormat($format, $first);
                            $lastDate  = \DateTime::createFromFormat($format, $last);
                            break;
                        case 'weeks' :
                            $format = 'Y \S\e\m\a\i\n\e W';
                            list($firstYear, $firstWeek) = explode(' Semaine ', $first);
                            list($lastYear, $lastWeek) = explode(' Semaine ', $last);

                            $interval = \DateInterval::createFromDateString('1 week');

                            $firstDateTstamp = strtotime($firstYear . 'W' . $firstWeek);
                            $firstDate       = new \DateTime();
                            $firstDate->setTimestamp($firstDateTstamp);

                            $lastDateTstamp = strtotime($lastYear . 'W' . $lastWeek);
                            $lastDate       = new \DateTime();
                            $lastDate->setTimestamp($lastDateTstamp);
                            break;
                        case 'months' :
                            $format    = 'Y-m';
                            $interval  = \DateInterval::createFromDateString('1 month');
                            $firstDate = \DateTime::createFromFormat($format, $first);
                            $lastDate  = \DateTime::createFromFormat($format, $last);
                            break;
                        case 'years' :
                            $format    = 'Y';
                            $interval  = \DateInterval::createFromDateString('1 year');
                            $firstDate = \DateTime::createFromFormat($format, $first);
                            $lastDate  = \DateTime::createFromFormat($format, $last);
                            break;
                    }

                    if ($timeExport->getAllYear()) {
                        $firstDate = new \DateTime($firstDate->format('Y-01-01 00:00'));
                        $lastDate  = new \DateTime($lastDate->format('Y-12-31 00:00'));
                    } else if ($timeExport->getCompleteMonths()) {
                        $firstDate    = new \DateTime($firstDate->format('Y-m-01 00:00'));
                        $currentMonth = new \DateTime($lastDate->format('Y-m-01 00:00'));
                        $lastDate     = new \DateTime($lastDate->format('Y-m-' . $currentMonth->format('t') . ' 00:00'));
                    }

                    // To get the last day in the foreach of the period.
                    $lastDate->add(\DateInterval::createFromDateString('1 days'));

                    $period = new \DatePeriod($firstDate, $interval, $lastDate);

                    if ($timeExport->getOrientation() == 'landscape') {
                        $allColumns = [];
                        foreach ($period as $dt) {
                            $key              = $dt->format($format);
                            $allColumns[$key] = 0;

                            if (!isset($totals['cols'][$key])) {
                                $totals['cols'][$key] = 0;
                            }
                        }
                        $allColumns[$last] = 0;
                    } else {
                        foreach ($period as $dt) {
                            $key = $dt->format($format);
                            if (!isset($groupedContent[$key])) {
                                $groupedContent[$key] = [];
                            }
                            if (!isset($totals['lines'][$key])) {
                                $totals['lines'][$key] = 0;
                            }
                        }

                        ksort($groupedContent);
                    }
                }

                /**
                 * Now, we modify the grouped content line to have a complete,ordered, no named line array to insert it in the
                 * $body array (used by phpexcel).
                 */
                $body = [];
                foreach ($groupedContent as $line => $cols) {
                    // Fill the array with the non present cols
                    $flatLine = array_replace($allColumns, $cols);
                    // Get only values
                    $values = array_values($flatLine);
                    // Insert the line key as first element.
                    array_unshift($values, $line);

                    // Total of the line
                    $values[] = ''; // Add a blank col
                    if ($timeExport->getTotalUnit()) {
                        foreach ($timeExport->getTotalUnit() as $totalUnit) {
                            $totalValue = 0;
                            switch ($totalUnit) {
                                case 'hours' :
                                    $totalValue = $totals['lines'][$line];
                                    break;
                                case 'days' :
                                    $totalValue = $totals['lines'][$line] / $timeExport->getHoursByDay();
                                    break;
                                case 'cost' :
                                    $totalValue = $totals['lines'][$line] * $timeExport->getCost();
                                    break;
                            }

                            if ($totalValue == 0) {
                                $totalValue = '0';
                            }

                            $values[] = $totalValue;
                        }
                    }

                    $body[] = $values;
                }

                // For the header and the total lines
                $head   = array_keys($allColumns);
                $head[] = ''; // Add a blank line
                $footer = [];
                if ($timeExport->getTotalUnit()) {
                    foreach ($timeExport->getTotalUnit() as $totalUnit) {
                        $title = '';
                        switch ($totalUnit) {
                            case 'hours' :
                                $title = 'Total en heures';
                                break;
                            case 'days' :
                                $title = 'Total en jours';
                                break;
                            case 'cost' :
                                $title = 'Total en euros';
                                break;
                        }
                        // Add the total in the header
                        $head[] = $title;
                        // Start by the total name
                        $totalLine = [$title];
                        // Foreach columns, insert the value in the total line
                        foreach ($allColumns as $keyCol => $_) {
                            $totalValue = 0;
                            switch ($totalUnit) {
                                case 'hours' :
                                    $totalValue = $totals['cols'][$keyCol];
                                    break;
                                case 'days' :
                                    $totalValue = $totals['cols'][$keyCol] / $timeExport->getHoursByDay();
                                    break;
                                case 'cost' :
                                    $totalValue = $totals['cols'][$keyCol] * $timeExport->getCost();
                                    break;
                            }

                            $totalLine[] = $totalValue;
                        }

                        $footer[] = $totalLine;
                    }
                }

                array_unshift($head, '');
                $head = [$head];
                array_unshift($footer, []);

                $rows = array_merge($head, $body, $footer);

                // Get the footer in bold on Excel.
                if ($timeExport->getOrientation() == 'landscape') {
                    $number_of_rows_before_footer = count($head) + count($body);
                    $i                            = 1;
                    foreach ($footer as $line) {
                        $linesBolded[] = $number_of_rows_before_footer + $i;
                        $i++;
                    }
                }

                ExcelExporter::download($rows, $linesBolded);
            } else {
                ExcelExporter::download([$translator->__invoke('time_export_error_no_timesheet')]);
            }
        } else {
            ExcelExporter::download([$translator->__invoke('time_export_error_no_timesheet')]);
        }
    }
}