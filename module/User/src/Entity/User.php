<?php

namespace User\Entity;

use Core\Entity\Contact\AbstractPerson;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use Profile\Entity\Profile;
use Laminas\Filter\Boolean;
use Laminas\Validator\Regex as RegexValidator;

/**
 * Class User
 *
 * @package User\Entity
 * @author  Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity(repositoryClass="User\Repository\UserRepository")
 * @ORM\Table(name="user_user")
 */
class User extends AbstractPerson
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", unique=true)
	 */
	protected $login;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	protected $password;

	/**
	 * @var Role
	 *
	 * @ORM\ManyToOne(targetEntity="User\Entity\Role")
	 */
	protected $role;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string")
	 */
	protected $avatar = '/img/default-avatar.png';

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $color;

	/**
	 * @var bool
	 *
	 * @ORM\Column(type="boolean", options={"default" : 0})
	 */
	protected $disabled = false;

	/**
	 * @var Profile
	 *
	 * @ORM\ManyToOne(targetEntity="Profile\Entity\Profile", inversedBy="users")
	 * @ORM\JoinColumn(name="profile_id", referencedColumnName="id")
	 */
	protected $profile;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $worked_hours;

	public function __construct()
	{
		
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getLogin()
	{
		return $this->login;
	}

	/**
	 * @param string $login
	 */
	public function setLogin($login)
	{
		$this->login = $login;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		if ($password !== $this->password) {
			$password = sha1($password);
		}

		$this->password = $password;
	}

	/**
	 * @return Role
	 */
	public function getRole()
	{
		return $this->role;
	}

	/**
	 * @param Role $role
	 */
	public function setRole($role)
	{
		$this->role = $role;
	}

	/**
	 * @return string
	 */
	public function getAvatar()
	{
		if (!$this->avatar) {
			$this->avatar = '/img/default-avatar.png';
		}
		return $this->avatar;
	}

	/**
	 * @param string $avatar
	 */
	public function setAvatar($avatar)
	{
		if (!$avatar) {
			$avatar = '/img/default-avatar.png';
		}
		$this->avatar = $avatar;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * @param string $color
	 */
	public function setColor($color)
	{
		$this->color = $color;
	}

	/**
	 * @return bool
	 */
	public function isDisabled()
	{
		return $this->disabled;
	}

	/**
	 * @return bool
	 */
	public function getDisabled()
	{
		return $this->disabled;
	}

	/**
	 * @param bool $disabled
	 */
	public function setDisabled($disabled)
	{
		$this->disabled = $disabled;
	}

	/**
	 * @param float $hours
	 * @return float
	 */
	public function calculTimeCost($hours)
	{
		if (empty($this->profile)) {
			return 0;
		}

		$price_per_hours = $this->profile->getPrice_per_hour();

		return $price_per_hours * $hours;
	}


	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilter = parent::getInputFilter($entityManager);

		$inputFilter->add([
			'name' => 'login',
			'required' => true,
			'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
			'validators' => [
				[
					'name' => 'Callback',
					'options' => [
						'callback' => function ($value, $context) use ($entityManager) {
							$queryBuilder = $entityManager->createQueryBuilder();
							$queryBuilder
								->select('u')
								->from('User\Entity\User', 'u')
								->where('u.login = :login')
								->andWhere('u.id != :id')
								->setParameters([
									'id' => !isset($context['id']) ? '' : $context['id'],
									'login' => $value,
								]);

							try {
								$user = $queryBuilder->getQuery()->getOneOrNullResult();
							} catch (\Exception $e) {
								$user = null;
							}
							return $user === null;
						},
						'message' => 'user_field_login_error_unique',
					],
				],
			],
		]);

		$inputFilter->add([
			'name' => 'password',
			'required' => true,
		]);

		$inputFilter->add([
			'name' => 'role',
			'required' => true,
			'validators' => [
				[
					'name' => 'Callback',
					'options' => [
						'callback' => function ($value) use ($entityManager) {
							if (is_array($value)) {
								$value = $value['id'];
							}
							$role = $entityManager->getRepository('User\Entity\Role')->find($value);

							return $role !== null;
						},
						'message' => 'user_field_role_error_non_exists',
					],
				],
			],
		]);

		$inputFilter->add([
			'name' => 'profile',
			'required' => false,
			'validators' => [
				[
					'name' => 'Callback',
					'options' => [
						'callback' => function ($value) use ($entityManager) {
							if (is_array($value)) {
								$value = $value['id'];
							}
							$profile = $entityManager->getRepository('Profile\Entity\Profile')->find($value);

							return $profile !== null;
						},
						'message' => 'user_field_profile_error_non_exists',
					],
				],
			],
		]);

		$inputFilter->add([
			'name' => 'worked_hours',
			'required' => false,
			'filters'    => [
				['name' => 'NumberParse'],
			],
			'validators' => [
				[
					'name'    => 'Between',
					'options' => [
						'min' => 0,
						'max' => 24,
					],
				],
			],
		]);


		$inputFilter->add([
			'name' => 'avatar',
			'required' => false,
		]);

		$inputFilter->add(
			[
				'required' => true,
				'name' => 'email',
				'validators' => [['name' => 'EmailAddress']],
			],
			'email'
		);

		$inputFilter->add([
			'name' => 'color',
			'required' => false,
			'filters' => [['name' => 'Laminas\Filter\StringTrim'], ['name' => 'Laminas\Filter\StringToLower']],
			'validators' => [new RegexValidator('/^#[0-9a-fA-F]{6}$/')],
		]);

		$inputFilter->add([
			'name' => 'disabled',
			'required' => false,
			'allow_empty' => true,
			'filters' => [['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]]],
		]);

		return $inputFilter;
	}

	/**
	 * @param User $user
	 * @param string $crudAction
	 * @param null $owner
	 * @param null $entityManager
	 *
	 * @return bool
	 */
	public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
	{		
		return $user->getId() === $this->getId();
	}

	public function getArray()
	{
		return [
			'id' => $this->getId(),
			'name' => $this->getName(),
			'avatar' => $this->getAvatar(),
			'color' => $this->getColor(),
		];
	}

	/**
	 * @param QueryBuilder $queryBuilder
	 * @param User $user
	 */
	/**
	 * @param QueryBuilder $queryBuilder
	 * @param User $user
	 * @param string $owner
	 */
	public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
	{
		$ownCondition = 'object = :ownerControl';

		if ($owner === 'own') {
			$queryBuilder->andWhere($ownCondition);
			$queryBuilder->setParameter('ownerControl', $user);
		}
	}

	/**
	 * @return Profile
	 */
	public function getProfile()
	{
		return $this->profile;
	}

	/**
	 * @param Profile $profile
	 */
	public function setProfile($profile): void
	{
		$this->profile = $profile;
	}

	/**
	 * @return float
	 */
	public function getWorked_hours()
	{
		return $this->worked_hours;
	}

	/**
	 * @param float $worked_hours
	 */
	public function setWorkedHours($worked_hours)
	{
		$this->worked_hours = $worked_hours;
	}

	public function getKeywords($em)
	{
		$associations = $em->getRepository('Keyword\Entity\Association')->findBy([
			'entity' => 'User\Entity\User',
			'primary' => $this->id,
		]);

		$keywords = new ArrayCollection();
		foreach ($associations as $association) {
			$keywords->add($association->getKeyword());
		}
		
		return $keywords;
	}
}
