(function(app) {
    app.factory('destinationService', [
        'restFactoryService',
        function (restFactoryService) {
            return restFactoryService.create('/api/budget/destination');
        },
    ]);
})(window.EVA.app);
