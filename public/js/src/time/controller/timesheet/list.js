(function (app) {

    app.controller('timesheetListController', [
        'timesheetService', '$scope', 'uiCalendarConfig', 'flashMessenger', 'translator', '$timeout', '$rootScope', '$http', 'userConfigurationService',
        function timesheetListController(timesheetService, $scope, uiCalendarConfig, flashMessenger, translator, $timeout, $rootScope, $http, userConfigurationService) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'user.id',
                    'user.name',
                    'user.avatar',
                    'user.color',
                    'user.keywords',
                    'user.keywords.hierarchySup',
                    'project.id',
                    'project.code',
                    'project.name',
                    'project.keywords',
                    'project.keywords.hierarchySup',
                    'project.hierarchySup.id',
                    'project.hierarchySup.name',
                    'start',
                    'end',
                    'hours',
                    'createdAt',
                    'updatedAt',
                    'type',
                    'absenceType.id',
                    'absenceType.name',
                    'keywords',
                    'description',
                    'territories',
                ],
            };

            self.calendarName = 'calendar' + moment().format('x');
            self.view = 'calendar';
            self.exportModel = null;
            self.customExport = {};
            self.timesheets = [];
            self.user_id = null;

            self.loading = {};

            self.multipleEdit = {
                use: {},
            };
            self.multipleEditErrors = {};

            var calendarBounds = {
                start: null,
                end: null,
            };
            var buttonSelected = 'month';
            self.calendar = {
                editable: true,
                header: {
                    left: 'month agendaWeek agendaDay',
                    center: 'title',
                    right: 'today prev,next',
                },
                lang: 'fr',
                buttonText: {
                    today: 'Aujourd\'hui',
                    month: 'Mois',
                    week: 'Semaine',
                    day: 'Jour',
                },
                selectable: true,
                selectHelper: true,
                select: function (start, end) {
                    if (uiCalendarConfig.calendars[self.calendarName].fullCalendar('getView').type === 'month') {
                        var startFormat = start.add(8, 'hours').format('DD/MM/YYYY HH:mm');
                        var endFormat = end.subtract(6, 'hours').format('DD/MM/YYYY HH:mm');
                    } else {
                        var startFormat = start.format('DD/MM/YYYY HH:mm');
                        var endFormat = end.format('DD/MM/YYYY HH:mm');
                    }

                    self.createTimesheetModal(startFormat, endFormat);
                },
                eventResize: function (event) {
                    self.alterTimesheet(event);
                },
                eventDrop: function (event) {
                    self.alterTimesheet(event);
                },
                eventClick: function (date) {
                    self.editTimesheetModal(date);
                },
                viewRender: function (view) {
                    calendarBounds.start = view.start;
                    calendarBounds.end = view.end;

                    $timeout(function () {
                        $(window).trigger('resize');
                        uiCalendarConfig.calendars[self.calendarName].fullCalendar('removeEvents');
                        $scope.__tb.loadData();

                        const monthButton = angular.element('.fc-month-button');
                        const weekButton = angular.element('.fc-agendaWeek-button');
                        const dayButton = angular.element('.fc-agendaDay-button');

                        monthButton.click(() => {
                            saveCalendarConfiguration('month');
                            buttonSelected = 'month';
                        });
                        weekButton.click(() => {
                            saveCalendarConfiguration('week');
                            buttonSelected = 'week';
                        });
                        dayButton.click(() => {
                            saveCalendarConfiguration('day');
                            buttonSelected = 'day';
                        });

                        switch (buttonSelected) {
                            case 'month':
                                monthButton.click();
                                break;
                            case 'week':
                                weekButton.click();
                                break;
                            case 'day':
                                dayButton.click();
                                break;
                        }
                    });
                },
            };

            var calendarEvents = [];
            self.eventSources = [
                {
                    events: function (start, end, timezone, callback) {
                        var events = [];
                        for (var i in calendarEvents) {
                            var timesheet = calendarEvents[i];
                            events.push({
                                id: timesheet.id,
                                title: `${timesheet.name}${timesheet.absenceType ? ' - ' + timesheet.absenceType.name : ''} - ${timesheet.project.code} - ${timesheet.project.name}`,
                                start: moment(timesheet.start, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm:ss'),
                                end: moment(timesheet.end, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm:ss'),
                                timesheet: timesheet,
                                editable: timesheet._rights.update,
                                color: timesheet.user.color,
                            });
                        }

                        callback(events);
                    },
                },
            ];

            self.options = {
                name: 'timesheets-list',
                col: apiParams.col,
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    if (self.queryHome != null) {
                        params.search.data.filters = self.queryHome.filters;
                    }

                    var p = angular.copy(params);

                    if (self.view === 'calendar') {
                        delete p.search.data.page;
                        delete p.search.data.limit;

                        p.search.data.filters.start = {
                            op: 'calendar',
                            val: {
                                start: calendarBounds.start.format('DD/MM/YYYY'),
                                end: calendarBounds.end.format('DD/MM/YYYY'),
                                col: 'end',
                            },
                        };

                        timesheetService.findAll(p, abort.promise).then(function (res) {
                            callback(res);

                            calendarEvents = res.rows;
                            uiCalendarConfig.calendars[self.calendarName].fullCalendar('refetchEvents');

                        });
                    } else {
                        timesheetService.findAll(p, abort.promise).then(function (res) {
                            callback(res);
                        });
                    }
                    params.search.data.nbFilters = Object.keys(params.search.data.filters).length;
                },
            };

            self.init = function (user_id) {
                self.user_id = user_id;
                userConfigurationService.getTable('timesheets-calendar').then(function (res) {
                    if (res.button) {
                        buttonSelected = res.button;

                        const monthButton = angular.element('.fc-month-button');
                        const weekButton = angular.element('.fc-agendaWeek-button');
                        const dayButton = angular.element('.fc-agendaDay-button');

                        switch (buttonSelected) {
                            case 'month':
                                monthButton.click();
                                break;
                            case 'week':
                                weekButton.click();
                                break;
                            case 'day':
                                dayButton.click();
                                break;
                        }
                    }
                });
                if (!self.query) {
                    self.query = {
                        filters: {
                            user: {
                                op: 'eq',
                                val: [user_id],
                            },
                            type: {
                                op: 'eq',
                                val: ['done'],
                            },
                        },
                    };
                    $timeout(function () {
                        $scope.load(self.query);
                        self.query = null;
                    }, 1000);
                }
            };

            self.initHome = function (name, query, view, user_id) {
                self.options.name = name;
                self.queryHome = query;
                self.view = view;
                self.user_id = user_id;
            };

            self.alterTimesheet = function (event) {
                var timesheet = event.timesheet;
                timesheet.start = moment(event.start).format('DD/MM/YYYY HH:mm');
                timesheet.end = moment(event.end).format('DD/MM/YYYY HH:mm');

                timesheetService.save(timesheet).then(function () {
                    $rootScope.$broadcast('refresh-all-calendars', self.calendarName);
                }, function (err) {
                    for (var field in err.fields) {
                        for (var message in err.fields[field]) {
                            flashMessenger.error(field + ' : ' + err.fields[field][message]);
                        }
                    }
                });
            };

            self.showExportModal = function () {
                angular.element('#exportModal').modal('show');
            };

            self.export = function () {
                if (self.exportModel == 'default') {
                    $scope.__tb.exportExcel();
                    self.exportModel = null;
                    angular.element('#exportModal').modal('hide');
                } else {
                    angular.element('#exportForm').submit();
                    self.exportModel = null;
                    angular.element('#exportModal').modal('hide');
                }
            };

            self.isCustomExport = function () {
                return /[a-zA-Z]+/.test(self.exportModel)
                    && self.exportModel !== 'default';
            };

            self.createTimesheetModal = function (start, end) {
                var calendarName = self.calendarName;
                $scope.$broadcast('create-timesheet-modal', start, end, calendarName, self.user_id);
            };

            self.editTimesheetModal = function (date) {
                var calendarName = self.calendarName;
                var timesheet = date.timesheet;
                $scope.$broadcast('edit-timesheet-modal', timesheet, calendarName);
            };

            self.showMultipleEditModal = function () {
                angular.element('#multipleEditModal').modal('show');
            };

            self.deleteMultiple = function () {
                self.multipleEditErrors = {};

                var timesheets = [];
                for (var i in self.timesheets) {
                    if (self.timesheets[i].selected) {
                        timesheets.push(self.timesheets[i]);
                    }
                }
                timesheetService.removeAll(timesheets, { col: self.options.col }).then(function (res) {
                    if (confirm(translator('timesheet_question_deletes'))) {
                        var success = false;
                        for (var i in res) {
                            if (res[i].success) {
                                success = true;
                                timesheets[i] = angular.extend(timesheets[i], res[i].object);
                            } else {
                                if (res[i].errors.fields instanceof Array && res[i].errors.fields.length === 0) {
                                    res[i].errors.fields = {};
                                    res[i].errors.fields[translator('right_errors')] = {
                                        update: translator('right_delete_denied'),
                                    };
                                }

                                for (var field in res[i].errors.fields) {
                                    var fieldTranslation;
                                    if (
                                        ['type'].indexOf(field) !== -1 ||
                                        /^timesheet_field/.test(translator('timesheet_field_' + field))
                                    ) {
                                        fieldTranslation = field;
                                    } else if (['absenceType'].indexOf(field) !== -1 ||
                                        /^timesheet_field/.test(translator('timesheet_field_' + field))) {

                                    } else {
                                        fieldTranslation = translator('timesheet_field_' + field);
                                    }

                                    if (!self.multipleEditErrors[fieldTranslation]) {
                                        self.multipleEditErrors[fieldTranslation] = {};
                                    }

                                    var timesheetName =
                                        timesheets[i].id +
                                        ' - ' +
                                        (timesheets[i].code ? timesheets[i].code + ' - ' : '') +
                                        (timesheets[i].name ? timesheets[i].name : '');

                                    self.multipleEditErrors[fieldTranslation][timesheetName] = [];

                                    for (var error in res[i].errors.fields[fieldTranslation]) {
                                        self.multipleEditErrors[fieldTranslation][timesheetName].push(
                                            res[i].errors.fields[fieldTranslation][error],
                                        );
                                    }
                                }
                            }
                        }

                        if (success) {
                            flashMessenger.success(translator('timesheet_message_delete_at_least_one'));
                        } else {
                            flashMessenger.error(translator('timesheet_message_delete_all_errors'));
                        }
                        $scope.__tb.removeFromList();
                        self.loading.multipleEdit = false;
                    }
                })
            }

            self.doMultipleEdit = function () {
                self.loading.multipleEdit = true;
                self.multipleEditErrors = {};

                var timesheets = [];
                for (var i in self.timesheets) {
                    if (self.timesheets[i].selected) {
                        timesheets.push(self.timesheets[i]);
                    }
                }

                var timesheetsCopy = [];
                angular.copy(timesheets, timesheetsCopy);

                for (i in timesheetsCopy) {
                    for (var attribute in self.multipleEdit) {
                        if (self.multipleEdit.use[attribute]) {
                            timesheetsCopy[i][attribute] = self.multipleEdit[attribute];
                        }
                    }
                }
                timesheetService.saveAll(timesheetsCopy, { col: self.options.col }).then(function (res) {
                    var success = false;
                    for (var i in res) {
                        if (res[i].success) {
                            success = true;
                            timesheets[i] = angular.extend(timesheets[i], res[i].object);
                        } else {
                            if (res[i].errors.fields instanceof Array && res[i].errors.fields.length === 0) {
                                res[i].errors.fields = {};
                                res[i].errors.fields[translator('right_errors')] = {
                                    update: translator('right_update_denied'),
                                };
                            }

                            for (var field in res[i].errors.fields) {
                                var fieldTranslation;
                                if (
                                    ['type'].indexOf(field) !== -1 ||
                                    /^timesheet_field/.test(translator('timesheet_field_' + field))
                                ) {
                                    fieldTranslation = field;
                                } else if (['absenceType'].indexOf(field) !== -1 ||
                                    /^timesheet_field/.test(translator('timesheet_field_' + field))) {

                                } else {
                                    fieldTranslation = translator('timesheet_field_' + field);
                                }

                                if (!self.multipleEditErrors[fieldTranslation]) {
                                    self.multipleEditErrors[fieldTranslation] = {};
                                }

                                var timesheetName =
                                    timesheets[i].id +
                                    ' - ' +
                                    (timesheets[i].code ? timesheets[i].code + ' - ' : '') +
                                    (timesheets[i].name ? timesheets[i].name : '');

                                self.multipleEditErrors[fieldTranslation][timesheetName] = [];

                                for (var error in res[i].errors.fields[fieldTranslation]) {
                                    self.multipleEditErrors[fieldTranslation][timesheetName].push(
                                        res[i].errors.fields[fieldTranslation][error],
                                    );
                                }
                            }
                        }
                    }

                    if (success) {
                        flashMessenger.success(translator('timesheet_message_saved_at_least_one'));
                    } else {
                        flashMessenger.error(translator('timesheet_message_saved_all_errors'));
                    }

                    self.loading.multipleEdit = false;
                });
            };
            $scope.$on('delete-timesheet-modal', function (event, timesheet, calendarName) {
                if (self.calendarName === calendarName) {
                    for (var i in calendarEvents) {
                        if (calendarEvents[i].id === timesheet.id) {
                            calendarEvents.splice(calendarEvents.indexOf(calendarEvents[i]), 1);
                        }
                    }
                    $rootScope.$broadcast('refresh-all-calendars', self.calendarName);
                    uiCalendarConfig.calendars[self.calendarName].fullCalendar('refetchEvents');
                }
            });

            $scope.$on('save-timesheet-modal', function (event, timesheet, calendarName, isCreation) {
                if (self.calendarName === calendarName) {
                    if (isCreation) {
                        calendarEvents.push(timesheet);
                    } else {
                        for (var i = 0; i < calendarEvents.length; i++) {
                            if (timesheet.id === calendarEvents[i].id) {
                                calendarEvents[i] = timesheet;
                                break;
                            }
                        }
                    }
                    $rootScope.$broadcast('refresh-all-calendars', self.calendarName);
                    uiCalendarConfig.calendars[self.calendarName].fullCalendar('refetchEvents');
                }
            });

            self.editedLine = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};

            self.editLine = function editLine(timesheet) {
                if (angular.isArray(timesheet.keywords) || timesheet.keywords == null) {
                    timesheet.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine = timesheet;
                self.editedLineFields = angular.copy(timesheet);
            };

            self.saveEditedLine = function saveEditedLine(timesheet) {
                self.editedLineErrors = {};
                timesheetService.save(angular.extend({}, timesheet, self.editedLineFields), {
                    col: self.options.col,
                }).then(function (res) {
                    if (res.success) {
                        timesheet = angular.extend(timesheet, res.object);
                        self.editedLine = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('timesheet_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                });
            };

            self.duplicate = function duplicate(timesheet) {
                $http({
                    method: 'POST',
                    url: '/timesheet/duplicate/' + timesheet.id,
                }).then(function (res) {
                    if (res.data.success) {
                        timesheetService.find(res.data.timesheet.id, apiParams).then(function (res) {
                            var index = self.timesheets.findIndex(function (element) {
                                return element.id === timesheet.id;
                            });
                            self.timesheets.splice(index + 1, 0, res.object);
                        });
                    } else {
                        for (var i in res.data.error) {
                            flashMessenger.error(res.data.error[i]);
                        }
                    }
                });
            };

            self.deleteTimesheet = function deleteTimesheet(timesheet) {
                if (confirm(translator('timesheet_question_delete').replace('%1', timesheet.name))) {
                    timesheetService.remove(timesheet).then(function (res) {
                        if (res.success) {
                            self.timesheets.splice(self.timesheets.indexOf(timesheet), 1);
                            flashMessenger.success(translator('timesheet_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function (event) {
                if (event.which === 13) {
                    self.fullTextSearch();
                }
            });

            self.createTimesheet = function () {
                $scope.$broadcast('create-timesheet');
            };

            $rootScope.$on('refresh-all-calendars', function (event, calendarName) {
                if (self.calendarName !== calendarName) {
                    $scope.__tb.loadData();
                }
            });

            $scope.$watch(function () {
                return self.view;
            }, function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    self.options.pager = self.view === 'table';
                    $scope.__tb.loadData();
                }
            });

            $scope.$watch(function () {
                if (typeof self.timesheet !== 'undefined') {
                    return self.timesheet.end;
                }
            }, function () {
                if (typeof self.timesheet !== 'undefined') {
                    self.controlDates();
                }
            });

            $scope.$watch(function () {
                if (typeof self.timesheet !== 'undefined') {
                    return self.timesheet.start;
                }
            }, function () {
                if (typeof self.timesheet !== 'undefined') {
                    self.controlDates();
                }
            });

            $scope.$watch(function () {
                if (typeof $scope.__tb.apiParams.search.data.filters.user !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.user.val !== 'undefined'
                ) {
                    return $scope.__tb.apiParams.search.data.filters.user.val || null;
                }
            }, function () {
                if (typeof $scope.__tb.apiParams.search.data.filters.user !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.user.val !== 'undefined'
                ) {
                    if (angular.isArray($scope.__tb.apiParams.search.data.filters.user.val)) {
                        self.customExport.user = $scope.__tb.apiParams.search.data.filters.user.val[0];
                    } else {
                        self.customExport.user = $scope.__tb.apiParams.search.data.filters.user.val;
                    }
                }
            }, true);

            $scope.$watch(function () {
                if (typeof $scope.__tb.apiParams.search.data.filters.start !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val.start !== 'undefined'
                ) {
                    return $scope.__tb.apiParams.search.data.filters.start.val.start || null;
                }
            }, function () {
                if (typeof $scope.__tb.apiParams.search.data.filters.start !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val.start !== 'undefined'
                ) {
                    self.customExport.start = $scope.__tb.apiParams.search.data.filters.start.val.start;
                }
            }, true);

            $scope.$watch(function () {
                if (typeof $scope.__tb.apiParams.search.data.filters.start !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val.end !== 'undefined'
                ) {
                    return $scope.__tb.apiParams.search.data.filters.start.val.end;
                }
            }, function () {
                if (typeof $scope.__tb.apiParams.search.data.filters.start !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val !== 'undefined'
                    && typeof $scope.__tb.apiParams.search.data.filters.start.val.end !== 'undefined'
                ) {
                    self.customExport.end = $scope.__tb.apiParams.search.data.filters.start.val.end;
                }
            }, true);

            function saveCalendarConfiguration(button) {
                userConfigurationService
                    .saveTable('timesheets-calendar', { button })
                    .then(function (res) { });
            }

            /*
            self.customExport.start = $scope.__tb.apiParams.search.data.filters.start.val.start;
            self.customExport.end = $scope.__tb.apiParams.search.data.filters.start.val.end;
*/
        },
    ]);

})(window.EVA.app);
