(function(app) {
    app.factory('managementUnitService', [
        'restFactoryService',
        function (restFactoryService) {
            return restFactoryService.create('/api/budget/management-unit');
        },
    ]);
})(window.EVA.app);
