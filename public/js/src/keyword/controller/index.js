(function(app) {

    app.controller('keywordIndexController', [
        'groupService', 'keywordService', 'treeBuilderService', 'flashMessenger', 'translator',
        function keywordIndexController(groupService, keywordService, treeBuilderService, flashMessenger, translator) {
            var self = this;

            self.type   = 'keyword';
            self.groups = [];
            self.fusion = {
                type: 'duplicate'
            };
            self.loading = {
                save: false
            };

            var groupsApiParams   = {
                col: [
                    'id',
                    'name',
                    'type',
                    'entities',
                    'multiple',
                    'archived'
                ],
                search: {
                    type: 'list',
                    data: {
                        sort: 'name',
                        order: 'asc',
                        filters: {
                            archived: {
                                op: 'eq',
                                val: ['0', '1']
                            }
                        }
                    }
                }
            };

            var kwApiParams = {
                col: [
                    'id',
                    'name',
                    'group.id',
                    'group.name',
                    'parents.id',
                    'parents.name',
                    'description',
                    'order',
                    'archived'
                ]
            };

            self.init = function init(projectActive){
                self.projectActive = projectActive;
                groupService.findAll(groupsApiParams).then(function(res) {
                    self.groups = res.rows;
                });
            };

            self.setGroupType = function setGroupType(type) {
                self.type = type;
            };

            self.updateEntity = function updateEntity(group, entity, save) {
                var index = group.entities.indexOf(entity);
                if (index > -1) {
                    group.entities.splice(index, 1);
                } else {
                    group.entities.push(entity);
                }

                if (save) {
                    self.saveGroup(group);
                }
            };

            self.saveGroup = function saveGroup(group) {
                group.errors = { fields : {}};
                group.loading = true;

                self.loading.save = true;

                var isCreation = group.id == null;

                groupService.save(group, groupsApiParams).then(function(res) {
                    if (res.success) {
                        //group.edit = false;
                        if (isCreation) {
                            group.id = res.object.id;
                            group._rights = res.object._rights;

                            self.groups.push(group);
                        } else {
                            for (var i in self.groups) {
                                if (self.groups[i].id == res.object.id) {
                                    if (self.groups[i].loaded) {
                                        res.object.loaded   = true;
                                        res.object.keywords = self.groups[i].keywords;
                                        res.object.open     = self.groups[i].open;
                                    }
                                    self.groups[i] = res.object;

                                    break;
                                }
                            }
                        }
                        flashMessenger.success(translator('group_message_saved'));
                        group.errors = { fields : {}};
                        angular.element('#groupFormModal').modal('hide');
                    }
                    group.loading = false;
                    self.loading.save = false;

                }, function (err) {
                    for (var field in err.fields) {
                        group.errors.fields[field] = err.fields[field];
                    }
                    group.loading = false;
                    self.loading.save = false;
                });
            };

            self.deleteGroup = function deleteGroup(group) {
                if (confirm(translator('group_question_delete').replace('%1', group.name))) {
                    groupService.remove(group).then(function(res) {
                        if (res.success) {
                            self.groups.splice(self.groups.indexOf(group), 1);
                            flashMessenger.success(translator('group_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.editGroup = function editGroup(group) {
                angular.element('#groupFormModal').modal('show');
                self.editedGroup = angular.copy(group);
                if (!group.id) {
                    self.panelTitle = translator('group_nav_form');
                } else {
                    self.panelTitle = group.name;
                }

            };


            self.archiveGroup = function (group) {
                group.archived = !group.archived;
                groupService.save(group, groupsApiParams).then(function (res) {
                    if (res.success) {
                        group.archived ? flashMessenger.success(translator('group_message_archived')) : flashMessenger.success(translator('group_message_unarchived'));
                    } else {
                        flashMessenger.error(translator('error_occured'));
                        group.archived = !group.archived;
                    }
                });
            };

            self.createGroup = function createGroup() {
                var group = {
                    _rights: {
                        update: true
                    },
                    type: self.type,
                    mulitple: false,
                    entities: []
                };

                if(self.type == 'reference'){
                    group.entities = ['project', 'indicator'];
                }

                self.editGroup(group);
            };

            self.openGroup = function openGroup(group) {
                self.loadElements(group);
            };

            self.loadElements = function loadElements(group) {
                if (!group.loaded) {
                    group.loading = true;

                    var apiParams = angular.extend({}, kwApiParams, {
                        search: {
                            type: 'list',
                            data: {
                                sort: 'order',
                                order: 'asc',
                                filters: {
                                    group: {
                                        op: 'eq',
                                        val: group.id
                                    },
                                    archived: {
                                        op: 'eq',
                                        val: ['0', '1']
                                    }
                                }
                            }
                        }
                    });

                    keywordService.findAll(apiParams).then(function(res) {
                        group.keywords = treeBuilderService.toNNTree(res.rows);
                        group.loaded   = true;
                        group.loading  = false;
                    }, function () {
                        group.loading = false;
                    })
                }
            };

            self.editKeyword = function editKeyword(keyword) {
                angular.element('#keywordFormModal').modal('show');
                self.editedKeyword = angular.copy(keyword);
                if (!keyword.id) {
                    self.panelTitle = translator('keyword_nav_form');
                } else {
                    self.panelTitle = keyword.name;
                }
            };

            self.saveKeyword = function saveKeyword(keyword) {
                keyword.errors = { fields : {}};
                self.loading.save = true;

                keywordService.save(keyword, kwApiParams).then(function(res) {
                    if (res.success) {

                        for (var i in self.groups) {
                            if (self.groups[i].id == res.object.group.id) {
                                self.groups[i].loaded = false;
                                self.loadElements(self.groups[i]);
                                break;
                            }
                        }

                        flashMessenger.success(translator('keyword_message_saved'));
                        keyword.errors = { fields : {}};
                        angular.element('#keywordFormModal').modal('hide');
                    }
                    self.loading.save = false;

                }, function (err) {
                    for (var field in err.fields) {
                        keyword.errors.fields[field] = err.fields[field];
                    }
                    self.loading.save = false;
                });
            };

            self.archiveKeyword = function (keyword) {
                keyword.archived = !keyword.archived;
                keywordService.save(keyword, kwApiParams).then(function (res) {
                    if (res.success) {
                        keyword.archived ? flashMessenger.success(translator('keyword_message_archived')) : flashMessenger.success(translator('keyword_message_unarchived'));
                    } else {
                        flashMessenger.error(translator('error_occured'));
                        keyword.archived = !keyword.archived;
                    }
                });
            };

            self.deleteKeyword = function deleteKeyword(keyword) {
                if (confirm(translator('keyword_question_delete').replace('%1', keyword.name))) {
                    keywordService.remove(keyword).then(function(res) {
                        if (res.success) {
                            for (var i in self.groups) {
                                if (self.groups[i].id == keyword.group.id) {
                                    self.groups[i].loaded = false;
                                    self.loadElements(self.groups[i]);
                                    break;
                                }
                            }
                            flashMessenger.success(translator('keyword_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.createKeyword = function createKeyword(group) {
                group.open = true;
                self.loadElements(group);
                var keyword = {
                    _rights: {
                        update: true
                    },
                    parents: [],
                    group: {
                        id: group.id
                    },
                    order: 0,
                };
                self.editKeyword(keyword);
            };

            self.showFusionModal = function (keywordToFusion) {
                angular.element('#fusionModal').modal('show');
                self.fusion.keywordToFusion = keywordToFusion;
            };

            self.doFusion = function () {
                if (
                    !self.fusion.keywordToKeep ||
                    !self.fusion.keywordToFusion ||
                    !self.fusion.type
                ) {
                    flashMessenger.error(translator('error_occured'));
                    return;
                }

                self.loading.fusion = true;

                keywordService
                    .fusion(self.fusion.keywordToKeep.id, self.fusion.keywordToFusion.id, self.fusion.type)
                    .then(function (res) {
                        self.loading.fusion = false;
                        angular.element('#fusionModal').modal('hide');

                        if (res.success) {
                            flashMessenger.success(translator('fusion_message_success'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
            };
        }
    ]);

})(window.EVA.app);
