<?php

namespace Indicator\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class GroupIndicatorController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('indicator:e:group-indicator:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('indicator:e:group-indicator', $id)) {
            return $this->notFoundAction();
        }

        $groupindicator = $this->entityManager()->getRepository('Indicator\Entity\GroupIndicator')->find($id);

        if ($groupindicator && 
        !$this->acl()->ownerControl('indicator:e:group-indicator:u', $groupindicator) && 
        !$this->acl()->ownerControl('indicator:e:group-indicator:r', $groupindicator)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
			'isMaster' => $this->environment()
				->getClient()
				->isMaster(),
            'groupindicator' => $groupindicator
        ]);
    }
}
