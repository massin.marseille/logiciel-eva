<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class ManagementUnitController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:managementUnit:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        if (!$this->acl()->basicFormAccess('budget:e:managementUnit', $id)) {
            return $this->notFoundAction();
        }

        $managementUnit = $this->entityManager()
            ->getRepository('Budget\Entity\ManagementUnit')
            ->find($id);

        if (
            $managementUnit &&
            !$this->acl()->ownerControl('budget:e:managementUnit:u', $managementUnit) &&
            !$this->acl()->ownerControl('budget:e:managementUnit:r', $managementUnit)
        ) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'managementUnit' => $managementUnit,
        ]);
    }
}
