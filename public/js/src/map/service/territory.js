(function(app) {

    app.factory('territoryService', [
        'restFactoryService', '$http', '$q',
        function (restFactoryService, $http, $q) {
            var url = '/api/map/territory';

            var methods = restFactoryService.create(url);

            methods.fusion = function (territoryToKeep_id, territories_id) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: url + '/' + territoryToKeep_id + '/fusion',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({ territories_id : territories_id })
                }).then(function(res) {
                    deferred.resolve(res.data)
                }, function(err) {
                    deferred.reject(err)
                });

                return deferred.promise;
            };

            return methods;
        }
    ])

})(window.EVA.app);
