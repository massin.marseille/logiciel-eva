<?php

namespace Convention\Entity;

use Budget\Entity\Expense;
use Core\Entity\BasicRestEntityAbstract;
use Directory\Entity\Structure;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;
use Laminas\InputFilter\OptionalInputFilter;

/**
 * Class Convention
 *
 * @package Convention\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="convention_convention")
 */
class Convention extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $networkAccessible = false;

    /**
     * @var Structure
     *
     * @ORM\ManyToOne(targetEntity="Directory\Entity\Structure")
     */
    protected $contractor;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $end;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $progress;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Convention\Entity\Line", mappedBy="convention", cascade={"all"})
     */
    protected $lines;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $numberArr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $decisionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $notificationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $returnDate;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $amount;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $amountSubv;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User\Entity\User")
     */
    protected $members;

    public function __construct()
    {
        $this->lines   = new ArrayCollection();
        $this->members = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isNetworkAccessible()
    {
        return $this->getNetworkAccessible();
    }

    /**
     * @return boolean
     */
    public function getNetworkAccessible()
    {
        return $this->networkAccessible;
    }

    /**
     * @param boolean $networkAccessible
     */
    public function setNetworkAccessible($networkAccessible)
    {
        $this->networkAccessible = $networkAccessible;
    }

    /**
     * @return Structure
     */
    public function getContractor()
    {
        return $this->contractor;
    }

    /**
     * @param Structure $contractor
     */
    public function setContractor($contractor)
    {
        $this->contractor = $contractor;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param int $progress
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return ArrayCollection
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @param ArrayCollection $lines
     */
    public function setLines($lines)
    {
        $this->lines = $lines;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getNumberArr()
    {
        return $this->numberArr;
    }

    /**
     * @param string $numberArr
     */
    public function setNumberArr($numberArr)
    {
        $this->numberArr = $numberArr;
    }

    /**
     * @return \DateTime
     */
    public function getDecisionDate()
    {
        return $this->decisionDate;
    }

    /**
     * @param \DateTime $decisionDate
     */
    public function setDecisionDate($decisionDate)
    {
        $this->decisionDate = $decisionDate;
    }

    /**
     * @return \DateTime
     */
    public function getNotificationDate()
    {
        return $this->notificationDate;
    }

    /**
     * @param \DateTime $notificationDate
     */
    public function setNotificationDate($notificationDate)
    {
        $this->notificationDate = $notificationDate;
    }

    /**
     * @return \DateTime
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * @param \DateTime $returnDate
     */
    public function setReturnDate($returnDate)
    {
        $this->returnDate = $returnDate;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getAmountSubv()
    {
        return $this->amountSubv;
    }

    /**
     * @param float $amountSubv
     */
    public function setAmountSubv($amountSubv)
    {
        $this->amountSubv = $amountSubv;
    }

    /**
     * @return ArrayCollection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param ArrayCollection $members
     */
    public function setMembers($members)
    {
        $this->members = $members;
    }

    public function addMembers($members)
    {
        foreach ($members as $member) {
            $this->members->add($member);
        }
    }

    public function removeMembers($members)
    {
        foreach ($members as $member) {
            $this->members->removeElement($member);
        }
    }

    public function getPartTotalProjects()
    {
        $amount = 0;
        foreach ($this->lines as $line) {
            $amount += $line->getPartTotalProject();
        }

        return $amount;
    }

    public function getTimeSpent()
    {
        $amount = 0;
        foreach ($this->lines as $line) {
            $amount += $line->getTimeSpent();
        }

        return $amount;
    }

    public function getAutofinancement()
    {
        $amount = 0;
        foreach ($this->lines as $line) {
            $amount += $line->getAutofinancement();
        }

        return $amount;
    }

    public function getFinancers()
    {
        $financers  = new ArrayCollection();
        foreach ($this->lines as $line) {
            $projectFinancers = $line->getProject()->getFinancers();
            foreach ($projectFinancers as $financer) {
                if (!$financers->contains($financer)) {
                    $financers->add($financer);
                }
            }
        }
        return $financers;
    }

    protected function getAmountExpense($type)
    {
        $amount = 0;
        foreach ($this->lines as $line) {
            $amount += $line->{'getAmountExpense' . ucfirst($type)}();
        }

        return $amount;
    }

    protected function getAmountHTExpense($type)
    {
        $amount = 0;
        foreach ($this->lines as $line) {
            $amount += $line->{'getAmountHTExpense' . ucfirst($type)}();
        }

        return $amount;
    }

    public function __call($name, $arguments)
    {
        foreach (Expense::getTypes() as $type) {
            if ($name === 'getAmountExpense' . ucfirst($type)) {
                return $this->getAmountExpense($type);
            } else if ($name === 'getAmountHTExpense' . ucfirst($type)) {
                return $this->getAmountHTExpense($type);
            }
        }

        return null;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();

        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'contractor',
                'required' => false,
                'allow_empty' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                $contractor = null;
                                if (is_array($value)) {
                                    $contractor = $entityManager->getRepository('Directory\Entity\Structure')->find($value['id']);
                                } else if (is_numeric($value)) {
                                    $contractor = $entityManager->getRepository('Directory\Entity\Structure')->find($value);
                                } else if (is_string($value)) {
                                    $contractor = $entityManager->getRepository('Directory\Entity\Structure')->findBy(array('name' => $value));

                                    if (sizeof($contractor) == 0) {
                                        $contractor = null;
                                    }
                                }
                                return $contractor !== null;
                            },
                            'message' => 'convention_field_contractor_error_non_exists'
                        ],
                    ]
                ],
            ],
            [
                'name'        => 'networkAccessible',
                'required'    => false,
                'allow_empty' => true,
                'filters' => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ],
            ],
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'description',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'number',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'numberArr',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'start',
                'required' => false,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'     => 'end',
                'required' => false,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'       => 'progress',
                'required'   => false,
                'allow_empty' => true,
                'filters'    => [
                    ['name' => 'NumberParse'],
                ],
                'continue_if_empty' => false,
                'validators' => [
                    [
                        'name'    => 'Between',
                        'options' => [
                            'min' => 0,
                            'max' => 100,
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'decisionDate',
                'required' => false,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'     => 'notificationDate',
                'required' => false,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'     => 'returnDate',
                'required' => false,
                'filters' => [
                    ['name' => 'Core\Filter\DateTimeFilter'],
                ],
            ],
            [
                'name'     => 'amount',
                'required' => false,
                'allow_empty' => true,
                'continue_if_empty' => false,
                'filters'  => [
                    ['name' => 'NumberParse']
                ]
            ],
            [
                'name'     => 'amountSubv',
                'required' => false,
                'allow_empty' => true,
                'continue_if_empty' => false,
                'filters'  => [
                    ['name' => 'NumberParse']
                ]
            ],
            [
                'name'     => 'members',
                'required' => false
            ],
            [
                'name'     => 'projects',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_string($value)) {
                                    foreach (explode(" -- ", $value) as $id) {
                                        $project = $entityManager->getRepository('Project\Entity\Project')->find($id);

                                        if ($project === null) {
                                            return false;
                                        }
                                    }
                                }
                                return true;
                            },
                            'message' => 'convention_field_project_error_non_exists',
                        ],
                    ],
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                $isCorrect = true;
                                if (is_string($value)) {
                                    $value = ' ' . $value . ' ';
                                    foreach (explode("--", $value) as $val) {
                                        if ($val[0] == " " && $val[strlen($val) - 1] == " ") {
                                            if (strpos($val, ',') !== false) {
                                                $val2 = explode(',', $val);
                                                if ($val2[0][strlen($val2[0]) - 1] == " " || $val2[1][0] == " ") {
                                                    $isCorrect = false;
                                                }
                                            }
                                        } else {
                                            $isCorrect = false;
                                        }
                                    }
                                }
                                return $isCorrect;
                            },
                            'message' => 'convention_field_project_syntax_error',
                        ],
                    ],
                ],
            ],
        ]);

        return $inputFilter;
    }
}
