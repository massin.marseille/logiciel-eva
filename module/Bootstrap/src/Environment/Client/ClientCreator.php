<?php

namespace Bootstrap\Environment\Client;

use Application\Entity\Configuration;
use Bootstrap\Entity\Client;
use Doctrine\ORM\Tools\SchemaTool;
use User\Entity\Role;
use User\Entity\User;
use Laminas\I18n\Translator\TextDomain;
use Laminas\ServiceManager\ServiceLocatorInterface;

class ClientCreator
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * Service Locator
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @param Client $client
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(Client $client, ServiceLocatorInterface $serviceLocator)
    {
        $this->client         = $client;
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Create the database for a client
     *
     * @return self
     * @since Version 0.1
     * @version 0.1
     */
    public function createDatabase()
    {
        $entityManagerFactory = $this->serviceLocator->get('EntityManagerFactory');
        $moduleManager        = $this->serviceLocator->get('MyModuleManager');

        try {
            /**
             * Create the database
             */
            $pdo = new \PDO(
                'mysql:host=' . $this->client->getDbHost() . ';port=' . $this->client->getDbPort(),
                $this->client->getDbUser(),
                $this->client->getDbPassword()
            );
            $sql       = "CREATE DATABASE IF NOT EXISTS " . $this->client->getDbName() . " DEFAULT CHARACTER SET = 'utf8'";
            $statement = $pdo->prepare($sql);
            $statement->execute();
            $statement = null;
            $pdo       = null;

            /**
             * The entity manager of the client database. Must use
             * it to add entities into its own database.
             */
            $clientEm = $entityManagerFactory->getEntityManager($this->client);

            /**
             * Update of the client database with the
             */
            $schemaTool = new SchemaTool($clientEm);
            $schemaTool->updateSchema($clientEm->getMetadataFactory()->getAllMetadata());

            /**
             * Adding Role and Admin
             */
            $clientModules = $this->client->getModules();
            $adminRights   = $moduleManager->getModulesAdminRights($clientModules);

            $adminRole = new Role();
            $adminRole->setName('Administrateur');
            $adminRole->setRights($adminRights);
            $adminRole->setCreatedAt(new \DateTime());

            $admin = new User();
            $admin->setRole($adminRole);
            $admin->setGender('m');
            $admin->setFirstName('Admin');
            $admin->setLastName('Admin');
            $admin->setLogin('admin');
            $admin->setPassword('admin');
            $admin->setCreatedAt(new \DateTime());

            $clientEm->persist($adminRole);
            $clientEm->persist($admin);

            /**
             * Adding configuration
             */
            $configuration = new Configuration();
            $clientEm->persist($configuration);

            $clientEm->flush();
            $clientEm->close();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function createFolders()
    {
        $folder = $this->client->getFolder();
        if (!file_exists($folder)) {
            //throw new \Exception('Client folder already exists.', 1);
            mkdir($folder, 0777);
            mkdir($folder . '/avatar', 0777);
        }

        return $this;
    }

    public function createJSLanguage()
    {
        $this->createFolders();

        $translator = $this->serviceLocator->get('Translator');
        $defaultTextDomain = $translator->getAllMessages();
        $defaultTranslations = $defaultTextDomain->getArrayCopy();

        $clientTranslations = $this->client->getTranslations();
        $clientTextDomain = new TextDomain($clientTranslations);

        $translator->getAllMessages()->merge($clientTextDomain);

        $translate = $this->serviceLocator->get('ViewHelperManager')->get('translate');
        $allTranslations = $translator->getAllMessages();
        foreach ($allTranslations as $key => $aTranslation) {
            $allTranslations[$key] = $translate->__invoke($aTranslation);
        }

        file_put_contents($this->client->getFolder() . '/language.js', 'var translations = ' . json_encode($allTranslations));

        // Reset translator
        $translator->getAllMessages()->exchangeArray($defaultTranslations);
    }
}
