(function (app) {

    app.controller('exportFormPublipostagesController', [
        'exportService', 'attachmentService', '$scope', 'flashMessenger', 'translator',
        function exportFormPublipostagesController(exportService, attachmentService, $scope, flashMessenger, translator) {
            var self       = this;
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'templates.id',
                    'templates.content',
                    'reference',
                    'year',
                    'type',
                    'showArbo',
                    'dataArbo',
                    'groupByKeywords',
                    'groupByKeywordsTitles'
                ]
            };

            self.cols = [];

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(id, idClient, type) {
                if (id) {
                    exportService.find(id, apiParams).then(function (res) {
                        self.export = res.object;
                        contentToCols(self.export.templates[0].content);
                        self.panelTitle = self.export.name;

                        self.currentTemplate = self.export.templates[0];
                    })
                } else {
                    self.export          = {
                        _rights  : {
                            update: true
                        },
                        reference: 'initial',
                        templates: [{}],
                        type     : type,
                        showArbo : 'no',
                        dataArbo : null,
                        groupByKeywords: false,
                        groupByKeywordsTitles: null
                    };
                    self.currentTemplate = self.export.templates[0];
                }
            };

            self.saveExport = function saveExport() {
                self.errors.fields = {};
                self.loading.save  = true;
                self.save();
            };

            self.save = function save() {
                var isCreation = !self.export.id;

                if (self.export.year == 0) {
                    self.export.year = '';
                }

                self.export.templates[0].content = '';
                for (var i in self.cols) {
                    self.export.templates[0].content += objToVar(self.cols[i]);
                }

                exportService.save(self.export, apiParams).then(function (res) {
                    self.export       = res.object;
                    self.panelTitle   = self.export.name;
                    self.loading.save = false;
                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.export.id;
                        history.replaceState('', '', url);
                    }

                    self.currentTemplate = self.export.templates[0];

                    flashMessenger.success(translator('export_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteExport = function deleteExport() {
                if (confirm(translator('export_question_delete').replace('%1', self.export.name))) {
                    exportService.remove(self.export).then(function (res) {
                        if (res.success) {
                            window.location = '/export';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };



            self.variablesParams     = {};
            self.variableTranslation = null;
            self.configureVariable   = function configureVariable(variable, translation) {
                self.insertVariable(translation + '***' + translation + '|||' + variable);
            };

    

            self.insertVariable = function insertVariable(variable) {
                var obj = varToObj(variable);
                self.cols.push(obj);
            };

            self.deleteCol = function (index) {
                self.cols.splice(index, 1);
            };

            function varToObj(variable) {
                var obj = {};

                variable = variable.split('|||');

                variable[0]     = variable[0].split('***');
                obj.name        = variable[0][0];
                obj.translation = variable[0][1];

                variable[1]    = variable[1].split('(');
                obj.identifier = variable[1][0];

                obj.params     = null;

                if (typeof variable[1][1] !== 'undefined') {
                    obj.params = variable[1][1].slice(0, -1);
                }

                return obj;
            }

            function objToVar(obj) {
                var variable = '$' + obj.name + '***' + obj.translation + '|||' + obj.identifier;

                if (obj.params) {
                    variable += '(' + obj.params + ')';
                }

                return variable + '$';
            }

            function contentToCols(content) {
                content = content.split('$');

                for (var i in content) {
                    if (content[i] !== '') {
                        self.cols.push(varToObj(content[i]));
                    }
                }
            }
        }
    ]);

})(window.EVA.app);
