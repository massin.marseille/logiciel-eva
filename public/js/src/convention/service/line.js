(function(app) {

    app.factory('conventionLineService', [
        'restFactoryService',
        function conventionLineService(restFactoryService) {
            return restFactoryService.create('/api/convention-line');
        }
    ])

})(window.EVA.app);
