<?php

namespace PostParc\Command;

use Admin\Command\Runner;
use Bootstrap\Entity\Client;
use Core\Command\AbstractCommand;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncParc extends AbstractCommand
{

    /**
     * Call : php vendor/bin/laminas post-parc:sync-post-parc
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[START] Synchro PostParc...');

        $em                   = $this->getEmDefault();
        $entityManagerFactory = $this->getEmFactory();
        $clients              = $em->getRepository('Bootstrap\Entity\Client')->findAll();
        $exitCode             = Command::SUCCESS;

        /** @var Client $client */
        foreach ($clients as $client) {
            try {
                $output->writeln('[ ' . $client->getName() . ' ]');
                $clientEm = $entityManagerFactory->getEntityManager($client);

                $runner   = new Runner($clientEm, $client);
                $runner->run('sync-post-parc');
            } catch (Exception $e) {
                $exitCode = Command::FAILURE;
                $output->writeln('[ERROR] -' . $e->getMessage());
            }
        }

        $output->writeln('[END] Synchro PostParc...');
        return $exitCode;
    }

}
