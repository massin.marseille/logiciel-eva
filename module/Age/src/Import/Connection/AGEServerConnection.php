<?php

namespace Age\Import\Connection;

class AGEServerConnection extends \PDO
{

    /**
     * The connection to the genuine AGE database
     */
    public function __construct($configuration)
    {
        parent::__construct(
            "sqlsrv:Server={$configuration['masterDbHost']}, {$configuration['masterDbPort']};Database={$configuration['masterDbName']}",
            $configuration['dbUser'],
            $configuration['dbPassword']
        );
    }


    public function getPortfolio($projectCode, $year)
    {
        if (empty($projectCode)) {
            return [];
        }
        $statement = $this->prepare('EXEC dbo.REPORT_ConsultationAE_ADEME ?, ?');
        $statement->bindParam(1, $year, \PDO::PARAM_STR);
        $statement->bindParam(2, $projectCode, \PDO::PARAM_STR);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }
}
