(function(app) {

    app.factory('attachmentService', [
        'restFactoryService',
        function attachmentService(restFactoryService) {
            return restFactoryService.create('/api/attachment');
        }
    ])

})(window.EVA.app);
