<?php

return [
    'icons' => [
        'word' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-file-word-o',
        ],
        'picture' => [
            'type' => 'css',
            'element' => 'i',
            'classes' => 'fa fa-picture-o',
        ]
    ]
];
