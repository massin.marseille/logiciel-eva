<?php

namespace Indicator\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use User\Entity\User;
use Laminas\InputFilter\Factory;

/**
 * Class Indicator
 *
 * @package Indicator\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity(repositoryClass="Indicator\Repository\IndicatorRepository")
 * @ORM\Table(name="indicator_indicator")
 */
class Indicator extends BasicRestEntityAbstract
{
    const TYPE_FREE  = 'free';
    const TYPE_FIXED = 'fixed';

    const OPERATOR_SUM = 'sum';
    const OPERATOR_AVG = 'avg';
    const OPERATOR_MED = 'med';
    const OPERATOR_MAX = 'max';
    const OPERATOR_MIN = 'min';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $uom;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var array
     *
     * @ORM\Column(type="json", name="`values`", nullable=true)
     */
    protected $values;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Indicator\Entity\Measure", mappedBy="indicator")
     */
    protected $measures;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $master;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $operator;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $archived;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Indicator\Entity\Deployment", mappedBy="indicator")
     */
    protected $deployments;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $definition;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $method;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $interpretation;

   /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Indicator\Entity\GroupIndicator", mappedBy="indicators")
     */
    protected $groupIndicators;

    public function __construct()
    {
        $this->measures    = new ArrayCollection();
        $this->groupIndicators = new ArrayCollection();
        $this->deployments = new ArrayCollection();
        $this->values   = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUom()
    {
        return $this->uom;
    }

    /**
     * @param string $uom
     */
    public function setUom($uom)
    {
        $this->uom = $uom;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return ArrayCollection
     */
    public function getMeasures()
    {
        return $this->measures;
    }

    /**
     * @param ArrayCollection $measures
     */
    public function setMeasures($measures)
    {
        $this->measures = $measures;
    }

    /**
     * @return ArrayCollection
     */
    public function getDeployments()
    {
        return $this->deployments;
    }

    /**
     * @param ArrayCollection $deployments
     */
    public function setDeployments($deployments)
    {
        $this->deployments = $deployments;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return int
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * @param int $master
     */
    public function setMaster($master)
    {
        $this->master = $master;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

  /**
     * @return ArrayCollection
     */
    public function getGroupIndicators()
    {
        return $this->groupIndicators;
    }

    /**
     * @param ArrayCollection $groupIndicators
     */
    public function setGroupIndicators($groupIndicators)
    {
        $this->groupIndicators = $groupIndicators;
    }

    /**
     * @param ArrayCollection $groupIndicators
     */
    public function addGroupIndicators(ArrayCollection $groupIndicators)
    {
        foreach ($groupIndicators as $groupIndicator) {
            $this->getGroupIndicators()->add($groupIndicator);
        }
    }

    /**
     * @param ArrayCollection $groupIndicators
     */
    public function removeGroupIndicators(ArrayCollection $groupIndicators)
    {
        foreach ($groupIndicators as $groupIndicator) {
            $this->getGroupIndicators()->removeElement($groupIndicator);
        }
    }

    public function getIsTransverse()
    {
        return $this->deployments->count() > 0;
    }

    public function getFixedValue($value)
    {
        if ($this->getType() == self::TYPE_FIXED && $value !== null) {
            $fixedValues = $this->getValues();

            if ($fixedValues) {
                usort($fixedValues, function ($a, $b) {
                    $aVal = $a['value'];
                    $bVal = $b ['value'];

                    if ($aVal == $bVal) {
                        return 0;
                    }

                    return ($aVal < $bVal) ? -1 : 1;
                });

                for ($i = 0; $i < count($fixedValues); $i++) {
                    $_fixedValue = $fixedValues[$i];
                    if ($value <= $_fixedValue['value']) {
                        return $_fixedValue;
                    }
                }

                return $fixedValues[count($fixedValues) - 1];
            }
        }

        return $value;
    }

    public function getValue($measures)
    {
        $value = null;
        if ($measures) {
            $values = [];
            foreach ($measures as $measure) {
                $values[] = is_array($measure) ? $measure['value'] : $measure->getValue();
            }
            if ($values) {
                switch ($this->getOperator()) {
                    case self::OPERATOR_SUM:
                        $value = array_sum($values);
                        break;
                    case self::OPERATOR_AVG:
                        $value = array_sum($values) / count($values);
                        break;
                    case self::OPERATOR_MED:
                        $count  = count($values);
                        $middle = floor($count / 2);
                        sort($values, SORT_NUMERIC);
                        $median = $values[$middle];
                        if ($count % 2 == 0) {
                            $median = ($median + $values[$middle - 1]) / 2;
                        }

                        $value = $median;

                        break;
                    case self::OPERATOR_MAX:
                        $value = max($values);
                        break;
                    case self::OPERATOR_MIN:
                        $value = min($values);
                        break;
                }

                $value = round($value);
            }
        }

        return $value;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
				'name'     => 'groupIndicators',
				'required' => false
			],
            [
                'name'     => 'uom',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'description',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'type',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getTypes()
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'operator',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'InArray',
                        'options' => [
                            'haystack' => self::getOperators()
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'archived',
                'required' => false,
                'allow_empty' => true,
                'filters' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {
                                if ((is_string($value) && (strtolower($value) == 'oui' || $value === '1' )) ||  $value === 1) {
                                    return true;
                                }
                                if ((is_string($value) && (strtolower($value) == 'non' || $value === '0')) || $value === 0 ) {
                                    return false;
                                }
                                return $value;
                            },
                        ],
                    ],
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) {return  is_bool($value);},
                            'message' => 'structure_field_type_boolean'
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'values',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($values) {
                                foreach ($values as $value) {
                                    if (!isset($value['text']) || $value['text'] == '') {
                                        return false;
                                    }

                                    if (!isset($value['value']) || $value['value'] == '') {
                                        return false;
                                    }
                                }

                                return true;
                            },
                            'message' => 'indicator_field_values_error_malformed'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'master',
                'required' => false
            ],
            [
                'name'     => 'definition',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'method',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'interpretation',
                'required' => false,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_FREE,
            self::TYPE_FIXED
        ];
    }

    /**
     * @return array
     */
    public static function getOperators()
    {
        return [
            self::OPERATOR_SUM,
            self::OPERATOR_AVG,
            self::OPERATOR_MED,
            self::OPERATOR_MAX,
            self::OPERATOR_MIN
        ];
    }

    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        return true;
    }

    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        if ($owner === 'service') {
            $serviceCondition = '
                object.id IN (
                        SELECT a1.primary
                        FROM User\Entity\Association a1
                        INNER JOIN a1.service s1
                        WHERE a1.entity = \'Indicator\\Entity\\Indicator\'
                        AND s1.id IN (
                            SELECT s2.id
                            FROM User\Entity\Association a2
                            INNER JOIN a2.service s2
                            WHERE a2.entity = \'User\\Entity\\User\'
                            AND a2.primary = :ownerService_primary
                        )
                )
            ';

            $queryBuilder->andWhere($serviceCondition);
            $queryBuilder->setParameter('ownerService_primary', $user->getId());
        }
    }



    /**
     * Get the value of archived
     *
     * @return  bool
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Set the value of archived
     *
     * @param  bool  $archived
     *
     * @return  self
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get the value of definition
     *
     * @return  string
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * Set the value of definition
     *
     * @param  string  $definition
     *
     * @return  self
     */
    public function setDefinition($definition)
    {
        $this->definition = $definition;

        return $this;
    }

    /**
     * Get the value of method
     *
     * @return  string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set the value of method
     *
     * @param  string  $method
     *
     * @return  self
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get the value of interpretation
     *
     * @return  string
     */
    public function getInterpretation()
    {
        return $this->interpretation;
    }

    /**
     * Set the value of interpretation
     *
     * @param  string  $interpretation
     *
     * @return  self
     */
    public function setInterpretation($interpretation)
    {
        $this->interpretation = $interpretation;

        return $this;
    }
}
