<?php

return [
    'icons' => [
        'profile'           => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-user-o',
        ],
    ],
];
