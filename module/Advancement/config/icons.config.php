<?php

return [
    'icons' => [
        'advancement' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-tachometer'
        ]
    ]
];
