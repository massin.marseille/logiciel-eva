<?php

namespace Budget;

use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Budget\Service\SimplifiedInputService;
use Budget\View\Helper\BudgetCodeFilters;
use Budget\View\Helper\ExpenseFilters;
use Budget\View\Helper\ExpenseTypes;
use Budget\View\Helper\IncomeTypes;
use Budget\View\Helper\PosteExpenseBalances;
use Budget\View\Helper\PosteExpenseFilters;
use Budget\View\Helper\PosteIncomeBalances;
use Budget\View\Helper\PosteIncomeFilters;
use Core\Module\AbstractModule;
use Core\Utils\RequestUtils;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
	/**
	 * @param MvcEvent $event
	 */
	public function onBootstrap(MvcEvent $event)
	{
		$application = $event->getApplication();
		$request = $application->getRequest();

		if (!RequestUtils::isCommandRequest($request)) {
			$serviceManager = $application->getServiceManager();
			$environment = $serviceManager->get('Environment');
			//$environment->getEntityManager()->getEventManager()->addEventsubscriber(new PosteSubscriber($serviceManager));

			// On fait ça pour prendre en compte les types de montants configurés par client
			if ($environment->getName() === 'client') {
				$expenseTypes = $serviceManager->get('expenseTypes');
				$incomeTypes = $serviceManager->get('incomeTypes');
				$moduleConfig = $this->getConfig();
				foreach ($moduleConfig['module']['budget']['acl']['entities'] as &$entityConfig) {
					if ($entityConfig['name'] === 'expense') {
						$entityConfig['conditions'][0]['options'] = array_map(function ($type) {
							return [
								'value' => $type,
								'text' => 'expense_type_' . $type,
							];
						}, $expenseTypes);
					} elseif ($entityConfig['name'] === 'income') {
						$entityConfig['conditions'][0]['options'] = array_map(function ($type) {
							return [
								'value' => $type,
								'text' => 'income_type_' . $type,
							];
						}, $incomeTypes);
					}
				}
				$this->setConfig($moduleConfig);
			}
		}
	}

	/**
	 * @return array
	 */
	public function getServiceConfig()
	{
		return [
			'factories' => [
				'allowedIncomeTypes' => function (ServiceManager $serviceManager) {
					$user = $serviceManager->get('AuthStorage')->getUser();
					if ($user) {
						$rights = $user->getRole()->getRights();
						if (
							isset($rights) && is_array($rights) && 
							array_key_exists('budget:c:income:type:c', $rights) && is_array($rights['budget:c:income:type:c']) &&
							array_key_exists('value', $rights['budget:c:income:type:c']) && is_array($rights['budget:c:income:type:c']['value'])
						) {
							$all = $serviceManager->get('incomeTypes');
							return array_filter($rights['budget:c:income:type:c']['value'], function ($type) use (
								$all
							) {
								return in_array($type, $all);
							});
						} else {
							return [];
						}
					}

					return $serviceManager->get('incomeTypes');
				},
				'allowedExpenseTypes' => function (ServiceManager $serviceManager) {
					$user = $serviceManager->get('AuthStorage')->getUser();
					if ($user) {
						$rights = $user->getRole()->getRights();
						if (
							isset($rights) && is_array($rights) &&
							array_key_exists('budget:c:expense:type:c', $rights) && is_array($rights['budget:c:expense:type:c']) &&
							array_key_exists('value', $rights['budget:c:expense:type:c']) && is_array($rights['budget:c:expense:type:c']['value'])
						) {
							$all = $serviceManager->get('expenseTypes');
							return array_filter($rights['budget:c:expense:type:c']['value'], function (
								$type
							) use ($all) {
								return in_array($type, $all);
							});
						} else {
							return [];
						}
					}

					return $serviceManager->get('expenseTypes');
				},
				'expenseTypes' => function (ServiceManager $serviceManager) {
					$client = $serviceManager->get('Environment')->getClient();
					$modules = $client->getModules();
					if (isset($modules) && array_key_exists('budget', $modules) && array_key_exists('configuration', $modules['budget']) && array_key_exists('expenseTypes', $modules['budget']['configuration'])) {
						return $modules['budget']['configuration']['expenseTypes'];
					}
					return Expense::getTypes();
				},
				'incomeTypes' => function (ServiceManager $serviceManager) {
					$client = $serviceManager->get('Environment')->getClient();
					$modules = $client->getModules();
					if (isset($modules) && array_key_exists('budget', $modules) && array_key_exists('configuration', $modules['budget']) && array_key_exists('incomeTypes', $modules['budget']['configuration'])) {
						return $modules['budget']['configuration']['incomeTypes'];
					}
					return Income::getTypes();
				},
				'posteExpenseBalances' => function (ServiceManager $serviceManager) {
					$expenseTypes = $serviceManager->get('expenseTypes');
					if(!isset($expenseTypes)) {
						$expenseTypes = [];		// to avoid NULL issues with PHP functions
					}
					$posteExpenseBalances = [];

					if (in_array(Expense::TYPE_PAID , $expenseTypes) || in_array(Expense::TYPE_COMMITTED_PAID, $expenseTypes)) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_TOTAL_PAID;
					}
					if (!array_diff([Expense::TYPE_COMMITTED, Expense::TYPE_COMMITTED_PAID], $expenseTypes)) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_COMMITTED_BALANCE;
					}
					if (
						!array_diff(
							[
								Expense::TYPE_PLANNED,
								Expense::TYPE_COMMITTED,
								Expense::TYPE_PAID,
								Expense::TYPE_RELEASED,
							],
							$expenseTypes
						)
					) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_BALANCE;
					}
					if (!array_diff([Expense::TYPE_AE, Expense::TYPE_AE_COMMITTED], $expenseTypes)) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_AE_BALANCE;
						$posteExpenseBalances[] = PosteExpense::BALANCE_AE_RATE_BALANCE;
					}
					if (!array_diff([Expense::TYPE_CP, Expense::TYPE_CP_COMMITTED], $expenseTypes)) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_CP_BALANCE;
					}
					if (
						!array_diff([Expense::TYPE_AE_ESTIMATED, Expense::TYPE_AE_COMMITTED], $expenseTypes)
					) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_AE_COMMITTED_BALANCE;
					}
					if (
						!array_diff([Expense::TYPE_AE_COMMITTED, Expense::TYPE_CP_COMMITTED], $expenseTypes)
					) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_AE_COMMITTED_CP_COMMITTED_BALANCE;
					}
					if (
						!array_diff([Expense::TYPE_AE_COMMITTED, Expense::TYPE_CP_COMMITTED], $expenseTypes)
					) {
						$posteExpenseBalances[] = PosteExpense::BALANCE_CP_COMMITTED_AE_COMMITTED_RATE_BALANCE;
					}
					if (!array_diff([Expense::TYPE_PLANNED, Expense::TYPE_COMMITTED_PAID], $expenseTypes)) {
                        $posteExpenseBalances[] = PosteExpense::BALANCE_PLANNED_COMMITTED_PAID_BALANCE;
                    }

					return $posteExpenseBalances;
				},
				'posteIncomeBalances' => function (ServiceManager $serviceManager) {
					$incomeTypes = $serviceManager->get('incomeTypes');
					if(!isset($incomeTypes)) {
						$incomeTypes = [];		// to avoid NULL issues with PHP functions
					}
					$posteIncomeBalances = [];

					if (!array_diff([Income::TYPE_REQUESTED, Income::TYPE_PERCEIVED], $incomeTypes)) {
						$posteIncomeBalances[] = PosteIncome::BALANCE_BALANCE;
					}
					if (!array_diff([Income::TYPE_ASSIGNED, Income::TYPE_PERCEIVED], $incomeTypes)) {
						$posteIncomeBalances[] = PosteIncome::BALANCE_TO_BE_PERCEIVED;
					}
					if (!array_diff([Income::TYPE_NOTIFIED, Income::TYPE_PERCEIVED], $incomeTypes)) {
						$posteIncomeBalances[] = PosteIncome::BALANCE_NOTIFIED;
					}

					return $posteIncomeBalances;
				},
				'simplifiedInputService' => function (ServiceManager $serviceManager) {
					$environment = $serviceManager->get('Environment');
					return new SimplifiedInputService(
						$environment->getClient(),
						$environment->getEntityManager(),
						$serviceManager->get('AuthStorage'),
						$serviceManager->get('translator')
					);
				}
			],
		];
	}

	/**
	 * @return array
	 */
	public function getViewHelperConfig()
	{
		return [
			'factories' => [
				'budgetCodeFilters' => function (ServiceManager $serviceManager) {
					$helperPluginManager = $serviceManager->get('ViewHelperManager');
					$translateViewHelper = $helperPluginManager->get('translate');
					$moduleManager = $serviceManager->get('MyModuleManager');
					$entityManager = $helperPluginManager->get('entityManager')->__invoke();

					return new BudgetCodeFilters($translateViewHelper, $moduleManager, $entityManager);
				},
				'allowedExpenseTypes' => function (ServiceManager $serviceManager) {
					return new ExpenseTypes($serviceManager->get('allowedExpenseTypes'));
				},
				'allowedIncomeTypes' => function (ServiceManager $serviceManager) {
					return new IncomeTypes($serviceManager->get('allowedIncomeTypes'));
				},
				'expenseTypes' => function (ServiceManager $serviceManager) {
					return new ExpenseTypes($serviceManager->get('expenseTypes'));
				},
				'incomeTypes' => function (ServiceManager $serviceManager) {
					return new IncomeTypes($serviceManager->get('incomeTypes'));
				},
				'posteExpenseBalances' => function (ServiceManager $serviceManager) {
					return new PosteExpenseBalances($serviceManager->get('posteExpenseBalances'));
				},
				'posteIncomeBalances' => function (ServiceManager $serviceManager) {
					return new PosteIncomeBalances($serviceManager->get('posteIncomeBalances'));
				},
				'posteExpenseFilters' => function (ServiceManager $serviceManager) {
					$helperPluginManager = $serviceManager->get('ViewHelperManager');
					$allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
					$translateViewHelper = $helperPluginManager->get('translate');
					$moduleManager = $serviceManager->get('MyModuleManager');
					$entityManager = $helperPluginManager->get('entityManager')->__invoke();

					return new PosteExpenseFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
				},
				'posteIncomeFilters' => function (ServiceManager $serviceManager) {
					$helperPluginManager = $serviceManager->get('ViewHelperManager');
					$allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
					$translateViewHelper = $helperPluginManager->get('translate');
					$moduleManager = $serviceManager->get('MyModuleManager');
					$entityManager = $helperPluginManager->get('entityManager')->__invoke();

					return new PosteIncomeFilters($translateViewHelper, $moduleManager, $entityManager, $allowedKeywordGroupsViewHelper);
				},
				'expenseFilters' => function (ServiceManager $serviceManager) {
					$helperPluginManager = $serviceManager->get('ViewHelperManager');
					$allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
					$translateViewHelper = $helperPluginManager->get('translate');
					$moduleManager = $serviceManager->get('MyModuleManager');
					$entityManager = $helperPluginManager->get('entityManager')->__invoke();
					$expenseTypes = (new ExpenseTypes($serviceManager->get('expenseTypes')))->__invoke();

					return new ExpenseFilters(
						$translateViewHelper,
						$moduleManager,
						$entityManager,
						$expenseTypes,
						$allowedKeywordGroupsViewHelper
					);
				},
				'incomeFilters' => function (ServiceManager $serviceManager) {
					$helperPluginManager = $serviceManager->get('ViewHelperManager');
					$allowedKeywordGroupsViewHelper = $helperPluginManager->get('allowedKeywordGroups');
					$translateViewHelper = $helperPluginManager->get('translate');
					$moduleManager = $serviceManager->get('MyModuleManager');
					$entityManager = $helperPluginManager->get('entityManager')->__invoke();
					$incomeTypes = (new ExpenseTypes($serviceManager->get('incomeTypes')))->__invoke();

					return new ExpenseFilters(
						$translateViewHelper,
						$moduleManager,
						$entityManager,
						$incomeTypes,
						$allowedKeywordGroupsViewHelper
					);
				},
			],
		];
	}
}
