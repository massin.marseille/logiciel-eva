<?php

namespace Budget\Controller\API;

use Budget\Entity\Account;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Laminas\View\Model\JsonModel;

class AccountController extends BasicRestController
{
    protected $entityClass = 'Budget\Entity\Account';
    protected $repository  = 'Budget\Entity\Account';
    protected $entityChain = 'budget:e:account';

    /**
     * @param QueryBuilder $queryBuilder
     * @param $data
     */
    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort = $data['sort'] ? $data['sort'] : 'object.code';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                    'object.code LIKE :f_full',
                    'object.description LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }

    public function fusionAction()
    {
        $em = $this->getEntityManager();
        $keep_id = $this->params()->fromRoute('id');
        $ids     = $this->params()->fromPost('ids', null);

        if (
            !$ids ||
            count($ids) === 0 ||
            !$this->acl()->isAllowed('budget:e:account:d')
        ) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Account $keep */
        $keep = $em->getRepository(Account::class)->find($keep_id);

        if (!$keep) {
            return new JsonModel([
                'success' => false,
            ]);
        }

        /** @var Account[] $objects */
        $objects = $em->getRepository(Account::class)->findBy(['parent' => $ids]);

        foreach ($objects as $object) {
            if ($object->getId() !== $keep->getId()) {
                $object->setParent($keep);
            } else {
                $object->setParent(null);
            }

            $em->persist($object);
        }

        /** @var PosteExpense[] $objects */
        $objects = $em->getRepository(PosteExpense::class)->findBy(['account' => $ids]);

        foreach ($objects as $object) {
            $object->setAccount($keep);
            $em->persist($object);
        }

        /** @var PosteIncome[] $objects */
        $objects = $em->getRepository(PosteIncome::class)->findBy(['account' => $ids]);

        foreach ($objects as $object) {
            $object->setAccount($keep);
            $em->persist($object);
        }


        $removes = $em->getRepository(Account::class)->findById($ids);

        foreach ($removes as $remove) {
            if ($remove->getId() === $keep->getId()) {
                continue;
            }

            $em->remove($remove);
        }

        $em->flush();

        return new JsonModel([
            'success' => true,
        ]);
    }
}
