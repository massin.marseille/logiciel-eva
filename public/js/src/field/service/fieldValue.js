(function(app) {

    app.factory('fieldValueService', [
        'restFactoryService',
        function fieldValueService(restFactoryService) {
            return restFactoryService.create('/api/fieldValue');
        }
    ])

})(window.EVA.app);
