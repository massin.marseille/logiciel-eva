<?php

use Age\Command\ImportAge;
use Age\Command\ImportAgeFactory;
use Alert\Command\SendAlerts;
use Alert\Command\SendAlertsFactory;

/**
 * Inject configuations on global scope.
 */
return [
    'service_manager' => [
        'factories' => [
            ImportAge::class => ImportAgeFactory::class,
        ],
    ],
    'laminas-cli'     => [
        'commands' => [
            'age:import-age' => ImportAge::class,
        ],
    ],
];
