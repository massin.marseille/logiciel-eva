(function(app) {

    app.directive('clientSelect', [
        'clientService', 'selectFactoryService',
        function clientSelect(clientService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: clientService
                },
                selectize: {
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name']
                }
            });
        }
    ])

})(window.EVA.app);