<?php

namespace Application\Controller\Plugin;

use Application\Entity\Configuration;
use User\Auth\Acl;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class ClientPrivateConfigurationPlugin
 *
 * Access client private configuration from controller.
 *
 * @package  Application\Controller\Plugin
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class ClientPrivateConfigurationPlugin extends AbstractPlugin
{
    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return Acl
     */
    public function __invoke()
    {
        return $this->configuration;
    }
}
