(function(app) {

    app.factory('campainService', [
        'restFactoryService',
        function campainService(restFactoryService) {
            return restFactoryService.create('/api/campain');
        }
    ])

})(window.EVA.app);
