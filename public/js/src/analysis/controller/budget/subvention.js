(app => {
	app.controller('subventionController', [
		'queryService',
		'$rootScope',
		'$scope',
		'translator',
		'$http',
		'flashMessenger',
		app.expenseControllerFactory('budget-analysis-subvention-list', true),
	]);
})(window.EVA.app);
