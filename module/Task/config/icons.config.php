<?php

return [
    'icons' => [
        'task' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-tasks',
        ],
        'kanban' => [
            'type'    => 'css',
            'element' => 'i',
            'classes' => 'fa fa-columns',
        ]
    ]
];
