(function(app) {

    app.factory('settingsTimeService', [
        'restFactoryService',
        function settingsTimeService(restFactoryService) {
            return restFactoryService.create('/api/timesheet/settings-time');
        }
    ])

})(window.EVA.app);
