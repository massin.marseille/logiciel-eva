<?php

namespace Alert\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class AlertController extends BasicRestController
{
    protected $entityClass = 'Alert\Entity\Alert';
    protected $repository  = 'Alert\Entity\Alert';
    protected $entityChain = 'alert:e:alert';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort'] ? $data['sort'] : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full'
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
