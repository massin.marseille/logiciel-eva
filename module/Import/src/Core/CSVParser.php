<?php
namespace Import\Core;

/**
 * La class CSVParser permet de parser le contenu d'un fichier CSV.
 * @package Import\Core
 */
class CSVParser
{
    /**
     * Raw String Content
     * @var string
     */
    protected $rawContent;

    /**
     * Parsed Content
     * @var array
     */
    protected $content;

    /**
     * Delimiter char
     * @var string
     */
    protected $delimiter;

    /**
     * Enclosure char
     * @var string
     */
    protected $enclosure;

    /**
     * Escape char
     * @var string
     */
    protected $escape;


    /**
     * CSVParser constructor.
     * @param $file
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     */
    public function __construct($file, $delimiter = ";", $enclosure = '"', $escape = "\\")
    {
        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
        $this->escape = $escape;

        if (is_string($file)) {
            $this->rawContent = $file;
        }
    }

    /**
     * @param $content
     */
    public function setRawContent($content)
    {
        $this->rawContent = $content;
    }


    /**
     * @return array
     * @throws \Exception
     */
    public function parse()
    {
        $trim_fields      = true;
        $skip_empty_lines = true;
        $delimiter        = $this->delimiter;
        $csv_string       = $this->rawContent;

        return array_map(
            function ($line) use ($delimiter, $trim_fields) {
                return array_map(
                    function ($field) {
                        return str_replace('!!Q!!', '"', urldecode($field));
                    },
                    $trim_fields ? array_map('trim', explode($delimiter, $line)) : explode($delimiter, $line)
                );
            },
            preg_split(
                $skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s',
                preg_replace_callback(
                    '/"(.*?)"/s',
                    function ($field) {
                        return urlencode($field[1]);
                    },
                    $enc = preg_replace('/(?<!")""/', '!!Q!!', $csv_string)
                )
            )
        );
    }
}
