(function (app) {

    app.controller('userController', [
        '$timeout', '$scope', '$http', 'translator',
        function userController($timeout, $scope, $http, translator) {
            var self         = this;
            var shouldReload = false;
            self.totalDone = 0;
            self.totalTarget = 0;
            self.datas = [];

            $scope.chart = {
                users: {
                    api    : {},
                    options: {
                        chart: {
                            type         : 'multiBarChart',
                            showControls : false,
                            showValues   : true,
                            stacked      : true,
                            height       : 400,
                            labelsOutside: true,
                            x            : function (d) {
                                return d.label;
                            },
                            y            : function (d) {
                                return d.value;
                            },
                            yAxis        : {
                                tickFormat: function (d) {
                                    return d3.format(',')(d) + ' h.';
                                }
                            }
                        }
                    },
                    data   : [
                        {
                            'key'   : translator('timesheet_type_done'),
                            'color' : '#4f99b4',
                            'values': []
                        },
                        {
                            'key'   : translator('timesheet_type_left'),
                            'color' : '#d67777',
                            'values': []
                        }
                    ]
                }
            };

            $scope.$on('switchTab', function (event, data) {
                if (data.active === 'user' && shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                }
            });

            $scope.$on('applyFilters', function (event, data) {
                if (data.active === 'user' && data.shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                } else if (data.shouldReload) {
                    shouldReload = true;
                }
            });

            self.applyFilters = function (data) {
                self.loadingOn();
                $http({
                    url   : '/analysis/time/users?' + $.param({filters: data.filters}),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;

                    $scope.chart.users.data = [
                        {
                            'key'   : translator('timesheet_type_done'),
                            'color' : '#4f99b4',
                            'values': []
                        },
                        {
                            'key'   : translator('timesheet_type_left'),
                            'color' : '#d67777',
                            'values': []
                        }
                    ];

                    self.totalDone = 0;
                    self.totalTarget = 0;
                    if (typeof self.datas.res !== 'undefined') {
                        for (var clientId in self.datas.res.users) {
                            var users = self.datas.res.users[clientId];
                            for (var i in users) {
                                self.totalDone += self.datas.res.users[clientId][i].done;
                                self.totalTarget += self.datas.res.users[clientId][i].target;
                                var user = users[i];
                                $scope.chart.users.data[0].values.push({
                                    'label': user.name,
                                    'value': user.done
                                });
                                $scope.chart.users.data[1].values.push({
                                    'label': user.name,
                                    'value': user.target - user.done
                                });
                            }
                        }
                        for (var clientId in self.datas.res.users) {
                            for (var i in self.datas.res.users[clientId]) {
                                self.datas.res.users[clientId][i].percentDone = self.datas.res.users[clientId][i].done / self.totalDone * 100 || 0;
                                self.datas.res.users[clientId][i].percentTarget = self.datas.res.users[clientId][i].target / self.totalTarget * 100 || 0;
        
                            }
                        }
                    }

                    self.switchChartData('users');

                    $timeout(function () {
                        $scope.apiUsers.refresh();
                        self.loadingOff();
                    });
                }, function () {});
            };

            self.switchChartData = function switchChartData(which) {
                if ($scope.chart[which].options.chart.type === 'pieChart') {
                    var values               = $scope.chart[which].data[0].values;
                    $scope.chart[which].data = [];
                    for (var i = 0; i < values.length; ++i) {
                        $scope.chart[which].data.push(values[i]);
                    }
                } else {
                    $scope.chart[which].data = [
                        {
                            'key'   : translator('timesheet_type_done'),
                            'color' : '#4f99b4',
                            'values': []
                        },
                        {
                            'key'   : translator('timesheet_type_left'),
                            'color' : '#d67777',
                            'values': []
                        }
                    ];

                    if (typeof self.datas.res !== 'undefined') {
                        for (var clientId in self.datas.res[which]) {
                            var objs = self.datas.res[which][clientId];
                            for (var i in objs) {
                                var obj = objs[i];
                                $scope.chart[which].data[0].values.push({
                                    'label': obj.name,
                                    'value': obj.done
                                });
                                $scope.chart[which].data[1].values.push({
                                    'label': obj.name,
                                    'value': obj.target - obj.done
                                });
                            }
                        }
                    }
                }
            };

            self.switchChart = function switchChart(api, which) {
                if ($scope.chart[which].options.chart.type != 'pieChart') {
                    $scope.chart[which].options.chart.type = 'pieChart';
                } else {
                    $scope.chart[which].options.chart.type = 'multiBarChart';
                }
                self.switchChartData(which);

                $timeout(function () {
                    api.refresh();
                });
            };

            self.export = function (type, parent, group) {
                $scope.$emit('export', {type: type, parent: parent, group: group});
            };

            self.loadingOn = function () {
                self.loading = true;
                $scope.$emit('loadingOn');
            };

            self.loadingOff = function () {
                self.loading = false;
                $scope.$emit('loadingOff');
            };
        }
    ]);

})(window.EVA.app);
