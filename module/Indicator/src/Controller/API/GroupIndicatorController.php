<?php

namespace Indicator\Controller\API;

use Core\Controller\BasicRestSyncController;
use Doctrine\ORM\QueryBuilder;

class GroupIndicatorController extends BasicRestSyncController
{
    protected $entityClass  = 'Indicator\Entity\GroupIndicator';
    protected $repository   = 'Indicator\Entity\GroupIndicator';
    protected $entityChain  = 'indicator:e:group-indicator';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }
        $queryBuilder->join('object.indicators', 'indicators');
        $queryBuilder->orderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
        
        if(isset($data['filters']['campainId'])){
            $em = $this->entityManager();
            $campainIndicator = $em ->createQuery('SELECT i.primary FROM Campain\Entity\Indicator i WHERE i.campain = :campainId AND i.entity = :entityName')
                ->setParameter('campainId', $data['filters']['campainId'])
                ->setParameter('entityName', 'Indicator\Entity\GroupIndicator')
                ->getResult();

            $queryBuilder->where('object.id in (:ids)')
            ->setParameter('ids', array_values($campainIndicator));
        }
    }
}
