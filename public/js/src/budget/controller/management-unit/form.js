(function(app) {
    app.controller('managementUnitFormController', [
        'managementUnitService',
        'flashMessenger',
        'translator',
        function (managementUnitService, flashMessenger, translator) {
            var self = this;

            var apiParams = {
                col: ['id', 'code', 'name', 'parent.id', 'parent.code', 'parent.name'],
            };

            self.loading = {
                save: false,
            };

            self.errors = {
                fields: {},
            };

            self.managementUnit = {
                _rights: {
                    update: true,
                },
            };

            self.init = function (id) {
                if (id) {
                    managementUnitService.find(id, apiParams).then(function(res) {
                        self.managementUnit = res.object;
                        self.panelTitle = self.managementUnit.name;
                    });
                }
            };

            self.saveOrganization = function () {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.managementUnit.id;

                managementUnitService.save(self.managementUnit, apiParams).then(
                    function(res) {
                        self.managementUnit = res.object;
                        self.panelTitle = self.managementUnit.name;
                        self.loading.save = false;

                        if (isCreation) {
                            var url = window.location.pathname + '/' + self.managementUnit.id;
                            history.replaceState('', '', url);
                        }

                        flashMessenger.success(translator('management_unit_message_saved'));
                    },
                    function(err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }

                        self.loading.save = false;
                    },
                );
            };

            self.deleteOrganization = function () {
                if (confirm(translator('management_unit_question_delete').replace('%1', self.managementUnit.name))) {
                    managementUnitService.remove(self.managementUnit).then(function(res) {
                        if (res.success) {
                            window.location = '/budget/management-unit';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        },
    ]);
})(window.EVA.app);
