(function(app) {

    app.factory('actorService', [
        'restFactoryService',
        function actorService(restFactoryService) {
            return restFactoryService.create('/api/actor');
        }
    ])

})(window.EVA.app);
