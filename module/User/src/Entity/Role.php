<?php

namespace User\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * Class Role
 *
 * @package User\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="user_role")
 */
class Role extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    protected $name;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $rights;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * @param array $rights
     */
    public function setRights($rights)
    {
        $this->rights = $rights;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $queryBuilder = $entityManager->createQueryBuilder();
                                $queryBuilder->select('r')
                                             ->from('User\Entity\Role', 'r')
                                             ->where('r.name = :name')
                                             ->andWhere('r.id != :id')
                                             ->setParameters([
                                                 'id'   => !isset($context['id']) ? '' : $context['id'],
                                                 'name' => $value
                                             ]);

                                try {
                                    $role = $queryBuilder->getQuery()->getOneOrNullResult();
                                } catch (\Exception $e) {
                                    $role = null;
                                }
                                return $role === null;
                            },
                            'message' => 'role_field_name_error_unique'
                        ],
                    ]
                ],
            ],
            [
                'name'     => 'rights',
                'required' => true,
            ]
        ]);

        return $inputFilter;
    }
}
