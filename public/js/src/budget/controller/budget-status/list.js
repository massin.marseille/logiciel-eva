(function(app) {

    app.controller('budgetStatusController', [
        'budgetStatusService', '$scope', 'flashMessenger', 'translator',
        function budgetStatusController(budgetStatusService, $scope, flashMessenger, translator) {
            var self = this;

            self.statutes   = [];
            self.options = {
                name: 'budgetStatutes-list',
                col: [
                    'id',
                    'name',
                    'lock',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    budgetStatusService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    })
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(status) {
                self.editedLineErrors = {};
                self.editedLine       = status;
                self.editedLineFields = angular.copy(status);
            };

            self.saveEditedLine = function saveEditedLine(status) {
                self.editedLineErrors = {};
                budgetStatusService.save(angular.extend({}, status, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        status = angular.extend(status, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('budgetStatus_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.createStatus = function createStatus() {
                $scope.$broadcast('create-status');
            };

            self.editStatus = function editStatus(status) {
                $scope.$broadcast('edit-status', status);
            };

            self.deleteStatus = function deleteStatus(status) {
                if (confirm(translator('budgetStatus_question_delete').replace('%1', status.name))) {
                    budgetStatusService.remove(status).then(function (res) {
                        if (res.success) {
                            self.statutes.splice(self.statutes.indexOf(status), 1);
                            flashMessenger.success(translator('budgetStatus_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
