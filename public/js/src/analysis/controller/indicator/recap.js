(function (app) {

    app.controller('recapController', [
        '$http', '$timeout', '$scope', 'queryService', '$rootScope',
        function recapController($http, $timeout, $scope, queryService, $rootScope) {
            var self = this;

            self.view = 'table';
            self.loading = false;
            self.filters = {
                project: {},
                indicator: {},
                measure: {},
                group: 'indicator'
            };
            self.nbFilters = 1;

            self.group = null;
            self.indicators = {};
            self.years = [];

            self.applyFilters = function applyFilters() {
                self.loading = true;
                self.nbFilters = Object.keys(self.filters.project).length + Object.keys(self.filters.indicator).length + Object.keys(self.filters.measure).length;

                var group = self.filters.group;
                $http({
                    url: '/analysis/indicator/recap?' + $.param({filters: self.filters}),
                    method: 'GET',
                }).then(function (res) {
                    self.group = group;

                    self.indicators = res.data.indicators;
                    self.years = res.data.years;

                    self.loading = false;

                    self.loadBubbleChart();
                    self.loadLineChart();
                    self.loadBarChart();
                    self.loadPieChart();
                });
            };

            self.resetFilters = function () {
                self.filters.project = {};
                self.filters.indicator = {};
                self.filters.measure = {};
                self.applyFilters();
            };

            self.export = function () {
                window.open('/analysis/indicator/recap?' + $.param({filters: self.filters, export: true}));
            };

            self.chart = {
                bubble: {
                    options: {
                        chart: {
                            type: 'scatterChart',
                            height: 600,
                            showDistX: true,
                            showDistY: true,
                            xAxis: {
                                tickFormat: function (d) {
                                    if (d == parseInt(self.years[0] - 1)) {
                                        return 'Initiale'
                                    } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                        return 'Finale';
                                    }
                                    return d;
                                },
                                tickValues: []
                            },
                        }
                    },
                    data: [],
                    api: null
                },
                line: {
                    options: {
                        chart: {
                            type: 'lineChart',
                            height: 600,
                            showDistX: true,
                            showDistY: true,
                            xAxis: {
                                tickFormat: function (d) {
                                    if (d == parseInt(self.years[0] - 1)) {
                                        return 'Initiale'
                                    } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                        return 'Finale';
                                    }
                                    return d;
                                },
                                tickValues: []
                            },
                        }
                    },
                    data: [],
                    api: null
                },
                bar: {
                    options: {
                        chart: {
                            type: 'multiBarChart',
                            height: 600,
                            showDistX: true,
                            showDistY: true,
                            xAxis: {
                                tickFormat: function (d) {
                                    if (d == parseInt(self.years[0] - 1)) {
                                        return 'Initiale'
                                    } else if (d == parseInt(self.years[self.years.length - 1]) + 1) {
                                        return 'Finale';
                                    }
                                    return d;
                                },
                                tickValues: []
                            },
                        }
                    },
                    data: [],
                    api: null
                },
                pie: []
            };

            self.loadBubbleChart = function () {
                self.chart.bubble.data = [];
                self.chart.bubble.options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];
                for (var y in self.years) {
                    self.chart.bubble.options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                }
                self.chart.bubble.options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);

                for (var i in self.indicators) {
                    var data = {
                        key: self.indicators[i].name,
                        values: []
                    };

                    if (self.indicators[i].data.start.ratio) {
                        data.values.push({
                            y: self.indicators[i].data.start.ratio,
                            x: parseInt(self.years[0] - 1),
                            size: self.indicators[i].data.start.number
                        });
                    }

                    for (var y in self.years) {
                        var year = self.years[y];
                        if (self.indicators[i].data.intermediate[year] && self.indicators[i].data.intermediate[year].ratio) {
                            data.values.push({
                                y: self.indicators[i].data.intermediate[year].ratio,
                                x: parseInt(year),
                                size: self.indicators[i].data.intermediate[year].number
                            });
                        }
                    }

                    if (self.indicators[i].data.end.ratio) {
                        data.values.push({
                            y: self.indicators[i].data.end.ratio,
                            x: parseInt(self.years[self.years.length - 1]) + 1,
                            size: self.indicators[i].data.end.number
                        });
                    }

                    self.chart.bubble.data.push(data);
                }
            };

            self.loadLineChart = function () {
                self.chart.line.data = [];
                self.chart.line.options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];
                for (var y in self.years) {
                    self.chart.line.options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                }
                self.chart.line.options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);

                for (var i in self.indicators) {
                    var dataT = {
                        key: self.indicators[i].name + ' - Cible',
                        values: []
                    }, dataD = {
                        key: self.indicators[i].name + ' - Réalisé',
                        values: []
                    };

                    if (self.indicators[i].data.start.target.value) {
                        dataT.values.push({
                            y: self.indicators[i].data.start.target.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }

                    if (self.indicators[i].data.start.done.value) {
                        dataD.values.push({
                            y: self.indicators[i].data.start.done.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }

                    for (var y in self.years) {
                        var year = self.years[y];
                        if (self.indicators[i].data.intermediate[year] && self.indicators[i].data.intermediate[year].target.value) {
                            dataT.values.push({
                                y: self.indicators[i].data.intermediate[year].target.value,
                                x: parseInt(year),
                            });
                        }

                        if (self.indicators[i].data.intermediate[year] && self.indicators[i].data.intermediate[year].done.value) {
                            dataD.values.push({
                                y: self.indicators[i].data.intermediate[year].done.value,
                                x: parseInt(year),
                            });
                        }
                    }

                    if (self.indicators[i].data.end.target.value) {
                        dataT.values.push({
                            y: self.indicators[i].data.end.target.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }

                    if (self.indicators[i].data.end.done.value) {
                        dataD.values.push({
                            y: self.indicators[i].data.end.done.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }

                    self.chart.line.data.push(dataT);
                    self.chart.line.data.push(dataD);
                }
            };

            self.loadBarChart = function () {
                self.chart.bar.data = [];
                self.chart.bar.options.chart.xAxis.tickValues = [parseInt(self.years[0] - 1)];

                for (var y in self.years) {
                    self.chart.bar.options.chart.xAxis.tickValues.push(parseInt(self.years[y]));
                }
                self.chart.bar.options.chart.xAxis.tickValues.push(parseInt(self.years[self.years.length - 1]) + 1);

                for (var i in self.indicators) {
                    var dataT = {
                        key: self.indicators[i].name + ' - Cible',
                        values: []
                    }, dataD = {
                        key: self.indicators[i].name + ' - Réalisé',
                        values: []
                    };

                    if (self.indicators[i].data.start.target.value) {
                        dataT.values.push({
                            y: self.indicators[i].data.start.target.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }

                    if (self.indicators[i].data.start.done.value) {
                        dataD.values.push({
                            y: self.indicators[i].data.start.done.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }

                    for (var y in self.years) {
                        var year = self.years[y];
                        if (self.indicators[i].data.intermediate[year] && self.indicators[i].data.intermediate[year].target.value) {
                            dataT.values.push({
                                y: self.indicators[i].data.intermediate[year].target.value,
                                x: parseInt(year),
                            });
                        }

                        if (self.indicators[i].data.intermediate[year] && self.indicators[i].data.intermediate[year].done.value) {
                            dataD.values.push({
                                y: self.indicators[i].data.intermediate[year].done.value,
                                x: parseInt(year),
                            });
                        }
                    }

                    if (self.indicators[i].data.end.target.value) {
                        dataT.values.push({
                            y: self.indicators[i].data.end.target.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }

                    if (self.indicators[i].data.end.done.value) {
                        dataD.values.push({
                            y: self.indicators[i].data.end.done.value,
                            x: parseInt(self.years[0] - 1)
                        });
                    }
                    if (dataT.values.length > 0){
                        self.chart.bar.data.push(dataT);
                    }
                    if (dataD.values.length > 0){
                        self.chart.bar.data.push(dataD);
                    }
                }
            };

            self.loadPieChart = function () {
                var data = {
                    start: [],
                    end: []
                };
                for (var y in self.years) {
                    data[self.years[y]] = [];
                }

                for (var i in self.indicators) {
                    if (self.indicators[i].data.start.done.value) {
                        data.start.push({
                            label: self.indicators[i].name,
                            value: self.indicators[i].data.start.done.value
                        })
                    }

                    for (var y in self.years) {
                        var year = self.years[y];

                        if (self.indicators[i].data.intermediate[year] && self.indicators[i].data.intermediate[year].done.value) {
                            data[year].push({
                                value: self.indicators[i].data.intermediate[year].done.value,
                                label: self.indicators[i].name,
                            });
                        }
                    }

                    if (self.indicators[i].data.end.done.value) {
                        data.end.push({
                            label: self.indicators[i].name,
                            value: self.indicators[i].data.end.done.value
                        })
                    }
                }


                self.chart.pie = [];

                if (data.end.start > 0) {
                    self.chart.pie.push({
                        api: {},
                        name: 'Initiale',
                        options: {
                            chart: {
                                type: 'pieChart',
                                showControls: false,
                                showValues: true,
                                stacked: true,
                                height: 400,
                                useInteractiveGuideline: true,
                                labelsOutside: true,
                                x: function (d) {
                                    return d.label;
                                },
                                y: function (d) {
                                    return d.value;
                                },
                            }
                        },
                        data: data.start
                    });
                }

                for (var y in self.years) {
                    self.chart.pie.push({
                        api: {},
                        name: self.years[y],
                        options: {
                            chart: {
                                type: 'pieChart',
                                showControls: false,
                                showValues: true,
                                stacked: true,
                                height: 400,
                                useInteractiveGuideline: true,
                                labelsOutside: true,
                                x: function (d) {
                                    return d.label;
                                },
                                y: function (d) {
                                    return d.value;
                                },
                            }
                        },
                        data: data[self.years]
                    });
                }

                if (data.end.length > 0) {
                    self.chart.pie.push({
                        api: {},
                        name: 'Finale',
                        options: {
                            chart: {
                                type: 'pieChart',
                                showControls: false,
                                showValues: true,
                                stacked: true,
                                height: 400,
                                useInteractiveGuideline: true,
                                labelsOutside: true,
                                x: function (d) {
                                    return d.label;
                                },
                                y: function (d) {
                                    return d.value;
                                },
                            }
                        },
                        data: data.end
                    });
                }
            };

            $scope.$watch(function () {
                return self.view
            }, function () {
                $timeout(function () {
                    self.chart.bubble.api.refresh();
                    self.chart.line.api.refresh();
                    self.chart.bar.api.refresh();
                    self.chart.pie.map(pie => pie.api.refresh());
                });
            });

            // QUERIES
            self.queries = [];
            self.query = {
                list: 'analysis-indicator-recap'
            };
            self.errors = {
                fields: {}
            };
            self.loadingQueries = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                self.applyFilters();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters = self.filters;

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };
        }
    ]);
})(window.EVA.app);
