<?php
/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */

$modules = [
	'Laminas\Router',
	'Laminas\Validator',
	'Laminas\Mvc\I18n',
	'Core',
	'DoctrineModule',
	'DoctrineORMModule',
];

// if($env === 'development') {
//   array_push($modules, 'Laminas\DeveloperTools');
// }

// Modules auto-loading
foreach (scandir(getcwd() . '/module') as $module) {
	if (
		$module !== '.' &&
		$module !== '..' &&
		$module !== 'Core' &&
		file_exists(getcwd() . '/module/' . $module . '/src/Module.php')
	) {
		$modules[] = $module;
	}
}

return $modules;
