<?php

return [
    'module' => [
        'project' => [
            'environments' => [
                'client'
            ],

            'active'   => true,
            'required' => false,

            'acl' => [
                'entities' => [
                    [
                        'name'     => 'project',
                        'class'    => 'Project\Entity\Project',
                        'keywords' => true,
                        'owner'    => ['own', 'service']
                    ],
                    [
                        'name'     => 'actor',
                        'class'    => 'Project\Entity\Actor',
                        'keywords' => true,
                        'owner'    => []
                    ],
                    [
                        'name'     => 'template',
                        'class'    => 'Project\Entity\Template',
                        'owner'    => ['service']
                    ],
                    [
                        'name' => 'member',
                        'class' => 'Project\Entity\Member',
                        'keywords' => true,
                        'owner' => []
                    ]
                ]
            ],

            'configuration' => [
                'gantt_view' => [
                    'type'    => 'boolean',
                    'text'    => 'project_gantt_view_configuration'
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
