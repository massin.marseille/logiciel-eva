<?php

namespace Time\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;

class SynchronisationController extends BasicRestController
{
    protected $entityClass  = 'Time\Entity\Synchronisation';
    protected $repository   = 'Time\Entity\Synchronisation';
    protected $entityChain  = 'time:e:synchronisation';

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.name';
        $order = $data['order'] ? $data['order'] : 'ASC';

        $queryBuilder->join('object.user', 'user');

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.name LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
    }
}
