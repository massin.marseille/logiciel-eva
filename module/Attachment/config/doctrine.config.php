<?php

namespace Attachment;

return [
    'doctrine' => [
        'driver' => [
            'attachment_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity'
                ]
            ],
            'orm_environment' => [
                'drivers' => [
                    'Attachment\Entity' => 'attachment_entities'
                ]
            ]
        ]
    ]
];
