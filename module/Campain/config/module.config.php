<?php

return [
    'module' => [
        'campain' => [
            'environments' => [
                'client'
            ],

            'active'    => true,
            'required'  => false,

            'acl' => [
                'entities' => [
                    [
                        'name'     => 'campain',
                        'class'    => 'Campain\Entity\Campain',
                        'keywords' => false,
                        'owner'    => ['service']
                    ]
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
