<?php

namespace Core\Utils;

/**
 * Class ArrayUtils
 *
 * @package Core\Utils
 * @author Jules Bertrand <j.bertrand@siter.fr>
 */
class ArrayUtils
{
    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public static function arrayMergeRecursiveDistinct(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            $isArray      = is_array($value) && isset($merged[$key]) && is_array($merged[$key]);
            $merged[$key] = $isArray ? self::arrayMergeRecursiveDistinct($merged[$key], $value) : $value;
        }

        return $merged;
    }
}
