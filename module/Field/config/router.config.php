<?php

namespace Field;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'field' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/field',
                    'defaults' => [
                        'controller' => 'Field\Controller\Index',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
            ],
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'field' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/field[/:id]',
                            'defaults' => [
                                'controller' => 'Field\Controller\API\Field'
                            ]
                        ]
                    ],
                    'fieldGroup' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/fieldGroup[/:id]',
                            'defaults' => [
                                'controller' => 'Field\Controller\API\Group'
                            ]
                        ]
                    ],
                    'fieldValue' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/fieldValue[/:id]',
                            'defaults' => [
                                'controller' => 'Field\Controller\API\Value'
                            ]
                        ]
                    ],
                ]
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class     => ServiceLocatorFactory::class,
            Controller\API\FieldController::class => ServiceLocatorFactory::class,
            Controller\API\GroupController::class => ServiceLocatorFactory::class,
            Controller\API\ValueController::class => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Field\Controller\Index'     => Controller\IndexController::class,
            'Field\Controller\API\Field' => Controller\API\FieldController::class,
            'Field\Controller\API\Group' => Controller\API\GroupController::class,
            'Field\Controller\API\Value' => Controller\API\ValueController::class,
        ]
    ]
];
