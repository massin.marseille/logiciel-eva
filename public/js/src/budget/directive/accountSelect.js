(function(app) {

    app.directive('accountSelect', [
        'accountService', 'selectFactoryService',
        function accountSelect(accountService, selectFactoryService) {
            return selectFactoryService.create({
                service : {
                    object: accountService,
                    apiParams: {
                        col: ['id', 'code', 'description', 'name', 'type', 'parent.id']
                    },
                    tree: true,
                    full: true
                },
                selectize: {
                    valueField: 'id',
                    searchField: ['code', 'name', 'description'],
                    render: {
                        option: function (data, escape) {
                            return '<div class="option">' + escape(data.code) + ' - ' + escape(data.name) + '</div>';
                        },
                        item: function (data, escape) {
                            var tooltip = '';
                            if (data.description) {
                                tooltip = ' tooltip="' + escape(data.description) + '" ';
                            }
                            return '<div class="option"' + tooltip + '>' + escape(data.code) + ' - ' + escape(data.name) + '</div>';
                        }
                    }
                }
            });
        }
    ])

})(window.EVA.app);
