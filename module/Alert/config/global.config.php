<?php

use Alert\Command\SendAlerts;
use Alert\Command\SendAlertsFactory;

/**
 * Inject configuations on global scope.
 */
return [
    'service_manager' => [
        'factories' => [
            SendAlerts::class => SendAlertsFactory::class,
        ],
    ],
    'laminas-cli'     => [
        'commands' => [
            'alert:send-alerts' => SendAlerts::class,
        ],
    ],
];
