<?php

namespace Campain\Entity;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;


/**
 * Class Measure
 *
 * @package Campain\Entity
 * @author Jules Bertrand <j.bertrand@siter.fr>
 *
 * @ORM\Entity
 * @ORM\Table(name="campain_measure")
 */
class Measure extends BasicRestEntityAbstract
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * @var Campain
	 *
	 * @ORM\ManyToOne(targetEntity="Campain\Entity\Campain")
     * @ORM\JoinColumn(name="campain_id", referencedColumnName="id", nullable=false)
	 */
	protected $campain;
    
    /**
	 * @var Indicator
	 *
	 * @ORM\ManyToOne(targetEntity="Campain\Entity\Indicator")
     * @ORM\JoinColumn(name="indicator_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
	 */
	protected $indicator;

    /**
     * @var Indicator\Entity\Measure
     *
     * @ORM\OneToOne(targetEntity="Indicator\Entity\Measure", cascade={"all"})
     * @ORM\JoinColumn(name="targetMeasure_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $targetMeasure;

    /**
     * @var Indicator\Entity\Measure
     *
     * @ORM\OneToOne(targetEntity="Indicator\Entity\Measure", cascade={"all"})
     * @ORM\JoinColumn(name="realiseMeasure_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $realiseMeasure;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date")
	 */
	protected $supposedDate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    
    /**
     * @return Campain
     */
    public function getCampain()
    {
        return $this->campain;
    }

    /**
     * @param Campain $campain
     */
    public function setCampain($campain)
    {
        $this->campain = $campain;
    }

    /**
     * @return Indicator
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * @param Indicator $indicator
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;
    }

        /**
     * @return Indicator\Entity\Measure
     */
    public function getTargetMeasure()
    {
        return $this->targetMeasure;
    }

    /**
     * @param Indicator\Entity\Measure $targetMeasure
     */
    public function setTargetMeasure($targetMeasure)
    {
        $this->targetMeasure = $targetMeasure;
    }

    /**
     * @return Indicator\Entity\Measure
     */
    public function getRealiseMeasure()
    {
        return $this->realiseMeasure;
    }

    /**
     * @param Indicator\Entity\Measure $realiseMeasure
     */
    public function setRealiseMeasure($realiseMeasure)
    {
        $this->realiseMeasure = $realiseMeasure;
    }

    /**
     * @return \DateTime
     */
    public function getSupposedDate()
    {
        return $this->supposedDate;
    }

    /**
     * @param \DateTime $supposedDate
     */
    public function setSupposedDate($supposedDate)
    {
        $this->supposedDate = $supposedDate;
    }

    public function getRatio(){

        if($this->getTargetMeasure()->getValue()!=0) {
            return $this->getRealiseMeasure()->getValue() / $this->getTargetMeasure()->getValue() * 100;
        }
        return null;
    }

    public function getUser(){
        return $this->getIndicator()->getUsers();
    }

     /**
     * @param EntityManager $entityManager
     * @return \Zend\InputFilter\InputFilterInterface
     */
	public function getInputFilter(EntityManager $entityManager)
	{
		$inputFilterFactory = new Factory();
		$inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'campain',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => array(
                            'callback' => function ($value) use ($entityManager) {
                                if ($value!=null && is_array($value)) {
                                    if(isset($value['id'])){
                                        $value = $value['id'];
                                    }
                                }
                                $campain = $entityManager->getRepository('Campain\Entity\Campain')->find($value);

                                return $campain !== null;
                            },
                            'message' => 'measure_campain_field_campain_error_non_exists'
                        ),
                    ]
                ],
            ],
            [
                'name'     => 'indicator',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => array(
                            'callback' => function ($value) use ($entityManager) {
                                if ($value!=null && is_array($value)) {
                                    if(isset($value['id'])){
                                        $value = $value['id'];
                                    }
                                }
                                $indicator = $entityManager->getRepository('Campain\Entity\Indicator')->find($value);

                                return $indicator !== null;
                            },
                            'message' => 'measure_campain_field_indicator_error_non_exists'
                        ),
                    ]
                ],
            ],
			[
				'name'     => 'supposedDate',
				'required' => true,
				'filters' => [
					['name' => 'Core\Filter\DateTimeFilter'],
				],
			],
			[
                'name'     => 'targetMeasure',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if ($value!=null && is_array($value)) {
                                    if(isset($value['id'])){
                                        $value = $value['id'];
                                    }
                                }
                                $measure = $entityManager->getRepository('Indicator\Entity\Measure')->find($value);

                                return $measure !== null;
                            },
                            'message' => 'measure_campain_field_targetMeasure_error_non_exists'
                        ],
                    ]
				]
            ],
			[
                'name'     => 'realiseMeasure',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                if ($value!=null && is_array($value)) {
                                    if(isset($value['id'])){
                                        $value = $value['id'];
                                    }
                                }
                                $measure = $entityManager->getRepository('Indicator\Entity\Measure')->find($value);

                                return $measure !== null;
                            },
                            'message' => 'measure_campain_field_realiseMeasure_error_non_exists'
                        ],
                    ],[
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value, $context) use ($entityManager) {
                                $id = -1;
                                if (is_array($value) && isset($value['id'])) {
                                    $id = $value['id'];
                                }
                                $measure = new \Indicator\Entity\Measure();
                                $inputFilter = $measure->getInputFilter($entityManager);
                                
                                $inputFilter->setData($value);
                                $inputFilter->isValid();
                                
                                $messages = $inputFilter->getMessages();
                                return !array_key_exists("date", $messages);
                            },
                            'message' => 'measure_field_enddate_before_startdate_error'
                        ],
                    ]
				]
            ]
		]);

		return $inputFilter;
	}
}
