<?php

namespace Note\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class IndexController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('project:e:project:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel([

        ]);
    }
}
