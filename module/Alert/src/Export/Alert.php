<?php
namespace Alert\Export;

use Core\Export\IExporter;

abstract class Alert implements IExporter
{
    public static function getConfig()
    {
        return [
            'recipients.name' => [
                'name' => 'recipients',
            ],
            'enabled'     => [
                'format' => function ($value) {
                    if ($value == 1) {
                        return 'oui';
                    } else {
                        return 'non';
                    }
                },
            ],
            'toManagers'     => [
                'format' => function ($value) {
                    if ($value == 1) {
                        return 'oui';
                    } else {
                        return 'non';
                    }
                },
            ],
            'toValidators'     => [
                'format' => function ($value) {
                    if ($value == 1) {
                        return 'oui';
                    } else {
                        return 'non';
                    }
                },
            ],
            'toMembers'     => [
                'format' => function ($value) {
                    if ($value == 1) {
                        return 'oui';
                    } else {
                        return 'non';
                    }
                },
            ],
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
