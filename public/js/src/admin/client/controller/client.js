(function(app) {

    app.controller('clientController', [
        'clientService', '$filter', '$timeout',
        function clientController(clientService, $filter, $timeout) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'network.name',
                    'master'
                ]
            };

            self.loading = {
                save: false,
                remove: false
            };

            self.errors = {
                fields: {}
            };

            self.modules       = {};
            self.icons         = {};
            self.translations  = {};

            self.clients = [];
            self.client  = {};
            self.users = [];

            clientService.findAll(apiParams).then(function(res) {
                self.clients = res.rows;
            });

            self.resetClient = function resetClient() {
                self.clientName     = 'Nouveau client';
                self.client         =  {
                    name: 'Nouveau client',
                    modules: angular.extend({}, self.modules),
                    icons: angular.extend({}, self.icons),
                    translations: angular.extend({}, self.translations)
                };
                self.errors.fields  = {};
            };

            self.editClient = function editClient(client) {
                self.users = [];
                clientService.find(client.id, {
                    col: [
                        'id',
                        'name',
                        'host',
                        'key',
                        'theme',
                        'email',
                        'dbHost',
                        'dbPort',
                        'dbUser',
                        'dbPassword',
                        'dbName',
                        'modules',
                        'translations',
                        'icons',
                        'network.name',
                        'master'
                    ]
                }).then(function(res) {

                    res.object.modules      = angular.extend({}, self.modules, res.object.modules);
                    res.object.icons        = angular.extend({}, self.icons, res.object.icons);
                    res.object.translations = angular.extend({}, self.translations, res.object.translations);


                    self.clientName     = client.name;
                    self.client         = angular.copy(res.object);
                    self.errors.fields  = {};

                    clientService.getUsers(client).then(function (res) {
                        self.users = res;
                    })
                });
            };

            self.saveClient = function saveClient() {
                self.errors.fields  = {};
                self.loading.save = true;

                var isCreation = (self.client.id ? false : true);

                clientService.save(self.client, apiParams).then(function(res) {
                    if (res.success) {
                        if (isCreation) {
                            self.clients.push(res.object);
                            self.editClient(res.object);
                        } else {
                            var baseClient = $filter('filter')(self.clients, function(item) {
                                return item.id === self.client.id;
                            })[0];
                            self.clients[self.clients.indexOf(baseClient)] = res.object;
                            self.clientName = res.object.name;
                        }
                    }

                    self.loading.save = false;
                }, function(err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                });
            };

            self.removeClient = function removeClient(client) {
                if (confirm('Voulez-vous réellement supprimer ce client ?')) {
                    self.loading.remove = true;
                    clientService.remove(client).then(function (res) {
                        if (res.success) {
                            var baseClient = $filter('filter')(self.clients, function(item) {
                                return item.id === self.client.id;
                            })[0];
                            self.clients.splice(self.clients.indexOf(baseClient), 1);
                            self.resetClient();
                        }

                        self.loading.remove = false;
                    }, function () {
                        self.loading.remove = false;
                    });
                }
            };

            self.loginAs = function loginAS(user) {
                if (self.client.id) {
                    clientService.loginAs(self.client, user).then(function(res) {
                        if (res.success) {
                            window.open('http://' + res.host + '/user/auth/login?secret=' + encodeURIComponent(res.secret));
                        }
                    });
                }
            };

            $timeout(function() {
                self.resetClient();
            });
        }
    ])

})(window.EVA.app);
