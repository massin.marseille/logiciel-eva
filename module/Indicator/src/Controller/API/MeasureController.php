<?php

namespace Indicator\Controller\API;

use Core\Controller\BasicRestController;
use Doctrine\ORM\QueryBuilder;
use Core\Filter\DateTimeFilter;


class MeasureController extends BasicRestController
{
    protected $entityClass  = 'Indicator\Entity\Measure';
    protected $repository   = 'Indicator\Entity\Measure';
    protected $entityChain  = 'indicator:e:measure';
    private $dateFilter;

    public function __construct()
    {
        $this->dateFilter = new DateTimeFilter();
    }

    protected function searchList(QueryBuilder $queryBuilder, &$data)
    {
        $sort  = $data['sort']  ? $data['sort']  : 'object.date';
        $order = $data['order'] ? $data['order'] : 'ASC';

        $queryBuilder->join('object.indicator', 'indicator');
        $queryBuilder->leftJoin('object.project', 'project');
        $queryBuilder->leftJoin('object.campain', 'campain');
        $queryBuilder->leftJoin('object.groupIndicator', 'groupIndicator');
        $queryBuilder->leftJoin('object.campainMeasure', 'campainMeasure');

        if (strpos($sort, '.') === false) {
            $sort = 'object.' . $sort;
        }

        $queryBuilder->addOrderBy($sort, $order);

        if (isset($data['full']) && $data['full']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...[
                    'object.type LIKE :f_full',
                ])
            );

            $queryBuilder->setParameter('f_full', '%' . $data['full'] . '%');
        }
        if (isset($data['filters']) && isset($data['filters']['client'])) {
            $op = $data['filters']['client']['op'];
            if ($op == 'isNull'){
                $queryBuilder->orWhere(
                    'object.client IS NULL'
                );
            } else if($op == 'isNotNull'){
                $queryBuilder->orWhere(
                    'object.client IS NOT NULL'
                );
            } else {
                foreach($data['filters']['client']['val'] as $i => $id){
                    if ($op == 'eq'){
                        $queryBuilder->orWhere(
                           'JSON_EXTRACT(object.client ,\'$.id\') =:value'.$i
                        );
                        $queryBuilder->setParameter('value' . $i,   (int)$id );
                    }
                    else if ($op == 'neq'){
                        $queryBuilder->andWhere(
                          'NOT JSON_EXTRACT(object.client ,\'$.id\') =:value'.$i
                        );    
                       $queryBuilder->setParameter('value' . $i,   (int)$id );
                    }
                    
                }
            }
            unset($data['filters']['client']);
        }
        if(isset($data['filters'])&&isset($data['filters']['groupIndicator.id'])){
            $groupindicatorquery = $this->entityManager()->getrepository('Indicator\Entity\GroupIndicator')->createquerybuilder('gi');
            $indicatorIds = $groupindicatorquery->select('ind.id')
                ->join('gi.indicators', 'ind')
                ->where('gi in (:groupIndicatorId)')
                ->setParameter('groupIndicatorId', $data['filters']['groupIndicator.id'])
                ->getQuery()->getResult();

            $queryBuilder->Where('indicator.id in (:ids)')
            ->setParameter('ids', array_values($indicatorIds));
            
            unset($data['filters']['groupIndicator.id']);
        }
    }

    public function create($data)
    {
        if (!isset($data['client'])){
            $data['client'] = [];
        }
        $currentClient = $this->environment()->getClient();
        $controllerManager   = $this->serviceLocator->get('ControllerManager');
        $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');

        $response = parent::create($data);

        $indicator = $this->getEntityManager()
        ->getRepository('Indicator\Entity\Indicator')
        ->findOneBy(['id' => $data['indicator']['id']]);
        // Save in the master
        if ($indicator->getMaster()){
            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                if($client->isMaster()){
                    $controllerMeasure->setClient($client);                    
                    $clientData = $data;
                    $clientData['project'] = null;
                    $clientData['indicator']['id'] = $indicator->getMaster();
                    $clientData['client'] = array('id' => $currentClient->getId(), 'name' => $currentClient->getName(), 'selected' => true); 
                    parent::create($clientData);
                }
            }
        }

        // Save in the child
        if ($this->environment()->getClient()->isMaster())
        {
            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                if ($client->getId() == $data['client']['id']){
                    $controllerMeasure->setClient($client);
                    $indicator = $this->getEntityManager()
                    ->getRepository('Indicator\Entity\Indicator')
                    ->findOneBy([
                        'master' => $data['indicator']['id']
                    ]);
                    $clientData = $data;
                    $clientData['project'] = null;
                    $clientData['indicator']['id'] = $indicator->getId();
                    $clientData['indicator']['master'] = $indicator->getMaster();
                    $clientData['master'] = $response->getVariable('object')["id"];
                    parent::create($clientData);
                }
            }
        }

        return $response;
    }

    public function delete($id)
    {
        $response = parent::delete($id);

        if ($response->getVariable('success', false) === true) {
            $em = $this->entityManager();

            // On supprime les territoires
            $em ->createQuery('DELETE Map\Entity\Association e WHERE e.entity = :entity AND e.primary = :primary')
                ->setParameter('entity', $this->repository)
                ->setParameter('primary', $id)
                ->execute();
        }

        if ($this->environment()->getClient()->isMaster())
        {
            $controllerManager   = $this->serviceLocator->get('ControllerManager');
            $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');
            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                    $controllerMeasure->setClient($client);
                    $measure = $this->getEntityManager()
                    ->getRepository($this->repository)
                    ->findOneBy([
                        'master' => $id
                    ]);
                    if ($measure) {
                        parent::delete($measure->getId());
                    }
            }
        }

        return $response;
    }

	public function update($id, $data)
    {
        if (!isset($data['client'])){
            $data['client'] = [];
        }
        $response = parent::update($id, $data);
        if(isset($data["campainMeasure"]) && isset($data["campainMeasure"]['id'])){
            $em = $this->getEntityManager();
        $campainMeasure = $em->getRepository('Campain\Entity\Measure')
				->find($data["campainMeasure"]['id']);
                if($campainMeasure !=null){
                    $campainMeasure->setSupposedDate($this->dateFilter->filter($data["campainMeasure"]['supposedDate']));
                    $em->persist($campainMeasure);
                    $em->flush();
                }
        }
        if ($this->environment()->getClient()->isMaster())
        {
            $controllerManager   = $this->serviceLocator->get('ControllerManager');
            $controllerMeasure   = $controllerManager->get('Indicator\Controller\API\Measure');
            foreach($this->environment()->getClient()->getNetwork()->getClients() as $client){
                $controllerMeasure->setClient($client);
                $measure = $this->getEntityManager()
                ->getRepository($this->repository)
                ->findOneBy([
                    'master' => $id
                ]);
                if ($measure) {
                    parent::delete($measure->getId());
                }
                if ($client->getId() == $data['client']['id']){
                    $indicator = $this->getEntityManager()
                    ->getRepository('Indicator\Entity\Indicator')
                    ->findOneBy([
                        'master' => $data['indicator']['id']
                    ]);
                    $clientData = $data;
                    $clientData['project'] = null;
                    $clientData['indicator']['id'] = $indicator->getId();
                    $clientData['indicator']['master'] = $indicator->getMaster();
                    $clientData['master'] = $response->getVariable('object')["id"];
                    parent::create($clientData);
                }
            }
        }

        return $response;
    }

    private function applyDateFilter($data, $measurekey, $datekey ){
        if(isset($data[$measurekey][$datekey])){
            $data[$measurekey][$datekey] = $this->dateFilter->filter($data[$measurekey][$datekey]);
        }
        return $data;
    }
}
