<?php

namespace Application\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\QueryBuilder;
use User\Entity\User;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * @ORM\Entity
 * @ORM\Table(name="application_query")
 */
class Query extends BasicRestEntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $list;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $filters;

    /**
     * @var array
     *
     * @ORM\Column(type="simple_array", nullable=true)
     */
    protected $columns;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $shared = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $sharedService = false;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $owner;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param string $list
     */
    public function setList($list)
    {
        $this->list = $list;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return boolean
     */
    public function getShared()
    {
        return $this->shared;
    }

    /**
     * @param bool $shared
     */
    public function setShared($shared)
    {
        $this->shared = $shared;
    }

    /**
     * @return boolean
     */
    public function getSharedService()
    {
        return $this->sharedService;
    }

    /**
     * @param bool $sharedService
     */
    public function setSharedService($sharedService)
    {
        $this->sharedService = $sharedService;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @param EntityManager $entityManager
     * @return \Laminas\InputFilter\InputFilterInterface
     */
    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name'     => 'name',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'list',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
            ],
            [
                'name'     => 'filters',
                'required' => true,
            ],
            [
                'name'     => 'columns',
                'required' => false,
            ],
            [
                'name'        => 'shared',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ]
            ],
            [
                'name'        => 'sharedService',
                'required'    => false,
                'allow_empty' => true,
                'filters'     => [
                    ['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]],
                ]
            ],
            [
                'name'     => 'owner',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => array(
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $user = $entityManager->getRepository('User\Entity\User')->find($value);

                                return $user !== null;
                            },
                            'message' => 'query_field_owner_error_non_exists'
                        ),
                    ]
                ],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @param User $user
     * @param string $crudAction
     * @param null   $owner
     * @param null   $entityManager
     * @return bool
     */
    public function ownerControl(User $user, $crudAction, $owner = null, $entityManager = null)
    {
        if ($owner === 'service' && $this->getSharedService()) {
            return $entityManager->getRepository('User\Entity\Service')->sameService($user, $this->getOwner());
        } else {
            return  $this->owner && ($user->getId() === $this->owner->getId());
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $user
     * @param string $owner
     */
    public static function ownerControlDQL(QueryBuilder $queryBuilder, User $user, string $owner = '')
    {
        $ownCondition = 'object.owner = :ownerControl OR object.shared = true';

        if ($owner === 'own') {
            $queryBuilder->andWhere($ownCondition);
            $queryBuilder->setParameter('ownerControl', $user);
        }
    }
}
