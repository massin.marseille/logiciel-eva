(function(app) {

    app.factory('homeService', [
        '$http', '$q', 'restFactoryService',
        function homeService($http, $q, restFactoryService) {
            return angular.extend(restFactoryService.create('/api/home'));
        }
    ])

})(window.EVA.app);
