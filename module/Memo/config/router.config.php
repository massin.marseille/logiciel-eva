<?php

namespace Memo;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'memo' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/memo[/:id]',
                            'defaults' => [
                                'controller' => 'Memo\Controller\API\Memo'
                            ]
                        ]
                    ],
                ]
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\API\MemoController::class => ServiceLocatorFactory::class
        ],
        'aliases' => [
            'Memo\Controller\API\Memo' => Controller\API\MemoController::class
        ]
    ]
];
