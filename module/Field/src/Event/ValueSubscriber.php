<?php

namespace Field\Event;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Field\Entity\Value;
use Ramsey\Uuid\Uuid;

class ValueSubscriber implements EventSubscriber
{
    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Value) {
                if (!$entity->getUuid()) {
                    $entity->setUuid(Uuid::uuid4()->toString());
                }

                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }
    }

    public function getSubscribedEvents()
    {
        return [
            Events::onFlush
        ];
    }
}
