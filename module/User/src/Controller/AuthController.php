<?php

namespace User\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use Core\Email\Email;
use Core\Utils\MailUtils;

class AuthController extends AbstractActionSLController
{
    public function loginAction()
    {
        $error   = false;
        $request = $this->getRequest();
        $url     = $this->params()->fromQuery('url', false);
        $partial = (boolean) $this->params()->fromQuery('partial', false);

        $secret = $this->params()->fromQuery('secret', null);
        if ($secret) {
            $secret = urldecode($secret);
            $decoded = openssl_decrypt($secret, 'aes-256-ctr', 'BYPASS_LOGIN_SECRET', 0, 'mlmvSXoRqHF9cOkt');
            $decoded = json_decode($decoded, true);
            if ($decoded) {
                if ($decoded['client'] === $this->environment()->getClient()->getId()) {
                    $user = $this->entityManager()->getRepository('User\Entity\User')->find($decoded['user']);

                    if ($user && !$user->isDisabled()) {
                        $this->authStorage()->store($user);
                        return $this->redirect()->toRoute('root');
                    }
                }
            }
        }

        if ($request->isPost()) {
            $error  = true;

            $login    = $this->params()->fromPost('login', false);
            $password = $this->params()->fromPost('password', false);

            $user = $this->entityManager()->getRepository('User\Entity\User')->authenticate($login, $password);

            if ($user && !$user->isDisabled()) {
                $error = false;

                $this->authStorage()->store($user);

                if (!$partial) {
                    if ($url) {
                        return $this->redirect()->toUrl($url);
                    }
                    return $this->redirect()->toRoute('root');
                }
            }

            if ($partial) {
                return new JsonModel([
                    'error' => $error
                ]);
            }
        }

        $viewModel = new ViewModel([
            'partial' => $partial,
            'url'     => $url,
            'error'   => (isset($user) && $user && $user->isDisabled()) ? 'Votre compte est inactif' : $error
        ]);

        if ($partial) {
            $viewModel->setTerminal(true);
        }

        return $viewModel;
    }

    public function recoverAction()
    {
        $error   = false;
        $success = false;

        $request = $this->getRequest();
        if ($request->isPost()) {

            $error = true;

            $login  = $this->params()->fromPost('login', false);
            $user   = $this->entityManager()->getRepository('User\Entity\User')->findOneBy(['login' => $login]);

            if ($user) {
                $success = true;
                $error   = false;

                $password = $this->randomPassword();

                $user->setPassword($password);
                $this->entityManager()->flush();

                if ($user->getEmail()) {
                    $client = $this->environment()->getClient();

                    $email = new Email($client);
                    $email->setSubject('Votre nouveau mot de passe');
                    $email->addHtmlContent('
                        Bonjour,
                        <br />
                        <br />
                        Vous avez demandé un nouveau mot de passe sur le logiciel : <a href="http://' . $client->getHost() . '">' . $client->getHost() . '</a>
                        <br />
                        Voici vos identifiants :
                        <br />
                        <br />
                        Nom d\'utilisateur : ' . $user->getLogin() . '
                        <br />
                        Mot de passe : ' . $password . '
                        <br />
                        <br />
                        Nous vous invitons à modifier votre mot de passe lors de votre prochaine connexion.
                        <br /><br />
                        <br /><br />
                        <small>Ceci est un e-mail automatique, veuillez ne pas y répondre.</small>
                    ');
                    $email->setFrom(MailUtils::getFrom($client, 'recover'), '');
                    $email->sendTo($user->getEmail());
                }
            }
        }

        return new ViewModel([
            'error'   => $error,
            'success' => $success
        ]);
    }

    public function logoutAction()
    {
        $this->authStorage()->clear();
        return $this->redirect()->toRoute('root');
    }

    public function controlAction()
    {
        return new JsonModel([
            'control' => $this->authStorage()->isAuthenticated()
        ]);
    }

    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass     = [];

        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}
