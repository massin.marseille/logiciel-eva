(function(app) {
	app.controller('timesheetTabController', [
		'$controller',
		'$timeout',
		'$rootScope',
		'timesheetService',
		'uiCalendarConfig',
		'flashMessenger',
		'$scope',
		'translator',
		function timesheetTabController(
			$controller,
			$timeout,
			$rootScope,
			timesheetService,
			uiCalendarConfig,
			flashMessenger,
			$scope,
			translator,
		) {
			var self = this;
			self.project = null;
			self.type = null;

			self.calendarName = 'calendar' + moment().format('x');
			self.view = 'table';
			self.panelTitle = null;
			self.userConnected = {};

			self.users = {};
			for (var i in $scope.$parent.$parent.self.project.members) {
				var member = $scope.$parent.$parent.self.project.members[i];
				if (typeof self.users[member.user.id] === 'undefined') {
					self.users[member.user.id] = member.user;
				}
			}

			self.timesheet = {
				_rights: {
					update: true,
				},
			};

			self.timesheets = [];

			self.errors = {
				fields: {},
			};

			self.apiParams = {
				col: [
					'id',
					'name',
					'user.id',
					'user.name',
					'user.avatar',
					'project.id',
					'project.code',
					'project.name',
					'start',
					'end',
					'hours',
					'description',
					'keywords',
					'territories',
					'type',
				],
			};

			self.options = {
				name: 'timesheets-project-tab',
				col: self.apiParams.col,
				searchType: 'list',
				sort: 'name',
				order: 'asc',
				pager: true,
				getItems: function getData(params, callback, abort) {
					if (typeof params !== 'undefined') {
						var p = angular.copy(params);

						if (self.view === 'calendar') {
							delete p.search.data.page;
							delete p.search.data.limit;

							timesheetService
								.findAll(
									{
										col: ['id'],
										search: {
											data: {
												filters: {
													start: {
														op: 'calendar',
														val: {
															start: calendarBounds.start.format('DD/MM/YYYY'),
															end: calendarBounds.end.format('DD/MM/YYYY'),
															col: 'end',
														},
													},
												},
												sort: 'name',
												order: 'asc',
											},
											type: 'list',
										},
									},
									abort.promise,
								)
								.then(function(res1) {
									var ids = [];
									for (var i in res1.rows) {
										ids.push(res1.rows[i].id);
									}

									p.search.data.filters = angular.extend(params.search.data.filters, {
										project: {
											op: 'eq',
											val: $scope.$parent.$parent.self.project.id,
										},
										type: {
											op: 'eq',
											val: self.type,
										},
										id: {
											op: 'eq',
											val: ids,
										},
									});

									timesheetService.findAll(p, abort.promise).then(function(res) {
										calendarEvents = res.rows;
										uiCalendarConfig.calendars[self.calendarName].fullCalendar('refetchEvents');
									});
								});
						} else {
							p.search.data.filters = angular.extend(params.search.data.filters, {
								project: {
									op: 'eq',
									val: $scope.$parent.$parent.self.project.id,
								},
								type: {
									op: 'eq',
									val: self.type,
								},
							});

							delete p.search.data.filters.id;

							timesheetService.findAll(p, abort.promise).then(function(res) {
								callback(res);
							});
						}
					}
				},
			};

			self.init = function(project, user, type) {
				self.project = project;
				self.type = type;

				if (typeof user === 'object') {
					self.userConnected = user;
				}
			};

			self.openTimesheet = function openTimesheet(timesheet, data) {
				self.loading = {
					save: false,
				};

				if (typeof timesheet !== 'object' || timesheet === null) {
					self.timesheet = {
						_rights: {
							update: true,
						},
						project: self.project,
						user: self.userConnected,
						type: self.type,
					};

					if (data) {
						if (data.startFormat && data.endFormat) {
							self.timesheet.start = data.startFormat;
							self.timesheet.end = data.endFormat;
						} else if (data.timesheet) {
							self.timesheet = angular.copy(data.timesheet);
							self.panelTitle = self.timesheet.name;
						}
					}
				} else {
					self.timesheet = angular.copy(timesheet);
					self.panelTitle = self.timesheet.name;
				}

				$('#timesheet-form-' + self.type).modal('show');
				$scope.$broadcast('create-timesheet');
			};

			self.saveTimesheet = function saveTimesheet() {
				self.errors.fields = {};
				self.loading.save = true;

				var isCreation = !self.timesheet.id;

				timesheetService.save(self.timesheet, self.apiParams).then(
					function(res) {
						self.timesheet = res.object;
						self.panelTitle = self.timesheet.name;
						self.loading.save = false;

						if (self.timesheet.project.id !== $scope.$parent.$parent.self.project.id) {
							self.timesheets = self.timesheets.filter(
								timesheet => timesheet.id !== self.timesheet.id,
							);
							calendarEvents = calendarEvents.filter(
								timesheet => timesheet.id !== self.timesheet.id,
							);
						} else {
							if (isCreation) {
								self.timesheets.push(self.timesheet);
								calendarEvents.push(self.timesheet);
							} else {
								var found = false;

								for (var i in self.timesheets) {
									if (self.timesheets[i].id === self.timesheet.id) {
										self.timesheets[i] = self.timesheet;
										found = true;
										break;
									}
								}

								if (!found) {
									self.timesheets = [...self.timesheets, self.timesheet];
								}

								found = false;

								for (var j in calendarEvents) {
									if (calendarEvents[j].id === self.timesheet.id) {
										found = true;
										calendarEvents[j] = self.timesheet;
										break;
									}
								}

								if (!found) {
									calendarEvents = [...calendarEvents, self.timesheet];
								}
							}
						}

						// $('#timesheet-form-' + self.type).modal('hide');
						flashMessenger.success(translator('timesheet_message_saved'));

						if (self.view === 'calendar') {
							uiCalendarConfig.calendars[self.calendarName].fullCalendar('refetchEvents');
						}
					},
					function(err) {
						for (var field in err.fields) {
							self.errors.fields[field] = err.fields[field];
						}

						self.loading.save = false;
					},
				);
			};

			self.computeHours = function computeHours() {
				if (self.timesheet.start && self.timesheet.end) {
					var start = moment(self.timesheet.start, 'DD/MM/YYYY H:m');
					var end = moment(self.timesheet.end, 'DD/MM/YYYY H:m');

					var hours = end.diff(start, 'hours', true);
					self.timesheet.hours = Math.round(hours * 100) / 100;
				}
			};

			self.controlDates = function controlDates() {
				if (self.timesheet.start && self.timesheet.end) {
					var start = moment(self.timesheet.start, 'DD/MM/YYYY H:m');
					var end = moment(self.timesheet.end, 'DD/MM/YYYY H:m');

					var hours = end.diff(start, 'hours', true);
					if (hours < 0) {
						self.errors.fields['end'] = [translator('timesheet_field_end_before_start_error')];
					} else {
						self.errors.fields['end'] = [];
					}
				}
			};

			$scope.$watch(
				function() {
					if (typeof self.timesheet !== 'undefined') {
						return self.timesheet.end;
					}
				},
				function() {
					if (typeof self.timesheet !== 'undefined') {
						self.controlDates();
					}
				},
			);

			$scope.$watch(
				function() {
					if (typeof self.timesheet !== 'undefined') {
						return self.timesheet.start;
					}
				},
				function() {
					if (typeof self.timesheet !== 'undefined') {
						self.controlDates();
					}
				},
			);

			/**
			 * Calendar Timesheet
			 */
			self.alterTimesheet = function(event) {
				var timesheet = event.timesheet;
				timesheet.start = moment(event.start).format('DD/MM/YYYY HH:mm');
				timesheet.end = moment(event.end).format('DD/MM/YYYY HH:mm');

				timesheetService.save(timesheet).then(
					function() {
						$rootScope.$broadcast('refresh-all-calendars', self.calendarName);
					},
					function(err) {
						for (var field in err.fields) {
							for (var message in err.fields[field]) {
								flashMessenger.error(field + ' : ' + err.fields[field][message]);
							}
						}
					},
				);
			};

			var calendarBounds = {
				start: null,
				end: null,
			};
			self.calendar = {
				editable: true,
				header: {
					left: 'month agendaWeek agendaDay',
					center: 'title',
					right: 'today prev,next',
				},
				lang: 'fr',
				buttonText: {
					today: "Aujourd'hui",
					month: 'Mois',
					week: 'Semaine',
					day: 'Jour',
				},
				selectable: true,
				selectHelper: true,
				select: function(start, end) {
					if (
						uiCalendarConfig.calendars[self.calendarName].fullCalendar('getView').type === 'month'
					) {
						var startFormat = start.add(8, 'hours').format('DD/MM/YYYY HH:mm');
						var endFormat = end.subtract(6, 'hours').format('DD/MM/YYYY HH:mm');
					} else {
						var startFormat = start.format('DD/MM/YYYY HH:mm');
						var endFormat = end.format('DD/MM/YYYY HH:mm');
					}

					var data = { startFormat: startFormat, endFormat: endFormat };

					self.openTimesheet(null, data);
				},
				eventResize: function(event) {
					self.alterTimesheet(event);
				},
				eventDrop: function(event) {
					self.alterTimesheet(event);
				},
				eventClick: function(date) {
					self.openTimesheet(null, date);
				},
				viewRender: function(view) {
					calendarBounds.start = view.start;
					calendarBounds.end = view.end;

					$timeout(function() {
						$(window).trigger('resize');
						uiCalendarConfig.calendars[self.calendarName].fullCalendar('removeEvents');
						$scope.__tb.loadData();
					});
				},
			};

			var calendarEvents = [];
			self.eventSources = [
				{
					events: function(start, end, timezone, callback) {
						var events = [];
						for (var i in calendarEvents) {
							var timesheet = calendarEvents[i];
							events.push({
								id: timesheet.id,
								title: timesheet.project == null ? timesheet.name : timesheet.project.name,
								start: moment(timesheet.start, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm:ss'),
								end: moment(timesheet.end, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm:ss'),
								timesheet: timesheet,
								editable: timesheet._rights.update,
								color: timesheet.user.color,
							});
						}

						callback(events);
					},
				},
			];

			/**
			 * Edited line
			 */
			self.editedLine = null;
			self.editedLineFields = null;
			self.editedLineErrors = {};
			self.editLine = function editLine(timesheet) {
				if (angular.isArray(timesheet.keywords) || timesheet.keywords == null) {
					timesheet.keywords = {};
				}

				self.editedLineErrors = {};
				self.editedLine = timesheet;
				self.editedLineFields = angular.copy(timesheet);
			};

			self.saveEditedLine = function saveEditedLine(timesheet) {
				self.editedLineErrors = {};
				timesheetService
					.save(angular.extend({}, timesheet, self.editedLineFields), {
						col: self.options.col,
					})
					.then(
						function(res) {
							if (res.success) {
								timesheet = angular.extend(timesheet, res.object);
								self.editedLine = null;
								self.editedLineFields = null;

								flashMessenger.success(translator('timesheet_message_saved'));
							}
						},
						function(err) {
							for (var field in err.fields) {
								self.editedLineErrors[field] = err.fields[field];
							}
						},
					);
			};

			$scope.$watch(
				function() {
					return self.view;
				},
				function(newVal, oldVal) {
					if (newVal !== oldVal) {
						self.options.pager = self.view == 'table';
						$scope.__tb.loadData();
					}
				},
			);

			self.deleteTimesheet = function deleteTimesheet(timesheet) {
				if (confirm(translator('timesheet_question_delete').replace('%1', timesheet.name))) {
					timesheetService.remove(timesheet).then(function(res) {
						if (res.success) {
							for (var i in calendarEvents) {
								if (calendarEvents[i].id === timesheet.id) {
									calendarEvents.splice(i, 1);
									break;
								}
							}

							for (var j in self.timesheets) {
								if (self.timesheets[i].id === timesheet.id) {
									self.timesheets.splice(i, 1);
									break;
								}
							}

							if (self.view === 'calendar') {
								$('#timesheet-form-' + self.type).modal('hide');
								uiCalendarConfig.calendars[self.calendarName].fullCalendar('refetchEvents');
							}
							flashMessenger.success(translator('timesheet_message_deleted'));
						} else {
							flashMessenger.error(translator('error_occured'));
						}
					});
				}
			};

			self.isMember = function isMember(user) {
				for (var i in $scope.$parent.$parent.self.project.members) {
					var member = $scope.$parent.$parent.self.project.members[i];
					if (member.id && member.user.id == user) {
						return true;
					}
				}

				return false;
			};
		},
	]);
})(window.EVA.app);
