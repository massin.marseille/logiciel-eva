<?php

namespace Keyword\Entity;

use Budget\Entity\Expense;
use Budget\Entity\Income;
use Budget\Entity\PosteExpense;
use Budget\Entity\PosteIncome;
use Budget\Helper\IBudgetComputable;
use Core\Entity\BasicRestEntityAbstract;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\Filter\Boolean;
use Laminas\InputFilter\Factory;

/**
 * Class Keyword
 * @package Keyword\Entity
 *
 * @ORM\Entity(repositoryClass="Keyword\Repository\KeywordRepository")
 * @ORM\Table(name="keyword_keyword")
 */
class Keyword extends BasicRestEntityAbstract implements IBudgetComputable
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="Keyword\Entity\Group", inversedBy="keywords")
     */
    protected $group;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Keyword\Entity\Keyword", mappedBy="children")
     */
    protected $parents;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Keyword\Entity\Keyword", inversedBy="parents")
     * @ORM\JoinTable(name="keyword_keyword_children",
     *      joinColumns={@ORM\JoinColumn(name="keyword_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $children;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="`order`")
     */
    protected $order;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $color;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $master;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $externalId;

    /**
     * @var ArrayCollection
     */
    protected $projects;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $archived = false;

    /**
     * Associations
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Keyword\Entity\Association",  mappedBy="keyword")
     */
    protected $associations;

    public function __construct()
    {
        $this->parents = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->associations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return ArrayCollection
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * @param ArrayCollection $parents
     */
    public function setParents($parents)
    {
        $this->parents = $parents;
    }

    /**
     * @param ArrayCollection $parents
     */
    public function addParents($parents)
    {
        foreach ($parents as $parent) {
            $parent->getChildren()->add($this);
            $this->getParents()->add($parent);
        }
    }

    /**
     * @param  $parent
     */
    public function addParent($parent)
    {
        $parent->getChildren()->add($this);
        $this->getParents()->add($parent);
    }

    /**
     * @param ArrayCollection $parents
     */
    public function removeParents($parents)
    {
        foreach ($parents as $parent) {
            $parent->getChildren()->removeElement($this);
            $this->getParents()->removeElement($parent);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function addChildren($children)
    {
        foreach ($children as $child) {
            $child->getParents()->add($this);
            $this->getChildren()->add($child);
        }
    }

    /**
     * @param ArrayCollection $children
     */
    public function removeChildren($children)
    {
        foreach ($children as $child) {
            $child->getParents()->removeElement($this);
            $this->getChildren()->removeElement($child);
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * @param int $master
     */
    public function setMaster($master)
    {
        $this->master = $master;
    }

    /**
     * @return bool
     */
    public function isArchived()
    {
        return $this->archived;
    }

    /**
     * @return bool
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param bool $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

    /**
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $id
     */
    public function setExternalId($id)
    {
        $this->externalId = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getAssociations()
    {
        return $this->associations;
    }

    /**
     * @param ArrayCollection $associations
     */
    public function setAssociations($associations)
    {
        $this->associations = $associations;
    }
    /**
     * @param ArrayCollection $associations
     */
    public function addAssociations($associations)
    {
        foreach ($associations as $association) {
            $association->setKeyword($this);
            $this->getAssociations()->add($association);
        }
    }

    /**
     * @param ArrayCollection $associations
     */
    public function removeAssociations($associations)
    {
        foreach ($associations as $association) {
            $association->setKeyword(null);
            $this->getAssociations()->removeElement($association);
        }
    }

    /**
     * @param bool $inclusive
     * @return array
     */
    public function getFlatTree($inclusive = false)
    {
        $tree = [];
        if ($inclusive) {
            $tree[] = $this->getId();
        }

        foreach ($this->getChildren() as $child) {
            $tree[] = $child->getId();
            $tree = array_merge($tree, $child->getFlatTree(false));
        }

        return $tree;
    }

    public function getMaxLevel()
    {
        $level = 0;

        if ($this->getParents() == null || $this->getParents()->count() == 0) {
            return $level;
        }

        foreach ($this->getParents() as $parent) {
            $parentMaxLevel = $parent->getMaxLevel() + 1;
            $level = $parentMaxLevel > $level ? $parentMaxLevel : $level;
        }

        return $level;
    }

    public function getLevels()
    {
        $levels = [];

        if ($this->getParents() == null || $this->getParents()->count() == 0) {
            $levels[] = 0;
            return $levels;
        }

        foreach ($this->getParents() as $parent) {
            $parentsLevels = $parent->getLevels();
            foreach ($parentsLevels as $parentLevel) {
                $levels[] = $parentLevel + 1;
            }
        }

        return $levels;
    }

    public function getHierarchySup()
    {
        $hierarchy = new ArrayCollection();

        if ($this->parents !== null) {
            foreach ($this->parents as $parent) {
                $hierarchy[] = $parent;
                $hierarchy = array_merge($parent->getHierarchySup()->toArray(), $hierarchy->toArray());
                $hierarchy = new ArrayCollection($hierarchy);
            }
        }

        return $hierarchy;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        $inputFilterFactory = new Factory();
        $inputFilter = $inputFilterFactory->createInputFilter([
            [
                'name' => 'name',
                'required' => true,
                'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
            ],
            [
                'name' => 'description',
                'required' => false,
                'filters' => [['name' => 'StringTrim'], ['name' => 'StripTags']],
            ],
            [
                'name' => 'group',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }
                                $group = $entityManager->getRepository('Keyword\Entity\Group')->find($value);

                                return $group !== null;
                            },
                            'message' => 'keyword_field_group_error_non_exists',
                        ],
                    ],
                ],
            ],
            [
                'name' => 'parents',
                'required' => false,
            ],
            [
                'name' => 'associations',
                'required' => false,
            ],
            [
                'name' => 'order',
                'required' => true,
            ],
            [
                'name' => 'color',
                'required' => false,
            ],
            [
                'name' => 'master',
                'required' => false,
            ],
            [
                'name' => 'archived',
                'required' => false,
                'allow_empty' => true,
                'filters' => [['name' => 'Boolean', 'options' => ['type' => Boolean::TYPE_ALL]]],
            ],
        ]);

        return $inputFilter;
    }

    /**
     * @param EntityManager $em
     *
     * @return float
     */
    public function getTimeSpent($year, $em)
    {
        $timeSpent = 0;
        foreach ($this->getProjects($em) as $project) {
            $timeSpent += $project->getTimeSpent($year);
        }

        return $timeSpent;
    }

    /**
     * @param EntityManager $em
     *
     * @return float
     */
    public function getTimeSpentArbo($year, $em)
    {
        $timeSpent = 0;
        foreach ($this->children as $child) {
            $timeSpent += $child->getTimeSpentArbo($year, $em);
        }

        return $timeSpent + $this->getTimeSpent($year, $em);
    }

    public function getTimeTarget($year, $em)
    {
        $timeTarget = 0;
        foreach ($this->getProjects($em) as $project) {
            $timeTarget += $project->getTimeTarget($year);
        }

        return $timeTarget;
    }

    /**
     * @param EntityManager $em
     *
     * @return float
     */
    public function getTimeTargetArbo($year, $em)
    {
        $timeTarget = 0;
        foreach ($this->children as $child) {
            $timeTarget += $child->getTimeTargetArbo($year, $em);
        }

        return $timeTarget + $this->getTimeTarget($year, $em);
    }

    public function getTimeLeft($year, $em)
    {
        return $this->getTimeTarget($year, $em) - $this->getTimeSpent($year, $em);
    }

    public function getTimeLeftArbo($year, $em)
    {
        return $this->getTimeTargetArbo($year, $em) - $this->getTimeSpentArbo($year, $em);
    }


    public function getAmountPosteExpense($em = null, $balance = '')
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountPosteExpense'}($em, $balance);
        }
        return $amount;
    }

    public function getAmountPosteExpenseArbo($em = null, $balance = '')
    {
        $amount = 0;
        foreach($this->children as $child) {
            $amount += $child->getAmountPosteExpenseArbo($em, $balance);
        }
        return $amount + $this->getAmountPosteExpense($em, $balance);
    }

    public function getAmountHTPosteExpense($em = null, $balance = '')
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountHTPosteExpense'}($em, $balance);
        }
        return $amount;
    }

    public function getAmountHTPosteExpenseArbo($em = null, $balance = '')
    {
        $amount = 0;
        foreach($this->children as $child) {
            $amount += $child->getAmountHTPosteExpenseArbo($em, $balance);
        }
        return $amount + $this->getAmountHTPosteExpense($em, $balance);
    }

    /**
     * Returns the sum of expenses for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    public function getAmountExpense($type, $year, $em, $evolution = null)
    {
        $amount = 0;

        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountExpense' . ucfirst($type)}($year, $evolution);
        }

        return $amount;
    }

    /**
     * Returns the sum of expenses for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    public function getAmountExpenseArbo($type, $year, $em, $evolution = null)
    {
        $amount = 0;

        foreach ($this->children as $child) {
            $amount += $child->getAmountExpenseArbo($type, $year, $em, $evolution);
        }

        return $amount + $this->getAmountExpense($type, $year, $em, $evolution);
    }


    public function getAmountPosteIncome($em = null, $balance = '')
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountPosteIncome'}($em, $balance);
        }
        return $amount;
    }

    public function getAmountPosteIncomeArbo($em = null, $balance = '')
    {
        $amount = 0;
        foreach($this->children as $child) {
            $amount += $child->getAmountPosteIncomeArbo($em, $balance);
        }
        return $amount + $this->getAmountPosteIncome($em, $balance);
    }

    public function getAmountHTPosteIncome($em = null, $balance = '')
    {
        $amount = 0;
        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountHTPosteIncome'}($em, $balance);
        }
        return $amount;
    }

    public function getAmountHTPosteIncomeArbo($em = null, $balance = '')
    {
        $amount = 0;
        foreach($this->children as $child) {
            $amount += $child->getAmountHTPosteIncomeArbo($em, $balance);
        }
        return $amount + $this->getAmountHTPosteIncome($em, $balance);
    }

    /**
     * Returns the sum of incomes for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    protected function getAmountIncome($type, $year, $em, $evolution = null)
    {
        $amount = 0;

        foreach ($this->getProjects($em) as $project) {
            $amount += $project->{'getAmountIncome' . ucfirst($type)}($year, $evolution);
        }

        return $amount;
    }

    /**
     * Returns the sum of incomes for a given type
     *
     * @param $type
     * @param $em
     * @return float
     */
    protected function getAmountIncomeArbo($type, $year, $em, $evolution = null)
    {
        $amount = 0;

        foreach ($this->children as $child) {
            $amount += $child->getAmountIncomeArbo($type, $year, $em, $evolution);
        }

        return $amount + $this->getAmountIncome($type, $year, $em, $evolution);
    }

    public function __call($name, $arguments)
    {
        foreach (Expense::getTypes() as $type) {
            if ($name === 'getAmountExpense' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountExpense'], $arguments);
            } elseif ($name === 'getAmountExpenseArbo' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountExpenseArbo'], $arguments);
            }
        }

        foreach (Income::getTypes() as $type) {
            if ($name === 'getAmountIncome' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountIncome'], $arguments);
            } elseif ($name === 'getAmountIncomeArbo' . ucfirst($type)) {
                array_unshift($arguments, $type);
                return call_user_func_array([$this, 'getAmountIncomeArbo'], $arguments);
            }
        }

        foreach (PosteExpense::getBalances() as $balance) {
			if (
				$name === 'getAmountPosteExpense' . ucfirst($balance) ||
				$name === 'getAmountHTPosteExpense' . ucfirst($balance) ||
				$name === 'getAmountPosteExpenseArbo' . ucfirst($balance) ||
				$name === 'getAmountHTPosteExpenseArbo' . ucfirst($balance)
			) {
				$method = str_replace(ucfirst($balance), '', $name);
				array_unshift($arguments, $balance);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

		foreach (PosteIncome::getBalances() as $balance) {
			if (
				$name === 'getAmountPosteIncome' . ucfirst($balance) ||
				$name === 'getAmountHTPosteIncome' . ucfirst($balance) ||
				$name === 'getAmountPosteIncomeArbo' . ucfirst($balance) ||
				$name === 'getAmountHTPosteIncomeArbo' . ucfirst($balance)
			) {
				$method = str_replace(ucfirst($balance), '', $name);
				array_unshift($arguments, $balance);

				return call_user_func_array([$this, $method], $arguments);
			}
		}

        return null;
    }

    protected function getProjects($em)
    {
        if ($this->projects === null) {
            $this->projects = new ArrayCollection();

            $associations = $em->getRepository('Keyword\Entity\Association')->findBy([
                'keyword' => $this,
                'entity' => 'Project\Entity\Project',
            ]);

            foreach ($associations as $association) {
                $project = $em->getRepository('Project\Entity\Project')->find($association->getPrimary());
                $this->projects->add($project);
            }
        }

        return $this->projects;
    }

    public function getUserTimeRepartition($year, $em)
    {
        $lines = [];
        foreach ($this->getProjects($em) as $project) {
            $projectLines = $project->getUserTimeRepartition($year);
            foreach ($projectLines as $key => $line) {
                if (!isset($lines[$key])) {
                    $lines[$key] = $line;
                } else {
                    $lines[$key]['done'] += $line['done'];
                    $lines[$key]['target'] += $line['target'];
                    $lines[$key]['left'] += $line['left'];
                }
            }
        }

        return $lines;
    }

    public function getUserTimeRepartitionArbo($year, $em)
    {
        $lines = $this->getUserTimeRepartition($year, $em);
        foreach ($this->children as $keyword) {
            $keywordLines = $keyword->getUserTimeRepartitionArbo($year, $em);
            foreach ($keywordLines as $key => $line) {
                if (!isset($lines[$key])) {
                    $lines[$key] = $line;
                } else {
                    $lines[$key]['done'] += $line['done'];
                    $lines[$key]['target'] += $line['target'];
                    $lines[$key]['left'] += $line['left'];
                }
            }
        }

        return $lines;
    }

    public function getTerritories($em)
    {
        $territories = new ArrayCollection();
        foreach ($this->getProjects($em) as $project) {
            $projectTerritories = $project->getTerritories($em);
            foreach ($projectTerritories as $territory) {
                if (!$territories->contains($territory)) {
                    $territories->add($territory);
                }
            }
        }
        return $territories;
    }

    public function getTerritoriesArbo($em)
    {
        $territories = $this->getTerritories($em);

        foreach ($this->children as $child) {
            $childTerritories = $child->getTerritoriesArbo($em);
            foreach ($childTerritories as $territory) {
                if (!$territories->contains($territory)) {
                    $territories->add($territory);
                }
            }
        }
        return $territories;
    }

    public function getPosteIncomes($em)
    {
        $postes = new ArrayCollection();
        foreach ($this->getProjects($em) as $project) {
            foreach ($project->getPosteIncomes() as $poste) {
                if (!$postes->contains($poste)) {
                    $postes->add($poste);
                }
            }
        }
        return $postes;
    }

    public function getPosteExpenses($em)
    {
        $postes = new ArrayCollection();
        foreach ($this->getProjects($em) as $project) {
            foreach ($project->getPosteExpenses() as $poste) {
                if (!$postes->contains($poste)) {
                    $postes->add($poste);
                }
            }
        }
        return $postes;
    }

    public function getPosteIncomesArbo($em)
    {
        $postes = new ArrayCollection();
        foreach ($this->getProjects($em) as $project) {
            foreach ($project->getPosteIncomesArbo() as $poste) {
                if (!$postes->contains($poste)) {
                    $postes->add($poste);
                }
            }
        }
        return $postes;
    }

    public function getPosteExpensesArbo($em)
    {
        $postes = new ArrayCollection();
        foreach ($this->getProjects($em) as $project) {
            foreach ($project->getPosteExpensesArbo() as $poste) {
                if (!$postes->contains($poste)) {
                    $postes->add($poste);
                }
            }
        }
        return $postes;
    }
}
