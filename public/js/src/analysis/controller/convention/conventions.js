(function (app) {

    app.controller('conventionsController', [
        'conventionService', '$timeout', '$scope', '$filter', 'queryService', '$rootScope',
        function conventionsController(conventionService, $timeout, $scope, $filter, queryService, $rootScope) {
            var self = this;

            self.conventions = [];

            self.loading = false;

            self.options = {
                name      : 'budget-analysis-conventions-list',
                col       : [
                    'id',
                    'name',
                    'lines.project.name',
                    'lines.project.stackedAdvancement',
                    'start',
                    'end',
                    'amount',
                    'amountSubv',
                    'progress',
                    'contractor.id',
                    'contractor.name',
                    'territories'
                ],
                searchType: 'list',
                sort      : 'name',
                order     : 'asc',
                pager     : false,
                autoload  : false,
                getItems  : function getData(params, callback, abort) {
                    self.loading = true;

                    params.master = $scope.isMaster;

                    conventionService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                        self.loading = false;
                    });
                }
            };

            self.export = function () {
                window.open('/analysis/convention/conventions?' + $.param({filters: $scope.__tb.apiParams.search.data.filters, export: true}));
            };


            self.init = function (expenseTypes) {
                self.initQueries();

                self.expenseTypes = expenseTypes;
                for (var i in expenseTypes) {
                    self.options.col.push('amountExpense' + expenseTypes[i].ucfirst());
                    self.options.col.push('amountHTExpense' + expenseTypes[i].ucfirst());
                    self.options.col.push('lines.amountExpense' + expenseTypes[i].ucfirst());
                    self.options.col.push('lines.amountHTExpense' + expenseTypes[i].ucfirst());
                }
            };

            self.getTotal = function (type, ht) {
                return self.getPartialTotal(type, self.conventions, ht);
            };

            self.getPartialTotal = function (type, conventions, ht) {
                ht = ht ? 'HT' : '';
                var total = 0;
                var uniq  = [];
                for (var i in conventions) {
                    if (type == 'territories') {
                        for (var j in conventions[i].territories) {
                            var territory = conventions[i].territories[j];
                            var key       = $scope.isMaster ? conventions[i]._client.id + '_' + territory.id : territory.id;
                            if (uniq.indexOf(key) < 0) {
                                total++;
                                uniq.push(key);
                            }
                        }
                    } else if (type == 'contractors') {
                        var key = $scope.isMaster ? conventions[i]._client.id + '_' + conventions[i].contractor.id : conventions[i].contractor.id;
                        if (uniq.indexOf(key) < 0) {
                            total++;
                            uniq.push(key);
                        }
                    } else if (type == 'amount') {
                        total += conventions[i].amount;
                    } else if (type == 'amountSubv') {
                        total += conventions[i].amountSubv;
                    } else {
                        total += conventions[i]['amount' + ht + 'Expense' + type.ucfirst()];
                    }
                }
                return total;
            };

            // QUERIES
            self.queries          = [];
            self.query            = {
                list: 'analysis-convention-conventions'
            };
            self.errors           = {
                fields: {}
            };
            self.loadingQueries   = false;
            self.loadingSaveQuery = false;

            var queryApiCol = ['id', 'name', 'filters', 'columns', 'list', 'shared'];

            self.initQueries = function () {
                self.loadingQueries = true;

                queryService
                    .findAll({
                        col: queryApiCol,
                        search: {
                            data: {
                                filters: {
                                    list: {
                                        op: 'eq',
                                        val: self.query.list
                                    }
                                }
                            }
                        }
                    })
                    .then(function (res) {
                        self.queries        = res.rows;
                        self.loadingQueries = false;
                    }, function () {
                        self.loadingQueries = false;
                    });
            };

            self.loadQuery = function (query) {
                self.filters = angular.copy(query.filters);

                $scope.__tb.apiParams.search.data.filters = self.filters.convention;

                $scope.__tb.loadData();

                $rootScope.$broadcast(self.query.list + '-query-loaded', self.filters);
            };

            self.saveQuery = function () {
                self.loadingSaveQuery = true;
                self.query.filters    = {
                    convention: $scope.__tb.apiParams.search.data.filters
                };

                queryService
                    .save(self.query, {col: queryApiCol})
                    .then(function (res) {
                        self.queries.push(res.object);

                        self.query.name = '';
                        self.loadingSaveQuery = false;
                    }, function (err) {
                        for (var field in err.fields) {
                            self.errors.fields[field] = err.fields[field];
                        }
                        self.loadingSaveQuery = false;
                    });
            };

            self.deleteQuery = function (query) {
                if (confirm('Voulez-vous réellement supprimer la requête ' + query.name + ' ?')) {
                    self.loadingSaveQuery = true;
                    queryService.remove(query).then(function () {
                        self.queries.splice(self.queries.indexOf(query), 1);
                        self.loadingSaveQuery = false;
                    }, function () {
                        self.loadingSaveQuery = false;
                    });
                }
            };

            if ($scope.isMaster) {
                self.charts = {
                    conventions: {
                        api    : {},
                        options: {
                            chart: {
                                type         : 'pieChart',
                                showControls : false,
                                height       : 300,
                                labelsOutside: true,
                                x            : function (d) {
                                    return d.label;
                                },
                                y            : function (d) {
                                    return d.value;
                                },
                                tooltip      : {
                                    valueFormatter: function (d) {
                                        return Math.round(d);
                                    }
                                }
                            }
                        },
                        data   : []
                    },
                    contractors: {
                        api    : {},
                        options: {
                            chart: {
                                type         : 'pieChart',
                                showControls : false,
                                height       : 300,
                                labelsOutside: true,
                                x            : function (d) {
                                    return d.label;
                                },
                                y            : function (d) {
                                    return d.value;
                                },
                                tooltip      : {
                                    valueFormatter: function (d) {
                                        return Math.round(d);
                                    }
                                }
                            }
                        },
                        data   : []
                    },
                    territories: {
                        api    : {},
                        options: {
                            chart: {
                                type         : 'pieChart',
                                showControls : false,
                                height       : 300,
                                labelsOutside: true,
                                x            : function (d) {
                                    return d.label;
                                },
                                y            : function (d) {
                                    return d.value;
                                },
                                tooltip      : {
                                    valueFormatter: function (d) {
                                        return Math.round(d);
                                    }
                                }
                            }
                        },
                        data   : []
                    }
                };

                $scope.$watch(function () { return self.conventions}, function () {
                    self.charts.conventions.data = [];
                    self.charts.contractors.data = [];
                    self.charts.territories.data = [];

                    var clients = $filter('groupBy')(self.conventions, '_client.name');
                    for (var client in clients) {
                        self.charts.conventions.data.push({
                            label: client,
                            value: clients[client].length
                        });

                        self.charts.contractors.data.push({
                            label: client,
                            value: self.getPartialTotal('contractors', clients[client]) || 0
                        });

                        self.charts.territories.data.push({
                            label: client,
                            value: self.getPartialTotal('territories', clients[client]) || 0
                        });
                    }
                });

                $scope.$watch('tabConventionLoaded', function (newVal) {
                    $timeout(function () {
                        self.charts.conventions.api.refresh();
                        self.charts.contractors.api.refresh();
                        self.charts.territories.api.refresh();
                    });
                });
            }
        }
    ]);
})(window.EVA.app);
