(function(app) {

    app.factory('groupService', [
        'restFactoryService',
        function groupService(restFactoryService) {
            return restFactoryService.create('/api/group');
        }
    ])

})(window.EVA.app);
