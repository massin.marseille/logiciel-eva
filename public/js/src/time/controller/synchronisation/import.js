(function (app) {

    app.controller('synchronisationImportController', [
        'FileUploader', 'timesheetService', 'flashMessenger', 'translator', '$scope', '$http', '$filter', '$location', '$anchorScroll',
        function synchronisationImportController(FileUploader, timesheetService, flashMessenger, translator, $scope, $http, $filter, $location, $anchorScroll) {
            var self = this;

            self.timesheets = [];
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'start',
                    'end',
                    'hours',
                    'syncHash',
                    'synchronisation.id',
                    'project.id',
                    'project.code',
                    'project.name',
                    'user.id',
                    'user.name',
                    'user.avatar',
                    'type',
                    'createdAt',
                    'updatedAt',
                ]
            };

            self.importprocess = {
                running : false
            };
            self.loading = {
                save: false,
                parse: false
            };

            self.errors = {
                fields: {}
            };

            self.synchronisation = {
                _rights: {
                    update: true
                },
                config: {}
            };

            self.rows = [];
            self.user = null;
            self.time = {};
            self.projectActive = true;
            self.hoursPerDay   = 24;
            self.currentTimesheet = null;

            self.uploader = new FileUploader();
            self.reader = new FileReader();
            self.file = null;

            self.uploader.filters.push({
                name: 'icsFilter',
                fn: function (item) {
                    var type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase() + '|';
                    return '|ics|'.indexOf(type) !== -1;
                }
            });

            self.uploader.onAfterAddingFile = function (fileItem) {
                self.reader.readAsDataURL(fileItem._file);
            };

            self.uploader.onWhenAddingFileFailed = function (fileItem) {
                self.file = null;
            };

            self.reader.onloadend = function (e) {
                $scope.$apply(function () {
                    self.file = e.target.result;
                });
            };

            self.importICS = function () {
                self.loading.save = true;

                var params = {
                    'ics': true,
                };

                if (typeof self.time.start !== 'undefined' && self.time.start != '') {
                    params.start = self.time.start;
                }
                if (typeof self.time.end !== 'undefined' && self.time.end != '') {
                    params.end = self.time.end;
                }
                if (typeof self.synchronisation.config.field !== 'undefined' && self.synchronisation.config.field != '') {
                    params.field = self.synchronisation.config.field;
                }
                if (typeof self.synchronisation.config.closing !== 'undefined' && self.synchronisation.config.closing != '') {
                    params.closing = self.synchronisation.config.closing;
                }
                if (typeof self.synchronisation.config.join !== 'undefined' && self.synchronisation.config.join != '') {
                    params.join = self.synchronisation.config.join;
                }
                if (typeof self.synchronisation.config.custom !== 'undefined' && self.synchronisation.config.custom != '') {
                    params.custom = self.synchronisation.config.custom;
                }
                if (self.user) {
                    params.user = self.user.id;
                }
                $http({
                    url: '/timesheet/synchronisation/do-import?' + $.param(params),
                    method: 'POST',
                    data: self.file,
                }).then(function (res) {
                    if (res.data.error) {
                        flashMessenger.error(res.data.error);
                    } else {
                        self.timesheets = res.data.timesheets;
                        self.compileKeywords();
                        flashMessenger.success(translator('timesheet_synchonisation_import_success'));
                    }
                    self.loading.save = false;
                }, function () {
                    flashMessenger.error(translator('timesheet_synchonisation_import_error'));
                    self.loading.save = false;
                });
            };

            self.init = function init(rows, user, time, projectActive) {
                if (rows) {

                    self.loading.parse = true;

                    self.rows = rows;

                    self.user = user;

                    var params = {
                        'rows': self.rows
                    };

                    if (time) {
                        self.time = time;
                        if (typeof time.start !== 'undefined' && time.start != '') {
                            params.start = self.time.start;
                        }
                        if (typeof time.end !== 'undefined' && time.end != '') {
                            params.end = self.time.end;
                        }
                    }
                    params.user = self.user.id;

                    $http({
                        url: '/timesheet/synchronisation/do-import?' + $.param(params),
                        method: 'GET',
                    }).then(function (res) {
                        self.timesheets = res.data.timesheets;
                        self.compileKeywords();
                        self.loading.parse = false;
                    }, function () {
                        self.loading.parse = false;
                        alert('Une erreur s\'est produite lors de la synchronisation.');
                    });

                    if (typeof projectActive === 'boolean') {
                        self.projectActive = projectActive;
                    }
                }

            };

            self.import = function () {
                var selected = [],
                    notAllHAsProjects = false,
                    doContinue = true;

                for (var i = 0; i < self.timesheets.length; ++i) {
                    if (!self.projectExists(self.timesheets[i]) ||
                        (
                            self.timesheets[i].type === 'absence' &&
                            !self.timesheets[i].absenceType
                        )
                    ) {
                        notAllHAsProjects = true;
                        break;
                    }
                }

                if (notAllHAsProjects) {
                    doContinue = confirm(translator('timesheet_import_caution_not_all_has_project'));
                }

                if (doContinue) {
                    for (var j = 0; j < self.timesheets.length; ++j) {
                        if (
                            !self.projectExists(self.timesheets[j]) ||
                            (
                                self.timesheets[j].type === 'absence' &&
                                !self.timesheets[j].absenceType
                            )
                        ) {
                            continue;
                        }

                        self.timesheets[j].user = self.user;
                        selected.push(self.timesheets[j]);
                    }
                    if(self.importprocess.running){ return ;}
                    if (selected.length > 0) {
                        self.importprocess.running = true;
                        timesheetService.saveAll(selected, apiParams).then(function (res) {
                            self.importprocess.running = false;
                            var max = self.timesheets.length;
                            for (var i = 0; i < res.length; ++i) {
                                var success = res[i].success,
                                    object = res[i].object;

                                if (!success) {
                                    object = selected[i];
                                }

                                object.errors = res[i].errors;

                                for (var j = 0; j < max; ++j) {
                                    var timesheet = self.timesheets[j];
                                    if (object.syncHash == timesheet.syncHash) {
                                        if (success) {
                                            self.timesheets.splice(j, 1);
                                            --max;
                                            --j;
                                        } else {
                                            timesheet.errors = object.errors;
                                        }
                                        break;
                                    }
                                }
                            }

                            if (selected.length !== res.length) {
                                flashMessenger.error(translator('timesheet_import_errors'));
                            }
                            if (res[0] && !res[0].success) {
                                if (res[0].errors?.exceptions[0]) {
                                    flashMessenger.error(translator('timesheet_import_errors'));
                                    console.error('Server exception', res[0].errors.exceptions);
                                    return;
                                }
                                if (res[0].errors?.fields && Object.keys(res[0].errors.fields).length > 0) {
                                    flashMessenger.error(translator('timesheet_import_errors'));
                                    console.error('Fields exception', res[0].errors.fields);
                                    return;
                                }
                            }

                            flashMessenger.success(translator('timesheet_import_success'));
                        }, function (err) {
                            self.import.running = false;
                        });
                    } else {
                        flashMessenger.error(translator('timesheet_import_error_least_one'));
                    }
                }
            };

            /**
             * Check if a project exists in a timesheet.
             *
             * @param timesheet
             * @returns {boolean}
             */
            self.projectExists = function projectExists(timesheet) {
                return !self.projectActive ||
                    (typeof timesheet !== 'undefined' &&
                        typeof timesheet.project !== 'undefined' &&
                        timesheet.project != null &&
                        typeof timesheet.project.id !== 'undefined' &&
                        timesheet.project.id > 0);
            };

            /**
             * Delete une ligne du tableau
             * @param row
             */
            self.deleteTimesheet = function deleteTimesheet(row) {
                if (confirm(translator('timesheet_import_confirm_delete'))) {
                    var index = self.timesheets.indexOf(row);
                    if (index > -1) {
                        self.timesheets.splice(index, 1);
                    }
                    self.check();
                }
            };

            /**
             * Delete the selected timesheets
             */
            self.deleteTimesheets = function deleteTimesheets() {
                if (confirm(translator('timesheet_import_confirm_delete_selection'))) {
                    var max = self.timesheets.length;
                    for (var i = 0; i < max; ++i) {
                        if (self.timesheets[i].checked) {
                            self.timesheets.splice(i, 1);
                            --max;
                            --i;
                        }
                    }
                    self.check();
                }
            };

            /**
             * Check all checkboxes
             * @param $event
             */
            self.checkAll = function checkAll($event) {
                var isChecked = $event;
                if (typeof $event !== 'boolean') {
                    isChecked = $event.currentTarget.checked;
                } else {
                    self.checkboxAll = isChecked;
                }

                for (var i = 0; i < self.timesheets.length; ++i) {
                    self.timesheets[i].checked = false;
                }

                var filtered = $filter('filter')(self.timesheets, { 'name': self.search });

                for (var i = 0; i < filtered.length; ++i) {
                    filtered[i].checked = isChecked;
                }
            };

            /**
             * Check a checkbox
             */
            self.check = function check() {
                var count = self.timesheets.length;
                for (var i = self.timesheets.length - 1; i >= 0; --i) {
                    if (self.timesheets[i].checked) {
                        --count;
                    }
                }

                self.checkboxAll = count == 0 && self.timesheets.length > 0;
            };

            /**
             * Keywords
             */
            self.keywordGroups = {
                _rights: {
                    'update': true
                }
            };

            /**
             * Ajoute les keywords aux rows séléctionnées.
             */
            self.addKeywords = function addKeywords() {
                var keywords = self.getKeywords();
                if (self.currentTimesheet){
                    var timesheet = self.currentTimesheet;
                    for (var g in keywords) {
                        for (var k = 0; k < keywords[g].length; ++k) {
                            var exists = -1;
                            if (typeof timesheet.keywords[g] !== 'undefined') {
                                exists = self.getKeywordIndexOf(timesheet.keywords[g], keywords[g][k]);
                            } else {
                                timesheet.keywords[g] = [];
                            }

                            if (exists == -1) {
                                timesheet.keywords[g].push(keywords[g][k]);
                            }
                        }
                    }
                    self.currentTimesheet = null;
                } else {
                    for (var i = 0; i < self.timesheets.length; ++i) {
                        var timesheet = self.timesheets[i];
                        if (timesheet.checked) {
                            if (typeof timesheet.keywords === 'undefined') {
                                timesheet.keywords = {};
                            }

                            for (var g in keywords) {
                                for (var k = 0; k < keywords[g].length; ++k) {
                                    var exists = -1;
                                    if (typeof timesheet.keywords[g] !== 'undefined') {
                                        exists = self.getKeywordIndexOf(timesheet.keywords[g], keywords[g][k]);
                                    } else {
                                        timesheet.keywords[g] = [];
                                    }

                                    if (exists == -1) {
                                        timesheet.keywords[g].push(keywords[g][k]);
                                    }
                                }
                            }
                        }
                    }
                }

                self.cleanKeywords();
            };

            /**
             * Remplace les keywords dans les timesheets sélectionnées.
             */
            self.replaceKeywords = function replaceKeywords() {
                var keywords = self.getKeywords();
                if (self.currentTimesheet){
                    var timesheet = self.currentTimesheet;
                    timesheet.keywords = keywords;
                    self.currentTimesheet = null;
                } else {
                    for (var i = 0; i < self.timesheets.length; ++i) {
                        var timesheet = self.timesheets[i];
                        if (timesheet.checked) {
                            timesheet.keywords = keywords;
                        }
                    }
                }

                self.cleanKeywords();
            };

            /**
             * Retire les keywords des timesheets sélectionnées.
             */
            self.removeKeywords = function removeKeywords() {
                var keywords = self.getKeywords();
                if (self.currentTimesheet){
                    var timesheet = self.currentTimesheet;
                    for (var g in keywords) {
                        for (var k = 0; k < keywords[g].length; ++k) {
                            var exists = -1;
                            if (typeof timesheet.keywords[g] !== 'undefined') {
                                exists = self.getKeywordIndexOf(timesheet.keywords[g], keywords[g][k]);
                            }

                            if (exists > -1) {
                                timesheet.keywords[g].splice(exists, 1);
                            }
                        }
                    }
                    self.currentTimesheet = null;
                } else {
                    for (var i = 0; i < self.timesheets.length; ++i) {
                        var timesheet = self.timesheets[i];
                        if (timesheet.checked) {
                            if (typeof timesheet.keywords === 'undefined') {
                                timesheet.keywords = {};
                            }

                            for (var g in keywords) {
                                for (var k = 0; k < keywords[g].length; ++k) {
                                    var exists = -1;
                                    if (typeof timesheet.keywords[g] !== 'undefined') {
                                        exists = self.getKeywordIndexOf(timesheet.keywords[g], keywords[g][k]);
                                    }

                                    if (exists > -1) {
                                        timesheet.keywords[g].splice(exists, 1);
                                    }
                                }
                            }
                        }
                    }
                }

                self.cleanKeywords();
            };

            /**
             * Récupère les keywords par groupes
             * @returns {{}}
             */
            self.getKeywords = function getKeywords() {
                var keywords = {};
                if (typeof self.keywordGroups.keywords !== 'undefined') {
                    for (var i in self.keywordGroups.keywords) {
                        var group = self.keywordGroups.keywords[i];

                        if (typeof keywords[i] === 'undefined') {
                            keywords[i] = [];
                        }

                        if (angular.isArray(group)) {
                            keywords[i] = group;
                        } else {
                            keywords[i] = [group];
                        }
                    }
                }
                return keywords;
            };

            /**
             * Récupère l'index d'un keyword spécifique dans
             * la liste des keywords passés.
             *
             * @param timesheet La liste des keywords
             * @param keyword Le keyword à chercher
             * @returns {number}
             */
            self.getKeywordIndexOf = function (timesheet, keyword) {
                for (var j = 0; j < timesheet.length; ++j) {
                    if (keyword.id == timesheet[j].id) {
                        return j;
                    }
                }
                return -1;
            };

            /**
             * Vide les keywords actuels.
             */
            self.cleanKeywords = function cleanKeywords() {
                self.compileKeywords();
                delete self.keywordGroups['keywords'];
            };

            /**
             * Prends tous les keywords et les rassemble dans
             * un tableau pour la vue uniquement.
             */
            self.compileKeywords = function compileKeywords() {
                for (var i = 0; i < self.timesheets.length; ++i) {
                    var timesheet = self.timesheets[i];
                    timesheet.keywordsView = [];

                    if (typeof timesheet.keywords !== 'undefined') {
                        for (var g in timesheet.keywords) {
                            for (var k = 0; k < timesheet.keywords[g].length; ++k) {
                                timesheet.keywordsView.push(timesheet.keywords[g][k]);
                            }
                        }
                    }
                }
            };

            /**
             * Territories
             */
            self.territories = [];

            /**
             * Ajoute les territoires aux rows séléctionnées.
             */
            self.addTerritories = function addTerritories() {
                if (self.currentTimesheet){
                    var timesheet = self.currentTimesheet;
                    if (typeof timesheet.territories === 'undefined') {
                        timesheet.territories = [];
                    }

                    for (var g in self.territories) {
                        var territory = self.territories[g];
                        var index     = self.getTerritoryIndexOf(timesheet.territories, territory);
                        if (index === -1) {
                            timesheet.territories.push(territory);
                        }
                    }
                    self.currentTimesheet = null;
            } else {
                    for (var i = 0; i < self.timesheets.length; ++i) {
                        var timesheet = self.timesheets[i];
                        if (timesheet.checked) {
                            if (typeof timesheet.territories === 'undefined') {
                                timesheet.territories = [];
                            }

                            for (var g in self.territories) {
                                var territory = self.territories[g];
                                var index     = self.getTerritoryIndexOf(timesheet.territories, territory);
                                if (index === -1) {
                                    timesheet.territories.push(territory);
                                }
                            }
                        }
                    }
                }

                self.cleanTerritories();
            };

            /**
             * Remplace les territoires dans les rows sélectionnées.
             */
            self.replaceTerritories = function replaceTerritories() {
                if (self.currentTimesheet){
                    var timesheet = self.currentTimesheet;
                    timesheet.territories = self.territories;
                    self.currentTimesheet = null;
                } else {
                    for (var i = 0; i < self.timesheets.length; ++i) {
                        var timesheet = self.timesheets[i];
                        if (timesheet.checked) {
                            if (typeof timesheet.territories === 'undefined') {
                                timesheet.territories = [];
                            }

                            timesheet.territories = self.territories;
                        }
                    }
                }

                self.cleanTerritories();
            };

            /**
             * Retire les territoires des rows sélectionnées.
             */
            self.removeTerritories = function removeTerritories() {
                if (self.currentTimesheet){
                    var timesheet = self.currentTimesheet;
                    for (var g in self.territories) {
                        var territory = self.territories[g];
                        var index     = self.getTerritoryIndexOf(timesheet.territories, territory);
                        if (index !== -1) {
                            timesheet.territories.splice(index, 1);
                        }
                    }
                    self.currentTimesheet = null;
                } else {
                    for (var i = 0; i < self.timesheets.length; ++i) {
                        var timesheet = self.timesheets[i];
                        if (timesheet.checked) {
                            if (typeof timesheet.territories === 'undefined') {
                                timesheet.territories = [];
                            }

                            for (var g in self.territories) {
                                var territory = self.territories[g];
                                var index     = self.getTerritoryIndexOf(timesheet.territories, territory);
                                if (index !== -1) {
                                    timesheet.territories.splice(index, 1);
                                }
                            }
                        }
                    }
                }

                self.cleanTerritories();
            };


            /**
             * Vide les territories actuels.
             */
            self.cleanTerritories = function cleanTerritories() {
                self.territories = [];
            };

            self.getTerritoryIndexOf = function (row, territory) {
                for (var j = 0; j < row.length; ++j) {
                    if (territory.id == row[j].id) {
                        return j;
                    }
                }
                return -1;
            };
        }
    ]);

})(window.EVA.app);
