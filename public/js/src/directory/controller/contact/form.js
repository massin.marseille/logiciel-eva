(function (app) {

    app.controller('contactFormController', [
        'contactService', 'jobService', 'flashMessenger', '$scope', 'translator', 'FileUploader',
        function contactFormController(contactService, jobService, flashMessenger, $scope, translator, FileUploader) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'lastName',
                    'firstName',
                    'name',
                    'gender',
                    'vip',
                    'subscriptionStatus',
                    'subscriptionStatusUpdatedAt',
                    'avatar',
                    'email',
                    'phone',
                    'portable',
                    'zipcode',
                    'city',
                    'country',
                    'addressLine1',
                    'addressLine2',
                    'keywords',
                    'links.id',
                    'links.structure.id',
                    'links.structure.name',
                    'links.job.id',
                    'links.job.name',
                    'links.keywords',
                    'keywords'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {},
                job: {
                    fields: {}
                }
            };

            self.contact = {
                subscriptionStatus: 'empty',
                avatar: '/img/default-avatar.png',
                _rights: {
                    update: true
                }
            };

            self.uploader = new FileUploader();
            self.uploader.filters.push({
                name: 'imageFilter',
                fn: function(item, options) {
                    var type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase() + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            });
            self.reader   = new FileReader();
            self.reader.onloadend = function(e) {
                $scope.$apply(function() {
                    self.contact.avatar = e.target.result;
                })
            };

            self.uploader.onAfterAddingFile = function(fileItem) {
                self.reader.readAsDataURL(fileItem._file);
                self.errors.fields['avatar'] = null;
            };
            self.uploader.onWhenAddingFileFailed = function(item , filter, options) {
                self.errors.fields['avatar'] = ['Veuillez sélectionner un fichier image (jpg, png, bmp, gif)'];
            };

            self.init = function init(id) {
                if (id) {
                    contactService.find(id, apiParams).then(function (res) {
                        self.contact = res.object;
                        self.panelTitle = self.contact.firstName + ' ' + self.contact.lastName;
                        self.contact.gender = self.contact.gender ? self.contact.gender.replace('.', '').toLowerCase() : '';
                        if (self.contact.gender === 'non binaire') {
                            self.contact.gender = 'neutral';
                        }
                    })
                }
            };

            self.saveContact = function saveContact() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.contact.id;

                contactService.save(self.contact, apiParams).then(function (res) {
                    self.contact = res.object;
                    $scope.$broadcast("custom-fields-save", { entity: 'contact', id: self.contact.id });
                    self.panelTitle = self.contact.firstName + ' ' + self.contact.lastName;
                    self.loading.save = false;
                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.contact.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('contact_message_saved'));

                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteContact = function deleteContact() {
                if (confirm(translator('contact_question_delete').replace('%1', self.contact.firstName + ' ' + self.contact.lastName))) {
                    contactService.remove(self.contact).then(function(res) {
                        if (res.success) {
                            window.location = '/directory/contact';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };
        }
    ])

})(window.EVA.app);
