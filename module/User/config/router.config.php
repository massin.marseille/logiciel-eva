<?php

namespace User;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'routes' => [
            'user' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/user',
                    'defaults' => [
                        'controller' => 'User\Controller\User',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'User\Controller\User',
                                'action'     => 'form'
                            ]
                        ]
                    ],
                    'auth' => [
                        'type' => 'Literal',
                        'options' => [
                            'route'    => '/auth',
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'login' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route'    => '/login',
                                    'defaults' => [
                                        'controller' => 'User\Controller\Auth',
                                        'action'     => 'login'
                                    ]
                                ],
                            ],
                            'logout' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route'    => '/logout',
                                    'defaults' => [
                                        'controller' => 'User\Controller\Auth',
                                        'action'     => 'logout'
                                    ]
                                ],
                            ],
                            'recover' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route'    => '/recover',
                                    'defaults' => [
                                        'controller' => 'User\Controller\Auth',
                                        'action'     => 'recover'
                                    ]
                                ],
                            ],
                            'control' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route'    => '/control',
                                    'defaults' => [
                                        'controller' => 'User\Controller\Auth',
                                        'action'     => 'control'
                                    ]
                                ],
                            ]
                        ]
                    ]
                ]
            ],
            'role' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/role',
                    'defaults' => [
                        'controller' => 'User\Controller\Role',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'form' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/form[/:id]',
                            'defaults' => [
                                'controller' => 'User\Controller\Role',
                                'action'     => 'form'
                            ]
                        ]
                    ],
                ]
            ],            
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'user' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/user[/:id]',
                            'defaults' => [
                                'controller' => 'User\Controller\API\User'
                            ]
                        ],
                    ],
                    'role' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/role[/:id]',
                            'defaults' => [
                                'controller' => 'User\Controller\API\Role'
                            ]
                        ]
                    ],                    
                    'user_configuration' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/user-configuration',
                        ],
                        'may_terminate' => false,
                        'child_routes'  => [
                            'save-table' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/save-table',
                                    'defaults' => [
                                        'controller' => 'User\Controller\API\Configuration',
                                        'action'     => 'saveTable'
                                    ]
                                ],
                            ],
                            'get-table' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/get-table',
                                    'defaults' => [
                                        'controller' => 'User\Controller\API\Configuration',
                                        'action'     => 'getTable'
                                    ]
                                ]
                            ],
                            'save-customisation' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/save-customisation',
                                    'defaults' => [
                                        'controller' => 'User\Controller\API\Configuration',
                                        'action'     => 'saveCustomisation'
                                    ]
                                ]
                            ],
                            'get-configuration-with-table' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route'      => '/get-configuration-with-table',
                                    'defaults' => [
                                        'controller' => 'User\Controller\API\Configuration',
                                        'action'     => 'getConfigurationWithTable'
                                    ]
                                ]
                            ],
                        ]
                    ]
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\AuthController::class              => ServiceLocatorFactory::class,
            Controller\RoleController::class              => ServiceLocatorFactory::class,
            Controller\UserController::class              => ServiceLocatorFactory::class,
            Controller\API\ConfigurationController::class => ServiceLocatorFactory::class,
            Controller\API\RoleController::class          => ServiceLocatorFactory::class,
            Controller\API\UserController::class          => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'User\Controller\Auth'              => Controller\AuthController::class,
            'User\Controller\User'              => Controller\UserController::class,
            'User\Controller\API\User'          => Controller\API\UserController::class,
            'User\Controller\Role'              => Controller\RoleController::class,
            'User\Controller\API\Role'          => Controller\API\RoleController::class,
            'User\Controller\API\Configuration' => Controller\API\ConfigurationController::class,
        ],
    ],
];
