<?php

namespace Budget\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class AccountController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('budget:e:account:r')) {
            return $this->notFoundAction();
        }

        return new ViewModel();
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);
        if (!$this->acl()->basicFormAccess('budget:e:account', $id)) {
            return $this->notFoundAction();
        }

        $account = $this->entityManager()->getRepository('Budget\Entity\Account')->find($id);

        if ($account &&
            !$this->acl()->ownerControl('budget:e:account:u', $account) &&
            !$this->acl()->ownerControl('budget:e:account:r', $account)
        ) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'account' => $account,
        ]);
    }
}
