<?php

namespace Core\Export;

use Core\Extractor\ObjectExtractor;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class ExcelExporter
{
    /**
     * Table name
     * @var string
     */
    protected $name;

    /**
     * Entity name
     * @var string
     */
    protected $entity;

    /**
     * Entity Class
     * @var string
     */
    protected $entityClass;

    /**
     * Rows
     * @var array
     */
    protected $rows = [];

    /**
     * Cols
     * @var array
     */
    protected $cols = [];

    /**
     * @var array
     */
    protected $itemsRemoved = [];

    /**
     * @var array
     */
    protected $itemsSelected = [];

    /**
     * Exporter name
     * @var string
     */
    protected $exporter;

    /**
     * Config
     * @var array
     */
    protected $config = [];

    /**
     * Aliases
     * @var array
     */
    protected $aliases = [];

    /**
     * CustomParsedRows
     * @var \Closure
     */
    protected $customParsedRows;

    /**
     * Translator
     * @var Translate
     */
    protected $translator;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * ExcelExporter constructor.
     * @param $translator
     */
    public function __construct($translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param string $entityClass
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;
        $this->entity      = preg_replace('#^.*([\w]+)$#Usi', '$1', $this->entityClass);
    }

    /**
     * @param $rows
     */
    public function setRows($rows)
    {
        $this->rows = $rows;
    }

    /**
     * @param array $cols
     */
    public function setCols($cols)
    {
        $this->cols = $cols;
        if (!in_array('id', $this->cols)) {
            array_unshift($this->cols, 'id');
        }
    }

    /**
     * @param array $itemsRemoved
     */
    public function setItemsRemoved($itemsRemoved)
    {
        $this->itemsRemoved = $itemsRemoved;
    }

    /**
     * @param array $itemsSelected
     */
    public function setItemsSelected($itemsSelected)
    {
        $this->itemsSelected = $itemsSelected;
    }

    /**
     * @param string $exporter
     */
    public function setExporter($exporter)
    {
        $this->exporter = $exporter;

        if (class_exists($exporter) && class_implements($exporter, IExporter::class)) {
            $this->config  = call_user_func([$exporter, 'getConfig']);
            $this->aliases = call_user_func([$exporter, 'getAliases']);
        }
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Return the Excel file to the user.
     */
    public function render()
    {
        $arr = $this->parsedRows();
        self::download($arr['rows'], $arr['linesBolded']);
    }

    /**
     * Parse the rows
     * @return array
     */
    public function parsedRows()
    {
        $default   = true;
        $rows      = $this->buildArray();
        $extractor = new ObjectExtractor();
        $extractor->setExporter($this->exporter);
        $extractor->setEntityManager($this->entityManager);
        $extractor->setTranslator($this->translator);

        $res = [
            'rows'        => $rows,
            'linesBolded' => [],
        ];

        if (class_exists($this->exporter) && class_implements($this->exporter, IExporter::class)) {
            $this->customParsedRows = call_user_func([$this->exporter, 'getCustomParsedRows'], $extractor, $rows, $this->rows, $this->cols, $this->itemsRemoved, $this->itemsSelected, $this->translator);

            if ($this->customParsedRows) {
                $default = false;
                $res     = $this->customParsedRows;
            }
        }

        if ($default) {
            foreach ($this->rows as $object) {
                if (!in_array($object->getId(), $this->itemsRemoved) && ($this->itemsSelected === [] xor in_array($object->getId(), $this->itemsSelected))) {
                    $row = [];
                    $extractor->setObject($object);

                    foreach ($this->cols as $col) {
                        $row[] = $extractor->extract($col, $this->translate('exporter_error_value'));
                    }

                    $res['rows'][] = $row;
                }
            }
        }
        return $res;
    }

    /**
     * Build the base array.
     *
     * @return array
     */
    public function buildArray()
    {
        $rows = [];

        $row  = [];
        $base = lcfirst($this->entity) . '_field_';
        foreach ($this->cols as $col) {
            if (isset($this->aliases[$col]) && isset($this->config[$this->aliases[$col]])) {
                if (isset($this->config[$this->aliases[$col]]['translation'])) {
                    $val = $this->translate($this->config[$this->aliases[$col]]['translation']);
                } else if (isset($this->config[$this->aliases[$col]]['name'])) {
                    $val = $this->translate($base . $this->config[$this->aliases[$col]]['name']);
                } else {
                    $val = $this->translate($base . $this->aliases[$col]);
                }
            } else if (isset($this->config[$col]) && isset($this->config[$col]['translation'])) {
                $val = $this->translate($this->config[$col]['translation']);
            } else if (isset($this->config[$col]) && isset($this->config[$col]['name'])) {
                $val = $this->translate($base . $this->config[$col]['name']);
            } else if (preg_match('/keyword[0-9]+/', $col)) {
                $val = $this->entityManager->getRepository('Keyword\Entity\Group')->find(str_replace('keyword', '', strstr($col, 'keyword')))->getName();
            } else if (preg_match('/hierarchySup[0-9]+/', $col)) {
                $val = $this->entityManager->getRepository('Keyword\Entity\Group')->find(str_replace('hierarchySup', '', strstr($col, 'hierarchySup')))->getName();
            } else if (preg_match('/indicator[0-9]+/', $col)) {
                $val = $this->entityManager->getRepository('Indicator\Entity\Indicator')->find(str_replace('indicator', '', strstr($col, 'indicator')))->getName();
            } else if (preg_match('/customField[0-9]+/', $col)) {
                $val = $this->entityManager->getRepository('Field\Entity\Field')->find(str_replace('customField', '', strstr($col, 'customField')))->getName();
            } else if ($col == 'id') {
                $val = 'Identifiant';
            } else {
                $val = $this->translate($base . $col);
            }
            $row[] = $val;
        }

        $rows[0] = $row;

        return $rows;
    }

    /**
     * Translate a text.
     *
     * @param $string
     * @return mixed
     */
    public function translate($string)
    {
        if ($this->translator instanceof Translate) {
            $string = $this->translator->__invoke($string);
        }
        return $string;
    }

    public static function download($array, $linesBolded = [], $beforeDownload = null)
    {
        if (is_array($array)) {
            foreach ($array as $i => $row) {
                if (is_array($row)) {
                    foreach ($row as $j => $value) {
                        if (is_numeric($value) && $value == 0) {
                            $array[$i][$j] = '0';
                        }
                    }
                }
            }
        }

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();

        $sheet->fromArray($array, null, 'A1');

        $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())
            ->getAlignment()
            ->setWrapText(true);

        $headerLine = $sheet->getStyle('A1:' . $sheet->getHighestDataColumn() . '1');
        $headerLine->getFont()
            ->setBold(true);
        $headerLine->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER)
            ->setVertical(Alignment::VERTICAL_CENTER);

        foreach ($linesBolded as $line) {
            $headerLine = $sheet->getStyle('A' . $line . ':' . $sheet->getHighestDataColumn() . $line);
            $headerLine->getFont()->setBold(true);
        }

        foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }

        if ($beforeDownload instanceof \Closure) {
            $beforeDownload($spreadsheet, $sheet);
        }


        /**
         * Generates the Excel from the datas
         */
        $objWriter = IOFactory::createWriter($spreadsheet, 'Xlsx');

        /**
         * Return the file in the output
         */
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
        header('Content-Disposition: attachment; filename="export.xlsx"');
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        ob_end_clean();
        $objWriter->save('php://output');
        exit();
    }
}
