<?php

return [
	'module' => [
		'postParc' => [
			'environments' => ['client'],

			'active' => false,
			'required' => false,
			'activable' => false,

			'acl' => [
				'entities' => [],
			],

			'configuration' => [
				'apiUrl' => [
					'type' => 'text',
					'text' => 'post_parc_api_url',
				],
				'user' => [
					'type' => 'text',
					'text' => 'post_parc_api_user',
				],
				'password' => [
					'type' => 'text',
					'text' => 'post_parc_api_password',
				]
			],
		],
	],
	'view_manager' => [
		'template_path_stack' => [__DIR__ . '/../view'],
	],
	'translator' => [
		'translation_file_patterns' => [
			[
				'type' => 'phparray',
				'base_dir' => __DIR__ . '/../language',
				'pattern' => '%s.php',
			],
		],
	],
];
