<?php

namespace Directory\Export;

use Core\Export\IExporter;

abstract class Contact implements IExporter
{
    public static function getConfig()
    {
        return [
            'vip'                => [
                'translation' => 'contact_field_vip',
                'format'      => function ($value, $row, $translator) {
                    return $value
                        ? $translator('yes')
                        : $translator('no');
                },
            ],
            'subscriptionStatus' => [
                'translation' => 'contact_field_subscriptionStatus',
                'format'      => function ($value, $row, $translator) {
                    return $translator('contact_subscription_status_' . $value);
                },
            ],
            'gender' => [
                'translation' => 'contact_field_gender',
                'format' => function ($value, $row, $translator) {
                    if ($value) {
                        return $translator('contact_gender_' . $value);
                    } else {
                        return '';
                    }
                }
            ]
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
