(function(app) {

    app.controller('posteIncomeFormController', [
        'posteIncomeService', 'envelopeService', 'flashMessenger', 'translator', '$scope',
        function posteIncomeFormController(posteIncomeService, envelopeService, flashMessenger, translator, $scope) {
            var self = this;

            var $modal = angular.element('#posteIncomeFormModal');
            var apiParams = {
                col: [
                    'id',
                    'name',
                    'autofinancement',
                    'account.id',
                    'account.name',
                    'account.code',
                    'nature.id',
                    'nature.name',
                    'nature.code',
                    'organization.id',
                    'organization.name',
                    'organization.code',
                    'destination.id',
                    'destination.name',
                    'destination.code',
                    'parent.id',
                    'parent.name',
                    'parent.account.id',
                    'parent.account.code',
                    'project.id',
                    'project.name',
                    'project.parent.id',
                    'envelope.id',
                    'envelope.name',
                    'envelope.financer.id',
                    'envelope.financer.name',
                    'amount',
                    'amountHT',
                    'amountArbo',
                    'amountHTArbo',
                    'amountAggreg',
                    'amountHTAggreg',
                    'amountArboBalance',
                    'caduciteStart',
                    'caduciteEnd',
                    'caduciteSendProof',
                    'arrDate',
                    'folderSendingDate',
                    'folderReceiptDate',
                    'deliberationDate',
                    'arrNumber',
                    'subventionAmount',
                    'subventionAmountHT',
                    'createdAt',
                    'updatedAt',
                    'incomesArbo.id',
                    'incomesArbo.name',
                    'incomesArbo.amount',
                    'incomesArbo.amountHT',
                    'incomesArbo.date',
                    'incomesArbo.type',
                    'incomesArbo.poste.id',
                    'incomesArbo.keywords',
                    'incomesArbo.convention',
                    'incomesArbo.conventionTitle',
                    'incomesArbo.tiers',
                    'incomesArbo.anneeExercice',
                    'incomesArbo.bordereau',
                    'incomesArbo.mandat',
                    'incomesArbo.complement',
                    'incomesArbo.engagement',
                    'incomesArbo.serie',
                    'incomesArbo.project.id',
                    'incomesArbo.project.name',
                    'incomesArbo.age',
                    'keywords'
                ]
            };

            self.displayEnvelopeWarning = false;

            self.posteIncome = {};
            self.from    = {
                type: null,
                id: null
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.init = function init(from, id, types, balances) {
                self.from.type = from;
                self.from.id   = id;

                for (var i in types) {
                    apiParams.col.push('amountArbo' + types[i].ucfirst());
                    apiParams.col.push('amountHTArbo' + types[i].ucfirst());
                }

                for (var j in balances) {
                    apiParams.col.push('amountArbo' + balances[j].ucfirst());
                    apiParams.col.push('amountHTArbo' + balances[j].ucfirst());
                }
            };

            self.checkEnvelopeAmount = function checkEnvelopeAmount() {
                envelopeService.checkAmount(self.posteIncome.envelope.id).then(function(res) {
                    self.displayEnvelopeWarning = parseInt(self.posteIncome.amount) + res.envelope_use_amount > res.envelope_amount;
                });
            }

            self.savePosteIncome = function savePosteIncome() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.posteIncome.id;

                posteIncomeService.save(self.posteIncome, apiParams).then(function(res) {
                    if (isCreation) {
                        $scope.$parent.self.posteIncomes.splice($scope.$parent.self.posteIncomes.length - 1, 0, res.object);
                    } else {
                        for(var i in $scope.$parent.self.posteIncomes){
                            if($scope.$parent.self.posteIncomes[i].id == res.object.id){
                                $scope.$parent.self.posteIncomes[i] = res.object;
                                break;
                            }
                        }
                    }

                    flashMessenger.success(translator('poste_income_message_saved'));
                    self.loading.save = false;
                    $modal.modal('hide');
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            $scope.$on('create-poste-income', function () {
                self.posteIncome = {
                    _rights: {
                        update: true
                    },
                    amount: 0,
                    amountHT: 0,
                    project: $scope.$parent.$parent.$parent.self[self.from.type].id
                };

                self.errors = {
                    fields: {}
                };

                self.panelTitle = translator('poste_income_nav_form');
                $modal.modal('show');

                // Pour reinitialiser le select account
                $modal.on('hidden.bs.modal', function () {
                    $scope.$apply(function() {
                        self.reset = false;
                    });
                });
                self.reset = true;
            });

            $scope.$on('edit-poste-income', function (event, posteIncome) {
                self.editedPosteIncome = posteIncome;
                self.posteIncome = angular.copy(posteIncome);
                self.panelTitle = self.posteIncome.name;

                self.errors = {
                    fields: {}
                };

                $modal.modal('show');

                // Pour reinitialiser le select account
                $modal.on('hidden.bs.modal', function () {
                    $scope.$apply(function() {
                        self.reset = false;
                    });
                });
                self.reset = true;
            });
        }
    ]);

})(window.EVA.app);
