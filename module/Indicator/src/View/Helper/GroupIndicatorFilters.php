<?php

namespace Indicator\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class GroupIndicatorFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'id' => [
                'label' => $this->translator->__invoke('group-indicator_field_id'),
                'type'  => 'string'
            ],
            'name' => [
                'label' => $this->translator->__invoke('group-indicator_field_name'),
                'type'  => 'string'
            ],
            'indicators.id' => [
                'label' => $this->translator->__invoke('group-indicator_field_indicators'),
                'type'  => 'indicator-select'
            ]
        ];

        $this->filters = $filters;
    }
}
