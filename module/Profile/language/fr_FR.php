<?php

return [
    'profile_module_title' => 'Profil',
    'profile_nav_list' => 'Liste des  {{ __tb.total }} profils',
    'profile_field_name' => 'Nom du profil',
    'profile_field_price_per_hour' => 'Prix par heure',
    'profile_field_price_per_day' => 'Prix par jour',
    'profile_field_hour_per_day' => 'Nombre d\'heures par jour',
    'profile_nav_form' => 'Créer un profil',
    'profile_message_saved' => '[[ account_entity | ucfirst ]] enregistré',
    'user_field_profile' => 'Profil',
    'user_field_worked_hours' => 'Nombre d\'heures travaillé par jour',
    'timesheet_type_done_cost' => 'Total passé',
    'timesheet_type_target_cost' => 'Total prévu',
    'timesheet_type_profile' => 'Profil',
    'profile_entity' => 'Profil',
    'timesheet_type_profile_cost' => 'Coût profil',
];
