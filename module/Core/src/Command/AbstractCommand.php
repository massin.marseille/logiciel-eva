<?php

namespace Core\Command;

use Bootstrap\ORM\EntityManagerFactory;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;

/**
 * CLI Command.
 */
abstract class AbstractCommand extends Command
{

    public function __construct(
        protected ?ContainerInterface $serviceLocator = null,
        protected ?string             $name = null
    )
    {
        parent::__construct($name);
    }

    /**
     * By default commande name was the class name.
     * You can override command name.
     *
     * @return string
     */
    public function getDefaultCommandName(): string
    {
        $classNames = explode('\\', get_class($this));
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', str_replace('Command', '', end($classNames))));
    }

    protected function configure(): void
    {
        $this->setName($this->getDefaultCommandName());
        $this->arguments();
    }

    /**
     * Command arguments definitions.
     * Override me for add arguments on your commandline
     * @return void
     */
    protected function arguments(): void
    {
        // DO NOTHING
    }

    /**
     * @return EntityManager
     */
    public function getEmDefault()
    {
        if (!$this->serviceLocator) {
            throw new \Exception(__FILE__ . ' DI Factory is missing. You need to create and declare a DI Factory');
        }
        return $this->serviceLocator->get('DefaultEntityManager');
    }

    /**
     * @return EntityManagerFactory;
     */
    public function getEmFactory()
    {
        if (!$this->serviceLocator) {
            throw new \Exception(__FILE__ . ' DI Factory is missing. You need to create and declare a DI Factory');
        }
        return $this->serviceLocator->get('EntityManagerFactory');
    }
}
