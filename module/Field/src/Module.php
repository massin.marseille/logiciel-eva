<?php

namespace Field;

use Core\Module\AbstractModule;
use Core\Utils\RequestUtils;
use Doctrine\Common\EventManager;
use Field\Controller\Plugin\AllowedFieldGroups;
use Field\Controller\Plugin\AllowedFieldGroupsPlugin;
use Field\Event\FieldSubscriber;
use Field\Event\ValueSubscriber;
use Field\View\Helper\AllowedFieldGroupsHelper;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;

class Module extends AbstractModule
{
    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        $request = $application->getRequest();

        if (!RequestUtils::isCommandRequest($request)) {
            /** @var EventManager $eventManager */
            $eventManager = $application
                ->getServiceManager()
                ->get('Environment')
                ->getEntityManager()
                ->getEventManager();

            $eventManager->addEventsubscriber(new FieldSubscriber());
            $eventManager->addEventsubscriber(new ValueSubscriber());
        }
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'allowedFieldGroups' => function (ServiceManager $serviceManager) {
                    $env = $serviceManager->get('Environment');
                    $moduleManager = $serviceManager->get('MyModuleManager');
                    $user = $serviceManager->get('AuthStorage')->getUser();
                    $acl = $serviceManager->get('Acl');

                    return new AllowedFieldGroups($env->getEntityManager(), $acl, $user);
                }
            ]
        ];
    }

    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'allowedFieldGroups' => function (ServiceManager $serviceManager) {
                    $allowedFieldGroups = $serviceManager->get('allowedFieldGroups');
                    return new AllowedFieldGroupsPlugin($allowedFieldGroups);
                },
            ]
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'allowedFieldGroups' => function (ServiceManager $serviceManager) {
                    $allowedFieldGroups = $serviceManager->get('allowedFieldGroups');
                    return new AllowedFieldGroupsHelper($allowedFieldGroups);
                },
            ]
        ];
    }
}
