(function(app) {

    app.factory('clientService', [
        'restFactoryService', '$http', '$q',
        function clientService(restFactoryService, $http, $q) {
            return angular.extend({
                getUsers: function (client) {
                    var deferred = $q.defer();

                    $http({
                        method: 'GET',
                        url: '/client/' + client.id + '/users'
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                },
                loginAs: function (client, user) {
                    var deferred = $q.defer();

                    $http({
                        method: 'GET',
                        url: '/client/' + client.id + '/users/login/' + user.id
                    }).then(function(res) {
                        deferred.resolve(res.data)
                    }, function(err) {
                        deferred.reject(err)
                    });

                    return deferred.promise;
                }
            }, restFactoryService.create('/api/client'));
        }
    ])

})(window.EVA.app);
