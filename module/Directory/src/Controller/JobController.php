<?php

namespace Directory\Controller;

use Core\Controller\AbstractActionSLController;
use Laminas\View\Model\ViewModel;

class JobController extends AbstractActionSLController
{
    public function indexAction()
    {
        if (!$this->acl()->isAllowed('directory:e:job:r')) {
            return $this->notFoundAction();
        }

        $groups = [];
        if ($this->moduleManager()->isActive('keyword')) {
            $groups = $this->allowedKeywordGroups('job', false);
        }

        return new ViewModel([
            'groups'  => $groups,
        ]);
    }

    public function formAction()
    {
        $id = $this->params()->fromRoute('id', false);

        $job = $this->entityManager()->getRepository('Directory\Entity\Job')->find($id);

        if ($job && 
        !$this->acl()->ownerControl('directory:e:job:job', $job) && 
        !$this->acl()->ownerControl('directory:e:job:r', $job)) {
            return $this->notFoundAction();
        }

        return new ViewModel([
            'job' => $job
        ]);
    }
}
