<?php

return [
    'module' => [
        'field' => [
            'environments' => [
                'client'
            ],
            'active'   => true,
            'required' => false,
            'acl' => [
                'entities' => [
                    [
                        'name'     => 'fieldGroup',
                        'class'    => 'Field\Entity\Group',
                        'owner'    => ['service']
                    ],
                    [
                        'name'     => 'field',
                        'class'    => 'Field\Entity\Field',
                        'owner'    => []
                    ]
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
