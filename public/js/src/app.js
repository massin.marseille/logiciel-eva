window.EVA = {};

String.prototype.ucfirst = function(){
    return this.charAt(0).toUpperCase() + this.substr(1);
};

String.prototype.toCamelCase = function(){
    return this.replace(/[\s,_](\w)/ig, function(all, letter){return letter.toUpperCase();})
        .replace(/(^\w)/, function($1){return $1.toLowerCase()});
};

(function(angular, EVA) {

    if (typeof appDependencies == 'undefined') {
        appDependencies = [];
    }

    appDependencies.push('selectize');
    appDependencies.push('angularFileUpload');
    appDependencies.push('ui.tinymce');
    appDependencies.push('ui.sortable');
    appDependencies.push('angular.filter');
    appDependencies.push('nvd3');
    appDependencies.push('rzModule');

    EVA.appName = 'eva';
    EVA.app     = angular.module(EVA.appName, appDependencies);

    EVA.app.config([
        '$httpProvider', '$compileProvider', '$provide', '$qProvider',
        function($httpProvider, $compileProvider, $provide, $qProvider) {
            $httpProvider.interceptors.push([
                '$rootScope', '$q',
                function($rootScope, $q) {
                    return {
                        'request': function(config) {
                            $rootScope.httpLoading = true;
                            return config;
                        },
                        'response': function(response) {
                            $rootScope.httpLoading = false;
                            return response;
                        },
                        'responseError': function(rejection) {
                            $rootScope.httpLoading = false;
                            return $q.reject(rejection);
                        }
                    }
                }
            ]);

            $qProvider.errorOnUnhandledRejections(false);

            $compileProvider.debugInfoEnabled(false);

            $provide.decorator('$browser', [
                '$delegate',
                function ($delegate) {
                    $delegate.onUrlChange = function () {};
                    $delegate.url = function () { return ''};

                    return $delegate;
                }
            ]);
        }
    ]);

    EVA.app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
    EVA.app.filter('dateFormat', function() {
        return function(input, format) {
            if (input) {
                return moment(input, 'DD/MM/YYYY HH:mm').format(format);
            }
            return null;
        };
    });

    EVA.app.run([
        '$rootScope', 'controlService',
        function($rootScope, controlService) {

            controlService.start();

            $rootScope.forceDelete = function (foo, bar) {
                if (typeof foo !== 'undefined') {
                    delete foo[bar];
                }
            }
        }
    ]);

    EVA.app.directive('includeReplace', function () {
        return {
            require: 'ngInclude',
            restrict: 'A', /* optional */
            link: function (scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    });

    EVA.app.controller('voidController', [function(){}]);

    angular.element(document).ready(function() {
        angular.bootstrap(document, [EVA.appName]);
        angular.element('[data-toggle="tooltip"]').tooltip();
    });

})(window.angular, window.EVA);
