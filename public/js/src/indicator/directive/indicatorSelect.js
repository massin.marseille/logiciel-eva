(function(app) {

    app.directive('indicatorSelect', [
        'indicatorService', 'selectFactoryService',
        function indicatorSelect(indicatorService, selectFactoryService) {
            var optgroups = [
                {label: 'A', value: 'fixed'},
                {label: 'B', value: 'free'}
            ];

            return selectFactoryService.create({
                service : {
                    object: indicatorService,
                    apiParams: {
                        col: ['id', 'name', 'keywords', 'values', 'type','definition', 'description', 'method']
                    }
                },
                selectize: {
                    optgroups: optgroups,
                    labelField: 'name',
                    valueField: 'id',
                    searchField: ['name'],
                    onLoad: function(scope, data) {
                        var t =  {id: 55555, name: 'intest'};
                        data.push(t);
                    },
                    optgroupField: 'type'
                }
            });
        }
    ])

})(window.EVA.app);
