(function(app) {

    app.controller('territoryFormController', [
        'territoryService', 'flashMessenger', 'translator', 'leafletData', 'FileUploader', 'toGeoJson', '$scope',
        function territoryFormController(territoryService, flashMessenger, translator, leafletData, FileUploader, toGeoJson, $scope) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'code',
                    'json',
                    'children.id',
                    'children.name',
                    'keywords'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.territory = {
                values: [],
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    territoryService.find(id, apiParams).then(function (res) {
                        self.territory = res.object;
                        self.panelTitle = self.territory.name;
                    });
                }
            };

            var layer = null;
            $scope.$watch(function () { return self.territory.json }, function () {
                if (self.territory.json) {
                    leafletData.getMap('map').then(function(map) {
                        if (layer !== null) {
                            map.removeLayer(layer);
                        }
                        layer = L.geoJson(self.territory.json, {
                            fillColor: '#ff2424',
                            color: '#ff2424',
                            weight: 2,
                            fillOpacity: 0.5
                        }).addTo(map);
                        map.fitBounds(layer.getBounds());
                    });
                }
            });

            self.saveTerritory = function saveTerritory() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.territory.id;

                territoryService.save(self.territory, apiParams).then(function (res) {
                    self.territory = res.object;
                    self.panelTitle = self.territory.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.territory.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('territory_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteTerritory = function deleteTerritory() {
                if (confirm(translator('territory_question_delete').replace('%1', self.territory.name))) {
                    territoryService.remove(self.territory).then(function (res) {
                        if (res.success) {
                            window.location = '/map/territory';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };


            self.uploader = new FileUploader();
            self.uploader.filters.push({
                name: 'geoFilter',
                fn: function(item) {
                    var type = self.getFileExtension(item.name);
                    return '|kml|csv|geojson|json|gpx|wkt|'.indexOf(type) !== -1;
                }
            });

            self.uploader.onAfterAddingFile = function(fileItem) {
                self.fileError = false;
                toGeoJson.fromFile(fileItem._file).then(function(res) {
                    self.territory.json = res;
                });
            };
            self.uploader.onWhenAddingFileFailed = function() {
                self.errors.fields['json'] = translator('territory_field_json_wrong_format');
            };

            self.getFileExtension = function getFileExtension(filename) {
                return filename.split('.').pop();
            }
        }
    ]);

})(window.EVA.app);
