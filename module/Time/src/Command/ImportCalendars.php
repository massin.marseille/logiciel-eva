<?php

namespace Time\Command;

use Bootstrap\Entity\Client;
use Core\Command\AbstractCommand;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Time\Service\TimeService;

class ImportCalendars extends AbstractCommand
{

    /**
     * Sync. calendars between app and external Ical calendars.
     *
     * Install on unix system :
     * ON PRODUCTION : 0 7 * * * php vendor/bin/laminas time:import-calendars >> /opt/logs/import_calendars_$(date +\%Y\%m\%d\%H\%M\%S).log
     * ON PREPROD : 0 7 * * * php vendor/bin/laminas time:import-calendars >> /opt/logs/import_calendars_$(date +\%Y\%m\%d\%H\%M\%S).log
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('[START] Importing calendars...');

        $em                   = $this->getEmDefault();
        $entityManagerFactory = $this->getEmFactory();
        $clients              = $em->getRepository('Bootstrap\Entity\Client')->findAll();
        $haveError            = Command::SUCCESS;

        /** @var Client $client */
        foreach ($clients as $client) {
            try {
                $modules = $client->getModules();
                if (isset($modules['time']) && $modules['time']['active']) {
                    $output->writeln('[ ' . $client->getName() . ' ] Process...');

                    $clientEm = $entityManagerFactory->getEntityManager($client);

                    try {
                        $clientEm->getConnection()->connect();

                        $timeService = new TimeService($clientEm, $this->serviceLocator);
                        $timeService->setModulesConfiguration($modules);
                        $result = $timeService->importCalendars();

                        $this->outputResult($output, $client, $result);
                    } catch (Exception $e) {
                        $output->writeln('[ERROR] - ' . $client->getName() . ' - ' . $e->getMessage());
                        $haveError = Command::FAILURE;
                    }

                }
            } catch (\Exception $e) {
                $output->writeln('[ERROR] - ' . $client->getName() . ' - ' . $e->getMessage());
                $haveError = Command::FAILURE;
            }
        }

        $output->writeln('[END] Importing calendars...');
        return $haveError;
    }

    /**
     * @param OutputInterface $output
     * @param Client $client
     * @param array $result
     * @return void
     */
    private function outputResult(OutputInterface $output, Client $client, array $result)
    {
        $output->writeln('[INFO] - ' . $client->getName());
        $output->writeln(' - Success : ' . $result['countSuccess'] . '/' . $result['countTotal']);
        $output->writeln(' - Created : ' . $result['countCreated'] . '/' . $result['countSuccess']);
        $output->writeln(' - Updated : ' . $result['countUpdated'] . '/' . $result['countSuccess']);
        $output->writeln(' - BusinessError : ' . (empty($result['errors']['process']) ? '0' : PHP_EOL . '   - ' . implode(PHP_EOL . '   - ', $result['errors']['process'])));
        $output->writeln(' - PersistError : ' . (empty($result['errors']['persist']) ? '0' : PHP_EOL . '   - ' . implode(PHP_EOL . '   - ', $result['errors']['persist'])));
    }
}
