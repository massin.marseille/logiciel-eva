<?php

use Bootstrap\Entity\Client;
use Budget\Entity\Expense;
use Core\Controller\Plugin\ApiRequestPlugin;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style;
use Project\Entity\Project;

class Export
{
	const STYLE_TABLE        = [
		'borders' => [
			'outline' => [
				'borderStyle' => Style\Border::BORDER_MEDIUM,
				'color'       => ['rgb' => '000000'],
			],
			'inside'  => [
				'borderStyle' => Style\Border::BORDER_THIN,
				'color'       => ['rgb' => '000000'],
			],
		],
	];

	const STYLE_ALIGN_CENTER = [
		'alignment' => [
			'horizontal' => Style\Alignment::HORIZONTAL_CENTER,
			'vertical'   => Style\Alignment::VERTICAL_CENTER,
		],
	];

	const STYLE_BOLD         = [
		'font' => [
			'bold' => true,
		],
	];

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var Translate
	 */
	protected $translator;

	/**
	 * @var ApiRequestPlugin
	 */
	protected $apiRequest;

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * Export constructor.
	 *
	 * @param EntityManager    $em
	 * @param Translate        $translator
	 * @param ApiRequestPlugin $apiRequest
	 * @param Client           $client
	 */
	public function __construct(EntityManager $em, Translate $translator, ApiRequestPlugin $apiRequest, Client $client)
	{
		$this->em         = $em;
		$this->translator = $translator;
		$this->apiRequest = $apiRequest;
		$this->client     = $client;
	}

	public function doExport($params, $data)
	{
		ini_set('memory_limit', -1);
		ini_set('max_execution_time', -1);
		setlocale(LC_TIME, 'fr_FR', 'French_France', 'French');

		/** @var Project $project */
		$project = $this->em->getRepository(Project::class)->find($data['id']);

		$lines = [
			['Récapitulatif des dépenses'],
			["Projet : {$project->getName()}"],
			[]
		];

		$headerLine = [
			'Date Cpt.',
			'N°',
			'Objet',
			'Tiers',
			'CP',
		];

		$lines[] = $headerLine;

		foreach ($project->getPosteExpensesArbo() as $poste) {
			foreach ($poste->getExpensesArbo() as $expense) {
				if ($expense->getType() !== Expense::TYPE_CP_COMMITTED) {
					continue;
				}

				$expenseLine = [
					$expense->getDate()->format('d/m/Y'),
					$expense->getBordereau(),
					$expense->getComplement(),
					$expense->getTiers(),
					$expense->getAmount(),
				];

				$lines[] = $expenseLine;
			}
		}

		$sum = '';
		$expensesLength = count($lines) - 4;

		if ($expensesLength > 0) {
			$sum = '=SUM(E5:E' . ($expensesLength + 4) . ')';
		}

		$lines[] = ['Total', '', '', '', $sum];

		$this->download($lines);
	}

	protected function download($array)
	{
		try {
			foreach ($array as $i => $line) {
				foreach ($line as $j => $value) {
					if (is_numeric($value) && $value == 0) {
						$array[$i][$j] = '0';
					}
				}
			}

			$spreadsheet = new Spreadsheet();
			$sheet       = $spreadsheet->getActiveSheet();

			$sheet->fromArray($array);

			$sheet->getStyle('A1:E1')->applyFromArray(array_merge(
				self::STYLE_TABLE,
				self::STYLE_BOLD,
				self::STYLE_ALIGN_CENTER
			));
			$sheet->mergeCells('A1:E1');

			$sheet->getStyle('A2:E2')->applyFromArray(array_merge(
				self::STYLE_BOLD,
				self::STYLE_ALIGN_CENTER
			));

			$sheet->mergeCells('A2:E2');

			$highestDataRow = $sheet->getHighestDataRow();
			$sheet->getStyle("A4:E$highestDataRow")->applyFromArray(self::STYLE_TABLE);
			$sheet->getStyle("A4:E4")->applyFromArray(self::STYLE_BOLD);

			$sheet->getStyle("A{$highestDataRow}:E$highestDataRow")->applyFromArray(self::STYLE_BOLD);
			$sheet->getStyle("A{$highestDataRow}:D$highestDataRow")->applyFromArray(self::STYLE_ALIGN_CENTER);
			$sheet->mergeCells("A{$highestDataRow}:D$highestDataRow");

			foreach (range('A', $sheet->getHighestDataColumn()) as $col) {
				$sheet->getColumnDimension($col)->setAutoSize(true);
			}

			$sheet
				->getStyle('A1:' . $sheet->getHighestDataColumn() . $sheet->getHighestDataRow())
				->getAlignment()
				->setWrapText(true);

			$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

			header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
			header('Content-Disposition: attachment; filename="export.xlsx"');
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false);
			ob_end_clean();
			$writer->save('php://output');
			exit;
		} catch (Exception $exception) {
			var_dump($exception->getMessage());
		}
	}
}
