<?php
/**
 * Created by PhpStorm.
 * User: curtis
 * Date: 12/08/16
 * Time: 15:10
 */

namespace Attachment\Export;

use Core\Export\IExporter;

abstract class Attachment implements IExporter
{
    public static function getConfig()
    {
        return [
            'user.name' => [
                'name' => 'user',
            ],
            'type'      => [
                'format' => function ($value, $obj, $translator) {
                    return $translator('attachment_type_' . $value);
                },
            ],
            'size'      => [
                'format' => function ($bytes) {
                    $precision = 0;
                    $units = array('B', 'KB', 'MB', 'GB', 'TB');

                    $bytes = max($bytes, 0);
                    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
                    $pow = min($pow, count($units) - 1);

                    $bytes /= pow(1024, $pow);
                    return round($bytes, $precision) . ' ' . $units[$pow];
                },
            ],
        ];
    }

    public static function getAliases()
    {
        return [
            'user' => 'user.name',
        ];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}