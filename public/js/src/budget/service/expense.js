(function(app) {

    app.factory('expenseService', [
        'restFactoryService',
        function expenseService(restFactoryService) {
            return restFactoryService.create('/api/budget/expense');
        }
    ])

})(window.EVA.app);
