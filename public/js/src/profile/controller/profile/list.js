(function(app) {

    app.controller('profileListController', [
        'profileService', '$scope', 'flashMessenger', 'translator', '$timeout',
        function profileListController(profileService, $scope, flashMessenger, translator, $timeout) {
            var self = this;

            self.currentUser = null;
            self.view = 'list';
            self.profiles   = [];
            self.options = {
                name: 'profiles-list',
                col: [
                    'id',
                    'name',
                    'price_per_hour',
                    'price_per_day',
                    'hour_per_day',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: true,
                getItems: function getData(params, callback, abort) {
                    if (self.query != null) {
                        params.search.data.filters = self.query.filters;
                    }

                    profileService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                    });
                }
            };

            self.from   = {
                type: null,
                id: null,
                object: null
            };

            self.init = function init(currentUser, from, id) {
                self.currentUser = currentUser;

                self.from.type = from;
                self.from.id   = id;

            };

            self.initHome = function (name, query) {
                self.options.name = name;
                self.query = query;
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(profile) {
                if (angular.isArray(profile.keywords) || profile.keywords == null) {
                    profile.keywords = {};
                }

                self.editedLineErrors = {};
                self.editedLine       = profile;
                self.editedLineFields = angular.copy(profile);
            };

            self.saveEditedLine = function saveEditedLine(profile) {
                self.editedLineErrors = {};
                profileService.save(angular.extend({}, profile, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        profile = angular.extend(profile, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        flashMessenger.success(translator('profile_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                });
            };

            self.createProfile = function () {
                $scope.$broadcast('create-profile', {user: self.currentUser, project: self.from.object ? self.from.object.id : null});
            };

            self.editprofile = function editprofile(profile) {
                $scope.$broadcast('edit-profile', profile);
            };

            self.deleteprofile = function deleteprofile(profile) {
                if (confirm(translator('profile_question_delete').replace('%1', profile.name))) {
                    profileService.remove(profile).then(function (res) {
                        if (res.success) {
                            self.profiles.splice(self.profiles.indexOf(profile), 1);
                            flashMessenger.success(translator('profile_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };


            $scope.$watch(function () {
                return self.view
            }, function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    self.options.pager = self.view === 'list';
                    $scope.__tb.loadData();
                }
            });


        }
    ])

})(window.EVA.app);
