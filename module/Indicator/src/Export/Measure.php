<?php

namespace Indicator\Export;

use Core\Export\IExporter;

abstract class Measure implements IExporter
{
    public static function getConfig()
    {
        return [
            'indicator.name' => [
                'name' => 'indicator'
            ],
            'campain.name' => [
                'name' => 'campain'
            ],
            'project.name' => [
                'name' => 'project',
            ],
            'type'     => [
                'format' => function ($value, $row, $translator) {
                    return $translator('measure_type_' . $value);
                },
            ],
        ];
    }

    public static function getAliases()
    {
        return [];
    }

    public static function getCustomParsedRows($extractor, $rows, $objects, $cols, $itemsRemoved, $itemsSelected, $translator)
    {
        return null;
    }
}
