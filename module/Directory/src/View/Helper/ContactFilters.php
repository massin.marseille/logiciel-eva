<?php

namespace Directory\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Directory\Entity\Contact;
use Doctrine\ORM\EntityManager;

class ContactFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [];

        if ($moduleManager->isActive('event')) {
            $filters['vip'] = [
                'label' => $this->translator->__invoke('contact_field_vip'),
                'type'  => 'boolean',
            ];

            $opts = [];
            foreach (Contact::getSubscriptionStatutes() as $status) {
                $opts[] = [
                    'value' => $status,
                    'text'  => $this->translator->__invoke('contact_subscription_status_' . $status),
                ];
            }
            $filters['subscriptionStatus'] = [
                'label' => $this->translator->__invoke('contact_field_subscriptionStatus'),
                'type'  => 'manytoone',
                'options' => $opts
            ];
            $filters['subscriptionStatusUpdatedAt'] = [
                'label' => $this->translator->__invoke('contact_field_subscriptionStatusUpdatedAt'),
                'type'  => 'datetime'
            ];
            $filters['eventSpecial'] = [
                'label' => $this->translator->__invoke('invitation_filter_event_special'),
                'type'  => 'event-special',
            ];
        }

        $filters = array_merge($filters, [
            'id'    => [
                'label' => $this->translator->__invoke('contact_field_id'),
                'type'  => 'string',
            ],
            'firstName'    => [
                'label' => $this->translator->__invoke('contact_field_firstName'),
                'type'  => 'string',
            ],
            'lastName'     => [
                'label' => $this->translator->__invoke('contact_field_lastName'),
                'type'  => 'string',
            ],
            'gender'       => [
                'label' => $this->translator->__invoke('contact_field_gender'),
                'type'  => 'manytoone',
                'options' => [
                    [
                        'value' => 'm',
                        'text'  => $this->translator->__invoke('contact_gender_m'),
                    ],
                    [
                        'value' => 'mme',
                        'text'  => $this->translator->__invoke('contact_gender_mme'),
                    ],
                    [
                        'value' => 'neutral',
                        'text'  => $this->translator->__invoke('contact_gender_neutral'),
                    ]
                ]
            ],
            'email'        => [
                'label' => $this->translator->__invoke('contact_field_email'),
                'type'  => 'string',
            ],
            'phone'        => [
                'label' => $this->translator->__invoke('contact_field_phone'),
                'type'  => 'string',
            ],
            'portable'        => [
                'label' => $this->translator->__invoke('contact_field_portable'),
                'type'  => 'string',
            ],
            'zipcode'      => [
                'label' => $this->translator->__invoke('contact_field_zipcode'),
                'type'  => 'string',
            ],
            'country'      => [
                'label' => $this->translator->__invoke('contact_field_country'),
                'type'  => 'string',
            ],
            'city'         => [
                'label' => $this->translator->__invoke('contact_field_city'),
                'type'  => 'string',
            ],
            'addressLine1' => [
                'label' => $this->translator->__invoke('contact_field_addressLine1'),
                'type'  => 'string',
            ],
            'addressLine2' => [
                'label' => $this->translator->__invoke('contact_field_addressLine2'),
                'type'  => 'string',
            ],
            'createdAt'    => [
                'label' => $this->translator->__invoke('contact_field_createdAt'),
                'type'  => 'date',
            ],
            'updatedAt'    => [
                'label' => $this->translator->__invoke('contact_field_updatedAt'),
                'type'  => 'date',
            ],
            'job'          => [
                'label' => $this->translator->__invoke('contact_field_job'),
                'type'  => 'job-select',
            ],
            'structure'    => [
                'label' => $this->translator->__invoke('job_field_structure'),
                'type'  => 'structure-select',
            ],
            'contactEmails'    => [
                'label' => ucfirst($this->translator->__invoke('contactEmail_entities')),
                'type'  => 'contact-email-select',
            ],
        ]);

        if ($this->moduleManager->isActive('keyword')) {
            $groups = $allowedKeywordGroups('contact', false);
            foreach ($groups as $group) {
                $filters['keyword.' . $group->getId()] = [
                    'label' => $group->getName(),
                    'type'  => 'keyword-select',
                    'group' => $group->getId(),
                ];
            }
        }

        $this->filters = $filters;
    }
}
