(function(app) {

    app.controller('accountListController', [
        'accountService', '$scope', 'flashMessenger', 'translator', 'treeBuilderService',
        function accountListController(accountService, $scope, flashMessenger, translator, treeBuilderService) {
            var self = this;

            self.fusion = {};
            self.loading = {};
            self.originalAccount = [];

            self.accounts = [];
            self.options = {
                name: 'accounts-list',
                col: [
                    'id',
                    'name',
                    'code',
                    'type',
                    'default',
                    'description',
                    'parent.id',
                    'parent.name',
                    'createdAt',
                    'updatedAt'
                ],
                searchType: 'list',
                sort: 'name',
                order: 'asc',
                pager: false,
                getItems: function getData(params, callback, abort) {
                    accountService.findAll(params, abort.promise).then(function (res) {
                        self.originalAccount = res.rows;
                        var order = ($scope.__tb.apiParams.search.data.order === 'asc' ? '' : '-') + $scope.__tb.apiParams.search.data.sort;
                        res.rows = treeBuilderService.toArray(treeBuilderService.toTree(res.rows, order));
                        callback(res);
                    });
                }
            };

            self.fullTextSearch = function fullTextSearch() {
                $scope.__tb.resetFilters();
                $scope.__tb.apiParams.search.data.full = self.fullText;
                $scope.__tb.loadData();
            };

            self.resetfullTextSearch = function resetfullTextSearch() {
                self.fullText = '';
                delete $scope.__tb.apiParams.search.data.full;
            };

            angular.element('#full-text').on('keypress', function(event) {
                if(event.which == 13) {
                    self.fullTextSearch();
                }
            });

            self.editedLine       = null;
            self.editedLineFields = null;
            self.editedLineErrors = {};
            self.editLine = function editLine(account) {
                self.editedLineErrors = {};
                self.editedLine       = account;
                self.editedLineFields = angular.copy(account);
            };

            self.saveEditedLine = function saveEditedLine(account) {
                self.editedLineErrors = {};
                accountService.save(angular.extend({}, account, self.editedLineFields), {
                    col: self.options.col
                }).then(function (res) {
                    if (res.success) {
                        account = angular.extend(account, res.object);
                        self.editedLine       = null;
                        self.editedLineFields = null;

                        $scope.__tb.loadData();

                        flashMessenger.success(translator('account_message_saved'));
                    }
                }, function (err) {
                    for (var field in err.fields) {
                        self.editedLineErrors[field] = err.fields[field];
                    }
                })
            };

            self.deleteAccount = function deleteAccount(account) {
                if (confirm(translator('account_question_delete').replace('%1', account.code + ' - ' + account.name))) {
                    accountService.remove(account).then(function (res) {
                        if (res.success) {
                            self.accounts.splice(self.accounts.indexOf(account), 1);
                            flashMessenger.success(translator('account_message_deleted'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

            self.showFusionModal = function () {
                angular.element('#fusionModal').modal('show');
            };

            self.doFusion = function () {
                if (!self.fusion.keep) {
                    flashMessenger.error(translator('error_occured'));
                    return;
                }

                self.loading.fusion = true;

                var ids = [];
                for (var i in self.accounts) {
                    if (
                        self.accounts[i].selected &&
                        self.accounts[i].id !== self.fusion.keep.id
                    ) {
                        ids.push(self.accounts[i].id);
                    }
                }

                if (ids.length === 0) {
                    flashMessenger.success(translator('fusion_message_success'));
                    return;
                }

                accountService
                    .fusion(self.fusion.keep.id, ids)
                    .then(function (res) {
                        self.loading.fusion = false;
                        $scope.__tb.loadData();
                        angular.element('#fusionModal').modal('hide');

                        if (res.success) {
                            flashMessenger.success(translator('fusion_message_success'));
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
            };
        }
    ])

})(window.EVA.app);
