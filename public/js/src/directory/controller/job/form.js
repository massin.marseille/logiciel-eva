(function(app) {

    app.controller('jobFormController', [
        'jobService', 'flashMessenger', 'translator', '$scope',
        function jobFormController(jobService, flashMessenger, translator, $scope) {
            var self = this;

            var apiParams = {
                col: [
                    'id',
                    'name',
                    'keywords'
                ]
            };

            self.loading = {
                save: false
            };

            self.errors = {
                fields: {}
            };

            self.job = {
                values: [],
                _rights: {
                    update: true
                }
            };

            self.init = function init(id) {
                if (id) {
                    jobService.find(id, apiParams).then(function (res) {
                        self.job = res.object;
                        self.panelTitle = self.job.name;
                    });
                }
            };

            self.saveJob = function saveJob() {
                self.errors.fields = {};
                self.loading.save = true;

                var isCreation = !self.job.id;

                jobService.save(self.job, apiParams).then(function (res) {
                    self.job = res.object;
                    self.panelTitle = self.job.name;
                    self.loading.save = false;

                    if (isCreation) {
                        var url = window.location.pathname + '/' + self.job.id;
                        history.replaceState('', '', url);
                    }

                    flashMessenger.success(translator('job_message_saved'));
                }, function (err) {
                    for (var field in err.fields) {
                        self.errors.fields[field] = err.fields[field];
                    }

                    self.loading.save = false;
                })
            };

            self.deleteJob = function deleteJob() {
                if (confirm(translator('job_question_delete').replace('%1', self.job.name))) {
                    jobService.remove(self.job).then(function (res) {
                        if (res.success) {
                            window.location = '/directory/job';
                        } else {
                            flashMessenger.error(translator('error_occured'));
                        }
                    });
                }
            };

        }
    ]);

})(window.EVA.app);
