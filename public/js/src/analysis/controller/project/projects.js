(function (app) {

    app.controller('projectsController', [
        'projectService', '$timeout', '$scope', '$filter',
        function projectsController(projectService, $timeout, $scope, $filter) {
            var self = this;

            self.projects = [];

            self.loading = false;

            self.settingsTime = {};

            self.options = {
                name      : 'budget-analysis-projects-list',
                col       : [
                    'id',
                    'name',
                    'actors.structure.id',
                ],
                searchType: 'list',
                sort      : 'name',
                order     : 'asc',
                pager     : false,
                autoload  : false,
                getItems  : function getData(params, callback, abort) {
                    self.loading = true;

                    params.master = $scope.isMaster;

                    projectService.findAll(params, abort.promise).then(function (res) {
                        callback(res);
                        self.loading = false;
                    });
                }
            };

            self.export = function () {
                window.open('/analysis/project/projects?' + $.param({filters: $scope.__tb.apiParams.search.data.filters, export: true}));
            };

            self.getTotal = function (type) {
                return self.getPartialTotal(type, self.projects);
            };

            self.getPartialTotal = function (type, projects) {
                var total = 0;
                var uniq  = [];
                for (var i in projects) {
                    if (type == 'actors') {
                        for (var j in projects[i].actors) {
                            var actor = projects[i].actors[j];
                            var key   = $scope.isMaster ? projects[i]._client.id + '_' + actor.structure.id : actor.structure.id;
                            if (uniq.indexOf(key) < 0) {
                                total++;
                                uniq.push(key);
                            }
                        }
                    }
                }
                return total;
            };

            if ($scope.isMaster) {
                self.charts = {
                    projects: {
                        api    : {},
                        options: {
                            chart: {
                                type         : 'pieChart',
                                showControls : false,
                                height       : 300,
                                labelsOutside: true,
                                x            : function (d) {
                                    return d.label;
                                },
                                y            : function (d) {
                                    return d.value;
                                },
                                tooltip      : {
                                    valueFormatter: function (d) {
                                        return Math.round(d);
                                    }
                                }
                            }
                        },
                        data   : []
                    },
                    actors  : {
                        api    : {},
                        options: {
                            chart: {
                                type         : 'pieChart',
                                showControls : false,
                                height       : 300,
                                labelsOutside: true,
                                x            : function (d) {
                                    return d.label;
                                },
                                y            : function (d) {
                                    return d.value;
                                },
                                tooltip      : {
                                    valueFormatter: function (d) {
                                        return Math.round(d);
                                    }
                                }
                            }
                        },
                        data   : []
                    }
                };

                $scope.$watch(function () { return self.projects}, function () {
                    self.charts.projects.data = [];
                    self.charts.actors.data   = [];

                    var clients = $filter('groupBy')(self.projects, '_client.name');
                    for (var client in clients) {
                        self.charts.projects.data.push({
                            label: client,
                            value: clients[client].length
                        });

                        self.charts.actors.data.push({
                            label: client,
                            value: self.getPartialTotal('actors', clients[client]) || 0
                        });
                    }
                });

                $scope.$watch('tabProjectsLoaded', function (newVal) {
                    $timeout(function () {
                        self.charts.actors.api.refresh();
                        self.charts.projects.api.refresh();
                    });
                });
            }
        }
    ])

})(window.EVA.app);
