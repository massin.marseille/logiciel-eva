<?php

namespace Campain\View\Helper;

use Core\Module\MyModuleManager;
use Core\View\Helper\EntityFilters;
use Core\View\Helper\Translate;
use Doctrine\ORM\EntityManager;

class CampainFilters extends EntityFilters
{
    public function __construct(Translate $translator, MyModuleManager $moduleManager, EntityManager $entityManager, $allowedKeywordGroups)
    {
        parent::__construct($translator, $moduleManager, $entityManager);

        $filters = [
            'name' => [
                'label' => $this->translator->__invoke('campain_field_name'),
                'type'  => 'string'
            ],
            'evaluationManagers' => [
                'label' => $this->translator->__invoke('campain_field_evaluationManagers'),
                'type'  => 'user-select'
            ],
            'startDate' => [
                'label' => $this->translator->__invoke('measure_field_start'),
                'type'  => 'date'
            ],
            'endDate' => [
                'label' => $this->translator->__invoke('measure_field_date'),
                'type'  => 'date'
            ]
        ];

        $this->filters = $filters;
    }
}
