<?php

namespace Keyword;

use Core\Controller\Factory\ServiceLocatorFactory;

return [
    'router' => [
        'client-routes' => [
            'keyword' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/keyword',
                    'defaults' => [
                        'controller' => 'Keyword\Controller\Index',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
            ],
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/api',
                ],
                'may_terminate' => false,
                'child_routes'  => [
                    'keyword' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/keyword[/:id]',
                            'defaults' => [
                                'controller' => 'Keyword\Controller\API\Keyword'
                            ]
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'form' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'    => '/fusion',
                                    'defaults' => [
                                        'controller' => 'Keyword\Controller\API\Keyword',
                                        'action'     => 'fusion',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'group' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'      => '/group[/:id]',
                            'defaults' => [
                                'controller' => 'Keyword\Controller\API\Group'
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class       => ServiceLocatorFactory::class,
            Controller\API\KeywordController::class => ServiceLocatorFactory::class,
            Controller\API\GroupController::class   => ServiceLocatorFactory::class,
        ],
        'aliases' => [
            'Keyword\Controller\Index'       => Controller\IndexController::class,
            'Keyword\Controller\API\Keyword' => Controller\API\KeywordController::class,
            'Keyword\Controller\API\Group'   => Controller\API\GroupController::class,
        ],
    ],
];
