(function (app) {

    app.controller('timesheetController', [
        '$scope', '$http',
        function timesheetController($scope, $http) {
            var self         = this;
            var shouldReload = false;

            self.datas  = [];
            self.totals = [];

            $scope.$on('switchTab', function (event, data) {
                if (data.active === 'timesheet' && shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                }
            });

            $scope.$on('applyFilters', function (event, data) {
                if (data.active === 'timesheet' && data.shouldReload) {
                    shouldReload = false;
                    self.applyFilters(data);
                } else if (data.shouldReload) {
                    shouldReload = true;
                }
            });

            self.applyFilters = function (data) {
                self.loadingOn();
                $http({
                    url   : '/analysis/time/timesheets?' + $.param({filters: data.filters}),
                    method: 'GET'
                }).then(function (res) {
                    self.datas = res.data;

                    self.loadingOff();
                }, function () {});
            };

            self.export = function (type, parent, group) {
                $scope.$emit('export', {type: type, parent: parent, group: group});
            };

            self.loadingOn = function () {
                self.loading = true;
                $scope.$emit('loadingOn');
            };

            self.loadingOff = function () {
                self.loading = false;
                $scope.$emit('loadingOff');
            };
        }
    ]);

})(window.EVA.app);
