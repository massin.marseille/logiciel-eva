<?php
namespace Directory\Entity;

use Core\Entity\BasicRestEntityAbstract;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Laminas\InputFilter\Factory;

/**
 * @ORM\Entity
 * @ORM\Table(name="directory_contact_email")
 */
class ContactEmail extends BasicRestEntityAbstract
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Directory\Entity\Contact", inversedBy="emails")
     */
    protected $contact;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    public function getInputFilter(EntityManager $entityManager)
    {
        return (new Factory())->createInputFilter([
            [
                'name'     => 'name',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
            ],
            [
                'name'       => 'email',
                'required'   => true,
                'validators' => [
                    ['name' => 'EmailAddress'],
                ],
            ],
            [
                'name'       => 'contact',
                'required'   => true,
                'validators' => [
                    [
                        'name'    => 'Callback',
                        'options' => [
                            'callback' => function ($value) use ($entityManager) {
                                if (is_array($value)) {
                                    $value = $value['id'];
                                }

                                return $entityManager
                                        ->getRepository('Directory\Entity\Contact')
                                        ->find($value)
                                    !== null;
                            },
                            'message'  => 'email_field_contact_error_non_exists',
                        ],
                    ],
                ],
            ],
        ]);
    }
}
